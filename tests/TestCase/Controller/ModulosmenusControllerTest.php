<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ModulosmenusController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ModulosmenusController Test Case
 */
class ModulosmenusControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.modulosmenus',
        'app.modulos',
        'app.usermodulos',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.apuracaoformas',
        'app.previsaoorcamentos',
        'app.tiposervicos',
        'app.gruposervicos',
        'app.areaservicos',
        'app.grupodocumentos',
        'app.tipodocumentos',
        'app.documentos',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.mensagensdocumentos',
        'app.mensagens',
        'app.mensagensdestinatarios',
        'app.movimentacaobancarias',
        'app.movimentacaotipos',
        'app.fornecedores',
        'app.contabancarias',
        'app.clientes',
        'app.bancos',
        'app.boletos',
        'app.contratos',
        'app.formaspagamentos',
        'app.tiporeceitas',
        'app.tipodespesas',
        'app.movimentacoesfuturas',
        'app.tarefadocumentos',
        'app.tarefas',
        'app.tarefatipos',
        'app.tarefaprioridades',
        'app.tarefausuarios',
        'app.usersareaservicos',
        'app.telaquestionarios',
        'app.telas',
        'app.tipoactions',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.empresasocios',
        'app.comunhaoregimes',
        'app.acessosexternos',
        'app.sistemasexternos',
        'app.menus',
        'app.menusubmenus',
        'app.submenus'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
