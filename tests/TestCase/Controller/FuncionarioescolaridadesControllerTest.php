<?php
namespace App\Test\TestCase\Controller;

use App\Controller\FuncionarioescolaridadesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\FuncionarioescolaridadesController Test Case
 */
class FuncionarioescolaridadesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.funcionarioescolaridades',
        'app.funcionarios',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.apuracaoformas',
        'app.previsaoorcamentos',
        'app.tiposervicos',
        'app.gruposervicos',
        'app.areaservicos',
        'app.grupodocumentos',
        'app.tipodocumentos',
        'app.documentos',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.empresasocios',
        'app.cargos',
        'app.estadocivils',
        'app.comunhaoregimes',
        'app.sexos',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.turnos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
