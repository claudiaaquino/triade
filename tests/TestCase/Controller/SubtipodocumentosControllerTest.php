<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SubtipodocumentosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SubtipodocumentosController Test Case
 */
class SubtipodocumentosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.subtipodocumentos',
        'app.tipodocumentos',
        'app.documentos',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.empresacnaes',
        'app.cnaes',
        'app.empresausuarios',
        'app.users',
        'app.tipousuarios',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.turnos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
