<?php
namespace App\Test\TestCase\Controller;

use App\Controller\EscolasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\EscolasController Test Case
 */
class EscolasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.escolas',
        'app.escolacampus',
        'app.funcionarioescolaridades',
        'app.funcionarios',
        'app.users',
        'app.empresas',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.documentos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolacampus',
        'app.turnos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
