<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TiponotesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TiponotesController Test Case
 */
class TiponotesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tiponotes',
        'app.anotacaoassuntos',
        'app.assuntos',
        'app.users',
        'app.tipousuarios',
        'app.usermodulos',
        'app.modulos',
        'app.modulosmenus',
        'app.menus',
        'app.areaservicos',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.clientes',
        'app.contabancarias',
        'app.fornecedores',
        'app.movimentacaobancarias',
        'app.movimentacaotipos',
        'app.formaspagamentos',
        'app.tiporeceitas',
        'app.tipodespesas',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.comunhaoregimes',
        'app.empresasocios',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.mensagensdocumentos',
        'app.mensagens',
        'app.mensagensdestinatarios',
        'app.tarefadocumentos',
        'app.tarefas',
        'app.tarefatipos',
        'app.tiposervicos',
        'app.gruposervicos',
        'app.tipodeveres',
        'app.empresadeveres',
        'app.contratos',
        'app.boletos',
        'app.previsaoorcamentos',
        'app.apuracaoformas',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.formaspagamentoservicos',
        'app.telas',
        'app.telaquestionarios',
        'app.leisnormas',
        'app.entidades',
        'app.orgaos',
        'app.leisartigos',
        'app.leispartes',
        'app.legpartes',
        'app.leislivros',
        'app.leglivros',
        'app.leistitulos',
        'app.leiscapitulos',
        'app.legcapitulos',
        'app.leissections',
        'app.legsections',
        'app.leissubsections',
        'app.legsubsections',
        'app.leisincisos',
        'app.leisparagrafos',
        'app.leisletras',
        'app.leisassuntos',
        'app.leisdocumentos',
        'app.tipoactions',
        'app.userservicos',
        'app.empresaservicos',
        'app.tarefaprioridades',
        'app.servicos',
        'app.servicodocumentos',
        'app.tarefausuarios',
        'app.movimentacoesfuturas',
        'app.bancos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.empresausuarios',
        'app.acessosexternos',
        'app.sistemasexternos',
        'app.legislativoempresas',
        'app.usersareaservicos',
        'app.menusubmenus',
        'app.submenus',
        'app.assuntosinformativos',
        'app.tipoinformativos',
        'app.assuntosdocumentos',
        'app.assuntostags',
        'app.tags'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
