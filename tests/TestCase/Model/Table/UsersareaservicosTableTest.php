<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersareaservicosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersareaservicosTable Test Case
 */
class UsersareaservicosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersareaservicosTable
     */
    public $Usersareaservicos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.usersareaservicos',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.areaservicos',
        'app.gruposervicos',
        'app.tiposervicos',
        'app.previsaoorcamentos',
        'app.apuracaoformas',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.mensagens',
        'app.mensagensdestinatarios',
        'app.tarefas',
        'app.tarefatipos',
        'app.tarefaprioridades',
        'app.tarefadocumentos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.empresasocios',
        'app.comunhaoregimes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Usersareaservicos') ? [] : ['className' => 'App\Model\Table\UsersareaservicosTable'];
        $this->Usersareaservicos = TableRegistry::get('Usersareaservicos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Usersareaservicos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
