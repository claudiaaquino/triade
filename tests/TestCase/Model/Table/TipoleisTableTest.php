<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TipoleisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TipoleisTable Test Case
 */
class TipoleisTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TipoleisTable
     */
    public $Tipoleis;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tipoleis'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tipoleis') ? [] : ['className' => 'App\Model\Table\TipoleisTable'];
        $this->Tipoleis = TableRegistry::get('Tipoleis', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tipoleis);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
