<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GrupodocumentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GrupodocumentosTable Test Case
 */
class GrupodocumentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GrupodocumentosTable
     */
    public $Grupodocumentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.grupodocumentos',
        'app.areaservicos',
        'app.tipodocumentos',
        'app.documentos',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.empresacnaes',
        'app.cnaes',
        'app.empresausuarios',
        'app.users',
        'app.tipousuarios',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.turnos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Grupodocumentos') ? [] : ['className' => 'App\Model\Table\GrupodocumentosTable'];
        $this->Grupodocumentos = TableRegistry::get('Grupodocumentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Grupodocumentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
