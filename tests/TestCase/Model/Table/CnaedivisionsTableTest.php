<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CnaedivisionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CnaedivisionsTable Test Case
 */
class CnaedivisionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CnaedivisionsTable
     */
    public $Cnaedivisions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cnaedivisions',
        'app.cnaesections',
        'app.cnaegroups',
        'app.cnaeclasses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Cnaedivisions') ? [] : ['className' => 'App\Model\Table\CnaedivisionsTable'];
        $this->Cnaedivisions = TableRegistry::get('Cnaedivisions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cnaedivisions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
