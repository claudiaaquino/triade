<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TipoinformativosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TipoinformativosTable Test Case
 */
class TipoinformativosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TipoinformativosTable
     */
    public $Tipoinformativos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tipoinformativos',
        'app.assuntosinformativos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tipoinformativos') ? [] : ['className' => 'App\Model\Table\TipoinformativosTable'];
        $this->Tipoinformativos = TableRegistry::get('Tipoinformativos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tipoinformativos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
