<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmpresausuariosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EmpresausuariosTable Test Case
 */
class EmpresausuariosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EmpresausuariosTable
     */
    public $Empresausuarios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.empresausuarios',
        'app.users',
        'app.empresas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Empresausuarios') ? [] : ['className' => 'App\Model\Table\EmpresausuariosTable'];
        $this->Empresausuarios = TableRegistry::get('Empresausuarios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Empresausuarios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
