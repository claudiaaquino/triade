<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GruposervicosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GruposervicosTable Test Case
 */
class GruposervicosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GruposervicosTable
     */
    public $Gruposervicos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.gruposervicos',
        'app.areaservicos',
        'app.grupodocumentos',
        'app.tipodocumentos',
        'app.documentos',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.turnos',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresasocios',
        'app.comunhaoregimes',
        'app.tiposervicos',
        'app.previsaoorcamentos',
        'app.apuracaoformas',
        'app.atuacaoramos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Gruposervicos') ? [] : ['className' => 'App\Model\Table\GruposervicosTable'];
        $this->Gruposervicos = TableRegistry::get('Gruposervicos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Gruposervicos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
