<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LeisdocumentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LeisdocumentosTable Test Case
 */
class LeisdocumentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LeisdocumentosTable
     */
    public $Leisdocumentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.leisdocumentos',
        'app.leisnormas',
        'app.entidades',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.clientes',
        'app.contabancarias',
        'app.fornecedores',
        'app.movimentacaobancarias',
        'app.movimentacaotipos',
        'app.formaspagamentos',
        'app.tiporeceitas',
        'app.tipodespesas',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.areaservicos',
        'app.gruposervicos',
        'app.tiposervicos',
        'app.contratos',
        'app.boletos',
        'app.previsaoorcamentos',
        'app.apuracaoformas',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.formaspagamentoservicos',
        'app.telas',
        'app.telaquestionarios',
        'app.tipoactions',
        'app.tarefas',
        'app.tarefatipos',
        'app.tarefaprioridades',
        'app.tarefadocumentos',
        'app.tarefausuarios',
        'app.mensagens',
        'app.mensagensdocumentos',
        'app.mensagensdestinatarios',
        'app.usersareaservicos',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.comunhaoregimes',
        'app.empresasocios',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.movimentacoesfuturas',
        'app.bancos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.acessosexternos',
        'app.sistemasexternos',
        'app.usermodulos',
        'app.modulos',
        'app.modulosmenus',
        'app.menus',
        'app.menusubmenus',
        'app.submenus',
        'app.orgaos',
        'app.leisartigos',
        'app.leispartes',
        'app.leislivros',
        'app.leistitulos',
        'app.leiscapitulos',
        'app.legcapitulos',
        'app.leissections',
        'app.leissubsections',
        'app.leisincisos',
        'app.leisletras',
        'app.leisparagrafos',
        'app.leisassuntos',
        'app.assuntos',
        'app.anotacaoassuntos',
        'app.tiponotes',
        'app.assuntosdocumentos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Leisdocumentos') ? [] : ['className' => 'App\Model\Table\LeisdocumentosTable'];
        $this->Leisdocumentos = TableRegistry::get('Leisdocumentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Leisdocumentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
