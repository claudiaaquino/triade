<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AtividadesauxiliaresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AtividadesauxiliaresTable Test Case
 */
class AtividadesauxiliaresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AtividadesauxiliaresTable
     */
    public $Atividadesauxiliares;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.atividadesauxiliares'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Atividadesauxiliares') ? [] : ['className' => 'App\Model\Table\AtividadesauxiliaresTable'];
        $this->Atividadesauxiliares = TableRegistry::get('Atividadesauxiliares', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Atividadesauxiliares);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
