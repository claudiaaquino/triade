<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmpresaatividadesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EmpresaatividadesTable Test Case
 */
class EmpresaatividadesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EmpresaatividadesTable
     */
    public $Empresaatividades;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.empresaatividades',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.areaservicos',
        'app.gruposervicos',
        'app.tiposervicos',
        'app.previsaoorcamentos',
        'app.apuracaoformas',
        'app.atuacaoramos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.turnos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresasocios',
        'app.comunhaoregimes',
        'app.atividadesauxiliares'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Empresaatividades') ? [] : ['className' => 'App\Model\Table\EmpresaatividadesTable'];
        $this->Empresaatividades = TableRegistry::get('Empresaatividades', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Empresaatividades);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
