<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EscolasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EscolasTable Test Case
 */
class EscolasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EscolasTable
     */
    public $Escolas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.escolas',
        'app.escolacampus',
        'app.funcionarioescolaridades',
        'app.funcionarios',
        'app.users',
        'app.empresas',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.documentos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolacampus',
        'app.turnos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Escolas') ? [] : ['className' => 'App\Model\Table\EscolasTable'];
        $this->Escolas = TableRegistry::get('Escolas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Escolas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
