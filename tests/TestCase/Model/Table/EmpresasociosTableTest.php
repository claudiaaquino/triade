<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmpresasociosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EmpresasociosTable Test Case
 */
class EmpresasociosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EmpresasociosTable
     */
    public $Empresasocios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.empresasocios',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.empresacnaes',
        'app.cnaes',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.areaservicos',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.turnos',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.comunhaoregimes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Empresasocios') ? [] : ['className' => 'App\Model\Table\EmpresasociosTable'];
        $this->Empresasocios = TableRegistry::get('Empresasocios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Empresasocios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
