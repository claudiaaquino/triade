<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MovimentacaotiposTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MovimentacaotiposTable Test Case
 */
class MovimentacaotiposTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MovimentacaotiposTable
     */
    public $Movimentacaotipos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.movimentacaotipos',
        'app.livrocaixas',
        'app.movimentacaobancarias'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Movimentacaotipos') ? [] : ['className' => 'App\Model\Table\MovimentacaotiposTable'];
        $this->Movimentacaotipos = TableRegistry::get('Movimentacaotipos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Movimentacaotipos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
