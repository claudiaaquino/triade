<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LogErrosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LogErrosTable Test Case
 */
class LogErrosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LogErrosTable
     */
    public $LogErros;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.log_erros',
        'app.users',
        'app.tipousuarios',
        'app.usermodulos',
        'app.modulos',
        'app.modulosmenus',
        'app.menus',
        'app.areaservicos',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.clientes',
        'app.contabancarias',
        'app.fornecedores',
        'app.movimentacaobancarias',
        'app.movimentacaotipos',
        'app.formaspagamentos',
        'app.tiporeceitas',
        'app.tipodespesas',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.comunhaoregimes',
        'app.empresasocios',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.mensagensdocumentos',
        'app.mensagens',
        'app.mensagensdestinatarios',
        'app.tarefadocumentos',
        'app.tarefas',
        'app.tarefatipos',
        'app.tiposervicos',
        'app.gruposervicos',
        'app.tipodeveres',
        'app.empresadeveres',
        'app.contratos',
        'app.boletos',
        'app.previsaoorcamentos',
        'app.apuracaoformas',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.formaspagamentoservicos',
        'app.telas',
        'app.telaquestionarios',
        'app.assuntos',
        'app.anotacaoassuntos',
        'app.tiponotes',
        'app.assuntosinformativos',
        'app.tipoinformativos',
        'app.assuntosdocumentos',
        'app.leisassuntos',
        'app.leisnormas',
        'app.entidades',
        'app.orgaos',
        'app.leisartigos',
        'app.leispartes',
        'app.legpartes',
        'app.leislivros',
        'app.leglivros',
        'app.leistitulos',
        'app.leiscapitulos',
        'app.legcapitulos',
        'app.leissections',
        'app.legsections',
        'app.leissubsections',
        'app.legsubsections',
        'app.leisincisos',
        'app.leisparagrafos',
        'app.leisletras',
        'app.leisdocumentos',
        'app.assuntostags',
        'app.tags',
        'app.tipoactions',
        'app.userservicos',
        'app.empresaservicos',
        'app.tarefaprioridades',
        'app.servicos',
        'app.servicodocumentos',
        'app.tarefausuarios',
        'app.movimentacoesfuturas',
        'app.bancos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.empresausuarios',
        'app.acessosexternos',
        'app.sistemasexternos',
        'app.legislativoempresas',
        'app.usersareaservicos',
        'app.menusubmenus',
        'app.submenus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LogErros') ? [] : ['className' => 'App\Model\Table\LogErrosTable'];
        $this->LogErros = TableRegistry::get('LogErros', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LogErros);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
