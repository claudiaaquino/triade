<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TipoactionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TipoactionsTable Test Case
 */
class TipoactionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TipoactionsTable
     */
    public $Tipoactions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tipoactions',
        'app.telaquestionarios',
        'app.telas',
        'app.tiposervicos',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.apuracaoformas',
        'app.previsaoorcamentos',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.empresasocios',
        'app.cargos',
        'app.funcionarios',
        'app.estadocivils',
        'app.sexos',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.areaservicos',
        'app.gruposervicos',
        'app.mensagens',
        'app.mensagensdestinatarios',
        'app.tarefas',
        'app.tarefatipos',
        'app.tarefaprioridades',
        'app.tarefadocumentos',
        'app.usersareaservicos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.comunhaoregimes',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tipoactions') ? [] : ['className' => 'App\Model\Table\TipoactionsTable'];
        $this->Tipoactions = TableRegistry::get('Tipoactions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tipoactions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
