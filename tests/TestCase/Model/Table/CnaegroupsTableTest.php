<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CnaegroupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CnaegroupsTable Test Case
 */
class CnaegroupsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CnaegroupsTable
     */
    public $Cnaegroups;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.cnaeclasses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Cnaegroups') ? [] : ['className' => 'App\Model\Table\CnaegroupsTable'];
        $this->Cnaegroups = TableRegistry::get('Cnaegroups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cnaegroups);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
