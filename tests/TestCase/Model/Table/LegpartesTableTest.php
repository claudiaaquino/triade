<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LegpartesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LegpartesTable Test Case
 */
class LegpartesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LegpartesTable
     */
    public $Legpartes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.legpartes',
        'app.leispartes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Legpartes') ? [] : ['className' => 'App\Model\Table\LegpartesTable'];
        $this->Legpartes = TableRegistry::get('Legpartes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Legpartes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
