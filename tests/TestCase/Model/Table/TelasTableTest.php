<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TelasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TelasTable Test Case
 */
class TelasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TelasTable
     */
    public $Telas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.telas',
        'app.telaquestionarios',
        'app.tipoactions',
        'app.tiposervicos',
        'app.gruposervicos',
        'app.areaservicos',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.clientes',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.usermodulos',
        'app.modulos',
        'app.modulosmenus',
        'app.menus',
        'app.menusubmenus',
        'app.submenus',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.comunhaoregimes',
        'app.empresasocios',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.mensagensdocumentos',
        'app.mensagens',
        'app.mensagensdestinatarios',
        'app.movimentacaobancarias',
        'app.movimentacaotipos',
        'app.fornecedores',
        'app.contabancarias',
        'app.bancos',
        'app.boletos',
        'app.contratos',
        'app.formaspagamentos',
        'app.tiporeceitas',
        'app.tipodespesas',
        'app.movimentacoesfuturas',
        'app.tarefadocumentos',
        'app.tarefas',
        'app.tarefatipos',
        'app.tarefaprioridades',
        'app.tarefausuarios',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.usersareaservicos',
        'app.apuracaoformas',
        'app.previsaoorcamentos',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.acessosexternos',
        'app.sistemasexternos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Telas') ? [] : ['className' => 'App\Model\Table\TelasTable'];
        $this->Telas = TableRegistry::get('Telas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Telas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
