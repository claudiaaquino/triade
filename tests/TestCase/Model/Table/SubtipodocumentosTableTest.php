<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubtipodocumentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubtipodocumentosTable Test Case
 */
class SubtipodocumentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubtipodocumentosTable
     */
    public $Subtipodocumentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.subtipodocumentos',
        'app.tipodocumentos',
        'app.documentos',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.empresacnaes',
        'app.cnaes',
        'app.empresausuarios',
        'app.users',
        'app.tipousuarios',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.turnos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Subtipodocumentos') ? [] : ['className' => 'App\Model\Table\SubtipodocumentosTable'];
        $this->Subtipodocumentos = TableRegistry::get('Subtipodocumentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Subtipodocumentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
