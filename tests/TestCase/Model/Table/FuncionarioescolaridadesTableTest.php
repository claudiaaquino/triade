<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FuncionarioescolaridadesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FuncionarioescolaridadesTable Test Case
 */
class FuncionarioescolaridadesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FuncionarioescolaridadesTable
     */
    public $Funcionarioescolaridades;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.funcionarioescolaridades',
        'app.funcionarios',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.apuracaoformas',
        'app.previsaoorcamentos',
        'app.tiposervicos',
        'app.gruposervicos',
        'app.areaservicos',
        'app.grupodocumentos',
        'app.tipodocumentos',
        'app.documentos',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.empresasocios',
        'app.cargos',
        'app.estadocivils',
        'app.comunhaoregimes',
        'app.sexos',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.turnos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Funcionarioescolaridades') ? [] : ['className' => 'App\Model\Table\FuncionarioescolaridadesTable'];
        $this->Funcionarioescolaridades = TableRegistry::get('Funcionarioescolaridades', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Funcionarioescolaridades);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
