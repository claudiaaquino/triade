<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LeglivrosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LeglivrosTable Test Case
 */
class LeglivrosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LeglivrosTable
     */
    public $Leglivros;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.leglivros',
        'app.leislivros'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Leglivros') ? [] : ['className' => 'App\Model\Table\LeglivrosTable'];
        $this->Leglivros = TableRegistry::get('Leglivros', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Leglivros);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
