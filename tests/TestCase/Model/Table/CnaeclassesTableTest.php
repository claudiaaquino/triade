<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CnaeclassesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CnaeclassesTable Test Case
 */
class CnaeclassesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CnaeclassesTable
     */
    public $Cnaeclasses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cnaeclasses',
        'app.empresacnaes',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.areaservicos',
        'app.gruposervicos',
        'app.tiposervicos',
        'app.previsaoorcamentos',
        'app.apuracaoformas',
        'app.atuacaoramos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.turnos',
        'app.empresasocios',
        'app.comunhaoregimes',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Cnaeclasses') ? [] : ['className' => 'App\Model\Table\CnaeclassesTable'];
        $this->Cnaeclasses = TableRegistry::get('Cnaeclasses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cnaeclasses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
