<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NivelescolaridadesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NivelescolaridadesTable Test Case
 */
class NivelescolaridadesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NivelescolaridadesTable
     */
    public $Nivelescolaridades;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nivelescolaridades',
        'app.cursos',
        'app.funcionarioescolaridades',
        'app.funcionarios',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.apuracaoformas',
        'app.previsaoorcamentos',
        'app.tiposervicos',
        'app.gruposervicos',
        'app.areaservicos',
        'app.grupodocumentos',
        'app.tipodocumentos',
        'app.documentos',
        'app.atuacaoramos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.empresasocios',
        'app.cargos',
        'app.estadocivils',
        'app.comunhaoregimes',
        'app.sexos',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.escolas',
        'app.escolacampus',
        'app.turnos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Nivelescolaridades') ? [] : ['className' => 'App\Model\Table\NivelescolaridadesTable'];
        $this->Nivelescolaridades = TableRegistry::get('Nivelescolaridades', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Nivelescolaridades);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
