<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AtuacaoramosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AtuacaoramosTable Test Case
 */
class AtuacaoramosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AtuacaoramosTable
     */
    public $Atuacaoramos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.atuacaoramos',
        'app.previsaoorcamentos',
        'app.tiposervicos',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.apuracaoformas',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.empresasocios',
        'app.cargos',
        'app.funcionarios',
        'app.estadocivils',
        'app.sexos',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.areaservicos',
        'app.gruposervicos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.turnos',
        'app.comunhaoregimes',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Atuacaoramos') ? [] : ['className' => 'App\Model\Table\AtuacaoramosTable'];
        $this->Atuacaoramos = TableRegistry::get('Atuacaoramos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Atuacaoramos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
