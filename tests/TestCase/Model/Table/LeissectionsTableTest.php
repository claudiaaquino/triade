<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LeissectionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LeissectionsTable Test Case
 */
class LeissectionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LeissectionsTable
     */
    public $Leissections;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.leissections',
        'app.leisnormas',
        'app.entidades',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.clientes',
        'app.contabancarias',
        'app.fornecedores',
        'app.movimentacaobancarias',
        'app.movimentacaotipos',
        'app.formaspagamentos',
        'app.tiporeceitas',
        'app.tipodespesas',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.areaservicos',
        'app.gruposervicos',
        'app.tiposervicos',
        'app.contratos',
        'app.boletos',
        'app.previsaoorcamentos',
        'app.apuracaoformas',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.formaspagamentoservicos',
        'app.telas',
        'app.telaquestionarios',
        'app.tipoactions',
        'app.tarefas',
        'app.tarefatipos',
        'app.tarefaprioridades',
        'app.tarefadocumentos',
        'app.tarefausuarios',
        'app.mensagens',
        'app.mensagensdocumentos',
        'app.mensagensdestinatarios',
        'app.usersareaservicos',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.comunhaoregimes',
        'app.empresasocios',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.movimentacoesfuturas',
        'app.bancos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.acessosexternos',
        'app.sistemasexternos',
        'app.usermodulos',
        'app.modulos',
        'app.modulosmenus',
        'app.menus',
        'app.menusubmenus',
        'app.submenus',
        'app.orgaos',
        'app.leisartigos',
        'app.leispartes',
        'app.legpartes',
        'app.leislivros',
        'app.leglivros',
        'app.leistitulos',
        'app.leiscapitulos',
        'app.legcapitulos',
        'app.leissubsections',
        'app.legsubsections',
        'app.leisincisos',
        'app.leisparagrafos',
        'app.leisletras',
        'app.leisassuntos',
        'app.assuntos',
        'app.anotacaoassuntos',
        'app.tiponotes',
        'app.assuntosdocumentos',
        'app.leisdocumentos',
        'app.legsections'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Leissections') ? [] : ['className' => 'App\Model\Table\LeissectionsTable'];
        $this->Leissections = TableRegistry::get('Leissections', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Leissections);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
