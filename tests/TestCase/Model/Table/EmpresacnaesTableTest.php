<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmpresacnaesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EmpresacnaesTable Test Case
 */
class EmpresacnaesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EmpresacnaesTable
     */
    public $Empresacnaes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.empresacnaes',
        'app.empresas',
        'app.cnaes',
        'app.estados',
        'app.cidades',
        'app.documentos',
        'app.tipodocumentos',
        'app.funcionarios',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampus',
        'app.turnos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Empresacnaes') ? [] : ['className' => 'App\Model\Table\EmpresacnaesTable'];
        $this->Empresacnaes = TableRegistry::get('Empresacnaes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Empresacnaes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
