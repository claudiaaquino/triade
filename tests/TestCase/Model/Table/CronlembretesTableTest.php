<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CronlembretesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CronlembretesTable Test Case
 */
class CronlembretesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CronlembretesTable
     */
    public $Cronlembretes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cronlembretes',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.clientes',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.usermodulos',
        'app.modulos',
        'app.modulosmenus',
        'app.menus',
        'app.areaservicos',
        'app.grupodocumentos',
        'app.tipodocumentos',
        'app.documentos',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.comunhaoregimes',
        'app.empresasocios',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.mensagensdocumentos',
        'app.mensagens',
        'app.mensagensdestinatarios',
        'app.movimentacaobancarias',
        'app.movimentacaotipos',
        'app.fornecedores',
        'app.contabancarias',
        'app.bancos',
        'app.boletos',
        'app.contratos',
        'app.tiposervicos',
        'app.gruposervicos',
        'app.tipodeveres',
        'app.empresadeveres',
        'app.tarefas',
        'app.tarefatipos',
        'app.tarefaprioridades',
        'app.tarefadocumentos',
        'app.tarefausuarios',
        'app.previsaoorcamentos',
        'app.apuracaoformas',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.formaspagamentoservicos',
        'app.telas',
        'app.telaquestionarios',
        'app.assuntos',
        'app.anotacaoassuntos',
        'app.tiponotes',
        'app.assuntosinformativos',
        'app.tipoinformativos',
        'app.assuntosdocumentos',
        'app.leisassuntos',
        'app.leisnormas',
        'app.entidades',
        'app.orgaos',
        'app.leisartigos',
        'app.leispartes',
        'app.legpartes',
        'app.leislivros',
        'app.leglivros',
        'app.leistitulos',
        'app.leiscapitulos',
        'app.legcapitulos',
        'app.leissections',
        'app.legsections',
        'app.leissubsections',
        'app.legsubsections',
        'app.leisincisos',
        'app.leisparagrafos',
        'app.leisletras',
        'app.leisdocumentos',
        'app.assuntostags',
        'app.tags',
        'app.tipoactions',
        'app.userservicos',
        'app.empresaservicos',
        'app.formaspagamentos',
        'app.tiporeceitas',
        'app.tipodespesas',
        'app.movimentacoesfuturas',
        'app.usersareaservicos',
        'app.menusubmenus',
        'app.submenus',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.acessosexternos',
        'app.sistemasexternos',
        'app.legislativoempresas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Cronlembretes') ? [] : ['className' => 'App\Model\Table\CronlembretesTable'];
        $this->Cronlembretes = TableRegistry::get('Cronlembretes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cronlembretes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
