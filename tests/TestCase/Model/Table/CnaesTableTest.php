<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CnaesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CnaesTable Test Case
 */
class CnaesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CnaesTable
     */
    public $Cnaes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cnaes',
        'app.empresas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Cnaes') ? [] : ['className' => 'App\Model\Table\CnaesTable'];
        $this->Cnaes = TableRegistry::get('Cnaes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cnaes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
