<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EscolacampusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EscolacampusTable Test Case
 */
class EscolacampusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EscolacampusTable
     */
    public $Escolacampus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.escolacampus',
        'app.escolas',
        'app.funcionarioescolaridades',
        'app.funcionarios',
        'app.users',
        'app.empresas',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.documentos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolacampus',
        'app.turnos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Escolacampus') ? [] : ['className' => 'App\Model\Table\EscolacampusTable'];
        $this->Escolacampus = TableRegistry::get('Escolacampus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Escolacampus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
