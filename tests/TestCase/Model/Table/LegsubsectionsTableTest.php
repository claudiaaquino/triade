<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LegsubsectionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LegsubsectionsTable Test Case
 */
class LegsubsectionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LegsubsectionsTable
     */
    public $Legsubsections;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.legsubsections',
        'app.leissubsections'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Legsubsections') ? [] : ['className' => 'App\Model\Table\LegsubsectionsTable'];
        $this->Legsubsections = TableRegistry::get('Legsubsections', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Legsubsections);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
