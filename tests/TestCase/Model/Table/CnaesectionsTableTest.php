<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CnaesectionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CnaesectionsTable Test Case
 */
class CnaesectionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CnaesectionsTable
     */
    public $Cnaesections;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cnaesections',
        'app.cnaedivisions',
        'app.cnaegroups',
        'app.cnaeclasses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Cnaesections') ? [] : ['className' => 'App\Model\Table\CnaesectionsTable'];
        $this->Cnaesections = TableRegistry::get('Cnaesections', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cnaesections);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
