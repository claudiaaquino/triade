<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FormaspagamentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FormaspagamentosTable Test Case
 */
class FormaspagamentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FormaspagamentosTable
     */
    public $Formaspagamentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.formaspagamentos',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.apuracaoformas',
        'app.previsaoorcamentos',
        'app.tiposervicos',
        'app.gruposervicos',
        'app.areaservicos',
        'app.grupodocumentos',
        'app.tipodocumentos',
        'app.documentos',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.mensagens',
        'app.mensagensdestinatarios',
        'app.tarefas',
        'app.tarefatipos',
        'app.tarefaprioridades',
        'app.tarefadocumentos',
        'app.usersareaservicos',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.empresasocios',
        'app.comunhaoregimes',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.livrocaixas',
        'app.movimentacaobancarias'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Formaspagamentos') ? [] : ['className' => 'App\Model\Table\FormaspagamentosTable'];
        $this->Formaspagamentos = TableRegistry::get('Formaspagamentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Formaspagamentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
