<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LeislivrosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LeislivrosTable Test Case
 */
class LeislivrosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LeislivrosTable
     */
    public $Leislivros;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.leislivros',
        'app.leisnormas',
        'app.entidades',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.clientes',
        'app.contabancarias',
        'app.fornecedores',
        'app.movimentacaobancarias',
        'app.movimentacaotipos',
        'app.formaspagamentos',
        'app.tiporeceitas',
        'app.tipodespesas',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.areaservicos',
        'app.gruposervicos',
        'app.tiposervicos',
        'app.contratos',
        'app.boletos',
        'app.previsaoorcamentos',
        'app.apuracaoformas',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.formaspagamentoservicos',
        'app.telas',
        'app.telaquestionarios',
        'app.tipoactions',
        'app.tarefas',
        'app.tarefatipos',
        'app.tarefaprioridades',
        'app.tarefadocumentos',
        'app.tarefausuarios',
        'app.mensagens',
        'app.mensagensdocumentos',
        'app.mensagensdestinatarios',
        'app.usersareaservicos',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.comunhaoregimes',
        'app.empresasocios',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.movimentacoesfuturas',
        'app.bancos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.acessosexternos',
        'app.sistemasexternos',
        'app.usermodulos',
        'app.modulos',
        'app.modulosmenus',
        'app.menus',
        'app.menusubmenus',
        'app.submenus',
        'app.orgaos',
        'app.leisartigos',
        'app.leispartes',
        'app.leistitulos',
        'app.leiscapitulos',
        'app.legcapitulos',
        'app.leissections',
        'app.leissubsections',
        'app.leisincisos',
        'app.leisparagrafos',
        'app.leisletras',
        'app.leisassuntos',
        'app.assuntos',
        'app.anotacaoassuntos',
        'app.tiponotes',
        'app.assuntosdocumentos',
        'app.leglivros'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Leislivros') ? [] : ['className' => 'App\Model\Table\LeislivrosTable'];
        $this->Leislivros = TableRegistry::get('Leislivros', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Leislivros);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
