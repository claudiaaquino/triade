<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ComunhaoregimesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ComunhaoregimesTable Test Case
 */
class ComunhaoregimesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ComunhaoregimesTable
     */
    public $Comunhaoregimes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.comunhaoregimes',
        'app.empresasocios'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Comunhaoregimes') ? [] : ['className' => 'App\Model\Table\ComunhaoregimesTable'];
        $this->Comunhaoregimes = TableRegistry::get('Comunhaoregimes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Comunhaoregimes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
