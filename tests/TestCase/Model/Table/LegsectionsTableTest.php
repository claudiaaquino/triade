<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LegsectionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LegsectionsTable Test Case
 */
class LegsectionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LegsectionsTable
     */
    public $Legsections;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.legsections',
        'app.leissections'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Legsections') ? [] : ['className' => 'App\Model\Table\LegsectionsTable'];
        $this->Legsections = TableRegistry::get('Legsections', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Legsections);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
