<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ServicosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ServicosTable Test Case
 */
class ServicosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ServicosTable
     */
    public $Servicos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.servicos',
        'app.areaservicos',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.clientes',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.usermodulos',
        'app.modulos',
        'app.modulosmenus',
        'app.menus',
        'app.menusubmenus',
        'app.submenus',
        'app.funcionarios',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.comunhaoregimes',
        'app.empresasocios',
        'app.documentos',
        'app.tipodocumentos',
        'app.grupodocumentos',
        'app.mensagensdocumentos',
        'app.mensagens',
        'app.mensagensdestinatarios',
        'app.movimentacaobancarias',
        'app.movimentacaotipos',
        'app.fornecedores',
        'app.contabancarias',
        'app.bancos',
        'app.boletos',
        'app.contratos',
        'app.tiposervicos',
        'app.gruposervicos',
        'app.previsaoorcamentos',
        'app.apuracaoformas',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.formaspagamentoservicos',
        'app.telas',
        'app.telaquestionarios',
        'app.tipoactions',
        'app.tarefas',
        'app.tarefatipos',
        'app.tarefaprioridades',
        'app.tarefadocumentos',
        'app.tarefausuarios',
        'app.formaspagamentos',
        'app.tiporeceitas',
        'app.tipodespesas',
        'app.movimentacoesfuturas',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.usersareaservicos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.acessosexternos',
        'app.sistemasexternos',
        'app.servicodocumentos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Servicos') ? [] : ['className' => 'App\Model\Table\ServicosTable'];
        $this->Servicos = TableRegistry::get('Servicos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Servicos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
