<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MensagemdocumentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MensagemdocumentosTable Test Case
 */
class MensagemdocumentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MensagemdocumentosTable
     */
    public $Mensagemdocumentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.mensagemdocumentos',
        'app.mensagems',
        'app.user_anexous'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Mensagemdocumentos') ? [] : ['className' => 'App\Model\Table\MensagemdocumentosTable'];
        $this->Mensagemdocumentos = TableRegistry::get('Mensagemdocumentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Mensagemdocumentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
