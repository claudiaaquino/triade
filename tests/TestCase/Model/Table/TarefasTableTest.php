<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TarefasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TarefasTable Test Case
 */
class TarefasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TarefasTable
     */
    public $Tarefas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tarefas',
        'app.empresas',
        'app.estados',
        'app.cidades',
        'app.apuracaoformas',
        'app.previsaoorcamentos',
        'app.tiposervicos',
        'app.gruposervicos',
        'app.areaservicos',
        'app.grupodocumentos',
        'app.tipodocumentos',
        'app.documentos',
        'app.funcionarios',
        'app.users',
        'app.tipousuarios',
        'app.empresausuarios',
        'app.usermodulos',
        'app.modulos',
        'app.menus',
        'app.submenus',
        'app.mensagens',
        'app.mensagensdocumentos',
        'app.mensagensdestinatarios',
        'app.usersareaservicos',
        'app.cargos',
        'app.estadocivils',
        'app.sexos',
        'app.funcionariodependentes',
        'app.parentescos',
        'app.funcionarioescolaridades',
        'app.nivelescolaridades',
        'app.cursos',
        'app.escolas',
        'app.escolacampus',
        'app.escolacampuses',
        'app.turnos',
        'app.contratos',
        'app.boletos',
        'app.contabancarias',
        'app.clientes',
        'app.movimentacaobancarias',
        'app.movimentacaotipos',
        'app.fornecedores',
        'app.formaspagamentos',
        'app.tiporeceitas',
        'app.tipodespesas',
        'app.movimentacoesfuturas',
        'app.bancos',
        'app.telaquestionarios',
        'app.telas',
        'app.tipoactions',
        'app.atuacaoramos',
        'app.empresaramos',
        'app.portes',
        'app.empresacnaes',
        'app.cnaeclasses',
        'app.cnaegroups',
        'app.cnaedivisions',
        'app.cnaesections',
        'app.empresaatividades',
        'app.atividadesauxiliares',
        'app.empresasocios',
        'app.comunhaoregimes',
        'app.acessosexternos',
        'app.sistemasexternos',
        'app.tarefatipos',
        'app.tarefaprioridades',
        'app.tarefadocumentos',
        'app.tarefausuarios'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tarefas') ? [] : ['className' => 'App\Model\Table\TarefasTable'];
        $this->Tarefas = TableRegistry::get('Tarefas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tarefas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
