<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LegcapitulosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LegcapitulosTable Test Case
 */
class LegcapitulosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LegcapitulosTable
     */
    public $Legcapitulos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.legcapitulos',
        'app.leiscapitulos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Legcapitulos') ? [] : ['className' => 'App\Model\Table\LegcapitulosTable'];
        $this->Legcapitulos = TableRegistry::get('Legcapitulos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Legcapitulos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
