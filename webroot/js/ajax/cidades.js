$(document).ready(function () {
    var estado = $('#estado-id').attr('sel');
    if (estado !== undefined && estado !== null && estado > 0) {
        preencheCidades(estado);
    }
    $('#estado-id').change(function () {
        preencheCidades($(this).val());
    });

});
function preencheCidades(estado) {

    var cidadeselecionada = $('#cidade-id').attr('sel');
    $.ajax({
        url: $('#urlroot').val() + 'cidades/ajax/' + estado,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = '';
            var selected;
            $.each(data, function (i, item) {
                selected = '';
                if (cidadeselecionada === i) {
                    selected = "selected='selected'";
                }
                html = html + '<option value="' + i + '" ' + selected + ' >' + data[i] + '</option>';
            });
            $('#cidade-id').html(html);
            $('#cidade-id').trigger('change'); 
//            if ($('#cidade-id').hasClass('select2')) {
//                $("#cidade-id").select2("destroy");
//                $("#cidade-id").select2({minimumResultsForSearch: 5, placeholder: 'selecione uma opção'});
//            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}