var servicosolicitavel = 0;
$(document).ready(function () {
    $('#areaservico-id').change(function () {
        if ($('#gruposervico-id').length) {
            preencheGrupoServicos($(this).val(), false);
            $('#gruposervico-id').show();
        }
    });

    $('#gruposervico-id').change(function () {
        preencheTipoServicos($(this).val(), false);
        if ($('.document-details').length) {
            $('.document-details').show();
        }
    });

    $('#filtro-areaservico-id').change(function () {
        if ($('#filtro-gruposervico-id').length) {
            preencheGrupoServicos($(this).val(), true);
        }
    });

    $('#filtro-gruposervico-id').change(function () {
        preencheTipoServicos($(this).val(), true);
    });

});
function preencheGrupoServicos(area, flagfiltro) {
    $.ajax({
        url: $('#urlroot').val() + 'gruposervicos/ajax/' + area,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = "<option value='' selected='selected'> Selecione um subsetor</option>";
            $.each(data, function (i, item) {
                html = html + '<option value="' + i + '">' + data[i] + '</option>';
            });

            if (flagfiltro) {
                $('#filtro-gruposervico-id').html(html);
                $('#filtro-gruposervico-id').trigger('change');
                $('#filtro-tiposervico-id').trigger('change');
            } else {
                $('#gruposervico-id').html(html);
                $('#gruposervico-id').trigger('change');
                $('#tiposervico-id').trigger('change');
            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}
function preencheTipoServicos(grupo, flagfiltro) {
    $.ajax({
        url: $('#urlroot').val() + 'tiposervicos/ajax/' + grupo,
        type: 'PATCH',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = "<option value=''> Selecione um tipo</option>";
            $.each(data, function (i, item) {
                html = html + '<option value="' + i + '">' + data[i] + '</option>';
            });

            if (flagfiltro) {
                $('#filtro-tiposervico-id').html(html);
                $('#filtro-tiposervico-id').trigger('change');
            } else {
                $('#tiposervico-id').html(html);
                $('#tiposervico-id').trigger('change');
            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}
