var mensagens = null;
var current_key = null;
var count_unread = 0;


function registraLeitura(key) {
    $.ajax({
        url: $('#urlroot').val() + 'mensagens/registraleitura/' + mensagens[key].id,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            console.log(data);//registra no log

            $('#flag_lido_' + mensagens[key].id).html('<i class="fa fa-check-square-o"  data-placement="right" data-toggle="tooltip" data-original-title="Mensagem já foi lida" ></i>');//tira o flag icon de que a mensagem não foi lida
            count_unread--; //diminui o numero de mensagens não lidas
            mensagens[key]._matchingData.Mensagensdestinatarios.dt_leitura = data.retorno; //armazena a dt de leitura que foi salva no bd
            refreshUnread(); //atualiza o total de mensagens não lidas que exibe pro usuário
        },
        error: function (a) {
            console.log(a);
        }
    });
}

function showMensagem(key) {

    current_key = key;
    $('#dt_envio').html($.date(mensagens[key].dt_envio, true));
    $('#assunto_mensagem').html(mensagens[key].assunto);
    $('#nome_usuario').html(mensagens[key].user.nome);
    if (mensagens[key].user.empresa) {
        $('#nome_empresa').html(mensagens[key].user.empresa.razao);
    } else {
        $('#nome_empresa').html('Ainda não possui empresa aberta ');
    }

    var listusuarios = "";
    $.each(mensagens[key].mensagensdestinatarios, function (i, usuario) {
        listusuarios += "<div class='btn btn-sm  text-left' ><i class='fa fa-user green' ></i>  " + usuario.user.nome + " (" + usuario.user.empresa.razao + ") </div>";
    });
    $('#userdestinatarios').html(listusuarios);

    $('#texto_mensagem').empty().html(mensagens[key].texto);
    if (mensagens[key].mensagensdocumentos.length > 0) {
        $('#num_anexos').html(mensagens[key].mensagensdocumentos.length);
        $('#box-anexos').show();
        var listanexos = '';
        $.each(mensagens[key].mensagensdocumentos, function (i, anexo) {
            listanexos += "<a target='_blank' href='" + $('#urlroot').val() + "docs/" + anexo.documento.file + "'> ";
            listanexos += "<div class='file-name col-md-4 col-sm-4 col-xs-12 well well-sm green'>" + anexo.documento.tipodocumento.descricao;
            if (anexo.documento.filesize) {
                var kbytes = Math.floor(anexo.documento.filesize / 1024);
                if (kbytes < 1024) {
                    listanexos += " (" + kbytes + " KB)";
                } else {
                    kbytes = Math.floor(kbytes / 1024);
                    listanexos += " (" + kbytes + " MB)";
                }
            }
            listanexos += " <i class='fa fa-download green' style='float:right;'></i></div></a>";
        });
        $('#list-anexos').html(listanexos);
    } else {
        $('#box-anexos').hide();
    }

    //exibe a mensagem
    $('#box-mensagem').show();
    //direciona o foco da tela para ela
    $('html, body').animate({scrollTop: $('#box-mensagem').offset().top}, 'slow');

    //atualiza a dt_leitura do email
    if (!mensagens[key]._matchingData.Mensagensdestinatarios.dt_leitura) {
        registraLeitura(key);
    }
}

function showSentMensagem(key) {
    console.log(mensagens);
    $('#dt_envio').html($.date(mensagens[key].dt_envio, true));
    $('#assunto_mensagem').html(mensagens[key].assunto);

    var listusuarios = "";
    $.each(mensagens[key].mensagensdestinatarios, function (i, usuario) {
        listusuarios += "<div class='btn btn-sm  text-left' ><i class='fa fa-user green' ></i>  " + usuario.user.nome + " (" + usuario.user.empresa.razao + ") </div>";
    });
    $('#nome_usuario').html(listusuarios);
    $('#texto_mensagem').empty().html(mensagens[key].texto);
    if (mensagens[key].mensagensdocumentos.length > 0) {
        $('#num_anexos').html(mensagens[key].mensagensdocumentos.length);
        $('#box-anexos').show();
        var listanexos = '';
        $.each(mensagens[key].mensagensdocumentos, function (i, anexo) {
            listanexos += "<a target='_blank' href='" + $('#urlroot').val() + "docs/" + anexo.documento.file + "'> ";
            listanexos += "<div class='file-name col-md-4 col-sm-4 col-xs-12 well well-sm green'>" + anexo.documento.tipodocumento.descricao;
            if (anexo.documento.filesize) {
                var kbytes = Math.floor(anexo.documento.filesize / 1024);
                if (kbytes < 1024) {
                    listanexos += " (" + kbytes + " KB)";
                } else {
                    kbytes = Math.floor(kbytes / 1024);
                    listanexos += " (" + kbytes + " MB)";
                }
            }
            listanexos += " <i class='fa fa-download green' style='float:right;'></i></div></a>";
        });
        $('#list-anexos').html(listanexos);
    } else {
        $('#box-anexos').hide();
    }

    //exibe a mensagem
    $('#box-mensagem').show();
    //direciona o foco da tela para ela
    $('html, body').animate({scrollTop: $('#box-mensagem').offset().top}, 'slow');

}

function refreshUnread() {
    $('#count-unread').html("(" + count_unread + " Mensagens não lidas)");
}