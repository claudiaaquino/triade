var tablesocios = null;
$(document).ready(function () {
    $('#empresa-id').change(function () {
        if ($("#funcionario-id").length && ($("#user-ids").length || $("#sollaendern").length)) {
            preencheFuncionarios($(this).val());
        }
        if ($("#areaservico-id").length) {
            preencheSetoresEmpresa($(this).val());
        }
        if ($("#user-id").length || $("#user-ids").length) {
            preencheUsuariosEmpresa($(this).val(), null);
        }
    });
    $('#areaservico-id').change(function () {
        if ($("#user-id").length || $("#user-ids").length) {
            preencheUsuariosEmpresa($('#empresa-id').val(), $(this).val());
        }

    });
});
function preencheFuncionarios(empresa) {
    $.ajax({
        url: $('#urlroot').val() + 'funcionarios/listajax/' + empresa,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = "<option value='null'> Selecione (se necessário)</option>";
            $.each(data, function (i, item) {
                html = html + '<option value="' + i + '">' + data[i] + '</option>';
            });
            $('#funcionario-id').html(html);
            $('#funcionario-id').trigger('change');
//            if ($('#funcionario-id').hasClass('select2')) {
//                $("#funcionario-id").select2("destroy");
//                $("#funcionario-id").select2({placeholder: 'selecione (se necessário)', minimumResultsForSearch: 5});
//            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}
function preencheSetoresEmpresa(empresa, areaservico_id) {
    $.ajax({
        url: $('#urlroot').val() + 'empresas/setoresajax/' + empresa,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = "<option value='null'> Selecione (se necessário) </option>";
            $.each(data, function (i, item) {
                html = html + '<option value="' + i + '">' + data[i] + '</option>';
            });
            $('#areaservico-id').html(html);

            if (areaservico_id) {
                $('#areaservico-id').val(areaservico_id);
            }
            $('#areaservico-id').trigger('change');
//
//            if ($('#areaservico-id').hasClass('select2')) {
//                $("#areaservico-id").select2("destroy");
//                $("#areaservico-id").select2({minimumResultsForSearch: 2});
//            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}
function preencheUsuariosEmpresa(empresa, areaservico, selected_user) {

    $.ajax({
        url: $('#urlroot').val() + 'empresas/usuariosajax/',
        data: '&empresa_id=' + empresa + "&areaservico_id=" + areaservico + "&user_id=" + selected_user,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            data = data.retorno;
            var html = "";
            $.each(data, function (i, item) {
                html = html + '<option value="' + i + '">' + data[i] + '</option>';
            });

            var element = $("#user-ids").length ? "user-ids" : "user-id";
            $('#' + element).html(html);

            if (selected_user) {
                $('#' + element).val(selected_user);
            }
            $('#' + element).trigger('change');
//            if ($('#' + element).hasClass('select2')) {
//                $('#' + element).select2("destroy");
//                $('#' + element).select2();
//            }
        },
        error: function (a) {
            console.log(a);
        }
    });
}
