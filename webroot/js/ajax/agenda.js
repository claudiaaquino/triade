var cor_concluida = '#50e31c';
var cor_pendente = '#4e27ef';
var current_task;
$(document).ready(function () {
    loadTasks();

    $('#CalenderModalView').find(".btnEdit").on("click", function () {
        location.href = $('#urlroot').val() + "tarefas/edit/" + current_task.id;
    });

    $('#TaskModalConcluir').find("#btnConfirmaConclusao").on("click", function () {
        concluir(current_task, calendar);
    });

    $('#CalenderModalView').find(".btnView").on("click", function () {
        location.href = $('#urlroot').val() + "tarefas/view/" + current_task.id;
    });

    $('#CalenderModalView').find(".btnRemove").on('click', function () {
        if (confirm("Deseja mesmo remover essa tarefa?")) {
            remove(current_task, calendar);
        }
    });


    $('#btnAtualizaStatusServico').click(function (e) {
        $('#statusservicoField').html($("select[name='statusservico'] :selected").text()); // já coloca no display o novo status, considerando que o update no banco deu certo ne
        updateStatusServico();
    });

    $('.abrir-servico').click(function (e) {
        window.open($('#urlroot').val() + 'servicos/view/' + current_task.servicosolicitado.id, '_blank');
    });

    $("#btnConfirmaReabertura").click(function () {
        reabrirTarefa();
    });

});


var save = function (calendar, started, end, allDay) {

    if (validateTaskFields()) {
//        $('form .formNew').submit();
        $('#antoform').submit();
    } else {
        alert('Preencha todos os campos obrigatórios, por favor.');
        return false;
    }
};
var loadTasks = function () {

    var listTarefas = Array();
    var tarefas;

    $.ajax({
        url: $('#urlroot').val() + 'tarefas/load/',
        data: $("#formFiltros").serialize(),
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            $('#calendar').fullCalendar('removeEvents');
            if (data.tarefas) {
                var arrayTarefas = data.tarefas;
                arrayTarefas.forEach(function (tarefa) {
                    var dataInicio = fullDateToCalendarDate(tarefa.dt_inicio);
                    var dataFim = tarefa.dt_final ? fullDateToCalendarDate(tarefa.dt_final) : "";
                    var dataConcluida = tarefa.dt_concluida ? fullDateToCalendarDate(tarefa.dt_concluida) : "";

                    var allday = true;
                    if (dataInicio.getHours() != '0') {
                        allday = false;
                    }
                    tarefas = {
                        id: tarefa.id,
                        title: tarefa.titulo,
                        start: dataInicio,
                        end: dataFim,
                        dtinicio: fullDateToshortBrDate(tarefa.dt_inicio),
                        dtfinal: fullDateToshortBrDate(tarefa.dt_final),
                        dtconclusao: dataConcluida,
                        allDay: allday,
                        hr_tarefa: dataInicio.getHours(),
                        tipotarefa: tarefa.tarefatipo ? tarefa.tarefatipo.descricao : null,
                        descricao: tarefa.descricao,
                        prioridade: tarefa.tarefaprioridade ? tarefa.tarefaprioridade.descricao : null,
                        dt_final: tarefa.dt_final,
                        empresa: tarefa.empresa ? tarefa.empresa.razao : null,
                        color: dataConcluida ? cor_concluida : tarefa.cor_tarefa,
                        backgroundColor: dataConcluida ? cor_concluida : tarefa.cor_tarefa,
                        servico: tarefa.tiposervico,
                        servicosolicitado: tarefa.servico
                    };
                    listTarefas.push(tarefas);
                });
                $('#calendar').fullCalendar('addEventSource', listTarefas);
            }
        },
        error: function (a) {
            console.log(a);
        }
    });
};
var fullDateToCalendarDate = function (fulldate) {
    fulldate = fulldate.replace(" ", "-").replace("T", "-");
    var arrayDate = fulldate.split("-");
    var startYear = arrayDate[0];
    var startMonth = arrayDate[1] - 1;
    var startDay = parseInt(arrayDate[2]);
    var arrayTime = arrayDate[3].split(":");
    var startHour = arrayTime[0];
    var startMinutes = arrayTime[1];
    return  new Date(startYear, startMonth, startDay, startHour, startMinutes);
};
var fullDateToshortBrDate = function (fulldate) {
    var date = fulldate.split("T");
    date = date[0].split("-");
    return  date[2] + '/' + date[1] + '/' + date[0];
};
var calendarDateToSimpleBrDate = function (date) {
    return  (date.getUTCDate() < 10 ? '0' + date.getUTCDate() : date.getUTCDate())
            + '/' + (date.getUTCMonth() < 9 ? '0' + (date.getUTCMonth() + 1) : (date.getUTCMonth() + 1))
            + '/' + date.getUTCFullYear();
};


var calendar = $('#calendar').fullCalendar({
    contentHeight: 'auto',
    height: 'auto',
    lang: 'pt-br',
    timezone: 'America/Sao_Paulo',
    defaultView: 'basicDay',
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
    },
    selectable: true,
    selectHelper: true,
    select: function (start, end, allDay) {
        $('#fc_create').click();
        var currentdate = calendarDateToSimpleBrDate(start._d);
        $('#dt-inicio').val(currentdate);
        $('#dt-final').val(currentdate);
        $('#dt-lembrete').val(currentdate);

        var utchours = start._d.getUTCHours();
        var utcminutes = start._d.getUTCMinutes();
        if (utchours != '0') {
            var hora = utchours < 10 ? '0' + utchours : utchours;
            var minutos = utcminutes < 10 ? '0' + utcminutes : utcminutes;
            $("#hr-tarefa").val(hora + ':' + minutos);
        }

        $('#btnSave').on('click', function () {
            save(calendar, start, end, allDay);
        });
    },
    eventClick: function (Tarefa, jsEvent, view) {
        current_task = Tarefa;

        $('#fc_view').click();
        $('#tituloField').html(Tarefa.title);
        var hora = '';
        if (Tarefa.hr_tarefa != '0') {
            hora = ' às ' + Tarefa.hr_tarefa + ' horas';
        }
        $('#dtinicioField').html(Tarefa.dtinicio + hora);
        $('#dtfinalField').html(Tarefa.dtfinal);
        $('#prioridadeField').html(Tarefa.prioridade);
        $('#descricaoField').html(Tarefa.descricao);
        $('#tipotarefaField').html(Tarefa.tipotarefa);
        $('#empresaField').html(Tarefa.empresa);


        if (Tarefa.servico) {
            $('#servicoField').html(Tarefa.servico.nome);
            $('#servicoField').show();

            if (Tarefa.servicosolicitado) {
                var statusservico = Tarefa.servicosolicitado.status_servico;
                var status = statusservico == '1' ? 'Solicitado' : statusservico == '2' ? 'Em análise' : statusservico == '3' ? 'Em Execução' : statusservico == '4' ? 'Executado' : 'Sem informação'
                $('#statusservicoField').html(status);
                $('#detalhes_servico').html(Tarefa.servicosolicitado.descricao);
                $('.btnDetailsService').show(); // botão na janelinha da tarefa, para abrir detalhes do serviço solicitado
                $('#statusservicoInfo').show();  //descrição do status na janelinha da tarefa

                $("select[name='statusservico']").val(statusservico).change();


            } else {
                $('.btnDetailsService').hide(); // botão na janelinha da tarefa, para abrir detalhes do serviço solicitado
                $('#statusservicoInfo').hide();

            }


            if (Tarefa.servico.notificacao_concluido_cliente) {
                $('.notificacao_cliente').show();
            } else {
                $('.notificacao_cliente').hide();
            }


            if (Tarefa.servico.notificacao_dt_vencimento) {
                $('.dt_vencimento').show();
            } else {
                $('.dt_vencimento').hide();
            }
        } else {
            $('#servicoField').hide();
            $('.btnDetailsService').hide();
        }

        if (Tarefa.dtconclusao) {
            $('.btnConcluir').hide();
            $('.btnReabrir').show();
        } else {
            $('.btnConcluir').show();
            $('.btnReabrir').hide();

        }

        calendar.fullCalendar('unselect');

    },
    allDayDefault: true,
    editable: false,
    weekends: true,
    businessHours: {
        // days of week. an array of zero-based day of week integers (0=Sunday)
        dow: [1, 2, 3, 4, 5], // Monday - Friday
        start: '08:00', // a start time (10am in this example)
        end: '18:00', // an end time (6pm in this example)
    },
    minTime: "08:00:00",
    maxTime: "19:00:00"

});

var concluir = function (event, calendar) {

    $.ajax({
        url: $('#urlroot').val() + 'tarefas/concluir/' + event.id,
        data: $("#formConcluir").serialize(),
        dataType: 'json',
        type: 'PATCH',
        success: function (data) {
            if (data.retorno) {
                event.dtconclusao = fullDateToCalendarDate(data.retorno);
                event.backgroundColor = cor_concluida;
                event.color = cor_concluida;
                calendar.fullCalendar('updateEvent', event);

                new PNotify({text: 'Tarefa concluída!!', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
            } else {
                new PNotify({text: 'Erro ao concluir essa tarefa. Por favor, tente novamente.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
            }
        },
        error: function (a) {
            new PNotify({text: 'Erro ao concluir essa tarefa. Por favor, tente novamente.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
        }
    });
};
var remove = function (event, calendar) {

    $.ajax({
        url: $('#urlroot').val() + 'tarefas/deleteajax/',
        data: {
            id: event.id
        },
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                calendar.fullCalendar('removeEvents', event.id);
                $("#CalenderModalView").find(".antoclose2").trigger("click");

                new PNotify({text: 'Tarefa exluida da agenda!!', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
            } else {
                new PNotify({text: 'Não foi possível exluir essa tarefa.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
            }
        },
        error: function (a) {
            new PNotify({text: 'Não foi possível exluir essa tarefa.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
        }
    });
};

var validateTaskFields = function () {
    if (!$('#tarefatipo-id').val() || !$('#titulo').val()) {
        return false;
    }
    return true;
};

function updateStatusServico() {
    $.ajax({
        url: $('#urlroot').val() + 'servicos/updatestatus/' + current_task.servicosolicitado.id,
        data: $("#formUpdateService").serialize(),
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data.retorno) {
                current_task.servicosolicitado.status_servico = data.retorno;
                calendar.fullCalendar('updateEvent', current_task);
                new PNotify({text: 'Status do Serviço atualizado!!', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
            } else {
                new PNotify({text: 'Não foi possível atualizar o status desse serviço.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
            }
        },
        error: function (a) {
            new PNotify({text: 'Não foi possível atualizar o status desse serviço.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
        }
    });
}

function reabrirTarefa() {
    $.ajax({
        url: $('#urlroot').val() + 'tarefas/reabrir/' + current_task.id,
        data: $("#formReabrir").serialize(),
        type: 'PATCH',
        dataType: 'json',
        success: function (data) {
            if (data.retorno) {
                $('.btnConcluir').show();
                $(".btnReabrir").hide();

                current_task.dtconclusao = null;
                current_task.backgroundColor = cor_pendente;
                current_task.color = cor_pendente;
                calendar.fullCalendar('updateEvent', current_task);

                new PNotify({text: 'Tarefa reaberta na agenda', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
            } else {
                new PNotify({text: 'Não foi possivel reabrir essa tarefa', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
            }
        },
        error: function (a) {
            new PNotify({text: 'Não foi possivel reabrir essa tarefa', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
            console.log(a);
        }
    });

}

/*
 var save = function (calendar, started, end, allDay) {
 if ($(".formNew").find('#titulo').val() !== "") {
 
 $(".formNew").find('#dt-inicio').val(started._d);
 
 $.ajax({
 url: $('#urlroot').val() + 'tarefas/addAjax/',
 data: $(".formNew").serialize(),
 type: 'POST',
 dataType: 'json',
 
 success: function (data) {
 if (data.success) {
 var title = $(".formNew").find('#titulo').val();
 var tarefa = data.tarefa;
 
 
 var objDate = "";
 if ($(".formNew").find('#dt-final').val() !== "") {
 var dataFinal = $(".formNew").find('#dt-final').val();
 
 var arrData = dataFinal.split("/");
 var day = arrData[0];
 var month = arrData[1] - 1;
 var year = arrData[2];
 
 objDate = new Date(year, month, day);
 }
 
 var hasFile = false;
 var ext = "";
 
 if ($(".formNew").find("#doc_upload").val()) {
 uploadFile('formNew', data.tarefa.id);
 hasFile = true;
 var fileName = $(".formNew").find("#doc_upload").val();
 fileName = fileName.split(".");
 ext = fileName[fileName.length - 1];
 }
 
 if (title) {
 calendar.fullCalendar('renderEvent', {
 title: title,
 start: started,
 end: objDate,
 allDay: allDay,
 id: data.tarefa.id,
 tipotarefa: tarefa.tarefatipo_id,
 areaservico: tarefa.areaservico_id,
 descricao: tarefa.descricao,
 prioridade: tarefa.tarefaprioridade_id,
 dt_final: tarefa.dt_final,
 dt_lembrete: tarefa.dt_lembrete,
 permite_deletar: tarefa.permite_deletar,
 file: hasFile ? $(".formNew").find("#doc_nome").val() : "",
 ext: hasFile ? ext : ""
 }, true); //make the event stick
 }
 
 clear();
 calendar.fullCalendar('unselect');
 $('.antoclose').click();
 } else {
 alert('Erro ao salvar. Confira os campos e tente novamente.');
 }
 
 return false;
 },
 error: function (a) {
 alert('Preencha os campos obrigatórios e tente novamente.');
 }
 });
 } else {
 alert('Preencha os campos obrigatórios e tente novamente.');
 }
 };
 
 
 */