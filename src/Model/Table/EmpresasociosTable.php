<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Empresasocios Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\BelongsTo $Cargos
 * @property \Cake\ORM\Association\BelongsTo $Estados
 * @property \Cake\ORM\Association\BelongsTo $Cidades
 * @property \Cake\ORM\Association\BelongsTo $Estadocivils
 * @property \Cake\ORM\Association\BelongsTo $Comunhaoregimes
 * @property \Cake\ORM\Association\BelongsTo $Sexos
 * @property \Cake\ORM\Association\HasMany $Documentos
 *
 * @method \App\Model\Entity\Empresasocio get($primaryKey, $options = [])
 * @method \App\Model\Entity\Empresasocio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Empresasocio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Empresasocio|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Empresasocio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Empresasocio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Empresasocio findOrCreate($search, callable $callback = null)
 */
class EmpresasociosTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('empresasocios');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Empresas', [
            'foreignKey' => 'empresa_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cargos', [
            'foreignKey' => 'cargo_id'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id'
        ]);
        $this->belongsTo('Cidades', [
            'foreignKey' => 'cidade_id'
        ]);
        $this->belongsTo('Estadocivils', [
            'foreignKey' => 'estadocivil_id'
        ]);
        $this->belongsTo('Comunhaoregimes', [
            'foreignKey' => 'comunhaoregime_id'
        ]);
        $this->belongsTo('Sexos', [
            'foreignKey' => 'sexo_id'
        ]);
        $this->hasMany('Documentos', [
            'foreignKey' => 'empresasocio_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('nome', 'create')
                ->notEmpty('nome', 'Informe o Nome, ele é um campo obrigatório');

        $validator
                ->requirePresence('dt_nascimento', 'create')
                ->notEmpty('dt_nascimento');

        $validator
                ->requirePresence('cpf', 'create')
                ->notEmpty('cpf', 'Informe o CPF, ele é um campo obrigatório');

        $validator
                ->email('email')
                ->allowEmpty('email');
//            ->requirePresence('email', 'create')

        $validator
                ->notEmpty('endereco', 'O endereço deve ser informado.');

        $validator
                ->notEmpty('end_numero', 'O número do endereço deve ser informado.');

        $validator
                ->allowEmpty('end_complemento');

        $validator
                ->notEmpty('end_bairro', 'O Bairro deve ser informado');

        $validator
                ->notEmpty('end_cep', 'O CEP deve ser informado.');

        $validator
                ->allowEmpty('telefone_residencial');

        $validator
                ->allowEmpty('telefone_celular');

        $validator
                ->allowEmpty('ctps');

        $validator
                ->allowEmpty('ctps_serie');

        $validator
                ->allowEmpty('cnh');

        $validator
                ->allowEmpty('cnh_dt_habilitacao');

        $validator
                ->allowEmpty('cnh_dt_vencimento');

        $validator
                ->allowEmpty('rg');

        $validator
                ->allowEmpty('rg_estado');

        $validator
                ->allowEmpty('rg_expedidor');

        $validator
                ->allowEmpty('rg_dt_expedicao');

        $validator
                ->allowEmpty('militar_numero');

        $validator
                ->allowEmpty('militar_expedidor');

        $validator
                ->allowEmpty('militar_serie');

        $validator
                ->allowEmpty('eleitor_numero');

        $validator
                ->allowEmpty('eleitor_zona');

        $validator
                ->allowEmpty('eleitor_secao');

        $validator
                ->allowEmpty('eleitor_dt_emissao');

        $validator
                ->allowEmpty('pai_nome');

        $validator
                ->allowEmpty('mae_nome');

        $validator
                ->numeric('percentual_capitalsocial')
                ->allowEmpty('percentual_capitalsocial');

        $validator
                ->boolean('admin')
                ->allowEmpty('admin');

        $validator
                ->date('dt_cadastro')
                ->allowEmpty('dt_cadastro');

        $validator
                ->allowEmpty('last_updated_fields');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->integer('status')
                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
//        $rules->add($rules->isUnique(['email'], 'O email já está sendo usado por outro sócio. Utilize outro email.'));
//        $rules->add($rules->existsIn(['user_id'], 'Users'));
//        $rules->add($rules->existsIn(['empresa_id'], 'Empresas'));
        $rules->add($rules->existsIn(['cargo_id'], 'Cargos'));
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['cidade_id'], 'Cidades'));
        $rules->add($rules->existsIn(['estadocivil_id'], 'Estadocivils'));
        $rules->add($rules->existsIn(['comunhaoregime_id'], 'Comunhaoregimes'));
        $rules->add($rules->existsIn(['sexo_id'], 'Sexos'));

        return $rules;
    }

}
