<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Leisincisos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Leisnormas
 * @property \Cake\ORM\Association\BelongsTo $Leisartigos
 * @property \Cake\ORM\Association\BelongsTo $Leisparagrafos
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Leisinciso get($primaryKey, $options = [])
 * @method \App\Model\Entity\Leisinciso newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Leisinciso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Leisinciso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Leisinciso patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Leisinciso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Leisinciso findOrCreate($search, callable $callback = null)
 */
class LeisincisosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('leisincisos');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Leisnormas', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->belongsTo('Leisartigos', [
            'foreignKey' => 'leisartigo_id'
        ]);
        $this->belongsTo('Leisparagrafos', [
            'foreignKey' => 'leisparagrafo_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nome');

        $validator
            ->allowEmpty('numero');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->boolean('exibe_cliente')
            ->allowEmpty('exibe_cliente');

        $validator
            ->boolean('exibe_funcionario')
            ->allowEmpty('exibe_funcionario');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->integer('ordem')
            ->allowEmpty('ordem');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['leisnorma_id'], 'Leisnormas'));
        $rules->add($rules->existsIn(['leisartigo_id'], 'Leisartigos'));
        $rules->add($rules->existsIn(['leisparagrafo_id'], 'Leisparagrafos'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
