<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Escolacampus Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Escolas
 * @property \Cake\ORM\Association\HasMany $Funcionarioescolaridades
 *
 * @method \App\Model\Entity\Escolacampus get($primaryKey, $options = [])
 * @method \App\Model\Entity\Escolacampus newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Escolacampus[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Escolacampus|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Escolacampus patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Escolacampus[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Escolacampus findOrCreate($search, callable $callback = null)
 */
class EscolacampusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('escolacampus');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Escolas', [
            'foreignKey' => 'escola_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Funcionarioescolaridades', [
            'foreignKey' => 'escolacampus_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->requirePresence('endereco', 'create')
            ->notEmpty('endereco');

        $validator
            ->date('dt_cadastro') ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')->allowEmpty('last_update');

        $validator
            ->integer('status') ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['escola_id'], 'Escolas'));

        return $rules;
    }
}
