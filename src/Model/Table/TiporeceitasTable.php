<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tiporeceitas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Movimentacaobancarias
 *
 * @method \App\Model\Entity\Tiporeceita get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tiporeceita newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tiporeceita[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tiporeceita|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tiporeceita patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tiporeceita[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tiporeceita findOrCreate($search, callable $callback = null)
 */
class TiporeceitasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tiporeceitas');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->belongsTo('Empresas', [
            'foreignKey' => 'empresa_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Movimentacaobancarias', [
            'foreignKey' => 'tiporeceita_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('descricao');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->time('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['empresa_id'], 'Empresas'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
