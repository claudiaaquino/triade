<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Funcionarios Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\BelongsTo $Cargos
 * @property \Cake\ORM\Association\BelongsTo $Estados
 * @property \Cake\ORM\Association\BelongsTo $Cidades
 * @property \Cake\ORM\Association\BelongsTo $Estadocivils
 * @property \Cake\ORM\Association\BelongsTo $Sexos
 * @property \Cake\ORM\Association\BelongsTo $Comunhaoregimes
 * @property \Cake\ORM\Association\HasMany $Documentos
 * @property \Cake\ORM\Association\HasMany $Funcionariodependentes
 * @property \Cake\ORM\Association\HasMany $Funcionarioescolaridades
 *
 * @method \App\Model\Entity\Funcionario get($primaryKey, $options = [])
 * @method \App\Model\Entity\Funcionario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Funcionario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Funcionario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Funcionario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Funcionario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Funcionario findOrCreate($search, callable $callback = null)
 */
class FuncionariosTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('funcionarios');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Empresas', [
            'foreignKey' => 'empresa_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cargos', [
            'foreignKey' => 'cargo_id'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id'
        ]);
        $this->belongsTo('Cidades', [
            'foreignKey' => 'cidade_id'
        ]);
        $this->belongsTo('Estadocivils', [
            'foreignKey' => 'estadocivil_id'
        ]);
        $this->belongsTo('Sexos', [
            'foreignKey' => 'sexo_id'
        ]);
        $this->belongsTo('Comunhaoregimes', [
            'foreignKey' => 'comunhaoregime_id'
        ]);
        $this->hasMany('Documentos', [
            'foreignKey' => 'funcionario_id'
        ]);
        $this->hasMany('Funcionariodependentes', [
            'foreignKey' => 'funcionario_id'
        ]);
        $this->hasMany('Funcionarioescolaridades', [
            'foreignKey' => 'funcionario_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('num_contrato');

        $validator
                ->requirePresence('nome', 'create')
                ->notEmpty('nome');

        $validator
                ->requirePresence('dt_nascimento', 'create')
                ->notEmpty('dt_nascimento');
//                ->date('dt_nascimento')

        $validator
                ->requirePresence('cpf', 'create')
                ->notEmpty('cpf');

        $validator
                ->allowEmpty('endereco');

        $validator
                ->allowEmpty('end_numero');

        $validator
                ->allowEmpty('end_complemento');

        $validator
                ->allowEmpty('end_bairro');

        $validator
                ->allowEmpty('end_cep');

        $validator
                ->allowEmpty('telefone_residencial');

//        $validator
//                ->notEmpty('telefone_celular');
//            ->requirePresence('telefone_celular', 'create')

        $validator
                ->allowEmpty('cpf_responsavel');

        $validator
                ->allowEmpty('telefone_responsavel');

        $validator
                ->allowEmpty('ctps');

        $validator
                ->allowEmpty('ctps_serie');

        $validator
                ->allowEmpty('cnh');

        $validator
                ->allowEmpty('cnh_dt_habilitacao');
//                ->date('cnh_dt_habilitacao')

        $validator
                ->allowEmpty('cnh_dt_vencimento');
//                ->date('cnh_dt_vencimento')

        $validator
                ->allowEmpty('rg');

        $validator
                ->allowEmpty('rg_estado');

        $validator
                ->allowEmpty('rg_expedidor');

        $validator
                ->allowEmpty('rg_dt_expedicao');

        $validator
                ->allowEmpty('militar_numero');

        $validator
                ->allowEmpty('militar_expedidor');

        $validator
                ->allowEmpty('militar_serie');

        $validator
                ->allowEmpty('eleitor_numero');

        $validator
                ->allowEmpty('eleitor_zona');

        $validator
                ->allowEmpty('eleitor_secao');

        $validator
                ->allowEmpty('eleitor_dt_emissao');
//                ->date('eleitor_dt_emissao')

        $validator
                ->allowEmpty('pai_nome');

        $validator
                ->allowEmpty('mae_nome');

        $validator
                ->allowEmpty('salario');
//                ->numeric('salario')

        $validator
                ->allowEmpty('aux_transporte');
//                ->numeric('aux_transporte')

        $validator
                ->allowEmpty('aux_alimentacao');
//                ->numeric('aux_alimentacao')

        $validator
                ->integer('flag_deficiente')
                ->allowEmpty('flag_deficiente');

        $validator
                ->allowEmpty('desc_deficiencia');

        $validator
                ->date('dt_cadastro_sistema')
                ->allowEmpty('dt_cadastro_sistema');

        $validator
                ->allowEmpty('dt_contrato_efetivado');
//                ->date('dt_contrato_efetivado')

        $validator
                ->allowEmpty('dt_demissao');
//                ->date('dt_demissao')

        $validator
                ->allowEmpty('demissao_motivo');

        $validator
                ->allowEmpty('last_updated_fields');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->integer('status')
                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['empresa_id'], 'Empresas'));
        $rules->add($rules->existsIn(['cargo_id'], 'Cargos'));
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['cidade_id'], 'Cidades'));
        $rules->add($rules->existsIn(['estadocivil_id'], 'Estadocivils'));
        $rules->add($rules->existsIn(['sexo_id'], 'Sexos'));

        return $rules;
    }

}
