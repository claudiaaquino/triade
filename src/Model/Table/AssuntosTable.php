<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Assuntos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Assuntos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Anotacaoassuntos
 * @property \Cake\ORM\Association\HasMany $Assuntos
 * @property \Cake\ORM\Association\HasMany $Assuntosdocumentos
 * @property \Cake\ORM\Association\HasMany $Assuntosinformativos
 * @property \Cake\ORM\Association\HasMany $Leisassuntos
 * @property \Cake\ORM\Association\HasMany $Telaquestionarios
 * @property \Cake\ORM\Association\HasMany $Assuntostags
 *
 * @method \App\Model\Entity\Assunto get($primaryKey, $options = [])
 * @method \App\Model\Entity\Assunto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Assunto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Assunto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Assunto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Assunto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Assunto findOrCreate($search, callable $callback = null)
 */
class AssuntosTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('assuntos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->belongsTo('Assuntospai', [
            'className' => 'Assuntos',
            'foreignKey' => 'assunto_id',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Anotacaoassuntos', [
            'foreignKey' => 'assunto_id'
        ]);
        $this->hasMany('Assuntosinformativos', [
            'foreignKey' => 'assunto_id'
        ]);
        $this->hasMany('Assuntosfilhos', [
            'className' => 'Assuntos',
            'foreignKey' => 'assunto_id'
        ]);
        $this->hasMany('Assuntosdocumentos', [
            'foreignKey' => 'assunto_id'
        ]);
        $this->hasMany('Leisassuntos', [
            'foreignKey' => 'assunto_id'
        ]);
        $this->hasMany('Telaquestionarios', [
            'foreignKey' => 'assunto_id'
        ]);
        $this->hasMany('Assuntostags', [
            'foreignKey' => 'assunto_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('nome');

        $validator
                ->notEmpty('descricao');

        $validator
                ->boolean('exibe_cliente')
                ->allowEmpty('exibe_cliente');

        $validator
                ->boolean('exibe_funcionario')
                ->allowEmpty('exibe_funcionario');

        $validator
                ->dateTime('dt_cadastro')
                ->allowEmpty('dt_cadastro');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->boolean('status')
                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['assunto_id'], 'Assuntospai'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

}
