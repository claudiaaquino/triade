<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Comunhaoregimes Model
 *
 * @property \Cake\ORM\Association\HasMany $Empresasocios
 *
 * @method \App\Model\Entity\Comunhaoregime get($primaryKey, $options = [])
 * @method \App\Model\Entity\Comunhaoregime newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Comunhaoregime[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Comunhaoregime|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Comunhaoregime patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Comunhaoregime[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Comunhaoregime findOrCreate($search, callable $callback = null)
 */
class ComunhaoregimesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('comunhaoregimes');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Empresasocios', [
            'foreignKey' => 'comunhaoregime_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }
}
