<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Legcapitulos Model
 *
 * @property \Cake\ORM\Association\HasMany $Leiscapitulos
 *
 * @method \App\Model\Entity\Legcapitulo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Legcapitulo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Legcapitulo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Legcapitulo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Legcapitulo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Legcapitulo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Legcapitulo findOrCreate($search, callable $callback = null)
 */
class LegcapitulosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('legcapitulos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Leiscapitulos', [
            'foreignKey' => 'legcapitulo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        return $validator;
    }
}
