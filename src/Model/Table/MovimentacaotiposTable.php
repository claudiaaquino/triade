<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Movimentacaotipos Model
 *
 * @property \Cake\ORM\Association\HasMany $Movimentacaobancarias
 *
 * @method \App\Model\Entity\Movimentacaotipo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Movimentacaotipo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Movimentacaotipo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Movimentacaotipo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Movimentacaotipo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Movimentacaotipo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Movimentacaotipo findOrCreate($search, callable $callback = null)
 */
class MovimentacaotiposTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('movimentacaotipos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Movimentacaobancarias', [
            'foreignKey' => 'movimentacaotipo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->allowEmpty('evento');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }
}
