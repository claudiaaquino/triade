<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Boletos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Contratos
 * @property \Cake\ORM\Association\BelongsTo $Contabancarias
 *
 * @method \App\Model\Entity\Boleto get($primaryKey, $options = [])
 * @method \App\Model\Entity\Boleto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Boleto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Boleto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Boleto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Boleto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Boleto findOrCreate($search, callable $callback = null)
 */
class BoletosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('boletos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Contratos', [
            'foreignKey' => 'contrato_id'
        ]);
        $this->belongsTo('Contabancarias', [
            'foreignKey' => 'contabancaria_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('filename', 'create')
            ->notEmpty('filename');

        $validator
            ->requirePresence('valor_boleto', 'create')
            ->notEmpty('valor_boleto');

        $validator
            ->date('dt_vencimento')
            ->requirePresence('dt_vencimento', 'create')
            ->notEmpty('dt_vencimento');

        $validator
            ->date('dt_processamento')
            ->allowEmpty('dt_processamento');

        $validator
            ->numeric('valor_multa')
            ->allowEmpty('valor_multa');

        $validator
            ->numeric('valor_multa_dia')
            ->allowEmpty('valor_multa_dia');

        $validator
            ->date('dt_pagamento')
            ->allowEmpty('dt_pagamento');

        $validator
            ->boolean('pago')
            ->allowEmpty('pago');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['contrato_id'], 'Contratos'));
        $rules->add($rules->existsIn(['contabancaria_id'], 'Contabancarias'));

        return $rules;
    }
}
