<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sexos Model
 *
 * @property \Cake\ORM\Association\HasMany $Funcionarios
 *
 * @method \App\Model\Entity\Sexo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Sexo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Sexo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Sexo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sexo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Sexo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Sexo findOrCreate($search, callable $callback = null)
 */
class SexosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sexos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Funcionarios', [
            'foreignKey' => 'sexo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        return $validator;
    }
}
