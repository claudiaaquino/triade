<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Portes Model
 *
 * @property \Cake\ORM\Association\HasMany $Empresas
 *
 * @method \App\Model\Entity\Porte get($primaryKey, $options = [])
 * @method \App\Model\Entity\Porte newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Porte[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Porte|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Porte patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Porte[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Porte findOrCreate($search, callable $callback = null)
 */
class PortesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('portes');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Empresas', [
            'foreignKey' => 'porte_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        return $validator;
    }
}
