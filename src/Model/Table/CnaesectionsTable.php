<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cnaesections Model
 *
 * @property \Cake\ORM\Association\HasMany $Cnaedivisions
 *
 * @method \App\Model\Entity\Cnaesection get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cnaesection newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cnaesection[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cnaesection|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cnaesection patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cnaesection[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cnaesection findOrCreate($search, callable $callback = null)
 */
class CnaesectionsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('cnaesections');
        $this->displayField('secao');
        $this->primaryKey('id');

        $this->hasMany('Cnaedivisions', [
            'foreignKey' => 'cnaesection_id'
        ]);
    }

    public function findAjaxList(\Cake\ORM\Query $query, array $options) {

        $query->find('all');
        $concat = $query->func()->concat([
            'Cnaesections.secao' => 'identifier',
            ' - ',
            'Cnaesections.denominacao' => 'identifier'
        ]);
        $query->select(['id', 'secao' => $concat])->where(["Cnaesections.status" => 1])
                ->order('Cnaesections.secao');

        return $query;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('secao', 'create')
                ->notEmpty('secao');

        $validator
                ->requirePresence('denominacao', 'create')
                ->notEmpty('denominacao');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->integer('status')
                ->allowEmpty('status');

        return $validator;
    }

}
