<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Subtipodocumentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Tipodocumentos
 *
 * @method \App\Model\Entity\Subtipodocumento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Subtipodocumento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Subtipodocumento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Subtipodocumento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Subtipodocumento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Subtipodocumento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Subtipodocumento findOrCreate($search, callable $callback = null)
 */
class SubtipodocumentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('subtipodocumentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Tipodocumentos', [
            'foreignKey' => 'tipodocumento_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tipodocumento_id'], 'Tipodocumentos'));

        return $rules;
    }
}
