<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Formaspagamentoservicos Model
 *
 * @property \Cake\ORM\Association\HasMany $Previsaoorcamentos
 * @property \Cake\ORM\Association\HasMany $Tiposervicos
 *
 * @method \App\Model\Entity\Formaspagamentoservico get($primaryKey, $options = [])
 * @method \App\Model\Entity\Formaspagamentoservico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Formaspagamentoservico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Formaspagamentoservico|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Formaspagamentoservico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Formaspagamentoservico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Formaspagamentoservico findOrCreate($search, callable $callback = null)
 */
class FormaspagamentoservicosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('formaspagamentoservicos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Previsaoorcamentos', [
            'foreignKey' => 'formaspagamentoservico_id'
        ]);
        $this->hasMany('Tiposervicos', [
            'foreignKey' => 'formaspagamentoservico_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        return $validator;
    }
}
