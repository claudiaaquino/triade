<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Previsaoorcamentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Tiposervicos
 * @property \Cake\ORM\Association\BelongsTo $Apuracaoformas
 * @property \Cake\ORM\Association\BelongsTo $Atuacaoramos
 * @property \Cake\ORM\Association\BelongsTo $Formaspagamentoservicos
 * @property \Cake\ORM\Association\BelongsTo $Telas
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Previsaoorcamento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Previsaoorcamento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Previsaoorcamento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Previsaoorcamento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Previsaoorcamento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Previsaoorcamento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Previsaoorcamento findOrCreate($search, callable $callback = null)
 */
class PrevisaoorcamentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('previsaoorcamentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Tiposervicos', [
            'foreignKey' => 'tiposervico_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Apuracaoformas', [
            'foreignKey' => 'apuracaoforma_id'
        ]);
        $this->belongsTo('Atuacaoramos', [
            'foreignKey' => 'atuacaoramo_id'
        ]);
        $this->belongsTo('Formaspagamentoservicos', [
            'foreignKey' => 'formaspagamentoservico_id'
        ]);
        $this->belongsTo('Telas', [
            'foreignKey' => 'tela_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('min_funcionarios')
            ->allowEmpty('min_funcionarios');

        $validator
            ->integer('max_funcionarios')
            ->allowEmpty('max_funcionarios');

        $validator
            ->allowEmpty('min_faturamento_mensal');
//            ->numeric('min_faturamento_mensal')

        $validator
            ->allowEmpty('max_faturamento_mensal');
//            ->numeric('max_faturamento_mensal')

        $validator
            ->requirePresence('valor_servico', 'create')
            ->notEmpty('valor_servico');
//            ->numeric('valor_servico')

        $validator
            ->allowEmpty('valor_adicionalmensal');
//            ->numeric('valor_adicionalmensal')

        $validator
            ->boolean('gratis_todosusuarios')
            ->allowEmpty('gratis_todosusuarios');

        $validator
            ->boolean('gratis_todoscliente')
            ->allowEmpty('gratis_todoscliente');

        $validator
            ->boolean('gratis_clientemodulo')
            ->allowEmpty('gratis_clientemodulo');

        $validator
            ->integer('qtdegratis')
            ->allowEmpty('qtdegratis');

        $validator
            ->boolean('qtdeilimitadagratis')
            ->allowEmpty('qtdeilimitadagratis');

        $validator
            ->integer('tempo_execucao')
            ->requirePresence('tempo_execucao', 'create')
            ->notEmpty('tempo_execucao');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tiposervico_id'], 'Tiposervicos'));
        $rules->add($rules->existsIn(['apuracaoforma_id'], 'Apuracaoformas'));
        $rules->add($rules->existsIn(['atuacaoramo_id'], 'Atuacaoramos'));
        $rules->add($rules->existsIn(['formaspagamentoservico_id'], 'Formaspagamentoservicos'));
        $rules->add($rules->existsIn(['tela_id'], 'Telas'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
