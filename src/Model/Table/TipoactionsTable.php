<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tipoactions Model
 *
 * @property \Cake\ORM\Association\HasMany $Telaquestionarios
 *
 * @method \App\Model\Entity\Tipoaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tipoaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tipoaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tipoaction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tipoaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tipoaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tipoaction findOrCreate($search, callable $callback = null)
 */
class TipoactionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tipoactions');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Telaquestionarios', [
            'foreignKey' => 'tipoaction_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->allowEmpty('action');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }
}
