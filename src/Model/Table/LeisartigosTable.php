<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Leisartigos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Leisnormas
 * @property \Cake\ORM\Association\BelongsTo $Leispartes
 * @property \Cake\ORM\Association\BelongsTo $Leislivros
 * @property \Cake\ORM\Association\BelongsTo $Leistitulos
 * @property \Cake\ORM\Association\BelongsTo $Leiscapitulos
 * @property \Cake\ORM\Association\BelongsTo $Leissections
 * @property \Cake\ORM\Association\BelongsTo $Leissubsections
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Leisincisos
 * @property \Cake\ORM\Association\HasMany $Leisletras
 * @property \Cake\ORM\Association\HasMany $Leisparagrafos
 *
 * @method \App\Model\Entity\Leisartigo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Leisartigo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Leisartigo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Leisartigo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Leisartigo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Leisartigo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Leisartigo findOrCreate($search, callable $callback = null)
 */
class LeisartigosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('leisartigos');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Leisnormas', [
            'foreignKey' => 'leisnorma_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Leispartes', [
            'foreignKey' => 'leisparte_id'
        ]);
        $this->belongsTo('Leislivros', [
            'foreignKey' => 'leislivro_id'
        ]);
        $this->belongsTo('Leistitulos', [
            'foreignKey' => 'leistitulo_id'
        ]);
        $this->belongsTo('Leiscapitulos', [
            'foreignKey' => 'leiscapitulo_id'
        ]);
        $this->belongsTo('Leissections', [
            'foreignKey' => 'leissection_id'
        ]);
        $this->belongsTo('Leissubsections', [
            'foreignKey' => 'leissubsection_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Leisincisos', [
            'foreignKey' => 'leisartigo_id'
        ]);
        $this->hasMany('Leisletras', [
            'foreignKey' => 'leisartigo_id'
        ]);
        $this->hasMany('Leisparagrafos', [
            'foreignKey' => 'leisartigo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('numero');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->boolean('exibe_cliente')
            ->allowEmpty('exibe_cliente');

        $validator
            ->boolean('exibe_funcionario')
            ->allowEmpty('exibe_funcionario');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->integer('ordem')
            ->allowEmpty('ordem');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['leisnorma_id'], 'Leisnormas'));
        $rules->add($rules->existsIn(['leisparte_id'], 'Leispartes'));
        $rules->add($rules->existsIn(['leislivro_id'], 'Leislivros'));
        $rules->add($rules->existsIn(['leistitulo_id'], 'Leistitulos'));
        $rules->add($rules->existsIn(['leiscapitulo_id'], 'Leiscapitulos'));
        $rules->add($rules->existsIn(['leissection_id'], 'Leissections'));
        $rules->add($rules->existsIn(['leissubsection_id'], 'Leissubsections'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
