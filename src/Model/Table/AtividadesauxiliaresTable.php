<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Atividadesauxiliares Model
 * 
 * @property \Cake\ORM\Association\HasMany $Empresaatividades
 *
 * @method \App\Model\Entity\Atividadesauxiliare get($primaryKey, $options = [])
 * @method \App\Model\Entity\Atividadesauxiliare newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Atividadesauxiliare[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Atividadesauxiliare|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Atividadesauxiliare patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Atividadesauxiliare[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Atividadesauxiliare findOrCreate($search, callable $callback = null)
 */
class AtividadesauxiliaresTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('atividadesauxiliares');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Empresaatividades', [
            'foreignKey' => 'atividadesauxiliar_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('descricao');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->boolean('status')
                ->allowEmpty('status');

        return $validator;
    }

}
