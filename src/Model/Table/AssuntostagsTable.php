<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Assuntostags Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Assuntos
 * @property \Cake\ORM\Association\BelongsTo $Tags
 *
 * @method \App\Model\Entity\Assuntostag get($primaryKey, $options = [])
 * @method \App\Model\Entity\Assuntostag newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Assuntostag[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Assuntostag|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Assuntostag patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Assuntostag[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Assuntostag findOrCreate($search, callable $callback = null)
 */
class AssuntostagsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('assuntostags');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Assuntos', [
            'foreignKey' => 'assunto_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Tags', [
            'foreignKey' => 'tag_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['assunto_id'], 'Assuntos'));
        $rules->add($rules->existsIn(['tag_id'], 'Tags'));

        return $rules;
    }
}
