<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Menus Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Areaservicos
 * @property \Cake\ORM\Association\HasMany $Menusubmenus
 * @property \Cake\ORM\Association\HasMany $Modulosmenus
 *
 * @method \App\Model\Entity\Menu get($primaryKey, $options = [])
 * @method \App\Model\Entity\Menu newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Menu[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Menu|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Menu patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Menu[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Menu findOrCreate($search, callable $callback = null)
 */
class MenusTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('menus');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->belongsTo('Areaservicos', [
            'foreignKey' => 'areaservico_id'
        ]);
        $this->hasMany('Menusubmenus', [
            'foreignKey' => 'menu_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->hasMany('Modulosmenus', [
            'foreignKey' => 'menu_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    public function findMenusOfUsers(\Cake\ORM\Query $query, array $options) {

        $query->find('all')->distinct()
                ->innerJoinWith('Modulosmenus.Modulos.Usermodulos')
                ->contain(['Menusubmenus.Submenus' =>
                    ['strategy' => 'select', 'queryBuilder' => function ($q) {
                            return $q->where(['Submenus.status = 1'])->order(['Submenus.nome']);
                        }]])
                        ->where(['Usermodulos.user_id' => $options['user_id']])->order(['Menus.ordem']);

                return $query;
            }


            /**
             * Default validation rules.
             *
             * @param \Cake\Validation\Validator $validator Validator instance.
             * @return \Cake\Validation\Validator
             */
            public function validationDefault(Validator $validator) {
                $validator
                        ->integer('id')
                        ->allowEmpty('id', 'create');

                $validator
                        ->requirePresence('descricao', 'create')
                        ->notEmpty('descricao');

                $validator
                        ->allowEmpty('iconname');

                $validator
                        ->integer('ordem')
                        ->allowEmpty('ordem');

                $validator
                        ->boolean('status')
                        ->allowEmpty('status');

                $validator
                        ->dateTime('dt_cadastro')
                        ->allowEmpty('dt_cadastro');

                $validator
                        ->dateTime('last_update')
                        ->allowEmpty('last_update');

                return $validator;
            }

            /**
             * Returns a rules checker object that will be used for validating
             * application integrity.
             *
             * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
             * @return \Cake\ORM\RulesChecker
             */
            public function buildRules(RulesChecker $rules) {
                $rules->add($rules->existsIn(['areaservico_id'], 'Areaservicos'));

                return $rules;
            }

        }
        