<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sistemasexternos Model
 *
 * @property \Cake\ORM\Association\HasMany $Acessosexternos
 *
 * @method \App\Model\Entity\Sistemasexterno get($primaryKey, $options = [])
 * @method \App\Model\Entity\Sistemasexterno newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Sistemasexterno[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Sistemasexterno|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sistemasexterno patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Sistemasexterno[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Sistemasexterno findOrCreate($search, callable $callback = null)
 */
class SistemasexternosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sistemasexternos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Acessosexternos', [
            'foreignKey' => 'sistemasexterno_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }
}
