<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Assuntosdocumentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Assuntos
 * @property \Cake\ORM\Association\BelongsTo $Documentos
 *
 * @method \App\Model\Entity\Assuntosdocumento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Assuntosdocumento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Assuntosdocumento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Assuntosdocumento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Assuntosdocumento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Assuntosdocumento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Assuntosdocumento findOrCreate($search, callable $callback = null)
 */
class AssuntosdocumentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('assuntosdocumentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Assuntos', [
            'foreignKey' => 'assunto_id'
        ]);
        $this->belongsTo('Documentos', [
            'foreignKey' => 'documento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['assunto_id'], 'Assuntos'));
        $rules->add($rules->existsIn(['documento_id'], 'Documentos'));

        return $rules;
    }
}
