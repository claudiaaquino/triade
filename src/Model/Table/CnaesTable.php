<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cnaes Model
 *
 * @property \Cake\ORM\Association\HasMany $Empresacnaes
 *
 * @method \App\Model\Entity\Cnae get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cnae newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cnae[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cnae|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cnae patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cnae[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cnae findOrCreate($search, callable $callback = null)
 */
class CnaesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('cnaes');
        $this->displayField('classe');
        $this->primaryKey('id');

//        $this->hasMany('Empresas', [
//            'foreignKey' => 'cnae_id'
//        ]);
        $this->hasMany('Empresacnaes', [
            'foreignKey' => 'cnaeclasse_id',
            'joinType' => 'INNER'
        ]);
    }

    public function findList(\Cake\ORM\Query $query, array $options) {

        $query->find('all');
        $concat = $query->func()->concat([
            'Cnaes.classe' => 'identifier',
            ' - ',
            'Cnaes.denominacao' => 'identifier'
        ]);
        $query->select(['id','classe' => $concat])->where(function ($exp, $q) {
                    return $exp->isNotNull('Cnaes.classe');
                })->andWhere(["Cnaes.status" => 1])
                ->order('Cnaes.classe');

        return $query;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('secao', 'create')
                ->notEmpty('secao');

        $validator
                ->allowEmpty('divisao');

        $validator
                ->allowEmpty('grupo');

        $validator
                ->allowEmpty('classe');

        $validator
                ->requirePresence('denominacao', 'create')
                ->notEmpty('denominacao');


        return $validator;
    }

}
