<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Funcionariodependentes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Funcionarios
 * @property \Cake\ORM\Association\BelongsTo $Parentescos
 *
 * @method \App\Model\Entity\Funcionariodependente get($primaryKey, $options = [])
 * @method \App\Model\Entity\Funcionariodependente newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Funcionariodependente[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Funcionariodependente|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Funcionariodependente patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Funcionariodependente[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Funcionariodependente findOrCreate($search, callable $callback = null)
 */
class FuncionariodependentesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('funcionariodependentes');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Funcionarios', [
            'foreignKey' => 'funcionario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Parentescos', [
            'foreignKey' => 'parentesco_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->date('dt_nascimento')
            ->requirePresence('dt_nascimento', 'create')
            ->notEmpty('dt_nascimento');

        $validator
            ->requirePresence('cpf', 'create')
            ->notEmpty('cpf');

        $validator
            ->requirePresence('rg', 'create')
            ->notEmpty('rg');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->allowEmpty('last_updated_fields');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['funcionario_id'], 'Funcionarios'));
        $rules->add($rules->existsIn(['parentesco_id'], 'Parentescos'));

        return $rules;
    }
}
