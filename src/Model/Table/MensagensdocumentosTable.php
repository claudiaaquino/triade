<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Mensagensdocumentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Mensagens
 * @property \Cake\ORM\Association\BelongsTo $Documentos
 *
 * @method \App\Model\Entity\Mensagensdocumento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Mensagensdocumento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Mensagensdocumento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Mensagensdocumento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mensagensdocumento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Mensagensdocumento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Mensagensdocumento findOrCreate($search, callable $callback = null)
 */
class MensagensdocumentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('mensagensdocumentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Mensagens', [
            'foreignKey' => 'mensagen_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Documentos', [
            'foreignKey' => 'documento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('dt_download')
            ->allowEmpty('dt_download');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['mensagen_id'], 'Mensagens'));
        $rules->add($rules->existsIn(['documento_id'], 'Documentos'));

        return $rules;
    }
}
