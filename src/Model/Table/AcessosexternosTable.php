<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Acessosexternos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\BelongsTo $Sistemasexternos
 *
 * @method \App\Model\Entity\Acessosexterno get($primaryKey, $options = [])
 * @method \App\Model\Entity\Acessosexterno newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Acessosexterno[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Acessosexterno|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Acessosexterno patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Acessosexterno[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Acessosexterno findOrCreate($search, callable $callback = null)
 */
class AcessosexternosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('acessosexternos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Empresas', [
            'foreignKey' => 'empresa_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Sistemasexternos', [
            'foreignKey' => 'sistemasexterno_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('cpf');

        $validator
            ->allowEmpty('cnpj');

        $validator
            ->allowEmpty('insc_estadual');

        $validator
            ->allowEmpty('insc_municipal');

        $validator
            ->allowEmpty('cod_acesso');

        $validator
            ->allowEmpty('usuario');

        $validator
            ->allowEmpty('senha');

        $validator
            ->allowEmpty('frasesecreta');

        $validator
            ->allowEmpty('protocolo');

        $validator
            ->allowEmpty('link');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->boolean('exibecliente')
            ->allowEmpty('exibecliente');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['empresa_id'], 'Empresas'));
        $rules->add($rules->existsIn(['sistemasexterno_id'], 'Sistemasexternos'));

        return $rules;
    }
}
