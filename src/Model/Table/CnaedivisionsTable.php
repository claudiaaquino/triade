<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cnaedivisions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cnaesections
 * @property \Cake\ORM\Association\HasMany $Cnaegroups
 *
 * @method \App\Model\Entity\Cnaedivision get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cnaedivision newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cnaedivision[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cnaedivision|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cnaedivision patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cnaedivision[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cnaedivision findOrCreate($search, callable $callback = null)
 */
class CnaedivisionsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('cnaedivisions');
        $this->displayField('divisao');
        $this->primaryKey('id');

        $this->belongsTo('Cnaesections', [
            'foreignKey' => 'cnaesection_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Cnaegroups', [
            'foreignKey' => 'cnaedivision_id'
        ]);
    }

    public function findAjaxList(\Cake\ORM\Query $query, array $options) {

        $query->find('all');
        $concat = $query->func()->concat([
            'Cnaedivisions.divisao' => 'identifier',
            ' - ',
            'Cnaedivisions.denominacao' => 'identifier'
        ]);
        $query->select(['id', 'divisao' => $concat])->where(["Cnaedivisions.status" => 1])
                ->order('Cnaedivisions.divisao');

        return $query;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('divisao');

        $validator
                ->requirePresence('denominacao', 'create')
                ->notEmpty('denominacao');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->integer('status')
                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['cnaesection_id'], 'Cnaesections'));

        return $rules;
    }

}
