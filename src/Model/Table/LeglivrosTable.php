<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Leglivros Model
 *
 * @property \Cake\ORM\Association\HasMany $Leislivros
 *
 * @method \App\Model\Entity\Leglivro get($primaryKey, $options = [])
 * @method \App\Model\Entity\Leglivro newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Leglivro[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Leglivro|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Leglivro patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Leglivro[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Leglivro findOrCreate($search, callable $callback = null)
 */
class LeglivrosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('leglivros');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Leislivros', [
            'foreignKey' => 'leglivro_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        return $validator;
    }
}
