<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tipoinformativos Model
 *
 * @property \Cake\ORM\Association\HasMany $Assuntosinformativos
 *
 * @method \App\Model\Entity\Tipoinformativo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tipoinformativo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tipoinformativo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tipoinformativo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tipoinformativo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tipoinformativo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tipoinformativo findOrCreate($search, callable $callback = null)
 */
class TipoinformativosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tipoinformativos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Assuntosinformativos', [
            'foreignKey' => 'tipoinformativo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        return $validator;
    }
}
