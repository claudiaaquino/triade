<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tipodocumentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Grupodocumentos
 * @property \Cake\ORM\Association\HasMany $Documentos
 * //@property \Cake\ORM\Association\HasMany $Subtipodocumentos
 *
 * @method \App\Model\Entity\Tipodocumento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tipodocumento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tipodocumento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tipodocumento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tipodocumento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tipodocumento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tipodocumento findOrCreate($search, callable $callback = null)
 */
class TipodocumentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tipodocumentos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->belongsTo('Grupodocumentos', [
            'foreignKey' => 'grupodocumento_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Documentos', [
            'foreignKey' => 'tipodocumento_id'
        ]);
//        $this->hasMany('Subtipodocumentos', [
//            'foreignKey' => 'tipodocumento_id'
//        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['grupodocumento_id'], 'Grupodocumentos'));

        return $rules;
    }
}
