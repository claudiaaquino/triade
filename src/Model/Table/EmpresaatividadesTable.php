<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Empresaatividades Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\BelongsTo $Atividadesauxiliares
 *
 * @method \App\Model\Entity\Empresaatividade get($primaryKey, $options = [])
 * @method \App\Model\Entity\Empresaatividade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Empresaatividade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Empresaatividade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Empresaatividade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Empresaatividade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Empresaatividade findOrCreate($search, callable $callback = null)
 */
class EmpresaatividadesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('empresaatividades');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Empresas', [
            'foreignKey' => 'empresa_id'
        ]);
        $this->belongsTo('Atividadesauxiliares', [
            'foreignKey' => 'atividadesauxiliar_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->date('dt_cadastro')
                ->allowEmpty('dt_cadastro');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->boolean('status')
                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['empresa_id'], 'Empresas'));
        $rules->add($rules->existsIn(['atividadesauxiliar_id'], 'Atividadesauxiliares'));

        return $rules;
    }

}
