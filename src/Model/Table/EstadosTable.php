<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Estados Model
 *
 * @property \Cake\ORM\Association\HasMany $Cidades
 * @property \Cake\ORM\Association\HasMany $Clientes
 * @property \Cake\ORM\Association\HasMany $Contabancarias
 * @property \Cake\ORM\Association\HasMany $Empresas
 * @property \Cake\ORM\Association\HasMany $Empresasocios
 * @property \Cake\ORM\Association\HasMany $Fornecedores
 * @property \Cake\ORM\Association\HasMany $Funcionarios
 * @property \Cake\ORM\Association\HasMany $Telaquestionarios
 *
 * @method \App\Model\Entity\Estado get($primaryKey, $options = [])
 * @method \App\Model\Entity\Estado newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Estado[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Estado|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Estado patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Estado[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Estado findOrCreate($search, callable $callback = null)
 */
class EstadosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('estados');
        $this->displayField('estado_sigla');
        $this->primaryKey('id');

        $this->hasMany('Cidades', [
            'foreignKey' => 'estado_id'
        ]);
        $this->hasMany('Clientes', [
            'foreignKey' => 'estado_id'
        ]);
        $this->hasMany('Contabancarias', [
            'foreignKey' => 'estado_id'
        ]);
        $this->hasMany('Empresas', [
            'foreignKey' => 'estado_id'
        ]);
        $this->hasMany('Empresasocios', [
            'foreignKey' => 'estado_id'
        ]);
        $this->hasMany('Fornecedores', [
            'foreignKey' => 'estado_id'
        ]);
        $this->hasMany('Funcionarios', [
            'foreignKey' => 'estado_id'
        ]);
        $this->hasMany('Telaquestionarios', [
            'foreignKey' => 'estado_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('estado_sigla')
            ->add('estado_sigla', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('estado_nome');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->dateTime('ultdata')
            ->allowEmpty('ultdata');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['estado_sigla']));

        return $rules;
    }
}
