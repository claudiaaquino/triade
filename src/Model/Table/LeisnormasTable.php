<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Leisnormas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Entidades
 * @property \Cake\ORM\Association\BelongsTo $Orgaos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Leisartigos
 * @property \Cake\ORM\Association\HasMany $Leisassuntos
 * @property \Cake\ORM\Association\HasMany $Leiscapitulos
 * @property \Cake\ORM\Association\HasMany $Leisdocumentos
 * @property \Cake\ORM\Association\HasMany $Leisincisos
 * @property \Cake\ORM\Association\HasMany $Leisletras
 * @property \Cake\ORM\Association\HasMany $Leislivros
 * @property \Cake\ORM\Association\HasMany $Leisparagrafos
 * @property \Cake\ORM\Association\HasMany $Leispartes
 * @property \Cake\ORM\Association\HasMany $Leissections
 * @property \Cake\ORM\Association\HasMany $Leissubsections
 * @property \Cake\ORM\Association\HasMany $Leistitulos
 * @property \Cake\ORM\Association\HasMany $Telaquestionarios
 *
 * @method \App\Model\Entity\Leisnorma get($primaryKey, $options = [])
 * @method \App\Model\Entity\Leisnorma newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Leisnorma[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Leisnorma|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Leisnorma patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Leisnorma[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Leisnorma findOrCreate($search, callable $callback = null)
 */
class LeisnormasTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('leisnormas');
        $this->displayField('numero');
        $this->primaryKey('id');

        $this->belongsTo('Entidades', [
            'foreignKey' => 'entidade_id'
        ]);
        $this->belongsTo('Orgaos', [
            'foreignKey' => 'orgao_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Leisartigos', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->hasMany('Leisassuntos', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->hasMany('Leiscapitulos', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->hasMany('Leisdocumentos', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->hasMany('Leisincisos', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->hasMany('Leisletras', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->hasMany('Leislivros', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->hasMany('Leisparagrafos', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->hasMany('Leispartes', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->hasMany('Leissections', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->hasMany('Leissubsections', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->hasMany('Leistitulos', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->hasMany('Telaquestionarios', [
            'foreignKey' => 'leisnorma_id'
        ]);
    }

    public function findAjaxList(\Cake\ORM\Query $query, array $options) {

        $query->find('all');
        $concat = $query->func()->concat([
            'Leisnormas.tipolei_descricao' => 'identifier',
            ' Nº  ',
            'Leisnormas.numero' => 'identifier',
            ', de ',
            'DATE_FORMAT(Leisnormas.dt_publicacao, "%d/%m/%Y")' => 'identifier'
        ]);
        $query->select(['id', 'lei' => $concat])->where(["Leisnormas.status" => 1])
                ->orderAsc('Leisnormas.tipolei_descricao')->orderAsc('Leisnormas.numero')->orderAsc('Leisnormas.dt_publicacao');

        return $query;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('numero');

        $validator
                ->integer('ano')
                ->allowEmpty('ano');

        $validator
                ->allowEmpty('descricao');

        $validator
                ->allowEmpty('tipolei_descricao');

        $validator
                ->allowEmpty('texto_html');

        $validator
                ->allowEmpty('texto_completo');

        $validator
                ->allowEmpty('ementa');

        $validator
                ->allowEmpty('dt_publicacao_string');

//        $validator
//            ->date('dt_publicacao')
//            ->allowEmpty('dt_publicacao');

        $validator
                ->integer('ordem')
                ->allowEmpty('ordem');

        $validator
                ->allowEmpty('url');

        $validator
                ->allowEmpty('fonte');

        $validator
                ->dateTime('dt_cadastro')
                ->allowEmpty('dt_cadastro');

//        $validator
//            ->dateTime('last_update')
//            ->allowEmpty('last_update');
//
//        $validator
//            ->boolean('status')
//            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['entidade_id'], 'Entidades'));
        $rules->add($rules->existsIn(['orgao_id'], 'Orgaos'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

}
