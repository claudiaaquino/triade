<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Legsubsections Model
 *
 * @property \Cake\ORM\Association\HasMany $Leissubsections
 *
 * @method \App\Model\Entity\Legsubsection get($primaryKey, $options = [])
 * @method \App\Model\Entity\Legsubsection newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Legsubsection[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Legsubsection|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Legsubsection patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Legsubsection[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Legsubsection findOrCreate($search, callable $callback = null)
 */
class LegsubsectionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('legsubsections');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Leissubsections', [
            'foreignKey' => 'legsubsection_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        return $validator;
    }
}
