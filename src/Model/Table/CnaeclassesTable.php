<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cnaeclasses Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cnaegroups
 * @property \Cake\ORM\Association\HasMany $Empresacnaes
 *
 * @method \App\Model\Entity\Cnaeclass get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cnaeclass newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cnaeclass[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cnaeclass|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cnaeclass patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cnaeclass[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cnaeclass findOrCreate($search, callable $callback = null)
 */
class CnaeclassesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('cnaeclasses');
        $this->displayField('denominacao');
        $this->primaryKey('id');

        $this->belongsTo('Cnaegroups', [
            'foreignKey' => 'cnaegroup_id'
        ]);
        $this->hasMany('Empresacnaes', [
            'foreignKey' => 'cnaeclasse_id'
        ]);
    }

    public function findAjaxList(\Cake\ORM\Query $query, array $options) {

        $query->find('all');
        $concat = $query->func()->concat([
            'Cnaeclasses.classe' => 'identifier',
            ' - ',
            'Cnaeclasses.denominacao' => 'identifier'
        ]);
        $query->select(['id', 'classe' => $concat])->where(["Cnaeclasses.status" => 1])
                ->order('Cnaeclasses.classe');

        return $query;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('classe');

        $validator
                ->requirePresence('denominacao', 'create')
                ->notEmpty('denominacao');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->integer('status')
                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['cnaegroup_id'], 'Cnaegroups'));

        return $rules;
    }

}
