<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Servicos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Areaservicos
 * @property \Cake\ORM\Association\BelongsTo $Tiposervicos
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Empresasocios
 * @property \Cake\ORM\Association\BelongsTo $Funcionarios
 * @property \Cake\ORM\Association\HasMany $Contratos
 * @property \Cake\ORM\Association\HasMany $Servicodocumentos
 * @property \Cake\ORM\Association\HasOne $Tarefas
 *
 * @method \App\Model\Entity\Servico get($primaryKey, $options = [])
 * @method \App\Model\Entity\Servico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Servico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Servico|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Servico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Servico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Servico findOrCreate($search, callable $callback = null)
 */
class ServicosTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('servicos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Areaservicos', [
            'foreignKey' => 'areaservico_id'
        ]);
        $this->belongsTo('Tiposervicos', [
            'foreignKey' => 'tiposervico_id'
        ]);
        $this->belongsTo('Empresas', [
            'foreignKey' => 'empresa_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Empresasocios', [
            'foreignKey' => 'empresasocio_id'
        ]);
        $this->belongsTo('Funcionarios', [
            'foreignKey' => 'funcionario_id'
        ]);
        $this->hasMany('Contratos', [
            'foreignKey' => 'servico_id'
        ]);
        $this->hasMany('Servicodocumentos', [
            'foreignKey' => 'servico_id'
        ]);
        $this->hasOne('Tarefas', [
            'foreignKey' => 'servico_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');
        $validator
                ->integer('tiposervico_id')
                ->notEmpty('tiposervico_id');

        $validator
                ->allowEmpty('descricao');

//        $validator
//            ->numeric('valor_servico')
//            ->allowEmpty('valor_servico');
//
//        $validator
//            ->numeric('valor_adicionalmensal')
//            ->allowEmpty('valor_adicionalmensal');

        $validator
                ->integer('prazo_entrega')
                ->allowEmpty('prazo_entrega');

        $validator
                ->dateTime('dt_cadastro')
                ->allowEmpty('dt_cadastro');
        $validator
                ->dateTime('dt_finalizado')
                ->allowEmpty('dt_finalizado');

        $validator
                ->boolean('aceita_termos')
                ->notEmpty('aceita_termos');

        $validator
                ->boolean('aceita_valor')
                ->notEmpty('aceita_valor');


        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['areaservico_id'], 'Areaservicos'));
        $rules->add($rules->existsIn(['tiposervico_id'], 'Tiposervicos'));
        $rules->add($rules->existsIn(['empresa_id'], 'Empresas'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['empresasocio_id'], 'Empresasocios'));
        $rules->add($rules->existsIn(['funcionario_id'], 'Funcionarios'));

        return $rules;
    }

}
