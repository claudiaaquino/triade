<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Anotacaoassuntos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Assuntos
 * @property \Cake\ORM\Association\BelongsTo $Tiponotes
 *
 * @method \App\Model\Entity\Anotacaoassunto get($primaryKey, $options = [])
 * @method \App\Model\Entity\Anotacaoassunto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Anotacaoassunto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Anotacaoassunto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Anotacaoassunto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Anotacaoassunto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Anotacaoassunto findOrCreate($search, callable $callback = null)
 */
class AnotacaoassuntosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('anotacaoassuntos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Assuntos', [
            'foreignKey' => 'assunto_id'
        ]);
        $this->belongsTo('Tiponotes', [
            'foreignKey' => 'tiponote_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('anotacao');

        $validator
            ->boolean('exibe_cliente')
            ->allowEmpty('exibe_cliente');

        $validator
            ->boolean('exibe_funcionario')
            ->allowEmpty('exibe_funcionario');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['assunto_id'], 'Assuntos'));
        $rules->add($rules->existsIn(['tiponote_id'], 'Tiponotes'));

        return $rules;
    }
}
