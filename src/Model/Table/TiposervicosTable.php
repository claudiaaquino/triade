<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tiposervicos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Gruposervicos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Formaspagamentoservicos
 * @property \Cake\ORM\Association\BelongsTo $Telas
 * @property \Cake\ORM\Association\HasMany $Contratos
 * @property \Cake\ORM\Association\HasMany $Previsaoorcamentos
 * @property \Cake\ORM\Association\HasMany $Tarefas
 * @property \Cake\ORM\Association\HasMany $Telaquestionarios
 * @property \Cake\ORM\Association\HasMany $Userservicos
 * @property \Cake\ORM\Association\HasMany $Empresaservicos
 * @property \Cake\ORM\Association\HasMany $Servicos
 *
 * @method \App\Model\Entity\Tiposervico get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tiposervico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tiposervico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tiposervico|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tiposervico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tiposervico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tiposervico findOrCreate($search, callable $callback = null)
 */
class TiposervicosTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('tiposervicos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->belongsTo('Gruposervicos', [
            'foreignKey' => 'gruposervico_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Contratos', [
            'foreignKey' => 'tiposervico_id'
        ]);
        $this->hasMany('Previsaoorcamentos', [
            'foreignKey' => 'tiposervico_id'
        ]);
        $this->hasMany('Tarefas', [
            'foreignKey' => 'tiposervico_id'
        ]);
        $this->hasMany('Telaquestionarios', [
            'foreignKey' => 'tiposervico_id'
        ]);
        $this->hasMany('Userservicos', [
            'foreignKey' => 'tiposervico_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('Empresaservicos', [
            'foreignKey' => 'tiposervico_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->belongsTo('Formaspagamentoservicos', [
            'foreignKey' => 'formaspagamentoservico_id'
        ]);
        $this->belongsTo('Telas', [
            'foreignKey' => 'tela_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('nome', 'create')
                ->notEmpty('nome');

        $validator
                ->allowEmpty('descricao');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->dateTime('dt_cadastro')
                ->allowEmpty('dt_cadastro');

        $validator
                ->integer('dia_max_execucao')
                ->allowEmpty('dia_max_execucao');

        $validator
                ->integer('dia_inicio_execucao')
                ->allowEmpty('dia_inicio_execucao');


        $validator
                ->integer('dia_fim_execucao')
                ->allowEmpty('dia_fim_execucao');

        $validator
                ->integer('prazo_feedback')
                ->allowEmpty('prazo_feedback');

        $validator
                ->boolean('solicitavel')
                ->allowEmpty('solicitavel');


        $validator
                ->boolean('interno')
                ->allowEmpty('interno');


        $validator
                ->boolean('status')
                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['gruposervico_id'], 'Gruposervicos'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

}
