<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Gruposervicos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Areaservicos
 * @property \Cake\ORM\Association\HasMany $Tiposervicos
 * @property \Cake\ORM\Association\HasMany $Tipodeveres
 *
 * @method \App\Model\Entity\Gruposervico get($primaryKey, $options = [])
 * @method \App\Model\Entity\Gruposervico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Gruposervico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Gruposervico|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Gruposervico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Gruposervico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Gruposervico findOrCreate($search, callable $callback = null)
 */
class GruposervicosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('gruposervicos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->belongsTo('Areaservicos', [
            'foreignKey' => 'areaservico_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Tiposervicos', [
            'foreignKey' => 'gruposervico_id'
        ]);
        $this->hasMany('Tipodeveres', [
            'foreignKey' => 'gruposervico_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['areaservico_id'], 'Areaservicos'));

        return $rules;
    }
}
