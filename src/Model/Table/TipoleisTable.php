<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tipoleis Model
 *
 * @method \App\Model\Entity\Tipolei get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tipolei newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tipolei[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tipolei|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tipolei patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tipolei[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tipolei findOrCreate($search, callable $callback = null)
 */
class TipoleisTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tipoleis');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->allowEmpty('sigla');

        return $validator;
    }
}
