<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Telas Model
 *
 * @property \Cake\ORM\Association\HasMany $Previsaoorcamentos
 * @property \Cake\ORM\Association\HasMany $Telaquestionarios
 * @property \Cake\ORM\Association\HasMany $Tiposervicos
 *
 * @method \App\Model\Entity\Tela get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tela newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tela[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tela|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tela patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tela[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tela findOrCreate($search, callable $callback = null)
 */
class TelasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('telas');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Previsaoorcamentos', [
            'foreignKey' => 'tela_id'
        ]);
        $this->hasMany('Telaquestionarios', [
            'foreignKey' => 'tela_id'
        ]);
        $this->hasMany('Tiposervicos', [
            'foreignKey' => 'tela_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->boolean('flag_servico')
            ->allowEmpty('flag_servico');

        $validator
            ->allowEmpty('controller');

        $validator
            ->allowEmpty('action');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }
}
