<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Nivelescolaridades Model
 *
 * @property \Cake\ORM\Association\HasMany $Cursos
 * @property \Cake\ORM\Association\HasMany $Funcionarioescolaridades
 *
 * @method \App\Model\Entity\Nivelescolaridade get($primaryKey, $options = [])
 * @method \App\Model\Entity\Nivelescolaridade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Nivelescolaridade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Nivelescolaridade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Nivelescolaridade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Nivelescolaridade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Nivelescolaridade findOrCreate($search, callable $callback = null)
 */
class NivelescolaridadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('nivelescolaridades');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Cursos', [
            'foreignKey' => 'nivelescolaridade_id'
        ]);
        $this->hasMany('Funcionarioescolaridades', [
            'foreignKey' => 'nivelescolaridade_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        return $validator;
    }
}
