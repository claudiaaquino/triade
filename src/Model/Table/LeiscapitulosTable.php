<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Leiscapitulos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Leisnormas
 * @property \Cake\ORM\Association\BelongsTo $Legcapitulos
 * @property \Cake\ORM\Association\HasMany $Leisartigos
 * @property \Cake\ORM\Association\HasMany $Leissections
 *
 * @method \App\Model\Entity\Leiscapitulo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Leiscapitulo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Leiscapitulo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Leiscapitulo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Leiscapitulo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Leiscapitulo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Leiscapitulo findOrCreate($search, callable $callback = null)
 */
class LeiscapitulosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('leiscapitulos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Leisnormas', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->belongsTo('Legcapitulos', [
            'foreignKey' => 'legcapitulo_id'
        ]);
        $this->hasMany('Leisartigos', [
            'foreignKey' => 'leiscapitulo_id'
        ]);
        $this->hasMany('Leissections', [
            'foreignKey' => 'leiscapitulo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['leisnorma_id'], 'Leisnormas'));
        $rules->add($rules->existsIn(['legcapitulo_id'], 'Legcapitulos'));

        return $rules;
    }
}
