<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cnaegroups Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cnaedivisions
 * @property \Cake\ORM\Association\HasMany $Cnaeclasses
 *
 * @method \App\Model\Entity\Cnaegroup get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cnaegroup newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cnaegroup[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cnaegroup|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cnaegroup patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cnaegroup[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cnaegroup findOrCreate($search, callable $callback = null)
 */
class CnaegroupsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('cnaegroups');
        $this->displayField('denominacao');
        $this->primaryKey('id');

        $this->belongsTo('Cnaedivisions', [
            'foreignKey' => 'cnaedivision_id'
        ]);
        $this->hasMany('Cnaeclasses', [
            'foreignKey' => 'cnaegroup_id'
        ]);
    }

    public function findAjaxList(\Cake\ORM\Query $query, array $options) {

        $query->find('all');
        $concat = $query->func()->concat([
            'Cnaegroups.grupo' => 'identifier',
            ' - ',
            'Cnaegroups.denominacao' => 'identifier'
        ]);
        $query->select(['id', 'grupo' => $concat])->where(["Cnaegroups.status" => 1])
                ->order('Cnaegroups.grupo');

        return $query;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('grupo');

        $validator
                ->requirePresence('denominacao', 'create')
                ->notEmpty('denominacao');

        $validator
                ->dateTime('last_update')
                ->allowEmpty('last_update');

        $validator
                ->integer('status')
                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['cnaedivision_id'], 'Cnaedivisions'));

        return $rules;
    }

}
