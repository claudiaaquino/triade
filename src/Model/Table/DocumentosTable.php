<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Documentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Tipodocumentos
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\BelongsTo $Funcionarios
 * @property \Cake\ORM\Association\BelongsTo $Empresasocios
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Mensagensdocumentos
 * @property \Cake\ORM\Association\HasMany $Movimentacaobancarias
 * @property \Cake\ORM\Association\HasMany $Tarefadocumentos
 *
 * @method \App\Model\Entity\Documento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Documento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Documento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Documento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Documento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Documento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Documento findOrCreate($search, callable $callback = null)
 */
class DocumentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('documentos');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Tipodocumentos', [
            'foreignKey' => 'tipodocumento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Empresas', [
            'foreignKey' => 'empresa_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Funcionarios', [
            'foreignKey' => 'funcionario_id'
        ]);
        $this->belongsTo('Empresasocios', [
            'foreignKey' => 'empresasocio_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_enviou'
        ]);
        $this->hasMany('Mensagensdocumentos', [
            'foreignKey' => 'documento_id'
        ]);
        $this->hasMany('Movimentacaobancarias', [
            'foreignKey' => 'documento_id'
        ]);
        $this->hasMany('Tarefadocumentos', [
            'foreignKey' => 'documento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->boolean('flag_lido')
            ->allowEmpty('flag_lido');

        $validator
            ->dateTime('dt_lido')
            ->allowEmpty('dt_lido');

        $validator
            ->dateTime('dt_enviado')
            ->allowEmpty('dt_enviado');

        $validator
            ->allowEmpty('nome');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->allowEmpty('path');

        $validator
            ->requirePresence('file', 'create')
            ->notEmpty('file');

        $validator
            ->allowEmpty('filesize');

        $validator
            ->integer('user_destinatario')
            ->allowEmpty('user_destinatario');

        $validator
            ->integer('user_enviou')
            ->allowEmpty('user_enviou');

        $validator
            ->integer('user_leu')
            ->allowEmpty('user_leu');

        $validator
            ->allowEmpty('last_updated_fields');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

//        $validator
//            ->boolean('notificar_empresa')
//            ->allowEmpty('notificar_empresa');
        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tipodocumento_id'], 'Tipodocumentos'));
        $rules->add($rules->existsIn(['empresa_id'], 'Empresas'));
        $rules->add($rules->existsIn(['funcionario_id'], 'Funcionarios'));

        return $rules;
    }
}
