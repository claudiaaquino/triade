<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Leisassuntos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Leisnormas
 * @property \Cake\ORM\Association\BelongsTo $Assuntos
 *
 * @method \App\Model\Entity\Leisassunto get($primaryKey, $options = [])
 * @method \App\Model\Entity\Leisassunto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Leisassunto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Leisassunto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Leisassunto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Leisassunto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Leisassunto findOrCreate($search, callable $callback = null)
 */
class LeisassuntosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('leisassuntos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Leisnormas', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->belongsTo('Assuntos', [
            'foreignKey' => 'assunto_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['leisnorma_id'], 'Leisnormas'));
        $rules->add($rules->existsIn(['assunto_id'], 'Assuntos'));

        return $rules;
    }
}
