<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Atuacaoramos Model
 *
 * @property \Cake\ORM\Association\HasMany $Empresaramos
 * @property \Cake\ORM\Association\HasMany $Empresas
 * @property \Cake\ORM\Association\HasMany $Previsaoorcamentos
 *
 * @method \App\Model\Entity\Atuacaoramo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Atuacaoramo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Atuacaoramo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Atuacaoramo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Atuacaoramo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Atuacaoramo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Atuacaoramo findOrCreate($search, callable $callback = null)
 */
class AtuacaoramosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('atuacaoramos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('Empresaramos', [
            'foreignKey' => 'atuacaoramo_id'
        ]);
        $this->hasMany('Empresas', [
            'foreignKey' => 'atuacaoramo_id'
        ]);
        $this->hasMany('Previsaoorcamentos', [
            'foreignKey' => 'atuacaoramo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }
}
