<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Legpartes Model
 *
 * @property \Cake\ORM\Association\HasMany $Leispartes
 *
 * @method \App\Model\Entity\Legparte get($primaryKey, $options = [])
 * @method \App\Model\Entity\Legparte newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Legparte[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Legparte|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Legparte patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Legparte[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Legparte findOrCreate($search, callable $callback = null)
 */
class LegpartesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('legpartes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Leispartes', [
            'foreignKey' => 'legparte_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        return $validator;
    }
}
