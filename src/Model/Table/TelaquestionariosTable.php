<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Telaquestionarios Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Telaquestionarios
 * @property \Cake\ORM\Association\BelongsTo $Assuntos
 * @property \Cake\ORM\Association\BelongsTo $Leisnormas
 * @property \Cake\ORM\Association\BelongsTo $Tiposervicos
 * @property \Cake\ORM\Association\BelongsTo $Documentos
 * @property \Cake\ORM\Association\BelongsTo $Telas
 * @property \Cake\ORM\Association\BelongsTo $Tipoactions
 * @property \Cake\ORM\Association\BelongsTo $Estados
 * @property \Cake\ORM\Association\BelongsTo $Cidades
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Telaquestionarios
 *
 * @method \App\Model\Entity\Telaquestionario get($primaryKey, $options = [])
 * @method \App\Model\Entity\Telaquestionario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Telaquestionario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Telaquestionario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Telaquestionario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Telaquestionario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Telaquestionario findOrCreate($search, callable $callback = null)
 */
class TelaquestionariosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('telaquestionarios');
        $this->displayField('pergunta');
        $this->primaryKey('id');

        $this->belongsTo('Telaquestionarios', [
            'foreignKey' => 'telaquestionario_id'
        ]);
        $this->belongsTo('Assuntos', [
            'foreignKey' => 'assunto_id'
        ]);
        $this->belongsTo('Leisnormas', [
            'foreignKey' => 'leisnorma_id'
        ]);
        $this->belongsTo('Tiposervicos', [
            'foreignKey' => 'tiposervico_id'
        ]);
        $this->belongsTo('Documentos', [
            'foreignKey' => 'documento_id'
        ]);
        $this->belongsTo('Telas', [
            'foreignKey' => 'tela_id'
        ]);
        $this->belongsTo('Tipoactions', [
            'foreignKey' => 'tipoaction_id'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id'
        ]);
        $this->belongsTo('Cidades', [
            'foreignKey' => 'cidade_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Telaquestionarios', [
            'foreignKey' => 'telaquestionario_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('pergunta');

        $validator
            ->allowEmpty('resposta');

        $validator
            ->boolean('exibe_cliente')
            ->allowEmpty('exibe_cliente');

        $validator
            ->boolean('exibe_funcionario')
            ->allowEmpty('exibe_funcionario');

        $validator
            ->boolean('exigeconfirmacao')
            ->allowEmpty('exigeconfirmacao');

        $validator
            ->date('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['telaquestionario_id'], 'Telaquestionarios'));
        $rules->add($rules->existsIn(['assunto_id'], 'Assuntos'));
        $rules->add($rules->existsIn(['leisnorma_id'], 'Leisnormas'));
        $rules->add($rules->existsIn(['tiposervico_id'], 'Tiposervicos'));
        $rules->add($rules->existsIn(['documento_id'], 'Documentos'));
        $rules->add($rules->existsIn(['tela_id'], 'Telas'));
        $rules->add($rules->existsIn(['tipoaction_id'], 'Tipoactions'));
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['cidade_id'], 'Cidades'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
