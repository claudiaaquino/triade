<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Funcionarioescolaridades Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Funcionarios
 * @property \Cake\ORM\Association\BelongsTo $Nivelescolaridades
 * @property \Cake\ORM\Association\BelongsTo $Cursos
 * @property \Cake\ORM\Association\BelongsTo $Escolas
 * @property \Cake\ORM\Association\BelongsTo $Escolacampuses
 * @property \Cake\ORM\Association\BelongsTo $Turnos
 *
 * @method \App\Model\Entity\Funcionarioescolaridade get($primaryKey, $options = [])
 * @method \App\Model\Entity\Funcionarioescolaridade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Funcionarioescolaridade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Funcionarioescolaridade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Funcionarioescolaridade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Funcionarioescolaridade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Funcionarioescolaridade findOrCreate($search, callable $callback = null)
 */
class FuncionarioescolaridadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('funcionarioescolaridades');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Funcionarios', [
            'foreignKey' => 'funcionario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Nivelescolaridades', [
            'foreignKey' => 'nivelescolaridade_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cursos', [
            'foreignKey' => 'curso_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Escolas', [
            'foreignKey' => 'escola_id'
        ]);
        $this->belongsTo('Escolacampuses', [
            'foreignKey' => 'escolacampus_id'
        ]);
        $this->belongsTo('Turnos', [
            'foreignKey' => 'turno_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('matricula');

        $validator
            ->integer('periodo')
            ->allowEmpty('periodo');

        $validator
            ->date('formatura')
            ->requirePresence('formatura', 'create')
            ->notEmpty('formatura');

        $validator
            ->date('inicio')
            ->requirePresence('inicio', 'create')
            ->notEmpty('inicio');

        $validator
            ->dateTime('dt_cadastro')
            ->allowEmpty('dt_cadastro');

        $validator
            ->allowEmpty('last_updated_fields');

        $validator
            ->dateTime('last_update')
            ->allowEmpty('last_update');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['funcionario_id'], 'Funcionarios'));
        $rules->add($rules->existsIn(['nivelescolaridade_id'], 'Nivelescolaridades'));
        $rules->add($rules->existsIn(['curso_id'], 'Cursos'));
        $rules->add($rules->existsIn(['escola_id'], 'Escolas'));
        $rules->add($rules->existsIn(['escolacampus_id'], 'Escolacampuses'));
        $rules->add($rules->existsIn(['turno_id'], 'Turnos'));

        return $rules;
    }
}
