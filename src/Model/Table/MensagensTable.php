<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Mensagens Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Mensagens
 * @property \Cake\ORM\Association\BelongsTo $Empresas
 * @property \Cake\ORM\Association\BelongsTo $Areaservicos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Mensagensdocumentos
 * @property \Cake\ORM\Association\HasMany $Mensagens
 * @property \Cake\ORM\Association\HasMany $Mensagensdestinatarios
 *
 * @method \App\Model\Entity\Mensagen get($primaryKey, $options = [])
 * @method \App\Model\Entity\Mensagen newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Mensagen[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Mensagen|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mensagen patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Mensagen[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Mensagen findOrCreate($search, callable $callback = null)
 */
class MensagensTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('mensagens');
        $this->displayField('assunto');
        $this->primaryKey('id');

        $this->belongsTo('Mensagens', [
            'foreignKey' => 'mensagen_id'
        ]);
        $this->belongsTo('Empresas', [
            'foreignKey' => 'empresa_id'
        ]);
        $this->belongsTo('Areaservicos', [
            'foreignKey' => 'areaservico_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Mensagensdocumentos', [
            'foreignKey' => 'mensagen_id'
        ]);
        $this->hasMany('Respostas', [
            'className' => 'Mensagens',
            'foreignKey' => 'mensagen_id',
            'propertyName' => 'respostas'
        ]);

        $this->hasMany('Mensagensdestinatarios', [
            'foreignKey' => 'mensagen_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('assunto', 'create')
                ->notEmpty('assunto');

        $validator
                ->requirePresence('texto', 'create')
                ->notEmpty('texto');

        $validator
                ->boolean('todos_empresa')
                ->allowEmpty('todos_empresa');
        $validator
                ->boolean('todos_sistema')
                ->allowEmpty('todos_empresa');

        $validator
                ->dateTime('dt_envio')
                ->allowEmpty('dt_envio');



        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['mensagen_id'], 'Mensagens'));
        $rules->add($rules->existsIn(['empresa_id'], 'Empresas'));
        $rules->add($rules->existsIn(['areaservico_id'], 'Areaservicos'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

}
