<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Documento Entity
 *
 * @property int $id
 * @property int $tipodocumento_id
 * @property int $empresa_id
 * @property int $funcionario_id
 * @property int $empresasocio_id
 * @property bool $flag_lido
 * @property \Cake\I18n\Time $dt_lido
 * @property \Cake\I18n\Time $dt_enviado
 * @property string $nome
 * @property string $descricao
 * @property string $path
 * @property string $file
 * @property string $filesize
 * @property int $user_destinatario
 * @property int $user_enviou
 * @property int $user_leu
 * @property int $notificar_empresa
 * @property string $last_updated_fields
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Tipodocumento $tipodocumento
 * @property \App\Model\Entity\Empresa $empresa
 * @property \App\Model\Entity\Funcionario $funcionario
 * @property \App\Model\Entity\Empresasocio $empresasocio
 * @property \App\Model\Entity\Mensagensdocumento[] $mensagensdocumentos
 * @property \App\Model\Entity\Movimentacaobancaria[] $movimentacaobancarias
 * @property \App\Model\Entity\Tarefadocumento[] $tarefadocumentos
 */
class Documento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
