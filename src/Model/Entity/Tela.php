<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tela Entity
 *
 * @property int $id
 * @property string $descricao
 * @property bool $flag_servico
 * @property string $controller
 * @property string $action
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Telaquestionario[] $telaquestionarios
 * @property \App\Model\Entity\Tiposervico[] $tiposervicos
 */
class Tela extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
