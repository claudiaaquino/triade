<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Empresa Entity
 *
 * @property int $id
 * @property string $cnpj
 * @property string $cnpj_nf
 * @property string $cpf
 * @property string $razao
 * @property string $fantasia
 * @property string $telefone
 * @property string $fax
 * @property string $email
 * @property string $email_nfe
 * @property string $endereco
 * @property string $end_numero
 * @property string $end_complemento
 * @property string $end_bairro
 * @property string $end_cep
 * @property int $estado_id
 * @property int $cidade_id
 * @property string $nome_responsavel
 * @property string $cargo_responsavel
 * @property int $num_funcionarios
 * @property int $num_socios
 * @property int $solicitacao
 * @property int $solicitacao_finalizada
 * @property int $num_step
 * @property \Cake\I18n\Time $dt_solicitacao 
 * @property int $user_id
 * @property string $nome1
 * @property string $nome2
 * @property string $nome3
 * @property string $indicecadastral
 * @property string $insc_municipal
 * @property string $insc_estadual
 * @property string $nire
 * @property string $dt_nire
 * @property float $capitalsocial
 * @property float $faturamento_mensal
 * @property float $faturamento_anual
 * @property int $porte_id
 * @property int $sociedade
 * @property int $residencia_socio
 * @property int $arealocal
 * @property int $apuracaoforma_id
 * @property int $atuacaoramo_id
 * @property string $last_fields_updated
 * @property \Cake\I18n\Time $last_update
 * @property int $status
 *
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\Cidade $cidade
 * @property \App\Model\Entity\Porte $porte
 * @property \App\Model\Entity\Apuracaoforma $apuracaoforma
 * @property \App\Model\Entity\Atuacaoramo $atuacaoramo
 * @property \App\Model\Entity\Empresacnae[] $empresacnaes
 * @property \App\Model\Entity\Empresaatividade[] $empresaatividades
 * @property \App\Model\Entity\Empresasocio[] $empresasocios
 * @property \App\Model\Entity\Documento[] $documentos
 * @property \App\Model\Entity\Empresausuario[] $empresausuarios
 * @property \App\Model\Entity\Funcionario[] $funcionarios
 * @property \App\Model\Entity\Acessosexterno[] $acessosexternos
 * @property \App\Model\Entity\Areaservico[] $areaservicos
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\Legislativoempresa[] $legislativoempresas
 * @property \App\Model\Entity\Empresadevere[] $empresadeveres
 * @property \App\Model\Entity\Empresaservico[] $empresaservicos
 */
class Empresa extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

}
