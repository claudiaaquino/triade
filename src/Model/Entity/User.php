<?php

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property int $tipousuario_id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $nome
 * @property string $foto
 * @property \Cake\I18n\Time $last_login
 * @property \Cake\I18n\Time $last_update
 * @property int $status
 * @property int $admin_empresa
 * @property int $notificacao_mensagem
 * @property int $notificacao_tarefa
 * @property int $notificacao_semanal_tarefa
 * @property int $notificacao_prazo_tarefa
 * @property int notificacao_dever
 * @property int $notificacao_prazo_dever
 * 
 * @property \App\Model\Entity\Tipousuario $tipousuario
 * @property \App\Model\Entity\Empresausuario $empresausuario
 * @property \App\Model\Entity\Empresa $empresa
 * @property \App\Model\Entity\Funcionario $funcionario
 * @property \App\Model\Entity\Documento $documento
 * @property \App\Model\Entity\Usermodulo $usermodulos
 * @property \App\Model\Entity\Mensagen $mensagensdestinatarios
 * @property \App\Model\Entity\Mensagen $mensagens
 * @property \App\Model\Entity\Usersareaservico[] $usersareaservicos
 * @property \App\Model\Entity\Userservico[] $userservicos
 * @property \App\Model\Entity\Tarefausuario[] $tarefausuarios
 */
class User extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($password) {
        return (new DefaultPasswordHasher)->hash($password);
    }

}
