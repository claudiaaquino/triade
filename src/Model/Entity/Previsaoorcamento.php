<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Previsaoorcamento Entity
 *
 * @property int $id
 * @property int $tiposervico_id
 * @property int $apuracaoforma_id
 * @property int $atuacaoramo_id
 * @property int $min_funcionarios
 * @property int $max_funcionarios
 * @property float $min_faturamento_mensal
 * @property float $max_faturamento_mensal
 * @property int $formaspagamentoservico_id
 * @property float $valor_servico
 * @property float $valor_adicionalmensal
 * @property bool $gratis_todosusuarios
 * @property bool $gratis_todoscliente
 * @property bool $gratis_clientemodulo
 * @property int $qtdegratis
 * @property bool $qtdeilimitadagratis
 * @property int $tela_id
 * @property int $tempo_execucao
 * @property int $user_id
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Tiposervico $tiposervico
 * @property \App\Model\Entity\Apuracaoforma $apuracaoforma
 * @property \App\Model\Entity\Atuacaoramo $atuacaoramo
 * @property \App\Model\Entity\Formaspagamentoservico $formaspagamentoservico
 * @property \App\Model\Entity\Tela $tela
 * @property \App\Model\Entity\User $user
 */
class Previsaoorcamento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
