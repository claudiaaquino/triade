<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tarefatipo Entity
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $user_id
 * @property bool $todos_sistema
 * @property bool todos_empresa
 * @property string $descricao
 * @property \Cake\I18n\Time $dt_cadastro
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Tarefa[] $tarefas
 */
class Tarefatipo extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

}
