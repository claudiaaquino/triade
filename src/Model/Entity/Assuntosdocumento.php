<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Assuntosdocumento Entity
 *
 * @property int $id
 * @property int $assunto_id
 * @property int $documento_id
 * @property \Cake\I18n\Time $dt_cadastro
 *
 * @property \App\Model\Entity\Assunto $assunto
 * @property \App\Model\Entity\Documento $documento
 */
class Assuntosdocumento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
