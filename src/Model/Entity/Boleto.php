<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Boleto Entity
 *
 * @property int $id
 * @property int $contrato_id
 * @property int $contabancaria_id
 * @property string $filename
 * @property string $valor_boleto
 * @property \Cake\I18n\Time $dt_vencimento
 * @property \Cake\I18n\Time $dt_processamento
 * @property float $valor_multa
 * @property float $valor_multa_dia
 * @property \Cake\I18n\Time $dt_pagamento
 * @property bool $pago
 * @property bool $status
 *
 * @property \App\Model\Entity\Contrato $contrato
 * @property \App\Model\Entity\Contabancaria $contabancaria
 */
class Boleto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
