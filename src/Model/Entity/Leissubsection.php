<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Leissubsection Entity
 *
 * @property int $id
 * @property int $leisnorma_id
 * @property int $leissection_id
 * @property int $legsubsection_id
 * @property string $descricao
 * @property string $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Leisnorma $leisnorma
 * @property \App\Model\Entity\Leissection $leissection
 * @property \App\Model\Entity\Legsubsection $legsubsection
 * @property \App\Model\Entity\Leisartigo[] $leisartigos
 */
class Leissubsection extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
