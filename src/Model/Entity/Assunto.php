<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Assunto Entity
 *
 * @property int $id
 * @property int $assunto_id
 * @property string $nome
 * @property string $descricao
 * @property bool $exibe_cliente
 * @property bool $exibe_funcionario
 * @property int $user_id
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Assunto[] $assuntos
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Anotacaoassunto[] $anotacaoassuntos
 * @property \App\Model\Entity\Assuntosdocumento[] $assuntosdocumentos
 * @property \App\Model\Entity\Assuntosinformativo[] $assuntosinformativos
 * @property \App\Model\Entity\Leisassunto[] $leisassuntos
 * @property \App\Model\Entity\Telaquestionario[] $telaquestionarios
 * @property \App\Model\Entity\Assuntostag[] $assuntostags
 */
class Assunto extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

}
