<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cnaeclass Entity
 *
 * @property int $id
 * @property string $cnaegroup_id
 * @property string $classe
 * @property string $denominacao
 * @property int $simplesnacional
 * @property \Cake\I18n\Time $last_update
 * @property int $status
 *
 * @property \App\Model\Entity\Empresacnae[] $empresacnaes
 * @property \App\Model\Entity\Cnaegroup $cnaegroup
 */
class Cnaeclass extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
