<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cnae Entity
 *
 * @property int $id
 * @property string $secao
 * @property string $divisao
 * @property string $grupo
 * @property string $classe
 * @property string $denominacao
 * @property \Cake\I18n\Time $last_updated_fields
 * @property int $status
 *
 * @property \App\Model\Entity\Empresa[] $empresas
 */
class Cnae extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
