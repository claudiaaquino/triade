<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Leisletra Entity
 *
 * @property int $id
 * @property int $leisnorma_id
 * @property int $leisartigo_id
 * @property int $leisparagrafo_id
 * @property string $nome
 * @property string $descricao
 * @property bool $exibe_cliente
 * @property bool $exibe_funcionario
 * @property int $ordem
 * @property int $user_id
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Leisnorma $leisnorma
 * @property \App\Model\Entity\Leisartigo $leisartigo
 * @property \App\Model\Entity\User $user
 */
class Leisletra extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
