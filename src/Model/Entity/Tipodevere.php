<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tipodevere Entity
 *
 * @property int $id
 * @property int $areaservico_id
 * @property int $gruposervico_id
 * @property string $nome
 * @property string $descricao
 * @property int $user_id
 * @property int $dia_lembrete_execucao
 * @property bool $dia_lembrete_execucao_uteis
 * @property int $dia_inicio_execucao
 * @property bool $dia_inicio_execucao_uteis
 * @property int $dia_fim_execucao
 * @property int $dia_fim_execucao_uteis
 * @property int $prazo_feedback
 * @property bool $tarefa_automatica
 * @property int $dia_tarefa_automatica
 * @property bool $dia_tarefa_automatica_uteis
 * @property string $cor_tarefa
 * @property \Cake\I18n\Time $last_update
 * @property \Cake\I18n\Time $dt_cadastro
 * @property bool $status
 *
 * @property \App\Model\Entity\Gruposervico $gruposervico
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Empresadevere[] $empresadeveres
 * @property \App\Model\Entity\Tarefa[] $tarefas
 */
class Tipodevere extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
