<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cidade Entity
 *
 * @property int $id
 * @property int $estado_id
 * @property string $cidade_nome
 * @property string $cidade_longitude
 * @property string $cidade_latitude
 * @property string $cidade_classe
 * @property int $status
 * @property \Cake\I18n\Time $ultdata
 *
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\Empresa[] $empresas
 */
class Cidade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
