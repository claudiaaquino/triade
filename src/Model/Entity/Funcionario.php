<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Funcionario Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $empresa_id
 * @property string $num_contrato
 * @property int $cargo_id
 * @property string $nome
 * @property string $dt_nascimento
 * @property string $cpf
 * @property string $email
 * @property string $endereco
 * @property string $end_numero
 * @property string $end_complemento
 * @property string $end_bairro
 * @property string $end_cep
 * @property int $estado_id
 * @property int $cidade_id
 * @property string $telefone_residencial
 * @property string $telefone_celular
 * @property int $estadocivil_id
 * @property int $sexo_id
 * @property string $cpf_responsavel
 * @property string $telefone_responsavel
 * @property string $raca
 * @property string $pis
 * @property string $numreg_livro
 * @property string $ctps
 * @property string $ctps_serie
 * @property string $cnh
 * @property string $cnh_dt_habilitacao
 * @property string $cnh_dt_vencimento
 * @property string $rg
 * @property string $rg_estado
 * @property string $rg_expedidor
 * @property string $rg_dt_expedicao
 * @property string $militar_numero
 * @property string $militar_expedidor
 * @property string $militar_serie
 * @property string $eleitor_numero
 * @property string $eleitor_zona
 * @property string $eleitor_secao
 * @property string $eleitor_dt_emissao
 * @property string $pai_nome
 * @property string $mae_nome
 * @property float $salario
 * @property float $aux_transporte
 * @property float $aux_alimentacao
 * @property int $flag_deficiente
 * @property string $desc_deficiencia
 * @property \Cake\I18n\Time $dt_cadastro_sistema
 * @property string $dt_contrato_efetivado
 * @property string $dt_demissao
 * @property string $demissao_motivo
 * @property string $last_updated_fields
 * @property \Cake\I18n\Time $last_update
 * @property int $status
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Empresa $empresa
 * @property \App\Model\Entity\Cargo $cargo
 * @property \App\Model\Entity\Estadocivil $estadocivil
 * @property \App\Model\Entity\Sexo $sexo
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\Cidade $cidade
 * @property \App\Model\Entity\Documento[] $documentos
 * @property \App\Model\Entity\Funcionariodependente[] $funcionariodependentes
 * @property \App\Model\Entity\Funcionarioescolaridade[] $funcionarioescolaridades
 */
class Funcionario extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
