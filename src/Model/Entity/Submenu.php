<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Submenu Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 * @property string $controller
 * @property string $action
 * @property bool $status
 * @property \Cake\I18n\Time $last_update
 *
 * @property \App\Model\Entity\Menusubmenu[] $menusubmenus
 */
class Submenu extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
