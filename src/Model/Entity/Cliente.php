<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cliente Entity
 *
 * @property int $id
 * @property int $empresa_id
 * @property string $cnpj
 * @property string $cpf
 * @property string $nome
 * @property string $telefone
 * @property string $email
 * @property string $endereco
 * @property string $end_numero
 * @property string $end_complemento
 * @property string $end_bairro
 * @property string $end_cep
 * @property int $estado_id
 * @property int $cidade_id
 * @property string $nome_responsavel
 * @property string $cargo_responsavel
 * @property \Cake\I18n\Time $dt_cadastro
 * @property int $user_id
 * @property string $last_fields_updated
 * @property \Cake\I18n\Time $last_update
 * @property int $status
 *
 * @property \App\Model\Entity\Empresa $empresa
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\Cidade $cidade
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Contabancaria[] $contabancarias
 * @property \App\Model\Entity\Movimentacaobancaria[] $movimentacaobancarias
 */
class Cliente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
