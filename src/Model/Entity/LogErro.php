<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LogErro Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $empresa_id
 * @property string $documento_print
 * @property string $url
 * @property string $descricao_erro
 * @property string $msg_retorno
 * @property string $status
 * @property \Cake\I18n\Time $dt_cadastro
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Empresa $empresa
 */
class LogErro extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
