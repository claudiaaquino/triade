<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Servico Entity
 *
 * @property int $id
 * @property int $areaservico_id
 * @property int $tiposervico_id
 * @property int $empresa_id
 * @property int $user_id
 * @property int $empresasocio_id
 * @property int $funcionario_id
 * @property string $descricao
 * @property float $valor_servico
 * @property float $valor_adicionalmensal
 * @property int $prazo_entrega
 * @property int $aceita_termos
 * @property int $aceita_valor
 * @property int $status_servico
 * @property int $user_executou
 * @property \Cake\I18n\Time $dt_finalizado
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property int $status
 *
 * @property \App\Model\Entity\Areaservico $areaservico
 * @property \App\Model\Entity\Tiposervico $tiposervico
 * @property \App\Model\Entity\Empresa $empresa
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Tarefa $tarefa
 * @property \App\Model\Entity\Empresasocio $empresasocio
 * @property \App\Model\Entity\Funcionario $funcionario
 * @property \App\Model\Entity\Tarefa $tarefa
 * @property \App\Model\Entity\Contrato[] $contratos
 * @property \App\Model\Entity\Servicodocumento[] $servicodocumentos
 */
class Servico extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
