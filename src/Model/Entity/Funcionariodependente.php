<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Funcionariodependente Entity
 *
 * @property int $id
 * @property int $funcionario_id
 * @property int $parentesco_id
 * @property string $nome
 * @property \Cake\I18n\Time $dt_nascimento
 * @property string $cpf
 * @property string $rg
 * @property \Cake\I18n\Time $dt_cadastro
 * @property string $last_updated_fields
 * @property \Cake\I18n\Time $last_update
 * @property int $status
 *
 * @property \App\Model\Entity\Funcionario $funcionario
 * @property \App\Model\Entity\Parentesco $parentesco
 */
class Funcionariodependente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
