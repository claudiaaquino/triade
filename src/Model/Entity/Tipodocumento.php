<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tipodocumento Entity
 *
 * @property int $id
 * @property int $grupodocumento_id
 * @property string $descricao
 * @property int $exige_competencia
 * @property int $exige_exercicio
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Documento[] $documentos
 */
class Tipodocumento extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

}
