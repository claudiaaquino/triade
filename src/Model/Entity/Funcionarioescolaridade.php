<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Funcionarioescolaridade Entity
 *
 * @property int $id
 * @property int $funcionario_id
 * @property int $nivelescolaridade_id
 * @property int $curso_id
 * @property int $escola_id
 * @property int $escolacampus_id
 * @property string $matricula
 * @property int $periodo
 * @property int $turno_id
 * @property \Cake\I18n\Time $formatura
 * @property \Cake\I18n\Time $inicio
 * @property \Cake\I18n\Time $dt_cadastro
 * @property string $last_updated_fields
 * @property \Cake\I18n\Time $last_update
 * @property int $status
 *
 * @property \App\Model\Entity\Funcionario $funcionario
 * @property \App\Model\Entity\Nivelescolaridade $nivelescolaridade
 * @property \App\Model\Entity\Curso $curso
 * @property \App\Model\Entity\Escola $escola
 * @property \App\Model\Entity\Escolacampus $escolacampus
 * @property \App\Model\Entity\Turno $turno
 */
class Funcionarioescolaridade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
