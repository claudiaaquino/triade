<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Movimentacoesfutura Entity
 *
 * @property int $id
 * @property int $movimentacaobancaria_id
 * @property string $dt_programada
 * @property float $valorparcela
 * @property bool $pago
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Movimentacaobancaria $movimentacaobancaria
 */
class Movimentacoesfutura extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
