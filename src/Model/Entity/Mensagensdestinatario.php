<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mensagensdestinatario Entity
 *
 * @property int $id
 * @property int $mensagen_id
 * @property int $user_id
 * @property \Cake\I18n\Time $dt_resposta
 * @property \Cake\I18n\Time $dt_leitura
 * @property bool $status
 *
 * @property \App\Model\Entity\Mensagen $mensagen
 * @property \App\Model\Entity\User $user
 */
class Mensagensdestinatario extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
