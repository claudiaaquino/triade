<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Escolacampus Entity
 *
 * @property int $id
 * @property int $escola_id
 * @property string $nome
 * @property string $endereco
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property int $status
 *
 * @property \App\Model\Entity\Escola $escola
 * @property \App\Model\Entity\Funcionarioescolaridade[] $funcionarioescolaridades
 */
class Escolacampus extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
