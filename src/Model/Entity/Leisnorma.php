<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Leisnorma Entity
 *
 * @property int $id
 * @property string $numero
 * @property int $ano
 * @property string $descricao
 * @property int $entidade_id
 * @property int $orgao_id
 * @property string $tipolei_descricao
 * @property string $texto_html
 * @property string $texto_completo
 * @property string $ementa
 * @property string $dt_publicacao_string
 * @property \Cake\I18n\Time $dt_publicacao
 * @property int $ordem
 * @property string $url
 * @property string $fonte
 * @property int $user_id
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Leisnorma[] $leisnormas
 * @property \App\Model\Entity\Entidade $entidade
 * @property \App\Model\Entity\Orgao $orgao
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Leisartigo[] $leisartigos
 * @property \App\Model\Entity\Leisassunto[] $leisassuntos
 * @property \App\Model\Entity\Leiscapitulo[] $leiscapitulos
 * @property \App\Model\Entity\Leisdocumento[] $leisdocumentos
 * @property \App\Model\Entity\Leisinciso[] $leisincisos
 * @property \App\Model\Entity\Leisletra[] $leisletras
 * @property \App\Model\Entity\Leislivro[] $leislivros
 * @property \App\Model\Entity\Leisparagrafo[] $leisparagrafos
 * @property \App\Model\Entity\Leisparte[] $leispartes
 * @property \App\Model\Entity\Leissection[] $leissections
 * @property \App\Model\Entity\Leissubsection[] $leissubsections
 * @property \App\Model\Entity\Leistitulo[] $leistitulos
 * @property \App\Model\Entity\Telaquestionario[] $telaquestionarios
 */
class Leisnorma extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
