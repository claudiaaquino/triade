<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Telaquestionario Entity
 *
 * @property int $id
 * @property int $telaquestionario_id
 * @property int $assunto_id
 * @property int $leisnorma_id
 * @property int $tiposervico_id
 * @property int $documento_id
 * @property int $tela_id
 * @property int $tipoaction_id
 * @property string $pergunta
 * @property string $resposta
 * @property bool $exibe_cliente
 * @property bool $exibe_funcionario
 * @property bool $exigeconfirmacao
 * @property int $estado_id
 * @property int $cidade_id
 * @property int $user_id
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Telaquestionario[] $telaquestionarios
 * @property \App\Model\Entity\Assunto $assunto
 * @property \App\Model\Entity\Leisnorma $leisnorma
 * @property \App\Model\Entity\Tiposervico $tiposervico
 * @property \App\Model\Entity\Documento $documento
 * @property \App\Model\Entity\Tela $tela
 * @property \App\Model\Entity\Tipoaction $tipoaction
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\Cidade $cidade
 * @property \App\Model\Entity\User $user
 */
class Telaquestionario extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
