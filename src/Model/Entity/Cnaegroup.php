<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cnaegroup Entity
 *
 * @property int $id
 * @property string $cnaedivision_id
 * @property string $grupo
 * @property string $denominacao
 * @property \Cake\I18n\Time $last_update
 * @property int $status
 *
 * @property \App\Model\Entity\Cnaedivision $cnaedivision
 * @property \App\Model\Entity\Cnaeclass[] $cnaeclasses
 */
class Cnaegroup extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
