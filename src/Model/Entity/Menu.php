<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Menu Entity
 *
 * @property int $id
 * @property int $areaservico_id
 * @property string $descricao
 * @property string $iconname
 * @property int $ordem
 * @property bool $status
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 *
 * @property \App\Model\Entity\Areaservico $areaservico
 * @property \App\Model\Entity\Menusubmenu[] $menusubmenus
 * @property \App\Model\Entity\Modulosmenu[] $modulosmenus
 */
class Menu extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
