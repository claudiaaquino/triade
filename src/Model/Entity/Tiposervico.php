<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tiposervico Entity
 *
 * @property int $id
 * @property int $gruposervico_id
 * @property string $nome
 * @property string $descricao
 * @property int $user_id
 * @property \Cake\I18n\Time $last_update
 * @property \Cake\I18n\Time $dt_cadastro
 * @property int $dia_max_execucao
 * @property int $dia_inicio_execucao
 * @property int $dia_fim_execucao
 * @property int $prazo_feedback
 * @property bool $solicitavel
 * @property bool $interno
 * @property bool $status
 * @property bool $anual
 * @property bool $mensal
 * @property string $meses
 * @property bool $notificacao_concluido_cliente
 * @property bool $notificar_dt_vencimento
 * @property string $lembrete_vencimento
 * @property string $instrucoes_execucao
 * @property int $formaspagamentoservico_id
 * @property float $valor_servico
 * @property float $valor_adicionalmensal
 * @property bool $gratis_todosusuarios
 * @property bool $gratis_todoscliente
 * @property bool $gratis_clientemodulo
 * @property int $qtdegratis
 * @property bool $qtdeilimitadagratis
 * @property int $tela_id
 * @property int $tempo_execucao
 *
 * 
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Gruposervico $gruposervico
 * @property \App\Model\Entity\Formaspagamentoservico $formaspagamentoservico
 * @property \App\Model\Entity\Tela $tela
 * @property \App\Model\Entity\Previsaoorcamento[] $previsaoorcamentos
 * @property \App\Model\Entity\Userservico[] $userservicos
 * @property \App\Model\Entity\Empresaservico[] $empresaservicos
 * @property \App\Model\Entity\Servicos[] $servicos
 */
class Tiposervico extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

}
