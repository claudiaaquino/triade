<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contrato Entity
 *
 * @property int $id
 * @property int $tiposervico_id
 * @property int $user_id
 * @property float $valor_servico
 * @property string $filecontrato
 * @property \Cake\I18n\Time $dt_contrato
 * @property bool $status
 *
 * @property \App\Model\Entity\Tiposervico $tiposervico
 * @property \App\Model\Entity\User $user
 */
class Contrato extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
