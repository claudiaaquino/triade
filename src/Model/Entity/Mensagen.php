<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mensagen Entity
 *
 * @property int $id
 * @property int $mensagen_id
 * @property int $empresa_id
 * @property int $areaservico_id
 * @property string $assunto
 * @property string $texto
 * @property bool $todos_empresa
 * @property bool $todos_sistema
 * @property \Cake\I18n\Time $dt_envio
 * @property \Cake\I18n\Time $dt_leitura
 * @property int $user_id
 *
 * 
 * @property \App\Model\Entity\Mensagen[] $mensagens
 * @property \App\Model\Entity\Empresa $empresa
 * @property \App\Model\Entity\Areaservico $areaservico
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Mensagensdestinatario[] $mensagensdestinatarios
 * @property \App\Model\Entity\Mensagensdocumento[] $mensagensdocumentos
 */
class Mensagen extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
