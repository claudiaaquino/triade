<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Acessosexterno Entity
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $sistemasexterno_id
 * @property string $cpf
 * @property string $cnpj
 * @property string $insc_estadual
 * @property string $insc_municipal
 * @property string $cod_acesso
 * @property string $usuario
 * @property string $senha
 * @property string $frasesecreta
 * @property string $pin
 * @property string $puk
 * @property string $protocolo
 * @property string $link
 * @property string $observacao
 * @property bool $exibecliente
 * @property \Cake\I18n\Time $dt_cadastro
 * @property \Cake\I18n\Time $last_update
 * @property bool $status
 *
 * @property \App\Model\Entity\Empresa $empresa
 * @property \App\Model\Entity\Sistemasexterno $sistemasexterno
 */
class Acessosexterno extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
