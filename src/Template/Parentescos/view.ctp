<div class="page-title">
    <div class="title_left">
        <h3><?= __('Parentescos') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($parentesco->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Descricao') ?></p>
                                    <p><?= h($parentesco->descricao) ?></p>
                                    </tr>
                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $this->Number->format($parentesco->id) ?></p>

                                                                                                                                                    <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <?php if (!empty($parentesco->funcionariodependentes)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Funcionariodependentes Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Funcionario Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Parentesco Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Nascimento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cpf') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Rg') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Updated Fields') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php ?>
                                <tr>


                                </tr>

                                <?php
                                $cor = 'even';
                                foreach ($parentesco->funcionariodependentes as $funcionariodependentes):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= h($funcionariodependentes->id) ?>">
                                    </td>
                                                                    <td><?= h($funcionariodependentes->id) ?></td>
                                                                    <td><?= h($funcionariodependentes->funcionario_id) ?></td>
                                                                    <td><?= h($funcionariodependentes->parentesco_id) ?></td>
                                                                    <td><?= h($funcionariodependentes->nome) ?></td>
                                                                    <td><?= h($funcionariodependentes->dt_nascimento) ?></td>
                                                                    <td><?= h($funcionariodependentes->cpf) ?></td>
                                                                    <td><?= h($funcionariodependentes->rg) ?></td>
                                                                    <td><?= h($funcionariodependentes->dt_cadastro) ?></td>
                                                                    <td><?= h($funcionariodependentes->last_updated_fields) ?></td>
                                                                    <td><?= h($funcionariodependentes->last_update) ?></td>
                                                                    <td><?= h($funcionariodependentes->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Funcionariodependentes', 'action' => 'view', $funcionariodependentes->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Funcionariodependentes', 'action' => 'edit', $funcionariodependentes->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Funcionariodependentes', 'action' => 'delete', $funcionariodependentes->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $funcionariodependentes->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
            </div>
</div>


