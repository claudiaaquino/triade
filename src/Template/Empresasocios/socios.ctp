<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">    
            <div class="x_content">
                <h2><?= $this->Html->link(__('  Cadastrar Sócio'), ['action' => 'addframe', $idempresa], ['class' => "btn btn-dark fa fa-file"]) ?></h2>
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('nome') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('cpf') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('percentual_capitalsocial', 'Capital Social (%)') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('admin') ?></th>
                               <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $cor = 'even';
                            foreach ($empresasocios as $empresasocio):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                   <td><?= h($empresasocio->nome) ?></td>
                                    <td><?= h($empresasocio->cpf) ?></td>
                                    <td><?= $this->Number->format($empresasocio->percentual_capitalsocial) ?></td>
                                    <td><?= $empresasocio->admin == 1 ? 'SIM' : 'NÃO'; ?></td>
                                    <td class="last">
                                        <?= $this->Html->link(__('Visualizar'), ['action' => 'viewframe', $empresasocio->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                        <?= $this->Html->link(__('Editar'), ['action' => 'editframe', $empresasocio->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                        <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $empresasocio->id], ['class' => "btn btn-danger btn-xs",'confirm' => __('Tem certeza que deseja deletar esse sócio?', $empresasocio->id)]) ?>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                
                </div>
            </div>
        </div>
    </div>
</div>