<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Empresasocio'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Empresas'), ['controller' => 'Empresas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Empresa'), ['controller' => 'Empresas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cargos'), ['controller' => 'Cargos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cargo'), ['controller' => 'Cargos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cidades'), ['controller' => 'Cidades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cidade'), ['controller' => 'Cidades', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Estadocivils'), ['controller' => 'Estadocivils', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Estadocivil'), ['controller' => 'Estadocivils', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Comunhaoregimes'), ['controller' => 'Comunhaoregimes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Comunhaoregime'), ['controller' => 'Comunhaoregimes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sexos'), ['controller' => 'Sexos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sexo'), ['controller' => 'Sexos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="empresasocios index large-9 medium-8 columns content">
    <h3><?= __('Empresasocios') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('empresa_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cargo_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nome') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dt_nascimento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cpf') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('endereco') ?></th>
                <th scope="col"><?= $this->Paginator->sort('end_numero') ?></th>
                <th scope="col"><?= $this->Paginator->sort('end_complemento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('end_bairro') ?></th>
                <th scope="col"><?= $this->Paginator->sort('end_cep') ?></th>
                <th scope="col"><?= $this->Paginator->sort('estado_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cidade_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('telefone_residencial') ?></th>
                <th scope="col"><?= $this->Paginator->sort('telefone_celular') ?></th>
                <th scope="col"><?= $this->Paginator->sort('estadocivil_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('comunhaoregime_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sexo_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ctps') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ctps_serie') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cnh') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cnh_dt_habilitacao') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cnh_dt_vencimento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rg') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rg_estado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rg_expedidor') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rg_dt_expedicao') ?></th>
                <th scope="col"><?= $this->Paginator->sort('militar_numero') ?></th>
                <th scope="col"><?= $this->Paginator->sort('militar_expedidor') ?></th>
                <th scope="col"><?= $this->Paginator->sort('militar_serie') ?></th>
                <th scope="col"><?= $this->Paginator->sort('eleitor_numero') ?></th>
                <th scope="col"><?= $this->Paginator->sort('eleitor_zona') ?></th>
                <th scope="col"><?= $this->Paginator->sort('eleitor_secao') ?></th>
                <th scope="col"><?= $this->Paginator->sort('eleitor_dt_emissao') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pai_nome') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mae_nome') ?></th>
                <th scope="col"><?= $this->Paginator->sort('percentual_capitalsocial') ?></th>
                <th scope="col"><?= $this->Paginator->sort('admin') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dt_cadastro') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_updated_fields') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_update') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($empresasocios as $empresasocio): ?>
            <tr>
                <td><?= $this->Number->format($empresasocio->id) ?></td>
                <td><?= $empresasocio->has('user') ? $this->Html->link($empresasocio->user->username, ['controller' => 'Users', 'action' => 'view', $empresasocio->user->id]) : '' ?></td>
                <td><?= $empresasocio->has('empresa') ? $this->Html->link($empresasocio->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $empresasocio->empresa->id]) : '' ?></td>
                <td><?= $empresasocio->has('cargo') ? $this->Html->link($empresasocio->cargo->descricao, ['controller' => 'Cargos', 'action' => 'view', $empresasocio->cargo->id]) : '' ?></td>
                <td><?= h($empresasocio->nome) ?></td>
                <td><?= h($empresasocio->dt_nascimento) ?></td>
                <td><?= h($empresasocio->cpf) ?></td>
                <td><?= h($empresasocio->email) ?></td>
                <td><?= h($empresasocio->endereco) ?></td>
                <td><?= h($empresasocio->end_numero) ?></td>
                <td><?= h($empresasocio->end_complemento) ?></td>
                <td><?= h($empresasocio->end_bairro) ?></td>
                <td><?= h($empresasocio->end_cep) ?></td>
                <td><?= $empresasocio->has('estado') ? $this->Html->link($empresasocio->estado->estado_sigla, ['controller' => 'Estados', 'action' => 'view', $empresasocio->estado->id]) : '' ?></td>
                <td><?= $empresasocio->has('cidade') ? $this->Html->link($empresasocio->cidade->cidade_nome, ['controller' => 'Cidades', 'action' => 'view', $empresasocio->cidade->id]) : '' ?></td>
                <td><?= h($empresasocio->telefone_residencial) ?></td>
                <td><?= h($empresasocio->telefone_celular) ?></td>
                <td><?= $empresasocio->has('estadocivil') ? $this->Html->link($empresasocio->estadocivil->descricao, ['controller' => 'Estadocivils', 'action' => 'view', $empresasocio->estadocivil->id]) : '' ?></td>
                <td><?= $empresasocio->has('comunhaoregime') ? $this->Html->link($empresasocio->comunhaoregime->id, ['controller' => 'Comunhaoregimes', 'action' => 'view', $empresasocio->comunhaoregime->id]) : '' ?></td>
                <td><?= $empresasocio->has('sexo') ? $this->Html->link($empresasocio->sexo->descricao, ['controller' => 'Sexos', 'action' => 'view', $empresasocio->sexo->id]) : '' ?></td>
                <td><?= h($empresasocio->ctps) ?></td>
                <td><?= h($empresasocio->ctps_serie) ?></td>
                <td><?= h($empresasocio->cnh) ?></td>
                <td><?= h($empresasocio->cnh_dt_habilitacao) ?></td>
                <td><?= h($empresasocio->cnh_dt_vencimento) ?></td>
                <td><?= h($empresasocio->rg) ?></td>
                <td><?= h($empresasocio->rg_estado) ?></td>
                <td><?= h($empresasocio->rg_expedidor) ?></td>
                <td><?= h($empresasocio->rg_dt_expedicao) ?></td>
                <td><?= h($empresasocio->militar_numero) ?></td>
                <td><?= h($empresasocio->militar_expedidor) ?></td>
                <td><?= h($empresasocio->militar_serie) ?></td>
                <td><?= h($empresasocio->eleitor_numero) ?></td>
                <td><?= h($empresasocio->eleitor_zona) ?></td>
                <td><?= h($empresasocio->eleitor_secao) ?></td>
                <td><?= h($empresasocio->eleitor_dt_emissao) ?></td>
                <td><?= h($empresasocio->pai_nome) ?></td>
                <td><?= h($empresasocio->mae_nome) ?></td>
                <td><?= $this->Number->format($empresasocio->percentual_capitalsocial) ?></td>
                <td><?= h($empresasocio->admin) ?></td>
                <td><?= h($empresasocio->dt_cadastro) ?></td>
                <td><?= h($empresasocio->last_updated_fields) ?></td>
                <td><?= h($empresasocio->last_update) ?></td>
                <td><?= $this->Number->format($empresasocio->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $empresasocio->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $empresasocio->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $empresasocio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $empresasocio->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
