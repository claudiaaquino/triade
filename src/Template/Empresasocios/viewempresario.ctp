<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Informações Pessoais: <?= h($empresasocio->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <?php if ($empresasocio->empresa->solicitacao_finalizada) { ?>
                        <li><?= $this->Html->link(__('  Editar Informações'), ['action' => 'editempresario', $empresasocio->id], ['class' => "btn btn-dark fa fa-pencil-square-o"]) ?></li>
                    <?php } else { ?>
                        <li><?= $this->Html->link(__('  Editar Informações'), ['action' => 'editempresarioframe', $empresasocio->id], ['class' => "btn btn-dark fa fa-pencil-square-o"]) ?></li>                        
                    <?php } ?>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content"  >
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h4>Informações Principais</h4>
                    <div class="ln_solid"></div>
                    <div class="project_detail">

                        <p class="title"><?= __('Nome') ?></p>
                        <p><?= $empresasocio->nome ? $empresasocio->nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Dt. Nascimento') ?></p>
                        <p><?= $empresasocio->dt_nascimento ? $empresasocio->dt_nascimento : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Cpf') ?></p>
                        <p><?= $empresasocio->cpf ? $empresasocio->cpf : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Email') ?></p>
                        <p><?= $empresasocio->email ? $empresasocio->email : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Estado Civil') ?></p>
                        <p><?= $empresasocio->estadocivil ? $empresasocio->estadocivil->descricao : 'Não Informado' ?></p>

                        <?php if ($empresasocio->has('comunhaoregime')) { ?>
                            <p class="title"><?= __('Regime de Comunhão') ?></p>
                            <p><?= $empresasocio->comunhaoregime->descricao ?></p>
                        <?php } ?>

                        <p class="title"><?= __('Sexo') ?></p>
                        <p><?= $empresasocio->has('sexo') ? $this->Html->link($empresasocio->sexo->descricao, ['controller' => 'Sexos', 'action' => 'view', $empresasocio->sexo->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Nome do Pai') ?></p>
                        <p><?= $empresasocio->pai_nome ? $empresasocio->pai_nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Nome da Mãe') ?></p>
                        <p><?= $empresasocio->mae_nome ? $empresasocio->mae_nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Cargo') ?></p>
                        <p><?= $empresasocio->has('cargo') ? $this->Html->link($empresasocio->cargo->descricao, ['controller' => 'Cargos', 'action' => 'view', $empresasocio->cargo->id]) : 'Não Informado' ?></p>

                    </div>

                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h4>Endereço e Contato</h4>
                    <div class="ln_solid"></div>
                    <div class="project_detail">
                        <p class="title"><?= __('Endereco') ?></p>
                        <p><?= $empresasocio->endereco ? $empresasocio->endereco : 'Não Informado'; ?></p>

                        <p class="title"><?= __('End Numero') ?></p>
                        <p><?= $empresasocio->end_numero ? $empresasocio->end_numero : 'Não Informado'; ?></p>

                        <p class="title"><?= __('End Complemento') ?></p>
                        <p><?= $empresasocio->end_complemento ? $empresasocio->end_complemento : 'Não Informado'; ?></p>

                        <p class="title"><?= __('End Bairro') ?></p>
                        <p><?= $empresasocio->end_bairro ? $empresasocio->end_bairro : 'Não Informado'; ?></p>

                        <p class="title"><?= __('End Cep') ?></p>
                        <p><?= $empresasocio->end_cep ? $empresasocio->end_cep : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Estado') ?></p>
                        <p><?= $empresasocio->has('estado') ? $this->Html->link($empresasocio->estado->estado_sigla, ['controller' => 'Estados', 'action' => 'view', $empresasocio->estado->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Cidade') ?></p>
                        <p><?= $empresasocio->has('cidade') ? $this->Html->link($empresasocio->cidade->cidade_nome, ['controller' => 'Cidades', 'action' => 'view', $empresasocio->cidade->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Telefone Residencial') ?></p>
                        <p><?= $empresasocio->telefone_residencial ? $empresasocio->telefone_residencial : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Telefone Celular') ?></p>
                        <p><?= $empresasocio->telefone_celular ? $empresasocio->telefone_celular : 'Não Informado'; ?></p>
                    </div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                    <h4>Documentos Específicos</h4>
                    <div class="ln_solid"></div>
                    <div class="project_detail">

                        <?php if ($empresasocio->ctps) { ?>
                            <p class="title"><?= __('CTPS/Série') ?></p>
                            <p><?= $empresasocio->ctps ? $empresasocio->ctps . '/' . $empresasocio->ctps_serie : 'Não Informado'; ?></p>
                            <div class="ln_solid"></div>
                        <?php } ?>

                        <?php if ($empresasocio->cnh) { ?>
                            <p class="title"><?= __('CNH') ?></p>
                            <p><?= $empresasocio->cnh; ?></p>

                            <p class="title"><?= __('CNH Dt. Habilitacao') ?></p>
                            <p><?= $empresasocio->cnh_dt_habilitacao ? $empresasocio->cnh_dt_habilitacao : 'Não Informado'; ?></p>

                            <p class="title"><?= __('CNH Dt. Vencimento') ?></p>
                            <p><?= $empresasocio->cnh_dt_vencimento ? $empresasocio->cnh_dt_vencimento : 'Não Informado'; ?></p>
                            <div class="ln_solid"></div>
                        <?php } ?>


                        <?php if ($empresasocio->rg) { ?>
                            <p class="title"><?= __('RG') ?></p>
                            <p><?= $empresasocio->rg; ?></p>

                            <p class="title"><?= __('RG Órgão/Estado') ?></p>
                            <p><?= $empresasocio->rg_estado || $empresasocio->rg_expedidor ? $empresasocio->rg_estado . ' /' . $empresasocio->rg_expedidor : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Rg Dt Expedicao') ?></p>
                            <p><?= $empresasocio->rg_dt_expedicao ? $empresasocio->rg_dt_expedicao : 'Não Informado'; ?></p>
                            <div class="ln_solid"></div>
                        <?php } ?>


                        <?php if ($empresasocio->militar_numero) { ?>
                            <p class="title"><?= __('Certificado Reservista/Série') ?></p>
                            <p><?= $empresasocio->militar_numero . '/' . $empresasocio->militar_serie; ?></p>

                            <p class="title"><?= __('Órgão Expedidor') ?></p>
                            <p><?= $empresasocio->militar_expedidor ? $empresasocio->militar_expedidor : 'Não Informado'; ?></p>

                            <div class="ln_solid"></div>
                        <?php } ?>


                        <?php if ($empresasocio->eleitor_numero) { ?>
                            <p class="title"><?= __('Título de Eleitor') ?></p>
                            <p><?= $empresasocio->eleitor_numero ? $empresasocio->eleitor_numero . '/' . $empresasocio->eleitor_zona . '/' . $empresasocio->eleitor_secao : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Zona/Seção') ?></p>
                            <p><?= $empresasocio->eleitor_zona . '/' . $empresasocio->eleitor_secao; ?></p>

                            <p class="title"><?= __('Dt. Emissão do Título') ?></p>
                            <p><?= $empresasocio->eleitor_dt_emissao ? $empresasocio->eleitor_dt_emissao : 'Não Informado'; ?></p>

                            <div class="ln_solid"></div>
                        <?php } ?>
                    </div>
                </div>

            </div>
        </div>
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Informações do Empresário: <?= h($empresasocio->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" style="display: none;">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="project_detail">
                        <?php if ($empresasocio->has('empresa') && $empresasocio->empresa->razao) { ?>
                            <p class="title"><?= __('Empresa') ?></p>
                            <p><?= $this->Html->link($empresasocio->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $empresasocio->empresa->id]) ?></p>
                        <?php } ?>

                        <p class="title"><?= __('Contribui com o INSS?') ?></p>
                        <p><?= $empresasocio->contribuiinss ? __('SIM') : __('NÃO'); ?></p>

                        <p class="title"><?= __('Possui Certificado Digital de Pessoa Física?') ?></p>
                        <p><?= $empresasocio->certificadodigital_pessoafisica ? __('SIM') : __('NÃO'); ?></p>

                        <?php if ($empresasocio->certificadodigital_pessoafisica) { ?>
                            <p class="title"><?= __('Certificado é do tipo Token A3') ?></p>
                            <p><?= $empresasocio->token_a3 ? __('SIM') : __('NÃO'); ?></p>
                        <?php } ?>

                        <p class="title"><?= __('Dt. Cadastro') ?></p>
                        <p><?= $empresasocio->dt_cadastro ? $empresasocio->dt_cadastro : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $empresasocio->last_update ? $empresasocio->last_update : 'Registro nunca foi modificado'; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($empresasocio->documentos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Documentos Anexados do Empresário: <?= h($empresasocio->nome) ?> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"  style="display: none;" >
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Tipo de Documento') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Envio') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($empresasocio->documentos as $documentos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= $this->Html->link($documentos->tipodocumento->descricao, "/docs/" . $documentos->file, ['target' => '_blank']) ?></td>
                                        <td><?= h($documentos->dt_enviado) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Download'), "/docs/" . $documentos->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                                <?php if ($admin) { ?>
                                                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Documentos', 'action' => 'delete', $documentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $documentos->id)]) ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>