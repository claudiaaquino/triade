<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Mensagemdocumento'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="mensagemdocumentos index large-9 medium-8 columns content">
    <h3><?= __('Mensagemdocumentos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mensagem_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_anexou_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nomearquivo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('obs') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dt_envio') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($mensagemdocumentos as $mensagemdocumento): ?>
            <tr>
                <td><?= $this->Number->format($mensagemdocumento->id) ?></td>
                <td><?= $this->Number->format($mensagemdocumento->mensagem_id) ?></td>
                <td><?= $this->Number->format($mensagemdocumento->user_anexou_id) ?></td>
                <td><?= h($mensagemdocumento->nomearquivo) ?></td>
                <td><?= h($mensagemdocumento->obs) ?></td>
                <td><?= h($mensagemdocumento->dt_envio) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $mensagemdocumento->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mensagemdocumento->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mensagemdocumento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mensagemdocumento->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
