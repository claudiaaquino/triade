<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Mensagemdocumento'), ['action' => 'edit', $mensagemdocumento->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Mensagemdocumento'), ['action' => 'delete', $mensagemdocumento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mensagemdocumento->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Mensagemdocumentos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mensagemdocumento'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="mensagemdocumentos view large-9 medium-8 columns content">
    <h3><?= h($mensagemdocumento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nomearquivo') ?></th>
            <td><?= h($mensagemdocumento->nomearquivo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Obs') ?></th>
            <td><?= h($mensagemdocumento->obs) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($mensagemdocumento->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mensagem Id') ?></th>
            <td><?= $this->Number->format($mensagemdocumento->mensagem_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Anexou Id') ?></th>
            <td><?= $this->Number->format($mensagemdocumento->user_anexou_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dt Envio') ?></th>
            <td><?= h($mensagemdocumento->dt_envio) ?></td>
        </tr>
    </table>
</div>
