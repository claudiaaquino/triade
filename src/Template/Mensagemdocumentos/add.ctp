<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Mensagemdocumentos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="mensagemdocumentos form large-9 medium-8 columns content">
    <?= $this->Form->create($mensagemdocumento) ?>
    <fieldset>
        <legend><?= __('Add Mensagemdocumento') ?></legend>
        <?php
            echo $this->Form->input('mensagem_id');
            echo $this->Form->input('user_anexou_id');
            echo $this->Form->input('nomearquivo');
            echo $this->Form->input('obs');
            echo $this->Form->input('dt_envio', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
