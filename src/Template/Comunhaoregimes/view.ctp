<div class="page-title">
    <div class="title_left">
        <h3>Detalhes do Regime de Comunhão</h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($comunhaoregime->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Status</p>
                        <p><?= $comunhaoregime->status == 1 ? 'Ativo' : 'Inativo' ?></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Ultima Atualização</p>
                        <p><?= $comunhaoregime->last_update; ?></p>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php<?php
        if (!empty($comunhaoregime->empresasocios)) {
            ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Sócios relacionados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col" class="column-title"><?= __('ID') ?></th>
                                    <th scope="col" class="column-title"><?= __('Nome') ?></th>
                                    <th scope="col" class="column-title"><?= __('CPF') ?></th>
                                    <th scope="col" class="column-title"><?= __('Empresa') ?></th>
                                    <th scope="col" class="column-title"><?= __('Cargo') ?></th>
                                    <th scope="col" class="column-title"><?= __('Email') ?></th>
                                    <th scope="col" class="column-title"><?= __('Endereço') ?></th>
                                    <th scope="col" class="column-title"><?= __('CEP') ?></th>
                                    <th scope="col" class="column-title"><?= __('Cidade') ?></th>
                                    <th scope="col" class="column-title"><?= __('Estado') ?></th>
                                    <th scope="col" class="column-title"><?= __('Telefone') ?></th>
                                    <th scope="col" class="column-title"><?= __('Última Atualização') ?></th>
                                    <th></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $cor = 'even';
                                foreach ($comunhaoregime->empresasocios as $empresasocios) {                                    
                                    ?>

                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records">
                                        </td>
                                        <td><?= h($empresasocios->id) ?></td>
                                        <td><?= h($empresasocios->nome) ?></td>
                                        <td><?= h($empresasocios->cpf) ?></td>
                                        <td><?= h($empresasocios->empresa_id) ?></td>
                                        <td><?= h($empresasocios->cargo_id) ?></td>
                                        <td><?= h($empresasocios->email) ?></td>
                                        <td><?php
                                            $complemento = !empty($empresasocios->end_complemento) ? "({$empresasocios->end_complemento})" : "";
                                            ?>
                                        <?= !empty($empresasocios->endereco) ? h($empresasocios->endereco) . ', ' . h($empresasocios->end_numero) . " {$complemento} - {$empresasocios->end_bairro}" : "" ?>
                                        </td>
                                        <td><?= h($empresasocios->end_cep) ?></td>
                                        <td><?= !empty($empresasocios->cidade->cidade_nome) ? h($empresasocios->cidade->cidade_nome) : ""; ?></td>
                                        <td><?= !empty($empresasocios->estado->estado_sigla) ? $empresasocios->estado->estado_sigla : "" ?></td>
                                        <td><?= h($empresasocios->telefone) ?></td>
                                        <td><?= h($empresasocios->last_update) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Empresasocios', 'action' => 'view', $empresasocios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Empresasocios', 'action' => 'edit', $empresasocios->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Empresasocios', 'action' => 'delete', $empresasocios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Deseja mesmo remover o registro #{0}?', $empresasocios->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                }
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<div class="comunhaoregimes view large-9 medium-8 columns content">
    <h3><?= h($comunhaoregime->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Descricao') ?></th>
            <td><?= h($comunhaoregime->descricao) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($comunhaoregime->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dt Cadastro') ?></th>
            <td><?= h($comunhaoregime->dt_cadastro) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Update') ?></th>
            <td><?= h($comunhaoregime->last_update) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $comunhaoregime->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Empresasocios') ?></h4>
        <?php if (!empty($comunhaoregime->empresasocios)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Empresa Id') ?></th>
                <th scope="col"><?= __('Cargo Id') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Dt Nascimento') ?></th>
                <th scope="col"><?= __('Cpf') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Endereco') ?></th>
                <th scope="col"><?= __('End Numero') ?></th>
                <th scope="col"><?= __('End Complemento') ?></th>
                <th scope="col"><?= __('End Bairro') ?></th>
                <th scope="col"><?= __('End Cep') ?></th>
                <th scope="col"><?= __('Estado Id') ?></th>
                <th scope="col"><?= __('Cidade Id') ?></th>
                <th scope="col"><?= __('Telefone Residencial') ?></th>
                <th scope="col"><?= __('Telefone Celular') ?></th>
                <th scope="col"><?= __('Estadocivil Id') ?></th>
                <th scope="col"><?= __('Comunhaoregime Id') ?></th>
                <th scope="col"><?= __('Sexo Id') ?></th>
                <th scope="col"><?= __('Ctps') ?></th>
                <th scope="col"><?= __('Ctps Serie') ?></th>
                <th scope="col"><?= __('Cnh') ?></th>
                <th scope="col"><?= __('Cnh Dt Habilitacao') ?></th>
                <th scope="col"><?= __('Cnh Dt Vencimento') ?></th>
                <th scope="col"><?= __('Rg') ?></th>
                <th scope="col"><?= __('Rg Estado') ?></th>
                <th scope="col"><?= __('Rg Expedidor') ?></th>
                <th scope="col"><?= __('Rg Dt Expedicao') ?></th>
                <th scope="col"><?= __('Militar Numero') ?></th>
                <th scope="col"><?= __('Militar Expedidor') ?></th>
                <th scope="col"><?= __('Militar Serie') ?></th>
                <th scope="col"><?= __('Eleitor Numero') ?></th>
                <th scope="col"><?= __('Eleitor Zona') ?></th>
                <th scope="col"><?= __('Eleitor Secao') ?></th>
                <th scope="col"><?= __('Eleitor Dt Emissao') ?></th>
                <th scope="col"><?= __('Pai Nome') ?></th>
                <th scope="col"><?= __('Mae Nome') ?></th>
                <th scope="col"><?= __('Percentual Capitalsocial') ?></th>
                <th scope="col"><?= __('Admin') ?></th>
                <th scope="col"><?= __('Dt Cadastro') ?></th>
                <th scope="col"><?= __('Last Updated Fields') ?></th>
                <th scope="col"><?= __('Last Update') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($comunhaoregime->empresasocios as $empresasocios): ?>
            <tr>
                <td><?= h($empresasocios->id) ?></td>
                <td><?= h($empresasocios->user_id) ?></td>
                <td><?= h($empresasocios->empresa_id) ?></td>
                <td><?= h($empresasocios->cargo_id) ?></td>
                <td><?= h($empresasocios->nome) ?></td>
                <td><?= h($empresasocios->dt_nascimento) ?></td>
                <td><?= h($empresasocios->cpf) ?></td>
                <td><?= h($empresasocios->email) ?></td>
                <td><?= h($empresasocios->endereco) ?></td>
                <td><?= h($empresasocios->end_numero) ?></td>
                <td><?= h($empresasocios->end_complemento) ?></td>
                <td><?= h($empresasocios->end_bairro) ?></td>
                <td><?= h($empresasocios->end_cep) ?></td>
                <td><?= h($empresasocios->estado_id) ?></td>
                <td><?= h($empresasocios->cidade_id) ?></td>
                <td><?= h($empresasocios->telefone_residencial) ?></td>
                <td><?= h($empresasocios->telefone_celular) ?></td>
                <td><?= h($empresasocios->estadocivil_id) ?></td>
                <td><?= h($empresasocios->comunhaoregime_id) ?></td>
                <td><?= h($empresasocios->sexo_id) ?></td>
                <td><?= h($empresasocios->ctps) ?></td>
                <td><?= h($empresasocios->ctps_serie) ?></td>
                <td><?= h($empresasocios->cnh) ?></td>
                <td><?= h($empresasocios->cnh_dt_habilitacao) ?></td>
                <td><?= h($empresasocios->cnh_dt_vencimento) ?></td>
                <td><?= h($empresasocios->rg) ?></td>
                <td><?= h($empresasocios->rg_estado) ?></td>
                <td><?= h($empresasocios->rg_expedidor) ?></td>
                <td><?= h($empresasocios->rg_dt_expedicao) ?></td>
                <td><?= h($empresasocios->militar_numero) ?></td>
                <td><?= h($empresasocios->militar_expedidor) ?></td>
                <td><?= h($empresasocios->militar_serie) ?></td>
                <td><?= h($empresasocios->eleitor_numero) ?></td>
                <td><?= h($empresasocios->eleitor_zona) ?></td>
                <td><?= h($empresasocios->eleitor_secao) ?></td>
                <td><?= h($empresasocios->eleitor_dt_emissao) ?></td>
                <td><?= h($empresasocios->pai_nome) ?></td>
                <td><?= h($empresasocios->mae_nome) ?></td>
                <td><?= h($empresasocios->percentual_capitalsocial) ?></td>
                <td><?= h($empresasocios->admin) ?></td>
                <td><?= h($empresasocios->dt_cadastro) ?></td>
                <td><?= h($empresasocios->last_updated_fields) ?></td>
                <td><?= h($empresasocios->last_update) ?></td>
                <td><?= h($empresasocios->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Empresasocios', 'action' => 'view', $empresasocios->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Empresasocios', 'action' => 'edit', $empresasocios->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Empresasocios', 'action' => 'delete', $empresasocios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $empresasocios->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
