<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Empresaatividade'), ['action' => 'edit', $empresaatividade->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Empresaatividade'), ['action' => 'delete', $empresaatividade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $empresaatividade->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Empresaatividades'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Empresaatividade'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Empresas'), ['controller' => 'Empresas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Empresa'), ['controller' => 'Empresas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="empresaatividades view large-9 medium-8 columns content">
    <h3><?= h($empresaatividade->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Empresa') ?></th>
            <td><?= $empresaatividade->has('empresa') ? $this->Html->link($empresaatividade->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $empresaatividade->empresa->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($empresaatividade->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Atividadesauxiliar Id') ?></th>
            <td><?= $this->Number->format($empresaatividade->atividadesauxiliar_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dt Cadastro') ?></th>
            <td><?= h($empresaatividade->dt_cadastro) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Update') ?></th>
            <td><?= h($empresaatividade->last_update) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $empresaatividade->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
