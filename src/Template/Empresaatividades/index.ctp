<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Empresaatividade'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Empresas'), ['controller' => 'Empresas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Empresa'), ['controller' => 'Empresas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="empresaatividades index large-9 medium-8 columns content">
    <h3><?= __('Empresaatividades') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('empresa_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('atividadesauxiliar_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dt_cadastro') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_update') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($empresaatividades as $empresaatividade): ?>
            <tr>
                <td><?= $this->Number->format($empresaatividade->id) ?></td>
                <td><?= $empresaatividade->has('empresa') ? $this->Html->link($empresaatividade->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $empresaatividade->empresa->id]) : '' ?></td>
                <td><?= $this->Number->format($empresaatividade->atividadesauxiliar_id) ?></td>
                <td><?= h($empresaatividade->dt_cadastro) ?></td>
                <td><?= h($empresaatividade->last_update) ?></td>
                <td><?= h($empresaatividade->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $empresaatividade->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $empresaatividade->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $empresaatividade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $empresaatividade->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
