<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $empresaatividade->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $empresaatividade->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Empresaatividades'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Empresas'), ['controller' => 'Empresas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Empresa'), ['controller' => 'Empresas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="empresaatividades form large-9 medium-8 columns content">
    <?= $this->Form->create($empresaatividade) ?>
    <fieldset>
        <legend><?= __('Edit Empresaatividade') ?></legend>
        <?php
            echo $this->Form->input('empresa_id', ['options' => $empresas, 'empty' => true]);
            echo $this->Form->input('atividadesauxiliar_id');
            echo $this->Form->input('dt_cadastro', ['empty' => true]);
            echo $this->Form->input('last_update');
            echo $this->Form->input('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
