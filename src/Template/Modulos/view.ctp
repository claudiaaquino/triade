<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Módulo - <?= h($modulo->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Módulo') ?></p>
                        <p><?= $modulo->nome ? $modulo->nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Descrição') ?></p>
                        <p><?= $modulo->descricao ? $modulo->descricao : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Para todos do sistema?') ?></p>
                        <p><?= $modulo->todos ? 'SIM' : 'NÃO'; ?></p>

                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $modulo->status ? 'Ativo' : 'Desativado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $modulo->last_update ? $modulo->last_update : 'Não modificado'; ?></p>

                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                                <?= $this->Html->link("Editar", ['action' => 'edit', $modulo->id], ['class' => "btn btn-primary"]) ?>
                                <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $modulo->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $modulo->id)]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($modulo->modulosmenus)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Menus Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Menu') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($modulo->modulosmenus as $modulosmenus):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">

                                        <td><?= $this->Html->link($modulosmenus->menu->descricao, ['controller' => 'Menus', 'action' => 'view', $modulosmenus->menu_id]) ?></td>
                                        <td><?= h($modulosmenus->dt_cadastro) ?></td>
                                        <td><?= $modulosmenus->status ? 'Ativo' : 'Desativado' ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar Menu'), ['controller' => 'Menus', 'action' => 'view', $modulosmenus->menu_id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Desvíncular desse Menu'), ['controller' => 'Modulosmenus', 'action' => 'delete', $modulosmenus->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $modulosmenus->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($modulo->usermodulos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Usuários Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Usuário') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Empresa') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt. Vínculo') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($modulo->usermodulos as $usermodulos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= $this->Html->link($usermodulos->user->nome, ['controller' => 'Users', 'action' => 'view', $usermodulos->user_id]) ?></td>
                                        <td><?= $usermodulos->user->empresa ? $this->Html->link($usermodulos->user->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $usermodulos->user->empresa->id]) : '---' ?></td>

                                        <td><?= h($usermodulos->dt_cadastro) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar Usuário'), ['controller' => 'Users', 'action' => 'view', $usermodulos->user_id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?=  $this->Form->postLink(__('Desvinculá-lo do Módulo'), ['controller' => 'Usermodulos', 'action' => 'delete', $usermodulos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $usermodulos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


