<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Subtipodocumento'), ['action' => 'edit', $subtipodocumento->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Subtipodocumento'), ['action' => 'delete', $subtipodocumento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subtipodocumento->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Subtipodocumentos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subtipodocumento'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tipodocumentos'), ['controller' => 'Tipodocumentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tipodocumento'), ['controller' => 'Tipodocumentos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="subtipodocumentos view large-9 medium-8 columns content">
    <h3><?= h($subtipodocumento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Tipodocumento') ?></th>
            <td><?= $subtipodocumento->has('tipodocumento') ? $this->Html->link($subtipodocumento->tipodocumento->descricao, ['controller' => 'Tipodocumentos', 'action' => 'view', $subtipodocumento->tipodocumento->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Descricao') ?></th>
            <td><?= h($subtipodocumento->descricao) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($subtipodocumento->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dt Cadastro') ?></th>
            <td><?= h($subtipodocumento->dt_cadastro) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $subtipodocumento->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
