<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Subtipodocumentos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tipodocumentos'), ['controller' => 'Tipodocumentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipodocumento'), ['controller' => 'Tipodocumentos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subtipodocumentos form large-9 medium-8 columns content">
    <?= $this->Form->create($subtipodocumento) ?>
    <fieldset>
        <legend><?= __('Add Subtipodocumento') ?></legend>
        <?php
            echo $this->Form->input('tipodocumento_id', ['options' => $tipodocumentos]);
            echo $this->Form->input('descricao');
            echo $this->Form->input('status');
            echo $this->Form->input('dt_cadastro', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
