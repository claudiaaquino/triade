<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i><?= __('Assuntosinformativos') ?> - <?= h($assuntosinformativo->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Assunto') ?></p>
                                    <p><?= $assuntosinformativo->has('assunto') ? $this->Html->link($assuntosinformativo->assunto->descricao, ['controller' => 'Assuntos', 'action' => 'view', $assuntosinformativo->assunto->id]) : 'Não Informado' ?></p>

                                                                                                                    <p class="title"><?= __('Tipoinformativo') ?></p>
                                    <p><?= $assuntosinformativo->has('tipoinformativo') ? $this->Html->link($assuntosinformativo->tipoinformativo->id, ['controller' => 'Tipoinformativos', 'action' => 'view', $assuntosinformativo->tipoinformativo->id]) : 'Não Informado' ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $assuntosinformativo->id ? $this->Number->format($assuntosinformativo->id) : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Dt Cadastro') ?></p>
                                <p><?= $assuntosinformativo->dt_cadastro ? $assuntosinformativo->dt_cadastro : 'Não Informado'; ?></p>

                                                    
                        <p class="title"><?= __('Last Update') ?></p>
                                <p><?= $assuntosinformativo->last_update ? $assuntosinformativo->last_update : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Exibe Cliente') ?></p>
                                <p><?= $assuntosinformativo->exibe_cliente ? __('Ativo') : __('Desativado'); ?></p>

                                                    
                        <p class="title"><?= __('Exibe Funcionario') ?></p>
                                <p><?= $assuntosinformativo->exibe_funcionario ? __('Ativo') : __('Desativado'); ?></p>

                                                    
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $assuntosinformativo->status ? __('Ativo') : __('Desativado'); ?></p>

                                                                                                                                <p class="title"><?= __('Informativo') ?></p>
                                <p>  <?= $assuntosinformativo->informativo ? $this->Text->autoParagraph(h($assuntosinformativo->informativo)) : 'Não Informado'; ?></p>
                                                                        </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $assuntosinformativo->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $assuntosinformativo->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $assuntosinformativo->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
            </div>
</div>


