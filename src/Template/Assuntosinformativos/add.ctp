<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Cadastrar Assuntosinformativo') ?> <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($assuntosinformativo, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                                
                                    
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="assunto_id">assunto_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('assunto_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $assuntos, 'empty' => true]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipoinformativo_id">tipoinformativo_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('tipoinformativo_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tipoinformativos, 'empty' => true]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="informativo">informativo <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('informativo', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="exibe_cliente">exibe_cliente <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('exibe_cliente', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="exibe_funcionario">exibe_funcionario <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('exibe_funcionario', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                           
                                    
                                           
                                    
                                           
                    
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Cadastrar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>