<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i><?= __('Formaspagamentoservicos') ?> - <?= h($formaspagamentoservico->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Descricao') ?></p>
                                    <p><?= $formaspagamentoservico->descricao ? $formaspagamentoservico->descricao : 'Não Informado'; ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $formaspagamentoservico->id ? $this->Number->format($formaspagamentoservico->id) : 'Não Informado'; ?></p>

                                                                                                                                                </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $formaspagamentoservico->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $formaspagamentoservico->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $formaspagamentoservico->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
                    <?php if (!empty($formaspagamentoservico->previsaoorcamentos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Previsaoorcamentos Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tiposervico Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Apuracaoforma Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Atuacaoramo Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Min Funcionarios') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Max Funcionarios') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Min Faturamento Mensal') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Max Faturamento Mensal') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Formaspagamentoservico Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Valor Servico') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Valor Adicionalmensal') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Gratis Todosusuarios') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Gratis Todoscliente') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Gratis Clientemodulo') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Qtdegratis') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Qtdeilimitadagratis') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tela Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tempo Execucao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($formaspagamentoservico->previsaoorcamentos as $previsaoorcamentos):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                                                    <td><?= h($previsaoorcamentos->id) ?></td>
                                                                    <td><?= h($previsaoorcamentos->tiposervico_id) ?></td>
                                                                    <td><?= h($previsaoorcamentos->apuracaoforma_id) ?></td>
                                                                    <td><?= h($previsaoorcamentos->atuacaoramo_id) ?></td>
                                                                    <td><?= h($previsaoorcamentos->min_funcionarios) ?></td>
                                                                    <td><?= h($previsaoorcamentos->max_funcionarios) ?></td>
                                                                    <td><?= h($previsaoorcamentos->min_faturamento_mensal) ?></td>
                                                                    <td><?= h($previsaoorcamentos->max_faturamento_mensal) ?></td>
                                                                    <td><?= h($previsaoorcamentos->formaspagamentoservico_id) ?></td>
                                                                    <td><?= h($previsaoorcamentos->valor_servico) ?></td>
                                                                    <td><?= h($previsaoorcamentos->valor_adicionalmensal) ?></td>
                                                                    <td><?= h($previsaoorcamentos->gratis_todosusuarios) ?></td>
                                                                    <td><?= h($previsaoorcamentos->gratis_todoscliente) ?></td>
                                                                    <td><?= h($previsaoorcamentos->gratis_clientemodulo) ?></td>
                                                                    <td><?= h($previsaoorcamentos->qtdegratis) ?></td>
                                                                    <td><?= h($previsaoorcamentos->qtdeilimitadagratis) ?></td>
                                                                    <td><?= h($previsaoorcamentos->tela_id) ?></td>
                                                                    <td><?= h($previsaoorcamentos->tempo_execucao) ?></td>
                                                                    <td><?= h($previsaoorcamentos->user_id) ?></td>
                                                                    <td><?= h($previsaoorcamentos->dt_cadastro) ?></td>
                                                                    <td><?= h($previsaoorcamentos->last_update) ?></td>
                                                                    <td><?= h($previsaoorcamentos->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Previsaoorcamentos', 'action' => 'view', $previsaoorcamentos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Previsaoorcamentos', 'action' => 'edit', $previsaoorcamentos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Previsaoorcamentos', 'action' => 'delete', $previsaoorcamentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $previsaoorcamentos->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
            </div>
</div>


