<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2><?php if (!$admin) echo $this->Html->link(__('  Solicitar Abertura de Empresa'), ['action' => 'solicitarabertura'], ['class' => "btn btn-dark fa fa-file"]) ?></h2>
                <!--<h2><small></small></h2>-->
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>               
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <?php if (count($empresas)) { ?>
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_solicitacao', 'Dt. Solicitação') ?></th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('nome1', 'Possível Nome 1', ['escape' => false]) ?></th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('num_socios') ?></th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('num_funcionarios') ?></th>                                
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('capitalsocial', 'Capital Social') ?></th>                                
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($empresas as $empresa):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records">
                                        </td>
                                        <td class=" "><?= h($empresa->dt_solicitacao) ?></td>
                                        <td class=" "><?= h($empresa->nome1) ?></td>
                                        <td class=" "><?= h($empresa->num_socios) ?></td>
                                        <td class=" "><?= h($empresa->num_funcionarios) ?></td>
                                        <td class=" "><?= h($empresa->capitalsocial) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $empresa->id], ['class' => "btn btn-primary btn-xs"]); ?>
                                                <?= $this->Html->link(__('Editar'), ['action' => 'edit', $empresa->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?php if ($admin) echo $this->Html->link(__('Efetuar Abertura'), ['action' => 'efetuarabertura', $empresa->id], [ 'class' => "btn btn-success btn-xs"]) ?>
                                            </div>

                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('próximo') . ' >') ?>
                            </ul>
                            <?= $this->Paginator->counter() ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


