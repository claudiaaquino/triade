<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Abertura de Empresa<small>   * campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <?= $this->Flash->render() ?>
            <div class="x_content">
                <?= $this->Form->create($empresa, ["class" => "form-horizontal form-label-left"]) ?>
                <div id="wizard" class="form_wizard wizard_horizontal">
                    <ul class="wizard_steps">
                        <li>
                            <a href="#step-1">
                                <span class="step_no">1</span>
                                <span class="step_descr">
                                    <small>Formação</small>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-2">

                                <span class="step_no">2</span>
                                <small>Endereço</small>
                            </a>
                        </li>
                        <li>
                            <a href="#step-3">

                                <span class="step_no">3</span>
                                <small>Atividades</small>
                            </a>
                        </li>
                        <!--                        <li>
                                                    <a href="#step-4">
                        
                                                        <span class="step_no">4</span>
                                                        <small>Contato</small>
                                                    </a>
                                                </li>-->
                        <li>
                            <a href="#step-4"  data-content-url="<?= $this->request->webroot; ?>empresasocios/addajax/<?= $empresa->id ? $empresa->id : '' ?>">

                                <span class="step_no">4</span>
                                <small id="label_empresarios">Sócios</small>
                            </a>
                        </li>
                        <li>
                            <a href="#step-5">

                                <span class="step_no">5</span>
                                <small>Finalizar</small>
                            </a>
                        </li>
                    </ul>

                    <div id="step-1" class="col-md-12 col-sm-12 col-xs-12">
                        <h3>Nome da Empresa</h3>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome1">Primeira Opção de Nome<span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('nome1', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'placeholder' => "Primeiro possível nome", 'required' => 'required']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome2">Segunda Opção de Nome<span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('nome2', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'placeholder' => "Segundo possível nome", 'required' => 'required']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome3">Terceira Opção de Nome
                                <!--<span class="required">*</span>-->
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('nome3', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'placeholder' => "Terceiro possível nome"]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fantasia">Nome Fantasia
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('fantasia', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <h3>Formação Societária</h3>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-6 col-xs-12  col-md-offset-3">
                                <?=
                                $this->Form->radio('sociedade', [
                                    ['value' => '0', 'text' => 'Empresário', "class" => "form-control col-md-7 col-xs-12"],
                                    ['value' => '1', 'text' => 'Sociedade', "class" => "form-control col-md-7 col-xs-12"]
                                        ]
                                );
                                ?>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="capitalsocial">Capital Social
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('capitalsocial', ['type' => 'text',  "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'step' => '1000', 'min' => '0']); ?>
                            </div>
                        </div>
                        <div class="form-group sociedade" style="display: none;">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="num_socios">Número de Sócios
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('num_socios', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <h3>Porte da Empresa</h3>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="porte_id">Porte
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('porte_id', [ "options" => $portes, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => '(Selecione uma opção)']); ?>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <h3>Dados Específicos</h3>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="num_funcionarios">Previsão de Funcionários
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('num_funcionarios', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'min' => '0']); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="faturamento_mensal">Previsão de Faturamento Mensal
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('faturamento_mensal', ['type' => 'text',  "class" => "form-control col-md-7 col-xs-12", 'label' => false/*, 'step' => '1000', 'min' => '0'*/]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="apuracaoforma_id">Forma de Apuração em que se enquadra
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('apuracaoforma_id', ['options' => $apuracaoformas, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => '(Selecione uma opção)']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="apuracaoforma_id">Ramo de atuações
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('atuacaoramo_id', ['options' => $atuacaoramos, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => '(Selecione uma opção)']); ?>
                            </div>
                        </div>
                    </div>
                    <div id="step-2">
                        <h3>Endereço da Empresa</h3>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="indicecadastral">Índice Cadastral (IPTU)<span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('indicecadastral', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'required' => 'required']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="endereco">Endereço 
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('endereco', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_numero">Número
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('end_numero', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_complemento">Complemento
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('end_complemento', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_bairro">Bairro
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('end_bairro', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_cep">CEP
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('end_cep', ["data-inputmask" => "'mask': '99999-999'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="estado_id">Estado
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('estado_id', ['options' => $estados, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => '(Selecione um estado)', 'sel' => $empresa->estado_id]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cidade_id">Cidade
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('cidade_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'sel' => $empresa->cidade_id]); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="arealocal"> Área Total do Empreendimento (m²) 
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('arealocal', ["class" => "form-control col-md-2 col-xs-12", 'label' => false, 'min' => 0]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="residencia_socio">O local é residência de um dos sócios?
                            </label>
                            <div class="col-md-4 col-sm-2 col-xs-12">
                                <?= $this->Form->checkbox('residencia_socio', ["class" => "flat", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="enderecocorrespondencia">Esse será o endereço para correspondência?
                            </label>
                            <div class="col-md-4 col-sm-2 col-xs-12">
                                <?= $this->Form->checkbox('enderecocorrespondencia', ["class" => "flat", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="enderecodocumento">Esse é o endereço onde ficam os documentos?
                            </label>
                            <div class="col-md-4 col-sm-2 col-xs-12">
                                <?= $this->Form->checkbox('enderecodocumento', ["class" => "flat", 'label' => false]); ?>
                            </div>
                        </div>
                    </div>
                    <div id="step-3" >
                        <h3>Atividades</h3>
                        <div class="ln_solid"></div>
                        <div class="form_wizard col-md-5 col-sm-5 col-xs-12" style="height: 100%;">
                            <h2>Inserir Nova Atividade</h2>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="secao">Seção 
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <?= $this->Form->input('cnaesection_id', ["class" => "form-control", 'label' => false]); ?>
                                </div>
                            </div>   
                            <div class="form-group" id="divisao-box" >
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="divisao">Divisão 
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12" >
                                    <?= $this->Form->input('cnaedivision_id', [ "class" => "form-control", 'label' => false]); ?>
                                </div>
                            </div>   
                            <div class="form-group" id="grupo-box">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="grupo">Grupo 
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <?= $this->Form->input('cnaegroup_id', [ "class" => "form-control", 'label' => false]); ?>
                                </div>
                            </div>   
                            <div class="form-group" id="classe-box">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="classe">Classe
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <?= $this->Form->input('cnaeclasse_id', [ "class" => "form-control", 'label' => false]); ?>
                                </div>
                            </div>   
                            <div class="form-group quest-box">
                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="classe">Ativida Exercida no Local?
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="exercida_local" type="checkbox" class="flat"/>
                                </div>
                            </div>
                            <div class="form-group quest-box">
                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="classe">Ativida Principal?
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="principal" type="checkbox" class="flat"/>
                                </div>
                            </div>
                            <div class="form-group quest-box" >
                                <div class="control-label col-md-3 col-sm-3 col-xs-12">
                                    <!--col-md-offset-6">-->
                                    <?= $this->Form->input('empresa_id', ["type" => "hidden", "value" => $empresa->id]); ?>
                                    <?= $this->Form->button(__('  Inserir'), ['onclick' => 'addCnaeRelation()', "type" => 'button', 'class' => "btn btn-primary"]) ?>
                                </div>
                            </div>  
                            <div class="ln_solid"></div>
                        </div>  

                        <div class="form_wizard  col-md-7 col-sm-7 col-xs-12">
                            <h2>Atividades Inseridas</h2>
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th scope="col" class="column-title"><?= __('Classe') ?></th>
                                            <th scope="col" class="column-title"><?= __('Atividade') ?></th>  
                                            <th scope="col" class="column-title"><?= __('Exercida no Local') ?></th>  
                                            <th scope="col" class="column-title"><?= __('Atividade Principal') ?></th>  
                                            <th class="bulk-actions" colspan="7">
                                                <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                            </th>
                                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>

                                        </tr>
                                    </thead>
                                    <tbody id="table-cnaes">
                                        <tr><td>sem registros...</td></tr>    
                                    </tbody>                       
                                </table>
                            </div>
                        </div>
                        <span class="clearfix"></span>
                        <div class="ln_solid"></div>


                        <h3>Atividades Auxiliares</h3>
                        <div class="ln_solid"></div>
                        <div class="form_wizard col-md-5 col-sm-5 col-xs-12" style="height: 100%;">
                            <!--<h2>Inserir Atividade Auxiliar</h2>-->
                            <div class="form-group">
                                <div class="col-md-10 col-sm-10 col-xs-12">
                                    <?= $this->Form->input('atividadesauxiliar_id', [ "class" => "form-control", 'label' => false]); ?>
                                </div>
                                <div class="control-label col-md-2 col-sm-2 col-xs-12">
                                    <?= $this->Form->button(__('  Inserir'), ['onclick' => 'addAtividadeAuxiliarRelation()', "type" => 'button', 'class' => "btn btn-primary"]) ?>
                                </div>
                            </div>   
                            <div class="ln_solid"></div>
                        </div>  

                        <div class="form_wizard  col-md-7 col-sm-7 col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th scope="col" class="column-title"><?= __('Atividades Auxiliares Inseridas') ?></th>  
                                            <th class="bulk-actions" colspan="7">
                                                <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                            </th>
                                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>

                                        </tr>
                                    </thead>
                                    <tbody id="table-atividades">
                                        <tr><td>sem registros...</td></tr>    
                                    </tbody>                       
                                </table>
                            </div>
                        </div>
                        <span class="clearfix"></span>
                        </br></br></br>
                    </div>
                    <!--                    <div id="step-4">
                                            <h3>Dados de Contato do Solicitante da Abertura</h3>
                                            <div class="ln_solid"></div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome_responsavel">Nome Completo
                                                </label>
                                                <div class="col-md-4 col-sm-6 col-xs-12">
                    <? $this->Form->input('nome_responsavel', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                                    <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cpf">CPF
                                                </label>
                                                <div class="col-md-4 col-sm-6 col-xs-12">
                    <? $this->Form->input('cpf', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">E-mail <span class="required">*</span>
                                                </label>
                                                <div class="col-md-4 col-sm-6 col-xs-12">
                    <? $this->Form->input('email', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                                    <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefone">Telefone
                                                </label>
                                                <div class="col-md-4 col-sm-6 col-xs-12">
                    <? $this->Form->input('telefone', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                                    <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                                                </div>
                                            </div>
                    
                    
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cargo_responsavel">Cargo
                                                </label>
                                                <div class="col-md-4 col-sm-6 col-xs-12">
                    <? $this->Form->input('cargo_responsavel', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                                </div>
                                            </div>
                    
                                        </div>-->
                    <div id="step-4">
                        <iframe id="socios-frame" class="control-label col-md-12 col-sm-12 col-xs-12" frameborder='0' height="100%" ></iframe>
                    </div>
                    <div id="step-5">
                        <h3>Detalhes para contratar esse Serviço</h3>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="leituratermos">Eu li e aceito os 
                                <?= $this->Html->link(__('TERMOS DO CONTRATO'), "/docs/termoscontratos/abertura.pdf", array('target' => '_blank')); ?>
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->checkbox('leituratermos', ["class" => "flat", 'label' => false]); ?>
                            </div>
                        </div>                        
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <?= $this->Form->input('id', [ "type" => "hidden", 'label' => false]); ?>
                <?= $this->Form->input('num_step', [ "type" => "hidden", 'label' => false]); ?>
                <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js', array('inline' => false)); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js', array('inline' => false)); ?>
<?= $this->Html->script('/js/ajax/cidades.js'); ?>
<?= $this->Html->script('/js/ajax/documentos.js'); ?>
<?= $this->Html->script('/js/ajax/cnaes.js'); ?>

<script>
    var myIframe, srcframe = 'socios', loadedframe = '';
    var frameload = false;
    $(document).ready(function () {
        $(":input").inputmask();
        $("select").select2({placeholder: 'selecione uma opção'});
        $("select").addClass('select2');
        $('#divisao-box').hide();
        $('#grupo-box').hide();
        $('#classe-box').hide();
        $('.quest-box').hide();
        toggleSociedade();

        var intervalo;
        /*-------Form Wizard INICIO---------*/
        $('#wizard').smartWizard({
            selected: 0,
//            contentURL: $('#urlroot').val() + 'empresasocios/add',
            labelFinish: 'Salvar e Prosseguir',
            enableFinishButton: true,
            onLeaveStep: leaveStepCallback
        });
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-success');
        $('.buttonNext').addClass('btn btn-primary');
        $('.buttonPrevious').click(function () {
            $('html, body').animate({scrollTop: $('#wizard').offset().top}, 'slow');
        });
        $('.buttonNext').click(function () {
            $('html, body').animate({scrollTop: $('#wizard').offset().top}, 'slow');
        });

        function leaveStepCallback(obj, context) {
            if ($('#id').val() == '') {
                $('#num-step').val($('#wizard').smartWizard('currentStep'));
                $("form").submit();
            }

            if (context.toStep == 4 && (!frameload || loadedframe != srcframe)) {
                loadSociosFrame();
            }
            return true;
        }

        if ($('#num-step').val() > 0) {
            $('#wizard').smartWizard("goToStep", $('#num-step').val());
        }

        /*-----------------ABA 3 - Para Fazer registro de Atividades--------------------------------*/
        loadCnaesTable();
        loadCnaeSectionList();

        loadAtividadesTable();
        loadAtividadesList();
        /*-----------------FIM ABA 3 - Para Fazer registro de Atividades--------------------------------*/
        /*-------Form Wizard FIM---------*/


        $('input[name="sociedade"]:radio').change(function () {
            toggleSociedade();
        });

//        $(".select2_multiple").select2({
////            maximumSelectionLength: 4,
//            placeholder: "selecione os ramos de atuação",
//            allowClear: false
//        });

        $('#capitalsocial').inputmask('decimal', {
            radixPoint: ",",
            groupSeparator: ".",
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
            rightAlign: false,
            onBeforeMask: function (value, opts) {
                return value;
            }});
         $('#faturamento-mensal').inputmask('decimal', {
            radixPoint: ",",
            groupSeparator: ".",
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
            rightAlign: false,
            onBeforeMask: function (value, opts) {
                return value;
            }});

    });
    function toggleSociedade() {
        var formacao = $('input[name="sociedade"]:radio:checked').val();
        if (formacao == '1') {//sociedade
            $('.sociedade').show();
            $('#label_empresarios').html('Sócios');
            srcframe = 'socios';
        } else if (formacao == '0') {
            $('.sociedade').hide();
            $('#label_empresarios').html('Empresário');
            srcframe = 'empresario';
        }
    }
    function loadSociosFrame() {
        // isso vai ajustar o tamanho do frame de sócios, para quando for montar a aba, já vir com o tamanho apropriado.
        myIframe = $("#socios-frame").attr("src", "<?= $this->request->webroot; ?>empresasocios/" + srcframe + "/<?= $empresa->id ? $empresa->id : '' ?>");
        $(myIframe).load(ajustHeightSociosFrame);
        frameload = true;
        loadedframe = srcframe;
    }
    function ajustHeightSociosFrame() {
        var myDoc = (myIframe.get(0).contentDocument) ? myIframe.get(0).contentDocument : myIframe.get(0).contentWindow.document;
        myIframe.height(myDoc.body.scrollHeight);
        $('#wizard').smartWizard("fixHeight");
    }
</script>