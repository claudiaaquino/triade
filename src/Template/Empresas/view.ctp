<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<?= $this->Html->css('/vendors/pnotify/dist/pnotify.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel" >   
            <?= $this->Flash->render() ?>
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= $empresa->razao ? $empresa->razao : $empresa->nome1; ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <?php if ($admin) { ?>
                        <li><?= $this->Html->link(__('  Editar Informações'), ['action' => 'edit', $empresa->id], ['class' => "btn btn-dark fa fa-pencil-square-o"]) ?></li>
                    <?php } ?>
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" style="display: none;" >
                <h4>Dados Principais da Empresa</h4>
                <div class="ln_solid"></div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <?php if ($empresa->solicitacao) { ?>
                            <p class="title"><?= __('Dt da Solicitacao de Abertura de Empresa') ?></p>
                            <p><?= h($empresa->dt_solicitacao) ?></p>

                            <p class="title"><?= __('Primeira Opção de Nome') ?></p>
                            <p><?= h($empresa->nome1) ?></p>                            

                            <p class="title"><?= __('Segunda  Opção de Nome') ?></p>
                            <p><?= h($empresa->nome2) ?></p>

                            <p class="title"><?= __('Terceira Opção de Nome') ?></p>
                            <p><?= $empresa->nome1 ? $empresa->nome1 : 'Não informado'; ?></p>
                        <?php } else { ?>
                            <p class="title"><?= __('Razão Social') ?></p>
                            <p><?= h($empresa->razao) ?></p>

                            <p class="title"><?= __('Nome Fantasia') ?></p>
                            <p><?= h($empresa->fantasia) ?></p>

                            <p class="title"><?= __('CNPJ') ?></p>
                            <p><?= $empresa->cnpj ? $empresa->cnpj : 'Não informado'; ?></p>

                        <?php } ?>

                        <p class="title"><?= __('Inscrição Estadual') ?></p>
                        <p><?= $empresa->insc_estadual ? $empresa->insc_estadual : 'Não informado'; ?></p>

                        <p class="title"><?= __('Inscrição Municipal') ?></p>
                        <p><?= $empresa->insc_municipal ? $empresa->insc_municipal : 'Não informado'; ?></p>

                        <p class="title"><?= __('Número de Registro da Junta - Data') ?></p>
                        <p><?= $empresa->nire ? $empresa->nire . ' - ' . $empresa->dt_nire : 'Não informado'; ?></p>


                    </div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Nome Contato Principal') ?></p>
                        <p><?= $empresa->nome_responsavel ? $empresa->nome_responsavel : 'Não informado'; ?></p>

                        <p class="title"><?= __('CPF') ?></p>
                        <p><?= $empresa->cpf ? $empresa->cpf : 'Não informado'; ?></p>

                        <p class="title"><?= __('Cargo/Função') ?></p>
                        <p><?= $empresa->cargo_responsavel ? $empresa->cargo_responsavel : 'Não informado'; ?></p>

                        <p class="title"><?= __('Email') ?></p>
                        <p><?= $empresa->email ? $empresa->email : 'Não informado'; ?></p>

                        <p class="title"><?= __('Telefone') ?></p>
                        <p><?= $empresa->telefone ? $empresa->telefone : 'Não informado'; ?></p>
                    </div>
                </div>

                <?php if ($admin) { ?>
                    <div class="clearfix"></div>
                    <div class="ln_solid"></div>                
                    <h4>Informações Específicas</h4>
                    <div class="ln_solid"></div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="project_detail">

                            <p class="title"><?= __('Porte') ?></p>
                            <?php if ($admin && $empresa->has('porte')) { ?>
                                <p><?= $this->Html->link($empresa->porte->descricao, ['controller' => 'Portes', 'action' => 'view', $empresa->porte->id]) ?></p>
                            <?php } else if ($empresa->has('porte')) { ?>
                                <p><?= $empresa->porte->descricao ?></p>
                                <?php
                            } else {
                                echo '<p>Não informado</p>';
                            }
                            ?>

                            <p class="title"><?= __('Faturamento Mensal') ?></p>
                            <p><?= $this->Number->format($empresa->faturamento_mensal) ?></p>

                            <p class="title"><?= __('Forma de Apuração') ?></p>
                            <?php if ($admin && $empresa->has('apuracaoforma')) { ?>
                                <p><?= $empresa->has('apuracaoforma') ? $this->Html->link($empresa->apuracaoforma->descricao, ['controller' => 'Apuracaoformas', 'action' => 'view', $empresa->apuracaoforma->id]) : 'Não informado'; ?></p>
                            <?php } else if ($empresa->has('apuracaoforma')) { ?>   
                                <p><?= $empresa->apuracaoforma->descricao ?></p>
                                <?php
                            } else {
                                echo '<p>Não informado</p>';
                            }
                            ?>

                            <p class="title"><?= __('Ramo de Atuação') ?></p>
                            <?php if ($admin && $empresa->has('atuacaoramo')) { ?>
                                <p><?= $empresa->has('atuacaoramo') ? $this->Html->link($empresa->atuacaoramo->descricao, ['controller' => 'Atuacaoramos', 'action' => 'view', $empresa->atuacaoramo->id]) : 'Não informado'; ?></p>
                            <?php } else if ($empresa->has('atuacaoramo')) { ?>   
                                <p><?= $empresa->atuacaoramo->descricao ?></p>
                                <?php
                            } else {
                                echo '<p>Não informado</p>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="project_detail">

                            <p class="title"><?= __('Num. Funcionários') ?></p>
                            <p><?= $empresa->num_funcionarios ? $this->Number->format($empresa->num_funcionarios) : 'Não informado'; ?></p>

                            <?php if ($empresa->sociedade) { ?>
                                <p class="title"><?= __('Num. Sócios') ?></p>
                                <p><?= $empresa->num_socios ? $this->Number->format($empresa->num_socios) : 'Não informado'; ?></p>
                            <?php } ?>

                            <p class="title"><?= __('Capital Social') ?></p>
                            <p><?= $empresa->capitalsocial ? $this->Number->format($empresa->capitalsocial) : 'Não informado'; ?></p>

                        </div>
                    </div>
                <?php } ?>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <h4>Informações de Endereço</h4>
                <div class="ln_solid"></div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Indice Cadastral') ?></p>
                        <p><?= $empresa->indicecadastral ? $empresa->indicecadastral : 'Não informado'; ?></p>

                        <p class="title"><?= __('Endereco') ?></p>
                        <p><?= $empresa->endereco ? $empresa->endereco : 'Não informado'; ?></p>

                        <p class="title"><?= __('End Numero') ?></p>
                        <p><?= $empresa->end_numero ? $empresa->end_numero : 'Não informado'; ?></p>

                        <p class="title"><?= __('End Complemento') ?></p>
                        <p><?= $empresa->end_complemento ? $empresa->end_complemento : 'Não informado'; ?></p>

                        <p class="title"><?= __('End Bairro') ?></p>
                        <p><?= $empresa->end_bairro ? $empresa->end_bairro : 'Não informado'; ?></p>

                        <p class="title"><?= __('End Cep') ?></p>
                        <p><?= $empresa->end_cep ? $empresa->end_cep : 'Não informado'; ?></p>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-3">
                            <?php if ($admin) { ?>
                                <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                                <?= $this->Html->link(__('Editar'), ['action' => 'edit', $empresa->id], ['class' => "btn btn-primary"]) ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Estado') ?></p>
                        <p><?= $empresa->has('estado') ? $this->Html->link($empresa->estado->estado_sigla, ['controller' => 'Estados', 'action' => 'view', $empresa->estado->id]) : 'Não informado'; ?></p>

                        <p class="title"><?= __('Cidade') ?></p>
                        <p><?= $empresa->has('cidade') ? $this->Html->link($empresa->cidade->cidade_nome, ['controller' => 'Cidades', 'action' => 'view', $empresa->cidade->id]) : 'Não informado'; ?></p>

                        <p class="title"><?= __('Area do local') ?></p>
                        <p><?= $this->Number->format($empresa->arealocal) ?> m²</p>

                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Residência Sócio?') ?></p>
                        <p><?= $empresa->residencia_socio ? __('SIM') : __('NÃO'); ?></p>


                        <p class="title"><?= __('O Endereço informado é o de correspondência?') ?></p>
                        <p><?= $empresa->enderecocorrespondencia ? __('SIM') : __('NÃO'); ?></p>


                        <p class="title"><?= __('O Endereço informado é onde se encontram os Documentos') ?></p>
                        <p><?= $empresa->enderecodocumento ? __('SIM') : __('NÃO'); ?></p>

                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($empresa->empresacnaes)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> CNAES </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"  style="display: none;" >
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Classe') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Descrição') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Exercida no Local') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Principal') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($empresa->empresacnaes as $empresacnaes):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($empresacnaes->cnaeclass->classe) ?></td>
                                        <td><?= h($empresacnaes->cnaeclass->denominacao) ?></td>
                                        <td><?= h($empresacnaes->dt_cadastro) ?></td>
                                        <td><?= $empresacnaes->exercida_local ? 'SIM' : 'NÃO' ?></td>
                                        <td><?= $empresacnaes->principal ? 'SIM' : 'NÃO' ?></td>
                                        <?php if ($admin) { ?>
                                            <td  class=" last">
                                                <div class="btn-group">
                                                    <?= $this->Html->link(__('Visualizar'), ['controller' => 'Empresacnaes', 'action' => 'view', $empresacnaes->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                    <?= $this->Html->link(__('Editar'), ['controller' => 'Empresacnaes', 'action' => 'edit', $empresacnaes->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Empresacnaes', 'action' => 'delete', $empresacnaes->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $empresacnaes->id)]) ?>
                                                </div>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($empresa->empresaatividades)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Atividades Vínculadas </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="display: none;" >
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Atividade') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($empresa->empresaatividades as $empresaatividades):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($empresaatividades->atividadesauxiliar->descricao) ?></td>
                                        <td><?= h($empresaatividades->dt_cadastro) ?></td>
                                        <?php if ($admin) { ?>
                                            <td  class=" last">
                                                <div class="btn-group">
                                                    <?= $this->Html->link(__('Visualizar'), ['controller' => 'Empresaatividades', 'action' => 'view', $empresaatividades->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                    <?= $this->Html->link(__('Editar'), ['controller' => 'Empresaatividades', 'action' => 'edit', $empresaatividades->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Empresaatividades', 'action' => 'delete', $empresaatividades->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $empresaatividades->id)]) ?>
                                                </div>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($empresa->empresasocios)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> 
                        <?php if ($empresa->sociedade) { ?>
                            Sócios  
                        <?php } else { ?>
                            Empresário
                        <?php } ?>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"  style="display: none;" >
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Cpf') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Email') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Perc. Capital Social') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Contribui INSS') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Responsavel na Receita') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Certificado Digital PF') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Token A3') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Admin') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($empresa->empresasocios as $empresasocios):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($empresasocios->nome) ?></td>
                                        <td><?= h($empresasocios->cpf) ?></td>
                                        <td><?= h($empresasocios->email) ?></td>
                                        <td><?= h($empresasocios->percentual_capitalsocial) ?></td>
                                        <td><?= $empresasocios->contribuiinss ? 'SIM' : 'NÃO'; ?></td>
                                        <td><?= $empresasocios->responsavelreceita ? 'SIM' : 'NÃO'; ?></td>
                                        <td><?= $empresasocios->certificadodigital_pessoafisica ? 'SIM' : 'NÃO'; ?></td>
                                        <td><?= $empresasocios->token_a3 ? 'SIM' : 'NÃO'; ?></td>
                                        <td><?= $empresasocios->admin ? 'SIM' : 'NÃO'; ?></td>  
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Empresasocios', 'action' => 'view', $empresasocios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?php if ($admin) { ?>
                                                    <?= $this->Html->link(__('Editar'), ['controller' => 'Empresasocios', 'action' => 'edit', $empresasocios->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Empresasocios', 'action' => 'delete', $empresasocios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $empresasocios->id)]) ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($empresa->users)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Usuários dessa Empresa </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"  style="display: none;" >
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Usuário') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Email') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Ult. Login') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($empresa->users as $empresausuarios):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($empresausuarios->nome) ?></td>
                                        <td><?= h($empresausuarios->username) ?></td>
                                        <td><?= h($empresausuarios->email) ?></td>
                                        <td><?= h($empresausuarios->last_login) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Users', 'action' => 'view', $empresausuarios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?php if ($admin || $admin_empresa) { ?>
                                                    <?= $this->Html->link(__('Editar'), ['controller' => 'Users', 'action' => 'edit', $empresausuarios->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Users', 'action' => 'delete', $empresausuarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $empresausuarios->id)]) ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($empresa->funcionarios)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Funcionários  </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"  style="display: none;" > 
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Cargo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Salário') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Aux Transporte?') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Valor Aux Alim.') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Contrato') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($empresa->funcionarios as $funcionarios):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($funcionarios->nome) ?></td>
                                        <td><?= $funcionarios->cargo ? $funcionarios->cargo->descricao : 'Não informado' ?></td>
                                        <td>R$ <?= h($funcionarios->salario) ?></td>
                                        <td><?= $funcionarios->flag_aux_transporte ? 'Sim' : 'Não' ?></td>
                                        <td>R$ <?= $funcionarios->aux_alimentacao ? $funcionarios->aux_alimentacao : '--' ?></td>
                                        <td><?= h($funcionarios->dt_contrato_efetivado) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Funcionarios', 'action' => 'view', $funcionarios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Funcionarios', 'action' => 'edit', $funcionarios->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Funcionarios', 'action' => 'delete', $funcionarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $funcionarios->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($empresa->acessosexternos) && ($admin || $admin_empresa)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Senhas de Acesso à Sistemas Externos </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="display: none;" >
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <?php if ($admin) { ?>
                                        <th scope="col" class="column-title"><?= $this->Paginator->sort('Sistemasexternos.descricao', 'Acesso ao Sistema') ?></th>                                
                                        <th scope="col" class="column-title"><?= $this->Paginator->sort('exibecliente', 'Exibe para a Empresa?') ?></th>
                                    <?php } else { ?>
                                        <th scope="col" class="column-title"><?= $this->Paginator->sort('Sistemasexternos.descricao', 'Acesso ao Sistema') ?></th>                                
                                    <?php } ?>

                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($empresa->acessosexternos as $acessosexterno) {
                                    if ($admin || ($admin_empresa && $acessosexterno->exibecliente)) {
                                        ?>                                
                                        <tr class="<?= $cor ?> pointer">

                                            <?php if ($admin) { ?>
                                                <td><?= $acessosexterno->has('sistemasexterno') ? $this->Html->link($acessosexterno->sistemasexterno->descricao, ['controller' => 'Sistemasexternos', 'action' => 'view', $acessosexterno->sistemasexterno->id]) : '' ?></td>
                                                <td><?= $acessosexterno->exibecliente ? 'SIM' : 'NÃO'; ?></td>
                                            <?php } else { ?>
                                                <td><?= $acessosexterno->sistemasexterno->descricao; ?></td>
                                            <?php } ?>

                                            <td  class=" last">
                                                <div class="btn-group">
                                                    <?= $this->Html->link(__('Visualizar'), ['controller' => 'Acessosexternos', 'action' => 'view', $acessosexterno->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                    <?php if ($admin) { ?>
                                                        <?= $this->Html->link(__('Editar'), ['controller' => 'Acessosexternos', 'action' => 'edit', $acessosexterno->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                        <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Acessosexternos', 'action' => 'delete', $acessosexterno->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $acessosexterno->id)]) ?>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                        $cor = $cor == 'even' ? 'odd' : 'even';
                                    }
                                }
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($empresa->documentos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Documentos de Constituição da Empresa </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"  style="display: none;" >
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Tipo de Documento') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Enviado') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($empresa->documentos as $documentos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= $this->Html->link($documentos->tipodocumento->descricao, "/docs/" . $documentos->file, ['target' => '_blank']) ?></td>
                                        <td><?= h($documentos->dt_enviado) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Download'), "/docs/" . $documentos->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                                <?php if ($admin) { ?>
                                                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Documentos', 'action' => 'delete', $documentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $documentos->id)]) ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php
        if (!empty($empresa->empresasocios)) {
            foreach ($empresa->empresasocios as $empresasocio):
                if (!empty($empresasocio->documentos)) {
                    ?>
                    <div class="x_panel">
                        <div class="x_title">
                            <h2 class="green"><i class="fa fa-file"></i> Documentos do <?= $empresa->sociedade ? 'Sócio' : 'Empresário' ?>: <?= h($empresasocio->nome) ?> </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content"  style="display: none;" >
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th scope="col"  class="column-title"><?= __('Tipo de Documento') ?></th>
                                            <th scope="col"  class="column-title"><?= __('Dt Envio') ?></th>
                                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $cor = 'even';
                                        foreach ($empresasocio->documentos as $documentos):
                                            ?>
                                            <tr class="<?= $cor ?> pointer">
                                                <td><?= $this->Html->link($documentos->tipodocumento->descricao, "/docs/" . $documentos->file, ['target' => '_blank']) ?></td>
                                                <td><?= h($documentos->dt_enviado) ?></td>
                                                <td  class=" last">
                                                    <div class="btn-group">
                                                        <?= $this->Html->link(__('Download'), "/docs/" . $documentos->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                                        <?php if ($admin) { ?>
                                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Documentos', 'action' => 'delete', $documentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $documentos->id)]) ?>
                <?php } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                            $cor = $cor == 'even' ? 'odd' : 'even';
                                        endforeach;
                                        ?>
                                    </tbody>                       
                                </table>
                            </div>
                        </div>
                    </div>

                    <?php
                }
            endforeach;
        }
        ?>

        <div class="x_panel legislativo_panel" style="display: none;">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Informações Legislativas </h2>
                <ul class="nav navbar-right panel_toolbox">
<?php if ($admin) { ?> <li><button id='btn-modal-anotacao' type="button" class="btn btn-dark fa fa-plus" data-toggle="modal" data-target=".modal-anotacao"> Adicionar Anotação</button></li><?php } ?>
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content"  style="display: none;" >
<?= $this->Form->input('empresa_id', ["type" => "hidden", "value" => $empresa->id]); ?>
                <div id="legisanotacoes" class="col-md-12 col-sm-12 col-xs-12">
                </div>
            </div>
        </div>
    </div>
</div>

<!--CADASTRAR ANOTAÇÕES-->
<div class="modal fade modal-anotacao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Nova Anotação</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->input('tipo-anotacao', ['options' => [], "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'style' => 'width:100%', 'empty' => true]); ?>
                <div class="clearfix"></div><br/>
                <?= $this->Form->input('titulo-anotacao', ['type' => 'text', "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'placeholder' => 'Título da Anotação']); ?>
                <div class="clearfix"></div><br/>
<?= $this->Form->input('anotacao', ['type' => 'textarea', "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'placeholder' => 'Digite a nova anotação aqui...']); ?>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id='btn-add-anotacao' class="btn btn-primary">Adicionar</button>
                <button type="button" id='btn-edit-anotacao' class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </div>

</div>

<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/vendors/pnotify/dist/pnotify.js'); ?>
<?= $this->Html->script('/vendors/ckeditor/ckeditor.js'); ?>
<script>
    $(document).ready(function () {
        configInicial();
        eventsInicial();
    });
    function configInicial() {
        $("#tipo-anotacao").select2({dropdownParent: $(".modal-anotacao"), tokenSeparators: [',', ';'], tags: true, placeholder: 'Selecione um tipo ou digite um novo'});

    }
    function eventsInicial() {
        renderAnotacoes();
<?php if ($admin) { ?>
            refreshTipoNotes();
            $("#btn-add-anotacao").click(function () {
                addAnotacao();
            });
            $("#btn-modal-anotacao").click(function () {
                if (CKEDITOR.instances.anotacao)
                    CKEDITOR.instances.anotacao.destroy();
                $('#anotacao').val('');
                $("#btn-add-anotacao").show();
                $("#btn-edit-anotacao").hide();
                CKEDITOR.replace('anotacao');
            });
            $("#btn-edit-anotacao").click(function () {
                editAnotacao();
                renderAnotacoes();
            });
<?php } ?>
    }
<?php if ($admin) { ?>
        function addAnotacao() {
            $.ajax({
                url: $('#urlroot').val() + 'empresas/addlegisanotacao/' + $('#empresa-id').val(),
                data: JSON.stringify({anotacao: CKEDITOR.instances.anotacao.getData(), tiponote_id: $('#tipo-anotacao').val(), titulo: $('#titulo-anotacao').val()}),
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                type: 'patch',
                success: function (data) {
                    if (data.retorno) {
                        //fecha a modal
                        $(".modal-anotacao").find(".close").trigger("click");
                        renderAnotacoes();
                        clearFieldsInfo(['tipo-anotacao', 'titulo-anotacao']);
                        refreshTipoNotes();

                        new PNotify({text: 'Anotação adicionada com sucesso.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                    } else {
                        new PNotify({text: 'Não foi possível adicionar essa anotação.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                    }
                },
                error: function (a) {
                    console.log(a);
                }
            });

        }
        function deleteAnotacao(id) {
            $.ajax({url: $('#urlroot').val() + 'empresas/deletelegisanotacao/' + id, dataType: 'json', type: 'post',
                success: function (data) {
                    if (data.retorno) {
                        new PNotify({text: 'A anotação foi removida do sistema.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                        $('#legisanotacao-' + id).remove();// deleta a anotação do html
                    } else {
                        new PNotify({text: 'Não foi possível deletar essa anotação.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                    }
                },
                error: function (a) {
                    console.log(a);
                }
            });
        }
        function refreshTipoNotes() {
            $.ajax({
                url: $('#urlroot').val() + 'assuntos/tiponotesajax/',
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    data = data.retorno;
                    var html = "";
                    $.each(data, function (i, item) {
                        html += '<option value="' + i + '">' + item + '</option>';
                    });
                    $('#tipo-anotacao').html(html);
                    $('#tipo-anotacao').trigger("change");
                },
                error: function (a) {
                    console.log(a);
                }
            });
        }
        function loadDetailsAnotacao(id) {
            current_anotacao = id;
            $.ajax({
                url: $('#urlroot').val() + 'empresas/detailslegisview/' + current_anotacao,
                dataType: 'json',
                success: function (data) {
                    data = data.retorno;
                    if (CKEDITOR.instances.anotacao)
                        CKEDITOR.instances.anotacao.destroy();
                    $('#titulo-anotacao').val(data.titulo);
                    $("#tipo-anotacao option[value='" + data.tipo + "']").attr('selected', true);
                    $("#tipo-anotacao").trigger('change');
                    $('#anotacao').val(data.anotacao);
                    $("#btn-add-anotacao").hide();
                    $("#btn-edit-anotacao").show();
                    CKEDITOR.replace('anotacao');
                },
                error: function (a) {
                    console.log(a);
                }
            });
        }
        function editAnotacao() {
            $.ajax({
                url: $('#urlroot').val() + 'empresas/editlegisanotacao/' + current_anotacao,
                data: JSON.stringify({anotacao: CKEDITOR.instances.anotacao.getData(), tiponote_id: $('#tipo-anotacao').val(), titulo: $('#titulo-anotacao').val()}),
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                type: 'patch',
                success: function (data) {
                    if (data.retorno) {
                        //fecha a modal
                        $(".modal-anotacao").find(".close").trigger("click");
                        renderAnotacoes();
                        clearFieldsInfo(['tipo-anotacao', 'titulo-anotacao']);
                        refreshTipoNotes();

                        new PNotify({text: 'Anotação atualizada com sucesso.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                    } else {
                        new PNotify({text: 'Não foi possível atualizar essa anotação.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                    }
                },
                error: function (a) {
                    console.log(a);
                }
            });
        }
<?php } ?>
    function renderAnotacoes() {
        $.ajax({
            url: $('#urlroot').val() + 'empresas/getalllegisanotacoes/' + $('#empresa-id').val(),
            dataType: 'json',
            success: function (data) {
                data = data.retorno;
                if (data) {
                    $('.legislativo_panel').show();
                    var html = "";
                    $.each(data, function (i, item) {
                        html += "<div id='legisanotacao-" + item.id + "' class='well well-sm col-md-12 col-sm-12 col-xs-12'>"
                                + "     <div class='col-md-12 col-sm-12 col-xs-12'>"
                                + "         <h2 class='green'>"
                                + "             <a href='javascript:void(0);' class='anotacaoitem'><i class='fa fa-chevron-right'></i>  " + item.tiponote.descricao
                                + "              -  " + item.titulo + '</a>';
<?php if ($admin) { ?>
                            html += "              <button type='button' class='btn btn-sm btn-success' style='float:right;padding-top: 2px;' onclick='loadDetailsAnotacao(" + item.id + ")'  data-toggle='modal' data-target='.modal-anotacao'><i class='fa fa-pencil-square-o'></i></button>"
                                    + "              <button type='button' class='btn btn-sm btn-danger' style='float:right;padding-top: 2px;' onclick='if(confirm(\"Tem certeza que deseja deletar essa anotação?\")){deleteAnotacao(" + item.id + ");}'><i class='fa fa-trash'></i></button>";
<?php } ?>
                        html += "         </h2>"
                                + "     </div>"
                                + "     <div class='content_anotacao col-md-12 col-sm-12 col-xs-12' style='display:none;'><div class='ln_solid'></div>" + item.anotacao
                                + "     </div>"
                                + "</div>";
                    });
                    $('#legisanotacoes').html(html);

                    $(".anotacaoitem").click(function () {
                        toggleAnotacao($(this));
                    });
                } else {
                    $('.legislativo_panel').hide();
                }

            },
            error: function (a) {
                console.log(a);
            }
        });

    }
    function toggleAnotacao(anotacao) {
        anotacao.children('i').toggleClass('fa-chevron-right fa-chevron-down');
        anotacao.parent().parent().next('.content_anotacao').toggle();
    }

</script>