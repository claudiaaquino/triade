<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<?= $this->Html->css('/vendors/uploadify/uploadify.css') ?>
<?= $this->Html->css('/vendors/pnotify/dist/pnotify.css'); ?>
<?= $this->Html->script('/vendors/uploadify/jquery.uploadify.min.js', array('inline' => false)); ?>
<?php $session = $this->request->session()->id(); ?>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2> <?= $empresa->razao ? $empresa->razao : $empresa->nome1; ?> - Alteração Cadastral</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Visualizar Tudo'), ['action' => 'view', $empresa->id], ['class' => "btn btn-dark fa fa-file"]) ?>  </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <?= $this->Flash->render() ?>
            <?= $this->Form->create($empresa, ["class" => "form-horizontal form-label-left"]) ?>
            <div class="x_content">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="info" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-bank"></i> Principal</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="cnae" data-toggle="tab" aria-expanded="false" onclick="loadCnaesTable(); loadCnaeSectionList();"><i class="fa fa-file-text-o"></i> CNAES </a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="doc" data-toggle="tab" aria-expanded="false" onclick="loadDocTable();"><i class="fa fa-folder-open"></i> Docs</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content4" role="tab" id="senhas" data-toggle="tab" aria-expanded="false" ><i class="fa fa-chain"></i> Senhas</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content5" role="tab" id="legislativo" data-toggle="tab" aria-expanded="false" ><i class="fa fa-gavel"></i> Legislativo</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content6" role="tab" id="servicos" data-toggle="tab" aria-expanded="false" ><i class="fa fa-suitcase"></i> Serviços</a>
                        </li>

                    </ul>
                    <div id="myTabContent" class="tab-content">

                        <!--TAB INFORMAÇÕES DA EMPRESA-->
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="info">
                            <ul id="tabPrincipal" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#subtab_content1" id="dados" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-database"></i> Dados</a>
                                </li>
                                <li role="presentation" class=""><a href="#subtab_content2" role="tab" id="formacao" data-toggle="tab" aria-expanded="false"><i class="fa fa-book"></i> Formação</a>
                                </li>
                                <li role="presentation" class=""><a href="#subtab_content3" role="tab" id="end" data-toggle="tab" aria-expanded="false"><i class="fa fa-map-marker"></i> Endereço</a>
                                </li>
                                <li role="presentation" class=""><a href="#subtab_content4" role="tab" id="socios" data-toggle="tab" aria-expanded="false"><i class="fa fa-group"></i> <span id="label_empresarios">Sócios</span> </a>
                                </li>
                                <li role="presentation" class=""><a href="#subtab_content5" role="tab" id="funcionarios" data-toggle="tab" aria-expanded="false"><i class="fa fa-group"></i> Funcionários</a>
                                </li>

                            </ul>
                            <div id="tabContentPrincipal" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="subtab_content1" aria-labelledby="dados">
                                    <h2><small>Apenas os campos com * são obrigatórios</small></h2>
                                    <div class="ln_solid"></div>
                                    <?php
                                    $previsao = '';
                                    if ($empresa->solicitacao) {
                                        $previsao = 'Previsão de ';
                                        ?>
                                        <h2>Possíveis nomes da Empresa</h2>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome1">Nome 1<span class="required">*</span>
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <?= $this->Form->input('nome1', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'placeholder' => "Primeiro possível nome"]); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome2">Nome 2<span class="required">*</span>
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <?= $this->Form->input('nome2', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'placeholder' => "Segundo possível nome"]); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome3">Nome 3<span class="required">*</span>
                                                <!--<span class="required">*</span>-->
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <?= $this->Form->input('nome3', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'placeholder' => "Terceiro possível nome"]); ?>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>

                                    <?php } else { ?>
                                        <h2>Dados Principais</h2>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="razao">Nome Empresarial <span class="required">*</span>
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <?= $this->Form->input('razao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fantasia">Nome Fantasia
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <?= $this->Form->input('fantasia', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cnpj">CNPJ
                                                <!--<span class="required">*</span>-->
                                            </label>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <?= $this->Form->input('cnpj', [ "data-inputmask" => "'mask': '99.999.999/9999-99'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="ln_solid"></div>
                                    <h2>Responsável para Contato</h2>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">E-mail <span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('email', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                            <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefone">Telefone
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('telefone', ["data-inputmask" => "'mask': '(99) 9999[9]-9999'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                            <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome_responsavel">Contato Principal
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('nome_responsavel', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cpf">CPF
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('cpf', [ "data-inputmask" => "'mask': '999.999.999-99'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cargo_responsavel">Função
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('cargo_responsavel', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="ln_solid"></div>
                                    <h2>Números de Registro</h2>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="insc_municipal">Inscrição Municipal
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('insc_municipal', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="insc_estadual">Inscrição Estadual
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('insc_estadual', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nire">Nire (Número de Registro na Junta)
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('nire', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_nire">Data do Nire 
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('dt_nire', ['type' => 'text', "data-inputmask" => "'mask': '99/99/99'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mensalidade_base">Valor Base da Mensalidade
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('mensalidade_base', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-7 col-sm-7 col-xs-7 col-md-offset-3">
                                            <button type="submit" class="btn btn-success">Atualizar</button>
                                            <?= $this->Html->link(__('  Visualizar Tudo'), ['action' => 'view', $empresa->id], ['class' => "btn btn-primary"]) ?>  
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <?php if ($empresa->status == 1) { //empresa ativa no sistema ?>
                                                <?= $this->Html->link(__('Desativar no Sistema'), ['action' => 'desativar', $empresa->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja desativar essa empresa?', $empresa->id)]) ?>
                                                <?= $this->Html->link(__('Dar Baixa na Empresa'), ['action' => 'darbaixa', $empresa->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja dar baixa na empresa?', $empresa->id)]) ?>
                                            <?php } else if ($empresa->status == 0) { // empresa apenas desativada no sistema ?>
                                                <?= $this->Html->link(__('Reativar no Sistema'), ['action' => 'reativar', $empresa->id], ['class' => "btn btn-success", 'confirm' => __('Você tem certeza que deseja reativar essa empresa no sistema?', $empresa->id)]) ?>                                            
                                                <?= $this->Html->link(__('Dar Baixa na Empresa'), ['action' => 'darbaixa', $empresa->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja dar baixa na empresa?', $empresa->id)]) ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="subtab_content2" aria-labelledby="formacao">
                                    <h2><small>Apenas os campos com * são obrigatórios</small></h2>

                                    <div class="ln_solid"></div>
                                    <h2>Formação Societária</h2>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-4 col-sm-6 col-xs-12  col-md-offset-3">
                                            <?=
                                            $this->Form->radio('sociedade', [
                                                ['value' => '0', 'text' => 'Empresário', "class" => "form-control col-md-7 col-xs-12"],
                                                ['value' => '1', 'text' => 'Sociedade', "class" => "form-control col-md-7 col-xs-12"]
                                                    ]
                                            );
                                            ?>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="capitalsocial">Capital Social
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('capitalsocial', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'step' => '1000', 'min' => '0']); ?>
                                        </div>
                                    </div>
                                    <div class="form-group sociedade" style="display: none;">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="num_socios">Número de Sócios
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('num_socios', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <h2>Porte da Empresa</h2>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="porte_id">Porte
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('porte_id', [ "options" => $portes, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => '(Selecione uma opção)', "style" => "width: 100%"]); ?>
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <h2>Informações Específicas</h2>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="num_funcionarios"><?= $previsao ?>Nº de Funcionários
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('num_funcionarios', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="faturamento_mensal"> <?= $previsao ?>Faturamento Mensal
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('faturamento_mensal', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="apuracaoforma_id">Forma de Apuração em que se enquadra
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('apuracaoforma_id', ['options' => $apuracaoformas, "class" => "form-control col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%", 'empty' => '(Selecione uma opção)']); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="apuracaoforma_id">Ramo de atuações
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('atuacaoramo_id', ['options' => $atuacaoramos, "class" => "form-control col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%", 'empty' => '(Selecione uma opção)']); ?>
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-3">
                                            <button type="submit" class="btn btn-success">Atualizar</button>
                                            <?= $this->Html->link(__('  Visualizar Tudo'), ['action' => 'view', $empresa->id], ['class' => "btn btn-primary"]) ?>  
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="subtab_content3" aria-labelledby="end">
                                    <h2><small>Apenas os campos com * são obrigatórios</small></h2>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="indicecadastral">Índice Cadastral (IPTU)
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('indicecadastral', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="endereco">Endereço
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('endereco', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_numero">Número
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('end_numero', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_complemento">Complemento
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('end_complemento', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_bairro">Bairro
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('end_bairro', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_cep">CEP
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('end_cep', ["data-inputmask" => "'mask': '99999-999'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="estado_id">Estado
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('estado_id', ['options' => $estados, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => '(Selecione um estado)', 'sel' => $empresa->estado_id, "style" => "width: 100%"]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cidade_id">Cidade
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->input('cidade_id', [/* 'options' => $cidades, */ "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'sel' => $empresa->cidade_id, "style" => "width: 100%"]); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="arealocal"> Área Total do Empreendimento (m²) 
                                        </label>
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <?= $this->Form->input('arealocal', ["class" => "form-control col-md-2 col-xs-12", 'label' => false, 'min' => 0]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="residencia_socio">O local é residência de um dos sócios?
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->checkbox('residencia_socio', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="enderecocorrespondencia">Esse será o endereço para correspondência?
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->checkbox('enderecocorrespondencia', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="enderecodocumento">Esse é o endereço onde ficam os documentos?
                                        </label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <?= $this->Form->checkbox('enderecodocumento', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <!--<button type="submit" class="btn btn-primary">Cancelar</button>-->
                                            <button type="submit" class="btn btn-success">Atualizar</button>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="subtab_content4" aria-labelledby="socios">
                                    <div class="form-group btn-cadastrosocio">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <?= $this->Html->link(__('  Cadastrar'), ['controller' => 'Empresasocios', 'action' => 'add', $empresa->id], ['class' => "btn btn-dark fa fa-file"]) ?>
                                        </div>
                                    </div>
                                    <?php if (!$empresa->empresasocios) { ?>
                                        Não há nenhum empresário cadastrado nessa Empresa
                                    <?php } else { ?>
                                        <div class="table-responsive sociedade-box">
                                            <table class="table table-striped jambo_table bulk_action">
                                                <thead>
                                                    <tr class="headings">
                                                        <th>
                                                            <input type="checkbox" id="check-all" class="flat">
                                                        </th>
                                                        <th scope="col" class="column-title col-nomeempresarios">Nome do Sócio</th>
                                                        <th scope="col" class="column-title">CPF</th>                                
                                                        <th scope="col" class="column-title">Part. Capital Social (%)</th>                                
                                                        <th scope="col" class="column-title">Contribui INSS</th>                                
                                                        <th scope="col" class="column-title">Responsável Receita</th>                                
                                                        <th scope="col" class="column-title">Admin</th>           

                                                        <th class="bulk-actions" colspan="12">
                                                            <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                                        </th>
                                                        <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $cor = 'even';
                                                    foreach ($empresa->empresasocios as $socio) {
                                                        ?>                                
                                                        <tr class="<?= $cor ?> pointer">

                                                            <td class="a-center ">
                                                                <input type="checkbox" class="flat" name="table_records" value="<?= $socio->id ?>">
                                                            </td>

                                                            <td><?= $socio->nome; ?></td>
                                                            <td><?= $socio->cpf; ?></td>
                                                            <td><?= $socio->percentual_capitalsocial; ?>%</td>
                                                            <td><?= $socio->contribuiinss ? 'SIM' : 'NÃO'; ?></td>
                                                            <td><?= $socio->responsavelreceita ? 'SIM' : 'NÃO'; ?></td>
                                                            <td><?= $socio->admin ? 'SIM' : 'NÃO'; ?></td>

                                                            <td  class=" last">
                                                                <div class="btn-group">
                                                                    <?= $this->Html->link(__('Visualizar'), ['controller' => 'Empresasocios', 'action' => 'view', $socio->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                                    <?= $this->Html->link(__('Editar'), ['controller' => 'Empresasocios', 'action' => 'edit', $socio->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Empresasocios', 'action' => 'delete', $socio->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $socio->id)]) ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $cor = $cor == 'even' ? 'odd' : 'even';
                                                    }
                                                    ?>
                                                </tbody>                       
                                            </table>
                                        </div>
                                        <?php
                                        foreach ($empresa->empresasocios as $empresasocio):
                                            if (!empty($empresasocio->documentos)) {
                                                ?>
                                                <div class="x_panel">
                                                    <div class="x_title">
                                                        <h2 class="green"><i class="fa fa-file"></i> Documentos do <?= $empresa->sociedade ? 'Sócio' : 'Empresário' ?>: <?= h($empresasocio->nome) ?> </h2>
                                                        <ul class="nav navbar-right panel_toolbox">
                                                            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                                                        </ul>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="x_content"  style="display: none;" >
                                                        <div class="table-responsive">
                                                            <table class="table table-striped jambo_table bulk_action">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th scope="col"  class="column-title"><?= __('Tipo de Documento') ?></th>
                                                                        <th scope="col"  class="column-title"><?= __('Dt Envio') ?></th>
                                                                        <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    $cor = 'even';
                                                                    foreach ($empresasocio->documentos as $documentos):
                                                                        ?>
                                                                        <tr class="<?= $cor ?> pointer">
                                                                            <td><?= $this->Html->link($documentos->tipodocumento->descricao, "/docs/" . $documentos->file, ['target' => '_blank']) ?></td>
                                                                            <td><?= h($documentos->dt_enviado) ?></td>
                                                                            <td  class=" last">
                                                                                <div class="btn-group">
                                                                                    <?= $this->Html->link(__('Download'), "/docs/" . $documentos->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                                                                    <?php if ($admin) { ?>
                                                                                        <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Documentos', 'action' => 'delete', $documentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $documentos->id)]) ?>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                        $cor = $cor == 'even' ? 'odd' : 'even';
                                                                    endforeach;
                                                                    ?>
                                                                </tbody>                       
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php
                                            }
                                        endforeach;
                                    }
                                    ?>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="subtab_content5" aria-labelledby="funcionarios">
                                    <div class="form-group btn-cadastrofuncionario">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <?= $this->Html->link(__('  Cadastrar Funcionário'), ['controller' => 'Funcionarios', 'action' => 'add', $empresa->id], ['class' => "btn btn-dark fa fa-file"]) ?>
                                        </div>
                                    </div>
                                    <?php if (!$empresa->funcionarios) { ?>
                                        Não há nenhum funcionário cadastrado nessa Empresa
                                    <?php } else { ?>
                                        <div class="table-responsive">
                                            <table class="table table-striped jambo_table bulk_action">
                                                <thead>
                                                    <tr class="headings">
                                                        <th>
                                                            <input type="checkbox" id="check-all" class="flat">
                                                        </th>
                                                        <th scope="col" class="column-title">Funcionário</th>
                                                        <th scope="col" class="column-title">CPF</th>                                         
                                                        <th scope="col" class="column-title">Cargo</th>                                
                                                        <th scope="col" class="column-title">Salário</th>           

                                                        <th class="bulk-actions" colspan="12">
                                                            <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                                        </th>
                                                        <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $cor = 'even';
                                                    foreach ($empresa->funcionarios as $funcionario) {
                                                        ?>                                
                                                        <tr class="<?= $cor ?> pointer">

                                                            <td class="a-center ">
                                                                <input type="checkbox" class="flat" name="table_records" value="<?= $funcionario->id ?>">
                                                            </td>

                                                            <td><?= $funcionario->nome; ?></td>
                                                            <td><?= $funcionario->cpf; ?></td>
                                                            <td><?= $funcionario->cargo ? $funcionario->cargo->descricao : 'Não Informado'; ?></td>
                                                            <td><?= $funcionario->salario ? 'R$ ' . $funcionario->salario : '---'; ?></td>

                                                            <td  class=" last">
                                                                <div class="btn-group">
                                                                    <?= $this->Html->link(__('Visualizar'), ['controller' => 'Funcionarios', 'action' => 'view', $funcionario->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                                    <?= $this->Html->link(__('Editar'), ['controller' => 'Funcionarios', 'action' => 'edit', $funcionario->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Funcionarios', 'action' => 'delete', $funcionario->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $funcionario->id)]) ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $cor = $cor == 'even' ? 'odd' : 'even';
                                                    }
                                                    ?>
                                                </tbody>                       
                                            </table>
                                        </div>
                                        <?php
                                        foreach ($empresa->empresasocios as $empresasocio):
                                            if (!empty($empresasocio->documentos)) {
                                                ?>
                                                <div class="x_panel">
                                                    <div class="x_title">
                                                        <h2 class="green"><i class="fa fa-file"></i> Documentos do <?= $empresa->sociedade ? 'Sócio' : 'Empresário' ?>: <?= h($empresasocio->nome) ?> </h2>
                                                        <ul class="nav navbar-right panel_toolbox">
                                                            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                                                        </ul>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="x_content"  style="display: none;" >
                                                        <div class="table-responsive">
                                                            <table class="table table-striped jambo_table bulk_action">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th scope="col"  class="column-title"><?= __('Tipo de Documento') ?></th>
                                                                        <th scope="col"  class="column-title"><?= __('Dt Envio') ?></th>
                                                                        <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    $cor = 'even';
                                                                    foreach ($empresasocio->documentos as $documentos):
                                                                        ?>
                                                                        <tr class="<?= $cor ?> pointer">
                                                                            <td><?= $this->Html->link($documentos->tipodocumento->descricao, "/docs/" . $documentos->file, ['target' => '_blank']) ?></td>
                                                                            <td><?= h($documentos->dt_enviado) ?></td>
                                                                            <td  class=" last">
                                                                                <div class="btn-group">
                                                                                    <?= $this->Html->link(__('Download'), "/docs/" . $documentos->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                                                                    <?php if ($admin) { ?>
                                                                                        <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Documentos', 'action' => 'delete', $documentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $documentos->id)]) ?>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                        $cor = $cor == 'even' ? 'odd' : 'even';
                                                                    endforeach;
                                                                    ?>
                                                                </tbody>                       
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php
                                            }
                                        endforeach;
                                    }
                                    ?>
                                </div>

                            </div>

                        </div>
                        <!--TAB CNAES DA EMPRESA-->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="cnae">
                            <h3>Atividades</h3>
                            <div class="ln_solid"></div>
                            <div class="form_wizard col-md-5 col-sm-5 col-xs-12" style="height: 100%;">
                                <h2>Inserir Nova Atividade</h2>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="secao">Seção 
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <?= $this->Form->input('cnaesection_id', [ "class" => "form-control", 'label' => false, "style" => "width: 100%"]); ?>
                                    </div>
                                </div>   
                                <div class="form-group" id="divisao-box" >
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="divisao">Divisão 
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12" >
                                        <?= $this->Form->input('cnaedivision_id', [ "class" => "form-control", 'label' => false, "style" => "width: 100%"]); ?>
                                    </div>
                                </div>   
                                <div class="form-group" id="grupo-box">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="grupo">Grupo 
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <?= $this->Form->input('cnaegroup_id', [ "class" => "form-control", 'label' => false, "style" => "width: 100%"]); ?>
                                    </div>
                                </div>   
                                <div class="form-group" id="classe-box">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="classe">Classe
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <?= $this->Form->input('cnaeclasse_id', [ "class" => "form-control", 'label' => false, "style" => "width: 100%"]); ?>
                                    </div>
                                </div>   
                                <div class="form-group quest-box">
                                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="classe">Ativida Exercida no Local?
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="exercida_local" type="checkbox" class="flat"/>
                                    </div>
                                </div>
                                <div class="form-group quest-box">
                                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="classe">Ativida Principal?
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="principal" type="checkbox" class="flat"/>
                                    </div>
                                </div>
                                <div class="form-group quest-box" >
                                    <div class="control-label col-md-3 col-sm-3 col-xs-12">
                                        <!--col-md-offset-6">-->
                                        <?= $this->Form->input('empresa_id', ["type" => "hidden", "value" => $empresa->id]); ?>
                                        <?= $this->Form->button(__('  Inserir'), ['onclick' => 'addCnaeRelation()', "type" => 'button', 'class' => "btn btn-primary"]) ?>
                                    </div>
                                </div>  
                                <div class="ln_solid"></div>
                            </div>  

                            <div class="form_wizard  col-md-7 col-sm-7 col-xs-12">
                                <h2>Atividades Inseridas</h2>
                                <div class="table-responsive">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                            <tr class="headings">
                                                <th scope="col" class="column-title"><?= __('Classe') ?></th>
                                                <th scope="col" class="column-title"><?= __('Atividade') ?></th>  
                                                <th scope="col" class="column-title"><?= __('Exercida no Local') ?></th>  
                                                <th scope="col" class="column-title"><?= __('Atividade Principal') ?></th>  
                                                <th class="bulk-actions" colspan="7">
                                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                                </th>
                                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>

                                            </tr>
                                        </thead>
                                        <tbody id="table-cnaes">
                                            <tr><td>sem registros...</td></tr>    
                                        </tbody>                       
                                    </table>
                                </div>
                            </div>
                            <span class="clearfix"></span>
                            <div class="ln_solid"></div>


                            <h3>Atividades Auxiliares</h3>
                            <div class="ln_solid"></div>
                            <div class="form_wizard col-md-5 col-sm-5 col-xs-12" style="height: 100%;">
                                <!--<h2>Inserir Atividade Auxiliar</h2>-->
                                <div class="form-group">
                                    <div class="col-md-10 col-sm-10 col-xs-12">
                                        <?= $this->Form->input('atividadesauxiliar_id', [ "class" => "form-control", 'label' => false, "style" => "width: 100%"]); ?>
                                    </div>
                                    <div class="control-label col-md-2 col-sm-2 col-xs-12">
                                        <?= $this->Form->button(__('  Inserir'), ['onclick' => 'addAtividadeAuxiliarRelation()', "type" => 'button', 'class' => "btn btn-primary"]) ?>
                                    </div>
                                </div>   
                                <div class="ln_solid"></div>
                            </div>  

                            <div class="form_wizard  col-md-7 col-sm-7 col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                            <tr class="headings">
                                                <th scope="col" class="column-title"><?= __('Atividades Auxiliares Inseridas') ?></th>  
                                                <th class="bulk-actions" colspan="7">
                                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                                </th>
                                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>

                                            </tr>
                                        </thead>
                                        <tbody id="table-atividades">
                                            <tr><td>sem registros...</td></tr>    
                                        </tbody>                       
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!--TAB DOCUMENTOS REFERENTES A ABERTURA DA EMPRESA-->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="doc">
                            <h2>Enviar Novo Documento</h2>
                            <form action="" id="documentos" enctype="multipart/form-data" class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo">Tipo do Documento <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('tipodocumento_id', ['options' => $tipodocumentos, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => 'selecione o tipo de documento', "style" => "width: 100%"]); ?>
                                        <?= $this->Form->input('grupodocumento_id', [ "type" => "hidden", "value" => $grupodocumento_id, 'label' => false, "style" => "width: 100%"]); ?>
                                    </div>
                                </div>                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Nome do arquivo
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('documentos-nome', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                    </div>
                                </div>                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Descrição/Observação
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('documentos-descricao', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                    </div>
                                </div>                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="arquivopath">
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->file('file', ['id' => 'file_upload', 'label' => false]) ?>
                                        <?= $this->Form->button(__('Enviar'), ['onclick' => 'sendDocument()', "type" => 'button', 'class' => "btn btn-round btn-dark"]) ?>
                                    </div>
                                </div>      
                                <div class="form-group" id="queue"></div>
                                <?= $this->Form->input('empresa_id', ["type" => "hidden", "value" => $empresa->id]); ?>
                            </form>
                            <div class="ln_solid"></div>
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th scope="col" class="column-title"><?= __('Dt Enviado') ?></th>
                                            <th scope="col" class="column-title"><?= __('Tipo do Documento') ?></th>
                                            <th scope="col" class="column-title"><?= __('Nome') ?></th>
                                            <th scope="col" class="column-title"><?= __('Descrição') ?></th>
                                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-documentos">
                                        <tr><td>sem registros...</td></tr>  
                                    </tbody>                       
                                </table>
                            </div>
                        </div>

                        <!--TAB SENHAS DE ACESSOS À SISTEMAS EXTERNOS-->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="senhas">
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <?= $this->Html->link(__('  Cadastrar Senha'), ['controller' => 'Acessosexternos', 'action' => 'add', $empresa->id], ['class' => "btn btn-dark fa fa-file"]) ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <?php if (!$acessosexternos) { ?>
                                Não há nenhuma senha cadastrada para essa empresa
                            <?php } else { ?>
                                <div class="table-responsive">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                            <tr class="headings">
                                                <th>
                                                    <input type="checkbox" id="check-all" class="flat">
                                                </th>
                                                <th scope="col" class="column-title">Sistema Externo</th>                                
                                                <th scope="col" class="column-title">Exibir senha para a Empresa?</th>


                                                <th class="bulk-actions" colspan="12">
                                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                                </th>
                                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $cor = 'even';
                                            foreach ($acessosexternos as $acessosexterno) {
                                                ?>                                
                                                <tr class="<?= $cor ?> pointer">

                                                    <td class="a-center ">
                                                        <input type="checkbox" class="flat" name="table_records" value="<?= $acessosexterno->id ?>">
                                                    </td>
                                                    <td><?= $acessosexterno->has('sistemasexterno') ? $this->Html->link($acessosexterno->sistemasexterno->descricao, ['controller' => 'Sistemasexternos', 'action' => 'view', $acessosexterno->sistemasexterno->id]) : '' ?></td>
                                                    <td><?= $acessosexterno->exibecliente ? 'SIM' : 'NÃO'; ?></td>

                                                    <td  class=" last">
                                                        <div class="btn-group">
                                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Acessosexternos', 'action' => 'view', $acessosexterno->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Acessosexternos', 'action' => 'edit', $acessosexterno->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Acessosexternos', 'action' => 'delete', $acessosexterno->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $acessosexterno->id)]) ?>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                $cor = $cor == 'even' ? 'odd' : 'even';
                                            }
                                            ?>
                                        </tbody>                       
                                    </table>
                                </div>
                            <?php } ?>

                        </div>

                        <!--TAB ANOTAÇÕES DO LEGISLATIVO-->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="legislativo">
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <button id='btn-modal-anotacao' type="button" class="btn btn-dark fa fa-plus" data-toggle="modal" data-target=".modal-anotacao"> Adicionar Anotação</button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div id="legisanotacoes" class="col-md-12 col-sm-12 col-xs-12">

                            </div>
                        </div>

                        <!--TAB SERVIÇOS FORNECIDOS PARA A EMPRESA E DEVERES DELA PARA COM A TRIADE-->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="servicos">
                            <div class="col-md-6 col-sm-6 col-xs-12 subtab-servicos">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Serviços prestados atualmente</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>                   
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="form-group">
                                            <?= $this->Form->select('servicos', $servicos, [ 'multiple' => 'checkbox', "class" => "form-control flat col-md-12 col-xs-12"]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12  subtab-deveres" >
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Deveres Mensais da Empresa</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>                   
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <?= $this->Form->select('deveres', $deveres, [ 'multiple' => 'checkbox', "class" => "form-control flat col-md-12 col-xs-12"]); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js', array('inline' => false)); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/js/ajax/cidades.js'); ?>
<?= $this->Html->script('/js/ajax/cnaes.js'); ?>
<?= $this->Html->script('/js/ajax/documentos.js'); ?>
<?= $this->Html->script('/js/ajax/empresas.js'); ?>
<?= $this->Html->script('/vendors/pnotify/dist/pnotify.js'); ?>
<?= $this->Html->script('/vendors/ckeditor/ckeditor.js'); ?>
<script>
    $(document).ready(function () {
        configInicial();
        eventsInicial();
    });
    function configInicial() {

        $(":input").inputmask();
        $("select").select2({placeholder: 'selecione uma opção'});
        $("select").addClass('select2');
        $("#tab_content6 input[type=checkbox]").after('&nbsp;&nbsp;&nbsp;'); // apenas para distanciar o checkbox do texto, pq estava muito perto (com a classe flat)
        $("#tab_content6 input[type=checkbox]").addClass('flat');

        /*-----------------ABA 1 - PRINCIPAL--------------------------------*/
        $('#capitalsocial,#faturamento-mensal,#mensalidade-base').inputmask('decimal', {
            radixPoint: ",",
            groupSeparator: ".",
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
            rightAlign: false,
            onBeforeMask: function (value, opts) {
                return value;
            }});



        /*-----------------ABA 3 - DOCUMENTOS--------------------------------*/
        $('#file_upload').uploadify({
            'formData': {'tipodocumento_id': 0, 'nome': '', 'descricao': '', 'empresa_id': $('#empresa-id').val()},
            'auto': false,
            'swf': '<?= $this->request->webroot ?>vendors/uploadify/uploadify.swf',
            'uploader': $('#urlroot').val() + 'documentos/addajax/<?= $session ?>',
            'buttonText': 'SELECIONAR ARQUIVO',
            'width': 180,
            'onUploadError': function (file, errorCode, errorMsg, errorString) {
                //                $('#table-documentos').html(errorString + '<br>' + errorMsg + '<br>' + errorCode);
            },
            'onUploadSuccess': function (file, data, response) {
                $('#tipodocumento-id').val('');
                $('#documentos-nome').val('');
                $('#documentos-descricao').val('');
                loadDocTable();
            },
            'onUploadStart': function (file) {
                $("#file_upload").uploadify("settings", 'formData', {'tipodocumento_id': $('#tipodocumento-id').val(), 'nome': $('#documentos-nome').val(), 'descricao': $('#documentos-descricao').val()});
            }

        });

        /*-----------------ABA 5 - LEGISLATIVO--------------------------------*/
        $("#tipo-anotacao").select2({dropdownParent: $(".modal-anotacao"), tokenSeparators: [',', ';'], tags: true, placeholder: 'Selecione um tipo ou digite um novo'});

    }
    function eventsInicial() {
        /*-----------------ABA 1 - PRINCIPAL--------------------------------*/
        toggleSociedade();
        $('input[name="sociedade"]:radio').change(function () {
            toggleSociedade();
        });
        /*-----------------ABA 3 - CNAES--------------------------------*/
        loadCnaesTable();
        loadCnaeSectionList();
        loadAtividadesTable();
        loadAtividadesList();
        /*-----------------ABA 5 - LEGISLATIVO--------------------------------*/
        refreshTipoNotes();
        renderAnotacoes();
        $("#btn-add-anotacao").click(function () {
            addAnotacao();
        });
        $("#btn-modal-anotacao").click(function () {
            console.log(CKEDITOR.instances);
            if (CKEDITOR.instances.anotacao)
                CKEDITOR.instances.anotacao.destroy();
            $('#anotacao').val('');
            $("#btn-add-anotacao").show();
            $("#btn-edit-anotacao").hide();
            CKEDITOR.replace('anotacao');
        });
        $("#btn-edit-anotacao").click(function () {
            editAnotacao();
            renderAnotacoes();
        });


        /*-----------------ABA 6 - SERVIÇOS--------------------------------*/
        $('#tab_content6 .subtab-servicos input').on('ifChecked', function (event) {
            var parent = $(this).parent().get(0);
            var checkboxId = parent.getElementsByTagName('input')[0].value;
            addServico(checkboxId);
        });
        $('#tab_content6 .subtab-servicos input').on('ifUnchecked', function (event) {
            var parent = $(this).parent().get(0);
            var checkboxId = parent.getElementsByTagName('input')[0].value;
            removeServico(checkboxId);
        });
        $('#tab_content6 .subtab-deveres input').on('ifChecked', function (event) {
            var parent = $(this).parent().get(0);
            var checkboxId = parent.getElementsByTagName('input')[0].value;
            addDever(checkboxId);
        });
        $('#tab_content6 .subtab-deveres input').on('ifUnchecked', function (event) {
            var parent = $(this).parent().get(0);
            var checkboxId = parent.getElementsByTagName('input')[0].value;
            removeDever(checkboxId);
        });

        /*-----------------ABA 3 - CNAES--------------------------------*/
        loadServicos();
//        loadDeveres();
    }

//______FUNÇÕES ABA LEGISLATIVO___________//
    function toggleSociedade() {
        var formacao = $('input[name="sociedade"]:radio:checked').val();
        if (formacao == '1') {//sociedade
            $('.sociedade').show();
            $('.btn-cadastrosocio').show();
            $('.col-nomeempresarios').html('Nome do Sócio');
            $('#label_empresarios').html('Sócios');
        } else if (formacao == '0') {
            $('.sociedade').hide();
            if ($('.sociedade-box').length > 0) { // se já tiver um empresário cadastrado, então oculta o botão de cadastrar.
                $('.btn-cadastrosocio').hide();
            } else {
                $('.btn-cadastrosocio').show();
            }
            $('.col-nomeempresarios').html('Nome do Empresário');
            $('#label_empresarios').html('Empresário');
        }
    }
    function addAnotacao() {
        $.ajax({
            url: $('#urlroot').val() + 'empresas/addlegisanotacao/' + $('#empresa-id').val(),
            data: JSON.stringify({anotacao: CKEDITOR.instances.anotacao.getData(), tiponote_id: $('#tipo-anotacao').val(), titulo: $('#titulo-anotacao').val()}),
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    //fecha a modal
                    $(".modal-anotacao").find(".close").trigger("click");
                    renderAnotacoes();
                    clearFieldsInfo(['tipo-anotacao', 'titulo-anotacao']);
                    refreshTipoNotes();
                    new PNotify({text: 'Anotação adicionada com sucesso.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possível adicionar essa anotação.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function deleteAnotacao(id) {
        $.ajax({url: $('#urlroot').val() + 'empresas/deletelegisanotacao/' + id, dataType: 'json', type: 'post',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'A anotação foi removida do sistema.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                    $('#legisanotacao-' + id).remove(); // deleta a anotação do html
                } else {
                    new PNotify({text: 'Não foi possível deletar essa anotação.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function refreshTipoNotes() {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/tiponotesajax/',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                data = data.retorno;
                var html = "";
                $.each(data, function (i, item) {
                    html += '<option value="' + i + '">' + item + '</option>';
                });
                $('#tipo-anotacao').html(html);
                $('#tipo-anotacao').trigger("change");
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function loadDetailsAnotacao(id) {
        current_anotacao = id;
        $.ajax({
            url: $('#urlroot').val() + 'empresas/detailslegisview/' + current_anotacao,
            dataType: 'json',
            success: function (data) {
                data = data.retorno;
                if (CKEDITOR.instances.anotacao)
                    CKEDITOR.instances.anotacao.destroy();
                $('#titulo-anotacao').val(data.titulo);
                $("#tipo-anotacao option[value='" + data.tipo + "']").attr('selected', true);
                $("#tipo-anotacao").trigger('change');
                $('#anotacao').val(data.anotacao);
                $("#btn-add-anotacao").hide();
                $("#btn-edit-anotacao").show();
                CKEDITOR.replace('anotacao');
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function editAnotacao() {
        $.ajax({
            url: $('#urlroot').val() + 'empresas/editlegisanotacao/' + current_anotacao,
            data: JSON.stringify({anotacao: CKEDITOR.instances.anotacao.getData(), tiponote_id: $('#tipo-anotacao').val(), titulo: $('#titulo-anotacao').val()}),
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    //fecha a modal
                    $(".modal-anotacao").find(".close").trigger("click");
                    renderAnotacoes();
                    clearFieldsInfo(['tipo-anotacao', 'titulo-anotacao']);
                    refreshTipoNotes();
                    new PNotify({text: 'Anotação atualizada com sucesso.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possível atualizar essa anotação.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function renderAnotacoes() {
        $.ajax({
            url: $('#urlroot').val() + 'empresas/getalllegisanotacoes/' + $('#empresa-id').val(),
            dataType: 'json',
            success: function (data) {
                data = data.retorno;
                if (data) {
                    var html = "";
                    $.each(data, function (i, item) {
                        html += "<div id='legisanotacao-" + item.id + "' class='well well-sm col-md-12 col-sm-12 col-xs-12'>"
                                + "     <div class='col-md-12 col-sm-12 col-xs-12'>"
                                + "         <h2 class='green'>"
                                + "             <a href='javascript:void(0);' class='anotacaoitem'><i class='fa fa-chevron-right'></i>  " + item.tiponote.descricao
                                + "              -  " + item.titulo + '</a>'
                                + "              <button type='button' class='btn btn-sm btn-success' style='float:right;padding-top: 2px;' onclick='loadDetailsAnotacao(" + item.id + ")'  data-toggle='modal' data-target='.modal-anotacao'><i class='fa fa-pencil-square-o'></i></button>"
                                + "              <button type='button' class='btn btn-sm btn-danger' style='float:right;padding-top: 2px;' onclick='if(confirm(\"Tem certeza que deseja deletar essa anotação?\")){deleteAnotacao(" + item.id + ");}'><i class='fa fa-trash'></i></button>"
                                + "         </h2>"
                                + "     </div>"
                                + "     <div class='content_anotacao col-md-12 col-sm-12 col-xs-12' style='display:none;'><div class='ln_solid'></div>" + item.anotacao
                                + "     </div>"
                                + "</div>";
                    });
                    $('#legisanotacoes').html(html);
                    $(".anotacaoitem").click(function () {
                        toggleAnotacao($(this));
                    });
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function toggleAnotacao(anotacao) {
        anotacao.children('i').toggleClass('fa-chevron-right fa-chevron-down');
        anotacao.parent().parent().next('.content_anotacao').toggle();
    }


//______FUNÇÕES ABA SERVIÇOS___________//
    function addServico(id) {
        $.ajax({
            url: $('#urlroot').val() + 'empresas/addservico/' + $('#empresa-id').val(),
            data: '&tiposervico_id=' + id,
            dataType: 'json',
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'Serviço Adicionado para essa Empresa', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possivel adicionar esse serviço.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function removeServico(id) {
        $.ajax({
            url: $('#urlroot').val() + 'empresas/removeservico/' + $('#empresa-id').val(),
            data: '&tiposervico_id=' + id,
            dataType: 'json',
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'Serviço Removido dessa Empresa', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possivel remover esse serviço', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function loadServicos() {
        $.ajax({
            url: $('#urlroot').val() + 'empresas/getservicos/' + $('#empresa-id').val(),
            dataType: 'json',
            success: function (data) {
                if (data.retorno) {
                    $.each(data.retorno, function (i, servico) {
                        $('#servicos-' + servico.tiposervico_id).prop('checked', true);
                        $('#servicos-' + servico.tiposervico_id).parent().addClass('checked');
                    });
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function addDever(id) {
        $.ajax({
            url: $('#urlroot').val() + 'empresas/adddever/' + $('#empresa-id').val(),
            data: '&tipodever_id=' + id,
            dataType: 'json',
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'Dever Adicionado para essa Empresa', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possivel adicionar esse dever para a empresa.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function removeDever(id) {
        $.ajax({
            url: $('#urlroot').val() + 'empresas/removedever/' + $('#empresa-id').val(),
            data: '&tipodever_id=' + id,
            dataType: 'json',
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'Dever Removido dessa Empresa', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possivel remover esse dever da empresa', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function loadDeveres() {
        $.ajax({
            url: $('#urlroot').val() + 'empresas/getdeveres/' + $('#empresa-id').val(),
            dataType: 'json',
            success: function (data) {
                if (data.retorno) {
                    $.each(data.retorno, function (i, dever) {
                        $('#deveres-' + dever.tipodever_id).prop('checked', true);
                        $('#deveres-' + dever.tipodever_id).parent().addClass('checked');
                    });
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
</script>

<!--CADASTRAR ANOTAÇÕES-->
<div class="modal fade modal-anotacao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Nova Anotação</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->input('tipo-anotacao', ['options' => [], "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'style' => 'width:100%', 'empty' => true]); ?>
                <div class="clearfix"></div><br/>
                <?= $this->Form->input('titulo-anotacao', ['type' => 'text', "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'placeholder' => 'Título da Anotação']); ?>
                <div class="clearfix"></div><br/>
                <?= $this->Form->input('anotacao', ['type' => 'textarea', "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'placeholder' => 'Digite a nova anotação aqui...']); ?>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id='btn-add-anotacao' class="btn btn-primary">Adicionar</button>
                <button type="button" id='btn-edit-anotacao' class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </div>

</div>
