<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i><?= __('Estados') ?> - <?= h($estado->estado_sigla) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Estado Sigla') ?></p>
                                    <p><?= $estado->estado_sigla ? $estado->estado_sigla : 'Não Informado'; ?></p>

                                                                                                                    <p class="title"><?= __('Estado Nome') ?></p>
                                    <p><?= $estado->estado_nome ? $estado->estado_nome : 'Não Informado'; ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $estado->id ? $this->Number->format($estado->id) : 'Não Informado'; ?></p>

                                                    
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $estado->status ? $this->Number->format($estado->status) : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Ultdata') ?></p>
                                <p><?= $estado->ultdata ? $estado->ultdata : 'Não Informado'; ?></p>

                                                                                                                        </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $estado->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $estado->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $estado->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
                    <?php if (!empty($estado->cidades)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Cidades Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Nome') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Longitude') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Latitude') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Classe') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Ultdata') ?></th>
                                                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($estado->cidades as $cidades):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                                                    <td><?= h($cidades->id) ?></td>
                                                                    <td><?= h($cidades->estado_id) ?></td>
                                                                    <td><?= h($cidades->cidade_nome) ?></td>
                                                                    <td><?= h($cidades->cidade_longitude) ?></td>
                                                                    <td><?= h($cidades->cidade_latitude) ?></td>
                                                                    <td><?= h($cidades->cidade_classe) ?></td>
                                                                    <td><?= h($cidades->status) ?></td>
                                                                    <td><?= h($cidades->ultdata) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Cidades', 'action' => 'view', $cidades->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Cidades', 'action' => 'edit', $cidades->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Cidades', 'action' => 'delete', $cidades->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $cidades->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
                    <?php if (!empty($estado->clientes)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Clientes Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Empresa Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnpj') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cpf') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Telefone') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Email') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Endereco') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Numero') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Complemento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Bairro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Cep') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome Responsavel') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cargo Responsavel') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Fields Updated') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($estado->clientes as $clientes):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                                                    <td><?= h($clientes->id) ?></td>
                                                                    <td><?= h($clientes->empresa_id) ?></td>
                                                                    <td><?= h($clientes->cnpj) ?></td>
                                                                    <td><?= h($clientes->cpf) ?></td>
                                                                    <td><?= h($clientes->nome) ?></td>
                                                                    <td><?= h($clientes->telefone) ?></td>
                                                                    <td><?= h($clientes->email) ?></td>
                                                                    <td><?= h($clientes->endereco) ?></td>
                                                                    <td><?= h($clientes->end_numero) ?></td>
                                                                    <td><?= h($clientes->end_complemento) ?></td>
                                                                    <td><?= h($clientes->end_bairro) ?></td>
                                                                    <td><?= h($clientes->end_cep) ?></td>
                                                                    <td><?= h($clientes->estado_id) ?></td>
                                                                    <td><?= h($clientes->cidade_id) ?></td>
                                                                    <td><?= h($clientes->nome_responsavel) ?></td>
                                                                    <td><?= h($clientes->cargo_responsavel) ?></td>
                                                                    <td><?= h($clientes->dt_cadastro) ?></td>
                                                                    <td><?= h($clientes->user_id) ?></td>
                                                                    <td><?= h($clientes->last_fields_updated) ?></td>
                                                                    <td><?= h($clientes->last_update) ?></td>
                                                                    <td><?= h($clientes->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Clientes', 'action' => 'view', $clientes->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Clientes', 'action' => 'edit', $clientes->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Clientes', 'action' => 'delete', $clientes->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $clientes->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
                    <?php if (!empty($estado->contabancarias)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Contabancarias Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Empresa Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cliente Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Fornecedor Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Banco Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Operacao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Valorcaixa') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Agencia') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Ag Digito') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Conta') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Co Digito') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Vencimento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cpf') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Endereco') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Numero') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Complemento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Bairro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Cep') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($estado->contabancarias as $contabancarias):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                                                    <td><?= h($contabancarias->id) ?></td>
                                                                    <td><?= h($contabancarias->empresa_id) ?></td>
                                                                    <td><?= h($contabancarias->cliente_id) ?></td>
                                                                    <td><?= h($contabancarias->fornecedor_id) ?></td>
                                                                    <td><?= h($contabancarias->user_id) ?></td>
                                                                    <td><?= h($contabancarias->banco_id) ?></td>
                                                                    <td><?= h($contabancarias->operacao) ?></td>
                                                                    <td><?= h($contabancarias->valorcaixa) ?></td>
                                                                    <td><?= h($contabancarias->agencia) ?></td>
                                                                    <td><?= h($contabancarias->ag_digito) ?></td>
                                                                    <td><?= h($contabancarias->conta) ?></td>
                                                                    <td><?= h($contabancarias->co_digito) ?></td>
                                                                    <td><?= h($contabancarias->dt_vencimento) ?></td>
                                                                    <td><?= h($contabancarias->cpf) ?></td>
                                                                    <td><?= h($contabancarias->nome) ?></td>
                                                                    <td><?= h($contabancarias->endereco) ?></td>
                                                                    <td><?= h($contabancarias->end_numero) ?></td>
                                                                    <td><?= h($contabancarias->end_complemento) ?></td>
                                                                    <td><?= h($contabancarias->end_bairro) ?></td>
                                                                    <td><?= h($contabancarias->end_cep) ?></td>
                                                                    <td><?= h($contabancarias->estado_id) ?></td>
                                                                    <td><?= h($contabancarias->cidade_id) ?></td>
                                                                    <td><?= h($contabancarias->dt_cadastro) ?></td>
                                                                    <td><?= h($contabancarias->last_update) ?></td>
                                                                    <td><?= h($contabancarias->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Contabancarias', 'action' => 'view', $contabancarias->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Contabancarias', 'action' => 'edit', $contabancarias->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Contabancarias', 'action' => 'delete', $contabancarias->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $contabancarias->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
                    <?php if (!empty($estado->empresas)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Empresas Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnpj') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnpj Nf') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cpf') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Razao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Fantasia') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnae Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Telefone') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Fax') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Email') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Email Nfe') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Endereco') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Numero') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Complemento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Bairro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Cep') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome Responsavel') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cargo Responsavel') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Num Funcionarios') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Solicitacao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Solicitacao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Num Step') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome1') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome2') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome3') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Num Socios') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Faturamento Mensal') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Faturamento Anual') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Porte Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Indicecadastral') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Capitalsocial') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Sociedade') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Residencia Socio') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Arealocal') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Opcaosimplesnacional') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Enderecocorrespondencia') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Enderecodocumento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Apuracaoforma Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Atuacaoramo Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Insc Municipal') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Insc Estadual') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nire') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Nire') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Fields Updated') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($estado->empresas as $empresas):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                                                    <td><?= h($empresas->id) ?></td>
                                                                    <td><?= h($empresas->cnpj) ?></td>
                                                                    <td><?= h($empresas->cnpj_nf) ?></td>
                                                                    <td><?= h($empresas->cpf) ?></td>
                                                                    <td><?= h($empresas->razao) ?></td>
                                                                    <td><?= h($empresas->fantasia) ?></td>
                                                                    <td><?= h($empresas->cnae_id) ?></td>
                                                                    <td><?= h($empresas->telefone) ?></td>
                                                                    <td><?= h($empresas->fax) ?></td>
                                                                    <td><?= h($empresas->email) ?></td>
                                                                    <td><?= h($empresas->email_nfe) ?></td>
                                                                    <td><?= h($empresas->endereco) ?></td>
                                                                    <td><?= h($empresas->end_numero) ?></td>
                                                                    <td><?= h($empresas->end_complemento) ?></td>
                                                                    <td><?= h($empresas->end_bairro) ?></td>
                                                                    <td><?= h($empresas->end_cep) ?></td>
                                                                    <td><?= h($empresas->estado_id) ?></td>
                                                                    <td><?= h($empresas->cidade_id) ?></td>
                                                                    <td><?= h($empresas->nome_responsavel) ?></td>
                                                                    <td><?= h($empresas->cargo_responsavel) ?></td>
                                                                    <td><?= h($empresas->num_funcionarios) ?></td>
                                                                    <td><?= h($empresas->solicitacao) ?></td>
                                                                    <td><?= h($empresas->dt_solicitacao) ?></td>
                                                                    <td><?= h($empresas->num_step) ?></td>
                                                                    <td><?= h($empresas->user_id) ?></td>
                                                                    <td><?= h($empresas->nome1) ?></td>
                                                                    <td><?= h($empresas->nome2) ?></td>
                                                                    <td><?= h($empresas->nome3) ?></td>
                                                                    <td><?= h($empresas->num_socios) ?></td>
                                                                    <td><?= h($empresas->faturamento_mensal) ?></td>
                                                                    <td><?= h($empresas->faturamento_anual) ?></td>
                                                                    <td><?= h($empresas->porte_id) ?></td>
                                                                    <td><?= h($empresas->indicecadastral) ?></td>
                                                                    <td><?= h($empresas->capitalsocial) ?></td>
                                                                    <td><?= h($empresas->sociedade) ?></td>
                                                                    <td><?= h($empresas->residencia_socio) ?></td>
                                                                    <td><?= h($empresas->arealocal) ?></td>
                                                                    <td><?= h($empresas->opcaosimplesnacional) ?></td>
                                                                    <td><?= h($empresas->enderecocorrespondencia) ?></td>
                                                                    <td><?= h($empresas->enderecodocumento) ?></td>
                                                                    <td><?= h($empresas->apuracaoforma_id) ?></td>
                                                                    <td><?= h($empresas->atuacaoramo_id) ?></td>
                                                                    <td><?= h($empresas->insc_municipal) ?></td>
                                                                    <td><?= h($empresas->insc_estadual) ?></td>
                                                                    <td><?= h($empresas->nire) ?></td>
                                                                    <td><?= h($empresas->dt_nire) ?></td>
                                                                    <td><?= h($empresas->last_fields_updated) ?></td>
                                                                    <td><?= h($empresas->last_update) ?></td>
                                                                    <td><?= h($empresas->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Empresas', 'action' => 'view', $empresas->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Empresas', 'action' => 'edit', $empresas->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Empresas', 'action' => 'delete', $empresas->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $empresas->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
                    <?php if (!empty($estado->empresasocios)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Empresasocios Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Empresa Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cargo Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Nascimento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cpf') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Email') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Endereco') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Numero') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Complemento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Bairro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Cep') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Telefone Residencial') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Telefone Celular') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estadocivil Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Comunhaoregime Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Sexo Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Ctps') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Ctps Serie') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnh') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnh Dt Habilitacao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnh Dt Vencimento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Rg') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Rg Estado') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Rg Expedidor') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Rg Dt Expedicao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Militar Numero') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Militar Expedidor') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Militar Serie') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Eleitor Numero') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Eleitor Zona') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Eleitor Secao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Eleitor Dt Emissao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Pai Nome') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Mae Nome') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Percentual Capitalsocial') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Contribuiinss') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Empresario') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Responsavelreceita') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Certificadodigital Pessoafisica') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Token A3') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Admin') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Updated Fields') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($estado->empresasocios as $empresasocios):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                                                    <td><?= h($empresasocios->id) ?></td>
                                                                    <td><?= h($empresasocios->user_id) ?></td>
                                                                    <td><?= h($empresasocios->empresa_id) ?></td>
                                                                    <td><?= h($empresasocios->cargo_id) ?></td>
                                                                    <td><?= h($empresasocios->nome) ?></td>
                                                                    <td><?= h($empresasocios->dt_nascimento) ?></td>
                                                                    <td><?= h($empresasocios->cpf) ?></td>
                                                                    <td><?= h($empresasocios->email) ?></td>
                                                                    <td><?= h($empresasocios->endereco) ?></td>
                                                                    <td><?= h($empresasocios->end_numero) ?></td>
                                                                    <td><?= h($empresasocios->end_complemento) ?></td>
                                                                    <td><?= h($empresasocios->end_bairro) ?></td>
                                                                    <td><?= h($empresasocios->end_cep) ?></td>
                                                                    <td><?= h($empresasocios->estado_id) ?></td>
                                                                    <td><?= h($empresasocios->cidade_id) ?></td>
                                                                    <td><?= h($empresasocios->telefone_residencial) ?></td>
                                                                    <td><?= h($empresasocios->telefone_celular) ?></td>
                                                                    <td><?= h($empresasocios->estadocivil_id) ?></td>
                                                                    <td><?= h($empresasocios->comunhaoregime_id) ?></td>
                                                                    <td><?= h($empresasocios->sexo_id) ?></td>
                                                                    <td><?= h($empresasocios->ctps) ?></td>
                                                                    <td><?= h($empresasocios->ctps_serie) ?></td>
                                                                    <td><?= h($empresasocios->cnh) ?></td>
                                                                    <td><?= h($empresasocios->cnh_dt_habilitacao) ?></td>
                                                                    <td><?= h($empresasocios->cnh_dt_vencimento) ?></td>
                                                                    <td><?= h($empresasocios->rg) ?></td>
                                                                    <td><?= h($empresasocios->rg_estado) ?></td>
                                                                    <td><?= h($empresasocios->rg_expedidor) ?></td>
                                                                    <td><?= h($empresasocios->rg_dt_expedicao) ?></td>
                                                                    <td><?= h($empresasocios->militar_numero) ?></td>
                                                                    <td><?= h($empresasocios->militar_expedidor) ?></td>
                                                                    <td><?= h($empresasocios->militar_serie) ?></td>
                                                                    <td><?= h($empresasocios->eleitor_numero) ?></td>
                                                                    <td><?= h($empresasocios->eleitor_zona) ?></td>
                                                                    <td><?= h($empresasocios->eleitor_secao) ?></td>
                                                                    <td><?= h($empresasocios->eleitor_dt_emissao) ?></td>
                                                                    <td><?= h($empresasocios->pai_nome) ?></td>
                                                                    <td><?= h($empresasocios->mae_nome) ?></td>
                                                                    <td><?= h($empresasocios->percentual_capitalsocial) ?></td>
                                                                    <td><?= h($empresasocios->contribuiinss) ?></td>
                                                                    <td><?= h($empresasocios->empresario) ?></td>
                                                                    <td><?= h($empresasocios->responsavelreceita) ?></td>
                                                                    <td><?= h($empresasocios->certificadodigital_pessoafisica) ?></td>
                                                                    <td><?= h($empresasocios->token_a3) ?></td>
                                                                    <td><?= h($empresasocios->admin) ?></td>
                                                                    <td><?= h($empresasocios->dt_cadastro) ?></td>
                                                                    <td><?= h($empresasocios->last_updated_fields) ?></td>
                                                                    <td><?= h($empresasocios->last_update) ?></td>
                                                                    <td><?= h($empresasocios->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Empresasocios', 'action' => 'view', $empresasocios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Empresasocios', 'action' => 'edit', $empresasocios->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Empresasocios', 'action' => 'delete', $empresasocios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $empresasocios->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
                    <?php if (!empty($estado->fornecedores)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Fornecedores Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Empresa Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnpj') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cpf') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Telefone') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Email') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Endereco') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Numero') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Complemento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Bairro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Cep') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome Responsavel') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cargo Responsavel') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Fields Updated') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($estado->fornecedores as $fornecedores):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                                                    <td><?= h($fornecedores->id) ?></td>
                                                                    <td><?= h($fornecedores->empresa_id) ?></td>
                                                                    <td><?= h($fornecedores->cnpj) ?></td>
                                                                    <td><?= h($fornecedores->cpf) ?></td>
                                                                    <td><?= h($fornecedores->nome) ?></td>
                                                                    <td><?= h($fornecedores->telefone) ?></td>
                                                                    <td><?= h($fornecedores->email) ?></td>
                                                                    <td><?= h($fornecedores->endereco) ?></td>
                                                                    <td><?= h($fornecedores->end_numero) ?></td>
                                                                    <td><?= h($fornecedores->end_complemento) ?></td>
                                                                    <td><?= h($fornecedores->end_bairro) ?></td>
                                                                    <td><?= h($fornecedores->end_cep) ?></td>
                                                                    <td><?= h($fornecedores->estado_id) ?></td>
                                                                    <td><?= h($fornecedores->cidade_id) ?></td>
                                                                    <td><?= h($fornecedores->nome_responsavel) ?></td>
                                                                    <td><?= h($fornecedores->cargo_responsavel) ?></td>
                                                                    <td><?= h($fornecedores->dt_cadastro) ?></td>
                                                                    <td><?= h($fornecedores->user_id) ?></td>
                                                                    <td><?= h($fornecedores->last_fields_updated) ?></td>
                                                                    <td><?= h($fornecedores->last_update) ?></td>
                                                                    <td><?= h($fornecedores->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Fornecedores', 'action' => 'view', $fornecedores->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Fornecedores', 'action' => 'edit', $fornecedores->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Fornecedores', 'action' => 'delete', $fornecedores->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $fornecedores->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
                    <?php if (!empty($estado->funcionarios)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Funcionarios Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Empresa Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Num Contrato') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cargo Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Nascimento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cpf') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Email') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Endereco') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Numero') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Complemento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Bairro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Cep') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Telefone Residencial') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Telefone Celular') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estadocivil Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Sexo Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cpf Responsavel') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Telefone Responsavel') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Ctps') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Ctps Serie') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnh') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnh Dt Habilitacao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnh Dt Vencimento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Rg') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Rg Estado') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Rg Expedidor') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Rg Dt Expedicao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Militar Numero') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Militar Expedidor') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Militar Serie') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Eleitor Numero') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Eleitor Zona') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Eleitor Secao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Eleitor Dt Emissao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Pai Nome') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Mae Nome') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Salario') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Aux Transporte') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Aux Alimentacao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Flag Deficiente') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Desc Deficiencia') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro Sistema') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Contrato Efetivado') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Demissao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Demissao Motivo') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Updated Fields') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($estado->funcionarios as $funcionarios):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                                                    <td><?= h($funcionarios->id) ?></td>
                                                                    <td><?= h($funcionarios->user_id) ?></td>
                                                                    <td><?= h($funcionarios->empresa_id) ?></td>
                                                                    <td><?= h($funcionarios->num_contrato) ?></td>
                                                                    <td><?= h($funcionarios->cargo_id) ?></td>
                                                                    <td><?= h($funcionarios->nome) ?></td>
                                                                    <td><?= h($funcionarios->dt_nascimento) ?></td>
                                                                    <td><?= h($funcionarios->cpf) ?></td>
                                                                    <td><?= h($funcionarios->email) ?></td>
                                                                    <td><?= h($funcionarios->endereco) ?></td>
                                                                    <td><?= h($funcionarios->end_numero) ?></td>
                                                                    <td><?= h($funcionarios->end_complemento) ?></td>
                                                                    <td><?= h($funcionarios->end_bairro) ?></td>
                                                                    <td><?= h($funcionarios->end_cep) ?></td>
                                                                    <td><?= h($funcionarios->estado_id) ?></td>
                                                                    <td><?= h($funcionarios->cidade_id) ?></td>
                                                                    <td><?= h($funcionarios->telefone_residencial) ?></td>
                                                                    <td><?= h($funcionarios->telefone_celular) ?></td>
                                                                    <td><?= h($funcionarios->estadocivil_id) ?></td>
                                                                    <td><?= h($funcionarios->sexo_id) ?></td>
                                                                    <td><?= h($funcionarios->cpf_responsavel) ?></td>
                                                                    <td><?= h($funcionarios->telefone_responsavel) ?></td>
                                                                    <td><?= h($funcionarios->ctps) ?></td>
                                                                    <td><?= h($funcionarios->ctps_serie) ?></td>
                                                                    <td><?= h($funcionarios->cnh) ?></td>
                                                                    <td><?= h($funcionarios->cnh_dt_habilitacao) ?></td>
                                                                    <td><?= h($funcionarios->cnh_dt_vencimento) ?></td>
                                                                    <td><?= h($funcionarios->rg) ?></td>
                                                                    <td><?= h($funcionarios->rg_estado) ?></td>
                                                                    <td><?= h($funcionarios->rg_expedidor) ?></td>
                                                                    <td><?= h($funcionarios->rg_dt_expedicao) ?></td>
                                                                    <td><?= h($funcionarios->militar_numero) ?></td>
                                                                    <td><?= h($funcionarios->militar_expedidor) ?></td>
                                                                    <td><?= h($funcionarios->militar_serie) ?></td>
                                                                    <td><?= h($funcionarios->eleitor_numero) ?></td>
                                                                    <td><?= h($funcionarios->eleitor_zona) ?></td>
                                                                    <td><?= h($funcionarios->eleitor_secao) ?></td>
                                                                    <td><?= h($funcionarios->eleitor_dt_emissao) ?></td>
                                                                    <td><?= h($funcionarios->pai_nome) ?></td>
                                                                    <td><?= h($funcionarios->mae_nome) ?></td>
                                                                    <td><?= h($funcionarios->salario) ?></td>
                                                                    <td><?= h($funcionarios->aux_transporte) ?></td>
                                                                    <td><?= h($funcionarios->aux_alimentacao) ?></td>
                                                                    <td><?= h($funcionarios->flag_deficiente) ?></td>
                                                                    <td><?= h($funcionarios->desc_deficiencia) ?></td>
                                                                    <td><?= h($funcionarios->dt_cadastro_sistema) ?></td>
                                                                    <td><?= h($funcionarios->dt_contrato_efetivado) ?></td>
                                                                    <td><?= h($funcionarios->dt_demissao) ?></td>
                                                                    <td><?= h($funcionarios->demissao_motivo) ?></td>
                                                                    <td><?= h($funcionarios->last_updated_fields) ?></td>
                                                                    <td><?= h($funcionarios->last_update) ?></td>
                                                                    <td><?= h($funcionarios->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Funcionarios', 'action' => 'view', $funcionarios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Funcionarios', 'action' => 'edit', $funcionarios->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Funcionarios', 'action' => 'delete', $funcionarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $funcionarios->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
                    <?php if (!empty($estado->telaquestionarios)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Telaquestionarios Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Telaquestionario Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Assunto Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Lei Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tela Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tipoaction Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tiposervico Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Pergunta') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Resposta') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Exigeconfirmacao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($estado->telaquestionarios as $telaquestionarios):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                                                    <td><?= h($telaquestionarios->id) ?></td>
                                                                    <td><?= h($telaquestionarios->telaquestionario_id) ?></td>
                                                                    <td><?= h($telaquestionarios->assunto_id) ?></td>
                                                                    <td><?= h($telaquestionarios->estado_id) ?></td>
                                                                    <td><?= h($telaquestionarios->cidade_id) ?></td>
                                                                    <td><?= h($telaquestionarios->lei_id) ?></td>
                                                                    <td><?= h($telaquestionarios->tela_id) ?></td>
                                                                    <td><?= h($telaquestionarios->tipoaction_id) ?></td>
                                                                    <td><?= h($telaquestionarios->tiposervico_id) ?></td>
                                                                    <td><?= h($telaquestionarios->pergunta) ?></td>
                                                                    <td><?= h($telaquestionarios->resposta) ?></td>
                                                                    <td><?= h($telaquestionarios->exigeconfirmacao) ?></td>
                                                                    <td><?= h($telaquestionarios->user_id) ?></td>
                                                                    <td><?= h($telaquestionarios->dt_cadastro) ?></td>
                                                                    <td><?= h($telaquestionarios->last_update) ?></td>
                                                                    <td><?= h($telaquestionarios->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Telaquestionarios', 'action' => 'view', $telaquestionarios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Telaquestionarios', 'action' => 'edit', $telaquestionarios->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Telaquestionarios', 'action' => 'delete', $telaquestionarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $telaquestionarios->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
            </div>
</div>


