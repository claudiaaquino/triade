<div class="page-title">
    <div class="title_left">
        <h3><?= __('Contratos') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($contrato->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Tiposervico') ?></p>
                                    <p><?= $contrato->has('tiposervico') ? $this->Html->link($contrato->tiposervico->descricao, ['controller' => 'Tiposervicos', 'action' => 'view', $contrato->tiposervico->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('User') ?></p>
                                    <p><?= $contrato->has('user') ? $this->Html->link($contrato->user->nome, ['controller' => 'Users', 'action' => 'view', $contrato->user->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Filecontrato') ?></p>
                                    <p><?= h($contrato->filecontrato) ?></p>
                                    </tr>
                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $this->Number->format($contrato->id) ?></p>

                                                    
                        <p class="title"><?= __('Valor Servico') ?></p>
                                <p><?= $this->Number->format($contrato->valor_servico) ?></p>

                                                                                                                                
                        <p class="title"><?= __('Dt Contrato') ?></p>
                                <p><?= h($contrato->dt_contrato) ?></p>

                                                                                                                                
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $contrato->status ? __('Ativo') : __('Desativado'); ?></p>

                                                                                                    <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
</div>


