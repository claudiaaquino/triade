<div class="page-title">
    <div class="title_left">
        <h3><?= __('Funcionarioescolaridades') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($funcionarioescolaridade->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Funcionario') ?></p>
                                    <p><?= $funcionarioescolaridade->has('funcionario') ? $this->Html->link($funcionarioescolaridade->funcionario->nome, ['controller' => 'Funcionarios', 'action' => 'view', $funcionarioescolaridade->funcionario->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Nivelescolaridade') ?></p>
                                    <p><?= $funcionarioescolaridade->has('nivelescolaridade') ? $this->Html->link($funcionarioescolaridade->nivelescolaridade->descricao, ['controller' => 'Nivelescolaridades', 'action' => 'view', $funcionarioescolaridade->nivelescolaridade->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Curso') ?></p>
                                    <p><?= $funcionarioescolaridade->has('curso') ? $this->Html->link($funcionarioescolaridade->curso->nome, ['controller' => 'Cursos', 'action' => 'view', $funcionarioescolaridade->curso->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Escola') ?></p>
                                    <p><?= $funcionarioescolaridade->has('escola') ? $this->Html->link($funcionarioescolaridade->escola->nome, ['controller' => 'Escolas', 'action' => 'view', $funcionarioescolaridade->escola->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Escolacampus') ?></p>
                                    <p><?= $funcionarioescolaridade->has('escolacampus') ? $this->Html->link($funcionarioescolaridade->escolacampus->nome, ['controller' => 'Escolacampus', 'action' => 'view', $funcionarioescolaridade->escolacampus->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Matricula') ?></p>
                                    <p><?= h($funcionarioescolaridade->matricula) ?></p>
                                    </tr>
                                                                                                                    <p class="title"><?= __('Turno') ?></p>
                                    <p><?= $funcionarioescolaridade->has('turno') ? $this->Html->link($funcionarioescolaridade->turno->descricao, ['controller' => 'Turnos', 'action' => 'view', $funcionarioescolaridade->turno->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Last Updated Fields') ?></p>
                                    <p><?= h($funcionarioescolaridade->last_updated_fields) ?></p>
                                    </tr>
                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $this->Number->format($funcionarioescolaridade->id) ?></p>

                                                    
                        <p class="title"><?= __('Periodo') ?></p>
                                <p><?= $this->Number->format($funcionarioescolaridade->periodo) ?></p>

                                                    
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $this->Number->format($funcionarioescolaridade->status) ?></p>

                                                                                                                                
                        <p class="title"><?= __('Formatura') ?></p>
                                <p><?= h($funcionarioescolaridade->formatura) ?></p>

                                                    
                        <p class="title"><?= __('Inicio') ?></p>
                                <p><?= h($funcionarioescolaridade->inicio) ?></p>

                                                    
                        <p class="title"><?= __('Dt Cadastro') ?></p>
                                <p><?= h($funcionarioescolaridade->dt_cadastro) ?></p>

                                                    
                        <p class="title"><?= __('Last Update') ?></p>
                                <p><?= h($funcionarioescolaridade->last_update) ?></p>

                                                                                                                            <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
</div>


