

<div class="page-title">
    <div class="title_left">
        <h3><?= __('Funcionarioescolaridades') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2><?= $this->Html->link(__('  Cadastrar'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('funcionario_id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('nivelescolaridade_id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('curso_id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('escola_id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('escolacampus_id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('matricula') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('periodo') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('turno_id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('formatura') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('inicio') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_cadastro') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('last_updated_fields') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('last_update') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('status') ?></th>
                                                                
                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($funcionarioescolaridades as $funcionarioescolaridade){ ?>                                
                                <tr class="<?= $cor ?> pointer">
                                                                         
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?=$funcionarioescolaridade->id?>">
                                    </td>
                                    
                                                                                       <td><?= $this->Number->format($funcionarioescolaridade->id) ?></td>
                                                                                                     <td><?= $funcionarioescolaridade->has('funcionario') ? $this->Html->link($funcionarioescolaridade->funcionario->nome, ['controller' => 'Funcionarios', 'action' => 'view', $funcionarioescolaridade->funcionario->id]) : '' ?></td>
                                                                                                         <td><?= $funcionarioescolaridade->has('nivelescolaridade') ? $this->Html->link($funcionarioescolaridade->nivelescolaridade->descricao, ['controller' => 'Nivelescolaridades', 'action' => 'view', $funcionarioescolaridade->nivelescolaridade->id]) : '' ?></td>
                                                                                                         <td><?= $funcionarioescolaridade->has('curso') ? $this->Html->link($funcionarioescolaridade->curso->nome, ['controller' => 'Cursos', 'action' => 'view', $funcionarioescolaridade->curso->id]) : '' ?></td>
                                                                                                         <td><?= $funcionarioescolaridade->has('escola') ? $this->Html->link($funcionarioescolaridade->escola->nome, ['controller' => 'Escolas', 'action' => 'view', $funcionarioescolaridade->escola->id]) : '' ?></td>
                                                                                                         <td><?= $funcionarioescolaridade->has('escolacampus') ? $this->Html->link($funcionarioescolaridade->escolacampus->nome, ['controller' => 'Escolacampus', 'action' => 'view', $funcionarioescolaridade->escolacampus->id]) : '' ?></td>
                                                                                                     <td><?= h($funcionarioescolaridade->matricula) ?></td>
                                                                                                  <td><?= $this->Number->format($funcionarioescolaridade->periodo) ?></td>
                                                                                                     <td><?= $funcionarioescolaridade->has('turno') ? $this->Html->link($funcionarioescolaridade->turno->descricao, ['controller' => 'Turnos', 'action' => 'view', $funcionarioescolaridade->turno->id]) : '' ?></td>
                                                                                                     <td><?= h($funcionarioescolaridade->formatura) ?></td>
                                                                                                 <td><?= h($funcionarioescolaridade->inicio) ?></td>
                                                                                                 <td><?= h($funcionarioescolaridade->dt_cadastro) ?></td>
                                                                                                 <td><?= h($funcionarioescolaridade->last_updated_fields) ?></td>
                                                                                                 <td><?= h($funcionarioescolaridade->last_update) ?></td>
                                                                                                  <td><?= $this->Number->format($funcionarioescolaridade->status) ?></td>
                                                                                    
                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $funcionarioescolaridade->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $funcionarioescolaridade->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $funcionarioescolaridade->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $funcionarioescolaridade->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
