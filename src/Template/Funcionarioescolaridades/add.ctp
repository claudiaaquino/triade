
<div class="page-title">
    <div class="title_left">
        <h3><?= __('Cadastrar Funcionarioescolaridade') ?></h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Insira as informações para <?= __('cadastrar Funcionarioescolaridade') ?>. <small>* obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($funcionarioescolaridade, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                                
                                    
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="funcionario_id">funcionario_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('funcionario_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $funcionarios]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nivelescolaridade_id">nivelescolaridade_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('nivelescolaridade_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $nivelescolaridades]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="curso_id">curso_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('curso_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $cursos]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="escola_id">escola_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('escola_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $escolas, 'empty' => true]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="escolacampus_id">escolacampus_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('escolacampus_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $escolacampus, 'empty' => true]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="matricula">matricula <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('matricula', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="periodo">periodo <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('periodo', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="turno_id">turno_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('turno_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $turnos]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="formatura">formatura <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('formatura', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="inicio">inicio <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('inicio', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                           
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last_updated_fields">last_updated_fields <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('last_updated_fields', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                           
                                    
                                           
                    
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Cadastrar</button>
                        <?= $this->Html->link(__('Voltar'), ['action' => 'index'], ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>