<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= $cliente->nome ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Nome da Empresa/Cliente') ?></p>
                        <p><?= $cliente->nome ? $cliente->nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Cnpj') ?></p>
                        <p><?= $cliente->cnpj ? $cliente->cnpj : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Cpf') ?></p>
                        <p><?= $cliente->cpf ? $cliente->cpf : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Telefone') ?></p>
                        <p><?= $cliente->telefone ? $cliente->telefone : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Email') ?></p>
                        <p><?= $cliente->email ? $cliente->email : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Nome do Responsavel') ?></p>
                        <p><?= $cliente->nome_responsavel ? $cliente->nome_responsavel : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Cargo Responsavel') ?></p>
                        <p><?= $cliente->cargo_responsavel ? $cliente->cargo_responsavel : 'Não Informado'; ?></p>

                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Endereco') ?></p>
                        <p><?= $cliente->endereco ? $cliente->endereco : 'Não Informado'; ?></p>

                        <p class="title"><?= __('End Numero') ?></p>
                        <p><?= $cliente->end_numero ? $cliente->end_numero : 'Não Informado'; ?></p>

                        <p class="title"><?= __('End Complemento') ?></p>
                        <p><?= $cliente->end_complemento ? $cliente->end_complemento : 'Não Informado'; ?></p>

                        <p class="title"><?= __('End Bairro') ?></p>
                        <p><?= $cliente->end_bairro ? $cliente->end_bairro : 'Não Informado'; ?></p>

                        <p class="title"><?= __('End Cep') ?></p>
                        <p><?= $cliente->end_cep ? $cliente->end_cep : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Estado') ?></p>
                        <p><?= $cliente->has('estado') ? $cliente->estado->estado_sigla : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Cidade') ?></p>
                        <p><?= $cliente->has('cidade') ? $cliente->cidade->cidade_nome : 'Não Informado'; ?></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">



                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $cliente->dt_cadastro ? $cliente->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $cliente->last_update ? $cliente->last_update : 'Não Informado'; ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($cliente->contabancarias)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Contas Bancarias </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Banco') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Nome Títular') ?></th>
                                    <th scope="col"  class="column-title"><?= __('CPF/CNPJ') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Operacao') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Agencia') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Ag Digito') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Conta') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Co Digito') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th class="bulk-actions" colspan="12">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($cliente->contabancarias as $contabancarias):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($contabancarias->id) ?>">
                                        </td>
                                        <td><?= h($contabancarias->banco->nome) ?></td>
                                        <td><?= h($contabancarias->nome) ?></td>
                                        <td><?= h($contabancarias->cpf) ?></td>
                                        <td><?= h($contabancarias->operacao) ?></td>
                                        <td><?= h($contabancarias->agencia) ?></td>
                                        <td><?= h($contabancarias->ag_digito) ?></td>
                                        <td><?= h($contabancarias->conta) ?></td>
                                        <td><?= h($contabancarias->co_digito) ?></td>
                                        <td><?= h($contabancarias->dt_cadastro) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Contabancarias', 'action' => 'view', $contabancarias->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Contabancarias', 'action' => 'edit', $contabancarias->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Contabancarias', 'action' => 'delete', $contabancarias->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $contabancarias->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (!empty($cliente->movimentacaobancarias)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Movimentações de Caixa com esse Cliente </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Dt Lançamento') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Valor') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Conta Bancaria') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Forma de Pagamento') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Status Pagamento') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($cliente->movimentacaobancarias as $movimentacaobancarias):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($movimentacaobancarias->id) ?>">
                                        </td>
                                        <td><?= h($movimentacaobancarias->dt_cadastro) ?></td>
                                        <td>R$ <?= h($movimentacaobancarias->valor) ?></td>                                        
                                        <td><?= $movimentacaobancarias->has('contabancaria') ? $movimentacaobancarias->contabancaria->banco->nome . ($movimentacaobancarias->contabancaria->conta ? ' (' . $movimentacaobancarias->contabancaria->conta . '-' . $movimentacaobancarias->contabancaria->co_digito . ')' : '') : 'Não informado' ?></td>
                                        <td><?= $movimentacaobancarias->has('formaspagamento') ? $movimentacaobancarias->formaspagamento->descricao . ' ' . ($movimentacaobancarias->avista ? ' à vista' : ' parcelado') : 'Não Informado' ?></td>
                                        <td><?= $movimentacaobancarias->pago ? 'PAGO' : 'PENDENTE' ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Movimentacaobancarias', 'action' => 'view', $movimentacaobancarias->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Movimentacaobancarias', 'action' => 'edit', $movimentacaobancarias->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Movimentacaobancarias', 'action' => 'delete', $movimentacaobancarias->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $movimentacaobancarias->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


