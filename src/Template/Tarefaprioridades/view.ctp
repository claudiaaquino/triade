<div class="page-title">
    <div class="title_left">
        <h3><?= __('Tarefaprioridades') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($tarefaprioridade->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Descricao') ?></p>
                        <p><?= h($tarefaprioridade->descricao) ?></p>
                        </tr>

                        <p class="title"><?= __('Id') ?></p>
                        <p><?= $this->Number->format($tarefaprioridade->id) ?></p>


                        <p class="title"><?= __('Ordem') ?></p>
                        <p><?= $this->Number->format($tarefaprioridade->ordem) ?></p>


                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= h($tarefaprioridade->dt_cadastro) ?></p>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($tarefaprioridade->tarefas)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Tarefas Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tarefatipo Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tarefaprioridade Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Areaservico Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tiposervico Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Titulo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Descricao') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Inicio') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Final') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Lembrete') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Concluida') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Adiada') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Adiada') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th scope="col"  class="column-title"><?= __('User Cadastrou') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php ?>
                                <tr>


                                </tr>

                                <?php
                                $cor = 'even';
                                foreach ($tarefaprioridade->tarefas as $tarefas):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($tarefas->id) ?>">
                                        </td>
                                        <td><?= h($tarefas->id) ?></td>
                                        <td><?= h($tarefas->user_id) ?></td>
                                        <td><?= h($tarefas->tarefatipo_id) ?></td>
                                        <td><?= h($tarefas->tarefaprioridade_id) ?></td>
                                        <td><?= h($tarefas->areaservico_id) ?></td>
                                        <td><?= h($tarefas->tiposervico_id) ?></td>
                                        <td><?= h($tarefas->titulo) ?></td>
                                        <td><?= h($tarefas->descricao) ?></td>
                                        <td><?= h($tarefas->dt_inicio) ?></td>
                                        <td><?= h($tarefas->dt_final) ?></td>
                                        <td><?= h($tarefas->dt_lembrete) ?></td>
                                        <td><?= h($tarefas->dt_concluida) ?></td>
                                        <td><?= h($tarefas->adiada) ?></td>
                                        <td><?= h($tarefas->dt_adiada) ?></td>
                                        <td><?= h($tarefas->dt_cadastro) ?></td>
                                        <td><?= h($tarefas->user_cadastrou) ?></td>
                                        <td><?= h($tarefas->last_update) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Tarefas', 'action' => 'view', $tarefas->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Tarefas', 'action' => 'edit', $tarefas->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefas', 'action' => 'delete', $tarefas->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefas->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


