<div class="page-title">
    <div class="title_left">
        <h3>Detalhes do Menu</h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($escolacampus->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Instituição de Ensino</p>
                        <p><?= $escolacampus->has('escola') ? $this->Html->link($escolacampus->escola->nome, ['controller' => 'Escolas', 'action' => 'view', $escolacampus->escola->id]) : ''; ?></p>
                        <p class="title">Endereço</p>
                        <p><?= $escolacampus->endereco ?></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Data de Cadastro</p>
                        <p><?= $escolacampus->dt_cadastro ?></p>
                        <p class="title">Ultima Atualização</p>
                        <p><?= $escolacampus->last_update; ?></p>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>