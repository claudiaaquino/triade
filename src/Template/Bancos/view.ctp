<div class="page-title">
    <div class="title_left">
        <h3><?= __('Bancos') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($banco->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Nome') ?></p>
                                    <p><?= $banco->nome ? $banco->nome : 'Não Informado'; ?></p>

                                                                                                                    <p class="title"><?= __('Cod') ?></p>
                                    <p><?= $banco->cod ? $banco->cod : 'Não Informado'; ?></p>

                                                                                                                    <p class="title"><?= __('Sigla') ?></p>
                                    <p><?= $banco->sigla ? $banco->sigla : 'Não Informado'; ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $banco->id ? $this->Number->format($banco->id) : 'Não Informado'; ?></p>

                                                                                                                                                        
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $banco->status ? __('Ativo') : __('Desativado'); ?></p>

                                                                                                    <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <?php if (!empty($banco->contabancarias)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Contabancarias Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Empresa Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cliente Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Fornecedor Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Banco Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Operacao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Valorcaixa') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Agencia') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Ag Digito') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Conta') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Co Digito') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Vencimento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cpf') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Endereco') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Numero') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Complemento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Bairro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Cep') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($banco->contabancarias as $contabancarias):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= h($contabancarias->id) ?>">
                                    </td>
                                                                    <td><?= h($contabancarias->id) ?></td>
                                                                    <td><?= h($contabancarias->empresa_id) ?></td>
                                                                    <td><?= h($contabancarias->cliente_id) ?></td>
                                                                    <td><?= h($contabancarias->fornecedor_id) ?></td>
                                                                    <td><?= h($contabancarias->user_id) ?></td>
                                                                    <td><?= h($contabancarias->banco_id) ?></td>
                                                                    <td><?= h($contabancarias->operacao) ?></td>
                                                                    <td><?= h($contabancarias->valorcaixa) ?></td>
                                                                    <td><?= h($contabancarias->agencia) ?></td>
                                                                    <td><?= h($contabancarias->ag_digito) ?></td>
                                                                    <td><?= h($contabancarias->conta) ?></td>
                                                                    <td><?= h($contabancarias->co_digito) ?></td>
                                                                    <td><?= h($contabancarias->dt_vencimento) ?></td>
                                                                    <td><?= h($contabancarias->cpf) ?></td>
                                                                    <td><?= h($contabancarias->nome) ?></td>
                                                                    <td><?= h($contabancarias->endereco) ?></td>
                                                                    <td><?= h($contabancarias->end_numero) ?></td>
                                                                    <td><?= h($contabancarias->end_complemento) ?></td>
                                                                    <td><?= h($contabancarias->end_bairro) ?></td>
                                                                    <td><?= h($contabancarias->end_cep) ?></td>
                                                                    <td><?= h($contabancarias->estado_id) ?></td>
                                                                    <td><?= h($contabancarias->cidade_id) ?></td>
                                                                    <td><?= h($contabancarias->dt_cadastro) ?></td>
                                                                    <td><?= h($contabancarias->last_update) ?></td>
                                                                    <td><?= h($contabancarias->status) ?></td>
                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Contabancarias', 'action' => 'view', $contabancarias->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Contabancarias', 'action' => 'edit', $contabancarias->id], [ 'class' => "btn btn-info btn-xs"]) ?>
    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Contabancarias', 'action' => 'delete', $contabancarias->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $contabancarias->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>


