<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Inciso - <?= h($leisinciso->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Lei / Norma') ?></p>
                        <p><?= $leisinciso->has('leisnorma') ? $this->Html->link($leisinciso->leisnorma->numero, ['controller' => 'Leisnormas', 'action' => 'view', $leisinciso->leisnorma->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Artigo') ?></p>
                        <p><?= $leisinciso->has('leisartigo') ? $this->Html->link($leisinciso->leisartigo->nome, ['controller' => 'Leisartigos', 'action' => 'view', $leisinciso->leisartigo->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Nome') ?></p>
                        <p><?= $leisinciso->nome ? $leisinciso->nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Descricao') ?></p>
                        <p>  <?= $leisinciso->descricao ? $this->Text->autoParagraph(h($leisinciso->descricao)) : 'Não Informado'; ?></p>

                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Ordem') ?></p>
                        <p><?= $leisinciso->ordem ? $this->Number->format($leisinciso->ordem) : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $leisinciso->dt_cadastro ? $leisinciso->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $leisinciso->last_update ? $leisinciso->last_update : 'Não modificado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $leisinciso->status ? __('Ativo') : __('Inativo'); ?></p>


                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $leisinciso->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $leisinciso->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisinciso->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


