<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Editar Inciso') ?> <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($leisinciso, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                                
                                    
                                    <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leisnorma_id">Lei/Norma <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('leisnorma_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $leisnormas, 'empty' => true]); ?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leisartigo_id">Artigo <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('leisartigo_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $leisartigos, 'empty' => true]); ?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">Inciso <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('nome', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Descrição do Inciso <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ordem">Ordem que deve aparecer <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('ordem', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>

<script>
    $(document).ready(function () {
        $("select").select2();
    });
</script>