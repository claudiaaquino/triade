<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Filtrar Documentos</h2>
                <!--<h2><small></small></h2>-->
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" style="display: none;">
                <form action="/documentos/index" method="POST" id="documentos" class="form-horizontal form-label-left">
                    <?php if ($admin) { ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa">Empresa que o documento pertence 
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('empresa_id', ['options' => $empresas, "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'empty' => 'Selecione uma Empresa', "style" => "width: 100%"]); ?>
                            </div>
                        </div>                
                    <?php } ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="setor">Setor
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('areaservico_id', [ 'options' => $areaservicos, "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'empty' => 'Selecione um setor', "style" => "width: 100%"]); ?>
                        </div>
                    </div>     
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="grupo">Grupo do Documento 
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('grupodocumento_id', [ "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%"]); ?>
                        </div>
                    </div>                
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo">Documento 
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('tipodocumento_id', [ "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%"]); ?>
                        </div>
                    </div>    
                    <?php if ($admin || $admin_empresa || $admin_setores) { ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="funcionario">Funcionário que o documento pertence 
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('funcionario_id', [ 'options' => $funcionarios, "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%", 'empty' => 'selecione (se necessário)']); ?>
                            </div>
                        </div>     
                    <?php } ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="periodo">Data de envio entre
                        </label>
                        <div class="col-md-2 col-sm-2 col-xs-5">
                            <?= $this->Form->input('dt_inicio', ["class" => "form-control col-md-3 col-xs-3", 'label' => false, 'placeholder' => 'data inicial da busca', 'data-inputmask' => "'mask': '99/99/9999'"]); ?> 
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-5">
                            <?= $this->Form->input('dt_final', ["class" => "form-control col-md-3 col-xs-3", 'label' => false, 'placeholder' => 'data final da busca', 'data-inputmask' => "'mask': '99/99/9999'"]); ?>
                        </div>
                    </div>     
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit"  class="btn btn-success">Filtrar</button>
                        </div>
                    </div>
                    <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                </form>               
            </div>
        </div>

        <div class="x_panel">         
            <div class="x_title">
                <h2>Lista de Documentos Enviados <?= $nomeremetente ? ' por ' . $nomeremetente : '' ?></h2>
                <!--<h2><small></small></h2>-->
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Enviar Novo'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?> </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content"  style="display: none;">
                <?= $this->Flash->render() ?>

                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>

                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_enviado', 'Dt. Enviado') ?>  <i class="fa fa-sort"></i></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('Areaservicos.descricao', 'Setor') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('Grupodocumentos.descricao', 'Grupo do Documento') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('Tipodocumentos.descricao', 'Tipo do Documento') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('Empresas.razao', "Empresa") ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('Funcionarios.nome', "Funcionário") ?></th>
                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $cor = 'even';
                            foreach ($documentos as $documento):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records">
                                    </td>
                                    <td class=" "><?= h($documento->dt_enviado) ?></td>
                                    <td class=" "><?= $documento->has('tipodocumento') ? $this->Html->link($documento->tipodocumento->grupodocumento->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $documento->tipodocumento->grupodocumento->areaservico->id]) : '' ?> </td>
                                    <td class=" "><?= $documento->has('tipodocumento') ? $this->Html->link($documento->tipodocumento->grupodocumento->descricao, ['controller' => 'Grupodocumentos', 'action' => 'view', $documento->tipodocumento->grupodocumento->id]) : '' ?> </td>
                                    <td class=" "><?= $documento->has('tipodocumento') ? $this->Html->link($documento->tipodocumento->descricao, ['controller' => 'Tipodocumentos', 'action' => 'view', $documento->tipodocumento->id]) : '' ?> </td>
                                    <td class=" "><?= $documento->has('empresa') ? $this->Html->link($documento->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $documento->empresa->id]) : '' ?></td>
                                    <td class=" "><?= $documento->has('funcionario') ? $this->Html->link($documento->funcionario->nome, ['controller' => 'Funcionarios', 'action' => 'view', $documento->funcionario->id]) : '' ?></td>

                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $documento->id], ['class' => "btn btn-primary btn-xs"]); ?>
                                            <?= ''//$this->Html->link(__('Editar'), ['action' => 'edit', $documento->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Html->link(__('Download'), "/docs/" . $documento->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                            <?php if ($admin) { ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $documento->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Tem certeza que deseja deletar o registro desse documento?', $documento->id)]) ?>
                                            <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js'); ?>
<?= $this->Html->script('/js/ajax/empresas.js'); ?>
<?= $this->Html->script('/js/ajax/documentos.js'); ?>
<script>
    $(document).ready(function () {
        $(":input").inputmask();
        $("select").select2({minimumResultsForSearch: 6});
    });
</script>


