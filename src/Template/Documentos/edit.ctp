<nav class="large-1 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(__('Deletar Documento'), ['action' => 'delete', $documento->id], ['confirm' => __('Tem certeza que deseja deletar # {0}?', $documento->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Documentos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Enviar Outro Documento'), ['action' => 'add']) ?> </li>
        
    </ul>
</nav>
<div class="documentos form large-9 medium-8 columns content">
    <?= $this->Form->create($documento, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Edit Documento') ?></legend>
        <?php
            echo $this->Form->input('tipodocumento_id', ['options' => $tipodocumentos]);
            echo $this->Form->input('empresa_id', ['options' => $empresas]);
            echo $this->Form->input('funcionario_id', ['options' => $funcionarios, 'empty' => true]);
            echo $this->Form->input('nome');
            echo $this->Form->input('descricao');
            echo $this->Form->file('file');
            echo $this->Form->input('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
