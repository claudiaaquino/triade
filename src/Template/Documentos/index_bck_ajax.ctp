<?= $this->Html->script('/js/ajax/documentos.js'); ?>
<?= $this->Html->script('/js/ajax/empresas.js'); ?>
<div class="page-title">
    <div class="title_left">
        <h3>Lista de Documentos Enviados</h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Filtros</h2>
                <!--<h2><small></small></h2>-->
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" style="display: none;">
                <!--<h2>Filtar a Lista de Documentos</h2>-->    
                <form action="" id="documentos" class="form-horizontal form-label-left">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa">Empresa Destinatária 
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('empresa_id', ['options' => $empresas, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => 'Selecione uma Empresa']); ?>
                        </div>
                    </div>                
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="setor">Setor
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('areaservico_id', [ 'options' => $areaservicos, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => 'Selecione um setor']); ?>
                        </div>
                    </div>                
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="grupo">Grupo do Documento 
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('grupodocumento_id', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                        </div>
                    </div>                
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo">Tipo do Documento 
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('tipodocumento_id', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                        </div>
                    </div>    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="funcionario">Funcionário
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('funcionario_id', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                        </div>
                    </div>     
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="button" onclick='loadDocTable()' class="btn btn-success">Filtrar</button>
                        </div>
                    </div>
                    <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                </form>               
            </div>
        </div>

        <div class="x_panel">         
            <div class="x_title">
                <h2>Documentos</h2>
                <!--<h2><small></small></h2>-->
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content"  id="table-documentos">



            </div>
        </div>
    </div>
</div>



