<?php if ($tpview == 'table') { ?>
    <?= $this->Flash->render() ?>
    <div class="table-responsive">

        <table class="table table-striped jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th>
                        <input type="checkbox" id="check-all" class="flat">
                    </th>

                    <th scope="col" class="column-title" ><a href="#table-documentos" onclick="orderTable('dt_enviado')">Dt. Enviado <i class="fa fa-sort"></i></a></th>
                    <th scope="col" class="column-title"><a href="#table-documentos" onclick="orderTable('Areaservicos.descricao')">Setor<i class="fa fa-sort-alpha-asc"></i></a></th>
                    <th scope="col" class="column-title"><a href="#table-documentos" onclick="orderTable('Grupodocumentos.descricao')">Grupo do Documento<i class="fa fa-sort-alpha-asc"></i></a></th>
                    <th scope="col" class="column-title"><a href="#table-documentos" onclick="orderTable('Tipodocumentos.descricao')">Tipo do Documento<i class="fa fa-sort-alpha-asc"></i></a></th>
                    <th scope="col" class="column-title"><a href="#table-documentos" onclick="orderTable('Empresas.razao')">Empresa<i class="fa fa-sort-alpha-asc"></i></a></th>
                    <th scope="col" class="column-title"><a href="#table-documentos" onclick="orderTable('Funcionarios.nome')">Funcionário<i class="fa fa-sort-alpha-asc"></i></a></th>
                    <th scope="col" class="column-title"><a href="#table-documentos" onclick="orderTable('dt_lido')">Dt. Lido <i class="fa fa-sort"></i></a></th>
                    <th class="bulk-actions" colspan="7">
                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                    </th>
                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                </tr>
            </thead>
            <tbody >
                <?php
                $cor = 'even';
                foreach ($documentos as $documento):
                    ?>
                    <tr class="<?= $cor ?> pointer">
                        <td class="a-center ">
                            <input type="checkbox" class="flat" name="table_records">
                        </td>
                        <td class=" "><?= h($documento->dt_enviado) ?></td>
                        <td class=" "><?= $documento->has('tipodocumento') ? $this->Html->link($documento->tipodocumento->grupodocumento->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $documento->tipodocumento->grupodocumento->areaservico->id]) : '' ?> </td>
                        <td class=" "><?= $documento->has('tipodocumento') ? $this->Html->link($documento->tipodocumento->grupodocumento->descricao, ['controller' => 'Grupodocumentos', 'action' => 'view', $documento->tipodocumento->grupodocumento->id]) : '' ?> </td>
                        <td class=" "><?= $documento->has('tipodocumento') ? $this->Html->link($documento->tipodocumento->descricao, ['controller' => 'Tipodocumentos', 'action' => 'view', $documento->tipodocumento->id]) : '' ?> </td>
                        <td class=" "><?= $documento->has('empresa') ? $this->Html->link($documento->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $documento->empresa->id]) : '' ?></td>
                        <td class=" "><?= $documento->has('funcionario') ? $this->Html->link($documento->funcionario->nome, ['controller' => 'Funcionarios', 'action' => 'view', $documento->funcionario->id]) : '' ?></td>
                        <td class=" "><?= h($documento->dt_lido) ?></td>
                        <td  class=" last">
                            <div class="btn-group">
                                <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $documento->id], ['class' => "btn btn-primary btn-xs"]); ?>
                                <?= $this->Html->link(__('Download'), "/docs/" . $documento->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                <a class="btn btn-danger btn-xs" href='#' onclick="deleteDocument(<?= $documento->id ?>);">Deletar</a>
                            </div>
                        </td>
                    </tr>
                    <?php
                    $cor = $cor == 'even' ? 'odd' : 'even';
                endforeach;
                ?>
            </tbody>                      
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?php
                $this->Paginator->templates([
                    'number' => __("<li><a href='#table-documentos' onclick='paginatorTable({{text}})'>{{text}}</a></li>"),
                    'nextActive' => __("<li><a href='#table-documentos' onclick='paginatorTable({{text}})'>próximo ></a></li>"),
                    'prevActive' => __("<li><a href='#table-documentos' onclick='paginatorTable({{text}})'>< anterior</a></li>"),
                ]);
                ?>
                <?= $this->Paginator->prev() ?>
                <?php
                echo $this->Paginator->numbers();
                ?>
                <?= $this->Paginator->next() ?>
            </ul>
            <?= $this->Paginator->counter() ?>
            <?= $this->Form->input('currentPage', [ "type" => "hidden", "value" => $this->Paginator->current('Documentos'), 'label' => false]); ?>
            <?= $this->Form->input('direction', [ "type" => "hidden", "value" => $this->Paginator->sortDir('Documentos'), 'label' => false]); ?>
        </div>

    </div>
    <?php
}else if ($tpview == 'list') {
    echo $retorno;
} else if ($tpview == 'add') {
    echo $retorno;
}
?>