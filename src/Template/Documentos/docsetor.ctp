<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Documentos do Setor <?= $setor->descricao . (!empty($funcionarios) ? ' - Funcionários' : ''); ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>

                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <?php if ($admin) { ?>  <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_enviado', 'Dt. Enviado') ?>  <i class="fa fa-sort"></i></th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('Empresas.razao', "Empresa") ?></th><?php } ?>                                 
                                <th scope="col" class="column-title">Documento</th>
                                <?php if (!empty($funcionarios)) { ?>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('funcionario', 'Funcionário') ?></th>
                                <?php } else { ?>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('competencia', 'Competência') ?></th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('exercicio', 'Exercício') ?></th>
                                <?php } ?>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('versao', 'Versão') ?></th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $cor = 'even';
                            foreach ($documentos as $documento):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <?php if ($admin) { ?> <td class=" "><?= h($documento->dt_enviado) ?></td>
                                        <td class=" "><?= $documento->has('empresa') ? $this->Html->link($documento->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $documento->empresa->id]) : '' ?></td><?php } ?> 
                                    <td class=" "><?= $documento->tipodocumento->descricao ?> </td>
                                    <?php if (!empty($funcionarios)) { ?>
                                        <td class=" "><?= $documento->funcionario ? $documento->funcionario->nome : '--' ?> </td>
                                    <?php } else { ?>
                                        <td class=" "><?= $documento->tipodocumento->exige_competencia ? h($documento->competencia) : '--'; ?></td>
                                        <td class=" "><?= $documento->tipodocumento->exige_exercicio ? h($documento->exercicio) : '--'; ?></td>
                                    <?php } ?>
                                    <td class=" "><?= h($documento->versao) ?></td>
                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Download'), "/docs/" . $documento->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                            <?php if ($admin) { ?>
                                                <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $documento->id], ['class' => "btn btn-primary btn-xs"]); ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $documento->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Tem certeza que deseja deletar o registro desse documento?', $documento->id)]) ?>
                                            <?php } else if($documento->tipodocumento->reemissao) { ?>
                                                <?= $this->Html->link(__('Solicitar Remissão'), ['action' => 'solicitarreemissao', $documento->id], ['class' => "btn btn-primary btn-xs"]); ?>
                                            <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?php
                            $url = ['setor_id' => $setor->id];
                            if (!empty($funcionarios)) {
                                $url['filtro_extra'] = 'funcionarios';
                            }
                            ?>
                            <?= $this->Paginator->options(['url' => $url]); ?>
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Filtrar</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="/documentos/docsetor" method="POST" id="documentos" class="form-horizontal form-label-left">
                    <?php if ($admin) { ?>
                        <div class="form-group">
                            <!--                            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="empresa">Empresa que o documento pertence 
                                                        </label>-->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?= $this->Form->input('empresa_id', ['options' => $empresas, "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'empty' => 'Empresa', "style" => "width: 100%"]); ?>
                            </div>
                        </div>                
                    <?php } /* ?>
                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="grupo">Grupo do Documento
                      </label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      <?= $this->Form->input('grupodocumento_id', ['options' => $grupodocumentos, "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'empty' => 'Selecione um grupo', "style" => "width: 100%"]); ?>
                      </div>
                      </div>
                      <?php } */ ?>
                    <?php if (!empty($funcionarios)) { ?>
                        <div class="form-group">
                            <!--                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="funcionario">Funcionário
                                                      </label>-->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?= $this->Form->input('funcionario_id', [ 'options' => $funcionarios, "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%", 'empty' => 'Funcionário']); ?>
                                <?= $this->Form->input('sollaendern', ['type' => 'hidden', 'value' => 'funcionario']); ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <!--                        <label class="control-label col-md-12 col-sm-12 col-xs-12" for="tipo">Documento 
                                                </label>-->
                        <div class=" col-md-12 col-sm-12 col-xs-12">
                            <?= $this->Form->input('tipodocumento_id', ['options' => $tipodocumentos, "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%", 'empty' => 'Documento',]); ?>
                        </div>
                    </div>    
                    <?php if (empty($funcionarios)) { ?>
                        <div class="form-group competencia">
                            <!--                        <label class="control-label col-md-12 col-sm-12 col-xs-12" for="competencia">Competência
                                                    </label>-->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?= $this->Form->input('competencia', ['options' => $meses, "class" => "form-control col-md-3 col-xs-3", 'empty' => 'Competência', 'label' => false]); ?> 
                            </div>
                        </div>     
                        <div class="form-group exercicio">
                            <!--                        <label class="control-label col-md-12 col-sm-12 col-xs-12" for="competencia">Exercício
                                                    </label>-->
                            <div class=" col-md-12 col-sm-12 col-xs-12">
                                <?= $this->Form->input('exercicio', ['options' => $anos, "class" => "form-control col-md-3 col-xs-3", 'label' => false, 'empty' => 'Exercício']); ?>

                            </div>
                        </div>     
                    <?php } ?>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit"  class="btn btn-success">Filtrar</button>
                        </div>
                    </div>
                    <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                    <?= $this->Form->input('setor_id', [ "type" => "hidden", "value" => $setor->id, 'label' => false]); ?>
                    <?= $this->Form->input('filtro_extra', [ "type" => "hidden", "value" => 'funcionarios', 'label' => false]); ?>
                </form>               
            </div>
        </div>
    </div>

</div>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js'); ?>
<?= $this->Html->script('/js/ajax/empresas.js'); ?>
<?= $this->Html->script('/js/ajax/documentos.js'); ?>
<script>
    $(document).ready(function () {
        $(":input").inputmask();
        $("select").select2({minimumResultsForSearch: 6});
    });
</script>


