<div class="page-title">
    <div class="title_left">
        <h3>Detalhes de Documento</h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($documento->nome) ?></h2>
                <!--<h2><small></small></h2>-->
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Grupo do Documento</p>
                        <?php if ($admin) { ?>
                            <p><?= $documento->has('tipodocumento') ? $this->Html->link($documento->tipodocumento->grupodocumento->descricao, ['controller' => 'Grupodocumentos', 'action' => 'view', $documento->tipodocumento->grupodocumento->id]) : '' ?></p>
                        <?php } else { ?>
                            <p><?= $documento->tipodocumento->grupodocumento->descricao ?></p>
                        <?php } ?>
                            
                        <?php if ($admin) { ?>
                            <p class="title">Tipo de Documento</p>
                            <p><?= $documento->has('tipodocumento') ? $this->Html->link($documento->tipodocumento->descricao, ['controller' => 'Tipodocumentos', 'action' => 'view', $documento->tipodocumento->id]) : '' ?></p>
                        <?php } else { ?>
                            <p><?= $documento->tipodocumento->descricao ?></p>
                        <?php } ?>
                            
                        <p class="title">Empresa</p>
                        <?php if ($admin) { ?>
                            <p><?= $documento->has('empresa') ? $this->Html->link($documento->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $documento->empresa->id]) : '' ?></p>
                        <?php } else { ?>
                            <p><?= $documento->empresa->razao ?></p>
                        <?php } ?>
                            
                        <p class="title">Funcionário</p>
                        <p><?= $documento->has('funcionario') ? $this->Html->link($documento->funcionario->nome, ['controller' => 'Funcionarios', 'action' => 'view', $documento->funcionario->id]) : '' ?></p>
                    </div>

                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <?php if ($documento->nome) { ?>
                            <p class="title">Nome do Arquivo</p>
                            <p><?= h($documento->nome) ?></p>
                        <?php } ?>
                        <?php if ($documento->descricao) { ?>
                            <p class="title">Descrição/Observações</p>
                            <p><?= h($documento->descricao) ?></p>
                        <?php } ?>
                        <p class="title">Usuário que Enviou</p>
                        <?php if ($admin) { ?>
                            <p><?= $this->Html->link($user_enviou->nome, ['controller' => 'Users', 'action' => 'view', $user_enviou->id]) ?></p>
                        <?php } else { ?>
                            <p><?= $user_enviou->nome ?></p>
                        <?php } ?>
                        <p class="title">Data que foi Enviado</p>
                        <p><?= h($documento->dt_enviado) ?></p>
<!--                        <p class="title">Usuário que leu</p>
<p><?php /* if ($documento->dt_lido) {
                          echo $this->Html->link($user_leu->nome, ['controller' => 'Users', 'action' => 'view', $user_leu->id]);
                          } else {
                          echo 'O Arquivo ainda não foi lido';
                          }
                         */ ?></p>-->

                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Baixar Documento", "/docs/" . $documento->file, array('target' => '_blank', 'class' => "btn btn-sm btn-primary")); ?>
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-sm btn-default"]); ?>
                    </div>
                </div>

                <!--                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="project_detail">
                                        <p class="title">Data que foi Lido</p>
                                        <p><?= ''//$documento->dt_lido ? $documento->dt_lido : 'O Arquivo ainda não foi lido'                    ?></p>
                                        <p class="title">Ultima Atualização</p>
                                        <p><?= ''//h($documento->last_update)                    ?></p>
                                    </div>
                                </div>-->
            </div>
        </div>
    </div>
</div>




