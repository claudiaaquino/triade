<div class="page-title">
    <div class="title_left">
        <h3><?= __('Tipoactions') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($tipoaction->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Descricao') ?></p>
                                    <p><?= $tipoaction->descricao ? $tipoaction->descricao : 'Não Informado'; ?></p>

                                                                                                                    <p class="title"><?= __('Action') ?></p>
                                    <p><?= $tipoaction->action ? $tipoaction->action : 'Não Informado'; ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $tipoaction->id ? $this->Number->format($tipoaction->id) : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Dt Cadastro') ?></p>
                                <p><?= $tipoaction->dt_cadastro ? $tipoaction->dt_cadastro : 'Não Informado'; ?></p>

                                                    
                        <p class="title"><?= __('Last Update') ?></p>
                                <p><?= $tipoaction->last_update ? $tipoaction->last_update : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $tipoaction->status ? __('Ativo') : __('Desativado'); ?></p>

                                                                                                    <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <?php if (!empty($tipoaction->telaquestionarios)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Telaquestionarios Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tela Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tipoaction Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tiposervico Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Pergunta') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Resposta') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tipoaction->telaquestionarios as $telaquestionarios):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= h($telaquestionarios->id) ?>">
                                    </td>
                                                                    <td><?= h($telaquestionarios->id) ?></td>
                                                                    <td><?= h($telaquestionarios->tela_id) ?></td>
                                                                    <td><?= h($telaquestionarios->tipoaction_id) ?></td>
                                                                    <td><?= h($telaquestionarios->tiposervico_id) ?></td>
                                                                    <td><?= h($telaquestionarios->pergunta) ?></td>
                                                                    <td><?= h($telaquestionarios->resposta) ?></td>
                                                                    <td><?= h($telaquestionarios->user_id) ?></td>
                                                                    <td><?= h($telaquestionarios->dt_cadastro) ?></td>
                                                                    <td><?= h($telaquestionarios->last_update) ?></td>
                                                                    <td><?= h($telaquestionarios->status) ?></td>
                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Telaquestionarios', 'action' => 'view', $telaquestionarios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Telaquestionarios', 'action' => 'edit', $telaquestionarios->id], [ 'class' => "btn btn-info btn-xs"]) ?>
    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Telaquestionarios', 'action' => 'delete', $telaquestionarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $telaquestionarios->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>


