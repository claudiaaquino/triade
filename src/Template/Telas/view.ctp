<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i><?= __('Telas') ?> - <?= h($tela->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Descricao') ?></p>
                                    <p><?= $tela->descricao ? $tela->descricao : 'Não Informado'; ?></p>

                                                                                                                    <p class="title"><?= __('Controller') ?></p>
                                    <p><?= $tela->controller ? $tela->controller : 'Não Informado'; ?></p>

                                                                                                                    <p class="title"><?= __('Action') ?></p>
                                    <p><?= $tela->action ? $tela->action : 'Não Informado'; ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $tela->id ? $this->Number->format($tela->id) : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Dt Cadastro') ?></p>
                                <p><?= $tela->dt_cadastro ? $tela->dt_cadastro : 'Não Informado'; ?></p>

                                                    
                        <p class="title"><?= __('Last Update') ?></p>
                                <p><?= $tela->last_update ? $tela->last_update : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Flag Servico') ?></p>
                                <p><?= $tela->flag_servico ? __('Ativo') : __('Desativado'); ?></p>

                                                    
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $tela->status ? __('Ativo') : __('Desativado'); ?></p>

                                                                                                </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $tela->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $tela->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tela->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
                    <?php if (!empty($tela->telaquestionarios)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Telaquestionarios Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Telaquestionario Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Assunto Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Lei Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tela Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tipoaction Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tiposervico Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Pergunta') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Resposta') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Exigeconfirmacao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tela->telaquestionarios as $telaquestionarios):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                                                    <td><?= h($telaquestionarios->id) ?></td>
                                                                    <td><?= h($telaquestionarios->telaquestionario_id) ?></td>
                                                                    <td><?= h($telaquestionarios->assunto_id) ?></td>
                                                                    <td><?= h($telaquestionarios->estado_id) ?></td>
                                                                    <td><?= h($telaquestionarios->cidade_id) ?></td>
                                                                    <td><?= h($telaquestionarios->lei_id) ?></td>
                                                                    <td><?= h($telaquestionarios->tela_id) ?></td>
                                                                    <td><?= h($telaquestionarios->tipoaction_id) ?></td>
                                                                    <td><?= h($telaquestionarios->tiposervico_id) ?></td>
                                                                    <td><?= h($telaquestionarios->pergunta) ?></td>
                                                                    <td><?= h($telaquestionarios->resposta) ?></td>
                                                                    <td><?= h($telaquestionarios->exigeconfirmacao) ?></td>
                                                                    <td><?= h($telaquestionarios->user_id) ?></td>
                                                                    <td><?= h($telaquestionarios->dt_cadastro) ?></td>
                                                                    <td><?= h($telaquestionarios->last_update) ?></td>
                                                                    <td><?= h($telaquestionarios->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Telaquestionarios', 'action' => 'view', $telaquestionarios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Telaquestionarios', 'action' => 'edit', $telaquestionarios->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Telaquestionarios', 'action' => 'delete', $telaquestionarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $telaquestionarios->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
            </div>
</div>


