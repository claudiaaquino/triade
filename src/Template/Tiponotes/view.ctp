<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Tópicos de Anotações do Legislativo - <?= h($tiponote->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Nome do Tópico') ?></p>
                        <p><?= $tiponote->descricao ? $tiponote->descricao : 'Não Informado'; ?></p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $tiponote->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $tiponote->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tiponote->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($tiponote->anotacaoassuntos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Anotações referentes à esse Tópico </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Assunto') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Anotação') ?></th>
                                    <!--<th scope="col" class="column-title no-link"><span class="nobr"></span></th>-->
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tiponote->anotacaoassuntos as $anotacaoassuntos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($anotacaoassuntos->assunto->descricao) ?></td>
                                        <td><?= $this->Text->autoParagraph($anotacaoassuntos->anotacao) ?></td>
                                        <?php /* <td  class=" last">
                                          <div class="btn-group">
                                          <?= $this->Html->link(__('Visualizar'), ['controller' => 'Anotacaoassuntos', 'action' => 'view', $anotacaoassuntos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                          <?= $this->Html->link(__('Editar'), ['controller' => 'Anotacaoassuntos', 'action' => 'edit', $anotacaoassuntos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                          <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Anotacaoassuntos', 'action' => 'delete', $anotacaoassuntos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $anotacaoassuntos->id)]) ?>
                                          </div>
                                          </td> */ ?>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


