Vocês tem que me dar sugestões para essa tela inicial.
Pensei que poderia ser com o 'perfil' da empresa
<table class="vertical-table">
    <tr>
        <th scope="row"><?= __('Username') ?></th>
        <td><?= h($user->username) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Email') ?></th>
        <td><?= h($user->email) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Id') ?></th>
        <td><?= $this->Number->format($user->id) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Status') ?></th>
        <td><?= $this->Number->format($user->status) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Dt Nascimento') ?></th>
        <td><?= h($user->dt_nascimento) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Last Login') ?></th>
        <td><?= h($user->last_login) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Last Update') ?></th>
        <td><?= h($user->last_update) ?></td>
    </tr>
</table>