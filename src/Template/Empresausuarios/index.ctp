<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Empresausuario'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Empresas'), ['controller' => 'Empresas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Empresa'), ['controller' => 'Empresas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="empresausuarios index large-9 medium-8 columns content">
    <h3><?= __('Empresausuarios') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('empresa_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dt_cadastro') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($empresausuarios as $empresausuario): ?>
            <tr>
                <td><?= $this->Number->format($empresausuario->id) ?></td>
                <td><?= $empresausuario->has('user') ? $this->Html->link($empresausuario->user->id, ['controller' => 'Users', 'action' => 'view', $empresausuario->user->id]) : '' ?></td>
                <td><?= $empresausuario->has('empresa') ? $this->Html->link($empresausuario->empresa->id, ['controller' => 'Empresas', 'action' => 'view', $empresausuario->empresa->id]) : '' ?></td>
                <td><?= h($empresausuario->dt_cadastro) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $empresausuario->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $empresausuario->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $empresausuario->id], ['confirm' => __('Tem certeza que deseja deletar esse registro # {0}?', $empresausuario->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
