<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Empresausuario'), ['action' => 'edit', $empresausuario->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Empresausuario'), ['action' => 'delete', $empresausuario->id], ['confirm' => __('Tem certeza que deseja deletar esse registro # {0}?', $empresausuario->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Empresausuarios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Empresausuario'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Empresas'), ['controller' => 'Empresas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Empresa'), ['controller' => 'Empresas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="empresausuarios view large-9 medium-8 columns content">
    <h3><?= h($empresausuario->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $empresausuario->has('user') ? $this->Html->link($empresausuario->user->id, ['controller' => 'Users', 'action' => 'view', $empresausuario->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Empresa') ?></th>
            <td><?= $empresausuario->has('empresa') ? $this->Html->link($empresausuario->empresa->id, ['controller' => 'Empresas', 'action' => 'view', $empresausuario->empresa->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($empresausuario->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dt Cadastro') ?></th>
            <td><?= h($empresausuario->dt_cadastro) ?></td>
        </tr>
    </table>
</div>
