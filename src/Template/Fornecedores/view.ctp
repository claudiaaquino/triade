<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= $fornecedore->nome ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Nome da Empresa/Cliente') ?></p>
                        <p><?= $fornecedore->nome ? $fornecedore->nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Cnpj') ?></p>
                        <p><?= $fornecedore->cnpj ? $fornecedore->cnpj : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Cpf') ?></p>
                        <p><?= $fornecedore->cpf ? $fornecedore->cpf : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Telefone') ?></p>
                        <p><?= $fornecedore->telefone ? $fornecedore->telefone : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Email') ?></p>
                        <p><?= $fornecedore->email ? $fornecedore->email : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Nome do Responsavel') ?></p>
                        <p><?= $fornecedore->nome_responsavel ? $fornecedore->nome_responsavel : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Cargo Responsavel') ?></p>
                        <p><?= $fornecedore->cargo_responsavel ? $fornecedore->cargo_responsavel : 'Não Informado'; ?></p>

                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Endereco') ?></p>
                        <p><?= $fornecedore->endereco ? $fornecedore->endereco : 'Não Informado'; ?></p>

                        <p class="title"><?= __('End Numero') ?></p>
                        <p><?= $fornecedore->end_numero ? $fornecedore->end_numero : 'Não Informado'; ?></p>

                        <p class="title"><?= __('End Complemento') ?></p>
                        <p><?= $fornecedore->end_complemento ? $fornecedore->end_complemento : 'Não Informado'; ?></p>

                        <p class="title"><?= __('End Bairro') ?></p>
                        <p><?= $fornecedore->end_bairro ? $fornecedore->end_bairro : 'Não Informado'; ?></p>

                        <p class="title"><?= __('End Cep') ?></p>
                        <p><?= $fornecedore->end_cep ? $fornecedore->end_cep : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Estado') ?></p>
                        <p><?= $fornecedore->has('estado') ? $fornecedore->estado->estado_sigla : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Cidade') ?></p>
                        <p><?= $fornecedore->has('cidade') ? $fornecedore->cidade->cidade_nome : 'Não Informado'; ?></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">



                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $fornecedore->dt_cadastro ? $fornecedore->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $fornecedore->last_update ? $fornecedore->last_update : 'Não Informado'; ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($fornecedore->contabancarias)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Contas Bancárias </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Banco') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Nome Títular') ?></th>
                                    <th scope="col"  class="column-title"><?= __('CPF/CNPJ') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Operacao') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Agencia') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Ag Digito') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Conta') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Co Digito') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th class="bulk-actions" colspan="12">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($fornecedore->contabancarias as $contabancarias):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($contabancarias->id) ?>">
                                        </td>
                                        <td><?= h($contabancarias->banco->nome) ?></td>
                                        <td><?= h($contabancarias->nome) ?></td>
                                        <td><?= h($contabancarias->cpf) ?></td>
                                        <td><?= h($contabancarias->operacao) ?></td>
                                        <td><?= h($contabancarias->agencia) ?></td>
                                        <td><?= h($contabancarias->ag_digito) ?></td>
                                        <td><?= h($contabancarias->conta) ?></td>
                                        <td><?= h($contabancarias->co_digito) ?></td>
                                        <td><?= h($contabancarias->dt_cadastro) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Contabancarias', 'action' => 'view', $contabancarias->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Contabancarias', 'action' => 'edit', $contabancarias->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Contabancarias', 'action' => 'delete', $contabancarias->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $contabancarias->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (!empty($fornecedore->movimentacaobancarias)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Movimentações de Caixa com esse Fornecedor </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Dt Lançamento') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Valor') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Conta Bancaria') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Forma de Pagamento') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Status Pagamento') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($fornecedore->movimentacaobancarias as $movimentacaobancarias):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($movimentacaobancarias->id) ?>">
                                        </td>
                                        <td><?= h($movimentacaobancarias->dt_cadastro) ?></td>
                                        <td>R$ <?= h($movimentacaobancarias->valor) ?></td>                                        
                                        <td><?= $movimentacaobancarias->has('contabancaria') ? $movimentacaobancarias->contabancaria->banco->nome . ($movimentacaobancarias->contabancaria->conta ? ' (' . $movimentacaobancarias->contabancaria->conta . '-' . $movimentacaobancarias->contabancaria->co_digito . ')' : '') : 'Não informado' ?></td>
                                        <td><?= $movimentacaobancarias->has('formaspagamento') ? $movimentacaobancarias->formaspagamento->descricao . ' ' . ($movimentacaobancarias->avista ? ' à vista' : ' parcelado') : 'Não Informado' ?></td>
                                        <td><?= $movimentacaobancarias->pago ? 'PAGO' : 'PENDENTE' ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Movimentacaobancarias', 'action' => 'view', $movimentacaobancarias->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Movimentacaobancarias', 'action' => 'edit', $movimentacaobancarias->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Movimentacaobancarias', 'action' => 'delete', $movimentacaobancarias->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $movimentacaobancarias->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


