
<div class="page-title">
    <div class="title_left">
        <h3><?= __('Cadastrar Funcionariodependente') ?></h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Insira as informações para <?= __('cadastrar Funcionariodependente') ?>. <small>* obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($funcionariodependente, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                                
                                    
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="funcionario_id">funcionario_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('funcionario_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $funcionarios]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="parentesco_id">parentesco_id <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('parentesco_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $parentescos]);?>
                                </div> 
                            </div> 
                                            
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">nome <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('nome', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_nascimento">dt_nascimento <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('dt_nascimento', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cpf">cpf <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('cpf', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rg">rg <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('rg', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                           
                                    
                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last_updated_fields">last_updated_fields <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <?= $this->Form->input('last_updated_fields', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                    </div> 
                                </div>                         
                                    
                                           
                                    
                                           
                    
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Cadastrar</button>
                        <?= $this->Html->link(__('Voltar'), ['action' => 'index'], ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>