

<div class="page-title">
    <div class="title_left">
        <h3><?= __('Funcionariodependentes') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2><?= $this->Html->link(__('  Cadastrar'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('funcionario_id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('parentesco_id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('nome') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_nascimento') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('cpf') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('rg') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_cadastro') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('last_updated_fields') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('last_update') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('status') ?></th>
                                                                
                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($funcionariodependentes as $funcionariodependente){ ?>                                
                                <tr class="<?= $cor ?> pointer">
                                                                         
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?=$funcionariodependente->id?>">
                                    </td>
                                    
                                                                                       <td><?= $this->Number->format($funcionariodependente->id) ?></td>
                                                                                                     <td><?= $funcionariodependente->has('funcionario') ? $this->Html->link($funcionariodependente->funcionario->nome, ['controller' => 'Funcionarios', 'action' => 'view', $funcionariodependente->funcionario->id]) : '' ?></td>
                                                                                                         <td><?= $funcionariodependente->has('parentesco') ? $this->Html->link($funcionariodependente->parentesco->descricao, ['controller' => 'Parentescos', 'action' => 'view', $funcionariodependente->parentesco->id]) : '' ?></td>
                                                                                                     <td><?= h($funcionariodependente->nome) ?></td>
                                                                                                 <td><?= h($funcionariodependente->dt_nascimento) ?></td>
                                                                                                 <td><?= h($funcionariodependente->cpf) ?></td>
                                                                                                 <td><?= h($funcionariodependente->rg) ?></td>
                                                                                                 <td><?= h($funcionariodependente->dt_cadastro) ?></td>
                                                                                                 <td><?= h($funcionariodependente->last_updated_fields) ?></td>
                                                                                                 <td><?= h($funcionariodependente->last_update) ?></td>
                                                                                                  <td><?= $this->Number->format($funcionariodependente->status) ?></td>
                                                                                    
                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $funcionariodependente->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $funcionariodependente->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $funcionariodependente->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $funcionariodependente->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
