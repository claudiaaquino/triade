<div class="page-title">
    <div class="title_left">
        <h3><?= __('Funcionariodependentes') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($funcionariodependente->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Funcionario') ?></p>
                                    <p><?= $funcionariodependente->has('funcionario') ? $this->Html->link($funcionariodependente->funcionario->nome, ['controller' => 'Funcionarios', 'action' => 'view', $funcionariodependente->funcionario->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Parentesco') ?></p>
                                    <p><?= $funcionariodependente->has('parentesco') ? $this->Html->link($funcionariodependente->parentesco->descricao, ['controller' => 'Parentescos', 'action' => 'view', $funcionariodependente->parentesco->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Nome') ?></p>
                                    <p><?= h($funcionariodependente->nome) ?></p>
                                    </tr>
                                                                                                                    <p class="title"><?= __('Cpf') ?></p>
                                    <p><?= h($funcionariodependente->cpf) ?></p>
                                    </tr>
                                                                                                                    <p class="title"><?= __('Rg') ?></p>
                                    <p><?= h($funcionariodependente->rg) ?></p>
                                    </tr>
                                                                                                                    <p class="title"><?= __('Last Updated Fields') ?></p>
                                    <p><?= h($funcionariodependente->last_updated_fields) ?></p>
                                    </tr>
                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $this->Number->format($funcionariodependente->id) ?></p>

                                                    
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $this->Number->format($funcionariodependente->status) ?></p>

                                                                                                                                
                        <p class="title"><?= __('Dt Nascimento') ?></p>
                                <p><?= h($funcionariodependente->dt_nascimento) ?></p>

                                                    
                        <p class="title"><?= __('Dt Cadastro') ?></p>
                                <p><?= h($funcionariodependente->dt_cadastro) ?></p>

                                                    
                        <p class="title"><?= __('Last Update') ?></p>
                                <p><?= h($funcionariodependente->last_update) ?></p>

                                                                                                                            <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
</div>


