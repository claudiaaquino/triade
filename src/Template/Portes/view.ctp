<div class="page-title">
    <div class="title_left">
        <h3>Detalhes do Porte</h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($porte->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Status</p>
                        <p><?= $porte->status == 1 ? 'Ativo' : 'Inativo' ?></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Ultima Atualização</p>
                        <p><?= $porte->last_update; ?></p>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        if (!empty($porte->empresas)) {
            ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Empresas relacionadas </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col" class="column-title"><?= __('ID') ?></th>
                                    <th scope="col" class="column-title"><?= __('Nome Fantasia') ?></th>
                                    <th scope="col" class="column-title"><?= __('CNPJ') ?></th>
                                    <th scope="col" class="column-title"><?= __('Endereço') ?></th>
                                    <th scope="col" class="column-title"><?= __('CEP') ?></th>
                                    <th scope="col" class="column-title"><?= __('Cidade') ?></th>
                                    <th scope="col" class="column-title"><?= __('Estado') ?></th>
                                    <th scope="col" class="column-title"><?= __('Responsável') ?></th>
                                    <th scope="col" class="column-title"><?= __('Telefone') ?></th>
                                    <th scope="col" class="column-title"><?= __('Última Atualização') ?></th>
                                    <th></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $cor = 'even';
                                foreach ($porte->empresas as $empresas) {
                                    ?>

                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records">
                                        </td>
                                        <td><?= h($empresas->id) ?></td>
                                        <td><?= h($empresas->fantasia) ?></td>
                                        <td><?= h($empresas->cnpj) ?></td>
                                        <td><?php
                                            $complemento = !empty($empresas->end_complemento) ? "({$empresas->end_complemento})" : "";
                                            ?>
                                        <?= !empty($empresas->endereco) ? h($empresas->endereco) . ', ' . h($empresas->end_numero) . " {$complemento} - {$empresas->end_bairro}" : "" ?>
                                        </td>
                                        <td><?= h($empresas->end_cep) ?></td>
                                        <td><?= !empty($empresas->cidade->cidade_nome) ? h($empresas->cidade->cidade_nome) : ""; ?></td>
                                        <td><?= !empty($empresas->estado->estado_sigla) ? $empresas->estado->estado_sigla : "" ?></td>
                                        <td><?= h($empresas->nome_responsavel) ?></td>
                                        <td><?= h($empresas->telefone) ?></td>
                                        <td><?= h($empresas->last_update) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Empresas', 'action' => 'view', $empresas->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Empresas', 'action' => 'edit', $empresas->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Empresas', 'action' => 'delete', $empresas->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Deseja mesmo remover o registro #{0}?', $empresas->id)]) ?>

                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                }
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>