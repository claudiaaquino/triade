<div class="page-title">
    <div class="title_left">
        <h3><?= __('Tarefadocumentos') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($tarefadocumento->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Tarefa') ?></p>
                                    <p><?= $tarefadocumento->has('tarefa') ? $this->Html->link($tarefadocumento->tarefa->titulo, ['controller' => 'Tarefas', 'action' => 'view', $tarefadocumento->tarefa->id]) : 'Não Informado' ?></p>

                                                                                                                    <p class="title"><?= __('Documento') ?></p>
                                    <p><?= $tarefadocumento->has('documento') ? $this->Html->link($tarefadocumento->documento->nome, ['controller' => 'Documentos', 'action' => 'view', $tarefadocumento->documento->id]) : 'Não Informado' ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $tarefadocumento->id ? $this->Number->format($tarefadocumento->id) : 'Não Informado'; ?></p>

                                                    
                        <p class="title"><?= __('Obs') ?></p>
                                <p><?= $tarefadocumento->obs ? $this->Number->format($tarefadocumento->obs) : 'Não Informado'; ?></p>

                                                                                                                                                    <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
</div>


