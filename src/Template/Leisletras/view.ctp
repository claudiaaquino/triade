<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Letra - <?= h($leisletra->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Lei/Norma') ?></p>
                        <p><?= $leisletra->has('leisnorma') ? $this->Html->link($leisletra->leisnorma->numero, ['controller' => 'Leisnormas', 'action' => 'view', $leisletra->leisnorma->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Artigo') ?></p>
                        <p><?= $leisletra->has('leisartigo') ? $this->Html->link($leisletra->leisartigo->nome, ['controller' => 'Leisartigos', 'action' => 'view', $leisletra->leisartigo->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Letra') ?></p>
                        <p><?= $leisletra->nome ? $leisletra->nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Descrição da Letra') ?></p>
                        <p>  <?= $leisletra->descricao ? $this->Text->autoParagraph(h($leisletra->descricao)) : 'Não Informado'; ?></p>
                    </div>
                </div><div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Ordem que aparece') ?></p>
                        <p><?= $leisletra->ordem ? $this->Number->format($leisletra->ordem) : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Dt. Cadastro') ?></p>
                        <p><?= $leisletra->dt_cadastro ? $leisletra->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $leisletra->last_update ? $leisletra->last_update : 'Não modificado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $leisletra->status ? __('Ativo') : __('Inativo'); ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $leisletra->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $leisletra->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisletra->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


