<div class="page-title">
    <div class="title_left">
        <h3><?= __('Cursos') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($curso->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Nivelescolaridade') ?></p>
                                    <p><?= $curso->has('nivelescolaridade') ? $this->Html->link($curso->nivelescolaridade->descricao, ['controller' => 'Nivelescolaridades', 'action' => 'view', $curso->nivelescolaridade->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Nome') ?></p>
                                    <p><?= h($curso->nome) ?></p>
                                    </tr>
                                                                                                                    <p class="title"><?= __('Descricao') ?></p>
                                    <p><?= h($curso->descricao) ?></p>
                                    </tr>
                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $this->Number->format($curso->id) ?></p>

                                                    
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $this->Number->format($curso->status) ?></p>

                                                                                                                                
                        <p class="title"><?= __('Dt Cadastro') ?></p>
                                <p><?= h($curso->dt_cadastro) ?></p>

                                                    
                        <p class="title"><?= __('Last Update') ?></p>
                                <p><?= h($curso->last_update) ?></p>

                                                                                                                            <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <?php if (!empty($curso->funcionarioescolaridades)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Funcionarioescolaridades Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Funcionario Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nivelescolaridade Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Curso Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Escola Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Escolacampus Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Matricula') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Periodo') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Turno Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Formatura') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Inicio') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Updated Fields') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php ?>
                                <tr>


                                </tr>

                                <?php
                                $cor = 'even';
                                foreach ($curso->funcionarioescolaridades as $funcionarioescolaridades):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= h($funcionarioescolaridades->id) ?>">
                                    </td>
                                                                    <td><?= h($funcionarioescolaridades->id) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->funcionario_id) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->nivelescolaridade_id) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->curso_id) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->escola_id) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->escolacampus_id) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->matricula) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->periodo) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->turno_id) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->formatura) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->inicio) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->dt_cadastro) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->last_updated_fields) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->last_update) ?></td>
                                                                    <td><?= h($funcionarioescolaridades->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Funcionarioescolaridades', 'action' => 'view', $funcionarioescolaridades->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Funcionarioescolaridades', 'action' => 'edit', $funcionarioescolaridades->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Funcionarioescolaridades', 'action' => 'delete', $funcionarioescolaridades->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $funcionarioescolaridades->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
            </div>
</div>


