<div class="page-title">
    <div class="title_left">
        <h3><?= __('Movimentacaotipos') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($movimentacaotipo->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Descricao') ?></p>
                                    <p><?= $movimentacaotipo->descricao ? $movimentacaotipo->descricao : 'Não Informado'; ?></p>

                                                                                                                    <p class="title"><?= __('Evento') ?></p>
                                    <p><?= $movimentacaotipo->evento ? $movimentacaotipo->evento : 'Não Informado'; ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $movimentacaotipo->id ? $this->Number->format($movimentacaotipo->id) : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Dt Cadastro') ?></p>
                                <p><?= $movimentacaotipo->dt_cadastro ? $movimentacaotipo->dt_cadastro : 'Não Informado'; ?></p>

                                                    
                        <p class="title"><?= __('Last Update') ?></p>
                                <p><?= $movimentacaotipo->last_update ? $movimentacaotipo->last_update : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $movimentacaotipo->status ? __('Ativo') : __('Desativado'); ?></p>

                                                                                                    <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <?php if (!empty($movimentacaotipo->livrocaixas)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Livrocaixas Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Num Notafiscal') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Num Recibo') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Movimentacaotipo Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Formaspagamento Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cliente Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Fornecedor Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Avista') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Valor') ?></th>
                                                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($movimentacaotipo->livrocaixas as $livrocaixas):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= h($livrocaixas->id) ?>">
                                    </td>
                                                                    <td><?= h($livrocaixas->id) ?></td>
                                                                    <td><?= h($livrocaixas->num_notafiscal) ?></td>
                                                                    <td><?= h($livrocaixas->num_recibo) ?></td>
                                                                    <td><?= h($livrocaixas->movimentacaotipo_id) ?></td>
                                                                    <td><?= h($livrocaixas->formaspagamento_id) ?></td>
                                                                    <td><?= h($livrocaixas->cliente_id) ?></td>
                                                                    <td><?= h($livrocaixas->fornecedor_id) ?></td>
                                                                    <td><?= h($livrocaixas->avista) ?></td>
                                                                    <td><?= h($livrocaixas->valor) ?></td>
                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Livrocaixas', 'action' => 'view', $livrocaixas->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Livrocaixas', 'action' => 'edit', $livrocaixas->id], [ 'class' => "btn btn-info btn-xs"]) ?>
    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Livrocaixas', 'action' => 'delete', $livrocaixas->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $livrocaixas->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
            <?php if (!empty($movimentacaotipo->movimentacaobancarias)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Movimentacaobancarias Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Empresa Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Movimentacaotipo Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Fornecedor Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cliente Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Contabancaria Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Formaspagamento Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Saldo Anterior') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Saldo Posterior') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Num Notafiscal') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Num Recibo') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Valor') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Avista') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Pago') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($movimentacaotipo->movimentacaobancarias as $movimentacaobancarias):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= h($movimentacaobancarias->id) ?>">
                                    </td>
                                                                    <td><?= h($movimentacaobancarias->id) ?></td>
                                                                    <td><?= h($movimentacaobancarias->empresa_id) ?></td>
                                                                    <td><?= h($movimentacaobancarias->movimentacaotipo_id) ?></td>
                                                                    <td><?= h($movimentacaobancarias->fornecedor_id) ?></td>
                                                                    <td><?= h($movimentacaobancarias->cliente_id) ?></td>
                                                                    <td><?= h($movimentacaobancarias->contabancaria_id) ?></td>
                                                                    <td><?= h($movimentacaobancarias->formaspagamento_id) ?></td>
                                                                    <td><?= h($movimentacaobancarias->saldo_anterior) ?></td>
                                                                    <td><?= h($movimentacaobancarias->saldo_posterior) ?></td>
                                                                    <td><?= h($movimentacaobancarias->num_notafiscal) ?></td>
                                                                    <td><?= h($movimentacaobancarias->num_recibo) ?></td>
                                                                    <td><?= h($movimentacaobancarias->valor) ?></td>
                                                                    <td><?= h($movimentacaobancarias->avista) ?></td>
                                                                    <td><?= h($movimentacaobancarias->pago) ?></td>
                                                                    <td><?= h($movimentacaobancarias->dt_cadastro) ?></td>
                                                                    <td><?= h($movimentacaobancarias->last_update) ?></td>
                                                                    <td><?= h($movimentacaobancarias->status) ?></td>
                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Movimentacaobancarias', 'action' => 'view', $movimentacaobancarias->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Movimentacaobancarias', 'action' => 'edit', $movimentacaobancarias->id], [ 'class' => "btn btn-info btn-xs"]) ?>
    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Movimentacaobancarias', 'action' => 'delete', $movimentacaobancarias->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $movimentacaobancarias->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>


