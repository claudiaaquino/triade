<div class="page-title">
    <div class="title_left">
        <h3>Detalhes da Classes</h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($cnaeclass->denominacao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Grupo</p>
                        <p><?= $cnaeclass->has('cnaegroup') ? $this->Html->link($cnaeclass->cnaegroup->denominacao, ['controller' => 'Cnaegroups', 'action' => 'view', $cnaeclass->cnaegroup->id]) : '' ?></p>
                        <p class="title">Classe</p>
                        <p><?= $cnaeclass->classe ?></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Simples Nacional</p>
                        <p><?= $cnaeclass->simplesnacional == 1 ? 'Sim' : 'Não' ?></p>
                        <p class="title">Ultima Atualização</p>
                        <p><?= $cnaeclass->last_update; ?></p>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        if (!empty($cnaeclass->empresacnaes)) {
            ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Empresas com esta Classe </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col" class="column-title"><?= __('ID') ?></th>
                                    <th scope="col" class="column-title"><?= __('Empresa') ?></th>
                                    <th scope="col" class="column-title"><?= __('CNPJ') ?></th>
                                    <th scope="col" class="column-title"><?= __('Responsável') ?></th>
                                    <th scope="col" class="column-title"><?= __('Data de Cadastro') ?></th>
                                    <th scope="col" class="column-title"><?= __('Última Atualização') ?></th>
                                    <th></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $cor = 'even';
                                foreach ($cnaeclass->empresacnaes as $empresacnaes) {
                                    //echo "<pre>";print_r($empresacnaes);exit;
                                    ?>

                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records">
                                        </td>
                                        <td><?= h($empresacnaes->id) ?></td>
                                        <td><?= h($empresacnaes->empresa->fantasia) ?></td>
                                        <td><?= h($empresacnaes->cnpj) ?></td>
                                        <td><?= h($empresacnaes->nome_responsavel) ?></td>
                                        <td><?= h($empresacnaes->dt_cadastro) ?></td>
                                        <td><?= h($empresacnaes->last_update) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Empresas', 'action' => 'view', $empresacnaes->empresa->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Empresas', 'action' => 'edit', $empresacnaes->empresa->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Empresas', 'action' => 'delete', $empresacnaes->empresa->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Deseja mesmo remover o registro #{0}?', $empresacnaes->empresa->id)]) ?>

                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                }
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>s