<div class="page-title">
    <div class="title_left">
        <h3>Classes</h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2><?= $this->Html->link(__('  Cadastrar'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?></h2>
                <!--<h2><small></small></h2>-->
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>

                                <th scope="col" class="column-title"><?= $this->Paginator->sort('id') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('cnaegroup_id', 'Grupo') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('classe', 'Classe') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('denominacao', 'Denominação') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('simplesnacional', 'Simples Nacional') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('last_update', "Ult. Atualização") ?></th>                               
                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $cor = 'even';
                            foreach ($cnaeclasses as $cnaeclass) {
                                
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records">
                                    </td>
                                    <td><?= $this->Number->format($cnaeclass->id) ?></td>
                                    <td><?= $this->Html->link($cnaeclass->cnaegroup->denominacao, ['controller' => 'Cnaegroups', 'action' => 'view', $cnaeclass->cnaegroup->id]) ?></td>
                                    <td><?= h($cnaeclass->classe) ?></td>
                                    <td><?= h($cnaeclass->denominacao) ?></td>
                                    <td><?= h($cnaeclass->simplesnacional) ?></td>
                                    <td><?= h($cnaeclass->last_update) ?></td>
                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $cnaeclass->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $cnaeclass->id], ['class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $cnaeclass->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Deseja mesmo remover o registro #{0}?', $cnaeclass->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>