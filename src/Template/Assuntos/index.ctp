<?= $this->Html->css('/vendors/pnotify/dist/pnotify.css'); ?>
<?= $this->Html->css('/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css'); ?>
<?= $this->Html->css('/vendors/uploadify/uploadify.css') ?>
<?= $this->Html->script('/vendors/uploadify/jquery.uploadify.min.js', array('inline' => false)); ?>
<?php $session = $this->request->session()->id(); ?>
<!--MENU DE ASSUNTOS-->
<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view">
        <div class="clearfix"></div>
        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side  hidden-print main_menu">
            <div class="menu_section">
                <h3>Assuntos</h3>
                <?php if ($assuntos) { ?>
                    <ul class="nav side-menu">
                        <?php foreach ($assuntos as $assunto) {
                            ?>   
                            <li>
                                <a class="assunto-link" id="<?= $assunto->id ?>" descricao="<?= h($assunto->descricao) ?>">
                                    <i class="fa fa-chevron-right"></i> 
                                    <?= h($assunto->descricao) ?>
                                </a>  
                                <?php if ($assunto->assuntosfilhos) { ?>
                                    <ul class="nav child_menu sublist">
                                        <?php foreach ($assunto->assuntosfilhos as $subassunto) { ?>
                                            <li>
                                                <a class="subassunto-link"  ondblclick="toogleSubAssuntoList(this.id);"  onclick="redirectView(this.id);" id="<?= $subassunto->id ?>"  descricao="<?= h($subassunto->descricao) ?>">
                                                    <i class="fa fa-chevron-right"></i> 
                                                    <?= h($subassunto->descricao) ?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>                                    
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>

            </div>
            <div class="sidebar-footer">
                <a type='button' data-toggle="modal" data-placement="top" data-tt="tooltip" title="Adicionar Novo Assunto" href="javascript:void(0);" data-target=".modal-add-assunto">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
                <div class="col-md-12 col-sm-12 col-xs-12  top_search">
                    <!--<div class="input-group">-->
                    <?= $this->Form->input('assuntos_id', ['options' => $todosassuntos, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => '   ']); ?>
                    <!--</div>-->
                </div>
            </div>
        </div>
    </div>
</div>

<!--BARRA SUPERIOR-->
<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

                    <div class="input-group">
                        <input type="text" id="txt-search" name="txt-search" class="form-control" placeholder="Procurar palavras chave...">
                        <span class="input-group-btn">
                            <button id="btn-search" class="btn btn-default" type="button" >Pesquisar</button>
                        </span>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>

<!--CONTEÚDO PRINCIPAL-->
<div class="right_col" role="main">
    <!--RESULTADO DA PESQUISA-->
    <div id="result-search" style="display: none;">
        <div class="x_panel">
            <div class="x_title">
                <h2>Resultado da pesquisa</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                    </li>                   
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <ul  id="content-result-search" class="list-unstyled msg_list"></ul>

                </div>
            </div>
        </div>
    </div>
    <!--DETALHES DO ASSUNTO-->
    <div id="assunto-details" class="row">
        <!--PANELS DA ESQUESDA-->
        <div class="col-md-6 col-sm-6 col-xs-12 info-assunto"  style="display: none">
            <!--PANEL PRINCIPAL - COM DESCRIÇÃO DO ASSUNTO E O ASSUNTO SUPERIOR-->
            <div class="x_panel">
                <div class="x_title">
                    <h2>Editar Assunto <small>* campos obrigatórios</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                        </li>                   
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"  style="display: none">
                    <form id='form-info-assunto' method="POST" class="form-horizontal form-label-left">
                        <?= $this->Flash->render() ?>
                        <input type="hidden" name='current-assunto-id' id='current-assunto-id' value="<?= $assunto->id ?>">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Assunto <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?= $this->Form->input('current-assunto-descricao', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                            </div> 
                        </div>        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="assunto_id">Assunto superior    (se houver)
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?= $this->Form->input('current-supassunto-id', ['options' => $todosassuntos, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => '   ', 'style' => 'width:100%']); ?>

                            </div> 
                        </div>              
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-3">
                                <button type="button" id="btn-edit-assunto" class="btn btn-success">Salvar Alteração</button>
                                <button type="button" id="btn-delete-assunto" class="btn btn-danger">Deletar</button>
                                <?= $this->Html->link(__('Voltar'), ['action' => 'index'], ['class' => "btn btn-primary"]) ?>                        
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--PANEL COM OS SUBASSUNTOS-->
            <div class="x_panel">
                <div class="x_title">
                    <h2>Sub-assuntos</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><button id='btn-modal-subassuntos' type="button" class="btn btn-dark fa fa-plus" data-toggle="modal" data-target=".modal-add-subassuntos"> Adicionar</button></li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>                   
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" id="subassuntos-assunto" style="display: none">

                </div>
            </div>
            <!--PANEL COM ANOTAÇÕES GERAIS-->
            <div class="x_panel">
                <div class="x_title">
                    <h2>Anotações Gerais</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><button id='btn-modal-anotacao' type="button" class="btn btn-dark fa fa-plus" data-toggle="modal" data-target=".modal-add-anotacao"> Adicionar</button></li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>                   
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" id="anotacao-assunto" style="display: none">

                </div>
            </div>
            <!--PANEL COM PERGUNTAS E RESPOSTAS-->
            <div class="x_panel">
                <div class="x_title">
                    <h2>Perguntas/Respostas</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><button id='btn-modal-questionario' type="button" class="btn btn-dark fa fa-plus" data-toggle="modal" data-target=".modal-add-questionario"> Adicionar</button></li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>                   
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" id="questionario-assunto"  style="display: none">

                </div>
            </div>

        </div>
        <!--PANELS DA DIREITA-->
        <div class="col-md-6 col-sm-6 col-xs-12 info-assunto" style="display: none">
            <!--PANEL COM VINCULOS DE LEIS-->
            <div class="x_panel">
                <div class="x_title">
                    <h2>Legislativo</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><button id='btn-modal-legislativo' type="button" class="btn btn-dark fa fa-plus" data-toggle="modal" data-target=".modal-add-legislativo"> Adicionar</button></li>
                        <li><a type="button" href="/leisnormas/index" target="_blank" class="btn btn-success fa fa-external-link"> Leis</a></li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>                  
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" id="legislativo-assunto"  style="display: none">

                </div>
            </div>
            <!--PANEL INFORMATIVOS PARA TELAS DO SISTEMA-->
            <div class="x_panel" style="display: none">
                <div class="x_title">
                    <h2>Informativos no Sistema</h2>
                    <ul class="nav navbar-right panel_toolbox">                     
                        <li><button type="button" id='btn-add-informativo' class="btn btn-dark fa fa-plus"> Adicionar</button></li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>                   
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" id="informativo-assunto" style="display: none">

                </div>
            </div>
            <!--PANEL COM LEMBRETES PARA A EXECUÇÃO DE SERVIÇOS PELO SISTEMA-->
            <div class="x_panel" style="display: none">
                <div class="x_title">
                    <h2>Lembretes em Serviços</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" id='btn-add-lembrete' class="btn btn-dark fa fa-plus"> Adicionar</button></li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>                   
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" id="lembrete-assunto" style="display: none">

                </div>
            </div>
            <!--PANEL PARA CONSULTAR E INSERIR DOCUMENTOS-->
            <div class="x_panel">
                <div class="x_title">
                    <h2>Documentos</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><button id='btn-modal-documento' type="button" class="btn btn-dark fa fa-plus" data-toggle="modal" data-target=".modal-add-documento"> Adicionar</button></li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>                  
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" id="documentos-assunto"  style="display: none">

                </div>
            </div>
            <!--PANEL COM TAGS-->
            <div class="x_panel">
                <div class="x_title">
                    <h2>TAGS</h2>
                    <ul class="nav navbar-right panel_toolbox">   <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                   
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" id="tags-assunto">
                    <?= $this->Form->input('tags_id', ["class" => "form-control col-md-12 col-xs-12", 'label' => false, 'empty' => '', "style" => "width: 100%", 'multiple' => 'multiple']); ?>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /CONTEÚDO PRINCIPAL -->



<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js'); ?>
<?= $this->Html->script('/vendors/pnotify/dist/pnotify.js'); ?>
<?= $this->Html->script('/vendors/ckeditor/ckeditor.js'); ?>
<?= $this->Html->script('/js/ajax/index.js'); ?>
<!--SCRIPTS-->
<script>
    var submenusclicados = new Array();
    var submenusabertos = new Array();
    var current_assunto = 0;
    var current_leiassunto = 0;
    $(document).ready(function () {
        configInicial();
        eventosIniciais();
    });
    function configInicial() {
        /*LADO ESQUERDO*/
        $("#assuntos-id").css('z-index', '9999');
        $("#assunto-id").select2({placeholder: 'selecione o assunto'});
        $("#assuntos-id").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'pesquisar um assunto'});
        $("[data-tt=tooltip]").tooltip();
        $("#tags-id").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'digite tags aqui..'});


        /*EVENTOS LADO DIREITO*/
        $("#leisnorma-id").select2();
        $("#subassunto-id").select2({placeholder: 'Selecione um sub-assunto'});
        $("#tipo-anotacao").select2({dropdownParent: $(".modal-add-anotacao"), tokenSeparators: [',', ';'], tags: true, placeholder: 'Selecione um tipo ou digite um novo'});
        $("#current-supassunto-id").select2({placeholder: 'selecione se necessário'});
        $("#assunto-id").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione ou digite um novo'});

        var urlroot = '<?= $this->request->webroot ?>';
        $('#file_upload').uploadify({
            'formData': {'documento-nome': '', 'assunto_id': ''},
            'auto': false,
            'swf': '/vendors/uploadify/uploadify.swf',
            'uploader': '/assuntos/adddocumento/<?= $session ?>',
            'buttonText': 'SELECIONAR ARQUIVO',
            'width': 180,
            'onUploadError': function (file, errorCode, errorMsg, errorString) {
                console.log(errorMsg);
            },
            'onUploadSuccess': function (file, data, response) {
                refreshDocList();
                $('#documento-nome').val('');
                //fecha a modal
                $(".modal-add-documento").find(".close").trigger("click");
                //abre a tab com os documentos
                $('#documentos-assunto').slideDown();
            },
            'onUploadStart': function (file) {
                $("#file_upload").uploadify("settings", 'formData', {'documento-nome': $('#documento-nome').val(), 'assunto_id': current_assunto});
            }

        });

    }
    function eventosIniciais() {
        /*EVENTOS TOPO*/
        $('#menu_toggle').on('click', function () {
            $(".sidebar-footer").slideToggle();
        });
        $('#btn-search').on('click', function () {
            searchAssunto();
        });

        $('#txt-search').keypress(function (event) {
            if (event.keyCode == '13' || event.which == '13') {
                searchAssunto();
            }
        });

        /*EVENTOS LADO ESQUERDO*/
        $('.assunto-link').on('dblclick', function () {
            $(this).children('i').toggleClass('fa-chevron-right fa-chevron-down');
        });

        $('.assunto-link,.subassunto-link').on('click', function () {
            redirectView($(this).prop('id'));
        });

        $("#assuntos-id").change(function () {
            $("#assunto_title").html($("#assuntos-id option:selected").text());
            redirectView($(this).val());
        });

        $('#tags-id').on('select2:select', function (evt) {
            insertTag(evt.params.data);
        });
        $('#tags-id').on('select2:unselect', function (evt) {
            deleteTag(evt.params.data);
        });


        /*EVENTOS LADO DIREITO*/
        refreshListLeis();//carrega o list de leis na inicialização da pagina

        $("#btn-modal-anotacao").click(function () {
            if (CKEDITOR.instances.anotacao)
                CKEDITOR.instances.anotacao.destroy();
            $('#anotacao').val('');
            $("#btn-add-anotacao").show();
            $("#btn-edit-anotacao").hide();
            CKEDITOR.replace('anotacao');
        });

        $("#btn-add-subassunto").click(function () {
            addSubassunto();
        });
        $("#btn-add-anotacao").click(function () {
            addAnotacao();
        });
        $("#btn-add-questionario").click(function () {
            addQuestionario();
        });
        $("#btn-add-lei").click(function () {
            addLei();
        });
        $("#btn-add-documento").click(function () {
            $('#file_upload').uploadify('upload', '*');
        });

        $("#btn-edit-assunto").click(function () {
            editAssunto();
        });
        $("#btn-delete-assunto").click(function () {
            if (confirm("Tem certeza que deseja deletar esse assunto?")) {
                location.href = $('#urlroot').val() + 'assuntos/delete/' + current_assunto;
            }
        });
        $("#btn-edit-anotacao").click(function () {
            editAnotacao();
            renderAnotacoes();
        });

        /*configs legislativo*/
        $(".btn-enable-edit").click(function () {
            $('#editor').html($('#lei-original').html());
            $('#lei-original').slideUp();
            $('#editor').show();
            $("#btn-close-lei").html('Cancelar');
            $(".btn-enable-edit").slideUp();
            $(".btn-edit-lei").slideDown();
            CKEDITOR.replace('editor');
        });

        $(".btn-edit-lei").click(function () {
            $("#btn-close-lei").html('Fechar');
            $(".btn-enable-edit").slideDown();
            $(".btn-edit-lei").slideUp();
            //pegar conteudo editado e substituir o original no html
            $('#lei-original').html(CKEDITOR.instances.editor.getData());
            editLeiAssunto(current_leiassunto);
            refreshModalLei();
        });

    }
    function toogleSubAssuntoList(elem) {

        if (submenusclicados.indexOf(elem) < 0) {// se o elemento não está nalista de menus que foram clicados, então vai carregar ele por ajax
            submenusclicados.push(elem);
            abreSubAssuntoList(elem);
        }

        //            $("#" + elem).next('.sublist').slideToggle();
        $('#' + elem).children('i').toggleClass('fa-chevron-right fa-chevron-down');
    }
    function abreSubAssuntoList(id) {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/subassuntosajax/' + id,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                var assunto = data.assunto;
                var subassuntos = data.assunto.assuntosfilhos;
                if (subassuntos.length) {
                    var html = "<ul class='nav child_menu sublist'>";
                    $.each(subassuntos, function (i, item) {
                        html += "<li><a class='subassunto-link' ondblclick='toogleSubAssuntoList(this.id);' onclick='redirectView(this.id);' id='" + subassuntos[i].id + "' descricao='" + subassuntos[i].descricao + "'><i class ='fa fa-chevron-right'> </i> " + subassuntos[i].descricao + "</a></li>";
                    });
                    html += "</ul>";
                    $('#' + assunto.id).after(html);
                    $("#" + assunto.id).next('.sublist').show();
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function redirectView(elem) {
        $('#assunto-details').show();
        $('.info-assunto').show();
        $('#result-search .x_content').hide();
        if (elem && elem !== current_assunto) {
            current_assunto = elem;

            $.ajax({
                url: $('#urlroot').val() + 'assuntos/getallinfos/' + elem,
                dataType: 'json',
                success: function (data) {
                    var assunto = data.retorno;
                    if (!data.retorno) {
                        new PNotify({text: 'Não foi possível atualizar esse assunto.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                    }

                    /* Exibe informações principais desse assunto */
                    $("#assunto_title").html(assunto.descricao);
                    $('#current-assunto-id').val(assunto.id);
                    $('#current-assunto-descricao').val(assunto.descricao);
                    $('#current-supassunto-id').val(assunto.assunto_id);
                    $('#current-supassunto-id').select2("destroy");
                    $("#current-supassunto-id").select2({placeholder: 'selecione se necessário'});



                    /* Exibe informações relativas à esse assunto */
                    renderAnotacoes(assunto.anotacaoassuntos);
                    renderSubassuntos(assunto.assuntosfilhos);
                    renderQuestionario(assunto.telaquestionarios);
                    renderLegislativo(assunto.leisassuntos);
//                renderInformativos(assunto.assuntosinformativos);
//                renderLembretes(assunto.telaquestionarios);
                    refreshDocList();
                    renderTags(assunto.assuntostags);

                }
                ,
                error: function (a) {
                    console.log(a);
                }
            });
        }
    }
    function editAssunto() {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/edit/' + $("#current-assunto-id").val(),
            data: 'descricao=' + $('#current-assunto-descricao').val() + "&assunto_id=" + $('#current-supassunto-id').val(),
            dataType: 'json',
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'Informação do assunto foi atualizada.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possível atualizar esse assunto.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });

    }
    function searchAssunto() {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/search/',
            type: 'POST',
            data: '&txt-search=' + $('#txt-search').val(),
            dataType: 'json',
            success: function (data) {

                var html = "";
                $.each(data.assuntos, function (i, assunto) {
                    var html_tags = '';
                    if (assunto.assuntostags.length > 0) {
                        var tipo_tags = ['default', 'primary', 'success', 'info', 'warning', 'danger'];
                        html_tags = 'Tags: ';
                        $.each(assunto.assuntostags, function (i, assuntostag) {
                            html_tags += '<span class="label label-' + tipo_tags[(i % 6)] + '">' + assuntostag.tag.descricao + '</span>';
                        });
                    }
                    html += '<li><div class="col-md-12 col-lg-12 col-sm-12"><a href="javascript:void(0);" onclick="redirectView(' + assunto.id + ')"><i class="fa fa-chevron-right"></i> '
                            + assunto.descricao +
                            '<br>' +
                            html_tags +
                            '</a></div></li>';
                });
                $('#content-result-search').html(html);
                $('#result-search .x_content').show();
                $('#result-search').show();
                $('#assunto-details').hide();
            },
            error: function (a) {
                console.log(a);
            }
        });
    }

    /********INSERT-VIEW-DELETE DE ANOTAÇÕES**********/
    function addAnotacao() {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/addanotacao/' + $("#current-assunto-id").val(),
            data: JSON.stringify({anotacao: CKEDITOR.instances.anotacao.getData(), tiponote_id: $('#tipo-anotacao').val(), titulo: $('#titulo-anotacao').val()}),
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    renderAnotacoes(false);

                    //fecha a modal
                    $(".modal-add-anotacao").find(".close").trigger("click");
                    //abre a tab com as anotaçções
                    $('#anotacao-assunto').slideDown();
                    //limpa os campos da modal e da refresh nos selects
                    clearFieldsInfo(['tipo-anotacao', 'anotacao', 'titulo-anotacao']);
                    $("#tipo-anotacao").trigger('change');
                    refreshTipoNotes();


                    new PNotify({text: 'Anotação adicionada com sucesso.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possível adicionar essa anotação.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function renderAnotacoes(anotacoes) {
        if (!anotacoes || anotacoes == null) {
            $.ajax({
                url: $('#urlroot').val() + 'assuntos/getanotacoes/' + current_assunto,
                dataType: 'json',
                success: function (data) {
                    data = data.retorno;
                    if (data) {
                        exibeAnotacoes(data);
                    }
                }
            });
        } else {
            exibeAnotacoes(anotacoes);
        }
    }
    function exibeAnotacoes(anotacoes) {
        $('#anotacao-assunto').html('');
        if (anotacoes.length > 0) {
            $.each(anotacoes, function (index, anotacao) {
                if ($('#tiponote-' + anotacao.tiponote_id).length == 0) {
                    $('#anotacao-assunto').append("<div id='tiponote-" + anotacao.tiponote_id + "'><h4 class='green'> <i class='fa fa-chevron-right'></i>  " + anotacao.tiponote.descricao + "</h4><br></div><br>");
                }

                var html = "<div class='well well-sm' id='anotacao-" + anotacao.id + "'><a href='javascript:void(0);'  onclick='toogleAnotacao(" + anotacao.id + ")'><b><i class='fa fa-chevron-right'></i>  "
                        + anotacao.titulo
                        + " </b></a> <button type='button' class='btn btn-sm btn-success' style='float:right;padding-top: 2px;' onclick='loadDetailsAnotacao(" + anotacao.id + ")'  data-toggle='modal' data-target='.modal-add-anotacao'><i class='fa fa-pencil-square-o'></i></button>"
                        + "<button type='button' class='btn btn-sm btn-danger' style='float:right;padding-top: 2px;' onclick='if(confirm(\"Tem certeza que deseja deletar essa anotação?\")){deleteAnotacao(" + anotacao.id + "," + anotacao.tiponote_id + ");}'><i class='fa fa-trash'></i></button>"
                        + "<div id='conteudoanotacao-" + anotacao.id + "' style='display:none'><br>"
                        + anotacao.anotacao +
                        " </div></div>";

                $('#tiponote-' + anotacao.tiponote_id).append(html);

            });
            $('#anotacao-assunto').slideDown();
        } else {
            $('#anotacao-assunto').slideUp();
        }
    }
    function deleteAnotacao(id, tipo) {
        $.ajax({url: $('#urlroot').val() + 'assuntos/deleteanotacao/' + id, dataType: 'json', type: 'post',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'A anotação foi removida do sistema.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                    $('#anotacao-' + id).remove();// deleta a anotação do html
                    if ($('#tiponote-' + tipo + ' p').length == 0) {
                        $('#tiponote-' + tipo).remove();// deleta a div do tipo de anotação do html, se essa anotação for a unica desse tipo                   
                    }
                } else {
                    new PNotify({text: 'Não foi possível deletar essa anotação.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function refreshTipoNotes() {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/tiponotesajax/',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                data = data.retorno;
                var html = "";
                $.each(data, function (i, item) {
                    html += '<option value="' + i + '">' + item + '</option>';
                });
                $('#tipo-anotacao').html(html);
                $("#tipo-anotacao").select2("destroy");
                $('#tipo-anotacao').select2({dropdownParent: $(".modal-add-anotacao"), tokenSeparators: [',', ';'], tags: true, placeholder: 'Selecione um tipo ou digite um novo'});
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function editAnotacao() {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/editanotacao/' + current_anotacao,
            data: JSON.stringify({anotacao: CKEDITOR.instances.anotacao.getData(), tiponote_id: $('#tipo-anotacao').val(), titulo: $('#titulo-anotacao').val()}),
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    //fecha a modal
                    $(".modal-add-anotacao").find(".close").trigger("click");
                    renderAnotacoes(false);
                    clearFieldsInfo(['tipo-anotacao', 'titulo-anotacao']);
                    refreshTipoNotes();

                    new PNotify({text: 'Anotação atualizada com sucesso.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possível atualizar essa anotação.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function loadDetailsAnotacao(id) {
        current_anotacao = id;
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/getanotacao/' + current_anotacao,
            dataType: 'json',
            success: function (data) {
                data = data.anotacao;
                if (CKEDITOR.instances.anotacao)
                    CKEDITOR.instances.anotacao.destroy();
                $('#titulo-anotacao').val(data.titulo);
                $("#tipo-anotacao option[value='" + data.tiponote_id + "']").attr('selected', true);
                $("#tipo-anotacao").trigger('change');
                $('#anotacao').val(data.anotacao);
                $("#btn-add-anotacao").hide();
                $("#btn-edit-anotacao").show();
                CKEDITOR.replace('anotacao');
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function toogleAnotacao(id) {
        $('#conteudoanotacao-' + id).slideToggle();
        $('#anotacao-' + id).children('i').toggleClass('fa-chevron-right fa-chevron-down');
    }

    /********INSERT-VIEW-DELETE DE SUBASSUNTO**********/
    function addSubassunto() {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/addsubassunto/' + current_assunto,
            data: 'subassunto_id=' + $('#subassunto-id').val() + '&subassunto_descricao=' + $('#subassunto-descricao').val(),
            dataType: 'json',
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    var subassunto = data.retorno;
                    var html = "<p class='well well-sm' id='subassunto-" + subassunto.id + "'>" +
                            "<b>" + fullDateToshortBrDate(subassunto.dt_cadastro) + '</b> - '
                            + subassunto.descricao +
                            " <button type='button' class='btn btn-sm btn-success' style='float:right;padding-top: 2px;' onclick='redirectView(" + subassunto.id + ")'><i class='fa fa-external-link'></i></button>" +
                            " <button type='button' class='btn btn-sm btn-danger' style='float:right;padding-top: 2px;' onclick='if(confirm(\"Tem certeza que deseja deletar esse Sub-assunto?\")){deleteSubassunto(" + subassunto.id + ");}'><i class='fa fa-trash'></i></button>" +
                            " </p>";

                    /**************ADICIONA O SUBASSUNTO EM DETALHES DO ASSUNTO****************/
                    //acrescenta o novo subassunto no html
                    $('#subassuntos-assunto').append(html);
                    //fecha a modal
                    $(".modal-add-subassuntos").find(".close").trigger("click");
                    //abre a tab com as anotaçções
                    $('#subassuntos-assunto').slideDown();
                    //limpa os campos da modal e da refresh nos selects

                    clearFieldsInfo(['subassunto-id', 'subassunto-descricao']);
                    $('#subassunto-id').select2("destroy");
                    $("#subassunto-id").select2({placeholder: 'Selecione um sub-assunto'});

                    /**************ADICIONA O SUBASSUNTO NA HIERARQUIA DO ASSUNTO (NO MENU À ESQUERDA)****************/
                    if ($("#" + current_assunto).next('.sublist').length > 0) {
                        html = "<li><a class='subassunto-link' ondblclick='toogleSubAssuntoList(this.id);' onclick='redirectView(this.id);' id='" + subassunto.id + "' descricao='" + subassunto.descricao + "'><i class ='fa fa-chevron-right'> </i> " + subassunto.descricao + "</a></li>";
                        $("#" + current_assunto).next('.sublist').append(html);
                    } else {
                        html = "<ul class='nav child_menu sublist'>";
                        html += "<li><a class='subassunto-link' ondblclick='toogleSubAssuntoList(this.id);' onclick='redirectView(this.id);' id='" + subassunto.id + "' descricao='" + subassunto.descricao + "'><i class ='fa fa-chevron-right'> </i> " + subassunto.descricao + "</a></li>";
                        html += "</ul>";
                        $('#' + current_assunto).after(html);
                        $("#" + current_assunto).next('.sublist').show();
                    }



                    new PNotify({text: 'Sub-assunto adicionado com sucesso.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possível adicionar esse sub-assunto.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });

    }
    function renderSubassuntos(subassuntos) {
        $('#subassuntos-assunto').html('');
        if (subassuntos.length > 0) {
            $.each(subassuntos, function (index, subassunto) {
                var html = "<p class='well well-sm' id='subassunto-" + subassunto.id + "'>" +
                        "<b>" + fullDateToshortBrDate(subassunto.dt_cadastro) + '</b> - '
                        + subassunto.descricao +
                        " <button type='button' class='btn btn-sm btn-success' style='float:right;padding-top: 2px;' onclick='redirectView(" + subassunto.id + ")'><i class='fa fa-external-link'></i></button>" +
                        " <button type='button' class='btn btn-sm btn-danger' style='float:right;padding-top: 2px;' onclick='if(confirm(\"Tem certeza que deseja deletar esse Sub-assunto?\")){deleteSubassunto(" + subassunto.id + ");}'><i class='fa fa-trash'></i></button>" +
                        " </p>";

                $('#subassuntos-assunto').append(html);

            });
            $('#subassuntos-assunto').slideDown();
        } else {
            $('#subassuntos-assunto').slideUp();
        }
    }
    function deleteSubassunto(id) {
        $.ajax({url: $('#urlroot').val() + 'assuntos/deletesubassunto/' + id, dataType: 'json', type: 'post',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'O sub-assunto foi removido do sistema.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                    $('#subassunto-' + id).remove();// deleta o item do html -> detalhes do assunto
                    $("#" + current_assunto).next("ul").find("a[id='" + id + "']").remove();// deleta o item do html -> do menu de assuntos

                } else {
                    new PNotify({text: 'Não foi possível deletar esse esse sub-assunto.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }


    /********INSERT-VIEW-DELETE DE QUESTIONARIOS**********/
    function addQuestionario() {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/addquestionario/' + $("#current-assunto-id").val(),
            data: 'pergunta=' + $('#pergunta-descricao').val() + "&resposta=" + $('#resposta-descricao').val(),
            dataType: 'json',
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    var questionario = data.retorno;
                    var html = "<div class='well well-sm' id='questionario-" + questionario.id + "'><a href='javascript:void(0);'  onclick='toogleResposta(" + questionario.id + ")'><b><i class='fa fa-chevron-right'></i>  "
                            + questionario.pergunta
                            + "</b></a><button type='button' class='btn btn-sm btn-danger' style='float:right;padding-top: 2px;' onclick='if(confirm(\"Tem certeza que deseja deletar essa Pergunta/Resposta?\")){deleteQuestionario(" + (questionario.id) + ");}'><i class='fa fa-trash'></i></button>"
                            + "<div class='' id='resposta-" + questionario.id + "' style='display:none'><br>"
                            + questionario.resposta
                            + " </div>"
                            + " </div>";

                    $('#questionario-assunto').append(html);
                    //fecha a modal
                    $(".modal-add-questionario").find(".close").trigger("click");
                    //abre a tab com as anotaçções
                    $('#questionario-assunto').slideDown();
                    //limpa os campos da modal e da refresh nos selects
                    clearFieldsInfo(['pergunta-descricao', 'resposta-descricao']);


                    new PNotify({text: 'Pergunta/Resposta adicionada com sucesso.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possível adicionar essa Pergunta/Resposta.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });

    }
    function renderQuestionario(questionarios) {
        $('#questionario-assunto').html('');
        if (questionarios.length > 0) {
            $.each(questionarios, function (index, questionario) {
                var html = "<div class='well well-sm' id='questionario-" + questionario.id + "'><a href='javascript:void(0);'  onclick='toogleResposta(" + questionario.id + ")'><b><i class='fa fa-chevron-right'></i>  "
                        + questionario.pergunta
                        + "</b></a><button type='button' class='btn btn-sm btn-danger' style='float:right;padding-top: 2px;' onclick='if(confirm(\"Tem certeza que deseja deletar essa Pergunta/Resposta?\")){deleteQuestionario(" + (questionario.id) + ");}'><i class='fa fa-trash'></i></button>"
                        + "<div class='' id='resposta-" + questionario.id + "' style='display:none'><br>"
                        + questionario.resposta
                        + " </div>"
                        + " </div>";

                $('#questionario-assunto').append(html);
            });
            $('#questionario-assunto').slideDown();
        } else {
            $('#questionario-assunto').slideUp();
        }
    }
    function deleteQuestionario(id) {
        $.ajax({url: $('#urlroot').val() + 'assuntos/deletequestionario/' + id, dataType: 'json', type: 'post',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'A Pergunta/Resposta foi removida do sistema.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                    $('#questionario-' + id).remove();// deleta a Pergunta/Resposta do html                   
                } else {
                    new PNotify({text: 'Não foi possível deletar essa Pergunta/Resposta.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function toogleResposta(id) {
        $('#resposta-' + id).slideToggle();
        $('#questionario-' + id).children('i').toggleClass('fa-chevron-right fa-chevron-down');
    }

    /********INSERT-VIEW-DELETE DE legislativo**********/
    function addLei() {
        var dados = '', urldestino = $('#urlroot').val();
        if ($('#leisnorma-id').val()) {
            urldestino += 'assuntos/attachlei/' + $("#current-assunto-id").val();
            dados += 'leisnorma_id=' + $('#leisnorma-id').val();
        } else if ($('#url-lei').val()) {
            urldestino += 'leisnormas/addleiassunto/' + $("#current-assunto-id").val()
            dados += "url_lei=" + $('#url-lei').val();
        }
        $.ajax({
            url: urldestino,
            data: dados,
            dataType: 'json',
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    var lei = data.retorno;
                    if ($('#lei-' + lei.id).length <= 0) {
                        var dt_publicacao = '', descricao = '';
                        if (lei.dt_publicacao) {
                            dt_publicacao += ", de " + fullDateToshortBrDate(lei.dt_publicacao)
                            descricao = lei.tipolei_descricao + " Nº " + lei.numero + '' + dt_publicacao;
                        } else {
                            descricao = lei.descricao;

                        }

                        var html = "<div class='well well-sm' id='lei-" + lei.id + "'><a href='javascript:void(0);' data-toggle='modal' onclick='toogleEmenta(" + lei.id + ")' data-target='.modal-view-lei'>  "
                                + "<b class='lei-descricao-" + lei.id + "'><i class='fa fa-chevron-right'></i>" + descricao
                                + "</b></a><button type='button' class='btn btn-sm btn-success' style='float:right;padding-top: 2px;' "
                                + " onclick=\"loadDetailsLei(" + lei.id + ',' + data.leiassunto.id + ");\"><i class='fa fa-search'></i></button>"
                                + "</b></a><button type='button' class='btn btn-sm btn-danger' style='float:right;padding-top: 2px;' onclick='if(confirm(\"Tem certeza que deseja desvincular essa Lei desse Assunto?\")){detachLei(" + lei.id + ',' + data.leiassunto.id + ");}'><i class='fa fa-trash'></i></button>"
                                + "<div class='' id='ementa-" + lei.id + "' style='display:none'><br>"
                                + lei.ementa
                                + " </div>"
                                + " </div>";

                        $('#legislativo-assunto').append(html);
                    }
                    //fecha a modal
                    $(".modal-add-legislativo").find(".close").trigger("click");
                    //abre a tab com as leis
                    $('#legislativo-assunto').slideDown();
                    //limpa os campos da modal e da refresh nos selects
                    clearFieldsInfo(['url-lei', 'leisnorma-id']);
                    refreshListLeis();


                    new PNotify({text: 'Lei vinculada com sucesso.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possível vincular essa Lei.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                $(".modal-add-legislativo").find(".close").trigger("click");
                console.log(a);
            }
        });

    }
    function toogleEmenta(id) {
        $('#ementa-' + id).slideToggle();
        $('#lei-' + id).children('i').toggleClass('fa-chevron-right fa-chevron-down');
    }
    function renderLegislativo(leis) {
        $('#legislativo-assunto').html('');
        if (leis.length > 0) {
            $.each(leis, function (index, leiassunto) {
                var lei = leiassunto.leisnorma;
                if (lei) {
                    var dt_publicacao = '', descricao = '';
                    if (lei.dt_publicacao) {
                        dt_publicacao += ", de " + fullDateToshortBrDate(lei.dt_publicacao)
                        descricao = lei.tipolei_descricao + " Nº " + lei.numero + '' + dt_publicacao;
                    } else {
                        descricao = lei.descricao;

                    }

                    var html = "<div class='well well-sm' id='lei-" + lei.id + "'><a href='javascript:void(0);' data-toggle='modal' onclick='toogleEmenta(" + lei.id + ")' data-target='.modal-view-lei'>  "
                            + "<b class='lei-descricao-" + lei.id + "'><i class='fa fa-chevron-right'></i>" + descricao
                            + "</b></a><button type='button' class='btn btn-sm btn-success' style='float:right;padding-top: 2px;'"
                            + " onclick=\"loadDetailsLei(" + lei.id + ',' + leiassunto.id + ");\"><i class='fa fa-search'></i></button>"
                            + "</b></a><button type='button' class='btn btn-sm btn-danger' style='float:right;padding-top: 2px;' onclick='if(confirm(\"Tem certeza que deseja desvincular essa Lei desse Assunto?\")){detachLei(" + lei.id + ',' + leiassunto.id + ");}'><i class='fa fa-trash'></i></button>"
                            + "<div class='' id='ementa-" + lei.id + "' style='display:none'><br>"
                            + lei.ementa
                            + " </div>"
                            + " </div>";

                    $('#legislativo-assunto').append(html);
                }
            });
            $('#legislativo-assunto').slideDown();
        } else {
            $('#legislativo-assunto').slideUp();
        }
    }
    function loadDetailsLei(id, leiassunto) {
        current_leiassunto = leiassunto;
        $.ajax({
            url: $('#urlroot').val() + 'leisnormas/detailsview/' + leiassunto,
            async: true,
            success: function (data) {
                $('.titulo-lei').html($('.lei-descricao-' + id).text());
                $('#lei-original').html(data);
                /*garante que limpou o conteudo da ultima lei visualizada do html*/
                refreshModalLei();

            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function detachLei(id, leiassunto) {
        $.ajax({url: $('#urlroot').val() + 'assuntos/detachlei/' + leiassunto, dataType: 'json', type: 'post',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'Esse vínculo foi removida do sistema.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                    $('#lei-' + id).remove();// deleta essa lei do html                   
                } else {
                    new PNotify({text: 'Não foi possível desvincular esse item', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function refreshListLeis() {
        $.ajax({
            url: $('#urlroot').val() + 'leisnormas/listajax/',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                data = data.retorno;
                var html = "<option value=''> Selecione uma lei</option>";
                $.each(data, function (i, item) {
                    html = html + '<option value="' + item.id + '">' + item.lei + '</option>';
                });
                $('#leisnorma-id').html(html);
                $("#leisnorma-id").select2("destroy");
                $("#leisnorma-id").select2({minimumResultsForSearch: 5});
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function refreshModalLei() {
        $(".modal-lei-details").modal("show");
        $('#lei-original').show();
        if (CKEDITOR.instances.editor)
            CKEDITOR.instances.editor.destroy();
        $('#editor').html('');
        $('#editor').hide();
        $("#btn-close-lei").html('Fechar');
        $(".btn-enable-edit").show();
        $(".btn-edit-lei").hide();
    }
    function editLeiAssunto(id_leiassunto) {
        $.ajax({
//            '&texto_modificado='
            url: $('#urlroot').val() + 'leisnormas/editleiassunto/' + id_leiassunto,
            data: JSON.stringify({texto_modificado: CKEDITOR.instances.editor.getData()}),
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            type: 'patch',
            success: function (data) {
                if (data.retorno) {

                    new PNotify({text: 'O texto foi atualizado.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possível atualizar o texto.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
//        CKEDITOR.ajax.post($('#urlroot').val() + 'leisnormas/editleiassunto/' + id_leiassunto, JSON.stringify({foo: 'bar'}), 'application/json', function (data) {
//            console.log(data);
//        });

    }

    /********INSERT-DELETE documentos**********/
    function refreshDocList() {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/listdocumentos/' + current_assunto,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                var html = '';
                $.each(data.documentos, function (i, item) {
                    html += "<p class='well well-sm documento' nome='" + item + "'>" +
                            "<a href='" + data.diretorio + '\\' + item + "' target='_blank'> <b> " + item + '</b><a> ' +
                            " <button type='button' class='btn btn-sm btn-danger' style='float:right;padding-top: 2px;' onclick='if(confirm(\"Tem certeza que deseja deletar essa arquivo?\")){deleteDocumento(\"" + item + "\");}'><i class='fa fa-trash'></i></button>" +
                            " </p>";
                });
                $('#documentos-assunto').html(html);
                $('#documentos-assunto').slideDown();
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function deleteDocumento(doc) {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/deletedocumento/' + current_assunto,
            type: 'patch',
            data: '&documento-nome=' + doc,
            dataType: 'json',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'Documento removido', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                    $('.documento[nome=\'' + doc + '\']').remove();// deleta o documento do html                   
                } else {
                    new PNotify({text: 'Não foi possível deletar o documento', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }

    /********INSERT-DELETE TAGS**********/
    function insertTag(tag) {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/addtag/' + current_assunto,
            data: '&tag_id=' + tag.id,
            dataType: 'json',
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'Tag adicionada.', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                    if (isNaN(tag.id)) {
                        $("#tags-id option[value='" + tag.id + "']").prop('id', data.newtag_id);
                    }
                } else {
                    new PNotify({text: 'Não foi possível adicionar essa Tag.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function deleteTag(tag) {
        var tagid;
        if (isNaN(tag.id)) {
            tagid = tag.element.id;
        } else {
            tagid = tag.id;
        }
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/deletetag/' + current_assunto,
            type: 'patch',
            data: '&tag_id=' + tagid,
            dataType: 'json',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'Tag removida', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possível remover essa Tag', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
    function renderTags(selectedTags) {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/listtags/',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                data = data.retorno;

                var html = "";
                $.each(data, function (i, item) {
                    html += '<option value="' + i + '">' + item + '</option>';
                });
                $('#tags-id').html(html);


                //seleciona as tags que já pertencem à esse assunto
                $.each(selectedTags, function (i, item) {
                    $("#tags-id option[value='" + item.tag_id + "']").attr('selected', 'selected');
                });

                $('#tags-id').trigger('change');
            },
            error: function (a) {
                console.log(a);
            }
        });

    }
    /********OUTRAS FUNÇÕES**********/
    var fullDateToshortBrDate = function (fulldate) {
        var date = fulldate.replace(' ', 'T').split("T");
        date = date[0].split("-");
        return  date[2] + '/' + date[1] + '/' + date[0];
    }




</script>

<!-- footer content -->
<footer>
    <div class="pull-right">
        Tríade - Contabilidade Online 
        <a href="http://triadeconsultoria.adv.br">Tríade Consultoria</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->

<!--Modals content-->

<!--CADASTRAR NOVO ASSUNTO-->
<div class="modal fade modal-add-assunto" tabindex="-1" role="dialog" aria-hidden="true">
    <form action="/assuntos/add" method="post">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Cadastrar novo assunto</h4>
                </div>
                <div class="modal-body">
                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="descricao">
                        Digite qual será o novo assunto <span class="required">*</span>
                    </label>
                    <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                    <div class="clearfix"></div><br/>
                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="assunto_id">Selecione se houver um assunto pai
                    </label>
                    <?= $this->Form->input('assunto_id', ['options' => $todosassuntos, "class" => "form-control col-md-12 col-xs-12  col-sm-12 ", 'label' => false, 'empty' => '   ', "style" => "width: 100%"]); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Salvar e Prosseguir</button>
                </div>
            </div>
        </div>

    </form>
</div>

<!--CADASTRAR SUB ASSUNTOS-->
<div class="modal fade modal-add-subassuntos" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Novo Sub-Assunto</h4>
            </div>
            <div class="modal-body">
                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="subassunto_id">
                    Selecione um sub-assunto, se ele já estiver cadastrado
                </label>
                <?= $this->Form->input('subassunto_id', ['options' => $todosassuntos, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'style' => 'width:100%', 'empty' => true]); ?>

                <div class="clearfix"></div><br/>
                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="subassunto_descricao">
                    Se não estiver, digite um novo para cadastrar
                </label>
                <?= $this->Form->input('subassunto_descricao', ["class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'placeholder' => 'Digite um novo sub-assunto...']); ?>
                <div class="clearfix"></div><br/>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id='btn-add-subassunto' class="btn btn-primary">Adicionar</button>
            </div>
        </div>
    </div>

</div>

<!--CADASTRAR ANOTAÇÕES-->
<div class="modal fade modal-add-anotacao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Anotação</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->input('tipo-anotacao', ['options' => $tipoanotacoes, "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'style' => 'width:100%', 'empty' => true]); ?>
                <div class="clearfix"></div><br/>
                <?= $this->Form->input('titulo-anotacao', ['type' => 'text', "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'placeholder' => 'Título da Anotação']); ?>
                <div class="clearfix"></div><br/>
                <?= $this->Form->input('anotacao', ['type' => 'textarea', "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'placeholder' => 'Digite a nova anotação aqui...']); ?>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id='btn-add-anotacao' class="btn btn-primary">Adicionar</button>
                <button type="button" id='btn-edit-anotacao' class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </div>

</div>

<!--CADASTRAR PERGUNTAS E RESPOSTAS-->
<div class="modal fade modal-add-questionario" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Pergunta/Resposta</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->input('pergunta-descricao', ['type' => 'textarea', "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'placeholder' => 'Digite a pergunta aqui...']); ?>               
                <br>
                <?= $this->Form->input('resposta-descricao', ['type' => 'textarea', "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'placeholder' => 'Digite a resposta aqui...']); ?>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id='btn-add-questionario' class="btn btn-primary">Adicionar</button>
            </div>
        </div>
    </div>
</div>

<!--CADASTRAR/VINCULAR iNFORMAÇÕES DE LEGISLAÇÃO-->
<div class="modal fade modal-add-legislativo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Legislativo</h4>
            </div>
            <div class="modal-body">
                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="leisnorma_id">
                    Selecione a Lei que deseja víncular a esse assunto
                </label>
                <?= $this->Form->input('leisnorma_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'style' => 'width:100%', 'empty' => true]); ?>

                <div class="clearfix"></div><br/>
                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="url_lei">
                    Se não estiver cadastrada, insira a URL dessa lei (do site do planalto)
                </label>
                <?= $this->Form->input('url_lei', ["class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'placeholder' => 'cole aqui a url da lei']); ?>
                <div class="clearfix"></div><br/>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id='btn-add-lei' class="btn btn-primary">Adicionar</button>
            </div>
        </div>
    </div>

</div>

<!--Visualizar/Editar iNFORMAÇÕES DE LEGISLAÇÃO-->
<div class="modal fade modal-lei-details" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>

                <span style="float: right;">
                    <button type="button"  class="btn btn-primary btn-enable-edit">Habilitar Edição</button>
                    <button type="button"  class="btn btn-primary btn-edit-lei" style="display: none;">Salvar Alterações</button>
                </span>
                <h4 class="modal-title green titulo-lei"><i class="fa fa-legal"></i>
                </h4>
            </div>
            <div class="modal-body" id="editor" style="display: none;">

            </div>
            <div class="modal-body" id="lei-original">

            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="btn-close-lei">Fechar</button>
                <button type="button" id='btn-enable-edit' class="btn btn-primary btn-enable-edit">Habilitar Edição</button>
                <button type="button" id='btn-edit-lei' class="btn btn-primary btn-edit-lei" style="display: none;">Salvar Alterações</button>
            </div>
        </div>
    </div>

</div>

<!--ANEXAR ARQUIVOS AO ASSUNTO-->
<div class="modal fade modal-add-documento" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Upload de Documento</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="documento-nome">Título para o arquivo
                    </label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $this->Form->input('documento-nome', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'placeholder' => 'informação opcional']); ?>
                    </div>
                </div>                
                <div class="form-group">
                    <label class="control-label col-md-12 col-sm-12 col-xs-12" for="arquivopath">
                    </label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?= $this->Form->file('file', ['id' => 'file_upload', 'label' => false]) ?>
                    </div>
                </div>      <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id='btn-add-documento' class="btn btn-primary">Adicionar</button>
            </div>
        </div>
    </div>

</div>
