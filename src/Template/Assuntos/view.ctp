<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Assunto - <?= h($assunto->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Descrição do Assunto') ?></p>
                        <p><?= $assunto->descricao ? $assunto->descricao : 'Não Informado'; ?></p>


                        <?php if ($assunto->assuntospai) { ?>
                            <p class="title"><?= __('Assunto (Pai)') ?></p>
                            <p><?= $this->Html->link($assunto->assuntospai->descricao, ['action' => 'view', $assunto->assuntospai->id]) ?></p>
                        <?php } ?>

                        <p class="title"><?= __('Quem criou') ?></p>
                        <p><?= $assunto->has('user') ? $this->Html->link($assunto->user->nome, ['controller' => 'Users', 'action' => 'view', $assunto->user->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $assunto->last_update ? $assunto->last_update : 'Não modificado'; ?></p>

                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $assunto->status ? __('Ativo') : __('Inativo'); ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $assunto->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $assunto->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $assunto->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($assunto->assuntosfilhos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Assuntos (Filhos) </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Assunto') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($assunto->assuntosfilhos as $assuntos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($assuntos->descricao) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Assuntos', 'action' => 'view', $assuntos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Assuntos', 'action' => 'edit', $assuntos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Assuntos', 'action' => 'delete', $assuntos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $assuntos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($assunto->leisassuntos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Leis/Normas Vínculadas </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Lei/Norma') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($assunto->leisassuntos as $leisassuntos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($leisassuntos->leisnorma->descricao) ?></td>
                                        <td><?= h($leisassuntos->dt_cadastro) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar Lei'), ['controller' => 'Leisnormas', 'action' => 'view', $leisassuntos->leisnorma->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar Lei'), ['controller' => 'Leisnormas', 'action' => 'edit', $leisassuntos->leisnorma->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Desvincular'), ['controller' => 'Leisassuntos', 'action' => 'delete', $leisassuntos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisassuntos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($assunto->telaquestionarios)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Questionários Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Pergunta') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($assunto->telaquestionarios as $telaquestionarios):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($telaquestionarios->pergunta) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Telaquestionarios', 'action' => 'view', $telaquestionarios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Telaquestionarios', 'action' => 'edit', $telaquestionarios->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Telaquestionarios', 'action' => 'delete', $telaquestionarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $telaquestionarios->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


