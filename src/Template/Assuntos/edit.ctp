<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Editar Assunto <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($assunto, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Assunto <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?= $this->Form->input('descricao', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>        
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="assunto_id">Assunto superior    (se houver)
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?= $this->Form->input('assunto_id', ['options' => $assuntos, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => '   ']); ?>

                    </div> 
                </div>              
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                        <?= $this->Html->link(__('Voltar'), ['action' => 'index'], ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>Anotações Gerais</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><button id='btn-modal-anotacao' type="button" class="btn btn-dark fa fa-plus" data-toggle="modal" data-target=".modal-add-anotacao"> Adicionar</button></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="anotacao-assunto" style="display: none">

            </div>
        </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>Perguntas/Respostas</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><button type="button" id='btn-add-questionario' class="btn btn-dark fa fa-plus"> Adicionar</button></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="questionario-assunto"  style="display: none">

            </div>
        </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>Informativos no Sistema</h2>
                <ul class="nav navbar-right panel_toolbox">                     
                    <li><button type="button" id='btn-add-informativo' class="btn btn-dark fa fa-plus"> Adicionar</button></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="informativo-assunto" style="display: none">

            </div>
        </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>Lembretes em Serviços</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><button type="button" id='btn-add-lembrete' class="btn btn-dark fa fa-plus"> Adicionar</button></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="lembrete-assunto" style="display: none">

            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Legislativo</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><button type="button" id='btn-add-legislativo' class="btn btn-dark fa fa-plus"> Adicionar</button></li>
                    <li><button type="button" id='add-informativo' class="btn btn-success fa fa-external-link"> Leis</button></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>                  
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="legislativo-assunto">

            </div>
        </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>Documentos</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><button type="button" id='btn-add-documento' class="btn btn-dark fa fa-plus"> Adicionar</button></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>                  
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="documentos-assunto"  style="display: none">

            </div>
        </div>
    </div>
</div>
<input type="hidden" name='current-assunto-id' id='current-assunto-id' value="<?= $assunto->id ?>">
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/js/ajax/index.js'); ?>
<script>
    $(document).ready(function () {
//        $("select").select2();
        $("#tipo-anotacao").select2({placeholder: 'Selecione um tipo ou digite um novo'});
        $("#assunto-id").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione ou digite um novo'});
        $("#btn-add-anotacao").click(function () {
            addAnotacao();
        });
    });
    function addAnotacao() {
        $.ajax({
            url: $('#urlroot').val() + 'assuntos/addanotacao/' + $("#current-assunto-id").val(),
            data: 'anotacao=' + $('#descricao-anotacao').val() + "&tiponote_id=" + $('#tipo-anotacao').val(),
            dataType: 'json',
            type: 'patch',
            success: function (data) {
                if (data.retorno) {
                    console.log(data);

                    var html = "<p>";
                    html += "<b>" + data.retorno + '</b> - ' + $('#descricao-anotacao').val();
                    html += "</p>";

                    if (!$('#tiponote-' + $('#tipo-anotacao').val()).length) {
                        $('#anotacao-assunto').append("<div id='tiponote-" + $('#tipo-anotacao').val() + "'><h4 class='green'> <i class='fa fa-chevron-right'></i>  " + $('#tipo-anotacao option:selected').text() + "</h4><br></div><div class='ln_solid'></div><br>");
                    }

                    $('#tiponote-' + $('#tipo-anotacao').val()).append(html);

                    //fecha a modal
                    $(".modal-add-anotacao").find(".close").trigger("click");
                    //abre a tab com as anotaçções
                    $('#anotacao-assunto').slideDown();
                    //limpa os campos da modal
                    refreshSelect2(['tipo-anotacao']);
                    clearFieldsInfo(['tipo-anotacao', 'descricao-anotacao']);


                }
            },
            error: function (a) {
                console.log(a);
            }
        });

    }
</script>
<!--MODAIS DE CADASTRO-->

<div class="modal fade modal-add-anotacao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Nova Anotação</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->input('tipo-anotacao', ['options' => $tipoanotacoes, "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'style' => 'width:100%', 'empty' => true]); ?>
                <br>
                <?= $this->Form->input('descricao-anotacao', ['type' => 'textarea', "class" => "form-control col-md-12 col-sm-12 col-xs-12", 'label' => false, 'placeholder' => 'Digite a nova anotação aqui...']); ?>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id='btn-add-anotacao' class="btn btn-primary">Adicionar</button>
            </div>
        </div>
    </div>

</div>