<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Atribuir essa tarefa à outros usuários <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($tarefausuario, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>
                <?php if ($admin) { ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_id">Empresa 
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('empresa_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'options' => $empresas, "style" => "width: 100%", 'empty' => 'selecione se necessário - para filtrar']); ?>
                        </div> 
                    </div>                  
                <?php } ?>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_id">Setor 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('areaservico_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'options' => $areaservicos, "style" => "width: 100%", 'empty' => 'selecione se necessário - para filtrar']); ?>
                    </div> 
                </div>                  
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_id">Usuários 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('user_ids', ["class" => "form-control select2 col-md-7 col-xs-12", 'required' => 'required', 'label' => false, 'options' => $users, 'multiple' => 'multiple', "style" => "width: 100%", 'empty' => '']); ?>
                    </div> 
                </div>                  

                <?= $this->Form->input('tarefa_id', ['type' => 'hidden', 'value' => $tarefausuario->tarefa_id, 'label' => false]); ?>
                <?= $this->Form->input('backlink', ['type' => 'hidden', 'value' => $backlink, 'label' => false]); ?>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Cadastrar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/js/ajax/empresas.js'); ?>
<script>
    $(document).ready(function () {
        $("select").select2({minimumResultsForSearch: 6});
        $("#user-ids").select2({placeholder: 'selecione um ou mais usuários', multiple: true, tokenSeparators: [',', ' '], tags: true, allowClear: true});
    });
</script>