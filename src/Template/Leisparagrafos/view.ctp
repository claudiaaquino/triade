<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Parágrafo - <?= h($leisparagrafo->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Lei/Norma') ?></p>
                        <p><?= $leisparagrafo->has('leisnorma') ? $this->Html->link($leisparagrafo->leisnorma->numero, ['controller' => 'Leisnormas', 'action' => 'view', $leisparagrafo->leisnorma->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Artigo') ?></p>
                        <p><?= $leisparagrafo->has('leisartigo') ? $this->Html->link($leisparagrafo->leisartigo->nome, ['controller' => 'Leisartigos', 'action' => 'view', $leisparagrafo->leisartigo->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Parágrafo') ?></p>
                        <p><?= $leisparagrafo->nome ? $leisparagrafo->nome : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Descrição do Parágrafo') ?></p>
                        <p>  <?= $leisparagrafo->descricao ? $this->Text->autoParagraph(h($leisparagrafo->descricao)) : 'Não Informado'; ?></p>


                    </div>
                </div><div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Ordem que aparece') ?></p>
                        <p><?= $leisparagrafo->ordem ? $this->Number->format($leisparagrafo->ordem) : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $leisparagrafo->dt_cadastro ? $leisparagrafo->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $leisparagrafo->last_update ? $leisparagrafo->last_update : 'Não modificado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $leisparagrafo->status ? __('Ativo') : __('Inativo'); ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $leisparagrafo->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $leisparagrafo->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisparagrafo->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


