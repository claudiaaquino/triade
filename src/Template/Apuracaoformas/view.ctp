<div class="page-title">
    <div class="title_left">
        <h3><?= __('Apuracaoformas') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($apuracaoforma->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Descricao') ?></p>
                                    <p><?= h($apuracaoforma->descricao) ?></p>
                                    </tr>
                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $this->Number->format($apuracaoforma->id) ?></p>

                                                                                                                                
                        <p class="title"><?= __('Dt Cadastro') ?></p>
                                <p><?= h($apuracaoforma->dt_cadastro) ?></p>

                                                    
                        <p class="title"><?= __('Last Update') ?></p>
                                <p><?= h($apuracaoforma->last_update) ?></p>

                                                                                                                                
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $apuracaoforma->status ? __('Ativo') : __('Desativado'); ?></p>

                                                                                                    <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <?php if (!empty($apuracaoforma->previsaoorcamentos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Previsaoorcamentos Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tiposervico Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Apuracaoforma Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Atuacaoramo Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Min Funcionarios') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Max Funcionarios') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Min Faturamento Mensal') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Max Faturamento Mensal') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Valor Servico') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php ?>
                                <tr>


                                </tr>

                                <?php
                                $cor = 'even';
                                foreach ($apuracaoforma->previsaoorcamentos as $previsaoorcamentos):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= h($previsaoorcamentos->id) ?>">
                                    </td>
                                                                    <td><?= h($previsaoorcamentos->id) ?></td>
                                                                    <td><?= h($previsaoorcamentos->tiposervico_id) ?></td>
                                                                    <td><?= h($previsaoorcamentos->apuracaoforma_id) ?></td>
                                                                    <td><?= h($previsaoorcamentos->atuacaoramo_id) ?></td>
                                                                    <td><?= h($previsaoorcamentos->min_funcionarios) ?></td>
                                                                    <td><?= h($previsaoorcamentos->max_funcionarios) ?></td>
                                                                    <td><?= h($previsaoorcamentos->min_faturamento_mensal) ?></td>
                                                                    <td><?= h($previsaoorcamentos->max_faturamento_mensal) ?></td>
                                                                    <td><?= h($previsaoorcamentos->valor_servico) ?></td>
                                                                    <td><?= h($previsaoorcamentos->user_id) ?></td>
                                                                    <td><?= h($previsaoorcamentos->dt_cadastro) ?></td>
                                                                    <td><?= h($previsaoorcamentos->last_update) ?></td>
                                                                    <td><?= h($previsaoorcamentos->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Previsaoorcamentos', 'action' => 'view', $previsaoorcamentos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Previsaoorcamentos', 'action' => 'edit', $previsaoorcamentos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Previsaoorcamentos', 'action' => 'delete', $previsaoorcamentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $previsaoorcamentos->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
                    <?php if (!empty($apuracaoforma->empresas)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Empresas Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnpj') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnpj Nf') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cpf') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Razao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Fantasia') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cnae Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Telefone') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Fax') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Email') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Email Nfe') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Endereco') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Numero') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Complemento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Bairro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('End Cep') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cidade Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome Responsavel') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Cargo Responsavel') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Num Funcionarios') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Solicitacao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Solicitacao') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Num Step') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome1') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome2') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Nome3') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Num Socios') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Faturamento Mensal') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Faturamento Anual') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Porte Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Indicecadastral') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Capitalsocial') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Sociedade') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Residencia Socio') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Arealocal') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Opcaosimplesnacional') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Enderecocorrespondencia') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Enderecodocumento') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Apuracaoforma Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Atuacaoramo Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Fields Updated') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php ?>
                                <tr>


                                </tr>

                                <?php
                                $cor = 'even';
                                foreach ($apuracaoforma->empresas as $empresas):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= h($empresas->id) ?>">
                                    </td>
                                                                    <td><?= h($empresas->id) ?></td>
                                                                    <td><?= h($empresas->cnpj) ?></td>
                                                                    <td><?= h($empresas->cnpj_nf) ?></td>
                                                                    <td><?= h($empresas->cpf) ?></td>
                                                                    <td><?= h($empresas->razao) ?></td>
                                                                    <td><?= h($empresas->fantasia) ?></td>
                                                                    <td><?= h($empresas->cnae_id) ?></td>
                                                                    <td><?= h($empresas->telefone) ?></td>
                                                                    <td><?= h($empresas->fax) ?></td>
                                                                    <td><?= h($empresas->email) ?></td>
                                                                    <td><?= h($empresas->email_nfe) ?></td>
                                                                    <td><?= h($empresas->endereco) ?></td>
                                                                    <td><?= h($empresas->end_numero) ?></td>
                                                                    <td><?= h($empresas->end_complemento) ?></td>
                                                                    <td><?= h($empresas->end_bairro) ?></td>
                                                                    <td><?= h($empresas->end_cep) ?></td>
                                                                    <td><?= h($empresas->estado_id) ?></td>
                                                                    <td><?= h($empresas->cidade_id) ?></td>
                                                                    <td><?= h($empresas->nome_responsavel) ?></td>
                                                                    <td><?= h($empresas->cargo_responsavel) ?></td>
                                                                    <td><?= h($empresas->num_funcionarios) ?></td>
                                                                    <td><?= h($empresas->solicitacao) ?></td>
                                                                    <td><?= h($empresas->dt_solicitacao) ?></td>
                                                                    <td><?= h($empresas->num_step) ?></td>
                                                                    <td><?= h($empresas->user_id) ?></td>
                                                                    <td><?= h($empresas->nome1) ?></td>
                                                                    <td><?= h($empresas->nome2) ?></td>
                                                                    <td><?= h($empresas->nome3) ?></td>
                                                                    <td><?= h($empresas->num_socios) ?></td>
                                                                    <td><?= h($empresas->faturamento_mensal) ?></td>
                                                                    <td><?= h($empresas->faturamento_anual) ?></td>
                                                                    <td><?= h($empresas->porte_id) ?></td>
                                                                    <td><?= h($empresas->indicecadastral) ?></td>
                                                                    <td><?= h($empresas->capitalsocial) ?></td>
                                                                    <td><?= h($empresas->sociedade) ?></td>
                                                                    <td><?= h($empresas->residencia_socio) ?></td>
                                                                    <td><?= h($empresas->arealocal) ?></td>
                                                                    <td><?= h($empresas->opcaosimplesnacional) ?></td>
                                                                    <td><?= h($empresas->enderecocorrespondencia) ?></td>
                                                                    <td><?= h($empresas->enderecodocumento) ?></td>
                                                                    <td><?= h($empresas->apuracaoforma_id) ?></td>
                                                                    <td><?= h($empresas->atuacaoramo_id) ?></td>
                                                                    <td><?= h($empresas->last_fields_updated) ?></td>
                                                                    <td><?= h($empresas->last_update) ?></td>
                                                                    <td><?= h($empresas->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Empresas', 'action' => 'view', $empresas->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Empresas', 'action' => 'edit', $empresas->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Empresas', 'action' => 'delete', $empresas->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $empresas->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
            </div>
</div>


