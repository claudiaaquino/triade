<div class="page-title">
    <div class="title_left">
        <h3>Detalhes do Tipo de Documento</h3>
    </div>
</div>
<div class="clearfix"></div>



<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Detalhes do Documento <?= h($tipodocumento->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Grupo de Documento</p>
                        <p><?= $tipodocumento->grupodocumento->descricao ?></p>

                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Ultima Atualização</p>
                        <p><?= $tipodocumento->last_update; ?></p>
                        <p class="title">Status</p>
                        <p><?= $tipodocumento->status ? 'Ativo' : 'Inativo'; ?></p>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-3">
                            <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                            <?= $this->Html->link("Editar", ['action' => 'edit', $tipodocumento->id], ['class' => "btn btn-primary"]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        if (!empty($tipodocumento->documentos)) {
            ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Documentos Relacionados</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col" class="column-title"><?= __('Empresa') ?></th>
                                    <th scope="col" class="column-title"><?= __('Funcionário') ?></th>
                                    <th scope="col" class="column-title"><?= __('Enviado') ?></th>
                                    <th></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $cor = 'even';
                                foreach ($tipodocumento->documentos as $documento) {
                                    ?>

                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records">
                                        </td>
                                        <td><?= $this->Html->link($documento->empresa->fantasia, ['controller' => 'Empresas', 'action' => 'view', $documento->empresa->id]) ?></td>
                                        <td><?= $documento->funcionario ? $this->Html->link($documento->funcionario->nome, ['controller' => 'Funcionarios', 'action' => 'view', $documento->funcionario->id]) : '---' ?></td>
                                        <td><?= $documento->dt_enviado ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Documentos', 'action' => 'view', $documento->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Documentos', 'action' => 'edit', $documento->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Documentos', 'action' => 'delete', $documento->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Deseja mesmo remover o documento #{0}?', $documento->id)]) ?>

                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>