<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <div style="float:left" class="col-md-9 col-sm-9 col-xs-12">
                    <div class="col-md-4 col-sm-4 col-xs-12"><h2>Tipo de Documentos</h2></div>
                    <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-right top_search">
                        <form action="/tipodocumentos/index" method="POST" >
                            <div class="input-group">
                                <input id="termo-pesquisa" name="termo-pesquisa" class="form-control" placeholder="digite um grupo ou tipo de documento e clique em pesquisar" type="text">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Pesquisar</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?> </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('descricao', 'Documento') ?></th>                                          
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('grupodocumento_id', 'Grupo de Documento') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('exige_competencia', 'Tem Competência') ?></th>                                          
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('exige_exercicio', 'Tem Exercício') ?></th>                                          
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $cor = 'even';
                            foreach ($tipodocumentos as $tipodocumento) {
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td><?= h($tipodocumento->descricao) ?></td>
                                    <td><?= $this->Html->link($tipodocumento->grupodocumento->descricao, ['controller' => 'Grupodocumentos', 'action' => 'view', $tipodocumento->grupodocumento->id]) ?></td>
                                    <td><?= $tipodocumento->exige_competencia ? 'SIM' : 'NÃO'; ?></td>
                                    <td><?= $tipodocumento->exige_exercicio ? 'SIM' : 'NÃO'; ?></td>
                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $tipodocumento->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $tipodocumento->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $tipodocumento->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Deseja mesmo remover o registro #{0}?', $tipodocumento->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>