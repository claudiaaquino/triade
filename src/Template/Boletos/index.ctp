

<div class="page-title">
    <div class="title_left">
        <h3><?= __('Boletos') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2><?= $this->Html->link(__('  Cadastrar'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('contrato_id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('contabancaria_id') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('filename') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('valor_boleto') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_vencimento') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_processamento') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('valor_multa') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('valor_multa_dia') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_pagamento') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('pago') ?></th>
                                                                <th scope="col" class="column-title"><?= $this->Paginator->sort('status') ?></th>
                                                                
                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($boletos as $boleto){ ?>                                
                                <tr class="<?= $cor ?> pointer">
                                                                         
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?=$boleto->id?>">
                                    </td>
                                    
                                                                                       <td><?= $this->Number->format($boleto->id) ?></td>
                                                                                                     <td><?= $boleto->has('contrato') ? $this->Html->link($boleto->contrato->id, ['controller' => 'Contratos', 'action' => 'view', $boleto->contrato->id]) : '' ?></td>
                                                                                                         <td><?= $boleto->has('contabancaria') ? $this->Html->link($boleto->contabancaria->id, ['controller' => 'Contabancarias', 'action' => 'view', $boleto->contabancaria->id]) : '' ?></td>
                                                                                                     <td><?= h($boleto->filename) ?></td>
                                                                                                 <td><?= h($boleto->valor_boleto) ?></td>
                                                                                                 <td><?= h($boleto->dt_vencimento) ?></td>
                                                                                                 <td><?= h($boleto->dt_processamento) ?></td>
                                                                                                  <td><?= $this->Number->format($boleto->valor_multa) ?></td>
                                                                                                  <td><?= $this->Number->format($boleto->valor_multa_dia) ?></td>
                                                                                                 <td><?= h($boleto->dt_pagamento) ?></td>
                                                                                                 <td><?= h($boleto->pago) ?></td>
                                                                                                 <td><?= h($boleto->status) ?></td>
                                                                                    
                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $boleto->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $boleto->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $boleto->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $boleto->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
