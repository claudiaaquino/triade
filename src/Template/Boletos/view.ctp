<div class="page-title">
    <div class="title_left">
        <h3><?= __('Boletos') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($boleto->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Contrato') ?></p>
                                    <p><?= $boleto->has('contrato') ? $this->Html->link($boleto->contrato->id, ['controller' => 'Contratos', 'action' => 'view', $boleto->contrato->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Contabancaria') ?></p>
                                    <p><?= $boleto->has('contabancaria') ? $this->Html->link($boleto->contabancaria->id, ['controller' => 'Contabancarias', 'action' => 'view', $boleto->contabancaria->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Filename') ?></p>
                                    <p><?= h($boleto->filename) ?></p>
                                    </tr>
                                                                                                                    <p class="title"><?= __('Valor Boleto') ?></p>
                                    <p><?= h($boleto->valor_boleto) ?></p>
                                    </tr>
                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $this->Number->format($boleto->id) ?></p>

                                                    
                        <p class="title"><?= __('Valor Multa') ?></p>
                                <p><?= $this->Number->format($boleto->valor_multa) ?></p>

                                                    
                        <p class="title"><?= __('Valor Multa Dia') ?></p>
                                <p><?= $this->Number->format($boleto->valor_multa_dia) ?></p>

                                                                                                                                
                        <p class="title"><?= __('Dt Vencimento') ?></p>
                                <p><?= h($boleto->dt_vencimento) ?></p>

                                                    
                        <p class="title"><?= __('Dt Processamento') ?></p>
                                <p><?= h($boleto->dt_processamento) ?></p>

                                                    
                        <p class="title"><?= __('Dt Pagamento') ?></p>
                                <p><?= h($boleto->dt_pagamento) ?></p>

                                                                                                                                
                        <p class="title"><?= __('Pago') ?></p>
                                <p><?= $boleto->pago ? __('Ativo') : __('Desativado'); ?></p>

                                                    
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $boleto->status ? __('Ativo') : __('Desativado'); ?></p>

                                                                                                    <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
</div>


