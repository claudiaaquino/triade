<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Editar Lei / Norma') ?> <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($leisnorma, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="numero">Número de Lei <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('numero', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Descrição <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="entidade_id">Entidade 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('entidade_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $entidades, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="orgao_id">Orgão 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('orgao_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $orgaos, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ordem">Ordem que deve aparecer
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('ordem', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         
                <div class="ln_solid">    </div>   
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leisartigos_id">Assuntos
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('assuntos_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $assuntos,'default' => $selectedassuntos,  'empty' => true, 'multiple' => 'multiple']); ?>
                    </div> 
                </div> 

                <div class="ln_solid">    </div>   
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leisartigos_id">Artigos
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('leisartigos_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $leisartigos,'default' => $selectedartigos,  'empty' => true, 'multiple' => 'multiple']); ?>
                    </div> 
                </div> 
                <div class="ln_solid">    </div>   


                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>

<script>
    $(document).ready(function () {
        $("select").select2();
        $("#leisartigos-id,#assuntos-id").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione ou digite um novo', multiple: true});
    });
</script>