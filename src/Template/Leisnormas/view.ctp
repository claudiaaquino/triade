<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Lei/Norma - <?= h($leisnorma->numero) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Número') ?></p>
                        <p><?= $leisnorma->numero ? $leisnorma->numero : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Descrição') ?></p>
                        <p><?= $leisnorma->descricao ? $leisnorma->descricao : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Entidade') ?></p>
                        <p><?= $leisnorma->has('entidade') ? $this->Html->link($leisnorma->entidade->nome, ['controller' => 'Entidades', 'action' => 'view', $leisnorma->entidade->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Orgao') ?></p>
                        <p><?= $leisnorma->has('orgao') ? $this->Html->link($leisnorma->orgao->nome, ['controller' => 'Orgaos', 'action' => 'view', $leisnorma->orgao->id]) : 'Não Informado' ?></p>


                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Ordem que aparece') ?></p>
                        <p><?= $leisnorma->ordem ? $this->Number->format($leisnorma->ordem) : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Dt. Cadastro') ?></p>
                        <p><?= $leisnorma->dt_cadastro ? $leisnorma->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $leisnorma->last_update ? $leisnorma->last_update : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $leisnorma->status ? __('Ativo') : __('Inativo'); ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $leisnorma->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $leisnorma->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisnorma->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($leisnorma->leisassuntos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Assuntos </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Assunto') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt. Cadastro') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($leisnorma->leisassuntos as $leisassuntos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($leisassuntos->assunto->descricao) ?></td>
                                        <td><?= h($leisassuntos->dt_cadastro) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar Assunto'), ['controller' => 'Assuntos', 'action' => 'view', $leisassuntos->assunto_id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar Assunto'), ['controller' => 'Assuntos', 'action' => 'edit', $leisassuntos->assunto_id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Desvincular desse Assunto'), ['controller' => 'Leisassuntos', 'action' => 'delete', $leisassuntos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisassuntos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($leisnorma->leisartigos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Artigos </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Artigo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Descrição') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($leisnorma->leisartigos as $leisartigos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($leisartigos->nome) ?></td>
                                        <td><?= h($leisartigos->descricao) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Leisartigos', 'action' => 'view', $leisartigos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Leisartigos', 'action' => 'edit', $leisartigos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Leisartigos', 'action' => 'delete', $leisartigos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisartigos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($leisnorma->leisparagrafos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Parágrafos </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Artigo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Parágrafo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Descrição') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($leisnorma->leisparagrafos as $leisparagrafos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= $leisparagrafos->leisartigo ? h($leisparagrafos->leisartigo->nome) : '--' ?></td>
                                        <td><?= h($leisparagrafos->nome) ?></td>
                                        <td><?= h($leisparagrafos->descricao) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Leisparagrafos', 'action' => 'view', $leisparagrafos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Leisparagrafos', 'action' => 'edit', $leisparagrafos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Leisparagrafos', 'action' => 'delete', $leisparagrafos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisparagrafos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($leisnorma->leisincisos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Incisos </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Artigo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Inciso') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Descrição') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($leisnorma->leisincisos as $leisincisos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= $leisincisos->leisartigo ? h($leisincisos->leisartigo->nome) : '--' ?></td>
                                        <td><?= h($leisincisos->nome) ?></td>
                                        <td><?= h($leisincisos->descricao) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Leisincisos', 'action' => 'view', $leisincisos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Leisincisos', 'action' => 'edit', $leisincisos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Leisincisos', 'action' => 'delete', $leisincisos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisincisos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($leisnorma->leisletras)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Letras </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Artigo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Letra') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Descrição') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($leisnorma->leisletras as $leisletras):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= $leisletras->leisartigo ? h($leisletras->leisartigo->nome) : '--' ?></td>
                                        <td><?= h($leisletras->nome) ?></td>
                                        <td><?= h($leisletras->descricao) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Leisletras', 'action' => 'view', $leisletras->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Leisletras', 'action' => 'edit', $leisletras->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Leisletras', 'action' => 'delete', $leisletras->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisletras->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>


