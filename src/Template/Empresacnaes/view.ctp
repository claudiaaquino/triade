<nav class="large-1 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(__('Deletar Vínculo'), ['action' => 'delete', $empresacnae->id], ['confirm' => __('Tem certeza que deseja deletar esse registro # {0}?', $empresacnae->id)]) ?> </li>
        <li><?= $this->Html->link(__('Lista Cnaes/Empresas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Vincular Cnae/Empresa'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Lista Empresas'), ['controller' => 'Empresas', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="empresacnaes view large-9 medium-8 columns content">
    <h3>CNAE <?= h($empresacnae->cnae->classe) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Empresa') ?></th>
            <td><?= $empresacnae->has('empresa') ? $this->Html->link($empresacnae->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $empresacnae->empresa->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cnae - Seção') ?></th>
            <td><?= $empresacnae->cnae->secao?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cnae - Divisão') ?></th>
            <td><?= $empresacnae->cnae->divisao?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cnae - Grupo') ?></th>
            <td><?= $empresacnae->cnae->grupo?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cnae - Classe') ?></th>
            <td><?= $empresacnae->cnae->classe?></td>
        </tr>        
        <tr>
            <th scope="row"><?= __('Cnae - Denominação') ?></th>
            <td><?= $empresacnae->cnae->denominacao?></td>
        </tr>        
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($empresacnae->status) ?></td>
        </tr>
    </table>
</div>
