<?php
if ($tpview == 'table') {
    $cor = 'even';
    foreach ($retorno as $cnaeempresa):
        ?>
        <tr class="<?= $cor ?> pointer">
            <td class=" "><?= h($cnaeempresa->cnaeclass->classe) ?></td>
            <td class=" "><?= h($cnaeempresa->cnaeclass->denominacao) ?></td>
            <td class=" "><?= $cnaeempresa->exercida_local == 1 ? "Sim" : "Não" ?></td>
            <td class=" "><?= $cnaeempresa->principal == 1 ? "Sim" : "Não" ?></td>
            <td  class=" last">
                <div class="btn-group">
                    <a class="btn btn-danger btn-xs" href='#' onclick='deleteCnaeRelation(<?= $cnaeempresa->id ?>);'>Excluir</a>
                </div>
            </td>
        </tr>
        <?php
        $cor = $cor == 'even' ? 'odd' : 'even';
    endforeach;
}else if ($tpview == 'list') {
//    $retorno;
}
?>