<nav class="large-1 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Novo Vínculo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Lista Empresas'), ['controller' => 'Empresas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Cadastrar Empresa'), ['controller' => 'Empresas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="empresacnaes index large-9 medium-8 columns content">
    <h3><?= __('Lista de Cnaes das Empresas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
                <th scope="col"><?= $this->Paginator->sort('empresa_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cnae_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('denominacao') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($empresacnaes as $empresacnae): ?>
            <tr>
                <td class="actions">
                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $empresacnae->id]) ?>
                   
                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $empresacnae->id], ['confirm' => __('Tem certeza que deseja deletar esse registro # {0}?', $empresacnae->id)]) ?>
                </td>
                <td><?= $empresacnae->has('empresa') ? $this->Html->link($empresacnae->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $empresacnae->empresa->id]) : '' ?></td>
                <td><?= $empresacnae->has('cnae') ? $this->Html->link($empresacnae->cnae->classe, ['controller' => 'Cnaes', 'action' => 'view', $empresacnae->cnae->id]) : '' ?></td>
                <td><?= $empresacnae->cnae->denominacao ?></td>
                <td><?= $this->Number->format($empresacnae->status) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próximo') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
