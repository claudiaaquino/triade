<nav class="large-1 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Lista Cnaes/Empresas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Lista Empresas'), ['controller' => 'Empresas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Cadastrar Empresa'), ['controller' => 'Empresas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="empresacnaes form large-9 medium-8 columns content">
    <?= $this->Form->create($empresacnae) ?>
    <fieldset>
        <legend><?= __('Víncular Cnaes para as Empresas') ?></legend>
        <?php
        echo $this->Form->input('empresa_id', ['options' => $empresas]);
        echo $this->Form->input('cnae_id', ['options' => $cnaes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
</div>
