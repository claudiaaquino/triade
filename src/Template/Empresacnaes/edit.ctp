
<nav class="large-1 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(__('Delete Vínculo'), ['action' => 'delete', $empresacnae->id], ['confirm' => __('Tem certeza que deseja deletar esse registro # {0}?', $empresacnae->id)]) ?> </li>
        <li><?= $this->Html->link(__('Lista Cnaes/Empresas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Vincular Cnae/Empresa'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Lista Empresas'), ['controller' => 'Empresas', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="empresacnaes form large-9 medium-8 columns content">
    <?= $this->Form->create($empresacnae) ?>
    <fieldset>
        <legend><?= __('Edit Empresacnae') ?></legend>
        <?php
            echo $this->Form->input('empresa_id', ['options' => $empresas]);
            echo $this->Form->input('cnae_id', ['options' => $cnaes]);
            echo $this->Form->input('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
