<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Tipo de Receita</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?> </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('descricao', 'Descrição') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_cadastro') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('last_update', 'Ult. Atualização') ?></th>

                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($tiporeceitas as $tiporeceita) {
                                ?>                                
                                <tr class="<?= $cor ?> pointer">

                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= $tiporeceita->id ?>">
                                    </td>

                                    <td><?= h($tiporeceita->descricao) ?></td>
                                   <td><?= h($tiporeceita->dt_cadastro) ?></td>
                                    <td><?= h($tiporeceita->last_update) ?></td>

                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $tiporeceita->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $tiporeceita->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $tiporeceita->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $tiporeceita->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
