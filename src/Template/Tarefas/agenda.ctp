<?= $this->Html->css('/vendors/fullcalendar/dist/fullcalendar.min.css') ?>
<?= $this->Html->css('/vendors/fullcalendar/dist/fullcalendar.print.css', ['media' => 'print']) ?>
<?= $this->Html->css('agenda.css') ?>
<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<?= $this->Html->css('/vendors/pnotify/dist/pnotify.css'); ?>

<script type="text/javascript">
    var calendar;
</script>

<div class="row">
    <div class="" role="main">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Agenda</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </li>
                        <li>
                            <a class="close-link">
                                <i class="fa fa-close"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?= $this->Flash->render() ?>
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Filtros</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </li>
                        <li>
                            <a class="close-link">
                                <i class="fa fa-close"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">                   
                    <form name='formFiltros' id='formFiltros' class="form-horizontal form-label-left">
                        <?= $this->Flash->render() ?>
                        <?php if ($admin || $user_triade) { ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="filtro_areaservico_id">Setor 
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <?= $this->Form->input('filtro_areaservico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $areaservicos, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="filtro_gruposervico_id">Subsetor 
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <?= $this->Form->input('filtro_gruposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => [], 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="filtro_tiposervico_id">Serviço 
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <?= $this->Form->input('filtro_tiposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => [], 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                            </div>                         


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="filtro_empresa_id">Empresa
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <?= $this->Form->input('filtro_empresa_id', ['options' => $empresas, "class" => "form-control col-md-7 col-xs-12", "multiple" => "multiple", 'label' => false, "style" => "width: 100%"]); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="filtro_user_ids">Funcionário Tríade
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <?= $this->Form->input('filtro_user_ids', ['options' => $users, "class" => "form-control col-md-7 col-xs-12", "multiple" => "multiple", 'label' => false, "style" => "width: 100%"]); ?>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                        <?php } ?>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="filtro_tarefatipo_id">Tipo de Tarefa 
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?= $this->Form->input('filtro_tarefatipo_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tarefatipos, 'empty' => true, "style" => "width: 100%"]); ?>
                            </div> 
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="filtro_tarefaprioridade_id">Prioridade 
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?= $this->Form->input('filtro_tarefaprioridade_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tarefaprioridades, 'empty' => true, "style" => "width: 100%"]); ?>
                            </div> 
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mes">Mês 
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?= $this->Form->input('mes', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $meses, "multiple" => "multiple", "style" => "width: 100%"]); ?>
                            </div> 
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ano">Ano 
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?= $this->Form->input('ano', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $anos, "multiple" => "multiple", "style" => "width: 100%"]); ?>
                            </div> 
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="filtro_tarefaprioridade_id">Status 
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?= $this->Form->input('filtro_status', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => ["0" => "Atrasada", '1' => 'No Prazo', '2' => 'Concluída'], 'empty' => true, "style" => "width: 100%"]); ?>
                            </div> 
                        </div> 
                        <?php if ($admin || $user_triade) { ?>
                            <div class="form-group">
                                <label class="control-label col-md-9 col-sm-9 col-xs-10" for="filtro_minhas_tarefas">Exibir apenas minhas tarefas 
                                </label>
                                <div class="col-md-3 col-sm-3 col-xs-2">
                                    <?= $this->Form->checkbox('filtro_minhas_tarefas', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false, 'checked' => 'checked']); ?>
                                </div> 
                            </div> 
                        <?php } ?>
                        <div class="ln_solid"></div>
                        <!--</div>-->         
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                <button type="button" id='submitFiltros' class="btn btn-success">Filtrar</button>                 
                                <input type="button" id='resetFiltros' class="btn btn-primary" value="Limpar filtros" onclick="return clearFiltros();"/>                       
                            </div>
                        </div>
                    </form>
                </div>         
            </div>

        </div>
    </div>  
</div>

<!-- calendar modal New -->
<div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Nova Tarefa</h4>
            </div>
            <div class="modal-body">
                <div class="btn-group">
                    <a class="btn btn-principal"  title="Principal"><i class="fa fa-book"></i></a>
                </div>
                <?php if ($admin || $admin_empresa) { ?>
                    <div class="btn-group">
                        <a class="btn btn-destinatarios" title="Setor/Serviço"><i class="fa fa-cogs"></i>&nbsp;<b class="caret"></b></a>
                    </div>
                <?php } ?>
                <div class="btn-group">
                    <a class="btn btn-anexo"  title="Anexos"><i class="fa fa-paperclip"></i>&nbsp;<b class="caret"></b></a>
                </div>
                <div class="clearfix ln_solid"></div>
                <form id="antoform" class="form-horizontal calender formNew" action="/tarefas/add" data-form="formNew" role="form" enctype="multipart/form-data" method="POST">
                    <div id="principal">
                        <div class="form-group">
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" for="tarefatipo_id">Tipo <span class="required">*</span> <?= $this->Html->link('', ['controller' => 'Tarefatipos', 'action' => 'add'], ['class' => "fa fa-plus-square  fa-lg"]) ?>                        </label>
                            <div class="col-md-4 col-sm-3 col-xs-8">
                                <?= $this->Form->input('tarefatipo_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'options' => $tarefatipos, 'empty' => true, "style" => "width: 100%"]); ?>
                            </div>
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="tarefaprioridade_id">Prioridade</label>
                            <div class="col-md-4 col-sm-3 col-xs-8" style="padding-top: 5px;" >
                                <?= $this->Form->input('tarefaprioridade_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'options' => $tarefaprioridades, 'empty' => true, "style" => "width: 100%"]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" for="titulo">Título <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-3 col-xs-8">
                                <?= $this->Form->input('titulo', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>                  
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="descricao">Hora da Tarefa</label>
                            <div class="col-md-4 col-sm-3 col-xs-8" style="padding-top: 5px;">
                                <?= $this->Form->input('hr_tarefa', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'data-inputmask' => "'mask': '99:99'"]); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" for="dt_final">Dt. Final</label>
                            <div class="col-md-4 col-sm-3 col-xs-8">
                                <?= $this->Form->input('dt_final', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true, 'data-inputmask' => "'mask': '99/99/9999'"]); ?>

                            </div> 
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="dt_lembrete">Dt. Lembrete</label>
                            <div class="col-md-4 col-sm-3 col-xs-8" style="padding-top: 10px;">
                                <?= $this->Form->input('dt_lembrete', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true, 'data-inputmask' => "'mask': '99/99/9999'"]); ?>
                            </div> 
                        </div> 
                        <div class="form-group">          
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="descricao">Descrição</label>
                            <div class="col-md-10 col-sm-9 col-xs-8" style="padding-top: 5px;">
                                <?= $this->Form->input('descricao', ["type" => 'textarea', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <?php if ($admin) { ?>
                            <div class="form-group">
                                <label class="control-label label-input col-md-6 col-sm-6 col-xs-4" for="empresa_id">Informe se essa Tarefa será executada para uma empresa específica </label>
                                <div class="col-md-6 col-sm-6 col-xs-8">
                                    <?= $this->Form->input('empresa_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'options' => $empresas, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 

                            </div>
                        <?php } ?>


                    </div>
                    <div id="destinatarios">
                        <?php if ($admin) { ?>
                        <h2>Preencha SOMENTE se deseja criar uma tarefa para outra pessoa</h2>
                            <div class="form-group">
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" for="userempresa_id">Empresa </label>
                                <div class="col-md-4 col-sm-3 col-xs-8">
                                    <?= $this->Form->input('userempresa_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'options' => $empresas, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="areaservico_id">Setor </label>
                                <div class="col-md-4 col-sm-3 col-xs-8" style="padding-top: 5px;" >
                                    <?= $this->Form->input('areaservico_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                            </div>
                            <div class="form-group">
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" for="areaservico_id">Subsetor</label>
                                <div class="col-md-4 col-sm-3 col-xs-8">
                                    <?= $this->Form->input('gruposervico_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="tiposervico_id">Serviço</label>
                                <div class="col-md-4 col-sm-3 col-xs-8" style="padding-top: 5px;">
                                    <?= $this->Form->input('tiposervico_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                            </div>

                            <div class="form-group">
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" for="areaservico_id">Usuários</label>
                                <div class="col-md-10 col-sm-9 col-xs-8">
                                    <?= $this->Form->input('user_ids', ['options' => '', "class" => "select2 form-control", 'label' => false, "multiple" => "multiple", "style" => "width: 100%", 'placeholder' => 'click para selecionar os usuários']); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label label-input col-md-9 col-sm-9 col-xs-9" for="admin_empresa">Atribuir a tarefa SOMENTE ao(s) administradores da empresa</label>
                                <div class="col-md-3 col-sm-3 col-xs-3"  style="padding-top: 5px;">
                                    <?= $this->Form->checkbox('admin_empresa', ["class" => "form-control flat", 'label' => false, 'empty' => true]); ?>

                                </div>  
                            </div> 
                            <div class="form-group">
                                <label class="control-label label-input col-md-9 col-sm-9 col-xs-9" for="admin_setor">Atribuir a tarefa SOMENTE ao(s) administradores desse setor da empresa</label>
                                <div class="col-md-3 col-sm-3 col-xs-3" style="padding-top: 5px;">
                                    <?= $this->Form->checkbox('admin_setor', ["class" => "form-control flat", 'label' => false, 'empty' => true]); ?>

                                </div>  
                            </div> 
                        <?php } else if ($admin_empresa) { ?>
                            <div class="form-group">
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" for="areaservico_id">Setor <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-3 col-xs-8">
                                    <?= $this->Form->input('areaservico_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'options' => $areaservicos, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                                <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="user_ids">Usuário</label>
                                <div class="col-md-4 col-sm-3 col-xs-8" style="padding-top: 5px;">
                                    <?= $this->Form->input('user_ids', ['options' => $users, "class" => "select2 form-control", 'label' => false, "multiple" => "multiple", "style" => "width: 100%", 'placeholder' => 'click para selecionar os usuários']); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($admin || $admin_empresa) { ?>
                            <div class="form-group">
                                <label class="control-label label-input col-md-8 col-sm-8 col-xs-8" for="permite_deletar">PERMITIR que o usuário possa DELETAR essa tarefa?</label>
                                <div class="col-md-3 col-sm-3 col-xs-3"  style="padding-top: 5px;">
                                    <?= $this->Form->checkbox('permite_deletar', ["class" => "form-control flat", 'label' => false, 'empty' => true]); ?> Sim

                                </div>  
                            </div> 
                            <div class="form-group">
                                <label class="control-label label-input col-md-8 col-sm-8 col-xs-8" for="permite_editar">PERMITIR que o usuário possa EDITAR essa tarefa?
                                </label>
                                <div class="col-md-3 col-sm-3 col-xs-3"   style="padding-top: 5px;">
                                    <?= $this->Form->checkbox('permite_editar', ["class" => "form-control flat", 'label' => false, 'checked' => 'checked']); ?> Sim
                                </div> 
                            </div>   
                        <?php } ?>
                    </div>
                    <div id="anexos"  class="col-md-12 col-sm-12 col-xs-12">
                        <div id="attachment-fields">

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-4" for="setor">Setor  
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-8">
                                    <?= $this->Form->input('setor_id', [ 'options' => $areaservicos, "class" => "form-control col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%", 'empty' => 'selecione uma opção']); ?>
                                </div>
                                <div class="grupodocumento-id">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-4" for="grupo">Grupo 
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-8">
                                        <?= $this->Form->input('grupodocumento_id', [ "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%"]); ?>
                                    </div>
                                </div>
                            </div>                
                            <div class="form-group document-details">
                                <label class="control-label col-md-2 col-sm-2 col-xs-4" for="tipo">Documento 
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-8">
                                    <?= $this->Form->input('tipodocumento_id', [ "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%"]); ?>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-4" for="funcionario">Funcionário<i class="fa fa-question-circle" data-placement="top" data-toggle="tooltip" data-original-title="Selecione SOMENTE SE o documento for referente à esse funcionário."></i>
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-8">
                                    <?= $this->Form->input('funcionario_id', [ "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%"]); ?>
                                </div>
                            </div>                      
                            <div class="form-group document-details">
                                <label class="control-label col-md-2 col-sm-2 col-xs-4" for="descricao">Observação
                                </label>
                                <div class="col-md-10 col-sm-10 col-xs-8">
                                    <?= $this->Form->input('descricao-anexo', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                </div>
                            </div>  
                            <div id="fileField" class="col-md-12 col-sm-12 col-xs-12">
                                <div id="pathFile"  class="col-md-10 col-sm-10 col-xs-8">
                                    <?= $this->Form->file('file[]', ['label' => false, 'key' => 0, 'class' => 'file']) ?>      
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-4">
                                    <button class="addFile btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Adicionar</button>
                                </div>
                            </div>
                        </div>
                        <div class="btn-group">
                            <a class="btn btn-attach-view"  title="Anexos"><i class="fa fa-paperclip"></i>&nbsp; <span id="action-all-attach">Ver</span> anexos &nbsp;<b class="caret"></b></a>
                        </div>
                        <div class="form-group files"  style="display: none;">
                            <div id="queue">                        
                            </div>
                        </div> 
                    </div>
                    <?= $this->Form->input('dt_inicio', ['type' => 'hidden'], ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default antoclose btn-cancel" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnSave">Salvar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal de View -->
<div id="CalenderModalView" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Detalhes da Tarefa" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Detalhes da Tarefa</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12 col-sm-12 col-xs-12" id="tituloInfo">
                    <label class="label-input col-md-3 col-sm-3 col-xs-12">Título:
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12" id="tituloField">
                    </div> 
                </div> 
                <div class="col-md-12 col-sm-12 col-xs-12" id="empresaInfo">
                    <label class="label-input col-md-3 col-sm-3 col-xs-12">Empresa:
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12" id="empresaField">
                    </div> 
                </div> 
                <div class="col-md-12 col-sm-12 col-xs-12" id="tipotarefaInfo"  style="padding-top: 15px;">
                    <label class="label-input col-md-3 col-sm-3 col-xs-12">Tipo de Tarefa:
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12" id="tipotarefaField">
                    </div> 
                </div> 
                <div class="col-md-12 col-sm-12 col-xs-12" id="servicoInfo"  style="padding-top: 15px;">
                    <label class="label-input col-md-3 col-sm-3 col-xs-12">Serviço:
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12" id="servicoField">
                    </div> 
                </div> 
                <div class="col-md-12 col-sm-12 col-xs-12" id="statusservicoInfo"  style="padding-top: 15px;">
                    <label class="label-input col-md-3 col-sm-3 col-xs-12">Status do Serviço:
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12" id="statusservicoField">
                    </div> 
                </div> 
                <div class="col-md-12 col-sm-12 col-xs-12"  id="prioridadeInfo" style="padding-top: 15px;">
                    <label class="label-input col-md-3 col-sm-3 col-xs-12">Prioridade:
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12" id="prioridadeField">
                    </div> 
                </div> 
                <div class="col-md-12 col-sm-12 col-xs-12"  id="dtinicioInfo" style="padding-top: 15px;">
                    <label class="label-input col-md-3 col-sm-3 col-xs-12">Data da Tarefa:
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12" id="dtinicioField">
                    </div> 
                </div> 
                <div class="col-md-12 col-sm-12 col-xs-12"  id="dtfinalInfo" style="padding-top: 15px;">
                    <label class="label-input col-md-3 col-sm-3 col-xs-12">Data Final:
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12" id="dtfinalField">
                    </div> 
                </div> 
                <div class="col-md-12 col-sm-12 col-xs-12"  id="descricaoInfo" style="padding-top: 15px;">
                    <label class="label-input col-md-3 col-sm-3 col-xs-12">Descrição:
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12" id="descricaoField">
                    </div> 
                </div> 
                <!--Listar todos os campos aqui--> 
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-danger btnRemove margin-zero">Remover</button>-->
                <button type="button" class="btn btn-s btnView margin-zero" data-dismiss="modal">Mais Detalhes</button>
                <button type="button" class="btn btn-s btnDetailsService margin-zero" data-toggle="modal" data-target="#TaskModalServico">Info. Serviço</button>
                <button type="button" class="btn btn-primary btnEdit margin-zero">Editar</button>
                <button type="button" class="btn btn-success btnConcluir margin-zero" data-dismiss="modal" data-toggle="modal" data-target="#TaskModalConcluir">Concluir!</button>
                <button type="button" class="btn btn-success btnReabrir margin-zero" data-dismiss="modal"  data-toggle="modal" data-target="#TaskModalReabrir">Reabrir Tarefa</button>
                <button type="button" class="btn btn-default antoclose2 margin-zero" data-dismiss="modal">Fechar</button>

            </div>
        </div>
    </div>
</div>
<!-- /Modal de View-->

<!-- modal CONCLUIR -->
<div id="TaskModalConcluir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Concluir Tarefa" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Conclusão da Tarefa</h4>
            </div>
            <div class="modal-body">
                <form id="formConcluir" class="form-horizontal calender formConcluir" action="/tarefas/concluir" data-form="formConcluir" role="form" enctype="multipart/form-data" method="POST">
                    <div class="form-group">          
                        <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="anotacao_interna">Anotações internas da Tríade</label>
                        <div class="col-md-10 col-sm-9 col-xs-8" style="padding-top: 5px;">
                            <?= $this->Form->input('anotacao_interna', ['type' => 'textarea', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                        </div>
                    </div>
                    <div class="form-group notificacao_cliente">          
                        <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="anotacao_cliente"  data-placement="top" data-toggle="tooltip" data-original-title="O cliente receberá essas anotações por email">
                            Anotações para o Cliente</label>
                        <div class="col-md-10 col-sm-9 col-xs-8" style="padding-top: 5px;">
                            <?= $this->Form->input('anotacao_cliente', ['type' => 'textarea', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                        </div>
                    </div>
                    <div class="form-group dt_vencimento">
                        <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="dt_vencimento"  data-placement="top" data-toggle="tooltip" data-original-title="* o cliente será notificado por email no ato da conclusão dessa tarefa e também receberá lembrete no dia da data de vencimento">
                            Data de Vencimento</label>
                        <div class="col-md-4 col-sm-3 col-xs-8" style="padding-top: 10px;">
                            <?= $this->Form->input('dt_vencimento', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true, 'data-inputmask' => "'mask': '99/99/9999'"]); ?>
                        </div> 
                    </div> 
                    <div class="form-group dt_vencimento" >
                        <label class="control-label col-md-10 col-sm-10 col-xs-11" for="has_movimento">
                            Houve movimentação esse mês?
                        </label>
                        <div class="col-md-2 col-sm-2 col-xs-1">
                            <?= $this->Form->checkbox('has_movimento', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false, 'checked' => 'checked']); ?>
                        </div> 
                    </div>  

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"  data-dismiss="modal" id="btnConfirmaConclusao">Confirmar conclusão</button>
            </div>
        </div>
    </div>
</div>
<!--  /modal CONCLUIR -->


<!-- modal SERVIÇO -->
<div id="TaskModalServico" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Detalhes do Serviço" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Informações do Serviço</h4>
            </div>
            <div class="modal-body">
                <form id="formUpdateService" class="form-horizontal calender formUpdateService" data-form="formUpdateService" role="form">

                    <div class="form-group">          
                        <label class="control-label green" style="padding-top: 10px;" for="detalhes_servico" >
                            Detalhes do serviço (informado pelo cliente)
                        </label><br>
                        <span id="detalhes_servico" class="col-md-12 col-sm-12 col-xs-12"> </span>
                        <br><br>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="status_servico">Status do Serviço <span class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <?= $this->Form->select('statusservico', array('1' => 'Solicitado', '2' => 'Em análise', '3' => 'Em Execução', '4' => 'Executado'), ["class" => "form-control col-md-12  col-sm-12 col-xs-12", 'label' => false, 'style' => 'width:100%']); ?>
                        </div> 
                    </div> 
                </form>
            </div>
            <div class="modal-footer">
                <a type="button" class="btn btn-success abrir-servico">Abrir página do serviço</a>
                <button type="button" class="btn btn-primary"  data-dismiss="modal" id="btnAtualizaStatusServico">Atualizar Status</button>
            </div>
        </div>
    </div>
</div>
<!--  /modal SERVIÇO -->

<!-- modal Reabertura -->
<div id="TaskModalReabrir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Reabrir essa Tarefa" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Reabrir essa Tarefa</h4>
            </div>
            <div class="modal-body">
                <form id="formReabrir" class="form-horizontal calender formReabrir" action="/tarefas/reabrir" data-form="formReabrir" role="form" enctype="multipart/form-data" method="POST">
                    <div id="principal">                        
                        <div class="form-group">          
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" style="padding-top: 10px;" for="justificativa_refazer">Escreva em detalhes por qual motivo você deseja reabrir essa Tarefa</label>
                            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top: 5px;">
                                <?= $this->Form->input('justificativa_refazer', ['type' => 'textarea', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-10 col-sm-10 col-xs-11" for="notificacao_refazer">
                                O responsável deverá ser notificado para refazer essa Tarefa?
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-1">
                                <?= $this->Form->checkbox('notificacao_refazer', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false, 'checked' => 'checked']); ?>
                            </div> 
                        </div>  

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"  data-dismiss="modal" id="btnConfirmaReabertura">Confirmar Reabertura da Tarefa</button>
            </div>
        </div>
    </div>
</div>
<!--  /modal Reabertura -->



<?= $this->Form->input('urlroot', ["type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
</div>
<div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
<div id="fc_view" data-toggle="modal" data-target="#CalenderModalView"></div>


<?= $this->Html->script('/vendors/moment/min/moment.min.js'); ?>
<?= $this->Html->script('/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>
<?= $this->Html->script('/vendors/fullcalendar/dist/fullcalendar.min.js'); ?>
<?= $this->Html->script('/vendors/fullcalendar/dist/lang/pt-br.js'); ?>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js'); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/vendors/pnotify/dist/pnotify.js'); ?>
<?= $this->Html->script('/js/ajax/documentos.js'); ?>
<?= $this->Html->script('/js/ajax/empresas.js'); ?>
<?= $this->Html->script('/js/ajax/servicos.js'); ?>
<?= $this->Html->script('/js/date.js'); ?>
<?= $this->Html->script('/js/ajax/agenda.js'); ?>
<script>
    var keys_anexos = 0;
    $(document).ready(function () {
        $("#anexos").hide();
        $('#destinatarios').hide();
        $('#gruposervico-id').hide();
        $(":input").inputmask();
        $("select").select2({minimumResultsForSearch: 6});
        $("#filtro-empresa-id, #filtro-user-ids").select2({multiple: true, tokenSeparators: [',', ' '], tags: true, allowClear: true});
        $("#tarefatipo-id,#empresa-id,#userempresa-id,#areaservico-id,#gruposervico_id,#tiposervico-id,#setor-id,#grupodocumento-id,#tipodocumento-id,#funcionario-id").select2({dropdownParent: $("#CalenderModalNew"), minimumResultsForSearch: 6});
        $("#user-ids").select2({multiple: true, tokenSeparators: [',', ' '], tags: true, allowClear: true});

        /*-------------------------EVENTOS---------------------------------*/
        $('.btn-anexo').click(function (e) {
            $('#destinatarios').hide();
            if ($('#anexos').css('display') == 'block') {
                $('#principal').slideDown();
            } else {
                if ($('#setor-id').val() == '' || $('#setor-id').val() == null) {
                    $('.document-details').hide();
                    $('.grupodocumento-id').hide();
                } else {
                    if ($('#grupodocumento-id').val() == '' || $('#grupodocumento-id').val() == null) {
                        $('.grupodocumento-id').show();
                        $('.document-details').hide();
                    } else {
                        $('.document-details').show();
                        $('.grupodocumento-id').show();
                    }
                }
                $('#principal').slideUp();
            }
            $('#anexos').slideToggle();

        });
        $('.btn-destinatarios').click(function (e) {
            $('#anexos').hide();
            if ($('#destinatarios').css('display') == 'block') {
                $('#principal').slideDown();
            } else {
                $('#principal').slideUp();
            }
            $('#destinatarios').slideToggle();
        });
        $('.btn-principal').click(function (e) {
            $('#anexos').slideUp();
            $('#destinatarios').slideUp();
            $('#principal').slideDown();
        });
        $('#fc_create').click(function (e) {
            $('#anexos').hide();
            $('#destinatarios').hide();
            $('#principal').slideDown();
        });
        $('.btn-cancel').click(function (e) {
            clearModalNew();
        });
        $('.addFile').click(function (e) {
            e.preventDefault();
            if (!$('#setor-id').val() || !$('#grupodocumento-id').val() || !$('#tipodocumento-id').val()) {
                alert('Os campos Setor/Grupo/Tipo de documentos devem ser preenchidos para poder adicionar o arquivo.');
            } else {
                addAttachment();
            }
        });
        $('.btn-attach-view').click(function (e) {
            $('.files').slideToggle();
            if ($('#action-all-attach').html() == 'Ver') {
                $('#action-all-attach').html('Ocultar');
            } else {
                $('#action-all-attach').html('Ver');
            }
        });
        $('select').on('select2:open', function (e) {
            $('.select2-search input').prop('focus', false);
        });
        $('#submitFiltros').click(function (e) {
            loadTasks();
        });


    });
    function clearModalNew() {
        var fields = ['hr-tarefa', 'tarefatipo-id', 'setor-id', 'grupodocumento-id', 'tipodocumento-id', 'tiposervico-id', 'empresa-id', 'userempresa-id', 'funcionario-id', 'descricao', 'descricao-anexo', 'areaservico-id', 'user-ids', 'titulo', 'tarefaprioridade-id', 'dt-final', 'dt-lembrete'];
        var radios = ['permite_deletar', 'permite_editar', 'admin_empresa', 'admin_setor'];

        var selects = ['grupodocumento-id', 'tipodocumento-id', 'tiposervico-id', 'user-ids'];
        var selects2 = ['tarefaprioridade-id', 'tarefatipo-id', 'setor-id', 'grupodocumento-id', 'tipodocumento-id'];
<?php if ($admin) { ?>
            selects.push('userempresa-id');
            selects.push('empresa-id');
            selects.push('areaservico-id');
            selects.push('funcionario-id');
            selects.push('user-ids');
            selects2.push('userempresa-id');
            selects2.push('empresa-id');
            selects2.push('areaservico-id');
            selects2.push('funcionario-id');
            selects2.push('user-ids');
<?php } ?>
        clearFieldsInfo(fields);
        clearCheckedRadios(radios);
        clearSelectOptions(selects);
        refreshSelect2(selects2);
        detachAllFiles();
    }
    function clearFiltros() {
        var selects = ['filtro-areaservico-id', 'filtro-gruposervico-id', 'filtro-tiposervico-id', 'filtro-empresa-id', 'filtro-user-ids', 'filtro-tarefatipo-id', 'filtro-tarefaprioridade-id', 'mes', 'ano', 'filtro-status'];
        clearFieldsInfo(selects);
        refreshSelect2(selects);
    }
    function addAttachment() {
        new PNotify({
            text: 'Arquivo adicionado à lista de documentos',
            type: 'success',
            styling: 'bootstrap3',
            animate_speed: 'fast'
        });

        getPropertiesFile();
        additemListFiles();
        hideLatestAttachmentField();
        newAttachmentField();
    }
    function getPropertiesFile() {
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='tipo_documentos[" + keys_anexos + "]' value='" + $('#tipodocumento-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='grupo_documentos[" + keys_anexos + "]' value='" + $('#grupodocumento-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='area_servicos[" + keys_anexos + "]' value='" + $('#setor-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='funcionario[" + keys_anexos + "]' value='" + $('#funcionario-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='descricoes[" + keys_anexos + "]' value='" + $('#descricao-anexo').val() + "'>");
    }
    function additemListFiles() {
        $('#queue').append('<div class="itemqueue col-md-4 col-sm-4 col-xs-12 well well-sm"  style="padding: 0;" item="' + keys_anexos + '">\
                            <div id="descFile" class="col-md-9 col-sm-9 col-xs-9">' +
                $('#setor-id option:selected').text() + '/ ' + $('#grupodocumento-id option:selected').text() + '/ ' + $('#tipodocumento-id option:selected').text()
                + '</div>\
                <div class="removeFile col-md-3 col-sm-3 col-xs-3">\
                    <button class="remove btn btn-sm btn-danger" type="button" onclick="removeItemListFiles(this,' + keys_anexos + ');"><i class="fa fa-trash"></i></button>\
                </div></div>');
    }
    function hideLatestAttachmentField() {
        $('.file').hide();
    }
    function showLatestAttachmentField() {
        $('.file').show();
    }
    function newAttachmentField() {
        keys_anexos++;
        $('#pathFile').append("<input type='file' class='file' key='" + keys_anexos + "' name='file[]'>");
    }
    function removeItemListFiles(item, key) {
        $(item).parent().parent().remove();
        $(':input[key=' + key + ']').remove();
    }
    function detachAllFiles() {
        //remove a lista de arquivos
        $('.itemqueue').remove();
        //remove todos os campos vinculados aos arquivos
        for (var i = keys_anexos; i > 0; i--) {
            $(':input[key=' + i + ']').remove();
        }
        //limpa o campo do select do arquivo principal
        $(":input[key='0']").val('');
        //exibe para poder selecionar o arquivo
        showLatestAttachmentField();

    }


</script>