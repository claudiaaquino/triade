
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Tarefas Automáticas - geradas em <?= date('d/m/Y'); ?></h2> 
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php if (!empty($tarefas)) { ?>
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col" class="column-title">Dt. Inicio</th>
                                    <th scope="col" class="column-title">Prioridade</th>
                                    <th scope="col" class="column-title">Setor</th>
                                    <th scope="col" class="column-title">Serviço</th>
                                    <th scope="col" class="column-title">Executor</th>                              
                                    <th scope="col" class="column-title">Empresa</th>    
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tarefas as $tarefa) {
                                    ?>                                
                                    <tr class="<?= $cor ?> pointer">

                                        <td><?= h($tarefa->dt_inicio) ?></td>
                                        <td><?= $tarefa->has('tarefaprioridade') ? $this->Html->link($tarefa->tarefaprioridade->descricao, ['controller' => 'Tarefaprioridades', 'action' => 'view', $tarefa->tarefaprioridade->id]) : '' ?></td>
                                        <td><?= $tarefa->has('areaservico') ? $this->Html->link($tarefa->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $tarefa->areaservico->id]) : '' ?></td>
                                        <td><?= $tarefa->has('tiposervico') ? $this->Html->link($tarefa->tiposervico->nome, ['controller' => 'Tiposervicos', 'action' => 'view', $tarefa->tiposervico->id]) : '' ?></td>
                                        <td><?php
                                            if ($tarefa->has('tarefausuarios')) {
                                                foreach ($tarefa->tarefausuarios as $responsavel) {
                                                    echo '- ' . $responsavel->user->nome;
                                                }
                                            }
                                            ?></td>
                                        <td><?= $tarefa->has('empresa') ? $this->Html->link($tarefa->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $tarefa->empresa->id]) : '' ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $tarefa->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                }
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                <?php } else { ?>
                    Não houveram novas tarefas automáticas nessa data
                <?php } ?>
            </div>
        </div>
    </div>
</div>
