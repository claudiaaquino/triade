<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<?= $this->Html->css('/vendors/pnotify/dist/pnotify.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Editar informações da Tarefa  <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <?php if ($admin || $loggeduser == $tarefa->user_id) { ?>
                        <li><?= $this->Html->link(__(' Visualizar Tarefa'), ['controller' => 'Tarefas', 'action' => 'view', $tarefa->id], ['class' => "btn btn-dark fa fa-file-o"]) ?>  </li>
                        <li><?= $this->Html->link(__(' Anexar Documentos'), ['controller' => 'Tarefadocumentos', 'action' => 'add', $tarefa->id], ['class' => "btn btn-dark fa fa-paperclip"]) ?>  </li>
                        <?php if ($tarefa->dt_concluida) { ?>
                            <li><button type="button" class="btn btn-success btnReabrir margin-zero fa fa-angle-double-left" data-toggle="modal" data-target="#TaskModalReabrir">
                                    Reabrir Tarefa</button></li>
                        <?php } else { ?>
                            <li><button type="button" class="btn btn-success btnConcluir margin-zero fa fa-angle-double-right" data-toggle="modal" data-target="#TaskModalConcluir">
                                    Concluir Tarefa</button></li>
                            <?php
                        }
                    }
                    ?>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($tarefa, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>                
                <h3>Informações Principais</h3>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="titulo">Título <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('titulo', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>              

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tarefatipo_id">Tipo de Tarefa <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tarefatipo_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tarefatipos]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tarefaprioridade_id">Prioridade 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tarefaprioridade_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tarefaprioridades, 'empty' => true]); ?>
                    </div> 
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_inicio">Dt Início da tarefa <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('dt_inicio', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", "data-inputmask" => "'mask': '99/99/99'", 'label' => false, 'empty' => true]); ?>

                    </div> 
                </div>                         
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_inicio">Hora da tarefa 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('hr_tarefa', ["class" => "form-control col-md-7 col-xs-12", "data-inputmask" => "'mask': '99:99'", 'label' => false, 'empty' => true]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_final">Dt. Final 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('dt_final', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", "data-inputmask" => "'mask': '99/99/99'", 'label' => false, 'empty' => true]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_lembrete">Dt. Lembrete no sistema 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('dt_lembrete', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", "data-inputmask" => "'mask': '99/99/99'", 'label' => false, 'empty' => true]); ?>

                    </div> 
                </div>      


                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Descrição 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>      

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_concluida">Dt. da Conlusão
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('dt_concluida', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", "data-inputmask" => "'mask': '99/99/99'", 'label' => false, 'empty' => true]); ?>

                    </div> 
                </div>                         


                <?php if ($admin || $admin_empresa) { ?>
                    <div class="ln_solid"></div>                
                    <h3>Informações Especificas</h3>
                    <?php if ($admin) { ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_id">Empresa 
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('empresa_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $empresas]); ?>
                            </div> 
                        </div> 
                    <?php } ?>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="areaservico_id">Setor 
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('areaservico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $areaservicos, 'empty' => true]); ?>
                        </div> 
                    </div> 

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tiposervico_id">Serviço 
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('tiposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tiposervicos, 'empty' => true]); ?>
                        </div> 
                    </div> 


                    <?php if ($admin) { ?>

                        <div class="form-group">
                            <label class="control-label col-md-6 col-sm-6  col-xs-11" for="permite_deletar"  data-placement="top" data-toggle="tooltip" data-original-title="Essa opção só aparece para você que é administrador">
                                PERMITIR que o usuário possa DELETAR essa tarefa?
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-1">
                                <?= $this->Form->checkbox('permite_deletar', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                            </div> 
                        </div>              
                        <!--                        <div class="form-group">
                                                    <label class="control-label col-md-6 col-sm-6  col-xs-11" for="permite_editar">PERMITIR que o usuário possa EDITAR essa tarefa?
                                                    </label>
                                                    <div class="col-md-3 col-sm-3 col-xs-1">
                                                        <? $this->Form->checkbox('permite_editar', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>
                        
                                                    </div> 
                                                </div>   -->
                    <?php } ?>
                <?php } ?>
                <div class="clearfix ln_solid">  </div>              

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
    <?php if (!empty($tarefa->tarefadocumentos)): ?>
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Documentos Anexados </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar Outro'), ['controller' => 'Tarefadocumentos', 'action' => 'add', $tarefa->id], ['class' => "btn btn-dark fa fa-file"]) ?>  </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th scope="col"  class="column-title"><?= __('Documento') ?></th>
                                <th scope="col"  class="column-title"><?= __('Descrição') ?></th>
                                <th scope="col"  class="column-title"><?= __('Tamanho do Arquivo') ?></th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($tarefa->tarefadocumentos as $tarefadocumentos):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td><?= h($tarefadocumentos->documento->tipodocumento->descricao) ?></td>
                                    <td><?= h($tarefadocumentos->documento->descricao) ?></td>
                                    <td><?= ($tarefadocumentos->documento->filesize / 1024) < 1024 ? floor($tarefadocumentos->documento->filesize / 1024) . ' KB' : floor(($tarefadocumentos->documento->filesize / 1024) / 1024) . ' MB'; ?></td>

                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Download'), "/docs/" . $tarefadocumentos->documento->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefadocumentos', 'action' => 'delete', $tarefadocumentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefadocumentos->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (!empty($tarefa->tarefausuarios)): ?>
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Usuários Responsáveis por essa Tarefa </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar Outro'), ['controller' => 'Tarefausuarios', 'action' => 'add', $tarefa->id], ['class' => "btn btn-dark fa fa-file"]) ?>  </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th scope="col"  class="column-title">Nome do Responsável</th>
                                <th scope="col"  class="column-title">Dt Concluída</th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($tarefa->tarefausuarios as $tarefausuarios):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td><?= h($tarefausuarios->user->nome) ?></td>
                                    <td><?= $tarefausuarios->dt_concluida ? $tarefausuarios->dt_concluida : 'NÃO CONCLUÍDA' ?></td>

                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefausuarios', 'action' => 'delete', $tarefausuarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefausuarios->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js', array('inline' => false)); ?>
<?= $this->Html->script('/vendors/pnotify/dist/pnotify.js'); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/js/ajax/empresas.js'); ?>
<?= $this->Html->script('/js/ajax/servicos.js'); ?>


<!-- modal CONCLUIR -->
<div id="TaskModalConcluir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Concluir Tarefa" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Conclusão da Tarefa</h4>
            </div>
            <div class="modal-body">
                <form id="formConcluir" class="form-horizontal calender formConcluir" action="/tarefas/concluir" data-form="formConcluir" role="form" enctype="multipart/form-data" method="POST">
                    <div id="principal">
                        <div class="form-group">          
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="anotacao_interna">Anotações internas da Tríade</label>
                            <div class="col-md-10 col-sm-9 col-xs-8" style="padding-top: 5px;">
                                <?= $this->Form->input('anotacao_interna', ['type' => 'textarea', 'value' => $tarefa->anotacao_interna, "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group notificacao_cliente">          
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="anotacao_cliente"  data-placement="top" data-toggle="tooltip" data-original-title="O cliente receberá essas anotações por email">
                                Anotações para o Cliente</label>
                            <div class="col-md-10 col-sm-9 col-xs-8" style="padding-top: 5px;">
                                <?= $this->Form->input('anotacao_cliente', ['type' => 'textarea', 'value' => $tarefa->anotacao_cliente, "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group dt_vencimento">
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="dt_vencimento"  data-placement="top" data-toggle="tooltip" data-original-title="* o cliente será notificado por email no ato da conclusão dessa tarefa e também receberá lembrete no dia da data de vencimento">
                                Data de Vencimento</label>
                            <div class="col-md-4 col-sm-3 col-xs-8" style="padding-top: 10px;">
                                <?= $this->Form->input('dt_vencimento', ["class" => "form-control col-md-7 col-xs-12", 'value' => $tarefa->dt_vencimento, 'label' => false, 'empty' => true, 'data-inputmask' => "'mask': '99/99/9999'"]); ?>
                            </div> 
                        </div> 
                        <div class="form-group dt_vencimento" >
                            <label class="control-label col-md-10 col-sm-10 col-xs-11" for="has_movimento">
                                Houve movimentação esse mês?
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-1">
                                <?= $this->Form->checkbox('has_movimento', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false, 'checked' => 'checked']); ?>
                            </div> 
                        </div>  
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"  data-dismiss="modal" id="btnConfirmaConclusao">Confirmar conclusão</button>
            </div>
        </div>
    </div>
</div>
<!--  /modal CONCLUIR -->

<!-- modal Reabertura -->
<div id="TaskModalReabrir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Reabrir essa Tarefa" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Reabrir essa Tarefa</h4>
            </div>
            <div class="modal-body">
                <form id="formReabrir" class="form-horizontal calender formReabrir" action="/tarefas/reabrir" data-form="formReabrir" role="form" enctype="multipart/form-data" method="POST">
                    <div id="principal">                        
                        <div class="form-group">          
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" style="padding-top: 10px;" for="justificativa_refazer">Escreva em detalhes por qual motivo você deseja reabrir essa Tarefa</label>
                            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top: 5px;">
                                <?= $this->Form->input('justificativa_refazer', ['type' => 'textarea', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-10 col-sm-10 col-xs-11" for="notificacao_refazer">
                                O responsável deverá ser notificado para refazer essa Tarefa?
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-1">
                                <?= $this->Form->checkbox('notificacao_refazer', ['hiddenField' => false, "class" => "form-control flat col-md-7 col-xs-12", 'label' => false, 'checked' => 'checked']); ?>
                            </div> 
                        </div>  

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                <?= $this->Form->input('tarefa_id', [ "type" => "hidden", "value" => $tarefa->id, 'label' => false]); ?>
                <button type="button" class="btn btn-primary"  data-dismiss="modal" id="btnConfirmaReabertura">Confirmar Reabertura da Tarefa</button>
            </div>
        </div>
    </div>
</div>
<!--  /modal Reabertura -->


<script>
    $(document).ready(function () {
        $(":input").inputmask();
        $("select").select2();
        $("#btnConfirmaReabertura").click(function () {
            reabrirTarefa();
        });
        $("#btnConfirmaConclusao").click(function () {
            concluirTarefa();
        });
    });
    function reabrirTarefa() {
        $.ajax({
            url: $('#urlroot').val() + 'tarefas/reabrir/' + $('#tarefa-id').val(),
            data: $("#formReabrir").serialize(),
            type: 'PATCH',
            dataType: 'json',
            success: function (data) {
                if (data.retorno) {
                    $("#dt-concluida").val('');
                    $(".btnReabrir").hide();

                    new PNotify({text: 'Tarefa reaberta na agenda', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possivel reabrir essa tarefa', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });

    }
    function concluirTarefa() {

        $.ajax({
            url: $('#urlroot').val() + 'tarefas/concluir/' + $('#tarefa-id').val(),
            data: $("#formConcluir").serialize(),
            dataType: 'json',
            type: 'PATCH',
            success: function (data) {
                if (data.retorno) {
                    $("#dt-concluida").val(fullDateToshortBrDate(data.retorno));
                    $(".btnConcluir").hide();

                    new PNotify({text: 'Tarefa concluída!!', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Erro ao concluir essa tarefa. Por favor, tente novamente.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                new PNotify({text: 'Erro ao concluir essa tarefa. Por favor, tente novamente.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
            }
        });
    }
    var fullDateToshortBrDate = function (fulldate) {
        var date = fulldate.split(" ");
        date = date[0].split("-");
        return  date[2] + '/' + date[1] + '/' + date[0].substring(2, 4);
    };

</script>

