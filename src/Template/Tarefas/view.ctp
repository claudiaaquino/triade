<?= $this->Html->css('/vendors/pnotify/dist/pnotify.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Informações da Tarefa</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <?php if ($admin || ($loggeduser == $tarefa->user_id)) { ?>
                        <li><?= $this->Html->link(__(' Editar Tarefa'), ['controller' => 'Tarefas', 'action' => 'edit', $tarefa->id], ['class' => "btn btn-dark fa fa-pencil-square-o"]) ?>  </li>
                        <li><?= $this->Html->link(__(' Anexar Documentos'), ['controller' => 'Tarefadocumentos', 'action' => 'add', $tarefa->id], ['class' => "btn btn-dark fa fa-paperclip"]) ?>  </li>
                        <?php if ($tarefa->dt_concluida) { ?>
                            <li><button type="button" class="btn btn-success btnReabrir margin-zero fa fa-angle-double-left" data-toggle="modal" data-target="#TaskModalReabrir">
                                    Reabrir Tarefa</button></li>
                        <?php } else { ?>
                            <li><button type="button" class="btn btn-success btnConcluir margin-zero fa fa-angle-double-right" data-toggle="modal" data-target="#TaskModalConcluir">
                                    Concluir Tarefa</button></li>
                            <?php
                        }
                    }
                    ?>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <?php if ($admin || $user_triade) { ?>

                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="project_detail">


                            <p class="title green"><?= __('Título') ?></p>
                            <p>  <?= $tarefa->titulo ? $tarefa->titulo : 'Não Informado'; ?></p>

                            <p class="title green"><?= __('Tipo de Tarefa') ?></p>
                            <p><?= $tarefa->has('tarefatipo') ? $this->Html->link($tarefa->tarefatipo->descricao, ['controller' => 'Tarefatipos', 'action' => 'view', $tarefa->tarefatipo->id]) : 'Não Informado' ?></p>

                            <p class="title green"><?= __('Prioridade') ?></p>
                            <p><?= $tarefa->has('tarefaprioridade') ? $this->Html->link($tarefa->tarefaprioridade->descricao, ['controller' => 'Tarefaprioridades', 'action' => 'view', $tarefa->tarefaprioridade->id]) : 'Não Informado' ?></p>

                            <p class="title green"><?= __('Descrição') ?></p>
                            <p>  <?= $tarefa->descricao ? $tarefa->descricao : 'Não Informado'; ?></p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="project_detail">
                            <p class="title green"><?= __('Dia da Tarefa') ?></p>
                            <p><?= $tarefa->dt_inicio ? $tarefa->dt_inicio : 'Não Informado'; ?></p>


                            <p class="title green"><?= __('Data Final da Tarefa') ?></p>
                            <p><?= $tarefa->dt_final ? $tarefa->dt_final : 'Não Informado'; ?></p>

                            <p class="title green"><?= __('Dt Lembrete no sistema') ?></p>
                            <p><?= $tarefa->dt_lembrete ? $tarefa->dt_lembrete : 'Não Informado'; ?></p>

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="project_detail">

                            <p class="title green"><?= __('Dt Cadastro') ?></p>
                            <p><?= $tarefa->dt_cadastro ? $tarefa->dt_cadastro : 'Não Informado'; ?></p>


                            <p class="title green"><?= __('Dt Reabertura') ?></p>
                            <p id="info_dt_reaberta"><?= $tarefa->dt_reaberta ? $tarefa->dt_reaberta : 'não se aplica'; ?></p>



                            <?php /* if ($tarefa->dt_concluida) { */ ?>
                            <p class="title green"><?= __('Dt Conclusão') ?></p>
                            <p id="info_dt_concluida"><?= $tarefa->dt_concluida ? $tarefa->dt_concluida : 'ainda não concluída'; ?></p>
                            <?php /* } */ ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="project_detail">
                            <p class="title green"><?= __('Serviço') ?></p>
                            <p>  <?= $tarefa->titulo ? $tarefa->titulo : 'Não Informado'; ?></p>

                            <p class="title green"><?= __('Descrição') ?></p>
                            <p>  <?= $tarefa->descricao ? $tarefa->descricao : 'Não Informado'; ?></p>

                            <p class="title green"><?= __('Competência') ?></p>
                            <p>  <?= $tarefa->competencia ? $tarefa->competencia : 'Não Informado'; ?></p>

                            <p class="title green"><?= __('Dt. de Vencimento do Documento') ?></p>
                            <p>  <?= $tarefa->dt_vencimento ? $tarefa->dt_vencimento : 'Não Informado'; ?></p>
                        </div>
                    </div>
                <?php } ?>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="project_detail">
                        <?php if ($user_triade) { ?>
                            <div id="div_justificativa_refazer" style="<?= $tarefa->justificativa_refazer ? '' : 'display: none'; ?>">
                                <div class="ln_solid"></div>
                                <p class="title green">Motivo para reabertura da tarefa</p>
                                <p  id="info_justificativa_refazer"><?= $tarefa->justificativa_refazer ? $tarefa->justificativa_refazer : 'Não Informado'; ?></p>
                            </div>
                        <?php } ?>
                        <?php if ($tarefa->anotacao_interna && $user_triade) { ?>
                            <div class="ln_solid"></div>
                            <p class="title green">Anotações internas da Tríade</p>
                            <p><?= $tarefa->anotacao_cliente ? $tarefa->anotacao_cliente : 'Não Informado'; ?></p>
                        <?php } ?>
                        <?php if ($tarefa->anotacao_cliente) { ?>
                            <div class="ln_solid"></div>
                            <p class="title green">Anotações <?= $user_triade ? ' para o cliente' : '' ?></p>
                            <p><?= $tarefa->anotacao_cliente ? $tarefa->anotacao_cliente : 'Não Informado'; ?></p>
                            <div class="ln_solid"></div>
                        <?php } ?>

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($tarefa->empresa && $user_triade) { ?>
            <div class="x_panel">         
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Informações Específicas</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <?php if ($admin && $tarefa->servico_id) { ?>          
                            <li><?= $this->Html->link(__(' Ver Serviço que gerou essa Tarefa'), ['controller' => 'Servicos', 'action' => 'view', $tarefa->servico_id], ['class' => "btn btn-primary  margin-zero fa fa-inbox"]) ?>  </li>
                        <?php } ?>
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="project_detail">

                            <p class="title"><?= __('Empresa que foi atribuida a tarefa') ?></p>
                            <p><?= $tarefa->has('empresa') ? $this->Html->link($tarefa->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $tarefa->empresa->id]) : 'Não Informado' ?></p>

                            <?php if ($tarefa->areaservico) { ?>
                                <p class="title"><?= __('Setor da Empresa') ?></p>
                                <p><?= $tarefa->has('areaservico') ? $this->Html->link($tarefa->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $tarefa->areaservico->id]) : 'Não Informado' ?></p>
                            <?php } ?>

                            <?php if ($tarefa->tiposervico) { ?>
                                <p class="title"><?= __('Serviço refente à Tarefa') ?></p>
                                <p><?= $tarefa->has('tiposervico') ? $this->Html->link($tarefa->tiposervico->descricao, ['controller' => 'Tiposervicos', 'action' => 'view', $tarefa->tiposervico->id]) : 'Não Informado' ?></p>
                            <?php } ?>                     

                            <?php if ($admin && $tarefa->tarefausuarios) { ?>
                                <p class="title"><?= __('Usuário que cadastrou a tarefa') ?></p>
                                <p><?= $tarefa->has('user') ? $this->Html->link($tarefa->user->nome, ['controller' => 'Users', 'action' => 'view', $tarefa->user->id]) : 'Não Informado' ?></p>
                            <?php } ?>

                        </div>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <div class="project_detail">

                            <?php if ($tarefa->admin_empresa) { ?>
                                <p class="title"><i class="fa fa-tag green"></i> <?= __('Essa Tarefa foi atribuida SOMENTE para os Administradores da Empresa') ?></p><br>
                            <?php } ?>                     


                            <?php if ($tarefa->admin_setor) { ?>
                                <p class="title"><i class="fa fa-tag green"></i> Essa Tarefa foi atribuida SOMENTE para os <div class="green">Administradores do Setor <?= $tarefa->areaservico->descricao ?> da Empresa <?= $tarefa->empresa->razao ?></div></p><br>
                            <?php } ?>    

                            <?php if ($tarefa->automatica) { ?>
                                <p class="title"><i class="fa fa-tag green"></i> <?= __('Essa Tarefa foi gerada automaticamente pelo sistema') ?></p><br>
                            <?php } ?>

                            <?php if ($tarefa->permite_deletar) { ?>
                                <p class="title"><i class="fa fa-tag green"></i> Essa Tarefa PODE SER DELETADA pelas pessoas atribuidas à ela.</p><br>
                            <?php } else { ?>
                                <p class="title"><i class="fa fa-tag red"></i> Essa Tarefa NÃO PODE SER DELETADA pelas pessoas atribuidas à ela.<br> Somente pelo admin da Tríade ou pelo próprio criador dessa tarefa</p><br>
                            <?php } ?>

                            <?php if ($tarefa->permite_editar) { ?>
                                <p class="title"><i class="fa fa-tag green"></i> Essa Tarefa PODE SER EDITADA pelas pessoas atribuidas à ela.</p><br>
                            <?php } else { ?>
                                <p class="title"><i class="fa fa-tag red"></i> Essa Tarefa NÃO PODE SER EDITADA pelas pessoas atribuidas à ela.<br> Somente pelo admin da Tríade ou pelo próprio criador dessa tarefa</p><br>
                            <?php } ?>


                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php if (!empty($tarefa->tiposervico) && ($tarefa->tiposervico->instrucoes_execucao || $documentos)) { ?>
    <div class="x_panel">
        <div class="x_title">
            <h2 class="green"><i class="fa fa-file"></i> Instruções para execução dessa tarefa </h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?php if ($tarefa->tiposervico->instrucoes_execucao) { ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="project_detail">
                        <p class="title green"><?= __('Instruções') ?></p>
                        <p>  <?= $tarefa->tiposervico->instrucoes_execucao ? $this->Text->autoParagraph($tarefa->tiposervico->instrucoes_execucao) : 'Não Informado'; ?></p>

                    </div>
                </div>
                <br>
                <?php
            }
            if (!empty($documentos)) {
                ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="project_detail">
                        <p class="title green"><?= __('Arquivos de modelo/intruções') ?></p><br>
                        <?php
                        foreach ($documentos as $documento) {
                            ?>
                            <p class="title">
                                <i class="fa fa-chevron-right"></i> 
                                <?= $this->Html->link($documento, $diretorio . $documento, ['target' => '_blank']) ?>
                            </p>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>
<?php if (!empty($tarefa->tarefadocumentos)): ?>
    <div class="x_panel">
        <div class="x_title">
            <h2 class="green"><i class="fa fa-file"></i> Documentos Anexados </h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><?= $this->Html->link(__('  Cadastrar Outro'), ['controller' => 'Tarefadocumentos', 'action' => 'add', $tarefa->id], ['class' => "btn btn-dark fa fa-file"]) ?>  </li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th scope="col"  class="column-title"><?= __('Documento') ?></th>
                            <th scope="col"  class="column-title"><?= __('Descrição') ?></th>
                            <th scope="col"  class="column-title"><?= __('Tamanho do Arquivo') ?></th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $cor = 'even';
                        foreach ($tarefa->tarefadocumentos as $tarefadocumentos):
                            ?>
                            <tr class="<?= $cor ?> pointer">
                                <td><?= h($tarefadocumentos->documento->tipodocumento->descricao) ?></td>
                                <td><?= h($tarefadocumentos->documento->descricao) ?></td>
                                <td><?= ($tarefadocumentos->documento->filesize / 1024) < 1024 ? floor($tarefadocumentos->documento->filesize / 1024) . ' KB' : floor(($tarefadocumentos->documento->filesize / 1024) / 1024) . ' MB'; ?></td>

                                <td  class=" last">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('Download'), "/docs/" . $tarefadocumentos->documento->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                        <?php if ($user_triade) echo $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefadocumentos', 'action' => 'delete', $tarefadocumentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefadocumentos->id)]) ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $cor = $cor == 'even' ? 'odd' : 'even';
                        endforeach;
                        ?>
                    </tbody>                       
                </table>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (!empty($tarefa->tarefausuarios) && $user_triade): ?>
    <div class="x_panel">
        <div class="x_title">
            <h2 class="green"><i class="fa fa-file"></i> Responsáveis por essa Tarefa </h2>
            <ul class="nav navbar-right panel_toolbox">
                <?php if ($tarefa->permite_editar) { ?>
                    <li><?= $this->Html->link(__('  Cadastrar Outro'), ['controller' => 'Tarefausuarios', 'action' => 'add', $tarefa->id], ['class' => "btn btn-dark fa fa-file"]) ?>  </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                <?php } ?>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th scope="col"  class="column-title">Nome do Responsável</th>
                            <th scope="col"  class="column-title">Dt Concluída</th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $cor = 'even';
                        foreach ($tarefa->tarefausuarios as $tarefausuarios):
                            ?>
                            <tr class="<?= $cor ?> pointer">
                                <td><?= h($tarefausuarios->user->nome) ?></td>
                                <td><?= $tarefausuarios->dt_concluida ? $tarefausuarios->dt_concluida : 'NÃO CONCLUÍDA' ?></td>
                                <?php if ($tarefa->permite_deletar || $tarefa->user_id == $tarefausuarios->user->id) { ?>
                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefausuarios', 'action' => 'delete', $tarefausuarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefausuarios->id)]) ?>
                                        </div>
                                    </td>
                                <?php } ?>
                            </tr>
                            <?php
                            $cor = $cor == 'even' ? 'odd' : 'even';
                        endforeach;
                        ?>
                    </tbody>                       
                </table>
            </div>
        </div>
    </div>
<?php endif; ?>


<!-- modal CONCLUIR -->
<div id="TaskModalConcluir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Concluir Tarefa" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Conclusão da Tarefa</h4>
            </div>
            <div class="modal-body">
                <form id="formConcluir" class="form-horizontal calender formConcluir" action="/tarefas/concluir" data-form="formConcluir" role="form" enctype="multipart/form-data" method="POST">
                    <div id="principal">
                        <div class="form-group">          
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="anotacao_interna">Anotações internas da Tríade</label>
                            <div class="col-md-10 col-sm-9 col-xs-8" style="padding-top: 5px;">
                                <?= $this->Form->input('anotacao_interna', ['type' => 'textarea', 'value' => $tarefa->anotacao_interna, "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group notificacao_cliente">          
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="anotacao_cliente"  data-placement="top" data-toggle="tooltip" data-original-title="O cliente receberá essas anotações por email">
                                Anotações para o Cliente</label>
                            <div class="col-md-10 col-sm-9 col-xs-8" style="padding-top: 5px;">
                                <?= $this->Form->input('anotacao_cliente', ['type' => 'textarea', 'value' => $tarefa->anotacao_cliente, "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group dt_vencimento">
                            <label class="control-label label-input col-md-2 col-sm-3 col-xs-4" style="padding-top: 10px;" for="dt_vencimento"  data-placement="top" data-toggle="tooltip" data-original-title="* o cliente será notificado por email no ato da conclusão dessa tarefa e também receberá lembrete no dia da data de vencimento">
                                Data de Vencimento</label>
                            <div class="col-md-4 col-sm-3 col-xs-8" style="padding-top: 10px;">
                                <?= $this->Form->input('dt_vencimento', ["class" => "form-control col-md-7 col-xs-12", 'value' => $tarefa->dt_vencimento, 'label' => false, 'empty' => true, 'data-inputmask' => "'mask': '99/99/9999'"]); ?>
                            </div> 
                        </div> 
                        <div class="form-group dt_vencimento" >
                            <label class="control-label col-md-10 col-sm-10 col-xs-11" for="has_movimento">
                                Houve movimentação esse mês?
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-1">
                                <?= $this->Form->checkbox('has_movimento', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false, 'checked' => 'checked']); ?>
                            </div> 
                        </div>  
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"  data-dismiss="modal" id="btnConfirmaConclusao">Confirmar conclusão</button>
            </div>
        </div>
    </div>
</div>
<!--  /modal CONCLUIR -->

<!-- modal Reabertura -->
<div id="TaskModalReabrir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Reabrir essa Tarefa" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Reabrir essa Tarefa</h4>
            </div>
            <div class="modal-body">
                <form id="formReabrir" class="form-horizontal calender formReabrir" action="/tarefas/reabrir" data-form="formReabrir" role="form" enctype="multipart/form-data" method="POST">
                    <div id="principal">                        
                        <div class="form-group">          
                            <label class="control-label col-md-12 col-sm-12 col-xs-12" style="padding-top: 10px;" for="justificativa_refazer">Escreva em detalhes por qual motivo você deseja reabrir essa Tarefa</label>
                            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top: 5px;">
                                <?= $this->Form->input('justificativa_refazer', ['type' => 'textarea', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-10 col-sm-10 col-xs-11" for="notificacao_refazer">
                                O responsável deverá ser notificado para refazer essa Tarefa?
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-1">
                                <?= $this->Form->checkbox('notificacao_refazer', ['hiddenField' => false, "class" => "form-control flat col-md-7 col-xs-12", 'label' => false, 'checked' => 'checked']); ?>
                            </div> 
                        </div>   

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                <?= $this->Form->input('tarefa_id', [ "type" => "hidden", "value" => $tarefa->id, 'label' => false]); ?>
                <button type="button" class="btn btn-primary"  data-dismiss="modal" id="btnConfirmaReabertura">Confirmar Reabertura da Tarefa</button>
            </div>
        </div>
    </div>
</div>
<!--  /modal Reabertura -->


<?= $this->Html->script('/vendors/pnotify/dist/pnotify.js'); ?>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js'); ?>
<script>
    $(document).ready(function () {
        $(":input").inputmask();
        $("#btnConfirmaReabertura").click(function () {
            reabrirTarefa();
        });
        $("#btnConfirmaConclusao").click(function () {
            concluirTarefa();
        });
    });
    function reabrirTarefa() {
        $.ajax({
            url: $('#urlroot').val() + 'tarefas/reabrir/' + $('#tarefa-id').val(),
            data: $("#formReabrir").serialize(),
            type: 'PATCH',
            dataType: 'json',
            success: function (data) {
                if (data.retorno) {
                    $("#info_dt_reaberta").html(data.retorno);
                    $("#info_justificativa_refazer").html($("#justificativa-refazer").val());
                    $("#div_justificativa_refazer").show();
                    $(".btnReabrir").hide();


                    new PNotify({text: 'Tarefa reaberta na agenda', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possivel reabrir essa tarefa', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });

    }
    function concluirTarefa() {

        $.ajax({
            url: $('#urlroot').val() + 'tarefas/concluir/' + $('#tarefa-id').val(),
            data: $("#formConcluir").serialize(),
            dataType: 'json',
            type: 'PATCH',
            success: function (data) {
                if (data.retorno) {
                    $("#info_dt_concluida").html(data.retorno);
                    $(".btnConcluir").hide();

                    new PNotify({text: 'Tarefa concluída!!', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Erro ao concluir essa tarefa. Por favor, tente novamente.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                new PNotify({text: 'Erro ao concluir essa tarefa. Por favor, tente novamente.', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
            }
        });
    }

</script>



