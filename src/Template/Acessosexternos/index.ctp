<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                 <div style="float:left" class="col-md-10 col-sm-10 col-xs-12">
                    <div class="col-md-6 col-sm-6 col-xs-12"><h2>Senhas de Acesso para Sistemas Externos</h2></div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">
                        <form action="/acessosexternos/index" method="POST" >
                            <div class="input-group">
                                <input id="termo-pesquisa" name="termo-pesquisa" class="form-control" placeholder="digite o nome da empresa ou o sistema" type="text">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Pesquisar</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <?php if ($admin) { ?>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('Empresas.razao', 'Empresa') ?></th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('Sistemasexternos.descricao', 'Acesso ao Sistema') ?></th>                                
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('exibecliente', 'Exibe para a Empresa?') ?></th>
                                <?php } else { ?>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('Sistemasexternos.descricao', 'Acesso ao Sistema') ?></th>                                
                                <?php } ?>

                                <th class="bulk-actions" colspan="12">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($acessosexternos as $acessosexterno) {
                                ?>                                
                                <tr class="<?= $cor ?> pointer">

                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= $acessosexterno->id ?>">
                                    </td>
                                    <?php if ($admin) { ?>
                                        <td><?= $acessosexterno->has('empresa') ? $this->Html->link($acessosexterno->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $acessosexterno->empresa->id]) : '' ?></td>
                                        <td><?= $acessosexterno->has('sistemasexterno') ? $this->Html->link($acessosexterno->sistemasexterno->descricao, ['controller' => 'Sistemasexternos', 'action' => 'view', $acessosexterno->sistemasexterno->id]) : '' ?></td>
                                        <td><?= $acessosexterno->exibecliente ? 'SIM' : 'NÃO'; ?></td>
                                    <?php } else { ?>
                                        <td><?= $acessosexterno->sistemasexterno->descricao; ?></td>
                                    <?php } ?>

                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $acessosexterno->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?php if ($admin) { ?>
                                                <?= $this->Html->link(__('Editar'), ['action' => 'edit', $acessosexterno->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $acessosexterno->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $acessosexterno->id)]) ?>
                                            <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
