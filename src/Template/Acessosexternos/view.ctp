<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Senha de Acesso da Empresa <?= $acessosexterno->empresa->razao ?> ao Sistema <?= $acessosexterno->sistemasexterno->descricao; ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <?php if ($admin) { ?>
                            <p class="title"><?= __('Empresa') ?></p>
                            <p><?= $acessosexterno->has('empresa') ? $this->Html->link($acessosexterno->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $acessosexterno->empresa->id]) : '' ?></p>

                            <p class="title"><?= __('Sistema Externo') ?></p>
                            <p><?= $acessosexterno->has('sistemasexterno') ? $this->Html->link($acessosexterno->sistemasexterno->descricao, ['controller' => 'Sistemasexternos', 'action' => 'view', $acessosexterno->sistemasexterno->id]) : '' ?></p>

                        <?php } else { ?>
                            <p class="title"><?= __('Sistema Externo') ?></p>
                            <p><?= $acessosexterno->sistemasexterno->descricao; ?></p>

                        <?php } ?>

                        <?php if ($acessosexterno->cpf) { ?>
                            <p class="title"><?= __('Cpf') ?></p>
                            <p><?= h($acessosexterno->cpf) ?></p>
                        <?php } ?>

                        <?php if ($acessosexterno->cnpj) { ?>
                            <p class="title"><?= __('Cnpj') ?></p>
                            <p><?= h($acessosexterno->cnpj) ?></p>
                        <?php } ?>

                        <?php if ($acessosexterno->insc_estadual) { ?>
                            <p class="title"><?= __('Insc Estadual') ?></p>
                            <p><?= h($acessosexterno->insc_estadual) ?></p>
                        <?php } ?>

                        <?php if ($acessosexterno->insc_municipal) { ?>
                            <p class="title"><?= __('Insc Municipal') ?></p>
                            <p><?= h($acessosexterno->insc_municipal) ?></p>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <?php if ($acessosexterno->cod_acesso) { ?>
                            <p class="title"><?= __('Cod Acesso') ?></p>
                            <p><?= h($acessosexterno->cod_acesso) ?></p>
                        <?php } ?>

                        <?php if ($acessosexterno->usuario) { ?>
                            <p class="title"><?= __('Usuario') ?></p>
                            <p><?= h($acessosexterno->usuario) ?></p>
                        <?php } ?>

                        <?php if ($acessosexterno->senha) { ?>
                            <p class="title"><?= __('Senha') ?></p>
                            <p><?= h($acessosexterno->senha) ?></p>
                        <?php } ?>

                        <?php if ($acessosexterno->frasesecreta) { ?>
                            <p class="title"><?= __('Frasesecreta') ?></p>
                            <p><?= h($acessosexterno->frasesecreta) ?></p>
                        <?php } ?>

                        <?php if ($acessosexterno->pin) { ?>
                            <p class="title"><?= __('PIN') ?></p>
                            <p><?= h($acessosexterno->pin) ?></p>
                        <?php } ?>

                        <?php if ($acessosexterno->puk) { ?>
                            <p class="title"><?= __('PUK') ?></p>
                            <p><?= h($acessosexterno->puk) ?></p>
                        <?php } ?>

                        <?php if ($acessosexterno->protocolo) { ?>
                            <p class="title"><?= __('Protocolo') ?></p>
                            <p><?= h($acessosexterno->protocolo) ?></p>
                        <?php } ?>

                        <?php if ($acessosexterno->link) { ?>
                            <p class="title"><?= __('Link') ?></p>
                            <p><?= h($acessosexterno->link) ?></p>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <?php if ($acessosexterno->observacao) { ?>
                            <p class="title"><?= __('Observacao') ?></p>
                            <p><?= h($acessosexterno->observacao) ?></p>
                        <?php } ?>


                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= h($acessosexterno->dt_cadastro) ?></p>


                        <?php if ($acessosexterno->last_update) { ?>
                            <p class="title"><?= __('Ultima Atualização') ?></p>
                            <p><?= h($acessosexterno->last_update) ?></p>
                        <?php } ?>


                        <?php if ($admin) { ?>
                            <p class="title"><?= __('Exibe senha para a Empresa?') ?></p>
                            <p><?= $acessosexterno->exibecliente ? __('SIM') : __('NÃO'); ?></p>

                            <p class="title"><?= __('Status') ?></p>
                            <p><?= $acessosexterno->status ? __('Ativo') : __('Desativado'); ?></p>
                        <?php } ?>


                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?php if ($admin) { ?>
                            <?= $this->Html->link("Ir para Dados da Empresa", ['controller' => 'Empresas', 'action' => 'edit', $acessosexterno->empresa_id], ['class' => "btn btn-default"]) ?>
                            <?= $this->Html->link("Ir para a lista de todas senhas", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                        <?php } else { ?>
                            <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


