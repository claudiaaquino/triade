<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($telaquestionario->pergunta) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Tela') ?></p>
                        <p><?= $telaquestionario->has('tela') ? $this->Html->link($telaquestionario->tela->descricao, ['controller' => 'Telas', 'action' => 'view', $telaquestionario->tela->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Ação') ?></p>
                        <p><?= $telaquestionario->has('tipoaction') ? $this->Html->link($telaquestionario->tipoaction->descricao, ['controller' => 'Tipoactions', 'action' => 'view', $telaquestionario->tipoaction->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Tipo de Serviço (se houver)') ?></p>
                        <p><?= $telaquestionario->has('tiposervico') ? $this->Html->link($telaquestionario->tiposervico->descricao, ['controller' => 'Tiposervicos', 'action' => 'view', $telaquestionario->tiposervico->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Pergunta') ?></p>
                        <p>  <?= $telaquestionario->pergunta ? $this->Text->autoParagraph(h($telaquestionario->pergunta)) : 'Não Informado'; ?></p>
                        
                        <p class="title"><?= __('Resposta') ?></p>
                        <p>  <?= $telaquestionario->resposta ? $this->Text->autoParagraph(h($telaquestionario->resposta)) : 'Não Informado'; ?></p>
                        
                        <p class="title"><?= __('Exige Confirmacao') ?></p>
                        <p><?= $telaquestionario->exigeconfirmacao ? __('SIM') : __('NÃO'); ?></p>
                        
                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $telaquestionario->dt_cadastro ? $telaquestionario->dt_cadastro : 'Não Informado'; ?></p>
                        

                        <p class="title"><?= __('Last Update') ?></p>
                        <p><?= $telaquestionario->last_update ? $telaquestionario->last_update : 'Não Informado'; ?></p>


                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


