<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Insira as informações para <?= __('editar Questionário') ?>. <small>* obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($telaquestionario, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>



                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tela_id">Qual tela? (se houver) <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tela_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $telas, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipoaction_id">Qual ação? (se houver) <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tipoaction_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tipoactions, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tiposervico_id">Qual tipo de serviço? (se houver) <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tiposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tiposervicos, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pergunta">Perqunta que deseja fazer <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('pergunta', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="resposta">Resposta (se tiver necessidade) <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('resposta', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="exigeconfirmacao">Esse questionário exige confirmação? <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('exigeconfirmacao', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>         

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                        <?= $this->Html->link(__('Voltar'), ['action' => 'index'], ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>