<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i>Responsável do Setor <?= $usersareaservico->areaservico->descricao . ': ' . $usersareaservico->user->nome ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Setor da Empresa') ?></p>
                        <p><?= $usersareaservico->has('areaservico') ? $this->Html->link($usersareaservico->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $usersareaservico->areaservico->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Usuário Responsável') ?></p>
                        <p><?= $usersareaservico->has('user') ? $this->Html->link($usersareaservico->user->nome, ['controller' => 'Users', 'action' => 'view', $usersareaservico->user->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $usersareaservico->dt_cadastro ? $usersareaservico->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ultima Atualização') ?></p>
                        <p><?= $usersareaservico->last_update ? $usersareaservico->last_update : 'Não Informado'; ?></p>


                        <p class="title green"><?= $usersareaservico->admin_setor ? __('Esse usuário é o administrador desse setor, no sistema') : ''; ?></p>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


