<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?php if ($admin) { ?>                                                
            <div class="x_panel">         
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Informações da Função </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><li><?= $this->Html->link(__('  Editar Informações'), ['action' => 'edit', $funcionario->id], ['class' => "btn btn-dark fa fa-pencil-square-o"]) ?></li> </li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="display: none;">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="project_detail">
                            <p class="title"><?= __('Função') ?></p>
                            <p><?= $funcionario->has('cargo') ? $funcionario->cargo->descricao : 'Não informado'; ?></p>

                            <?php if ($funcionario->numreg_livro) { ?>
                                <p class="title"><?= __('Número de Registro no Livro') ?></p>
                                <p><?= $funcionario->numreg_livro ? $funcionario->numreg_livro : 'Não Informado'; ?></p>
                            <?php } ?>

                            <p class="title">Salário base</p>
                            <p><?= $funcionario->salario ? 'R$ ' . $funcionario->salario : 'Não Informado'; ?></p>

                            <p class="title">Possui Auxílio Transporte?</p>
                            <p><?= $funcionario->flag_aux_transporte ? 'SIM' : 'NÃO'; ?></p>

                            <p class="title">Valor do Auxílio Alimentação diário</p>
                            <p><?= $funcionario->aux_alimentacao ? 'R$ ' . $funcionario->aux_alimentacao : 'Não Informado'; ?></p>

                            <?php /* ?>
                              <p class="title">Possui Auxílio Alimentação?</p>
                              <p><?= $funcionario->flag_aux_alimentacao ? 'SIM' : 'NÃO'; ?></p>

                              <p class="title">Valor do Auxílio Transporte diário</p>
                              <p><?= $funcionario->aux_transporte ? 'R$ ' . $funcionario->aux_transporte : 'Não Informado'; ?></p>

                              <?php */ ?>

                            <?php if ($funcionario->flag_deficiente) { ?>
                                <p class="title">O funcionário possui deficiência:</p>
                                <p><?= $funcionario->desc_deficiencia ? $funcionario->desc_deficiencia : 'Não Informado'; ?></p>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="project_detail">
                            <p class="title"><?= __('Dt. do Contrato de Trabalho') ?></p>
                            <p><?= $funcionario->dt_contrato_efetivado ? $funcionario->dt_contrato_efetivado : 'Não Informado'; ?></p>

                            <?php if ($funcionario->dt_demissao) { ?>
                                <p class="title"><?= __('Dt Demissão') ?></p>
                                <p><?= $funcionario->dt_demissao ? $funcionario->dt_demissao : 'Não Informado'; ?></p>

                                <p class="title"><?= __('Motivo da Demissão') ?></p>
                                <p><?= $funcionario->demissao_motivo ? $funcionario->demissao_motivo : 'Não Informado'; ?></p>
                            <?php } ?>

                            <p class="title"><?= __('Dt. Cadastro no sistema') ?></p>
                            <p><?= $funcionario->dt_cadastro ? $funcionario->dt_cadastro : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Ult. Atualização') ?></p>
                            <p><?= $funcionario->last_update ? $funcionario->last_update : 'Registro nunca foi modificado'; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">         
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Informações Pessoais: <?= h($funcionario->nome) ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"   style="display: none;" >
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <h4>Informações Principais</h4>
                        <div class="ln_solid"></div>
                        <div class="project_detail">

                            <p class="title"><?= __('Nome') ?></p>
                            <p><?= $funcionario->nome ? $funcionario->nome : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Dt. Nascimento') ?></p>
                            <p><?= $funcionario->dt_nascimento ? $funcionario->dt_nascimento : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Cpf') ?></p>
                            <p><?= $funcionario->cpf ? $funcionario->cpf : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Email') ?></p>
                            <p><?= $funcionario->email ? $funcionario->email : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Estado Civil') ?></p>
                            <p><?= $funcionario->estadocivil ? $funcionario->estadocivil->descricao : 'Não Informado' ?></p>

                            <?php if ($funcionario->has('comunhaoregime')) { ?>
                                <p class="title"><?= __('Regime de Comunhão') ?></p>
                                <p><?= $funcionario->comunhaoregime->descricao ?></p>
                            <?php } ?>

                            <p class="title"><?= __('Sexo') ?></p>
                            <p><?= $funcionario->has('sexo') ? $this->Html->link($funcionario->sexo->descricao, ['controller' => 'Sexos', 'action' => 'view', $funcionario->sexo->id]) : 'Não Informado' ?></p>

                            <p class="title"><?= __('Raça') ?></p>
                            <p><?= $funcionario->raca ? $funcionario->raca : 'Não Informado'; ?></p>


                            <p class="title"><?= __('Nome do Pai') ?></p>
                            <p><?= $funcionario->pai_nome ? $funcionario->pai_nome : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Nome da Mãe') ?></p>
                            <p><?= $funcionario->mae_nome ? $funcionario->mae_nome : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Cargo') ?></p>
                            <p><?= $funcionario->has('cargo') ? $this->Html->link($funcionario->cargo->descricao, ['controller' => 'Cargos', 'action' => 'view', $funcionario->cargo->id]) : 'Não Informado' ?></p>

                        </div>

                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <h4>Endereço e Contato</h4>
                        <div class="ln_solid"></div>
                        <div class="project_detail">
                            <p class="title"><?= __('Endereco') ?></p>
                            <p><?= $funcionario->endereco ? $funcionario->endereco : 'Não Informado'; ?></p>

                            <p class="title"><?= __('End Numero') ?></p>
                            <p><?= $funcionario->end_numero ? $funcionario->end_numero : 'Não Informado'; ?></p>

                            <p class="title"><?= __('End Complemento') ?></p>
                            <p><?= $funcionario->end_complemento ? $funcionario->end_complemento : 'Não Informado'; ?></p>

                            <p class="title"><?= __('End Bairro') ?></p>
                            <p><?= $funcionario->end_bairro ? $funcionario->end_bairro : 'Não Informado'; ?></p>

                            <p class="title"><?= __('End Cep') ?></p>
                            <p><?= $funcionario->end_cep ? $funcionario->end_cep : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Estado') ?></p>
                            <p><?= $funcionario->has('estado') ? $this->Html->link($funcionario->estado->estado_sigla, ['controller' => 'Estados', 'action' => 'view', $funcionario->estado->id]) : 'Não Informado' ?></p>

                            <p class="title"><?= __('Cidade') ?></p>
                            <p><?= $funcionario->has('cidade') ? $this->Html->link($funcionario->cidade->cidade_nome, ['controller' => 'Cidades', 'action' => 'view', $funcionario->cidade->id]) : 'Não Informado' ?></p>

                            <p class="title"><?= __('Telefone Residencial') ?></p>
                            <p><?= $funcionario->telefone_residencial ? $funcionario->telefone_residencial : 'Não Informado'; ?></p>

                            <p class="title"><?= __('Telefone Celular') ?></p>
                            <p><?= $funcionario->telefone_celular ? $funcionario->telefone_celular : 'Não Informado'; ?></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <h4>Documentos Específicos</h4>
                        <div class="ln_solid"></div>
                        <div class="project_detail">

                            <?php if ($funcionario->pis) { ?>
                                <p class="title"><?= __('Número do PIS') ?></p>
                                <p><?= $funcionario->pis ? $funcionario->pis : 'Não Informado'; ?></p>
                                <div class="ln_solid"></div>
                            <?php } ?>

                            <?php if ($funcionario->ctps) { ?>
                                <p class="title"><?= __('CTPS/Série') ?></p>
                                <p><?= $funcionario->ctps ? $funcionario->ctps . '/' . $funcionario->ctps_serie : 'Não Informado'; ?></p>
                                <div class="ln_solid"></div>
                            <?php } ?>

                            <?php if ($funcionario->cnh) { ?>
                                <p class="title"><?= __('CNH') ?></p>
                                <p><?= $funcionario->cnh; ?></p>

                                <p class="title"><?= __('CNH Dt. Habilitacao') ?></p>
                                <p><?= $funcionario->cnh_dt_habilitacao ? $funcionario->cnh_dt_habilitacao : 'Não Informado'; ?></p>

                                <p class="title"><?= __('CNH Dt. Vencimento') ?></p>
                                <p><?= $funcionario->cnh_dt_vencimento ? $funcionario->cnh_dt_vencimento : 'Não Informado'; ?></p>
                                <div class="ln_solid"></div>
                            <?php } ?>


                            <?php if ($funcionario->rg) { ?>
                                <p class="title"><?= __('RG') ?></p>
                                <p><?= $funcionario->rg; ?></p>

                                <p class="title"><?= __('RG Órgão/Estado') ?></p>
                                <p><?= $funcionario->rg_estado || $funcionario->rg_expedidor ? $funcionario->rg_estado . ' /' . $funcionario->rg_expedidor : 'Não Informado'; ?></p>

                                <p class="title"><?= __('Rg Dt Expedicao') ?></p>
                                <p><?= $funcionario->rg_dt_expedicao ? $funcionario->rg_dt_expedicao : 'Não Informado'; ?></p>
                                <div class="ln_solid"></div>
                            <?php } ?>


                            <?php if ($funcionario->militar_numero) { ?>
                                <p class="title"><?= __('Certificado Reservista/Série') ?></p>
                                <p><?= $funcionario->militar_numero . '/' . $funcionario->militar_serie; ?></p>

                                <p class="title"><?= __('Órgão Expedidor') ?></p>
                                <p><?= $funcionario->militar_expedidor ? $funcionario->militar_expedidor : 'Não Informado'; ?></p>

                                <div class="ln_solid"></div>
                            <?php } ?>


                            <?php if ($funcionario->eleitor_numero) { ?>
                                <p class="title"><?= __('Título de Eleitor') ?></p>
                                <p><?= $funcionario->eleitor_numero ? $funcionario->eleitor_numero . '/' . $funcionario->eleitor_zona . '/' . $funcionario->eleitor_secao : 'Não Informado'; ?></p>

                                <p class="title"><?= __('Zona/Seção') ?></p>
                                <p><?= $funcionario->eleitor_zona . '/' . $funcionario->eleitor_secao; ?></p>

                                <p class="title"><?= __('Dt. Emissão do Título') ?></p>
                                <p><?= $funcionario->eleitor_dt_emissao ? $funcionario->eleitor_dt_emissao : 'Não Informado'; ?></p>

                                <div class="ln_solid"></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (!empty($funcionario->documentos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Documentos Anexados de <?= h($funcionario->nome) ?> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"  style="display: none;" >
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Tipo de Documento') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Envio') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($funcionario->documentos as $documentos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= $this->Html->link($documentos->tipodocumento->descricao, "/docs/" . $documentos->file, ['target' => '_blank']) ?></td>
                                        <td><?= h($documentos->dt_enviado) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Download'), "/docs/" . $documentos->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                                <?php if ($admin) { ?>
                                                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Documentos', 'action' => 'delete', $documentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $documentos->id)]) ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>


        <?php if (!empty($funcionario->funcionariodependentes)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Dependentes </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"  style="display: none;">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Parentesco') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt. Nascimento') ?></th>
                                    <th scope="col"  class="column-title"><?= __('CPF') ?></th>

                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($funcionario->funcionariodependentes as $funcionariodependentes):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($funcionariodependentes->parentesco->descricao) ?></td>
                                        <td><?= h($funcionariodependentes->nome) ?></td>
                                        <td><?= h($funcionariodependentes->dt_nascimento) ?></td>
                                        <td><?= h($funcionariodependentes->cpf) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Funcionariodependentes', 'action' => 'view', $funcionariodependentes->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Funcionariodependentes', 'action' => 'edit', $funcionariodependentes->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Funcionariodependentes', 'action' => 'delete', $funcionariodependentes->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $funcionariodependentes->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($funcionario->funcionarioescolaridades)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Formação Acadêmica/ Cursos </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"  style="display: none;">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Nível do Curso') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Curso') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Instituição') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt. Início') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt. Formatura') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($funcionario->funcionarioescolaridades as $funcionarioescolaridades):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($funcionarioescolaridades->nivelescolaridade->descricao) ?></td>
                                        <td><?= h($funcionarioescolaridades->curso->nome) ?></td>
                                        <td><?= h($funcionarioescolaridades->escola->nome) ?></td>
                                        <td><?= h($funcionarioescolaridades->inicio) ?></td>
                                        <td><?= h($funcionarioescolaridades->formatura) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Funcionarioescolaridades', 'action' => 'view', $funcionarioescolaridades->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Funcionarioescolaridades', 'action' => 'edit', $funcionarioescolaridades->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Funcionarioescolaridades', 'action' => 'delete', $funcionarioescolaridades->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $funcionarioescolaridades->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


