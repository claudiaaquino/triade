<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Alteração de Dados de Funcionário para <?= $funcionario->empresa->razao; ?><small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($funcionario, ["class" => "form-horizontal form-label-left", 'enctype' => "multipart/form-data"]) ?>
                <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                <?= $this->Flash->render() ?>
                <div id="wizard_verticle" class="form_wizard wizard_verticle col-md-12 col-sm-12 col-xs-12">
                    <ul class="wizard_steps" >
                        <li>
                            <a href="#substep-1">
                                <span class="step_no">1</span>
                            </a>
                        </li>
                        <li>
                            <a href="#substep-2">
                                <span class="step_no">2</span>
                            </a>
                        </li>
                        <li>
                            <a href="#substep-3">
                                <span class="step_no">3</span>
                            </a>
                        </li>
                        <li>
                            <a href="#substep-4">
                                <span class="step_no">4</span>
                            </a>
                        </li>
                        <li>
                            <a href="#substep-5">
                                <span class="step_no">5</span>
                            </a>
                        </li>
                        <li>
                            <a href="#substep-6">
                                <span class="step_no">6</span>
                            </a>
                        </li>
                    </ul>
                    <div id="substep-1">
                        <span class="section">Informações Principais</span>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nome">Nome <span class="required">*</span>
                            </label>
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <?= $this->Form->input('nome', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="dt_nascimento">Dt. Nascimento <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <?= $this->Form->input('dt_nascimento', ['type' => 'text', "data-inputmask" => "'mask': '99/99/99'", "class" => "date-picker form-control col-md-2 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="cpf">CPF <span class="required">*</span>
                            </label>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <?= $this->Form->input('cpf', ['type' => 'text', "data-inputmask" => "'mask': '999.999.999-99'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2  col-xs-12" for="sexo_id">Sexo
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <?= $this->Form->input('sexo_id', ['options' => $sexos, 'empty' => true, "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>           
                        </div>
                        <div class="form-group">

                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="estadocivil_id">Estado Civil 
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <?= $this->Form->input('estadocivil_id', ['options' => $estadocivils, 'empty' => true, "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12 casado" for="comunhaoregime_id">Regime
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 casado">
                                <?= $this->Form->input('comunhaoregime_id', ['options' => $comunhaoregimes, 'empty' => true, "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="pai_nome">Nome do pai
                            </label>
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <?= $this->Form->input('pai_nome', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="mae_nome">Nome da mãe
                            </label>
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <?= $this->Form->input('mae_nome', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="mae_nome">Raça
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <?= $this->Form->input('raca', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                    </div>
                    <div id="substep-2">
                        <span class="section">Documentações Específicas do Funcionário</span>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ctps">Nº da Carteira de Trabalho
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('ctps', ["class" => "form-control col-md-3 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ctps_serie">Série
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('ctps_serie', ["class" => "form-control col-md-2 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="ctps">Nº do PIS
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('pis', ["class" => "form-control col-md-3 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cnh">CNH
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('cnh', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cnh_dt_habilitacao">Habilitação
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('cnh_dt_habilitacao', ['type' => 'text', "data-inputmask" => "'mask': '99/99/99'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cnh_dt_vencimento">Vencimento
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('cnh_dt_vencimento', ['type' => 'text', "class" => "form-control  col-md-7 col-xs-12", 'label' => false, "data-inputmask" => "'mask': '99/99/99'"]); ?>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="rg">RG
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('rg', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="rg_estado">Estado
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('rg_estado', ['options' => $estados, "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="rg_expedidor">Expedidor
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('rg_expedidor', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="rg_dt_expedicao">Dt. Expedição
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('rg_dt_expedicao', ['type' => 'text', "data-inputmask" => "'mask': '99/99/99'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>

                    </div>
                    <div id="substep-3">

                        <span class="section">Documentações Específicas do Funcionário</span>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="militar_numero">Certificado de Reservista
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('militar_numero', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="militar_expedidor">Órgão Expedidor
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('militar_expedidor', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="militar_serie">Série
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('militar_serie', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="eleitor_numero">Título de Eleitor
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('eleitor_numero', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="eleitor_zona">Zona
                            </label>
                            <div class="col-md-1 col-sm-1 col-xs-12">
                                <?= $this->Form->input('eleitor_zona', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="eleitor_secao">Seção
                            </label>
                            <div class="col-md-1 col-sm-1 col-xs-12">
                                <?= $this->Form->input('eleitor_secao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="eleitor_dt_emissao">Emissão
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('eleitor_dt_emissao', ['type' => 'text', "data-inputmask" => "'mask': '99/99/99'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        </br></br></br></br></br>
                    </div>
                    <div id="substep-4">
                        <!--<h2 class="StepTitle">Endereço e Contato</h2>-->
                        <span class="section">Endereço e Contato</span>

                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="endereco">Endereço  <span class="required">*</span>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <?= $this->Form->input('endereco', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="end_numero">Número  <span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <?= $this->Form->input('end_numero', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="end_complemento">Complemento 
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('end_complemento', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="end_bairro">Bairro  <span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <?= $this->Form->input('end_bairro', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="end_cep">CEP  <span class="required">*</span>
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('end_cep', ["data-inputmask" => "'mask': '99999-999'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="estado_id">Estado
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <?= $this->Form->input('estado_id', ['options' => $estados, "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => 'selecione o estado']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="cidade_id">Cidade
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <?= $this->Form->input('cidade_id', ["class" => "form-control col-md-12 col-sm-12  col-xs-12", 'label' => false, 'sel' => $funcionario->cidade_id]); ?>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email">Email 
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <?= $this->Form->input('email', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telefone_residencial">Tel. Residencial 
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <?= $this->Form->input('telefone_residencial', [ "data-inputmask" => "'mask': '(99) 9999-9999'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="telefone_celular">Celular 
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <?= $this->Form->input('telefone_celular', ["data-inputmask" => "'mask': '(99) 9999[9]-9999'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>
                    </div>
                    <div id="substep-5">
                        <span class="section">Informações da Função</span>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cargo_id">Cargo  <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('cargo_id', ['options' => $cargos, 'empty' => true, "class" => "form-control col-md-7 col-xs-12  select2 ", 'label' => false, "style" => "width: 100%"]); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="salario">Salário base <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('salario', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div>                         
                            <?php /*
                              <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aux_transporte">Valor do Auxílio Transporte (diário) <span class="required">*</span>
                              </label>
                              <div class="col-md-4 col-sm-6 col-xs-12">
                              <?= $this->Form->input('aux_transporte', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                              </div>
                              </div>


                             * 
                             * 
                              <div class="form-group">
                              <label class="control-label col-md-4 col-sm-4 col-xs-10" for="flag_aux_alimentacao">Possui Auxílio Alimentação?
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-2">
                              <?= $this->Form->checkbox('flag_aux_alimentacao', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                              </div>
                              </div>

                             */ ?>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-10" for="flag_aux_transporte">Possui Auxílio Transporte? 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-2">
                                    <?= $this->Form->checkbox('flag_aux_transporte', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div>       
                            
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aux_alimentacao">Valor do Auxílio Alimentação (diário)
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('aux_alimentacao', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-10" for="flag_deficiente">Possui alguma Deficiência? 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-2">
                                    <?= $this->Form->checkbox('flag_deficiente', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div>                         

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="desc_deficiencia">Se sim, especifique qual:
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('desc_deficiencia', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div>     
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="numreg_livro">Número de Registro no Livro
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('numreg_livro', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div>     
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_contrato_efetivado">Dt. efetivação do contrato <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('dt_contrato_efetivado', ['type' => 'text', "data-inputmask" => "'mask': '99/99/99'", "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>

                                </div> 
                            </div> 
                        </div>
                    </div> 
                    <div id="substep-6">
                        <span class="section">Anexar Documentos referentes ao Funcionário</span>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group document-details">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tipodocumento_id">Documento 
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <?= $this->Form->input('tipodocumento_id', ['options' => $tipodocumentos, "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%"]); ?>
                                </div>
                            </div>
                            <div class="form-group document-details">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="descricao">Observações
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <?= $this->Form->input('descricao-anexo', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                                </div>
                            </div>  
                            <div class="clearfix"></div>  

                            <div id="fileField" class="col-md-12 col-sm-12 col-xs-12">
                                <!--                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                                </div>-->
                                <div id="pathFile"  class="col-md-9 col-sm-9 col-xs-7">
                                    <?= $this->Form->file('files[]', ['label' => false, 'key' => 0, 'class' => 'file']) ?>      
                                </div>
                            </div>
                            <div class="clearfix"></div>  
                            <div class="document-details col-md-12 col-sm-12 col-xs-12">
                                <div class="ln_solid"></div>
                                <div class="col-md-2 col-sm-2 col-xs-4">
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-4 btn-addFile">
                                    <button class="addFile btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Adicionar Arquivo à Lista</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 mail_list_column">
                            <div class="form-group files">
                                <div id="queue">          
                                    <h2>Documentos Anexados</h2>
                                </div>
                            </div>  
                        </div>  
                        <div class="clearfix"></div>  
                        <br><br>
                    </div>
                </div>    
                <?= $this->Form->end() ?>
            </div>

        </div>
    </div>
</div>
<?= $this->Html->script('/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js', array('inline' => false)); ?>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js', array('inline' => false)); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/js/ajax/cidades.js'); ?>
<script>
    var keys_anexos = 0;
    $(document).ready(function () {
        $(":input").removeClass('date');
        $(":input").inputmask();
        $("select").select2({placeholder: 'selecione uma opção'});
        $("select").addClass('select2');
        $("#cargo-id").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'Digite um novo (se não estiver na lista) e aperte enter'});
        toggleEstadoCivil();
        $('#queue').hide();

        $('#wizard_verticle').smartWizard({
            transitionEffect: 'slide',
            labelNext: 'Próximo',
            labelFinish: 'Atualizar Cadastro'
        });
        $('.buttonNext').addClass('btn btn-primary');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-success');
        $('.actionBar').before('<div class="clearfix"></div>');
        $('.stepContainer').addClass('col-md-12 col-sm-12 col-xs-12');

        $('#estadocivil-id').change(function () {
            toggleEstadoCivil();
        });

        $('#salario,#aux-alimentacao').inputmask('decimal', {
            radixPoint: ",",
            groupSeparator: ".",
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
            rightAlign: false,
            onBeforeMask: function (value, opts) {
                return value;
            }});

//adicionar documentos
        $('.addFile').click(function (e) {
            e.preventDefault();
            if (!$('#tipodocumento-id').val()) {
                alert('Você deve informar qual é o documento que está inserindo');
            } else {
                addAttachment();
                $('#queue').show();
            }
        });

    });
    function toggleEstadoCivil() {
        if ($('#estadocivil-id').val() == 2) {// casado
            $('.casado').show();
        } else {
            $('.casado').hide();
        }
    }

    function addAttachment() {
        getPropertiesFile();
        additemListFiles();
        hideLatestAttachmentField();
        newAttachmentField();
    }
    function getPropertiesFile() {
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='tipo_documentos[" + keys_anexos + "]' value='" + $('#tipodocumento-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='descricoes[" + keys_anexos + "]' value='" + $('#descricao-anexo').val() + "'>");
    }
    function additemListFiles() {
        $('#queue').append('<div class="itemqueue col-md-6 col-sm-6 col-xs-12 well well-sm"  style="padding: 0;" item="' + keys_anexos + '">\
                            <div id="descFile" class="col-md-9 col-sm-9 col-xs-9">' + $('#tipodocumento-id option:selected').text() + '</div>\
                <div class="removeFile col-md-3 col-sm-3 col-xs-3">\
                    <button class="remove btn btn-sm btn-danger" type="button" onclick="removeItemListFiles(this,' + keys_anexos + ');"><i class="fa fa-trash"></i></button>\
                </div></div>');
    }
    function hideLatestAttachmentField() {
        $('.file').hide();
    }
    function newAttachmentField() {
        keys_anexos++;
        $('#pathFile').append("<input type='file' class='file' key='" + keys_anexos + "' name='files[]'>");
    }
    function removeItemListFiles(item, key) {
        $(item).parent().parent().remove();
        $(':input[key=' + key + ']').remove();
    }
</script>