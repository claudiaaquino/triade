<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<?= $this->Html->css('/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastrar Deveres das Empresas <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($tipodevere, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gruposervico_id">Setor <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('areaservico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $areaservicos, 'empty' => '  ']); ?>
                    </div> 
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gruposervico_id">Subsetor <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('gruposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $gruposervicos, 'empty'=>'   ']); ?>
                    </div> 
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">Resumo do dever <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('nome', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Descrição do dever da empresa
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                              


                <div class="form-group">
                    <label class="control-label col-md-5 col-sm-5 col-xs-12" for="setor_ids">Quais empresas devem cumprir com esse dever mensalmente?
                    </label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <?= $this->Form->input('empresa_ids', ['options' => $empresas, "class" => "form-control col-md-7 col-xs-12", "multiple" => "multiple", 'label' => false, "style" => "width: 100%"]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-5 col-sm-5 col-xs-12" for="dia_inicio_execucao">Qual o dia do mês que essa tarefa DEVERIA SER INICIADA? 
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?= $this->Form->input('dia_inicio_execucao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'min' => 1, 'max' => '31']); ?>
                    </div> 
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        Considerar somente dias úteis? <?= $this->Form->checkbox('dia_inicio_execucao_uteis', ['hiddenField' => false, "class" => "form-control flat  col-md-1 col-xs-1", 'label' => false]); ?>
                    </div> 
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-5 col-sm-5 col-xs-12" for="dia_fim_execucao">Qual o ÚLTIMO PRAZO para a finalização dessa tarefa? *
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?= $this->Form->input('dia_fim_execucao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'min' => 1, 'max' => '31']); ?>
                    </div> 
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        Considerar somente dias úteis? <?= $this->Form->checkbox('dia_fim_execucao_uteis', ['hiddenField' => false, "class" => "form-control  flat  col-md-1 col-xs-1", 'label' => false]); ?>
                    </div> 
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-5 col-sm-5 col-xs-12" for="dia_lembrete_execucao">Que dia o funcionário deverá receber um LEMBRETE DO PRAZO FINAL? 
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?= $this->Form->input('dia_lembrete_execucao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'min' => 1, 'max' => '31']); ?>
                    </div> 
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        Considerar somente dias úteis? <?= $this->Form->checkbox('dia_lembrete_execucao_uteis', ['hiddenField' => false, "class" => "form-control flat   col-md-1 col-xs-1", 'label' => false]); ?>
                    </div> 
                </div> 
                <div class="ln_solid"></div>
                <div class="form-group">
                    <label class="control-label col-md-8 col-sm-8 col-xs-12 red" for="tarefa_automatica">Deseja que o sistema gere tarefas AUTOMATICAMENTE nas agendas dos reponsáveis por elas? *
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?= $this->Form->checkbox('tarefa_automatica', ['hiddenField' => false, "class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>
                    </div> 
                </div> 
                <div class="ln_solid"></div>
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12">Com qual cor essa tarefa deverá aparecer na agenda?</label>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="input-group colorpicker">
                            <?= $this->Form->input('cor_tarefa', ["class" => "form-control", 'value' => "#e01ab5", 'label' => false]); ?>
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>
                </div>
                <div class="ln_solid"></div>
            </div>             
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-success">Cadastrar</button>
                    <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                </div>
            </div>                                          
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
</div>
<?= $this->Html->script('/js/ajax/servicos.js'); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js'); ?>
<script>
    $(document).ready(function () {
        $("select").select2({placeholder: 'selecione uma opção'});
        $("#empresa-ids").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione a(s) empresa(s)'});
        $('.colorpicker').colorpicker();


    });
</script>