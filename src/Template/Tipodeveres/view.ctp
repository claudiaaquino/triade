<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($tipodevere->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Setor</p>
                        <p><?= $tipodevere->gruposervico->areaservico ? $this->Html->link($tipodevere->gruposervico->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $tipodevere->gruposervico->areaservico->id]) : 'Não Informado' ?></p>

                        <p class="title">Subsetor</p>
                        <p><?= $tipodevere->has('gruposervico') ? $this->Html->link($tipodevere->gruposervico->descricao, ['controller' => 'Gruposervicos', 'action' => 'view', $tipodevere->gruposervico->id]) : 'Não Informado' ?></p>

                        <p class="title">Dever</p>
                        <p><?= $tipodevere->nome ? $tipodevere->nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Descrição') ?></p>
                        <p><?= $tipodevere->descricao ? $tipodevere->descricao : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Dia do Lembrete para Execução') ?></p>
                        <p><?= $tipodevere->dia_lembrete_execucao ? $this->Number->format($tipodevere->dia_lembrete_execucao) . ($tipodevere->dia_lembrete_execucao_uteis ? 'º dia útil' : '') : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Dia Inicio Execução') ?></p>
                        <p><?= $tipodevere->dia_inicio_execucao ? $this->Number->format($tipodevere->dia_inicio_execucao) . ($tipodevere->dia_inicio_execucao_uteis ? 'º dia útil' : '') : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Dia Fim Execução') ?></p>
                        <p><?= $tipodevere->dia_fim_execucao ? $this->Number->format($tipodevere->dia_fim_execucao) . ($tipodevere->dia_fim_execucao_uteis ? 'º dia útil' : '') : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Tarefa Automática?') ?></p>
                        <p><?= $tipodevere->tarefa_automatica ? __('SIM') : __('NÃO'); ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $tipodevere->last_update ? $tipodevere->last_update : 'Não houve alteração'; ?></p>


                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $tipodevere->dt_cadastro ? $tipodevere->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $tipodevere->status ? __('Ativo') : __('Desativado'); ?></p>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
    </div>
</div>





