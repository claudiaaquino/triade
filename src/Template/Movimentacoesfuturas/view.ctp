<div class="page-title">
    <div class="title_left">
        <h3><?= __('Movimentacoesfuturas') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($movimentacoesfutura->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Movimentacaobancaria') ?></p>
                                    <p><?= $movimentacoesfutura->has('movimentacaobancaria') ? $this->Html->link($movimentacoesfutura->movimentacaobancaria->id, ['controller' => 'Movimentacaobancarias', 'action' => 'view', $movimentacoesfutura->movimentacaobancaria->id]) : 'Não Informado' ?></p>

                                                                                                                    <p class="title"><?= __('Dt Programada') ?></p>
                                    <p><?= $movimentacoesfutura->dt_programada ? $movimentacoesfutura->dt_programada : 'Não Informado'; ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $movimentacoesfutura->id ? $this->Number->format($movimentacoesfutura->id) : 'Não Informado'; ?></p>

                                                    
                        <p class="title"><?= __('Valorparcela') ?></p>
                                <p><?= $movimentacoesfutura->valorparcela ? $this->Number->format($movimentacoesfutura->valorparcela) : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Last Update') ?></p>
                                <p><?= $movimentacoesfutura->last_update ? $movimentacoesfutura->last_update : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Pago') ?></p>
                                <p><?= $movimentacoesfutura->pago ? __('Ativo') : __('Desativado'); ?></p>

                                                    
                        <p class="title"><?= __('Status') ?></p>
                                <p><?= $movimentacoesfutura->status ? __('Ativo') : __('Desativado'); ?></p>

                                                                                                    <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
</div>


