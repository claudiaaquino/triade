<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i><?= __('Tipoinformativos') ?> - <?= h($tipoinformativo->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Descricao') ?></p>
                                    <p><?= $tipoinformativo->descricao ? $tipoinformativo->descricao : 'Não Informado'; ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $tipoinformativo->id ? $this->Number->format($tipoinformativo->id) : 'Não Informado'; ?></p>

                                                                                                                                                </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $tipoinformativo->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $tipoinformativo->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tipoinformativo->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
                    <?php if (!empty($tipoinformativo->assuntosinformativos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Assuntosinformativos Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Assunto Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Tipoinformativo Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Informativo') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Exibe Cliente') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Exibe Funcionario') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tipoinformativo->assuntosinformativos as $assuntosinformativos):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                                                    <td><?= h($assuntosinformativos->id) ?></td>
                                                                    <td><?= h($assuntosinformativos->assunto_id) ?></td>
                                                                    <td><?= h($assuntosinformativos->tipoinformativo_id) ?></td>
                                                                    <td><?= h($assuntosinformativos->informativo) ?></td>
                                                                    <td><?= h($assuntosinformativos->exibe_cliente) ?></td>
                                                                    <td><?= h($assuntosinformativos->exibe_funcionario) ?></td>
                                                                    <td><?= h($assuntosinformativos->dt_cadastro) ?></td>
                                                                    <td><?= h($assuntosinformativos->last_update) ?></td>
                                                                    <td><?= h($assuntosinformativos->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Assuntosinformativos', 'action' => 'view', $assuntosinformativos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Assuntosinformativos', 'action' => 'edit', $assuntosinformativos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Assuntosinformativos', 'action' => 'delete', $assuntosinformativos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $assuntosinformativos->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
            </div>
</div>


