<div class="page-title">
    <div class="title_left">
        <h3><?= __('Sistemas Externos') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($sistemasexterno->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Id') ?></p>
                        <p><?= $this->Number->format($sistemasexterno->id) ?></p>


                        <p class="title"><?= __('Descricao') ?></p>
                        <p><?= h($sistemasexterno->descricao) ?></p>



                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= h($sistemasexterno->dt_cadastro) ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= h($sistemasexterno->last_update) ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $sistemasexterno->status ? __('Ativo') : __('Desativado'); ?></p>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($sistemasexterno->acessosexternos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Senhas de Acesso Vínculadas à esse Sistema</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Empresa') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Cpf') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Cnpj') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Insc Estadual') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Insc Municipal') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Cod Acesso') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Usuario') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Senha') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Protocolo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Exibe Cliente') ?></th>
                                    <th class="bulk-actions" colspan="12">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php ?>
                                <tr>


                                </tr>

                                <?php
                                $cor = 'even';
                                foreach ($sistemasexterno->acessosexternos as $acessosexternos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($acessosexternos->id) ?>">
                                        </td>
                                        <td><?= h($acessosexternos->empresa->razao) ?></td>
                                        <td><?= h($acessosexternos->cpf) ?></td>
                                        <td><?= h($acessosexternos->cnpj) ?></td>
                                        <td><?= h($acessosexternos->insc_estadual) ?></td>
                                        <td><?= h($acessosexternos->insc_municipal) ?></td>
                                        <td><?= h($acessosexternos->cod_acesso) ?></td>
                                        <td><?= h($acessosexternos->usuario) ?></td>
                                        <td><?= h($acessosexternos->senha) ?></td>
                                        <td><?= h($acessosexternos->protocolo) ?></td>
                                        <td><?= $acessosexternos->exibecliente ? 'SIM' : 'NÃO'; ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Acessosexternos', 'action' => 'view', $acessosexternos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Acessosexternos', 'action' => 'edit', $acessosexternos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Acessosexternos', 'action' => 'delete', $acessosexternos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $acessosexternos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


