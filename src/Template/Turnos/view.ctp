<div class="page-title">
    <div class="title_left">
        <h3>Detalhes do Turno</h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($turno->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        if (!empty($turno->funcionarioescolaridades)) {
            ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Funcionários </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col" class="column-title"><?= __('ID') ?></th>
                                    <th scope="col" class="column-title"><?= __('Funcionário') ?></th>
                                    <th scope="col" class="column-title"><?= __('Nível de Escolaridade') ?></th>
                                    <th scope="col" class="column-title"><?= __('Curso') ?></th>
                                    <th scope="col" class="column-title"><?= __('Instituição') ?></th>
                                    <th scope="col" class="column-title"><?= __('Matricula') ?></th>
                                    <th scope="col" class="column-title"><?= __('Inicio') ?></th>
                                    <th scope="col" class="column-title"><?= __('Previsão de Formatura') ?></th>
                                    <th scope="col" class="column-title"><?= __('Últ. Atualização') ?></th>
                                    <th></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($turno->funcionarioescolaridades as $funcionario) {
                                    ?>

                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records">
                                        </td>
                                        <td><?= h($funcionario->id) ?></td>
                                        <td><?= $this->Html->link($funcionario->funcionario->nome, ['controller' => 'Funcionarios', 'action' => 'view', $funcionario->funcionario->id]) ?></td>
                                        <td><?= $this->Html->link($funcionario->nivelescolaridade->descricao, ['controller' => 'Nivelescolaridades', 'action' => 'view', $funcionario->nivelescolaridade->id]) ?></td>
                                        <td><?= $this->Html->link($funcionario->curso->nome, ['controller' => 'Cursos', 'action' => 'view', $funcionario->curso->id]) ?></td>
                                        <td><?= $this->Html->link($funcionario->escola->nome, ['controller' => 'Escolas', 'action' => 'view', $funcionario->escola->id]) ?></td>
                                        <td><?= h($funcionario->matricula) ?></td>
                                        <td><?= h($funcionario->inicio) ?></td>
                                        <td><?= h($funcionario->formatura) ?></td>
                                        <td><?= h($funcionario->last_update) ?></td>
                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Funcionarioescolaridades', 'action' => 'view', $funcionario->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Funcionarioescolaridades', 'action' => 'edit', $funcionario->id], ['class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Funcionarioescolaridades', 'action' => 'delete', $funcionario->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Deseja mesmo remover o registro #{0}?', $funcionario->id)]) ?>

                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                }
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>