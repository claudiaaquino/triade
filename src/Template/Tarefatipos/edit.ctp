<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Editar informações de Tipo de Tarefa <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($tarefatipo, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>
                <?= $this->Form->input('backlink', ["type" => "hidden", 'value' => $backlink, 'label' => false]); ?>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Descrição da Tarefa <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         
                <?php if ($admin) { ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="todos">Exibe para todos os usuários do sistema? 
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->checkbox('todos_sistema', ["class" => "form-control flat", 'label' => false]); ?>

                        </div> 
                    </div>                         
                <?php } ?>
                <?php if ($admin || $admin_empresa) { ?>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="todos">Esse tipo de tarefa poderá/deverá ser usado por outros usuários da sua empresa? 
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->checkbox('todos_empresa', ["class" => "form-control flat", 'label' => false]); ?>

                        </div> 
                    </div>    
                <?php } ?>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>