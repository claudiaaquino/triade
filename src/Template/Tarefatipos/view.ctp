<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($tarefatipo->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__(' Editar Tipo'), ['controller' => 'Tarefatipos', 'action' => 'edit', $tarefatipo->id], ['class' => "btn btn-dark fa fa-pencil-square-o"]) ?>  </li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Descrição') ?></p>
                        <p><?= $tarefatipo->descricao ? $tarefatipo->descricao : 'Não Informado'; ?></p>

                        <?php if ($admin) { ?>
                            <p class="title"><?= __('Exibe para todos os usuários do sistema?') ?></p>
                            <p><?= $tarefatipo->todos ? 'SIM' : 'NÃO'; ?></p>
                        <?php } ?>

                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $tarefatipo->dt_cadastro ? $tarefatipo->dt_cadastro : 'Não Informado'; ?></p>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($tarefatipo->tarefas) && $admin): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Tarefas Registradas em  <?= h($tarefatipo->descricao) ?> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Empresa') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Setor') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Serviço') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Título') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt. Inicio') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Automática') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt. Conclusão') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tarefatipo->tarefas as $tarefas):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= $tarefas->empresa ? $tarefas->empresa->razao : '---' ?></td>
                                        <td><?= $tarefas->areaservico ? $tarefas->areaservico->descricao : '---' ?></td>
                                        <td><?= $tarefas->tiposervico ? $tarefas->tiposervico->descricao : '---' ?></td>
                                        <td><?= h($tarefas->titulo) ?></td>
                                        <td><?= h($tarefas->dt_inicio) ?></td>
                                        <td><?= $tarefas->automatica ? 'SIM' : 'NÃO' ?></td>
                                        <td><?= h($tarefas->dt_concluida) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Tarefas', 'action' => 'view', $tarefas->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Tarefas', 'action' => 'edit', $tarefas->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefas', 'action' => 'delete', $tarefas->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefas->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


