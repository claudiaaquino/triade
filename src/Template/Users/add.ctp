<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Triade Consultoria</title>
        <?= $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']) ?>

        <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('custom.min.css') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>    

    </head>

    <body class="login">
        <div>
            <a class="hiddenanchor" id="signin"></a>
            <a class="hiddenanchor" id="signup"></a>

            <div class="login_wrapper">
                <div class="animate form login_form">
                    <section class="login_content">
                        <?= $this->Form->create($user); ?>
                        <div>
                            <?php echo $this->Html->image("logo_p.png", ["alt" => "Sistema Tríade de Contabilidade"]); ?>
                        </div>
                        <h1>Sistema Tríade de Contabilidade</h1>
                        <p><?= $this->Flash->render() ?></p>
                        <div class="form-group">
                            <!--<input type="text" id="nome" name="nome"  class="form-control" placeholder="Nome" required="" />-->
                                <?= $this->Form->input('nome', ["class" => "form-control", "placeholder" => "Nome", 'label' => false]) ?>
                        </div>
                        <div class="form-group">
                            <!--<input type="text" id="username" name="username" class="form-control" placeholder="Usuário" required="" />-->
                            <?= $this->Form->input('username', ["class" => "form-control", "placeholder" => "Usuário", 'label' => false]) ?>
                        </div>
                        <div class="form-group">
                            <!--<input type="email" id="email" name="email"  class="form-control" placeholder="Email" required="" />-->
                            <?= $this->Form->input('email', ["class" => "form-control", "placeholder" => "Email", 'label' => false]) ?>
                        </div>
                        <div class="form-group">
                            <!--<input type="password" id="password" name="password" class="form-control" placeholder="Senha" required="" />-->
                            <?= $this->Form->input('password', ["class" => "form-control", "placeholder" => "Senha", 'label' => false]) ?>
                        </div>
                        <div>
                            <button class="btn btn-default submit" type="submit">Cadastrar</button>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <p class="change_link">Já é cadastrado? Faça
                                <?= $this->Html->link(__('Login'), ['action' => 'login']) ?>
                            </p>

                        </div>
                        <?= $this->Form->end() ?>
                    </section>
                </div>
            </div>
        </div>
    </body>
</html>
