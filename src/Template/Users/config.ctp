<?= $this->Html->css('/vendors/pnotify/dist/pnotify.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Configurações da conta</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="form-group">
                    <h2>Desejo receber notificações no email quando:</h2><br><br>
                    <input type="checkbox" class='form-control flat col-md-12 col-xs-12' name="configs" value="notificacao_mensagem" <?= ($user->notificacao_mensagem) ? "checked='checked'" : '' ?>> Receber uma <b class='green'>nova mensagem</b> no sistema <br><br>
                    <?php if ($user_triade) { ?>
                        <input type="checkbox" class='form-control flat col-md-12 col-xs-12'  name="configs" value="notificacao_novodocumento" <?= ($user->notificacao_novodocumento) ? "checked='checked'" : '' ?>> Algum cliente enviar <b class='green'>novo(s) documento(s) no sistema</b><br><br>
                        <input type="checkbox" class='form-control flat col-md-12 col-xs-12'  name="configs" value="notificacao_tarefa" <?= ($user->notificacao_tarefa) ? "checked='checked'" : '' ?>> For atribuido para executar uma <b class='green'>nova tarefa</b><br><br>
                        <input type="checkbox"  class='form-control flat col-md-12 col-xs-12' name="configs" value="notificacao_semanal_tarefa" <?= ($user->notificacao_semanal_tarefa) ? "checked='checked'" : '' ?>> Semanalmente informando as <b class='green'>tarefas programadas para a semana</b>  (* enviada no início da semana)<br><br>
                        <input type="checkbox" class='form-control flat col-md-12 col-xs-12'  name="configs" value="notificacao_diaria_tarefa" <?= ($user->notificacao_diaria_tarefa) ? "checked='checked'" : '' ?>> Diariamente informando as <b class='green'>tarefas programadas para o dia</b> (* enviada no início do dia)<br><br>
                        <input type="checkbox" class='form-control flat col-md-12 col-xs-12'  name="configs" value="notificacao_prazo_tarefa" <?= ($user->notificacao_prazo_tarefa) ? "checked='checked'" : '' ?>> For o <b class='green'>prazo final</b> de concluir uma tarefa (* enviada no último dia de prazo para concluir a tarefa)<br><br>
                        <input type="checkbox" class='form-control flat col-md-12 col-xs-12'  name="configs" value="notificacao_atraso_tarefa" <?= ($user->notificacao_atraso_tarefa) ? "checked='checked'" : '' ?>> Alguma <b class='green'>tarefa estiver atrasada</b> no calendário (* enviada até que a tarefa seja concluida no sistema)<br><br>
                    <?php }/* else { ?>
                      <input type="checkbox"  class='form-control flat col-md-12 col-xs-12' name="configs" value="notificacao_dever" <?= ($user->notificacao_dever) ? "checked='checked'" : '' ?>> Se houver alguma nova pendência a ser enviada/comunicada para a Tríade <br><br>
                      <input type="checkbox" class='form-control flat col-md-12 col-xs-12'  name="configs" value="notificacao_prazo_dever" <?= ($user->notificacao_prazo_dever) ? "checked='checked'" : '' ?>> Se estiver <b class='green'>vencendo o prazo</b> para enviar alguma pendência para a Tríade (* enviada no último dia de prazo)<br><br>
                      <?php } */ ?>

                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?= $this->Html->script('/vendors/pnotify/dist/pnotify.js'); ?>
<script>
    $(document).ready(function () {
        $('input[type=checkbox]').on('ifChecked', function (event) {
            var parent = $(this).parent().get(0);
            editCongig('add', parent.getElementsByTagName('input')[0].value);
        });
        $('input[type=checkbox]').on('ifUnchecked', function (event) {
            var parent = $(this).parent().get(0);
            editCongig('remove', parent.getElementsByTagName('input')[0].value);
        });
    });
    function editCongig(event, tpconfig) {
        $.ajax({
            url: $('#urlroot').val() + 'users/editconfig/',
            data: 'event=' + event + '&tpconfig=' + tpconfig,
            dataType: 'json',
            type: 'post',
            success: function (data) {
                if (data.retorno) {
                    new PNotify({text: 'Configuração atualizada', type: 'success', styling: 'bootstrap3', animate_speed: 'fast'});
                } else {
                    new PNotify({text: 'Não foi possivel alterar essa configuração', type: 'error', styling: 'bootstrap3', animate_speed: 'fast'});
                }
            },
            error: function (a) {
                console.log(a);
            }
        });

    }
</script>