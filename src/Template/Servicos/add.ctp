<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Solicitar Serviço <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($servico, ["class" => "form-horizontal form-label-left col-md-8 col-sm-8 col-xs-12"]) ?>
                <?= $this->Flash->render() ?>

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="areaservico_id">Setor para o qual deseja solicitar <span class="required">*</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <?= $this->Form->input('areaservico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $areaservicos, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="gruposervico_id">Sub-Setor<span class="required">*</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <?= $this->Form->input('gruposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tiposervico_id">Serviço <span class="required">*</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <?= $this->Form->input('tiposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>
                    </div> 
                </div> 
                <?php
                if ($empresasocios->count() > 0) {
                    ?>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="empresasocio_id">Informe se o serviço for referente à informações do Empresário/Sócio
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <?= $this->Form->input('empresasocio_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $empresasocios, 'empty' => true]); ?>
                        </div> 
                    </div> 
                <?php } ?>
                <?php if ($funcionarios->count() > 0) { ?>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="funcionario_id">Informe se o serviço for referente à um funcionário específico 
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <?= $this->Form->input('funcionario_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $funcionarios, 'empty' => true]); ?>
                        </div> 
                    </div> 
                <?php } ?>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="descricao">Descreva um pouco sobre o que necessita nesse serviço
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div> 
                <br>
                <div id="termos">                            
                    <div class="form-group aceita_termos">
                        <label class="control-label col-md-8 col-sm-8 col-xs-12" for="aceita_termos">Eu estou de acordo com os <a href="/docs/1">Termos de Contrato do Serviço</a>
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <?= $this->Form->input('aceita_termos', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                        </div> 
                    </div> 
                </div>
                <br>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-sm-12 col-xs-12">
                        <?= $this->Html->link(__('Cancelar'), $backlink, ['class' => "btn btn-primary"]) ?>         
                        <button type="submit" class="btn btn-success">Solicitar Serviço</button>
                    </div>
                </div>
                <?= $this->Form->end() ?>

                <div id="abertura-empresa" class="col-md-4 col-sm-4 col-xs-12 lead green well well-sm" style="display: none;">
                    <h2>O orçamento para a abertura de empresa pode variar de acordo com as características da futura empresa.<br>
                        Para obter o valor exato desse serviço é necessário prosseguir o cadastramento.<br>
                        Assim saberemos exatamente o enquadramento da sua empresa.
                    </h2>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 lead green well well-sm tempo-servico" style="display: none;">
                    <i class="fa fa-clock-o"></i>
                    O tempo médio para execução desse serviço é de <span id="diasuteis"></span> úteis.
                </div>


            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/js/ajax/servicos.js'); ?>
<script>
    $(document).ready(function () {
        $("select").select2({placeholder: 'selecione uma opção'});

        $('form').on('submit', function () {
            if ($('.aceita_termos .icheckbox_flat-green').hasClass('checked') || $('#tiposervico-id').val() == 1) { //tiposervico = 1 é abertura de empresa, tem mais etapas especificas em outras telas
                return true;
            } else {
                alert('Você deve estar de acordo com termos de contrato para poder prosseguir.');
                return false;
            }
        });

        $('#tiposervico-id').change(function () {
            if (this.value) {
                verificaPrazoExecucao(this.value);
            }

            if (this.value == 1) {//abertura de empresa
                $('#abertura-empresa').show();
//                $('#termos').hide();
            } else {
//                $('#termos').show();
                $('#abertura-empresa').hide();
            }
        });

    });
    function verificaPrazoExecucao() {
        $.ajax({
            url: $('#urlroot').val() + 'servicos/verificaprazoexecucao/' + $('#tiposervico-id').val(),
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data.retorno) {
                    $('.tempo-servico').show();
                    $('#diasuteis').html(data.retorno);
                }
            },
            error: function (a) {
                console.log(a);
            }
        });

    }
</script>