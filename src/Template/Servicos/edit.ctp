<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2> Editar Serviço Solicitado <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($servico, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="areaservico_id">Setor <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('areaservico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $areaservicos, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gruposervico_id">Sub-Setor<span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('gruposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tiposervico_id">Serviço <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tiposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>
                    </div> 
                </div> 
                <?php
                if ($empresasocios->count() > 0) {
                    ?>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresasocio_id">Informe se o serviço for referente à informações do Empresário/Sócio
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('empresasocio_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $empresasocios, 'empty' => true]); ?>
                    </div> 
                </div> 
                <?php } ?>
                <?php if ($funcionarios->count() > 0) { ?>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="funcionario_id">Informe se o serviço for referente à um funcionário específico 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('funcionario_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $funcionarios, 'empty' => true]); ?>
                    </div> 
                </div> 
                <?php } ?>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Descreva do serviço solicitado
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="valor_servico">Valor do Serviço <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('valor_servico', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="valor_adicionalmensal">Valor Adicional na Mensal <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('valor_adicionalmensal', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="prazo_entrega">Prazo para entrega (dias úteis) <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('prazo_entrega', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                    </div> 
                </div>         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status_servico">Status do Serviço <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->select('status_servico', array('1' => 'Solicitado', '2' => 'Em análise', '3' => 'Em Execução', '4' => 'Executado'), ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                    </div> 
                </div>                         

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js', array('inline' => false)); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/js/ajax/servicos.js'); ?>
<script>
    $(document).ready(function () {
        $(":input").inputmask();
        $("select").select2({placeholder: 'selecione uma opção'});
        
        $('#valor-servico,#valor-adicionalmensal').inputmask('decimal', {
            radixPoint: ",",
            groupSeparator: ".",
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
            rightAlign: false,
            onBeforeMask: function (value, opts) {
                return value;
            }});
    });
</script>