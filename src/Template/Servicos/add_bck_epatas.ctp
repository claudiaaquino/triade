<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Solicitar Serviço <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($servico, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>
                <div id="wizard" class="form_wizard wizard_horizontal">
                    <ul class="wizard_steps" >
                        <li>
                            <a href="#step-1">
                                <span class="step_no">1</span>
                                <span class="step_descr">Serviço</span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-2">
                                <span class="step_no">2</span>
                                <span class="step_descr">Orçamento</span>
                            </a>
                        </li>
                    </ul>
                    <div id="step-1">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="areaservico_id">Setor para o qual deseja solicitar <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('areaservico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $areaservicos, 'empty' => true]); ?>
                            </div> 
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gruposervico_id">Sub-Setor<span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('gruposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>
                            </div> 
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tiposervico_id">Serviço <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('tiposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>
                            </div> 
                        </div> 
                        <?php
                        if ($empresasocios->count() > 0) {
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresasocio_id">Informe se o serviço for referente à informações do Empresário/Sócio
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('empresasocio_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $empresasocios, 'empty' => true]); ?>
                                </div> 
                            </div> 
                        <?php } ?>
                        <?php if ($funcionarios->count() > 0) { ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="funcionario_id">Informe se o serviço for referente à um funcionário específico 
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('funcionario_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $funcionarios, 'empty' => true]); ?>
                                </div> 
                            </div> 
                        <?php } ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Descreva um pouco sobre o que necessita nesse serviço
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                            </div> 
                        </div> 
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?= $this->Html->link(__('Cancelar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                            </div>
                        </div>
                    </div>                
                    <div id="step-2">
                        <div id="abertura-empresa" class=" col-md-8 col-sm-8 col-xs-12" style="display: none;">
                            <h2>O orçamento para a abertura de empresa pode variar entre X e Y <br>
                                Para obter o valor exato desse serviço é necessário prosseguir o cadastramento.<br>
                                Assim saberemos exatamente o enquadramento da sua empresa.
                            </h2>
                        </div>
                        <div id="orcamento">
                            <p class="lead">Orçamento para o serviço de <span id="nomeservico"></span></p>
                            <div class="table-responsive col-md-8 col-sm-8 col-xs-12" > 
                                <table class="table table-striped orcamento">
                                    <tbody>
                                        <tr id="valorservico">
                                            <th style="width:50%">Valor pela Contratação do Serviço</th>
                                            <td></td>
                                        </tr>
                                        <tr  id="valoraddmensalidade">
                                            <th>Valor de Acréscimo na Mensalidade</th>
                                            <td></td>
                                        </tr>
                                        <tr  id="valoratualmensalidade">
                                            <th>Valor da Mensalidade Atual</th>
                                            <td></td>
                                        </tr>
                                        <tr id="valornovamensalidade">
                                            <th>Valor da Nova Mensalidade</th>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 lead green well well-sm"><i class="fa fa-clock-o"></i>  O tempo médio para execução desse serviço é de <span id="diasuteis"></span> úteis.</div>
                        <div id="termos">
                            <div class="form-group aceita_termos">
                                <label class="control-label col-md-7 col-sm-7 col-xs-12" for="aceita_termos">Eu estou de acordo com o valor do serviço, assim como o valor da nova mensalidade
                                </label>
                                <div class="col-md-1 col-sm-1 col-xs-12">
                                    <?= $this->Form->input('aceita_termos', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div> 
                            <div class="form-group aceita_valor">
                                <label class="control-label col-md-7 col-sm-7 col-xs-12" for="aceita_valor">Eu estou de acordo com os <a href="/docs/1">Termos de Contrato do Serviço</a>
                                </label>
                                <div class="col-md-1 col-sm-1 col-xs-12">
                                    <?= $this->Form->input('aceita_valor', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div> 
                        </div>
                    </div>
                </div>

                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js', array('inline' => false)); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/js/ajax/servicos.js'); ?>
<script>
    $(document).ready(function () {
        $("select").select2({placeholder: 'selecione uma opção'});
        $("select").addClass('select2');

        $('#wizard').smartWizard({
            transitionEffect: 'slide',
            labelNext: 'Próximo',
            labelFinish: 'Solicitar Serviço e Prosseguir Preenchimento'
        });

        $('.buttonNext').click(function () {
            $('html, body').animate({scrollTop: $('.row').offset().top}, 'slow');
        });

        $('form').on('submit', function () {
            if (($('.aceita_termos .icheckbox_flat-green').hasClass('checked') && $('.aceita_valor .icheckbox_flat-green').hasClass('checked')) || $('#tiposervico-id').val() == 1) {
                return true;
            } else {
                alert('Você deve estar de acordo com os valores e termos de contrato para poder prosseguir.');
                return false;
            }
        });

        $('.buttonNext').addClass('btn btn-primary');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-success');

        $('#tiposervico-id').change(function () {
            if (this.value == 1) {//abertura de empresa
                $('#abertura-empresa').show();
                $('#orcamento').hide();
                $('#termos').hide();
            } else {
                $('#termos').show();
                $('#orcamento').show();
                $('#abertura-empresa').hide();
                calculaValorOrcamento(this.value);
            }
        });

    });
    function calculaValorOrcamento(servico) {
        $.ajax({
            url: $('#urlroot').val() + 'previsaoorcamentos/calculaorcamento/' + servico,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                var orcamento = data.retorno;
                $('#nomeservico').html($('#tiposervico-id  option:selected').text());
                $('#valorservico td').html("R$ " + orcamento.valor_servico);
                $('#valoraddmensalidade td').html("R$ " + orcamento.valor_adicionalmensal);
                $('#valoratualmensalidade td').html("R$ " + orcamento.mensalidade_atual);
                $('#diasuteis').html(orcamento.tempo_execucao);
                console.log(orcamento);
                if (orcamento.gratis) {
                    $('#valornovamensalidade td').html("<div class='green'>Você possui isenção do valor desse serviço e também não haverá acréscimos no valor da sua mensalidade</div>");
                } else {
                    $('#valornovamensalidade td').html("R$ " + (orcamento.mensalidade_atual + orcamento.valor_adicionalmensal));
                }
            },
            error: function (a) {
                console.log(a);
            }
        });
    }
//    1	No Ato da solicitação
//    2	Mensal
//    3	Anual
//    4	No ato e Mensal
</script>