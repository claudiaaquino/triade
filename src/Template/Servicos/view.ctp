<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($servico->tiposervico->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <?php if ($admin) { ?>
                            <p class="title"><?= __('Empresa') ?></p>
                            <p><?= $servico->has('empresa') ? $this->Html->link($servico->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $servico->empresa->id]) : 'Não Informado' ?></p>
                        <?php } ?>

                        <p class="title">Setor do Serviço</p>
                        <?php if ($admin) { ?>
                            <p><?= $servico->has('areaservico') ? $this->Html->link($servico->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $servico->areaservico->id]) : 'Não Informado' ?></p>
                        <?php } else { ?>
                            <p><?= $servico->has('areaservico') ? $servico->areaservico->descricao : 'Não Informado' ?></p>
                        <?php } ?>

                        <p class="title">Serviço Solicitado</p>
                        <?php if ($admin) { ?>
                            <p><?= $servico->has('tiposervico') ? $this->Html->link($servico->tiposervico->descricao, ['controller' => 'Tiposervicos', 'action' => 'view', $servico->tiposervico->id]) : 'Não Informado' ?></p>
                        <?php } else { ?>
                            <p><?= $servico->has('tiposervico') ? $servico->tiposervico->descricao : 'Não Informado' ?></p>
                        <?php } ?>

                        <?php if ($servico->has('empresasocio')) { ?>
                            <p class="title">Empresário/Sócio</p>
                            <p><?= $servico->has('empresasocio') ? $this->Html->link($servico->empresasocio->nome, ['controller' => 'Empresasocios', 'action' => 'view', $servico->empresasocio->id]) : 'Não Informado' ?></p>
                        <?php } ?>
                        <?php if ($servico->has('funcionario')) { ?>
                            <p class="title">Funcionário</p>
                            <p><?= $servico->has('funcionario') ? $this->Html->link($servico->funcionario->nome, ['controller' => 'Funcionarios', 'action' => 'view', $servico->funcionario->id]) : 'Não Informado' ?></p>
                        <?php } ?>

                        <p class="title">Detalhes do serviço</p>
                        <p>  <?= $servico->descricao ? $this->Text->autoParagraph(h($servico->descricao)) : 'Não Informado'; ?></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <?php if ($admin) { ?>
                            <p class="title"><?= __('Valor do Serviço') ?></p>
                            <p><?= $servico->valor_servico ? 'R$ ' . $this->Number->format($servico->valor_servico) : 'Não Informado'; ?></p>


                            <p class="title"><?= __('Valor Adicional na Mensal') ?></p>
                            <p><?= $servico->valor_adicionalmensal ? 'R$ ' . $this->Number->format($servico->valor_adicionalmensal) : 'Não Informado'; ?></p>
                        <?php } ?>

                        <p class="title"><?= __('Prazo para Entrega') ?></p>
                        <p><?= $servico->prazo_entrega ? $this->Number->format($servico->prazo_entrega) . ' dias úteis' : 'Não Informado'; ?></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $servico->status_servico == 1 ? 'solicitado' : ($servico->status_servico == 2 ? 'em análise' : ($servico->status_servico == 3 ? 'em execução' : ($servico->status_servico == 4 ? 'executado' : '--') )); ?></p>

                        <p class="title">Solicitante</p>
                        <p><?= $servico->has('user') ? $this->Html->link($servico->user->nome, ['controller' => 'Users', 'action' => 'view', $servico->user->id]) : 'Não Informado' ?></p>

                        <p class="title"><?= __('Dt. Solicitação') ?></p>
                        <p><?= $servico->dt_cadastro ? $servico->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $servico->last_update ? $servico->last_update : 'Não Informado'; ?></p>


                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?php if ($admin) { ?>
                            <?= $this->Html->link("Editar", ['action' => 'edit', $servico->id], ['class' => "btn btn-primary"]) ?>
                            <?= $this->Form->postLink("Deletar", ['action' => 'delete', $servico->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $servico->id)]) ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($admin && !empty($servico->tarefa)): ?>
    <div class="x_panel">
        <div class="x_title">
            <h2 class="green"><i class="fa fa-file"></i> Tarefa gerada para esse serviço </h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><?= $this->Html->link(__(' Visualizar Tudo da Tarefa'), ['controller' => 'Tarefas', 'action' => 'view', $servico->tarefa->id], ['class' => "btn btn-dark fa fa-file-o"]) ?>  </li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?php if ($servico->tarefa->justificativa_refazer) { ?>
                <div id="div_justificativa_refazer">
                    <div class="ln_solid"></div>
                    <p class="title green">Motivo para reabertura da tarefa</p>
                    <p  id="info_justificativa_refazer"><?= $servico->tarefa->justificativa_refazer ? $servico->tarefa->justificativa_refazer : 'Não Informado'; ?></p>
                </div>
            <?php } ?>
            <?php if ($servico->tarefa->anotacao_interna) { ?>
                <div class="ln_solid"></div>
                <p class="title green">Anotações internas da Tríade</p>
                <p><?= $servico->tarefa->anotacao_cliente ? $servico->tarefa->anotacao_cliente : 'Não Informado'; ?></p>
            <?php } ?>
            <?php if ($servico->tarefa->anotacao_cliente) { ?>
                <div class="ln_solid"></div>
                <p class="title green">Anotações para o cliente</p>
                <p><?= $servico->tarefa->anotacao_cliente ? $servico->tarefa->anotacao_cliente : 'Não Informado'; ?></p>
                <div class="ln_solid"></div>
            <?php } ?>



            <?php
            /** Se houver documentos anexados à tarefa e se tiver usuario atribuido para essa tarefa */
            if (!empty($servico->tarefa->tarefadocumentos)):
                ?>
                <div class="x_panel">
                    <div class="x_title">
                        <h2 class="green"><i class="fa fa-file"></i> Documentos Anexados </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><?= $this->Html->link(__('  Cadastrar Outro'), ['controller' => 'Tarefadocumentos', 'action' => 'add', $servico->tarefa->id], ['class' => "btn btn-dark fa fa-file"]) ?>  </li>
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th scope="col"  class="column-title"><?= __('Documento') ?></th>
                                        <th scope="col"  class="column-title"><?= __('Descrição') ?></th>
                                        <th scope="col"  class="column-title"><?= __('Tamanho do Arquivo') ?></th>
                                        <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $cor = 'even';
                                    foreach ($servico->tarefa->tarefadocumentos as $tarefadocumentos):
                                        ?>
                                        <tr class="<?= $cor ?> pointer">
                                            <td><?= h($tarefadocumentos->documento->tipodocumento->descricao) ?></td>
                                            <td><?= h($tarefadocumentos->documento->descricao) ?></td>
                                            <td><?= ($tarefadocumentos->documento->filesize / 1024) < 1024 ? floor($tarefadocumentos->documento->filesize / 1024) . ' KB' : floor(($tarefadocumentos->documento->filesize / 1024) / 1024) . ' MB'; ?></td>

                                            <td  class=" last">
                                                <div class="btn-group">
                                                    <?= $this->Html->link(__('Download'), "/docs/" . $tarefadocumentos->documento->file, ['target' => '_blank', 'class' => "btn btn-info btn-xs"]) ?>
                                                    <?php if ($user_triade) echo $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefadocumentos', 'action' => 'delete', $tarefadocumentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefadocumentos->id)]) ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                        $cor = $cor == 'even' ? 'odd' : 'even';
                                    endforeach;
                                    ?>
                                </tbody>                       
                            </table>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (!empty($servico->tarefa->tarefausuarios)): ?>
                <div class="x_panel">
                    <div class="x_title">
                        <h2 class="green"><i class="fa fa-file"></i> Responsáveis por essa Tarefa </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <?php if ($servico->tarefa->permite_editar) { ?>
                                <li><?= $this->Html->link(__('  Cadastrar Outro'), ['controller' => 'Tarefausuarios', 'action' => 'add', $servico->tarefa->id], ['class' => "btn btn-dark fa fa-file"]) ?>  </li>
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                            <?php } ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th scope="col"  class="column-title">Nome do Responsável</th>
                                        <th scope="col"  class="column-title">Dt Concluída</th>
                                        <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $cor = 'even';
                                    foreach ($servico->tarefa->tarefausuarios as $tarefausuarios):
                                        ?>
                                        <tr class="<?= $cor ?> pointer">
                                            <td><?= h($tarefausuarios->user->nome) ?></td>
                                            <td><?= $tarefausuarios->dt_concluida ? $tarefausuarios->dt_concluida : 'NÃO CONCLUÍDA' ?></td>
                                            <?php if ($servico->tarefa->permite_deletar || $servico->tarefa->user_id == $tarefausuarios->user->id) { ?>
                                                <td  class=" last">
                                                    <div class="btn-group">
                                                        <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Tarefausuarios', 'action' => 'delete', $tarefausuarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $tarefausuarios->id)]) ?>
                                                    </div>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                        $cor = $cor == 'even' ? 'odd' : 'even';
                                    endforeach;
                                    ?>
                                </tbody>                       
                            </table>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

        </div>
    </div>
<?php endif; ?>
<?php if (!empty($servico->contratos)): ?>
    <div class="x_panel">
        <div class="x_title">
            <h2 class="green"><i class="fa fa-file"></i> Contratos Vínculados </h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th scope="col"  class="column-title"><?= __('Id') ?></th>
                            <th scope="col"  class="column-title"><?= __('Servico Id') ?></th>
                            <th scope="col"  class="column-title"><?= __('Empresa Id') ?></th>
                            <th scope="col"  class="column-title"><?= __('Documento Id') ?></th>
                            <th scope="col"  class="column-title"><?= __('Valor Servico') ?></th>
                            <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                            <th scope="col"  class="column-title"><?= __('Dt Contrato') ?></th>
                            <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                            <th scope="col"  class="column-title"><?= __('Status') ?></th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $cor = 'even';
                        foreach ($servico->contratos as $contratos):
                            ?>
                            <tr class="<?= $cor ?> pointer">
                                <td><?= h($contratos->id) ?></td>
                                <td><?= h($contratos->servico_id) ?></td>
                                <td><?= h($contratos->empresa_id) ?></td>
                                <td><?= h($contratos->documento_id) ?></td>
                                <td><?= h($contratos->valor_servico) ?></td>
                                <td><?= h($contratos->user_id) ?></td>
                                <td><?= h($contratos->dt_contrato) ?></td>
                                <td><?= h($contratos->last_update) ?></td>
                                <td><?= h($contratos->status) ?></td>

                                <td  class=" last">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('Visualizar'), ['controller' => 'Contratos', 'action' => 'view', $contratos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                        <?= $this->Html->link(__('Editar'), ['controller' => 'Contratos', 'action' => 'edit', $contratos->id], ['class' => "btn btn-info btn-xs"]) ?>
                                        <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Contratos', 'action' => 'delete', $contratos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $contratos->id)]) ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $cor = $cor == 'even' ? 'odd' : 'even';
                        endforeach;
                        ?>
                    </tbody>                       
                </table>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (!empty($servico->servicodocumentos)): ?>
    <div class="x_panel">
        <div class="x_title">
            <h2 class="green"><i class="fa fa-file"></i> Servicodocumentos Vínculados </h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th scope="col"  class="column-title"><?= __('Id') ?></th>
                            <th scope="col"  class="column-title"><?= __('Documento Id') ?></th>
                            <th scope="col"  class="column-title"><?= __('Servico Id') ?></th>
                            <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                            <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $cor = 'even';
                        foreach ($servico->servicodocumentos as $servicodocumentos):
                            ?>
                            <tr class="<?= $cor ?> pointer">
                                <td><?= h($servicodocumentos->id) ?></td>
                                <td><?= h($servicodocumentos->documento_id) ?></td>
                                <td><?= h($servicodocumentos->servico_id) ?></td>
                                <td><?= h($servicodocumentos->dt_cadastro) ?></td>

                                <td  class=" last">
                                    <div class="btn-group">
                                        <?= $this->Html->link(__('Visualizar'), ['controller' => 'Servicodocumentos', 'action' => 'view', $servicodocumentos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                        <?= $this->Html->link(__('Editar'), ['controller' => 'Servicodocumentos', 'action' => 'edit', $servicodocumentos->id], ['class' => "btn btn-info btn-xs"]) ?>
                                        <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Servicodocumentos', 'action' => 'delete', $servicodocumentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $servicodocumentos->id)]) ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $cor = $cor == 'even' ? 'odd' : 'even';
                        endforeach;
                        ?>
                    </tbody>                       
                </table>
            </div>
        </div>
    </div>
<?php endif; ?>
    


