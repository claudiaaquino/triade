
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Serviços Solicitados</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Solicitar Serviço'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <?php if (!empty($servicos)) { ?>
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_cadastro', 'Dt. da Solicitação') ?></th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('areaservico_id', 'Setor') ?></th>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('tiposervico_id', 'Serviço') ?></th>
                                    <?php if ($admin) { ?>
                                        <th scope="col" class="column-title"><?= $this->Paginator->sort('Empresas.razao', 'Empresa') ?></th>
                                        <th scope="col" class="column-title"><?= $this->Paginator->sort('valor_servico', 'Valor do Serviço') ?></th>
                                        <th scope="col" class="column-title"><?= $this->Paginator->sort('valor_adicionalmensal', 'Adicional na Mensal') ?></th>
                                    <?php } ?>
                                    <th scope="col" class="column-title"><?= $this->Paginator->sort('status') ?></th>

                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($servicos as $servico) {
                                    ?>                                
                                    <tr class="<?= $cor ?> pointer">

                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= $servico->id ?>">
                                        </td>
                                        <td><?= h($servico->dt_cadastro) ?></td>
                                        <td><?= $servico->has('areaservico') ? $this->Html->link($servico->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $servico->areaservico->id]) : '' ?></td>
                                        <td><?= $servico->has('tiposervico') ? $this->Html->link($servico->tiposervico->descricao, ['controller' => 'Tiposervicos', 'action' => 'view', $servico->tiposervico->id]) : '' ?></td>
                                        <?php if ($admin) { ?>
                                            <td><?= $servico->empresa->razao ? $servico->empresa->razao : '---' ?></td>
                                            <td><?= $servico->valor_servico ? 'R$ ' . $this->Number->format($servico->valor_servico) : '---' ?></td>
                                            <td><?= $servico->valor_adicionalmensal ? 'R$ ' . $this->Number->format($servico->valor_adicionalmensal) : '---' ?></td>
                                        <?php } ?>
                                        <td><?= $servico->status_servico == 1 ? 'solicitado' : ($servico->status_servico == 2 ? 'em análise' : ($servico->status_servico == 3 ? 'em execução' : ($servico->status_servico == 4 ? 'executado' : '--') )); ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $servico->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?php if ($admin) { ?>
                                                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $servico->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $servico->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $servico->id)]) ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                }
                                ?>
                            </tbody>                       
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('próximo') . ' >') ?>
                            </ul>
                            <?= $this->Paginator->counter() ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <h2>Você ainda não solicitou nenhum serviço extra ao seu contrato</h2>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
