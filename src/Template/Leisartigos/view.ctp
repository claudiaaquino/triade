<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Lei <?= h($leisartigo->leisnorma->numero) ?> - Artigo  <?= h($leisartigo->nome) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Lei/Norma') ?></p>
                        <p><?= $leisartigo->has('leisnorma') ? $this->Html->link($leisartigo->leisnorma->numero, ['controller' => 'Leisnormas', 'action' => 'view', $leisartigo->leisnorma->id]) . ' - ' . $leisartigo->leisnorma->descricao : 'Não Informado' ?></p>

                        <p class="title"><?= __('Artigo') ?></p>
                        <p><?= $leisartigo->nome ? $leisartigo->nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Descrição do Artigo') ?></p>
                        <p>  <?= $leisartigo->descricao ? $this->Text->autoParagraph(h($leisartigo->descricao)) : 'Não Informado'; ?></p>

                    </div>
                </div><div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title"><?= __('Ordem que aparece') ?></p>
                        <p><?= $leisartigo->ordem ? $this->Number->format($leisartigo->ordem) : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $leisartigo->dt_cadastro ? $leisartigo->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $leisartigo->last_update ? $leisartigo->last_update : 'Não modificado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $leisartigo->status ? __('Ativo') : __('Inativo'); ?></p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $leisartigo->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $leisartigo->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisartigo->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($leisartigo->leisparagrafos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Parágrafos </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Parágrafo') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Descricao') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Ult. Atualização') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($leisartigo->leisparagrafos as $leisparagrafos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($leisparagrafos->nome) ?></td>
                                        <td><?= h($leisparagrafos->descricao) ?></td>
                                        <td><?= h($leisparagrafos->last_update) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Leisparagrafos', 'action' => 'view', $leisparagrafos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Leisparagrafos', 'action' => 'edit', $leisparagrafos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Leisparagrafos', 'action' => 'delete', $leisparagrafos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisparagrafos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($leisartigo->leisincisos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Incisos </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Nome') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Descrição') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Ult. Atualização') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($leisartigo->leisincisos as $leisincisos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($leisincisos->nome) ?></td>
                                        <td><?= h($leisincisos->descricao) ?></td>
                                        <td><?= h($leisincisos->last_update) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Leisincisos', 'action' => 'view', $leisincisos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Leisincisos', 'action' => 'edit', $leisincisos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Leisincisos', 'action' => 'delete', $leisincisos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisincisos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($leisartigo->leisletras)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Letras  </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th scope="col"  class="column-title"><?= __('Letra') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Descrição') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Ult. Atualização') ?></th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($leisartigo->leisletras as $leisletras):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td><?= h($leisletras->nome) ?></td>
                                        <td><?= h($leisletras->descricao) ?></td>
                                        <td><?= h($leisletras->last_update) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Leisletras', 'action' => 'view', $leisletras->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Leisletras', 'action' => 'edit', $leisletras->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Leisletras', 'action' => 'delete', $leisletras->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $leisletras->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>


