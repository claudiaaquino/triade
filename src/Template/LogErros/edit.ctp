<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= __('Editar Log Erro') ?> <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($logErro, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>


                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao_erro">Descrição do Erro <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('descricao_erro', ['type'=>'textarea',"class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="msg_retorno">Mensagem de Retorno 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('msg_retorno', ['type'=>'textarea',"class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>      

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status do Erro
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->select('status', array('1' => 'Correção Solicitada', '2' => 'Em análise', '3' => 'Em correção', '4' => 'Corrigido'), ["class" => "form-control col-md-12  col-sm-12 col-xs-12", 'label' => false, 'style' => 'width:100%']); ?>
                    </div> 
                </div>                         



                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<script>
    $(document).ready(function () {
        $("select").select2();
    });
</script>
