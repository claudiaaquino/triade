<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Erro reportado #<?= h($logErro->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">

                        <p class="title"><?= __('Usuário que reportou erro:') ?></p>
                        <p><?= $logErro->has('user') ? $this->Html->link($logErro->user->nome, ['controller' => 'Users', 'action' => 'view', $logErro->user->id]) : 'Não Informado' ?></p>

                        <?php if ($user_triade) { ?>
                            <p class="title"><?= __('Empresa do usuário') ?></p>
                            <p><?= $logErro->has('empresa') ? $this->Html->link($logErro->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $logErro->empresa->id]) : 'Não Informado' ?></p>
                        <?php } else { ?>
                            <p class="title"><?= __('Empresa do usuário') ?></p>
                            <p><?= $logErro->has('empresa') ? $logErro->empresa->razao : 'Não Informado' ?></p>
                        <?php } ?>

                        <p class="title"><?= __('Documento Print') ?></p>
                        <p><?= $logErro->documento_print ? $logErro->documento_print : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Url do erro') ?></p>
                        <p><?= $logErro->url ? $logErro->url : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Descrição do Erro') ?></p>
                        <p><?= $logErro->descricao_erro ? $logErro->descricao_erro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $logErro->dt_cadastro ? $logErro->dt_cadastro : 'Não Informado'; ?></p>

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?php if ($logErro->documento_print) echo $this->Html->link("Ver Print", 'docs/erros/' . $logErro->documento_print, ['class' => "btn btn-success", 'target' => "_blank"]) ?>
                        <?php
                        if ($user_triade) {
                            echo $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]);
                            echo $this->Html->link("Editar", ['action' => 'edit', $logErro->id], ['class' => "btn btn-primary"]);
                            echo $this->Form->postLink("Deletar", ['action' => 'delete', $logErro->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $logErro->id)]);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


