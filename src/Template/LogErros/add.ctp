<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Reportar erro no Sistema <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($logErro, ["class" => "form-horizontal form-label-left", 'enctype' => "multipart/form-data"]) ?>
                <?= $this->Flash->render() ?>

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="documento_print">Se você tiver um print com o erro, anexe por favor 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->file('printfile', ['label' => false, 'class' => 'file']) ?>   

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="url">Ou então coloque aqui a URL da tela em que ocorreu o problema 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('url', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="descricao_erro">Descreva o erro <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('descricao_erro', ['type' => 'textarea', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>          
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Reportar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>