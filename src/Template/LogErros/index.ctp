
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2>Erros Reportados no Sistema</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Reportar outro Erro'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_cadastro') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('COD') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('user_id', 'Usuário') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('url') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('status') ?></th>

                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($logErros as $logErro) {
                                ?>                                
                                <tr class="<?= $cor ?> pointer">

                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= $logErro->id ?>">
                                    </td>

                                    <td><?= h($logErro->dt_cadastro) ?></td>
                                    <td><?= $this->Number->format($logErro->id) ?></td>
                                    <td><?= $logErro->has('user') ? $this->Html->link($logErro->user->nome, ['controller' => 'Users', 'action' => 'view', $logErro->user->id]) : '' ?></td>
                                    <td><?= h($logErro->url) ?></td>
                                    <td><?= ($logErro->status == 1) ? 'Solicitado' : ($logErro->status == 2 ? 'Em análise' : ($logErro->status == 3 ? 'Em correção' : 'Corrigido')) ?></td>

                                    <td  class=" last">
                                        <div class="btn-group">

                                            <?= $this->Html->link(__('Ver Print'), 'docs/erros/' . $logErro->documento_print, ['class' => "btn btn-success btn-xs", 'target' => '_blank']) ?>
                                            <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $logErro->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $logErro->id], ['class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $logErro->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $logErro->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
