<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<?= $this->Html->css('/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2  class="green">Cadastrar Serviço <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($tiposervico, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gruposervico_id">Setor <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('areaservico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $areaservicos, 'empty' => '  ']); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gruposervico_id">Subsetor <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('gruposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $gruposervicos, 'empty' => '  ']); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">Serviço <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('nome', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descricao">Descrição 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('descricao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>        
                <div class="ln_solid"></div>

                <div class="form-group">
                    <label class="control-label col-md-5 col-sm-5 col-xs-12" for="setor_ids">Quais funcionários da tríade serão responsáveis por essa tarefa?
                    </label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <?= $this->Form->input('user_ids', ['options' => $users, "class" => "form-control col-md-7 col-xs-12", "multiple" => "multiple", 'label' => false, "style" => "width: 100%", 'empty' => '  ']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-5 col-sm-5 col-xs-12" for="solicitavel" data-placement="top" data-toggle="tooltip" data-original-title="* além do já feito na rotina da Tríade">
                        Esse serviço pode ser solicitado pelo cliente?
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?= $this->Form->checkbox('solicitavel', ['id' => 'solicitavel', "class" => "form-control flat col-md-5 col-xs-5", 'label' => false]); ?>

                    </div> 
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-5 col-sm-5 col-xs-12" for="interno" data-placement="top" data-toggle="tooltip" data-original-title="* Serviços que a Tríade executa todo mês ou  ano para determinados Clientes">
                        Esse serviço é um serviço de rotina mensal/anual da Tríade?
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?= $this->Form->checkbox('interno', ['id' => 'interno', "class" => "form-control flat col-md-5 col-xs-5", 'label' => false]); ?>

                    </div> 
                </div> 
                <br><br>
                <div class="clearfix"></div>

                <div class="x_panel">
                    <div class="x_title">
                        <h2  class="green">Informações Específicas <small>* campos obrigatórios</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                            </li>                   
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" style="display: none">                   
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="setor_ids">Para quais empresas a Tríade deve executar esse serviço mensalmente?
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?= $this->Form->input('empresa_ids', ['options' => $empresas, "class" => "form-control col-md-7 col-xs-12", "multiple" => "multiple", 'label' => false, "style" => "width: 100%", 'empty' => '  ']); ?>
                            </div>
                            <a href="javascript:void(0);" onclick="marcarTodos('empresa-ids');">Marcar Todas</a>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-8 col-sm-8 col-xs-12" for="notificacao_concluido_cliente"   data-placement="top" data-toggle="tooltip" data-original-title="(* o cliente será notificado por email logo que o funcionário finalizar essa tarefa">
                                Os clientes devem ser notificados quando essa tarefa for concluida? 
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->checkbox('notificacao_concluido_cliente', [ "class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>
                            </div> 
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-8 col-sm-8 col-xs-12" for="notificacao_dt_vencimento"  data-placement="top" data-toggle="tooltip" data-original-title="(* o cliente será notificado por email também na data de vencimento, além da opção acima) ">
                                Os clientes devem ser notificados sobre a data de vencimento do documento?  
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->checkbox('notificacao_dt_vencimento', [ "class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>
                            </div> 
                        </div>  
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="lembrete_vencimento"  data-placement="top" data-toggle="tooltip" data-original-title="(* Se deixar em branco, o sistema enviará email com texto padrão de vencimento)">
                                Escreva uma mensagem de lembrete de vencimento para o cliente:  
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <?= $this->Form->input('lembrete_vencimento', ['type' => 'textarea', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                            </div> 
                        </div>  
                        <br><br>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="dia_inicio_execucao">Qual o dia do mês que essa tarefa DEVERIA SER INICIADA? 
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('dia_inicio_execucao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'min' => 1, 'max' => '31']); ?>
                            </div> 
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                Considerar somente dias úteis? <?= $this->Form->checkbox('dia_inicio_execucao_uteis', [ "class" => "form-control flat  col-md-1 col-xs-1", 'label' => false]); ?>
                            </div> 
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="dia_fim_execucao">Qual o ÚLTIMO PRAZO para a finalização dessa tarefa? *
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('dia_fim_execucao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'min' => 1, 'max' => '31']); ?>
                            </div> 
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                Considerar somente dias úteis? <?= $this->Form->checkbox('dia_fim_execucao_uteis', [ "class" => "form-control  flat  col-md-1 col-xs-1", 'label' => false]); ?>
                            </div> 
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="dia_lembrete_execucao">Que dia o funcionário deverá receber um LEMBRETE DO PRAZO FINAL? 
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->input('dia_lembrete_execucao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'min' => 1, 'max' => '31']); ?>
                            </div> 
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                Considerar somente dias úteis? <?= $this->Form->checkbox('dia_lembrete_execucao_uteis', ["class" => "form-control flat   col-md-1 col-xs-1", 'label' => false]); ?>
                            </div> 
                        </div> 

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <label class="control-label col-md-6 col-sm-6 col-xs-12">Com qual cor essa tarefa deverá aparecer na agenda?</label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="input-group colorpicker">
                                    <?= $this->Form->input('cor_tarefa', ["class" => "form-control", 'value' => "#5736e0", 'label' => false]); ?>
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <label class="control-label col-md-8 col-sm-8 col-xs-12 red" for="tarefa_automatica">Essa tarefa deve ser programada mensalmente? 
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->checkbox('mensal', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>
                            </div> 
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-8 col-sm-8 col-xs-12 red" for="tarefa_automatica">Essa tarefa deve ser programada anualmente? 
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->checkbox('anual', [ "class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>
                            </div> 
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="mes">Meses (* marque apenas se deseja determinar algum específico)
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <?= $this->Form->input('mes', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $meses, "multiple" => "multiple", 'empty' => true, "style" => "width: 100%"]); ?>
                            </div> 
                            <a href="javascript:void(0);" onclick="marcarTodos('mes');">Marcar Todos</a>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-8 col-sm-8 col-xs-12 red" for="tarefa_automatica">Deseja que o sistema gere tarefas AUTOMATICAMENTE nas agendas dos reponsáveis por elas? *
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <?= $this->Form->checkbox('tarefa_automatica', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>
                            </div> 
                        </div> 
                        <br><br>
                        <div class="ln_solid"></div>
                        <?php /* <div class="form-group">
                          <label class="control-label col-md-6 col-sm-6 col-xs-12 red" for="dia_tarefa_automatica">Se sim, qual dia do mês que deseja gerar as tarefas na agenda?
                          </label>
                          <div class="col-md-2 col-sm-2 col-xs-12">
                          <?= $this->Form->input('dia_tarefa_automatica', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'min' => 1, 'max' => '31']); ?>
                          </div>
                          <div class="clearfix"></div><br>
                          <label class="control-label col-md-6 col-sm-6 col-xs-12 red" for="dia_tarefa_automatica_uteis">Considerar somente dias úteis?
                          </label>
                          <div class="col-md-3 col-sm-3 col-xs-12 red">
                          <?= $this->Form->checkbox('dia_tarefa_automatica_uteis', ['hiddenField' => false, "class" => "form-control flat   col-md-1 col-xs-1", 'label' => false]); ?>
                          </div>
                          </div>
                         */ ?>




                    </div>      
                </div>      


                <div class="x_panel">
                    <div class="x_title">
                        <h2 class="green">Instruções para Execução desse Serviço <small>* campos obrigatórios</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                            </li>                   
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content"  style="display: none">
                        <div class="form-group">
                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="instrucoes_execucao"  data-placement="top" data-toggle="tooltip" data-original-title="Escreva as instruções como você deseja que apareça para o executor dela">
                                Digite as instruções para execução dessa tarefa:  
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <?= $this->Form->input('instrucoes_execucao', ['type' => 'textarea', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                            </div>         
                        </div>  
                    </div>        
                </div>        
                <div class="x_panel">
                    <div class="x_title">
                        <h2 class="green">Valor e Prazos quando o Serviço é Solicitado<small>* campos obrigatórios</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                            </li>                   
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content"  style="display: none">
                        <div class="form-group">
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="formaspagamentoservico_id">Forma de Pagamento <span class="required">*</span>
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('formaspagamentoservico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $formaspagamentoservicos, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="valor_servico">Valor do Serviço (no ato do contrato/solicitação) 
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('valor_servico', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div>                         

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="valor_adicionalmensal">Valor do Serviço (adicional na mensal do cliente) 
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('valor_adicionalmensal', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div>                         

                            <div class="form-group">
                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="gratis_todosusuarios">Esse serviço será gratuito para todos os USUÁRIOS do sistema? 
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('gratis_todosusuarios', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div>                         

                            <div class="form-group">
                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="gratis_todoscliente">Esse serviço será gratuito para todos os CLIENTES da Tríade?  
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('gratis_todoscliente', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div>                         

                            <div class="form-group">
                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="gratis_clientemodulo">Esse serviço será gratuito para os CLIENTES da Tríade que já tem acesso à esse módulo? 
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('gratis_clientemodulo', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div>                         


                            <div class="form-group">
                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="qtdeilimitadagratis">Esse serviço poderá ser solicitado gratuitamente e por vezes ilimitadas?
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('qtdeilimitadagratis', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                                </div> 
                            </div>                         
                            <div class="form-group">
                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for="qtdegratis">Que quantidade esse serviço poderá ser gratuito para o cliente? 
                                </label>
                                <div class="col-md-2 col-sm-4 col-xs-12">
                                    <?= $this->Form->input('qtdegratis', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'min' => 0]); ?>

                                </div> 
                            </div>                         

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tela_id">Caso o cliente solicite esse serviço, qual será a próxima tela para que ele possa informar dados específicos?
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('tela_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $telas, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tempo_execucao">Quantos dias planejados para a execução desse serviço? 
                                </label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <?= $this->Form->input('tempo_execucao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'min' => 0]); ?>

                                </div> 
                            </div>           
                        </div>  
                    </div>        
                </div>        
                <br>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Cadastrar</button>
                        <?= $this->Form->input('tiposervico_id', ["type" => "hidden", 'label' => false, 'value' => $tiposervico->id]); ?>
                        <?= $this->Html->link(__('Voltar'), ['action' => 'index'], ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/js/ajax/servicos.js'); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js'); ?>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js', array('inline' => false)); ?>
<script>
    $(document).ready(function () {
        $(":input").inputmask();
        $("select").select2({placeholder: 'selecione uma opção'});
        $("#user-ids").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione o(s) usuário(s)'});
        $("#empresa-ids").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione a(s) empresa(s)', allowClear: true});
        $("#mes").select2({tokenSeparators: [',', ';'], tags: true, placeholder: 'selecione meses específicos', allowClear: true});
        $('.colorpicker').colorpicker();
        $('#valor-servico,#valor-adicionalmensal').inputmask('decimal', {
            radixPoint: ",",
            groupSeparator: ".",
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
            rightAlign: false,
            onBeforeMask: function (value, opts) {
                return value;
            }});
    });

    function marcarTodos(item) {
        $("#" + item + " option").prop('selected', true);
        $("#" + item).trigger('change');
    }

</script>