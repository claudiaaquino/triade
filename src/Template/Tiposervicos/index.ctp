<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <div style="float:left" class="col-md-8 col-sm-8 col-xs-12">
                    <div class="col-md-2 col-sm-2 col-xs-12"><h2>Serviços</h2></div>
                    <div class="col-md-10 col-sm-10 col-xs-12 form-group pull-right top_search">
                        <form action="/tiposervicos/index" method="POST" >
                            <div class="input-group">
                                <input id="termo-pesquisa" name="termo-pesquisa" class="form-control" placeholder="digite um serviço e clique em pesquisar" type="text">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Pesquisar</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <ul class="nav navbar-right panel_toolbox">
                    <li><?= $this->Html->link(__('  Cadastrar'), ['action' => 'add'], ['class' => "btn btn-dark fa fa-file"]) ?></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Flash->render() ?>
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('nome', 'Serviço') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('Areaservicos.descricao', 'Setor') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('Gruposervicos.descricao', 'Subsetor') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('last_update', 'Ult. Atualização') ?></th>
                                <th scope="col" class="column-title"><?= $this->Paginator->sort('dt_cadastro') ?></th>

                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                                <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cor = 'even';
                            foreach ($tiposervicos as $tiposervico) {
                                ?>                                
                                <tr class="<?= $cor ?> pointer">

                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= $tiposervico->id ?>">
                                    </td>
                                    <td><?= h($tiposervico->nome) ?></td>
                                    <td><?= $tiposervico->gruposervico->areaservico ? $this->Html->link($tiposervico->gruposervico->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $tiposervico->gruposervico->areaservico->id]) : 'Não Informado' ?></td>
                                    <td><?= $tiposervico->has('gruposervico') ? $this->Html->link($tiposervico->gruposervico->descricao, ['controller' => 'Gruposervicos', 'action' => 'view', $tiposervico->gruposervico->id]) : '' ?></td>
                                    <td><?= h($tiposervico->last_update) ?></td>
                                    <td><?= h($tiposervico->dt_cadastro) ?></td>
                                    <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $tiposervico->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $tiposervico->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $tiposervico->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja remover esse registro?', $tiposervico->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            }
                            ?>
                        </tbody>                       
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('próximo') . ' >') ?>
                        </ul>
                        <?= $this->Paginator->counter() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
