<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($tiposervico->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Setor</p>
                        <p><?= $tiposervico->gruposervico->areaservico ? $this->Html->link($tiposervico->gruposervico->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $tiposervico->gruposervico->areaservico->id]) : 'Não Informado' ?></p>

                        <p class="title">Subsetor</p>
                        <p><?= $tiposervico->has('gruposervico') ? $this->Html->link($tiposervico->gruposervico->descricao, ['controller' => 'Gruposervicos', 'action' => 'view', $tiposervico->gruposervico->id]) : 'Não Informado' ?></p>

                        <p class="title">Serviço</p>
                        <p><?= $tiposervico->nome ? $tiposervico->nome : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Descrição') ?></p>
                        <p><?= $tiposervico->descricao ? $tiposervico->descricao : 'Não Informado'; ?></p>

                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $tiposervico->last_update ? $tiposervico->last_update : 'Não houve alteração'; ?></p>


                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $tiposervico->dt_cadastro ? $tiposervico->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Status') ?></p>
                        <p><?= $tiposervico->status ? __('Ativo') : __('Desativado'); ?></p>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($tiposervico->previsaoorcamentos)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Previsão de Orçamentos Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title">Valor do Serviço</th>
                                    <th scope="col"  class="column-title">Valor Adicional na Mensal</th>
                                    <th scope="col"  class="column-title">Prazo para Execução do Serviço</th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tiposervico->previsaoorcamentos as $previsaoorcamentos):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($previsaoorcamentos->id) ?>">
                                        </td>
                                        <td><?= $previsaoorcamentos->valor_servico ? 'R$ ' . $this->Number->format($previsaoorcamentos->valor_servico) : '---' ?></td>
                                        <td><?= $previsaoorcamentos->valor_adicionalmensal ? 'R$ ' . $this->Number->format($previsaoorcamentos->valor_adicionalmensal) : '---' ?></td>
                                        
                                        <td><?= h($previsaoorcamentos->tempo_execucao).' dias úteis' ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Previsaoorcamentos', 'action' => 'view', $previsaoorcamentos->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Previsaoorcamentos', 'action' => 'edit', $previsaoorcamentos->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Previsaoorcamentos', 'action' => 'delete', $previsaoorcamentos->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $previsaoorcamentos->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
       
        <?php if (!empty($tiposervico->telaquestionarios)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Tela de Questionarios Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Telaquestionario Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Assunto Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Estado Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Cidade Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Lei Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tela Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tipoaction Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Tiposervico Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Pergunta') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Resposta') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Exigeconfirmacao') ?></th>
                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Dt Cadastro') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Last Update') ?></th>
                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($tiposervico->telaquestionarios as $telaquestionarios):
                                    ?>
                                    <tr class="<?= $cor ?> pointer">
                                        <td class="a-center ">
                                            <input type="checkbox" class="flat" name="table_records" value="<?= h($telaquestionarios->id) ?>">
                                        </td>
                                        <td><?= h($telaquestionarios->id) ?></td>
                                        <td><?= h($telaquestionarios->telaquestionario_id) ?></td>
                                        <td><?= h($telaquestionarios->assunto_id) ?></td>
                                        <td><?= h($telaquestionarios->estado_id) ?></td>
                                        <td><?= h($telaquestionarios->cidade_id) ?></td>
                                        <td><?= h($telaquestionarios->lei_id) ?></td>
                                        <td><?= h($telaquestionarios->tela_id) ?></td>
                                        <td><?= h($telaquestionarios->tipoaction_id) ?></td>
                                        <td><?= h($telaquestionarios->tiposervico_id) ?></td>
                                        <td><?= h($telaquestionarios->pergunta) ?></td>
                                        <td><?= h($telaquestionarios->resposta) ?></td>
                                        <td><?= h($telaquestionarios->exigeconfirmacao) ?></td>
                                        <td><?= h($telaquestionarios->user_id) ?></td>
                                        <td><?= h($telaquestionarios->dt_cadastro) ?></td>
                                        <td><?= h($telaquestionarios->last_update) ?></td>
                                        <td><?= h($telaquestionarios->status) ?></td>

                                        <td  class=" last">
                                            <div class="btn-group">
                                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Telaquestionarios', 'action' => 'view', $telaquestionarios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'Telaquestionarios', 'action' => 'edit', $telaquestionarios->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                                <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Telaquestionarios', 'action' => 'delete', $telaquestionarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $telaquestionarios->id)]) ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $cor = $cor == 'even' ? 'odd' : 'even';
                                endforeach;
                                ?>
                            </tbody>                       
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


