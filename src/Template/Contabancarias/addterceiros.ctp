<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Insira as informações para <?= __('cadastrar uma Conta bancaria de Cliente ou Fornecedor') ?>. <small>* obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($contabancaria, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cliente_id">Criar nova conta para <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?=
                        $this->Form->radio('terceiros', [
                            ['value' => '0', 'text' => 'Cliente', "class" => "form-control col-md-7 col-xs-12"],
                            ['value' => '1', 'text' => 'Fornecedor', "class" => "form-control col-md-7 col-xs-12"]
                                ]
                        );
                        ?>

                    </div> 
                </div> 


                <div class="form-group cliente">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cliente_id">Cliente <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('cliente_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $clientes, 'empty' => 'selecione um cliente']); ?>
                    </div> 
                </div> 

                <div class="form-group fornecedor">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fornecedor_id">Fornecedor <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('fornecedor_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $fornecedores, 'empty' => 'selecione um fornecedor']); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="banco_id">Banco/Fonte <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('banco_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $bancos, 'empty' => 'selecione o banco/ fonte', 'required' => "required"]); ?>
                    </div> 
                </div> 

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="operacao">Operação <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?= $this->Form->input('operacao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, "placeholder" => "operação"]); ?>

                    </div> 
                </div>                         
                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="agencia">Agência - dígito <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-6">
                        <?= $this->Form->input('agencia', [ "class" => "form-control", 'label' => false, "placeholder" => "agência"]); ?>
                    </div> 
                    <div class="col-md-1 col-sm-1 col-xs-4">
                        <?= $this->Form->input('ag_digito', ["class" => "form-control", 'label' => false, "placeholder" => "dígito"]); ?>
                    </div> 
                </div>                           

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="conta">Conta - dígito <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-6">
                        <?= $this->Form->input('conta', ['type' => 'number', "class" => "form-control", 'label' => false, "placeholder" => "conta"]); ?>
                    </div> 
                    <div class="col-md-1 col-sm-1 col-xs-4">
                        <?= $this->Form->input('co_digito', ['type' => 'number', "class" => "form-control", 'label' => false, "placeholder" => "dígito"]); ?>
                    </div> 
                </div>             

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">Nome do Responsável/Títular <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('nome', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'required' => "required"]); ?>

                    </div> 
                </div>          

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cpf">CPF/CNPJ 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('cpf', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Cadastrar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<script>
    var requiredFields = ['agencia', 'conta', 'operacao'];
    var clearFields = ['agencia', 'ag-digito', 'conta', 'co-digito', 'operacao'];

    function addPropRequired() {
        $.each(requiredFields, function (index, value) {
            $('#' + value).prop('required', true);
        });
    }
    function removePropRequired() {
        $.each(requiredFields, function (index, value) {
            $('#' + value).prop('required', null);
        });
    }
    function clearFieldsInfo() {
        $.each(clearFields, function (index, value) {
            $('#' + value).val('');
        });
    }

    function toggleTerceirosForm() {
        if ($('input[name="terceiros"]:radio:checked').val() == '1') {//fornecedor
            $('.fornecedor').show();
            $('#fornecedor-id').prop('required', true);

            $('#cliente-id').prop('required', null);
            $('.cliente').hide();
            $('#cliente-id').val('');
        } else {
            $('.fornecedor').hide();
            $('#fornecedor-id').prop('required', null);
            $('#fornecedor-id').val('');

            $('.cliente').show();
            $('#cliente-id').prop('required', true);
        }
    }

    function toggleBancoForm() {
        if ($('#banco-id').val() != 1) {// outros bancos
            $('.infobanco').show();
            addPropRequired();
        } else {//caixa da empresa
            $('.infobanco').hide();
            removePropRequired();
            clearFieldsInfo();
        }
    }

    $(document).ready(function () {
        $(":input").inputmask();
        $('.fornecedor').hide();
        $('.cliente').hide();
        $('.infobanco').hide();

        $('#banco-id').change(function () {
            toggleBancoForm();
        });

        $('input[name="terceiros"]:radio').change(function () {
            toggleTerceirosForm();
        });

    });
</script>