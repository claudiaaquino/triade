<?= $this->Html->script('/js/ajax/cidades.js'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Insira as informações para <?= __('atualizar a Conta bancaria') ?>. <small>* obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($contabancaria, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="banco_id">Banco/Fonte <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('banco_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $bancos, 'empty' => 'selecione o banco/ fonte', 'required' => "required"]); ?>
                    </div> 
                </div> 

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="operacao">Operação <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?= $this->Form->input('operacao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, "placeholder" => "operação"]); ?>

                    </div> 
                </div>                         
                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="agencia">Agência - dígito <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-6">
                        <?= $this->Form->input('agencia', [ "class" => "form-control", 'label' => false, "placeholder" => "agência"]); ?>
                    </div> 
                    <div class="col-md-1 col-sm-1 col-xs-4">
                        <?= $this->Form->input('ag_digito', ["class" => "form-control", 'label' => false, "placeholder" => "dígito"]); ?>
                    </div> 
                </div>                           

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="conta">Conta - dígito <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 col-xs-6">
                        <?= $this->Form->input('conta', ['type' => 'number', "class" => "form-control", 'label' => false, "placeholder" => "conta"]); ?>
                    </div> 
                    <div class="col-md-1 col-sm-1 col-xs-4">
                        <?= $this->Form->input('co_digito', ['type' => 'number', "class" => "form-control", 'label' => false, "placeholder" => "dígito"]); ?>
                    </div> 
                </div>                 

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cpf">Saldo em Conta (R$) <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('valorcaixa', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'required' => "required"]); ?>

                    </div> 
                </div>                         
                <div class="ln_solid infobanco"></div>

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">Títular da Conta <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('nome', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cpf">CPF/CNPJ <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('cpf', ["type" => 'text', 'size' => '18', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>           
                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="endereco">Endereço
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('endereco', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_numero">Número 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('end_numero', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_complemento">Complemento 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('end_complemento', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_bairro">Bairro 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('end_bairro', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_cep">CEP 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('end_cep', ["data-inputmask" => "'mask': '99999-999'", "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="estado_id">Estado
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('estado_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $estados, 'empty' => 'selecione o estado']); ?>
                    </div> 
                </div> 

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cidade_id">Cidade
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('cidade_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group infobanco">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_vencimento">Dt. Vencimento do Cartão (se houver)
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('dt_vencimento', ['type' => 'text', "data-inputmask" => "'mask': '99/9999'", "class" => "form-control col-md-7 col-xs-12", 'label' => false, 'empty' => true]); ?>

                    </div> 
                </div>       

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js', array('inline' => false)); ?>
<script>
    var requiredFields = ['agencia', 'conta', 'operacao', 'nome', 'cpf'];
    var clearFields = ['agencia', 'ag-digito', 'conta', 'co-digito', 'operacao', 'nome', 'cpf', 'endereco', 'end-cep', 'end-bairro', 'end-complemento', 'end-numero', 'estado-id', 'cidade-id', 'dt-vencimento'];
    function addPropRequired() {
        $.each(requiredFields, function (index, value) {
            $('#' + value).prop('required', true);
        });
    }
    function removePropRequired() {
        $.each(requiredFields, function (index, value) {
            $('#' + value).prop('required', null);
        });
    }
    function clearFieldsInfo() {
        $.each(clearFields, function (index, value) {
            $('#' + value).val('');
        });
    }
    function toggleForm() {
        if ($('#banco-id').val() != 1) {// outros bancos
            $('.infobanco').show();
            addPropRequired();
        } else {//caixa da empresa
            $('.infobanco').hide();
            removePropRequired();
            clearFieldsInfo();
        }
    }

    function formatCpfCnpj() {
        var query = $('#cpf').val().replace(/[^a-zA-Z 0-9]+/g, '');
        if (query.length == 11) {
            $("#cpf").inputmask("999.999.999-99[9999]");
        }
        if (query.length == 14) {
            $("#cpf").inputmask("99.999.999/9999-99");
        }
    }

    $(document).ready(function () {
        $(":input").inputmask();
        formatCpfCnpj();
        toggleForm();
        $('#banco-id').change(function () {
            toggleForm();
        });


        $('#valorcaixa').inputmask('decimal', {
            radixPoint: ",",
            groupSeparator: ".",
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
            rightAlign: false,
            onBeforeMask: function (value, opts) {
                return value;
            }
        });


        $('#cpf').on('keyup', function (e) {
            formatCpfCnpj();
        });
    });

</script>