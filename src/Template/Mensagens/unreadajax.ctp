<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
    <i class="fa fa-envelope-o"></i>
    <?php if ($count_unread) { ?>
        <span class="badge bg-green"><?= $count_unread ?></span>
    <?php } ?>
</a>
<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
    <?php foreach ($mensagens as $mensagen) { ?>
        <li>
            <a href="<?= $this->request->webroot ?>mensagens/index/<?= h($mensagen->id) ?>">
                <span class="image">
                </span>
                <span>
                    <span><?php
                        $name = explode(' ', $mensagen->user->nome);
                        if ($mensagen->user->empresa) {
                            echo $name[0] . ' (' . $mensagen->user->empresa->razao . ')';
                        } else {
                            echo $name[0];                            
                        }
                        ?></span>
                    <span class="time"><?= h($mensagen->dt_envio) ?></span>
                </span>
                <span class="message">
                    <?= h($mensagen->assunto) ?>
                </span>
            </a>
        </li>
        <?php
    }
    if (!$count_unread) {
        ?>
        <li>
            <div class="text-center">
                <strong>Não há novas mensagens...</strong>
            </div>
        </li>
    <?php } ?>   
    <li>
        <div class="text-center">
            <a href="<?= $this->request->webroot ?>mensagens/">
                <strong>Ver todas as Mensagens</strong>
                <i class="fa fa-angle-right"></i>
            </a>
        </div>
    </li>
</ul>
