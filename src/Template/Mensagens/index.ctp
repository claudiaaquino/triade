<?= $this->Html->css('/vendors/google-code-prettify/bin/prettify.min.css'); ?>
<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<?= $this->Html->css('/vendors/pnotify/dist/pnotify.css'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Caixa de Entrada <small id="count-unread">(<?= $count_unread; ?> Mensagens não lidas)</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><i class="btn btn-primary fa fa-search" data-toggle="modal" data-target="#search-box">  PESQUISAR </i></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3 mail_list_column">
                        <button id="compose" class="btn btn-sm btn-success btn-block" type="button">ESCREVER NOVA MENSAGEM</button>
                        <?= $this->Flash->render() ?>
                        <div class="ln_solid"></div>
                        <?php
                        if ($count > 0) {
                            foreach ($mensagens as $key => $mensagen) {
                                if ($id && ($mensagen->id == $id)) {
                                    $id = $key;
                                }
                                $remetente = explode(' ', $mensagen->user->nome);
                                ?>

                                <a href="#" onclick="showMensagem(<?= $key ?>);">
                                    <div class="mail_list">
                                        <div class="left">
                                            <span id="flag_lido_<?= $mensagen->id; ?>"><?php if (!$mensagen->_matchingData['Mensagensdestinatarios']->dt_leitura) { ?><i class="fa fa-envelope"  data-placement="right" data-toggle="tooltip" data-original-title="Mensagem ainda não foi lida" ></i> <?php } else { ?><i class="fa fa-check-square-o"  data-placement="right" data-toggle="tooltip" data-original-title="Mensagem já foi lida" ></i><?php } ?></span>
                                            <?php if (!$mensagen->todos_empresa && !$mensagen->areaservico_id && !$mensagen->todos_sistema) { ?><i class="fa fa-user" data-placement="right" data-toggle="tooltip" data-original-title="Mensagem direta" ></i> <?php } ?>
                                            <?php if ($mensagen->todos_sistema) { ?><i class="fa fa-globe" data-placement="right" data-toggle="tooltip" data-original-title="Enviada para todos do sistema" ></i> <?php } ?>
                                            <?php if ($mensagen->todos_empresa) { ?><i class="fa fa-university" data-placement="right" data-toggle="tooltip" data-original-title="Enviada para todos da empresa" ></i> <?php } ?>
                                            <?php if ($mensagen->areaservico_id) { ?><i class="fa fa-users"  data-placement="right" data-toggle="tooltip" data-original-title="Enviada para o setor" ></i> <?php } ?>
                                            <?php if ($mensagen->mensagensdocumentos) { ?><i class="fa fa-paperclip"  data-placement="right" data-toggle="tooltip" data-original-title="A mensagem possui anexo" ></i> <?php } ?>
                                            <?php if ($mensagen->respostas) { ?><i class="fa fa-reply"   data-placement="right" data-toggle="tooltip" data-original-title="A mensagem já foi respondida" ></i> <?php } ?>
                                        </div>
                                        <div class="right">
                                            <h3><?= $remetente[0]; ?><small><?= $mensagen->dt_envio; ?></small></h3>
                                            <p><span class="badge">Assunto</span> <?= $mensagen->assunto; ?></p>
                                            <p><?= substr(strip_tags($mensagen->texto), 0, 50); ?>...</p>
                                        </div>
                                    </div>
                                </a>
                                <?php
                            }
                        } else {
                            ?>
                            Você não possui nenhuma mensagem inbox.
                        <?php } ?>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('<<') ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(' >>') ?>
                            </ul>
                        </div>
                        <!--  <div class="btn-group">
                            <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Anterior"><i class="fa fa-chevron-left"></i></button>
                            <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Próxima"><i class="fa fa-chevron-right"></i></button>
                        </div>-->
                    </div>
                    <!-- /MAIL LIST -->

                    <!-- CONTENT MAIL -->
                    <div class="col-sm-9 mail_view" id="box-mensagem" style="display: none;">
                        <div class="inbox-body">
                            <div class="mail_heading row well well-sm">
                                <div class="col-md-8">
                                    <div class="btn-group">
                                        <button class="reply btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i> Responder</button>
                                        <button class="forward btn btn-sm btn-default" type="button"  data-placement="top" data-toggle="tooltip" data-original-title="Encaminhar"><i class="fa fa-share"></i></button>
                                        <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Imprimir"  onclick="window.print();"><i class="fa fa-print"></i></button>
                                        <!--<button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Arquivar"><i class="fa fa-trash-o"></i></button>-->
                                    </div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <p class="date">Enviado em: <span id="dt_envio"></span><span id="dt_leitura"></span></p>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h4 id="assunto_mensagem"></h4>
                                </div>
                                <div class="sender-info">
                                    <div class="row">
                                        <div class="col-md-12">
                                            De <strong id="nome_usuario"></strong>
                                            (<span id="nome_empresa"></span>) para
                                            <strong>mim</strong>
                                            <a class="sender-dropdown" href="javascript:void(0);" onclick="$('#userdestinatarios').slideToggle();"><i class="fa fa-chevron-down"></i></a>
                                            <strong id="userdestinatarios" style="display:none"></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="view-mail">
                                </br>
                                <p  id="texto_mensagem"></p>
                            </div>
                            <div class="clearfix ln_solid"></div>
                            <div class="attachment" id="box-anexos" style="display:none;">
                                <p>
                                    <span><i class="fa fa-paperclip"></i>   
                                        <span id="num_anexos"></span> anexo(s) </span>
                                    <!--<a href="#">Download all attachments</a>--> 
                                </p>
                                <ul id="list-anexos">           
                                </ul>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 well well-sm">
                                <div class="btn-group">
                                    <button class="reply btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i> Responder</button>
                                    <button class="forward btn btn-sm btn-default" type="button"  data-placement="top" data-toggle="tooltip" data-original-title="Encaminhar"><i class="fa fa-share"></i></button>
                                    <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Imprimir"   onclick="window.print();"><i class="fa fa-print"></i></button>
                                    <!--<button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Arquivar"><i class="fa fa-trash-o"></i></button>-->
                                </div>
                            </div>
                        </div>
                        <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                    </div>
                    <!-- /CONTENT MAIL -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- compose -->
<div class="compose col-md-6 col-xs-12"  style="z-index: 4;">
    <form method="post" action="/mensagens/add" enctype="multipart/form-data" class="form-horizontal form-label-left">

        <div class="compose-header">
            <span class="tipomensagem">Escrever nova Mensagem</span>
            <button type="button" class="close compose-close">
                <span>×</span>
            </button>
        </div>

        <div class="compose-body">
            <div id="alerts"></div>

            <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">

                <?php if (!$this->request->isMobile()) { ?> 
                    <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a data-edit="fontSize 5">
                                    <p style="font-size:17px">Huge</p>
                                </a>
                            </li>
                            <li>
                                <a data-edit="fontSize 3">
                                    <p style="font-size:14px">Normal</p>
                                </a>
                            </li>
                            <li>
                                <a data-edit="fontSize 1">
                                    <p style="font-size:11px">Small</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                <?php } ?> 
                <div class="btn-group">
                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Tipo de Fonte"><i class="fa fa-font"></i>&nbsp;<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a></li>
                        <li><a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a></li>
                        <li><a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a></li>
                        <li><a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a></li>
                    </ul>
                </div>

                <div class="btn-group">
                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Tipo de Fonte"><i class="fa fa-list"></i>&nbsp;<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a></li>
                        <li><a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a></li>
                        <li><a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a></li>
                        <li><a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a></li>
                    </ul>
                </div>

                <div class="btn-group">
                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Tipo de Fonte"><i class="fa fa-align-left"></i>&nbsp;<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                        <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                        <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                        <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                    </ul>
                </div>

                <?php if (!$this->request->isMobile()) { ?> 
                    <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                        <div class="dropdown-menu input-append">
                            <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                            <button class="btn" type="button">Add</button>
                        </div>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                        <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                    </div>

                    <?php
                }
                ?> 
                <div class="btn-group">
                    <a class="btn btn-anexo"  title="Anexos"><i class="fa fa-paperclip"></i>&nbsp;<b class="caret"></b></a>
                </div>
                <div class="btn-group">
                    <a class="btn btn-destinatarios" title="Destinatários"><i class="fa fa-cogs"></i>&nbsp;<b class="caret"></b></a>
                </div>
                <?php if (!$this->request->isMobile()) { ?> 
                    <br>
                    <div class="clearfix ln_solid"></div>
                <?php } ?> 
                <div id="anexos" class="col-md-12 col-sm-12 col-xs-12">
                    <div id="attachment-fields">

                        <!--                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="anexar">
                                            Anexar arquivos <i class="fa fa-question-circle" data-placement="right" data-toggle="tooltip" data-original-title="Cada anexo deve ser adicionado separadamente"></i>
                                        </label>-->
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-4" for="setor">Setor  
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8">
                                <?= $this->Form->input('setor_id', [ 'options' => $areaservicos, "class" => "form-control col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%", 'empty' => 'selecione uma opção']); ?>
                            </div>
                            <div class="grupodocumento-id">
                                <label class="control-label col-md-2 col-sm-2 col-xs-4" for="grupo">Grupo 
                                </label>
                                <div class="col-md-4 col-sm-4 col-xs-8">
                                    <?= $this->Form->input('grupodocumento_id', [ "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%"]); ?>
                                </div>
                            </div>
                        </div>                
                        <div class="form-group document-details">
                            <label class="control-label col-md-2 col-sm-2 col-xs-4" for="tipo">Documento 
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8">
                                <?= $this->Form->input('tipodocumento_id', [ "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%"]); ?>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-4" for="funcionario">Funcionário<i class="fa fa-question-circle" data-placement="top" data-toggle="tooltip" data-original-title="Selecione SOMENTE SE o documento for referente à esse funcionário."></i>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8">
                                <?= $this->Form->input('funcionario_id', ['options' => $funcionarios, "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'empty' => 'selecione (se necessário)', "style" => "width: 100%"]); ?>
                            </div>
                        </div>                      
                        <div class="form-group document-details">
                            <label class="control-label col-md-3 col-sm-3 col-xs-4" for="descricao">Descrição/Observação
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-8">
                                <?= $this->Form->input('descricao', [ "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>
                            </div>
                        </div>  
                        <div id="fileField" class="col-md-12 col-sm-12 col-xs-12">
                            <div id="pathFile"  class="col-md-10 col-sm-10 col-xs-8">
                                <?= $this->Form->file('file[]', ['label' => false, 'key' => 0, 'class' => 'file']) ?>      
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-4">
                                <button class="addFile btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Adicionar</button>
                            </div>
                        </div>
                    </div>
                    <div class="btn-group">
                        <a class="btn btn-attach-view"  title="Anexos"><i class="fa fa-paperclip"></i>&nbsp; <span id="action-all-attach">Ver</span> anexos &nbsp;<b class="caret"></b></a>
                    </div>
                    <div class="form-group files"  style="display: none;">
                        <div id="queue">                        
                        </div>
                    </div>    
                </div>
                <div id="destinatarios"  class="col-md-12 col-sm-12 col-xs-12">
                    <?php if ($admin) { ?>
                        <div class="config-msg <?= $this->request->isMobile() ? '' : 'well well-sm'; ?>">
                            <div class="form-group  config-msg ">
                                <label class="control-label col-md-5 col-sm-5 col-xs-10" for="somente_admin">Somente para o admin da empresa
                                </label>
                                <div class="col-md-1 col-sm-1 col-xs-2">
                                    <input type="radio" class="form-control flat" name="sendoptions" id="somente_admin" value="somente_admin">
                                </div> 
                                <label class="control-label col-md-5 col-sm-5 col-xs-10" for="todos_empresa">Para todos os usuários da empresa
                                </label>
                                <div class="col-md-1 col-sm-1 col-xs-2">
                                    <input type="radio" class="form-control flat" name="sendoptions" id="todos_empresa" value="todos_empresa">
                                </div> 
                            </div> 
                            <div class="form-group  config-msg">
                                <label class="control-label col-md-5 col-sm-5 col-xs-10" for="todos_sistema">Para todos os usuários do sistema
                                </label>
                                <div class="col-md-1 col-sm-1 col-xs-2">
                                    <input type="radio" class="form-control flat" name="sendoptions" id="todos_sistema" value="todos_sistema">
                                </div> 
                                <label class="control-label col-md-5 col-sm-5 col-xs-10" for="todos_triade">Para todos os usuários da Tríade
                                </label>
                                <div class="col-md-1 col-sm-1 col-xs-2">
                                    <input type="radio" class="form-control flat" name="sendoptions" id="todos_triade" value="todos_triade">
                                </div> 
                            </div> 
                        </div>                        
                    <?php } ?> 

                    <div class="form-group config-msg">
                        <label class="control-label col-md-1 col-sm-1 col-xs-3" for="empresa_id">Empresa
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-9">
                            <?= $this->Form->input('empresa_id', ['options' => $empresas, "class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%", 'empty' => 'selecione a empresa']); ?>
                        </div> 
                        <label class="control-label col-md-1 col-sm-1 col-xs-3" for="areaservico_id">Setor 
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-9">
                            <?= $this->Form->input('areaservico_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, "style" => "width: 100%", 'empty' => 'selecione um setor']); ?>
                        </div> 
                    </div> 
                    <?php if ($admin) { ?>
                        <div class="form-group">
                            <label class="control-label  col-md-1 col-sm-1 col-xs-3 " for="user_id">Para<i class="fa fa-question-circle" data-placement="right" data-toggle="tooltip" data-original-title="Se nenhum usuário for selecionado, o sistema enviará seguindo apenas os critérios selecionados acima."></i>
                            </label>
                            <div class="col-md-11 col-sm-11 col-xs-9">
                                <?= $this->Form->input('user_ids', ['options' => $users, "class" => "select2 form-control", 'label' => false, "multiple" => "multiple", "style" => "width: 100%", 'placeholder' => 'click para selecionar os destinatários']); ?>

                            </div> 
                        </div> 
                    <?php } ?> 
                    <div class="form-group  config-msg">
                        <label class="control-label col-md-1 col-sm-1 col-xs-3" for="assunto">Assunto
                        </label>
                        <div class="col-md-11 col-sm-11 col-xs-9">
                            <?= $this->Form->input('assunto', ["class" => "form-control", 'label' => false, "style" => "width: 100%", 'required' => "required"]); ?>

                        </div> 
                    </div>      
                </div>
            </div>

            <div id="editor" class="editor-wrapper" placeholder="Digite a sua mensagem aqui."></div>
        </div>

        <div class="compose-footer">
            <button id="send" class="btn btn-sm btn-success" type="submit"><i class="fa fa-paper-plane"></i><span class="btn-txt-submit"> Enviar  </span></button>
            <button id="cancel" class="btn btn-sm btn-primary" type="button">Cancelar</button>
        </div>
        <span class="other-fields">
            <?= $this->Form->input('texto', ["type" => "hidden", 'label' => false]); ?>
            <?= $this->Form->input('mensagen_id', ["type" => "hidden", 'label' => false]); ?>
            <?= $this->Form->input('encaminhada', ["type" => "hidden", 'label' => false, 'value' => 'false']); ?>
        </span>
    </form>
</div>
<!-- /compose -->


<div id="search-box" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Pesquisar email" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal formSearch" action="/mensagens/index/" data-form="formSearch" role="form" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Pesquisar email</h4>
                </div>
                <div class="modal-body">
                    <div id="principal">         
                        <?php if ($admin) { ?>
                            <div class="form-group">
                                <label class="control-label label-input col-md-3 col-sm-3 col-xs-4" for="areaservico_id">Empresa </label>
                                <div class="col-md-9 col-sm-9 col-xs-8">
                                    <?= $this->Form->input('empresa_search', ["class" => "form-control select2", 'label' => false, 'options' => $empresas, "multiple" => "multiple", "style" => "width: 100%"]); ?>
                                </div> 
                            </div>
                        <?php } ?>

                        <div class="form-group">
                            <label class="control-label label-input col-md-3 col-sm-3 col-xs-4" for="user_ids">Meus contatos</label>
                            <div class="col-md-9 col-sm-9 col-xs-8">
                                <?= $this->Form->input('remetentes_search', ['options' => $users, "class" => "select2 form-control", 'label' => false, "multiple" => "multiple", "style" => "width: 100%"]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label label-input col-md-3 col-sm-3 col-xs-4" for="assunto">Assunto</label>
                            <div class="col-md-9 col-sm-9 col-xs-8">
                                <?= $this->Form->input('assunto_search', ["class" => "form-control", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="periodo">Data de envio entre
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-5">
                                <?= $this->Form->input('dt_inicio', ["class" => "form-control col-md-4 col-xs-4", 'label' => false, 'placeholder' => 'data inicial da busca', 'data-inputmask' => "'mask': '99/99/9999'"]); ?> 
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-5">
                                <?= $this->Form->input('dt_final', ["class" => "form-control col-md-4 col-xs-4", 'label' => false, 'placeholder' => 'data final da busca', 'data-inputmask' => "'mask': '99/99/9999'"]); ?>
                            </div>
                        </div>     
                        <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btnSearch margin-zero">Pesquisar</button>
                    <button type="button" class="btn btn-default antoclose2 margin-zero" data-dismiss="modal">Fechar</button>

                </div>
            </form>
        </div>
    </div>
</div>

<?= $this->Html->script('/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js'); ?>
<?= $this->Html->script('/vendors/jquery.hotkeys/jquery.hotkeys.js'); ?>
<?= $this->Html->script('/vendors/google-code-prettify/src/prettify.js'); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/vendors/pnotify/dist/pnotify.js'); ?>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js'); ?>
<?= $this->Html->script('/js/ajax/mensagens.js'); ?>
<?= $this->Html->script('/js/ajax/documentos.js'); ?>
<?= $this->Html->script('/js/ajax/empresas.js'); ?>
<?= $this->Html->script('/js/date.js'); ?>
<script>
    var keys_anexos = 0;


    $(document).ready(function () {
<?php if ($count > 0) { ?>
            mensagens = <?= json_encode($mensagens); ?>;
            count_unread = <?= $count_unread; ?>;
<?php } ?>
        refreshUnread();

        //      exibe a mensagem que se deseja abrir
<?php if ($id) { ?>
            if (mensagens[<?= $id; ?>])
                showMensagem(<?= $id; ?>);
<?php } ?>

        $(":input").inputmask();
        $("#anexos").hide();
        $("#user-ids").select2({multiple: true, tokenSeparators: [',', ' '], tags: true, allowClear: true});
        $("#empresa-id").select2({minimumResultsForSearch: 3});
        $("#areaservico-id").select2({minimumResultsForSearch: 10});
        $("#setor-id").select2({minimumResultsForSearch: 10});
        $("#grupodocumento-id").select2();
        $("#tipodocumento-id").select2();
        $("#funcionario-id").select2({placeholder: 'selecione (se necessário)', minimumResultsForSearch: 10});
        $('#editor').wysiwyg();
        window.prettyPrint;
        prettyPrint();

        /*Filtros da pesquisa*/
        $("#empresa-search").select2({multiple: true, tokenSeparators: [',', ' '], allowClear: true/*,placeholder:'click para selecionar uma ou mais empresas'*/});
        $("#remetentes-search").select2({multiple: true, tokenSeparators: [',', ' '], allowClear: true/*,placeholder:'click para selecionar uma ou mais remetentes'*/});
        /*FIM  - Filtros da pesquisa*/



        /*----------------------------EVENTS---------------------------------------*/
        $('#compose, .compose-close, #cancel').click(function (e) {
            e.preventDefault();
            clearComposeBox();
            $('.compose').slideToggle();
            $('.config-msg').show();
            $('#editor').addClass('placeholderText');
            $('#editor').html('Digite a sua mensagem aqui.');
            $('.tipomensagem').html('Escrever nova Mensagem');
            $('.btn-destinatarios').show();
            $('#encaminhada').val('false');
        });

        $('.reply').click(function (e) {
            e.preventDefault();
            clearComposeBox();
            $('.config-msg').hide();
            $('.compose').slideDown();
            $('#editor').addClass('placeholderText');
            $('#editor').html('Digite a sua mensagem aqui.');
            $('.tipomensagem').html('Escrever Resposta');
            $('#encaminhada').val('false');
            $('#assunto').val(mensagens[current_key].assunto);
            if (mensagens[current_key].mensagen_id) {
                $('#mensagen-id').val(mensagens[current_key].mensagen_id);
            } else {
                $('#mensagen-id').val(mensagens[current_key].id);
            }

            if (mensagens[current_key].user.empresa) {
                $('#empresa-id').val(mensagens[current_key].user.empresa.id);
                $("#empresa-id").select2("destroy");
                $("#empresa-id").select2();

                if ($('#user-ids').length) {
                    preencheUsuariosEmpresa(mensagens[current_key].user.empresa.id, null, mensagens[current_key].user.id);
                } else {
                    preencheSetoresEmpresa(mensagens[current_key].user.empresa.id, mensagens[current_key].areaservico_id);
                    $('.btn-destinatarios').hide();
                }
            } else {
                preencheUsuariosEmpresa(null, null, mensagens[current_key].user.id);
            }
        });

        $('.forward').click(function (e) {
            e.preventDefault();
            clearComposeBox();
            $('.config-msg').show();
            $('.compose').slideDown();
            $('#editor').addClass('placeholderText');
            $('#editor').html('Digite a sua mensagem aqui.');
            $('.tipomensagem').html('Encaminhar Mensagem');
            $('#assunto').val('ENC: ' + mensagens[current_key].assunto);
            $('#encaminhada').val(mensagens[current_key].id);
            if (mensagens[current_key].mensagen_id) {
                $('#mensagen-id').val(mensagens[current_key].mensagen_id);
            } else {
                $('#mensagen-id').val(mensagens[current_key].id);
            }
        });

        $('.addFile').click(function (e) {
            e.preventDefault();
            if (!$('#setor-id').val() || !$('#grupodocumento-id').val() || !$('#tipodocumento-id').val()) {
                alert('Os campos Setor/Grupo/Tipo de documentos devem ser preenchidos para poder adicionar o arquivo.');
            } else {
                addAttachment();
            }
        });

        $('.btn-anexo').click(function (e) {
            $('#destinatarios').slideToggle();
            if ($('#setor-id').val() == '' || $('#setor-id').val() == null) {
                $('.document-details').hide();
                $('.grupodocumento-id').hide();
            } else {
                if ($('#grupodocumento-id').val() == '' || $('#grupodocumento-id').val() == null) {
                    $('.grupodocumento-id').show();
                    $('.document-details').hide();
                } else {
                    $('.document-details').show();
                    $('.grupodocumento-id').show();
                }
            }
            $('#anexos').slideToggle();
        });

        $('.btn-destinatarios').click(function (e) {
            $('#destinatarios').slideToggle();
            $('#anexos').slideToggle();
        });

        $('.btn-attach-view').click(function (e) {
            $('.files').slideToggle();
            if ($('#action-all-attach').html() == 'Ver') {
                $('#action-all-attach').html('Ocultar');
            } else {
                $('#action-all-attach').html('Ver');
            }
        });

        $('#send').click(function (e) {

            if ($('#mensagen-id').val() === '') {
                var sendoptions = $('input[type=radio]:checked').val();
                var users = $('#user-ids option:selected').val();


                if (!$('#empresa-id').val() && sendoptions != 'todos_triade' && sendoptions != 'todos_sistema') {
                    alert('Se você não selecionar nenhuma empresa na lista, você precisa informar pelo menos se a mensagem é para todos os usuários do sistema ou se é para todos os usuários da Tríade.');
                    return false;
                }
                if ($('#empresa-id').val() && !sendoptions && !$('#areaservico-id').val() && !users) {
                    alert('Você deve selecionar pelo menos alguma opção entre:\n - Algum setor da empresa \n - Marcar alguma opção geral \n - Pelo menos um usuário dessa empresa \n');
                    return false;
                }
                if (!$('#assunto').val()) {
                    alert('Você deve preencher o assunto');
                    return false;
                }
            }

            $('#texto').val($('#editor').html());
            $('.btn-txt-submit').html(' Aguarde. Enviando ...');
            return true;
        });

        $('select').on('select2:open', function (e) {
            $('.select2-search input').prop('focus', false);
        });

    });
    function clearComposeBox() {
        var fields = ['mensagen-id', 'setor-id', 'grupodocumento-id', 'tipodocumento-id', 'empresa-id', 'funcionario-id', 'descricao', 'areaservico-id', 'user-ids', 'assunto'];
        var radios = ['todos_triade', 'todos_sistema', 'todos_empresa', 'somente_admin'];
        var selects = ['grupodocumento-id', 'tipodocumento-id', 'areaservico-id', 'user-ids'];
        var selects2 = ['setor-id', 'grupodocumento-id', 'tipodocumento-id', 'empresa-id', 'funcionario-id', 'areaservico-id', 'user-ids'];
        //limpar os campos já preenchidos
        $('#editor').html('');
        clearFieldsInfo(fields);
        clearCheckedRadios(radios);
        clearSelectOptions(selects);
        refreshSelect2(selects2);
    }
    function addAttachment() {
        new PNotify({
            text: 'Arquivo adicionado à lista de anexos',
            type: 'success',
            styling: 'bootstrap3',
            animate_speed: 'fast'
        });

        getPropertiesFile();
        additemListFiles();
        hideLatestAttachmentField();
        newAttachmentField();
    }
    function getPropertiesFile() {
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='tipo_documentos[" + keys_anexos + "]' value='" + $('#tipodocumento-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='grupo_documentos[" + keys_anexos + "]' value='" + $('#grupodocumento-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='area_servicos[" + keys_anexos + "]' value='" + $('#setor-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='funcionario[" + keys_anexos + "]' value='" + $('#funcionario-id').val() + "'>");
        $('#pathFile').append("<input type='hidden' key='" + keys_anexos + "' name='descricoes[" + keys_anexos + "]' value='" + $('#descricao').val() + "'>");
    }
    function additemListFiles() {
        $('#queue').append('<div class="itemqueue col-md-4 col-sm-4 col-xs-12 well well-sm"  style="padding: 0;" item="' + keys_anexos + '">\
                            <div id="descFile" class="col-md-10 col-sm-10 col-xs-10">' +
                $('#setor-id option:selected').text() + '/ ' + $('#grupodocumento-id option:selected').text() + '/ ' + $('#tipodocumento-id option:selected').text()
                + '</div>\
                <div class="removeFile col-md-2 col-sm-2 col-xs-2">\
                    <button class="remove btn btn-sm btn-danger" type="button" onclick="removeItemListFiles(this,' + keys_anexos + ');"><i class="fa fa-trash"></i></button>\
                </div></div>');
    }
    function hideLatestAttachmentField() {
        $('.file').hide();
    }
    function newAttachmentField() {
        keys_anexos++;
        $('#pathFile').append("<input type='file' class='file' key='" + keys_anexos + "' name='file[]'>");
    }
    function removeItemListFiles(item, key) {
        $(item).parent().parent().remove();
        $(':input[key=' + key + ']').remove();
    }



</script>