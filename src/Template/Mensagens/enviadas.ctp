<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Mensagens Enviadas <small id="count-unread">(<?= $count; ?>)</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><i class="btn btn-primary fa fa-search" data-toggle="modal" data-target="#search-box">  PESQUISAR </i></li>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-3 mail_list_column">                       
                        <?php
                        if ($count > 0) {
                            foreach ($mensagens as $key => $mensagen) {
                                $destinatario = explode(' ', $mensagen->mensagensdestinatarios[0]->user->nome);
                                ?>
                                <a href="#" onclick="showSentMensagem(<?= $key ?>);">
                                    <div class="mail_list">
                                        <div class="left">
                                            <?php if (!$mensagen->todos_empresa && !$mensagen->areaservico_id && !$mensagen->todos_sistema) { ?><i class="fa fa-user" data-placement="right" data-toggle="tooltip" data-original-title="Mensagem direta" ></i> <?php } ?>
                                            <?php if ($mensagen->todos_sistema) { ?><i class="fa fa-globe" data-placement="right" data-toggle="tooltip" data-original-title="Enviada para todos do sistema" ></i> <?php } ?>
                                            <?php if ($mensagen->todos_empresa) { ?><i class="fa fa-university" data-placement="right" data-toggle="tooltip" data-original-title="Enviada para todos da empresa" ></i> <?php } ?>
                                            <?php if ($mensagen->areaservico_id) { ?><i class="fa fa-users"  data-placement="right" data-toggle="tooltip" data-original-title="Enviada para o setor" ></i> <?php } ?>
                                            <?php if ($mensagen->mensagensdocumentos) { ?><i class="fa fa-paperclip"  data-placement="right" data-toggle="tooltip" data-original-title="A mensagem possui anexo" ></i> <?php } ?>
                                            <?php if ($mensagen->respostas) { ?><i class="fa fa-reply"   data-placement="right" data-toggle="tooltip" data-original-title="A mensagem já foi respondida" ></i> <?php } ?>
                                        </div>
                                        <div class="right">
                                            <h3>Para <?= $destinatario[0]; ?><small><?= $mensagen->dt_envio; ?></small></h3>
                                            <p><span class="badge">Assunto</span> <?= $mensagen->assunto; ?></p>
                                            <p><?= substr(strip_tags($mensagen->texto), 0, 50); ?>...</p>
                                        </div>
                                    </div>
                                </a>
                                <?php
                            }
                        } else {
                            ?>
                            Você não possui mensagens enviadas.
                        <?php } ?>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('<<') ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(' >>') ?>
                            </ul>
                        </div>
                    </div>
                    <!-- /MAIL LIST -->

                    <!-- CONTENT MAIL -->
                    <div class="col-sm-9 mail_view" id="box-mensagem" style="display: none;">
                        <div class="inbox-body">
                            <div class="mail_heading row well well-sm">
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <h4 id="assunto_mensagem"></h4>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 text-right">
                                    <p class="date"> <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Imprimir"  onclick="window.print();"><i class="fa fa-print"></i></button>Enviado em: <span id="dt_envio"></span><span id="dt_leitura"></span></p>
                                </div>
                                <div class="sender-info">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a class="sender-dropdown" href="javascript:void(0);" onclick="$('#nome_usuario').slideToggle();">Enviada por <strong>mim</strong> para <i class="fa fa-chevron-down"></i></a>
                                            <strong id="nome_usuario" style="display:none"></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="view-mail">
                                </br>
                                <p  id="texto_mensagem"></p>
                            </div>
                            <div class="clearfix ln_solid"></div>
                            <div class="attachment" id="box-anexos" style="display:none;">
                                <p>
                                    <span><i class="fa fa-paperclip"></i>   
                                        <span id="num_anexos"></span> anexo(s) </span>
                                    <!--<a href="#">Download all attachments</a>--> 
                                </p>
                                <ul id="list-anexos">           
                                </ul>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 well well-sm">
                                <div class="btn-group">
                                    <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Imprimir"   onclick="window.print();"><i class="fa fa-print"></i></button>
                                </div>
                            </div>
                        </div>
                        <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                    </div>
                    <!-- /CONTENT MAIL -->
                </div>
            </div>
        </div>
    </div>
</div>

<div id="search-box" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Pesquisar email" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal formSearch" action="/mensagens/enviadas/" data-form="formSearch" role="form" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Pesquisar email</h4>
                </div>
                <div class="modal-body">
                    <div id="principal">         
                        <?php if ($admin) { ?>
                            <div class="form-group">
                                <label class="control-label label-input col-md-3 col-sm-3 col-xs-4" for="areaservico_id">Empresa </label>
                                <div class="col-md-9 col-sm-9 col-xs-8">
                                    <?= $this->Form->input('empresa_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'options' => $empresas, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                            </div>
                            <div class="form-group">
                                <label class="control-label label-input col-md-3 col-sm-3 col-xs-4" style="padding-top: 10px;" for="areaservico_id">Setor </label>
                                <div class="col-md-9 col-sm-9 col-xs-8" style="padding-top: 5px;" >
                                    <?= $this->Form->input('areaservico_id', ["class" => "form-control select2 col-md-7 col-xs-12", 'label' => false, 'empty' => true, "style" => "width: 100%"]); ?>
                                </div> 
                            </div>
                            <div class="form-group">
                                <label class="control-label label-input col-md-3 col-sm-3 col-xs-4" for="user_ids">Destinatário(s)</label>
                                <div class="col-md-9 col-sm-9 col-xs-8">
                                    <?= $this->Form->input('user_ids', ['options' => $users, "class" => "select2 form-control", 'label' => false, "multiple" => "multiple", "style" => "width: 100%", 'placeholder' => 'click para selecionar os usuários']); ?>
                                </div>
                            </div>
                        <?php } else { ?>

                            <div class="form-group">
                                <label class="control-label label-input col-md-3 col-sm-3 col-xs-4" for="user_ids">Meus contatos</label>
                                <div class="col-md-9 col-sm-9 col-xs-8">
                                    <?= $this->Form->input('user_ids', ['options' => $users, "class" => "select2 form-control", 'label' => false, "multiple" => "multiple", "style" => "width: 100%", 'placeholder' => 'click para selecionar os usuários']); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="control-label label-input col-md-3 col-sm-3 col-xs-4" for="assunto">Assunto</label>
                            <div class="col-md-9 col-sm-9 col-xs-8">
                                <?= $this->Form->input('assunto', ["class" => "form-control", 'label' => false]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="periodo">Data de recebimento entre
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-5">
                                <?= $this->Form->input('dt_inicio', ["class" => "form-control col-md-4 col-xs-4", 'label' => false, 'placeholder' => 'data inicial da busca', 'data-inputmask' => "'mask': '99/99/9999'"]); ?> 
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-5">
                                <?= $this->Form->input('dt_final', ["class" => "form-control col-md-4 col-xs-4", 'label' => false, 'placeholder' => 'data final da busca', 'data-inputmask' => "'mask': '99/99/9999'"]); ?>
                            </div>
                        </div>     

                        <!--                        <div class="form-group">
                                                    <label class="control-label label-input col-md-4 col-sm-4 col-xs-9" for="has_anexo">O email possui anexo?</label>
                                                    <div class="col-md-3 col-sm-3 col-xs-3" style="padding-top: 5px;">
                        <?= ''// $this->Form->checkbox('has_anexo', ["class" => "form-control flat", 'label' => false, 'empty' => true]); ?>
                        
                                                    </div>  
                                                </div> -->
                        <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btnSearch margin-zero">Pesquisar</button>
                    <button type="button" class="btn btn-default antoclose2 margin-zero" data-dismiss="modal">Fechar</button>

                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js'); ?>
<?= $this->Html->script('/js/ajax/empresas.js'); ?>
<?= $this->Html->script('/js/ajax/mensagens.js'); ?>
<?= $this->Html->script('/js/date.js'); ?>
<script>
<?php if ($count > 0) { ?>
        mensagens = <?= json_encode($mensagens); ?>;
<?php } ?>

    $(document).ready(function () {
        $(":input").inputmask();
        $("select").select2({minimumResultsForSearch: 3});
    });
</script>
