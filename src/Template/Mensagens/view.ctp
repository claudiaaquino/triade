<div class="page-title">
    <div class="title_left">
        <h3><?= __('Mensagens') ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> <?= h($mensagen->assunto) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('Empresa') ?></p>
                                    <p><?= $mensagen->has('empresa') ? $this->Html->link($mensagen->empresa->razao, ['controller' => 'Empresas', 'action' => 'view', $mensagen->empresa->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Areaservico') ?></p>
                                    <p><?= $mensagen->has('areaservico') ? $this->Html->link($mensagen->areaservico->descricao, ['controller' => 'Areaservicos', 'action' => 'view', $mensagen->areaservico->id]) : '' ?></p>

                                                                                                                    <p class="title"><?= __('Assunto') ?></p>
                                    <p><?= h($mensagen->assunto) ?></p>
                                   
                                                                                                                    <p class="title"><?= __('User') ?></p>
                                    <p><?= $mensagen->has('user') ? $this->Html->link($mensagen->user->nome, ['controller' => 'Users', 'action' => 'view', $mensagen->user->id]) : '' ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $this->Number->format($mensagen->id) ?></p>

                                                                                                                                
                        <p class="title"><?= __('Dt Envio') ?></p>
                                <p><?= h($mensagen->dt_envio) ?></p>

                                                    
                        <p class="title"><?= __('Dt Leitura') ?></p>
                                <p><?= h($mensagen->dt_leitura) ?></p>

                                                                                                                                
                        <p class="title"><?= __('Has Destinatarios') ?></p>
                                <p><?= $mensagen->has_destinatarios ? __('Ativo') : __('Desativado'); ?></p>

                                                    
                        <p class="title"><?= __('Has Anexo') ?></p>
                                <p><?= $mensagen->has_anexo ? __('Ativo') : __('Desativado'); ?></p>

                                                                                                                                <p class="title"><?= __('Texto') ?></p>
                                <p>  <?= $this->Text->autoParagraph(h($mensagen->texto)); ?></p>
                                                                            <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <?= $this->Html->link("Voltar", ['action' => 'index'], ['class' => "btn btn-default"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <?php if (!empty($mensagen->mensagensdestinatarios)): ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="green"><i class="fa fa-file"></i> Mensagensdestinatarios Vínculados </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                                                    <th scope="col"  class="column-title"><?= __('Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Mensagen Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('User Id') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Dt Leitura') ?></th>
                                                                    <th scope="col"  class="column-title"><?= __('Status') ?></th>
                                                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                    <th scope="col" class="column-title no-link"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cor = 'even';
                                foreach ($mensagen->mensagensdestinatarios as $mensagensdestinatarios):
                                ?>
                                <tr class="<?= $cor ?> pointer">
                                    <td class="a-center ">
                                        <input type="checkbox" class="flat" name="table_records" value="<?= h($mensagensdestinatarios->id) ?>">
                                    </td>
                                                                    <td><?= h($mensagensdestinatarios->id) ?></td>
                                                                    <td><?= h($mensagensdestinatarios->mensagen_id) ?></td>
                                                                    <td><?= h($mensagensdestinatarios->user_id) ?></td>
                                                                    <td><?= h($mensagensdestinatarios->dt_leitura) ?></td>
                                                                    <td><?= h($mensagensdestinatarios->status) ?></td>
                                                                                          
                                <td  class=" last">
                                        <div class="btn-group">
                                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Mensagensdestinatarios', 'action' => 'view', $mensagensdestinatarios->id], ['class' => "btn btn-primary btn-xs"]) ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'Mensagensdestinatarios', 'action' => 'edit', $mensagensdestinatarios->id], [ 'class' => "btn btn-info btn-xs"]) ?>
                                            <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Mensagensdestinatarios', 'action' => 'delete', $mensagensdestinatarios->id], ['class' => "btn btn-danger btn-xs", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $mensagensdestinatarios->id)]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $cor = $cor == 'even' ? 'odd' : 'even';
                            endforeach;
                            ?>
                        </tbody>                       
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
            </div>
</div>


