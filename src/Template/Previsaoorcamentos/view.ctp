<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i> Orçamento do Serviço - <?= h($previsaoorcamento->tiposervico->descricao) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                        <p class="title">Serviço</p>
                        <p><?= $previsaoorcamento->has('tiposervico') ? $this->Html->link($previsaoorcamento->tiposervico->descricao, ['controller' => 'Tiposervicos', 'action' => 'view', $previsaoorcamento->tiposervico->id]) : 'Não Informado' ?></p>

                        <p class="title">Forma de Pagamento do Serviço</p>
                        <p><?= $previsaoorcamento->has('formaspagamentoservico') ? $this->Html->link($previsaoorcamento->formaspagamentoservico->descricao, ['controller' => 'Formaspagamentoservicos', 'action' => 'view', $previsaoorcamento->formaspagamentoservico->id]) : 'Não Informado' ?></p>

                        <p class="title">Dias planejados para a execução desse serviço</p>
                        <p><?= $previsaoorcamento->tempo_execucao ? $this->Number->format($previsaoorcamento->tempo_execucao) : 'Não Informado'; ?></p>


                        <p class="title">Valor do Serviço (no ato do contrato)</p>
                        <p><?= $previsaoorcamento->valor_servico ? 'R$ ' . $this->Number->format($previsaoorcamento->valor_servico) : 'Não Informado'; ?></p>


                        <p class="title">Valor do Serviço (adicional na mensal do cliente)</p>
                        <p><?= $previsaoorcamento->valor_adicionalmensal ? 'R$ ' . $this->Number->format($previsaoorcamento->valor_adicionalmensal) : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Dt Cadastro') ?></p>
                        <p><?= $previsaoorcamento->dt_cadastro ? $previsaoorcamento->dt_cadastro : 'Não Informado'; ?></p>


                        <p class="title"><?= __('Ult. Atualização') ?></p>
                        <p><?= $previsaoorcamento->last_update ? $previsaoorcamento->last_update : 'Não Informado'; ?></p>


                    </div>         
                </div>

                <?php if ($previsaoorcamento->has('apuracaoforma')) { ?>

                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="project_detail">
                            <p class="title">Forma de Apuração</p>
                            <p><?= $previsaoorcamento->has('apuracaoforma') ? $this->Html->link($previsaoorcamento->apuracaoforma->descricao, ['controller' => 'Apuracaoformas', 'action' => 'view', $previsaoorcamento->apuracaoforma->id]) : 'Não Informado' ?></p>

                            <p class="title">Ramo de Atuação</p>
                            <p><?= $previsaoorcamento->has('atuacaoramo') ? $this->Html->link($previsaoorcamento->atuacaoramo->descricao, ['controller' => 'Atuacaoramos', 'action' => 'view', $previsaoorcamento->atuacaoramo->id]) : 'Não Informado' ?></p>


                            <p class="title">Número min. de funcionários/ Prolabore</p>
                            <p><?= $previsaoorcamento->min_funcionarios ? $this->Number->format($previsaoorcamento->min_funcionarios) : 'Não Informado'; ?></p>


                            <p class="title">Número máx. de funcionários/ Prolabore</p>
                            <p><?= $previsaoorcamento->max_funcionarios ? $this->Number->format($previsaoorcamento->max_funcionarios) : 'Não Informado'; ?></p>


                            <p class="title">Faturamento mensal mín.</p>
                            <p><?= $previsaoorcamento->min_faturamento_mensal ? 'R$ ' . $this->Number->format($previsaoorcamento->min_faturamento_mensal) : 'Não Informado'; ?></p>


                            <p class="title">Faturamento mensal máx.</p>
                            <p><?= $previsaoorcamento->max_faturamento_mensal ? 'R$ ' . $this->Number->format($previsaoorcamento->max_faturamento_mensal) : 'Não Informado'; ?></p>
                        </div></div>

                <?php } ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="project_detail">


                        <p class="title">Quantidade de vezes que esse serviço poderá ser gratuito para o cliente</p>
                        <p><?= $previsaoorcamento->qtdegratis ? $this->Number->format($previsaoorcamento->qtdegratis) : 'Não Informado'; ?></p>


                        <p class="title">Esse serviço será gratuito para todos os USUÁRIOS do sistema?</p>
                        <p><?= $previsaoorcamento->gratis_todosusuarios ? __('SIM') : __('NÃO'); ?></p>


                        <p class="title">Esse serviço será gratuito para todos os CLIENTES da Tríade?</p>
                        <p><?= $previsaoorcamento->gratis_todoscliente ? __('SIM') : __('NÃO'); ?></p>


                        <p class="title">Esse serviço será gratuito para os CLIENTES da Tríade que já tem acesso à esse módulo</p>
                        <p><?= $previsaoorcamento->gratis_clientemodulo ? __('SIM') : __('NÃO'); ?></p>


                        <p class="title">Esse serviço poderá ser solicitado gratuitamente por vezes ilimitadas</p>
                        <p><?= $previsaoorcamento->qtdeilimitadagratis ? __('SIM') : __('NÃO'); ?></p>


                        <p class="title">Tela para redirecionamento</p>
                        <p><?= $previsaoorcamento->has('tela') ? $this->Html->link($previsaoorcamento->tela->descricao, ['controller' => 'Telas', 'action' => 'view', $previsaoorcamento->tela->id]) : 'Não Informado' ?></p>
                    </div>         
                </div>

                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $previsaoorcamento->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $previsaoorcamento->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $previsaoorcamento->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


