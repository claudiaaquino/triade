<?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Orçamento para os Serviços <small>* campos obrigatórios</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                   
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?= $this->Form->create($previsaoorcamento, ["class" => "form-horizontal form-label-left"]) ?>
                <?= $this->Flash->render() ?>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tiposervico_id">Serviço <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tiposervico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $tiposervicos, 'empty' => ' ']); ?>
                    </div> 
                </div> 

                <div class="aberturaempresa-details well well-sm">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="apuracaoforma_id">Forma de Apuração da Empresa <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('apuracaoforma_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $apuracaoformas, 'empty' => true]); ?>
                        </div> 
                    </div> 

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="atuacaoramo_id">Ramo de atuação <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('atuacaoramo_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $atuacaoramos, 'empty' => true]); ?>
                        </div> 
                    </div> 

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="min_funcionarios">Número min. de funcionários/ Prolabore <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('min_funcionarios', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                        </div> 
                    </div>                         

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="max_funcionarios">Número máx. de funcionários/ Prolabore <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('max_funcionarios', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                        </div> 
                    </div>                         

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="min_faturamento_mensal">Faturamento mensal mín. <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('min_faturamento_mensal', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                        </div> 
                    </div>                         

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="max_faturamento_mensal">Faturamento mensal máx. <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?= $this->Form->input('max_faturamento_mensal', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                        </div> 
                    </div>        
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="formaspagamentoservico_id">Forma de Pagamento <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('formaspagamentoservico_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $formaspagamentoservicos, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="valor_servico">Valor do Serviço (no ato do contrato) <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('valor_servico', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="valor_adicionalmensal">Valor do Serviço (adicional na mensal do cliente) <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('valor_adicionalmensal', ['type' => 'text', "class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="gratis_todosusuarios">Esse serviço será gratuito para todos os USUÁRIOS do sistema? 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('gratis_todosusuarios', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="gratis_todoscliente">Esse serviço será gratuito para todos os CLIENTES da Tríade?  
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('gratis_todoscliente', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="gratis_clientemodulo">Esse serviço será gratuito para os CLIENTES da Tríade que já tem acesso à esse módulo? 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('gratis_clientemodulo', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         


                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="qtdeilimitadagratis">Esse serviço poderá ser solicitado gratuitamente e por vezes ilimitadas?
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('qtdeilimitadagratis', ["class" => "form-control flat col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         
                <div class="form-group">
                    <label class="control-label col-md-6 col-sm-6 col-xs-12" for="qtdegratis">Que quantidade esse serviço poderá ser gratuito para o cliente? 
                    </label>
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <?= $this->Form->input('qtdegratis', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>                         

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tela_id">Caso o cliente solicite esse serviço, qual será a próxima tela para que ele possa informar dados específicos? <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tela_id', ["class" => "form-control col-md-7 col-xs-12", 'label' => false, 'options' => $telas, 'empty' => true]); ?>
                    </div> 
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="tempo_execucao">Quantos dias planejados para a execução desse serviço? <span class="required">*</span>
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <?= $this->Form->input('tempo_execucao', ["class" => "form-control col-md-7 col-xs-12", 'label' => false]); ?>

                    </div> 
                </div>         
                <div class="ln_solid"></div>                 
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Atualizar</button>
                        <?= $this->Html->link(__('Voltar'), $backlink, ['class' => "btn btn-primary"]) ?>                        
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js', array('inline' => false)); ?>
<?= $this->Html->script('/vendors/select2/dist/js/select2.full.min.js'); ?>
<script>
    var keys_anexos = 0;
    $(document).ready(function () {
        $(":input").inputmask();
        $("select").select2({placeholder: 'selecione uma opção'});
        $("select").addClass('select2');
        toogleAberturaEmpresaDetails();

        $('#valor-servico,#valor-adicionalmensal,#min-faturamento-mensal,#max-faturamento-mensal').inputmask('decimal', {
            radixPoint: ",",
            groupSeparator: ".",
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0',
            rightAlign: false,
            onBeforeMask: function (value, opts) {
                return value;
            }});


        $('#tiposervico-id').change(function (e) {
            toogleAberturaEmpresaDetails();
        });
    });
    function toogleAberturaEmpresaDetails() {
        if ($('#tiposervico-id').val() == 1) {//abertura de empresa
            $('.aberturaempresa-details').show();
        } else {
            $('.aberturaempresa-details').hide();
        }
    }
</script>