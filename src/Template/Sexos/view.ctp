<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sexo'), ['action' => 'edit', $sexo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sexo'), ['action' => 'delete', $sexo->id], ['confirm' => __('Tem certeza que deseja deletar esse registro # {0}?', $sexo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sexos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sexo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Funcionarios'), ['controller' => 'Funcionarios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Funcionario'), ['controller' => 'Funcionarios', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sexos view large-9 medium-8 columns content">
    <h3><?= h($sexo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Descricao') ?></th>
            <td><?= h($sexo->descricao) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($sexo->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Funcionarios') ?></h4>
        <?php if (!empty($sexo->funcionarios)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Empresa Id') ?></th>
                <th scope="col"><?= __('Num Contrato') ?></th>
                <th scope="col"><?= __('Cargo Id') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Dt Nascimento') ?></th>
                <th scope="col"><?= __('Cpf') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Endereco') ?></th>
                <th scope="col"><?= __('End Numero') ?></th>
                <th scope="col"><?= __('End Complemento') ?></th>
                <th scope="col"><?= __('End Bairro') ?></th>
                <th scope="col"><?= __('End Cep') ?></th>
                <th scope="col"><?= __('Id Estado') ?></th>
                <th scope="col"><?= __('Id Cidade') ?></th>
                <th scope="col"><?= __('Telefone Residencial') ?></th>
                <th scope="col"><?= __('Telefone Celular') ?></th>
                <th scope="col"><?= __('Estadocivil Id') ?></th>
                <th scope="col"><?= __('Sexo Id') ?></th>
                <th scope="col"><?= __('Cpf Responsavel') ?></th>
                <th scope="col"><?= __('Telefone Responsavel') ?></th>
                <th scope="col"><?= __('Ctps') ?></th>
                <th scope="col"><?= __('Ctps Serie') ?></th>
                <th scope="col"><?= __('Cnh') ?></th>
                <th scope="col"><?= __('Cnh Dt Habilitacao') ?></th>
                <th scope="col"><?= __('Cnh Dt Vencimento') ?></th>
                <th scope="col"><?= __('Rg') ?></th>
                <th scope="col"><?= __('Rg Estado') ?></th>
                <th scope="col"><?= __('Rg Expedidor') ?></th>
                <th scope="col"><?= __('Rg Dt Expedicao') ?></th>
                <th scope="col"><?= __('Militar Numero') ?></th>
                <th scope="col"><?= __('Militar Expedidor') ?></th>
                <th scope="col"><?= __('Militar Serie') ?></th>
                <th scope="col"><?= __('Eleitor Numero') ?></th>
                <th scope="col"><?= __('Eleitor Zona') ?></th>
                <th scope="col"><?= __('Eleitor Secao') ?></th>
                <th scope="col"><?= __('Eleitor Dt Emissao') ?></th>
                <th scope="col"><?= __('Pai Nome') ?></th>
                <th scope="col"><?= __('Mae Nome') ?></th>
                <th scope="col"><?= __('Salario') ?></th>
                <th scope="col"><?= __('Aux Transporte') ?></th>
                <th scope="col"><?= __('Aux Alimentacao') ?></th>
                <th scope="col"><?= __('Flag Deficiente') ?></th>
                <th scope="col"><?= __('Desc Deficiencia') ?></th>
                <th scope="col"><?= __('Dt Cadastro Sistema') ?></th>
                <th scope="col"><?= __('Dt Contrato Efetivado') ?></th>
                <th scope="col"><?= __('Dt Demissao') ?></th>
                <th scope="col"><?= __('Demissao Motivo') ?></th>
                <th scope="col"><?= __('Last Updated Fields') ?></th>
                <th scope="col"><?= __('Last Update') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($sexo->funcionarios as $funcionarios): ?>
            <tr>
                <td><?= h($funcionarios->id) ?></td>
                <td><?= h($funcionarios->user_id) ?></td>
                <td><?= h($funcionarios->empresa_id) ?></td>
                <td><?= h($funcionarios->num_contrato) ?></td>
                <td><?= h($funcionarios->cargo_id) ?></td>
                <td><?= h($funcionarios->nome) ?></td>
                <td><?= h($funcionarios->dt_nascimento) ?></td>
                <td><?= h($funcionarios->cpf) ?></td>
                <td><?= h($funcionarios->email) ?></td>
                <td><?= h($funcionarios->endereco) ?></td>
                <td><?= h($funcionarios->end_numero) ?></td>
                <td><?= h($funcionarios->end_complemento) ?></td>
                <td><?= h($funcionarios->end_bairro) ?></td>
                <td><?= h($funcionarios->end_cep) ?></td>
                <td><?= h($funcionarios->id_estado) ?></td>
                <td><?= h($funcionarios->id_cidade) ?></td>
                <td><?= h($funcionarios->telefone_residencial) ?></td>
                <td><?= h($funcionarios->telefone_celular) ?></td>
                <td><?= h($funcionarios->estadocivil_id) ?></td>
                <td><?= h($funcionarios->sexo_id) ?></td>
                <td><?= h($funcionarios->cpf_responsavel) ?></td>
                <td><?= h($funcionarios->telefone_responsavel) ?></td>
                <td><?= h($funcionarios->ctps) ?></td>
                <td><?= h($funcionarios->ctps_serie) ?></td>
                <td><?= h($funcionarios->cnh) ?></td>
                <td><?= h($funcionarios->cnh_dt_habilitacao) ?></td>
                <td><?= h($funcionarios->cnh_dt_vencimento) ?></td>
                <td><?= h($funcionarios->rg) ?></td>
                <td><?= h($funcionarios->rg_estado) ?></td>
                <td><?= h($funcionarios->rg_expedidor) ?></td>
                <td><?= h($funcionarios->rg_dt_expedicao) ?></td>
                <td><?= h($funcionarios->militar_numero) ?></td>
                <td><?= h($funcionarios->militar_expedidor) ?></td>
                <td><?= h($funcionarios->militar_serie) ?></td>
                <td><?= h($funcionarios->eleitor_numero) ?></td>
                <td><?= h($funcionarios->eleitor_zona) ?></td>
                <td><?= h($funcionarios->eleitor_secao) ?></td>
                <td><?= h($funcionarios->eleitor_dt_emissao) ?></td>
                <td><?= h($funcionarios->pai_nome) ?></td>
                <td><?= h($funcionarios->mae_nome) ?></td>
                <td><?= h($funcionarios->salario) ?></td>
                <td><?= h($funcionarios->aux_transporte) ?></td>
                <td><?= h($funcionarios->aux_alimentacao) ?></td>
                <td><?= h($funcionarios->flag_deficiente) ?></td>
                <td><?= h($funcionarios->desc_deficiencia) ?></td>
                <td><?= h($funcionarios->dt_cadastro_sistema) ?></td>
                <td><?= h($funcionarios->dt_contrato_efetivado) ?></td>
                <td><?= h($funcionarios->dt_demissao) ?></td>
                <td><?= h($funcionarios->demissao_motivo) ?></td>
                <td><?= h($funcionarios->last_updated_fields) ?></td>
                <td><?= h($funcionarios->last_update) ?></td>
                <td><?= h($funcionarios->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Funcionarios', 'action' => 'view', $funcionarios->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Funcionarios', 'action' => 'edit', $funcionarios->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Funcionarios', 'action' => 'delete', $funcionarios->id], ['confirm' => __('Tem certeza que deseja deletar esse registro # {0}?', $funcionarios->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
