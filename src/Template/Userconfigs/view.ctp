<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">         
            <div class="x_title">
                <h2 class="green"><i class="fa fa-file"></i><?= __('Userconfigs') ?> - <?= h($userconfig->id) ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="project_detail">
                                                                                                            <p class="title"><?= __('User') ?></p>
                                    <p><?= $userconfig->has('user') ? $this->Html->link($userconfig->user->nome, ['controller' => 'Users', 'action' => 'view', $userconfig->user->id]) : 'Não Informado' ?></p>

                                                                                                                                                                                        
                        <p class="title"><?= __('Id') ?></p>
                                <p><?= $userconfig->id ? $this->Number->format($userconfig->id) : 'Não Informado'; ?></p>

                                                                                                                                
                        <p class="title"><?= __('Last Update') ?></p>
                                <p><?= $userconfig->last_update ? $userconfig->last_update : 'Não Informado'; ?></p>

                                                                                                                                                        <p class="title"><?= __('Assinatura Mensagem') ?></p>
                                <p>  <?= $userconfig->assinatura_mensagem ? $this->Text->autoParagraph(h($userconfig->assinatura_mensagem)) : 'Não Informado'; ?></p>
                                                                        </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?= $this->Html->link("Voltar", $backlink, ['class' => "btn btn-default"]) ?>
                        <?= $this->Html->link("Editar", ['action' => 'edit', $userconfig->id], ['class' => "btn btn-primary"]) ?>
                        <?= $this->Form->postLink("Deletar", ['action' => 'delete', $userconfig->id], ['class' => "btn btn-danger", 'confirm' => __('Você tem certeza que deseja deletar esse registro?', $userconfig->id)]) ?>
                    </div>
                </div>
            </div>
        </div>
            </div>
</div>


