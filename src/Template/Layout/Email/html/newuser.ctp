<html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style type="text/css">
            @import url(http://fonts.googleapis.com/css?family=Lato:400);
            /* Take care of image borders and formatting */

            img {
                max-width: 600px;
                outline: none;
                text-decoration: none;
                -ms-interpolation-mode: bicubic;
            }

            a {
                text-decoration: none;
                border: 0;
                outline: none;
            }

            a img {
                border: none;
            }
            /* General styling */

            td,
            h1,
            h2,
            h3 {
                font-family: Helvetica, Arial, sans-serif;
                font-weight: 400;
            }

            body {
                -webkit-font-smoothing: antialiased;
                -webkit-text-size-adjust: none;
                width: 100%;
                height: 100%;
                color: #37302d;
                background: #ffffff;
            }

            /*            table {
                            background:
                        }*/

            h1,
            h2,
            h3 {
                padding: 0;
                margin: 0;
                color: #ffffff;
                font-weight: 400;
            }

            h3 {
                color: #21c5ba;
                font-size: 24px;
            }
        </style>
        <style type="text/css" media="screen">
            @media screen {
                /* Thanks Outlook 2013! http://goo.gl/XLxpyl*/
                td,
                h1,
                h2,
                h3 {
                    font-family: 'Lato', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                }
            }
        </style>
        <style type="text/css" media="only screen and (max-width: 480px)">
            /* Mobile styles */

            @media only screen and (max-width: 480px) {
                table[class="w320"] {
                    width: 320px !important;
                }
                table[class="w300"] {
                    width: 300px !important;
                }
                table[class="w290"] {
                    width: 290px !important;
                }
                td[class="w320"] {
                    width: 320px !important;
                }
                td[class="mobile-center"] {
                    text-align: center !important;
                }
                td[class="mobile-padding"] {
                    padding-left: 20px !important;
                    padding-right: 20px !important;
                    padding-bottom: 20px !important;
                }
            }
        </style>
    </head><body bgcolor="#ffffff" class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none">
        <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tbody>
                <tr>
                    <td align="center" valign="top" bgcolor="#ffffff" width="100%">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr>
                                    <td style="border-bottom: 3px solid #3bcdc3;" width="100%">
                            <center>
                                <table cellspacing="0" cellpadding="0" width="500" class="w320">
                                    <tbody>
                                        <tr>
                                            <td class="mobile-center" style="padding:10px 0; text-align:left;" valign="top">
                                                <img width="250" height="62" src="http://www.triadeconsultoria.adv.br/wp-content/uploads/2016/12/Contabilidade-Tr%C3%ADade-Consultoria.jpg?">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                    </td>
                </tr>
                <tr>
                    <td style="background: url(https://www.filepicker.io/api/file/zLBr1W6UT6qZP4jI2yRz) no-repeat center; background-color: #64594b; background-position: center;" background="https://www.filepicker.io/api/file/zLBr1W6UT6qZP4jI2yRz" bgcolor="#64594b" valign="top">
                        <!--[if gte mso 9]>
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"
                          style="mso-width-percent:1000;height:303px;">
                            <v:fill type="tile" src="https://www.filepicker.io/api/file/ewEXNrLlTneFGtlB5ryy"
                            color="#64594b" />
                            <v:textbox inset="0,0,0,0">
                            <![endif]-->
                        <div>
                            <center>
                                <table cellspacing="0" cellpadding="0" width="530" height="303" class="w320">
                                    <tbody>
                                        <tr>
                                            <td valign="middle" style="color:#fff;vertical-align:middle; padding-right: 15px; padding-left: 15px; text-align:left;" height="303" class="mobile-center">
                                                <h1>Contabilidade Online</h1>
                                                <br>
                                                <h2>Aproveitem a facilidade e benefícios de usar o nosso sistema</h2>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                        </div>
                        <!--[if gte mso 9]>
                        </v:textbox>
                      </v:rect>
                    <![endif]-->
                    </td>
                </tr>
                <tr>
                    <td valign="top">
            <center contenteditable="true">
                <table cellspacing="0" cellpadding="30" width="500" class="w290">
                    <tbody>
                        <tr>
                            <td valign="top" style="border-bottom:1px solid #a1a1a1;">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td style="color:#2E64FE;text-align: center;">
                                                <h3>Bem vindo <?= $nome ?>, 
                                                    <br>
                                                </h3>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table cellspacing="0" cellpadding="0" width="500" class="w320">
                    <tbody>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td class="mobile-padding" style="text-align:left;">
                                                <br>Aproveite a praticidade de usar nosso sistema de contabilidade online. Pelo
                                                sistema você pode acessar os dados da sua empresa, acompanhar os serviços
                                                gerais da sua contabilidade, enviar e solicitar documentos e muito mais.
                                                <br>
                                                <br>Seu usuário para login é: <?= $usuario ?><br><br></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="mobile-padding">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td style="width:150px; background-color: #2E64FE">
                                                <a href="http://sistema.triadeconsultoria.adv.br" target="_blank">
                                                    <div style="text-align: center;">
                                                        <font color="#ffffff">
                                                        <span style="font-size: 13px;">
                                                            <b>&nbsp;Acessar agora</b>
                                                        </span>
                                                        </font>
                                                    </div>
                                                </a>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="25" width="100%">
                                    <tbody>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </center>
        </td>
    </tr>
    <tr>
        <td style="background-color:#c2c2c2;">
    <center>
        <table cellspacing="0" cellpadding="0" width="500" class="w320">
            <tbody>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="30" width="100%">
                            <tbody>
                                <tr>
                                    <td style="text-align:center;">
                                        <a href="https://twitter.com/triadeconsult">
                                            <img width="61" height="51" src="https://www.filepicker.io/api/file/vkoOlof0QX6YCDF9cCFV" alt="twitter"> </a>
                                        <a href="https://plus.google.com/112189139575375264300">
                                            <img width="61" height="51" src="https://www.filepicker.io/api/file/fZaNDx7cSPaE23OX2LbB" alt="google plus"> </a>
                                        <a href="http://fb.me/triadeconsult">
                                            <img width="61" height="51" src="https://www.filepicker.io/api/file/b3iHzECrTvCPEAcpRKPp" alt="facebook"> </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
            <center>
                <table style="margin:0 auto;" cellspacing="0" cellpadding="5" width="100%">
                    <tbody>
                        <tr>
                            <td style="text-align:center; margin:0 auto;" width="100%">Tríade Consultoria e Contabilidade</td>
                        </tr>
                    </tbody>
                </table>
            </center>
            </td>
            </tr>
            </tbody>
        </table>
    </center>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>



</body></html>