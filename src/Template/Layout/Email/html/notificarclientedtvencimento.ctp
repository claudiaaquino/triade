<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style type="text/css">
            @import url(http://fonts.googleapis.com/css?family=Lato:400);
            /* Take care of image borders and formatting */



            a {
                text-decoration: none;
                border: 0;
                outline: none;
            }

            a img {
                border: none;
            }
            /* General styling */

            td,
            h1,
            h2,
            h3 {
                font-family: Helvetica, Arial, sans-serif;
                font-weight: 400;
            }

            body {
                -webkit-font-smoothing: antialiased;
                -webkit-text-size-adjust: none;
                width: 100%;
                height: 100%;
                background: #ffffff;
            }

            /*            table {
                            background:
                        }*/

            h1,
            h2,
            h3 {
                padding: 0;
                margin: 0;
                color: #ffffff;
                font-weight: 400;
            }

            h3 {
                color: #21c5ba;
                font-size: 24px;
            }
        </style>
        <style type="text/css" media="screen">
            @media screen {
                /* Thanks Outlook 2013! http://goo.gl/XLxpyl*/
                td,
                h1,
                h2,
                h3 {
                    font-family: 'Lato', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                }
            }
        </style>
        <style type="text/css" media="only screen and (max-width: 480px)">
            /* Mobile styles */

            @media only screen and (max-width: 480px) {
                table[class="w320"] {
                    width: 320px !important;
                }
                table[class="w300"] {
                    width: 300px !important;
                }
                table[class="w290"] {
                    width: 290px !important;
                }
                td[class="w320"] {
                    width: 320px !important;
                }
                td[class="mobile-center"] {
                    text-align: center !important;
                }
                td[class="mobile-padding"] {
                    padding-left: 20px !important;
                    padding-right: 20px !important;
                    padding-bottom: 20px !important;
                }
            }
        </style>
    </head>

    <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
          bgcolor="#ffffff">
        <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tbody>
                <tr>
                    <td align="center" valign="top" bgcolor="#ffffff" width="100%">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr>
                                    <td style="border-bottom: 3px solid;" width="100%">
                            <center>
                                <table cellspacing="0" cellpadding="0" width="500" class="w320">
                                    <tbody>
                                        <tr>
                                            <td class="mobile-center" style="padding:10px 0; text-align:left;" valign="top">
                                                <a href="http://triadeconsultoria.adv.br"> <img width="250" height="62" src="http://www.triadeconsultoria.adv.br/wp-content/uploads/2016/12/Contabilidade-Tr%C3%ADade-Consultoria.jpg?"></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
            <center contenteditable="true">
                <table cellspacing="0" cellpadding="30" width="500" class="w290">
                    <tbody>
                        <tr>
                            <td valign="top" style="border-bottom:1px solid #a1a1a1;">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td style="color:#2E64FE;text-align: center;">
                                                <h3>Serviço Concluído e Documento a Pagar
                                                    <br>
                                                </h3>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table cellspacing="0" cellpadding="0" width="500" class="w320">
                    <tbody>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td class="mobile-padding" style="text-align:left;">
                                                <h4>Olá <?= $nome; ?>, <br>
                                                    a Tríade consultoria gostaria de notificá-lo que você tem um novo documento da empresa para ser pago.<br>
                                                    Serviço: <?= $tarefa->tiposervico->descricao; ?><br>
                                                    Competencia: <?= $competencia ?> <br>
                                                    Dt. de Vencimento para pagamento: <?= $tarefa->dt_vencimento ?><br>
                                                    Anotações: <?= $tarefa->anotacao_cliente ?> <br>
                                                </h4> 
                                                <br>
                                                Para ver anexos e mais informações do serviço acesse o sistema.
                                                <br>
                                                <br>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="mobile-padding">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td style="width:150px; background-color: #2E64FE">
                                                <a href="http://sistema.triadeconsultoria.adv.br/tarefas/view/<?= $tarefa->id ?>">                                            
                                                    <div style="text-align: center;">
                                                        <font color="#ffffff">
                                                        <span style="font-size: 13px;">
                                                            <br>
                                                            <b>Ver mais detalhes</b>
                                                            <br>
                                                            <br>
                                                        </span>
                                                        </font>
                                                    </div>
                                                </a>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br>
                                                <br>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </center>
        </td>
    </tr>
    <tr>
        <td style="background-color:#c2c2c2;">
    <center>
        <table cellspacing="0" cellpadding="0" width="500" class="w320">
            <tbody>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="30" width="100%">
                            <tbody>
                                <tr>
                                     <td style="text-align:center;">
                                        <a href="https://twitter.com/triadeconsult">
                                            <img width="61" height="51" src="https://www.filepicker.io/api/file/vkoOlof0QX6YCDF9cCFV" alt="twitter"> </a>
                                        <a href="https://plus.google.com/112189139575375264300">
                                            <img width="61" height="51" src="https://www.filepicker.io/api/file/fZaNDx7cSPaE23OX2LbB" alt="google plus"> </a>
                                        <a href="http://fb.me/triadeconsult">
                                            <img width="61" height="51" src="https://www.filepicker.io/api/file/b3iHzECrTvCPEAcpRKPp" alt="facebook"> </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
            <center>
                <table style="margin:0 auto;" cellspacing="0" cellpadding="5" width="100%">
                    <tbody>
                        <tr>
                            <td style="text-align:center; margin:0 auto;" width="100%">
                                <a href="http://triadeconsultoria.adv.br"> Tríade Consultoria e Contabilidade</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </center>
            </td>
            </tr>
            </tbody>
        </table>
    </center>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>

</html>