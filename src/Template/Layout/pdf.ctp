<?php
// inclusion de la librairie TCPDF
//require_once ROOT . DS . 'vendor' . DS . 'tecnickcom' . DS . 'tcpdf' . DS . 'tcpdf.php';
//require_once('/home/username/html2pdf/html2pdf.class.php');
require_once ROOT . DS . 'vendor' . DS . 'spipu' . DS . 'html2pdf' . DS . 'html2pdf.class.php';


//$html = '<!DOCTYPE html><html lang="pt"><head>';
//$this->Html->charset();
//$this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css');
//$this->Html->css('/vendors/font-awesome/css/font-awesome.min.css');
//$this->Html->css('custom.min.css');
//$this->Html->script('/vendors/jquery/dist/jquery.min.js', array('inline' => false));
//$html .= $this->fetch('meta');
//$html .= $this->fetch('css');
//$html .= ' </head><body><div class="container body"><div class="right_col" role="main">';
//$html .= $this->fetch('content');
//$html .= ' </div></div>';
//$this->Html->script('/vendors/bootstrap/dist/js/bootstrap.min.js');
//$this->Html->script('/vendors/fastclick/lib/fastclick.js');
//$this->Html->script('/vendors/iCheck/icheck.min.js');
//$this->Html->script('custom.min.js');
//$html .= $this->fetch('script');
//$html .= '</body></html>';
//echo $html;
//
// init HTML2PDF
$html2pdf = new HTML2PDF('P', 'A4', 'pt', true, 'UTF-8', array(0, 0, 0, 0));

// display the full page
$html2pdf->pdf->SetDisplayMode('fullpage');

// get the HTML
ob_start();
?>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']) ?>

        <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('/vendors/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('/vendors/iCheck/skins/flat/green.css') ?>        
        <?= $this->Html->css('custom.min.css') ?>

        <?= $this->Html->script('/vendors/jquery/dist/jquery.min.js', array('inline' => false)) //necessário para carregas os outros js dentro dos content's ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>    

    </head>
    <body>
        <div class="container body">
            <div class="right_col" role="main">
                <?= $this->fetch('content') ?>
            </div>
        </div>

        <?= $this->Html->script('/vendors/bootstrap/dist/js/bootstrap.min.js') ?>

        <?= $this->Html->script('/vendors/fastclick/lib/fastclick.js') ?>
        <?= $this->Html->script('/vendors/iCheck/icheck.min.js') ?>

        <?= $this->Html->script('custom.min.js') ?>

        <?= $this->fetch('script') ?>
    </body>
</html>


<?php
$content = ob_get_clean();

// convert
$html2pdf->writeHTML($content);

// add the automatic index
$html2pdf->createIndex('Sommaire', 30, 12, false, true, 2);

// send the PDF
$html2pdf->Output('about.pdf');


//$html2pdf = new HTML2PDF('P', 'A4', 'pt');
//$html2pdf->writeHTML($html);
//$html2pdf->Output(WWW_ROOT . '/docs/' .$filename, 'F');

//// Création d'un document TCPDF avec les variables par défaut
//$pdf = new TCPDF('L', 'mm', PDF_PAGE_FORMAT, TRUE, 'UTF-8', FALSE);
//// Spécification de certains paramètres de TCPDF (içi on spécifie l'auteur par défaut)
//$pdf->SetCreator(PDF_CREATOR);
//
//// On enlève l'entête et le pied de page
//$pdf->setPrintHeader(FALSE);
//$pdf->setPrintFooter(FALSE);
//
//// On spécifie la fonte par défaut
//$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
//
//// On définit les marges
//$pdf->SetMargins(5, 37, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(5);
//
//// On indique que le dépassement d'une page entraine automatiquement la création d'un saut de page et d'une nouvelle page
//$pdf->SetAutoPageBreak(TRUE, 5);
//
//// ---------------------------------------------------------
//// La fonte et la couleur à utiliser dans la page qui va être créée
//$pdf->SetFont('times', '', 10);
//$pdf->setColor('text', 0, 0, 0);
//// On ajoute une page
//$pdf->AddPage();
//// voilà l'astuce, on récupère la vue HTML créée par CakePHP pour alimenter notre fichier PDF
//$pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');
//// on ferme la page
//$pdf->lastPage();
//// On indique à TCPDF que le fichier doit être enregistré sur le serveur ($filename étant une variable que vous aurez pris soin de définir dans l'action de votre controller)
//$pdf->Output(WWW_ROOT . '/docs/' .$filename, 'F');
