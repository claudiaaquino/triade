<!DOCTYPE html>
<html lang="pt">
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Sistema Tríade</title>

        <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('/vendors/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('/vendors/iCheck/skins/flat/green.css') ?>        
        <?= $this->Html->css('custom.min.css') ?>
        <?= $this->Html->css('/vendors/select2/dist/css/select2.min.css'); ?>

        <?= $this->Html->script('/vendors/jquery/dist/jquery.min.js', array('inline' => false)) //necessário para carregas os outros js dentro dos content's ?>
        <?= $this->Html->script('/js/ajax/index.js', array('inline' => false)); ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?> 


        <style type="text/css">
            body a:hover {
                color: #194f8c;
            }
        </style>

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <?= $this->fetch('content') ?>
                <!-- /page content -->


            </div>
            <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
        </div> 

        <?= $this->Html->script('/vendors/bootstrap/dist/js/bootstrap.min.js') ?>
        <?= $this->Html->script('/vendors/fastclick/lib/fastclick.js') ?>
        <?= $this->Html->script('/vendors/iCheck/icheck.min.js') ?>
        <?= $this->Html->script('custom.min.js') ?>
        <?= $this->fetch('script') ?>
    </body>
</html>