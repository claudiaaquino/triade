<!DOCTYPE html>
<html lang="pt">
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Sistema Tríade</title>

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">


        <?= $this->Html->css('/vendors/bootstrap/dist/css/bootstrap.min.css') ?>
        <?= $this->Html->css('/vendors/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('custom.min.css') ?>

        <?= $this->Html->script('/vendors/jquery/dist/jquery.min.js', array('inline' => false)) //necessário para carregas os outros js dentro dos content's ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?> 
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <span class="site_title">
                                <?php echo $this->Html->image("favicon-32x32.png", ["alt" => "Sistema Tríade Consultoria", 'url' => ['controller' => 'Home', 'action' => 'index']]); ?>
                                TRÍADE
                            </span>
                        </div>

                        <div class="clearfix"></div>

                        <!--menu profile quick info--> 
                        <div class="profile">
                            <div class="profile_pic">
                                <?php echo $this->Html->image("profiles/" . $foto, ["height" => 70, "class" => 'img-circle profile_img', 'url' => ['controller' => 'Home', 'action' => 'index']]); ?>
                            </div>
                            <div class="profile_info">
                                <span>Bem Vindo,</span>
                                <h2><?= $loggednome ?></h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info 

                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>Menu Geral</h3>
                                <?php if ($loggedIn) { ?>
                                    <ul class="nav side-menu">
                                        <?php foreach ($menus as $menu) { ?>
                                            <li><a><i class="fa fa-<?= $menu->iconname; ?>"></i> <?= $menu->descricao; ?> <span class="fa fa-chevron-down"></span></a>
                                                <!--http://fontawesome.io/icons/-->
                                                <ul class="nav child_menu">
                                                    <?php
                                                    foreach ($menu->menusubmenus as $submenu) {
                                                        if ($submenu->submenu->controller) {
                                                            ?>
                                                            <li><?= $this->Html->link(__($submenu->submenu->nome), ['controller' => ucfirst($submenu->submenu->controller), 'action' => $submenu->submenu->action]); ?></li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </ul>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                        <!-- /sidebar menu -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <?= $loggedfirstname ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><?= $this->Html->link('Perfil', ['controller' => 'users', 'action' => 'view']) ?></li>
                                        <li> <?= $this->Html->link('Sair', ['controller' => 'users', 'action' => 'logout']) ?></li>
                                    </ul>
                                </li>
                                <li id="box-mensagens" role="presentation" class="dropdown"></li>
                                <li id="box-tarefas" role="presentation" class="dropdown"></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <?= $this->Form->input('urlroot', [ "type" => "hidden", "value" => $this->request->webroot, 'label' => false]); ?>
                    <?= $this->Flash->render() ?>
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        Sistema Contabilidade Online <a href="http://triadeconsultoria.adv.br">Tríade Consultoria</a>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div> 
        <?= $this->Html->script('/js/ajax/index.js'); ?>
        <?= $this->Html->script('/vendors/bootstrap/dist/js/bootstrap.min.js') ?>

        <?= $this->Html->script('custom.min.js') ?>

        <?= $this->fetch('script') ?>
    </body>
</html>