<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * LogErros Controller
 *
 * @property \App\Model\Table\LogErrosTable $LogErros
 */
class LogErrosController extends AppController {

    public function isAuthorized($user) {
        if (!$this->Auth->user('user_triade')) {
            if (!in_array($this->request->param('action'), ['add', 'view'])) {
                return false;
            }
        }
        return true;
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Users', 'Empresas']
        ];
        $logErros = $this->paginate($this->LogErros);

        $this->set(compact('logErros'));
        $this->set('_serialize', ['logErros']);
    }

    /**
     * View method
     *
     * @param string|null $id Log Erro id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $logErro = $this->LogErros->get($id, [
            'contain' => ['Users', 'Empresas']
        ]);

        // se for um usuario qualquer tentando acessar um erro que não foi ele quem criou
        if (!$this->Auth->user('user_triade') && $logErro->user_id != $this->Auth->user('id')) {
            $this->Flash->error(__('Você não tem permissão de acessar esse log de erro.'));
            return $this->redirect(['controller' => 'Users', 'action' => 'view']);
        }

        $this->set('logErro', $logErro);
        $this->set('_serialize', ['logErro']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $logErro = $this->LogErros->newEntity();

        if ($this->request->is('post')) {
            $logErro = $this->LogErros->patchEntity($logErro, $this->request->data);
            $logErro->dt_cadastro = date('Y-m-d H:i:s');
            $logErro->user_id = $this->Auth->user('id');
            $logErro->empresa_id = $this->Auth->user('empresa_id');

            if (!empty($logErro->printfile)) {
                $logErro->documento_print = $this->saveAttachment($logErro->printfile, 'erros');
            }

            if ($this->LogErros->save($logErro)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));
                $this->emailReportarErro($logErro);

                return $this->redirect(['action' => 'view', $logErro->id]);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $users = $this->LogErros->Users->find('list', ['limit' => 200]);
        $empresas = $this->LogErros->Empresas->find('list', ['limit' => 200]);
        $this->set(compact('logErro', 'users', 'empresas'));
        $this->set('_serialize', ['logErro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Log Erro id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $logErro = $this->LogErros->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $logErro = $this->LogErros->patchEntity($logErro, $this->request->data);
        
            if ($this->LogErros->save($logErro)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $users = $this->LogErros->Users->find('list', ['limit' => 200]);
        $empresas = $this->LogErros->Empresas->find('list', ['limit' => 200]);
        $this->set(compact('logErro', 'users', 'empresas'));
        $this->set('_serialize', ['logErro']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Log Erro id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $logErro = $this->LogErros->get($id);
        if ($this->LogErros->delete($logErro)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /* Email disparado para avisar sobre o erro p triade */

    public function emailReportarErro($erro) {
        try {
            $email = new Email('default');
            $email->viewVars(['nomecliente' => $this->Auth->user('nome'), 'erro' => $erro]);
            $email->subject('[SISTEMA TRÍADE] Novo Erro Reportado')
                    ->template('default', 'erroreportado')
                    ->emailFormat('html')
                    ->to($this->_EMAIL_ADMIN)
                    ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

}
