<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Movimentacaobancarias Controller
 *
 * @property \App\Model\Table\MovimentacaobancariasTable $Movimentacaobancarias
 */
class MovimentacaobancariasController extends AppController {

    public $_TPDOCUMENTO_REGISTRONOTAFISCAL = 1;

    public function isAuthorized($user) {
        if (!$this->Auth->user('admin') && $this->Auth->user('empresa_id') != $this->_ID_TRIADE && !$this->request->is('ajax') && !$this->Auth->user('admin_empresa') && !$this->Auth->user('admin_setores')) {
            if (!in_array($this->request->param('action'), ['add'])) {
                return false;
            }
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [ 'limit' => 10,
            'contain' => ['Empresas', 'Movimentacaotipos', 'Fornecedores', 'Clientes', 'Contabancarias', 'Formaspagamentos']
        ];

        $movimentacaobancarias = $this->paginate($this->Movimentacaobancarias->find()->where(['Movimentacaobancarias.empresa_id' => $this->Auth->user('empresa_id')])->orderDesc('Movimentacaobancarias.dt_cadastro'));

        $this->set(compact('movimentacaobancarias'));
        $this->set('_serialize', ['movimentacaobancarias']);
    }

    /**
     * View method
     *
     * @param string|null $id Movimentacaobancaria id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $movimentacaobancaria = $this->Movimentacaobancarias->get($id, [
            'contain' => ['Empresas', 'Movimentacaotipos', 'Fornecedores', 'Clientes', 'Contabancarias.Bancos', 'Formaspagamentos', 'Documentos', 'Tiporeceitas', 'Tipodespesas', 'Movimentacoesfuturas']
        ]);

        if ($movimentacaobancaria->empresa_id != $this->Auth->user('empresa_id')) {
            $this->Flash->error(__('Você não tem permissão de visualizar um registro que não pertence à sua Empresa'));
            return $this->redirect($this->referer());
        }

        $this->set('movimentacaobancaria', $movimentacaobancaria);
        $this->set('_serialize', ['movimentacaobancaria']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $movimentacaobancaria = $this->Movimentacaobancarias->newEntity();
        if ($this->request->is('post')) {
            $movimentacaobancaria = $this->Movimentacaobancarias->patchEntity($movimentacaobancaria, $this->request->data);

            $movimentacaobancaria->dt_cadastro = date('Y-m-d H:i:s');
            $movimentacaobancaria->user_id = $this->Auth->user('id');
            $movimentacaobancaria->empresa_id = $this->Auth->user('empresa_id');

            if ($this->request->data('fornecedor_id') && is_string($this->request->data('fornecedor_id'))) {
                $new = $this->Movimentacaobancarias->Fornecedores->newEntity();
                $new->empresa_id = $this->Auth->user('empresa_id');
                $new->nome = $this->request->data('fornecedor_id');
                $new->dt_cadastro = date('Y-m-d H:i:s');
                if ($this->Movimentacaobancarias->Fornecedores->save($new)) {
                    $movimentacaobancaria->fornecedor_id = $new->id;
                }
            }

            if ($this->request->data('cliente_id') && is_string($this->request->data('cliente_id'))) {
                $new = $this->Movimentacaobancarias->Clientes->newEntity();
                $new->empresa_id = $this->Auth->user('empresa_id');
                $new->nome = $this->request->data('cliente_id');
                $new->dt_cadastro = date('Y-m-d H:i:s');
                if ($this->Movimentacaobancarias->Clientes->save($new)) {
                    $movimentacaobancaria->cliente_id = $new->id;
                }
            }

            if ($this->request->data('tipodespesa_id') && is_string($this->request->data('tipodespesa_id'))) {
                $new = $this->Movimentacaobancarias->Tipodespesas->newEntity();
                $new->empresa_id = $this->Auth->user('empresa_id');
                $new->descricao = $this->request->data('tipodespesa_id');
                $new->dt_cadastro = date('Y-m-d H:i:s');
                if ($this->Movimentacaobancarias->Tipodespesas->save($new)) {
                    $movimentacaobancaria->tipodespesa_id = $new->id;
                }
            }

            if ($this->request->data('tiporeceita_id') && is_string($this->request->data('tiporeceita_id'))) {
                $new = $this->Movimentacaobancarias->Tiporeceitas->newEntity();
                $new->empresa_id = $this->Auth->user('empresa_id');
                $new->descricao = $this->request->data('tiporeceita_id');
                $new->dt_cadastro = date('Y-m-d H:i:s');
                if ($this->Movimentacaobancarias->Tiporeceitas->save($new)) {
                    $movimentacaobancaria->tiporeceita_id = $new->id;
                }
            }



            $contabancariaTable = TableRegistry::get('Contabancarias');
            $contabancaria = $contabancariaTable->get($movimentacaobancaria->contabancaria_id);
            $saldoatual = $contabancaria->valorcaixa;
            $movimentacaobancaria->saldo_anterior = $saldoatual;

            /*             * ************SE O PAGAMENTO JA FOI FEITO À VISTA*************************** */
            if ($movimentacaobancaria->avista == 1 && $movimentacaobancaria->pago) {
                $saldoatual = $this->efetuarpagamento($movimentacaobancaria);
                $movimentacaobancaria->dt_pagamentoavista = date('Y-m-d H:i:s');
            }

            /*             * ************SE O PAGAMENTO FOI PARCELADO*************************** */
            if ($movimentacaobancaria->avista == 0) {
                $movimentacaobancaria->movimentacoesfuturas = $this->programarparcelas($movimentacaobancaria);
                /*                 * **********SE O PAGAMENTO FOI PARCELADO E JÁ PAGOU A PRIMEIRA PARCELA*************************** */
                if ($movimentacaobancaria->primeiraparcelapaga) {
                    $saldoatual = $this->efetuarpagamento($movimentacaobancaria, true);
                }
            }

            $movimentacaobancaria->saldo_posterior = $saldoatual; // se não houve pagamento à vista e nem de parcela, então o saldo posterior será igual ao saldo atual

            /*             * ************SALVA O DOCUMENTO NO SERVIDOR E O REGISTRO NO BANCO*************************** */
            if (!empty($this->request->data['file']['name'])) {
                $this->uploadDocument($this->request->data['file']);
                $movimentacaobancaria->documento_id = $this->saveDocument();
            }

            if ($this->Movimentacaobancarias->save($movimentacaobancaria)) {
                $this->Flash->success(__('Movimentação de caixa salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $movimentacaotipos = $this->Movimentacaobancarias->Movimentacaotipos->find('list');
        $tiporeceitas = $this->Movimentacaobancarias->Tiporeceitas->find('list')->where(['Tiporeceitas.empresa_id' => $this->Auth->user('empresa_id')]);
        $tipodespesas = $this->Movimentacaobancarias->Tipodespesas->find('list')->where(['Tipodespesas.empresa_id' => $this->Auth->user('empresa_id')]);
        $fornecedores = $this->Movimentacaobancarias->Fornecedores->find('list')->where(['Fornecedores.empresa_id' => $this->Auth->user('empresa_id')]);
        $clientes = $this->Movimentacaobancarias->Clientes->find('list')->where(['Clientes.empresa_id' => $this->Auth->user('empresa_id')]);
        $contabancarias = $this->Movimentacaobancarias->Contabancarias->find('listDetails')
                ->where(['Contabancarias.empresa_id' => $this->Auth->user('empresa_id')])
                ->andWhere(['Contabancarias.cliente_id is null', 'Contabancarias.fornecedor_id is null']);
        $formaspagamentos = $this->Movimentacaobancarias->Formaspagamentos->find('list');
        $this->set(compact('movimentacaobancaria', 'movimentacaotipos', 'fornecedores', 'clientes', 'contabancarias', 'formaspagamentos', 'tiporeceitas', 'tipodespesas'));
        $this->set('_serialize', ['movimentacaobancaria']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Movimentacaobancaria id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $movimentacaobancaria = $this->Movimentacaobancarias->get($id, [
            'contain' => ['Documentos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $movimentacaobancariaAnterior = $this->Movimentacaobancarias->get($id);
            $movimentacaobancaria = $this->Movimentacaobancarias->patchEntity($movimentacaobancaria, $this->request->data);

            if ($movimentacaobancaria->empresa_id != $this->Auth->user('empresa_id')) {
                $this->Flash->error(__('Você não tem permissão de editar um registro que não pertence à sua Empresa'));
                return $this->redirect($this->referer());
            }

            $contabancariaTable = TableRegistry::get('Contabancarias');
            $contabancaria = $contabancariaTable->get($movimentacaobancaria->contabancaria_id);
            $saldoatual = $contabancaria->valorcaixa;
            $movimentacaobancaria->saldo_anterior = $saldoatual;

            if (!$movimentacaobancariaAnterior->avista) { // PARCELADO, para poder obter se a primeira parcela foi paga ou não
                $primeiraparcela = $this->Movimentacaobancarias->Movimentacoesfuturas->find()->where(['movimentacaobancaria_id' => $movimentacaobancariaAnterior->id])->first();
            }

            /*             * ************SE O PAGAMENTO FOI FEITO À VISTA, e anteriormente era parcelado*************************** */
            if ($movimentacaobancaria->avista && $movimentacaobancaria->dirty('avista')) {

                if ($primeiraparcela->pago) {
                    $saldoatual = $this->desfazerpagamento($movimentacaobancariaAnterior, true);
                }

                $this->desfazerparcelamento($movimentacaobancariaAnterior);
                $movimentacaobancaria->movimentacoesfuturas = array();

                if ($movimentacaobancaria->pago) {
                    $saldoatual = $this->efetuarpagamento($movimentacaobancaria);
                    $movimentacaobancaria->dt_pagamentoavista = date('Y-m-d H:i:s');
                }
            }
            /*             * ************SE O PAGAMENTO CONTINUA SENDO À VISTA, MAS O alterou A OPÇÃO DE PAGAMENTO JÁ FEITO*************************** */
            if ($movimentacaobancaria->avista && !$movimentacaobancaria->dirty('avista')) {


                if (!$movimentacaobancaria->pago && $movimentacaobancaria->dirty('pago') && $movimentacaobancaria->getOriginal('pago')) {
                    $saldoatual = $this->desfazerpagamento($movimentacaobancariaAnterior);
                }

                if ($movimentacaobancaria->pago && $movimentacaobancaria->dirty('pago') && !$movimentacaobancaria->getOriginal('pago')) {
                    $saldoatual = $this->efetuarpagamento($movimentacaobancaria);
                }

                //se continua sendo pago, mas o cliente mudou o valor da movimentação, então tem que mudar o valor que debitou no saldo bancário
                if ($movimentacaobancaria->pago && !$movimentacaobancaria->dirty('pago') && $movimentacaobancaria->dirty('valor')) {
                    $this->desfazerpagamento($movimentacaobancariaAnterior);
                    $saldoatual = $this->efetuarpagamento($movimentacaobancaria);
                }
            }
            /*             * ************SE O PAGAMENTO CONTINUA SENDO PARCELADO, MAS O alterou A OPÇÃO DE PRIMEIRA PARCELA PAGA*************************** */
            if (!$movimentacaobancaria->avista && !$movimentacaobancariaAnterior->avista) {

                $this->loadModel('Movimentacoesfuturas');

                if (!$movimentacaobancaria->primeiraparcelapaga && $primeiraparcela->pago) {
                    $saldoatual = $this->desfazerpagamento($movimentacaobancariaAnterior, true);
                    $primeiraparcela->pago = false;
                    $this->Movimentacoesfuturas->save($primeiraparcela);
                }

                if ($movimentacaobancaria->primeiraparcelapaga && !$primeiraparcela->pago) {
                    $saldoatual = $this->efetuarpagamento($movimentacaobancaria, true);
                    $primeiraparcela->pago = true;
                    $this->Movimentacoesfuturas->save($primeiraparcela);
                }

                //se continua sendo pago, mas o cliente mudou o valor da movimentação, então tem que mudar o valor que debitou no saldo bancário
                if ($movimentacaobancaria->primeiraparcelapaga && $primeiraparcela->pago && ($movimentacaobancaria->dirty('valor') || $movimentacaobancaria->dirty('valorparcela'))) {
                    $this->desfazerpagamento($movimentacaobancariaAnterior, true);
                    $saldoatual = $this->efetuarpagamento($movimentacaobancaria, true);
                }

                //reprogramar as parcelas, caso tenha modificado o numero de parcelas e o valor de cada
                if ($movimentacaobancaria->dirty('num_parcelas') || $movimentacaobancaria->dirty('valor') || $movimentacaobancaria->dirty('valorparcela') || $movimentacaobancaria->dirty('diaprogramado')) {
                    $this->desfazerparcelamento($movimentacaobancariaAnterior);
                    $movimentacaobancaria->movimentacoesfuturas = $this->programarparcelas($movimentacaobancaria);
                }
            }

            /*             * ************SE O PAGAMENTO É PARCELADO, e anteriormente era à vista*************************** */
            if (!$movimentacaobancaria->avista && $movimentacaobancariaAnterior->avista) {

                $movimentacaobancaria->movimentacoesfuturas = $this->programarparcelas($movimentacaobancaria);

                if ($movimentacaobancariaAnterior->pago) {
                    $saldoatual = $this->desfazerpagamento($movimentacaobancariaAnterior);
                }
                /*                 * **********SE O PAGAMENTO FOI PARCELADO E JÁ PAGOU A PRIMEIRA PARCELA*************************** */
                if ($movimentacaobancaria->primeiraparcelapaga) {
                    $saldoatual = $this->efetuarpagamento($movimentacaobancaria, true);
                }
            }

            $movimentacaobancaria->saldo_posterior = $saldoatual; // se não houve pagamento à vista e nem de parcela, então o saldo posterior será igual ao saldo atual

            /*             * ************SALVA O DOCUMENTO NO SERVIDOR E O REGISTRO NO BANCO*************************** */
            if (!empty($this->request->data['file']['name'])) {
                $this->uploadDocument($this->request->data['file']);
                $movimentacaobancaria->documento_id = $this->saveDocument();
            }

            if ($this->Movimentacaobancarias->save($movimentacaobancaria)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $movimentacaotipos = $this->Movimentacaobancarias->Movimentacaotipos->find('list');
        $tiporeceitas = $this->Movimentacaobancarias->Tiporeceitas->find('list')->where(['Tiporeceitas.empresa_id' => $this->Auth->user('empresa_id')]);
        $tipodespesas = $this->Movimentacaobancarias->Tipodespesas->find('list')->where(['Tipodespesas.empresa_id' => $this->Auth->user('empresa_id')]);
        $fornecedores = $this->Movimentacaobancarias->Fornecedores->find('list')->where(['Fornecedores.empresa_id' => $this->Auth->user('empresa_id')]);
        $clientes = $this->Movimentacaobancarias->Clientes->find('list')->where(['Clientes.empresa_id' => $this->Auth->user('empresa_id')]);
        $contabancarias = $this->Movimentacaobancarias->Contabancarias->find('listDetails')
                ->where(['Contabancarias.empresa_id' => $this->Auth->user('empresa_id')])
                ->andWhere(['Contabancarias.cliente_id is null', 'Contabancarias.fornecedor_id is null']);
        $formaspagamentos = $this->Movimentacaobancarias->Formaspagamentos->find('list');
        $this->set(compact('movimentacaobancaria', 'movimentacaotipos', 'fornecedores', 'clientes', 'contabancarias', 'formaspagamentos', 'tiporeceitas', 'tipodespesas'));
        $this->set('_serialize', ['movimentacaobancaria']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Movimentacaobancaria id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $movimentacaobancaria = $this->Movimentacaobancarias->get($id);
        if ($movimentacaobancaria->empresa_id == $this->Auth->user('empresa_id')) {
            if ($this->Movimentacaobancarias->delete($movimentacaobancaria)) {
                $this->Flash->success(__('O registro foi removido com sucesso.'));
            } else {
                $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
            }
        } else {
            $this->Flash->error(__('Você não tem permissão de deletar um registro que não pertence à sua Empresa'));
        }

        return $this->redirect($this->request->referer());
    }

    public function desfazerparcelamento($movimentacaobancaria) {
        if ($movimentacaobancaria->empresa_id == $this->Auth->user('empresa_id')) {
            $this->Movimentacaobancarias->Movimentacoesfuturas->deleteAll(['movimentacaobancaria_id' => $movimentacaobancaria->id]);
        } else {
            $this->Flash->error(__('Você não tem permissão de deletar um registro que não pertence à sua Empresa'));
        }
    }

    public function efetuarpagamento($movimentacaobancaria, $parcela = false) {

        $calcular = $this->Movimentacaobancarias->Movimentacaotipos->find()->select('evento')->where(['Movimentacaotipos.id' => $movimentacaobancaria->movimentacaotipo_id])->first();
        $contabancariaTable = TableRegistry::get('Contabancarias');
        $contabancaria = $contabancariaTable->get($movimentacaobancaria->contabancaria_id);

        $valor = $parcela ? $movimentacaobancaria->valorparcela : $movimentacaobancaria->valor;

        if ($calcular->evento == 'sub') {
            $contabancaria->valorcaixa = $contabancaria->valorcaixa - $valor;
        } else {
            $contabancaria->valorcaixa = $contabancaria->valorcaixa + $valor;
        }

        if ($contabancariaTable->save($contabancaria)) {
            $this->Flash->success(__('Saldo da Conta Bancária Atualizado.'));
        } else {
            $this->Flash->error(__('Houve um erro ao atualiza o saldo da conta, contate o administrador.'));
        }

        return $contabancaria->valorcaixa;
    }

    public function desfazerpagamento($movimentacaobancaria, $parcela = false) {

        $calcular = $this->Movimentacaobancarias->Movimentacaotipos->find()->select('evento')->where(['Movimentacaotipos.id' => $movimentacaobancaria->movimentacaotipo_id])->first();
        $contabancariaTable = TableRegistry::get('Contabancarias');
        $contabancaria = $contabancariaTable->get($movimentacaobancaria->contabancaria_id);

        $valor = $parcela ? $movimentacaobancaria->valorparcela : $movimentacaobancaria->valor;

        if ($calcular->evento == 'sub') {
            $contabancaria->valorcaixa = $contabancaria->valorcaixa + $valor;
        } else {
            $contabancaria->valorcaixa = $contabancaria->valorcaixa - $valor;
        }

        if ($contabancariaTable->save($contabancaria)) {
            $this->Flash->success(__('A movimentação financeira anterior foi desfeita'));
        } else {
            $this->Flash->error(__('Houve um erro ao atualiza o saldo da conta, contate o administrador.'));
        }

        return $contabancaria->valorcaixa;
    }

    public function programarparcelas($movimentacaobancaria) {
        $movimentacoesfuturasTable = TableRegistry::get('Movimentacoesfuturas');
        $parcelas = array();

        $mes = date('m');
        $ano = date('Y');
        $dia = $movimentacaobancaria->diaprogramado;
        for ($i = 1; $i <= $movimentacaobancaria->num_parcelas; $i++) {
            $movimentacao = $movimentacoesfuturasTable->newEntity();
            $movimentacao->valorparcela = $movimentacaobancaria->valorparcela;
            if ($movimentacaobancaria->primeiraparcelapaga && $i == 1) {// registra a primeira parcela paga
                $movimentacao->dt_programada = date("$ano-$mes-d");
                $movimentacao->pago = 1;
            } else {
                $movimentacao->dt_programada = date("$ano-$mes-$dia");
            }

            $ano = $mes < 12 ? $ano : ++$ano;
            $mes = $mes < 12 ? ++$mes : '01';
            $parcelas[] = $movimentacao;
        }
        return $parcelas;
    }

    /**
     * uploadDocument method
     *
     * @return string $filename.
     */
    public function uploadDocument($file) {
        $setNewFileName = time() . "_" . rand(000000, 999999);
        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
        move_uploaded_file($file['tmp_name'], WWW_ROOT . '/docs/' . $setNewFileName . '.' . $ext);
        return $setNewFileName . '.' . $ext;
    }

    /**
     * uploadDocument method
     *
     * @return string $filename.
     */
    public function saveDocument() {
        $this->loadModel('Documentos');
        $documento = $this->Documentos->newEntity();
        $documento->tipodocumento_id = $this->_TPDOCUMENTO_REGISTRONOTAFISCAL;
        $documento->dt_enviado = date('Y-m-d H:i:s');
        $documento->empresa_id = $this->Auth->user('empresa_id');
        $documento->nome = 'Registro de Livro Caixa';
        $documento->descricao = 'Lançamento de Nota Fiscal/Recibo no Livro Caixa em ' . date('Y-m-d H:i:s');
        $documento->file = $documento->user_enviou = $this->Auth->user('id');

        if ($this->Documentos->save($documento)) {
            return $documento->id;
        } else {
            $this->Flash->error(__('Não foi possível anexar o documento, tente novamente.'));
        }
    }

}
