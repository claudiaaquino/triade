<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Client;

/**
 * Tiposervicos Controller
 *
 * @property \App\Model\Table\TiposervicosTable $Tiposervicos
 */
class TiposervicosController extends AppController {

    public function beforeFilter(Event $event) {
        if ($this->request->params['action'] == 'adddocinstrucao') {//seta a sessão
            $this->request->session()->id($this->request['pass'][0]);
            $this->request->session()->start();
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Gruposervicos.Areaservicos']
        ];


        if ($this->request->is('post') && !empty($this->request->data('termo-pesquisa'))) {
            $query = $this->Tiposervicos->find()->where(["Tiposervicos.nome like '%" . $this->request->data('termo-pesquisa') . "%'"]);
        } else {
            $query = $this->Tiposervicos->find();
        }
        $tiposervicos = $this->paginate($query->orderAsc('Tiposervicos.nome'));


        $this->set(compact('tiposervicos'));
        $this->set('_serialize', ['tiposervicos']);
    }

    /**
     * ajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function ajax($gruposervico_id = null) {

        $retorno = $this->Tiposervicos->find('list')->where(['gruposervico_id' => $gruposervico_id]);


        $solicitavel = $this->Auth->user('user_triade') ? false : true; // usuario da triade pode ver qualquer serviço na caixa de seleção. porém os clientes só podem ver os serviços solicitaveis
        if ($solicitavel) {
            $retorno->where(['solicitavel = 1']);
        }


        $retorno->order(['descricao' => 'ASC']);
        $tpview = 'list';
        $this->set(compact('retorno', 'tpview'));
        $this->set('_serialize', ['retorno', 'tpview']);
    }

    /**
     * View method
     *
     * @param string|null $id Tiposervico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tiposervico = $this->Tiposervicos->get($id, [
            'contain' => ['Gruposervicos.Areaservicos', 'Previsaoorcamentos', 'Telaquestionarios']
        ]);

        $this->set('tiposervico', $tiposervico);
        $this->set('_serialize', ['tiposervico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $tiposervico = $this->Tiposervicos->newEntity();
        if ($this->request->is('post')) {
            $tiposervico = $this->Tiposervicos->patchEntity($tiposervico, $this->request->data);
            $tiposervico->dt_cadastro = date('Y-m-d H:i:s');
            $tiposervico->user_id = $this->Auth->user('id');
            $tiposervico->empresa_id = $this->Auth->user('empresa_id');
            if (!empty($tiposervico->mes))
                $tiposervico->meses = json_encode($tiposervico->mes);

            $tiposervico->valor_servico = $this->formatarmoedaTomysql($tiposervico->valor_servico);
            $tiposervico->valor_adicionalmensal = $this->formatarmoedaTomysql($tiposervico->valor_adicionalmensal);


            $users = array();
            if ($tiposervico->user_ids) {
                foreach ($tiposervico->user_ids as $user) {
                    $servicousuario = $this->Tiposervicos->Userservicos->newEntity();
                    $servicousuario->empresa_id = $this->Auth->user('empresa_id');
                    $servicousuario->user_id = $user;
                    $servicousuario->dt_cadastro = date('Y-m-d H:i:s');
                    $users[] = $servicousuario;
                }
            }

            $tiposervico->userservicos = $users;



            $empresas = array();
            if ($tiposervico->empresa_ids) {
                foreach ($tiposervico->empresa_ids as $empresa) {
                    $servicoempresa = $this->Tiposervicos->Empresaservicos->newEntity();
                    $servicoempresa->empresa_id = $empresa;
                    $servicoempresa->user_id = $this->Auth->user('id');
                    $servicoempresa->dt_cadastro = date('Y-m-d H:i:s');
                    $empresas[] = $servicoempresa;
                }
            }
            $tiposervico->empresaservicos = $empresas;


            if ($this->Tiposervicos->save($tiposervico)) {

                //para gerar tarefas automaticamente ao salvar o novo serviço
                $http = new Client();
                $http->get('https://sistema.triadeconsultoria.adv.br/tarefas/gerartarefas');

                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'edit', $tiposervico->id]);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $tiposervico->valor_servico = $tiposervico->valor_servico ? str_replace('.', ',', $tiposervico->valor_servico) : '';
        $tiposervico->valor_adicionalmensal = $tiposervico->valor_adicionalmensal ? str_replace('.', ',', $tiposervico->valor_adicionalmensal) : '';


        $empresas = $this->Tiposervicos->Empresaservicos->Empresas->find('list');
        $areaservicos = $this->Tiposervicos->Gruposervicos->Areaservicos->find('list')->where(['empresa_id' => $this->_ID_TRIADE]);
        $gruposervicos = $this->Tiposervicos->Gruposervicos->find('list');
        $users = $this->Tiposervicos->Userservicos->Users->find('list')->where(['empresa_id' => $this->_ID_TRIADE]);
        $meses = array(1 => 'Janeiro', 2 => 'Fevereiro', 3 => 'Março', 4 => 'Abril', 5 => 'Maio', 6 => 'Junho', 7 => 'Julho', 8 => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro', 12 => 'Dezembro');
        $formaspagamentoservicos = $this->Tiposervicos->Formaspagamentoservicos->find('list');
        $telas = $this->Tiposervicos->Telas->find('list')->where(['flag_servico = 1 ']);


        $this->set(compact('tiposervico', 'gruposervicos', 'areaservicos', 'users', 'empresas', 'meses', 'formaspagamentoservicos', 'telas'));
        $this->set('_serialize', ['tiposervico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tiposervico id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tiposervico = $this->Tiposervicos->get($id, [
            'contain' => ['Gruposervicos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tiposervico = $this->Tiposervicos->patchEntity($tiposervico, $this->request->data);


            $tiposervico->valor_servico = $this->formatarmoedaTomysql($tiposervico->valor_servico);
            $tiposervico->valor_adicionalmensal = $this->formatarmoedaTomysql($tiposervico->valor_adicionalmensal);


            $tiposervico->meses = json_encode($tiposervico->mes);

            $userscadastrados = $this->Tiposervicos->Userservicos->find()->where(['tiposervico_id' => $id])->extract('user_id')->filter()->toArray();
            $users = array();
            if ($tiposervico->user_ids) {
                foreach ($tiposervico->user_ids as $user) {
                    if (!in_array($user, $userscadastrados)) {
                        $servicousuario = $this->Tiposervicos->Userservicos->newEntity();
                        $servicousuario->empresa_id = $this->Auth->user('empresa_id');
                        $servicousuario->user_id = $user;
                        $servicousuario->dt_cadastro = date('Y-m-d H:i:s');
                        $users[] = $servicousuario;
                    }
                }
                $this->Tiposervicos->Userservicos->deleteAll(['tiposervico_id' => $id, 'user_id NOT IN' => $tiposervico->user_ids]);
            } else {
                $this->Tiposervicos->Userservicos->deleteAll(['tiposervico_id' => $id]);
            }
            $tiposervico->userservicos = $users;




            $empresascadastradas = $this->Tiposervicos->Empresaservicos->find()->where(['tiposervico_id' => $id])->extract('empresa_id')->filter()->toArray();
            $empresas = array();
            if ($tiposervico->empresa_ids) {
                foreach ($tiposervico->empresa_ids as $empresa) {
                    if (!in_array($empresa, $empresascadastradas)) {
                        $servicoempresa = $this->Tiposervicos->Empresaservicos->newEntity();
                        $servicoempresa->empresa_id = $empresa;
                        $servicoempresa->user_id = $this->Auth->user('id');
                        $servicoempresa->dt_cadastro = date('Y-m-d H:i:s');
                        $empresas[] = $servicoempresa;
                    }
                }
                $this->Tiposervicos->Empresaservicos->deleteAll(['tiposervico_id' => $id, 'empresa_id NOT IN' => $tiposervico->empresa_ids]);
            } else {
                $this->Tiposervicos->Empresaservicos->deleteAll(['tiposervico_id' => $id]);
            }
            $tiposervico->empresaservicos = $empresas;

            if ($this->Tiposervicos->save($tiposervico)) {
                //para gerar tarefas automaticamente ao salvar o novo serviço
                $http = new Client();
                $http->get('https://sistema.triadeconsultoria.adv.br/tarefas/gerartarefas');
//                $http->get('http://localhost/tarefas/gerartarefas');

                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'edit', $tiposervico->id]);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $tiposervico->valor_servico = $tiposervico->valor_servico ? str_replace('.', ',', $tiposervico->valor_servico) : '';
        $tiposervico->valor_adicionalmensal = $tiposervico->valor_adicionalmensal ? str_replace('.', ',', $tiposervico->valor_adicionalmensal) : '';


        $selectedmeses = json_decode($tiposervico->meses);

        $this->loadModel('Users');
        $selectedusers = array_keys($this->Users->find('list')->innerJoinWith('Userservicos')->where(['Userservicos.tiposervico_id' => $id])->toArray());
        $users = $this->Tiposervicos->Userservicos->Users->find('list')->where(['empresa_id' => $this->_ID_TRIADE]);

        $this->loadModel('Empresas');
        $empresas = $this->Tiposervicos->Empresaservicos->Empresas->find('list');
        $selectedempresas = array_keys($this->Empresas->find('list')->innerJoinWith('Empresaservicos')->where(['Empresaservicos.tiposervico_id' => $id])->toArray());

        $tiposervico->areaservico_id = $tiposervico->gruposervico->areaservico_id;
        $areaservicos = $this->Tiposervicos->Gruposervicos->Areaservicos->find('list')->where(['empresa_id' => $this->_ID_TRIADE]);
        $gruposervicos = $this->Tiposervicos->Gruposervicos->find('list');
        $meses = array(1 => 'Janeiro', 2 => 'Fevereiro', 3 => 'Março', 4 => 'Abril', 5 => 'Maio', 6 => 'Junho', 7 => 'Julho', 8 => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro', 12 => 'Dezembro');
        $formaspagamentoservicos = $this->Tiposervicos->Formaspagamentoservicos->find('list');
        $telas = $this->Tiposervicos->Telas->find('list')->where(['flag_servico = 1 ']);

        $this->set(compact('tiposervico', 'gruposervicos', 'areaservicos', 'users', 'selectedusers', 'empresas', 'selectedempresas', 'meses', 'selectedmeses', 'formaspagamentoservicos', 'telas'));
        $this->set('_serialize', ['tiposervico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tiposervico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $tiposervico = $this->Tiposervicos->get($id);
        if ($this->Tiposervicos->delete($tiposervico)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

    /**
     * Adddocumento method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function adddocinstrucao() {

        if (!empty($this->request->data['Filename'])) {
            $file = $this->request->data['Filedata']; //put the data into a var for easy use
            $filename = $file['name'];
            if (!is_dir(WWW_ROOT . '/docs/instrucao_servicos/' . $this->request->data('tiposervico_id'))) {
                mkdir(WWW_ROOT . '/docs/instrucao_servicos/' . $this->request->data('tiposervico_id'));
            }
            $retorno = move_uploaded_file($file['tmp_name'], WWW_ROOT . '/docs/instrucao_servicos/' . $this->request->data('tiposervico_id') . '/' . $filename);
        }

        $this->set('tpview', 'add');
        $this->set('retorno', $retorno);
        $this->viewBuilder()->layout('ajax');
        $this->render('ajax');
    }

    public function listdocsinstrucao($tiposervico_id) {
        $documentos = array();
        $diretorio = WWW_ROOT . 'docs' . DS . 'instrucao_servicos' . DS . $tiposervico_id;

        if (is_dir($diretorio)) {
            $documentos = array_diff(scandir($diretorio), array('..', '.'));
        }
        $diretorio = 'docs/instrucao_servicos/' . $tiposervico_id;
        $this->set(compact('documentos', 'diretorio'));
        $this->set('_serialize', ['documentos', 'diretorio']);
    }

    public function deletedocumento($tiposervico_id) {

        $file = WWW_ROOT . 'docs' . DS . 'instrucao_servicos' . DS . $tiposervico_id . DS . $this->request->data('documento-nome');
        if (file_exists($file)) {
            $retorno = unlink($file);
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

}
