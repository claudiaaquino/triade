<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\I18n\Time;

/**
 * Servicos Controller
 *
 * @property \App\Model\Table\ServicosTable $Servicos
 */
class ServicosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Areaservicos', 'Tiposervicos', 'Empresas']
        ];

        /*         * se for usuario da triade perfil admin ou admin setores */
        if ($this->Auth->user('admin') || (($this->Auth->user('admin_empresa') || $this->Auth->user('admin_setores')) && $this->Auth->user('user_triade'))) {
            $servicos = $this->paginate($this->Servicos->find()->orderDesc('Servicos.dt_cadastro'));
        } else if (($this->Auth->user('admin_empresa') || $this->Auth->user('admin_setores')) && $this->Auth->user('empresa_id') != $this->_ID_TRIADE) {
            /*             * se for usuario de outras empresas */
            $servicos = $this->paginate($this->Servicos->find()->where(['Servicos.empresa_id' => $this->Auth->user('empresa_id')])->orderDesc('Servicos.dt_cadastro'));
        }


        $this->set(compact('servicos'));
        $this->set('_serialize', ['servicos']);
    }

    /**
     * View method
     *
     * @param string|null $id Servico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $servico = $this->Servicos->get($id, [
            'contain' => ['Areaservicos', 'Tiposervicos', 'Empresas', 'Users', 'Empresasocios', 'Funcionarios', 'Tarefas.Tarefausuarios.Users', 'Tarefas.Tarefadocumentos.Documentos.Tipodocumentos']
        ]);

        $this->set('servico', $servico);
        $this->set('_serialize', ['servico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $servico = $this->Servicos->newEntity();
        if ($this->request->is('post')) {
            $servico = $this->Servicos->patchEntity($servico, $this->request->data);
            $servico->dt_cadastro = date('Y-m-d H:i:s');
            $servico->user_id = $this->Auth->user('id');
            $servico->empresa_id = $this->Auth->user('empresa_id');
            $servico->status_servico = 1; //solicitado


            $orcamento = $this->calculavalorservico($servico->tiposervico_id);

            //se houver um orçamento parametrizado  e redirecionamento de tela, o sistema vai tratar diferente.... 
            //se não tiver o sistema vai notificar o recebimento da solicitação de serviço pro cliente e p triade por tela e por email.
            if (!empty($orcamento)) {
                if ($orcamento->gratis) {
                    $servico->valor_adicionalmensal = 0;
                    $servico->valor_servico = 0;
                } else {
                    $servico->valor_adicionalmensal = $orcamento->valor_adicionalmensal;
                    $servico->valor_servico = $orcamento->valor_servico;
                }
                $servico->prazo_entrega = $orcamento->tempo_execucao;

                $this->loadModel('Telas');
                $tela_redirecionamento = $this->Telas->find()->where(['Telas.id' => $orcamento->tela_id])->first();
            }

            if ($this->Servicos->save($servico)) {
                $tiposervico = $this->Servicos->Tiposervicos->find()->contain(['Gruposervicos', 'Userservicos.Users' => array('conditions' => ['Userservicos.empresa_id' => $this->_ID_TRIADE])])->where(['Tiposervicos.id' => $servico->tiposervico_id])->first();

                /** Atribui essa tarefa para o funcionario da triade determinado para esse tipo de serviço */
                $this->atribuiServicoFuncionario($servico, $tiposervico);

                /**                  colocar exceção solicitarabertura, já que ele envia um email específico depois, com informações da empresa solicitada */
                if (empty($tela_redirecionamento) || (!empty($tela_redirecionamento) && $tela_redirecionamento->action != 'solicitarabertura')) {
                    /** Notifica a triade que foi solicitado um novo serviço */
                    $this->disparaEmailSolicitacaoServicoTriade($servico, $tiposervico);
                    /** Avisa o Cliente que a sokicitação foi recebida */
                    $this->disparaEmailSolicitacaoServicoCliente($servico, $tiposervico);
                }

                /** Se houver mais etapas para cadastrar esse serviço, o sistema seguirá a diante */
                if (!empty($tela_redirecionamento)) {
                    return $this->redirect(['controller' => $tela_redirecionamento->controller, 'action' => $tela_redirecionamento->action]);
                }

                $this->Flash->success('Recebemos sua solicitação de serviço. Em breve a Tríade entrará em contato para dar prosseguimento na execução desse serviço. Verifique sempre o seu email e também a caixa de Spam.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao fazer essa solicitação, verifique os campos e tente novamente.'));
            }
        }
        $areaservicos = $this->Servicos->Areaservicos->find('list')->where(['todos = 1']);
        $empresasocios = $this->Servicos->Empresasocios->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')]);
        $funcionarios = $this->Servicos->Funcionarios->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')]);
        $this->set(compact('servico', 'areaservicos', 'tiposervicos', 'empresas', 'users', 'empresasocios', 'funcionarios'));
        $this->set('_serialize', ['servico']);
    }

    /**
     * Calculaorcamento method
     *
     * @return \Cake\Network\Response|null
     */
    public function calculavalorservico($tiposervico_id) {
        $this->loadModel('Tiposervicos');
        $orcamento = $this->Tiposervicos->find()->where(['id' => $tiposervico_id])->first();

        if (!empty($orcamento)) {
            $orcamento->gratis = $this->isServicoGratuito($orcamento);
            $orcamento->mensalidade_atual = $this->Auth->user('empresa.mensalidade');
        }

        return $orcamento;
    }

    public function isServicoGratuito($orcamento) {
        if ($orcamento->gratis_todosusuarios) {
            if ($orcamento->qtdeilimitadagratis) {
                return true;
            } else {
                // fazer pesquisa de gratuidade por periodo, seja mes, ano ...etc
                if ($this->Auth->user('empresa_id')) {
                    $count = $this->Servicos->find()->where(['tiposervico_id' => $orcamento->tiposervico_id, 'empresa_id' => $this->Auth->user('empresa_id')])->count();
                } else {
                    $count = $this->Servicos->find()->where(['tiposervico_id' => $orcamento->tiposervico_id, 'user_id' => $this->Auth->user('id')])->count();
                }
                if ($count < $orcamento->qtdegratis) {
                    return true;
                }
            }
        } else if ($orcamento->gratis_todoscliente && !empty($this->Auth->user('empresa_id'))) {
            if ($orcamento->qtdeilimitadagratis) {
                return true;
            } else {
                // fazer pesquisa de gratuidade por periodo, seja mes, ano ...etc
                $count = $this->Servicos->find()->where(['tiposervico_id' => $orcamento->tiposervico_id, 'empresa_id' => $this->Auth->user('empresa_id')])->count();
            }
            if ($count < $orcamento->qtdegratis) {
                return true;
            }
        }

        return false;
    }

    /**
     * Edit method
     *
     * @param string|null $id Servico id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        if ($this->Auth->user('admin') || (($this->Auth->user('admin_empresa') || $this->Auth->user('admin_setores')) && $this->Auth->user('user_triade')) || $this->Auth->user('user_triade')) {

            $servico = $this->Servicos->get($id, [
                'contain' => ['Tiposervicos']
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $servico = $this->Servicos->patchEntity($servico, $this->request->data);

                $servico->valor_servico = $this->formatarmoedaTomysql($servico->valor_servico);
                $servico->valor_adicionalmensal = $this->formatarmoedaTomysql($servico->valor_adicionalmensal);

                if ($this->Servicos->save($servico)) {
                    $this->Flash->success(__('O registro foi atualizado com sucesso'));

                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
                }
            }

            $servico->valor_servico = $servico->valor_servico ? str_replace('.', ',', $servico->valor_servico) : '';
            $servico->valor_adicionalmensal = $servico->valor_adicionalmensal ? str_replace('.', ',', $servico->valor_adicionalmensal) : '';


            $servico->gruposervico_id = $servico->tiposervico->gruposervico_id;
            $areaservicos = $this->Servicos->Areaservicos->find('list');
            $gruposervicos = $this->Servicos->Tiposervicos->Gruposervicos->find('list');
            $tiposervicos = $this->Servicos->Tiposervicos->find('list');
            $empresasocios = $this->Servicos->Empresasocios->find('list')->where(['Empresasocios.empresa_id' => $servico->empresa_id]);
            $funcionarios = $this->Servicos->Funcionarios->find('list')->where(['Funcionarios.empresa_id' => $servico->empresa_id]);
            $this->set(compact('servico', 'areaservicos', 'tiposervicos', 'gruposervicos', 'empresasocios', 'funcionarios'));
            $this->set('_serialize', ['servico']);
        } else {
            $this->Flash->error(__('Você não tem permissão para editar um serviço.'));
            return $this->redirect($this->request->referer());
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Servico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $servico = $this->Servicos->get($id);
        if ($this->Servicos->delete($servico)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /** Atribui esse serviço como uma tarefa para o funcionário especifico q faz esse tipo de serviço */
    public function atribuiServicoFuncionario($servico, $tiposervico) {
        $this->loadModel('Tarefas');
        $tarefa = $this->Tarefas->newEntity();
        $tarefa->titulo = 'Solicitação de Serviço: ' . $tiposervico->nome;
        $tarefa->tiposervico_id = $tiposervico->id;
        $tarefa->servico_id = $servico->id;
        $tarefa->empresa_cadastrou = 1;
        $tarefa->user_id = 1;

        $tarefa->descricao = 'Você foi atribuido para fazer esse serviço solicitado pelo sistema. Siga as instruções de como executar esse serviço.';

        if ($this->Auth->user('empresa_id')) {
            $tarefa->empresa_id = $this->Auth->user('empresa_id');
        }

        if ($tiposervico->gruposervico && $tiposervico->gruposervico->areaservico_id) {
            $tarefa->areaservico_id = $tiposervico->gruposervico->areaservico_id;
        }


        $now = Time::now();
        $tarefa->dt_cadastro = $now->toDateTimeString();
        $tarefa->dt_inicio = $now->toDateTimeString();
        if ($servico->prazo_entrega) {
            $tarefa->dt_final = $now->addDays($servico->prazo_entrega)->toDateTimeString();
        } else {
            $tarefa->dt_final = $now->addDays(5)->toDateTimeString();
        }
        $tarefa->dt_lembrete = $tarefa->dt_final;


//tipo de tarefa - cmpo obrigatorio
        $tipotarefa = $this->Tarefas->Tarefatipos->find()->where(['descricao' => 'Serviço Solicitado Pelo Cliente'])->first();
        if (empty($tipotarefa)) {
            $tipotarefa = $this->Tarefas->Tarefatipos->newEntity();
            $tipotarefa->descricao = 'Serviço Solicitado Pelo Cliente';
            $tipotarefa->empresa_id = $this->_ID_TRIADE;
            $tipotarefa->user_id = 1;
            $tipotarefa->todos_empresa = 1;
            $tipotarefa->dt_cadastro = date('Y-m-d H:i:s');
            $this->Tarefas->Tarefatipos->save($tipotarefa);
        }
        $tarefa->tarefatipo_id = $tipotarefa->id;



        //prioridade  
        $prioridade = $this->Tarefas->Tarefaprioridades->find()->where(["descricao" => 'Normal'])->first();
        if (empty($prioridade)) {
            $prioridade = $this->Tarefas->Tarefaprioridades->newEntity();
            $prioridade->descricao = 'Normal';
            $prioridade->ordem = 4;
            $prioridade->dt_cadastro = date('Y-m-d H:i:s');
            $this->Tarefas->Tarefaprioridades->save($prioridade);
        }
        $tarefa->tarefaprioridade_id = $prioridade->id;



        $users = array();
        if ($tiposervico->has('userservicos')) {
            foreach ($tiposervico->userservicos as $userservico) {
                $user = $this->Tarefas->Tarefausuarios->newEntity();
                $user->user_id = $userservico->user_id;
                $users[] = $user;
            }
        }
        $tarefa->tarefausuarios = $users;





        if ($this->Tarefas->save($tarefa)) {
            $this->disparaEmailNovoServicoResponsavel($tarefa, $tiposervico);
        }
    }

    /** Apenas atualiza o status do serviço */
    public function updatestatus($id) {
        $servico = $this->Servicos->get($id);
        $servico->status_servico = $this->request->data('statusservico');
        if ($this->Servicos->save($servico)) {
            $retorno = $servico->status_servico;
        } else {
            $retorno = null;
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /** Verifique quando tempo demora para executar o serviço 
     */
    public function verificaprazoexecucao($id) {
        $tiposervico = $this->Servicos->Tiposervicos->get($id);
        $retorno = $tiposervico->tempo_execucao;
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     *  Dispara email para o administrador da triade informando que foi solicitado um novo serviço para a triade */
    public function disparaEmailSolicitacaoServicoTriade($servico, $tiposervico) {
        try {
            $email = new Email('default');
            $email->viewVars(['servico' => $servico, 'tiposervico' => $tiposervico, 'cliente' => $this->Auth->user()]);
            $email->subject('[SISTEMA] Nova Solicitação de Serviço')
                    ->template('default', 'novasolicitacaoservico')
                    ->emailFormat('html')
                    ->to($this->_EMAIL_ADMIN)
                    ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

    /**
     *  Dispara email para o cliente informando que a triade recebeu a solicitação dele */
    public function disparaEmailSolicitacaoServicoCliente($servico, $tiposervico) {
        try {
            $email = new Email('default');
            $email->viewVars(['servico' => $servico, 'tiposervico' => $tiposervico, 'nomecliente' => $this->Auth->user('nome')]);
            $email->subject('[SISTEMA TRÍADE] Solicitação de Serviço Recebida')
                    ->template('default', 'confirmasolicitacaoservico')
                    ->emailFormat('html')
                    ->to($this->Auth->user('email'))
                    ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

    /**
     *  Dispara email para o cliente informando que a triade recebeu a solicitação dele */
    public function disparaEmailNovoServicoResponsavel($tarefa, $tiposervico) {
        try {
            if ($tiposervico->userservicos) {

                foreach ($tiposervico->userservicos as $userservico) {
                    $email = new Email('default');
                    $email->viewVars(['tarefa' => $tarefa, 'tiposervico' => $tiposervico, 'nomeresponsavel' => $userservico->user->nome, 'cliente' => $this->Auth->user()]);
                    $email->subject('[SISTEMA TRÍADE] Novo Serviço Atribuido')
                            ->template('default', 'novatarefasolicitada')
                            ->emailFormat('html')
                            ->to($userservico->user->email)
                            ->send();
                }
            }
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

}
