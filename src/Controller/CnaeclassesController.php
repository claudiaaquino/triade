<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Cnaeclasses Controller
 *
 * @property \App\Model\Table\CnaeclassesTable $Cnaeclasses
 */
class CnaeclassesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Cnaegroups']
        ];
        
        $cnaeclasses = $this->paginate($this->Cnaeclasses);

        $this->set(compact('cnaeclasses'));
        $this->set('_serialize', ['cnaeclasses']);
    }

    /**
     * Listajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function listajax($group_id) {
        $retorno = $this->Cnaeclasses->find('ajaxList')->where(['Cnaeclasses.cnaegroup_id' => $group_id]);
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * View method
     *
     * @param string|null $id Cnaeclass id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $cnaeclass = $this->Cnaeclasses->get($id, [
            'contain' => ['Empresacnaes', 'Empresacnaes.Empresas', 'Cnaegroups']
        ]);

        $this->set('cnaeclass', $cnaeclass);
        $this->set('_serialize', ['cnaeclass']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $cnaeclass = $this->Cnaeclasses->newEntity();
        if ($this->request->is('post')) {
            $cnaeclass = $this->Cnaeclasses->patchEntity($cnaeclass, $this->request->data);
            if ($this->Cnaeclasses->save($cnaeclass)) {
                $this->Flash->success(__('Classe cadastrada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao cadastrar. Por favor, tente novamente.'));
            }
        }
        
        $cnaegroups = $this->Cnaeclasses->Cnaegroups->find('list', ['limit' => 200]);
        $this->set(compact('cnaeclass', 'cnaegroups'));
        $this->set('_serialize', ['cnaeclass']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cnaeclass id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $cnaeclass = $this->Cnaeclasses->get($id, [
            'contain' => []
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cnaeclass = $this->Cnaeclasses->patchEntity($cnaeclass, $this->request->data);
            if ($this->Cnaeclasses->save($cnaeclass)) {
                $this->Flash->success(__('Classe atualizada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        
        $cnaegroupslist = $this->Cnaeclasses->Cnaegroups->find('list', ['limit' => 200]);
        $this->set(compact('cnaeclass', 'cnaegroupslist'));
        $this->set('_serialize', ['cnaeclass']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cnaeclass id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $cnaeclass = $this->Cnaeclasses->get($id);
        if ($this->Cnaeclasses->delete($cnaeclass)) {
            $this->Flash->success(__('Classe removida com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

}
