<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Contabancarias Controller
 *
 * @property \App\Model\Table\ContabancariasTable $Contabancarias
 */
class ContabancariasController extends AppController {

    public function isAuthorized($user) {
        if (!$this->Auth->user('admin') && $this->Auth->user('empresa_id') != $this->_ID_TRIADE && !$this->request->is('ajax') && !$this->Auth->user('admin_empresa') && !$this->Auth->user('admin_setores')) {
            return false;
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Empresas', 'Clientes', 'Fornecedores', 'Bancos']
        ];

        $contabancarias = $this->Contabancarias->find()->contain(['Empresas', 'Clientes', 'Fornecedores', 'Bancos'])->where(['Contabancarias.empresa_id' => $this->Auth->user('empresa_id'),
            'Contabancarias.cliente_id is null', 'Contabancarias.fornecedor_id is null']);

        $contabancariaterceiros = $this->paginate($this->Contabancarias->find()->where(['Contabancarias.empresa_id' => $this->Auth->user('empresa_id'),
                    'Contabancarias.cliente_id is not null or Contabancarias.fornecedor_id is not null']));


        $this->set(compact('contabancarias', 'contabancariaterceiros'));

        $this->set('_serialize', ['contabancarias']);
    }

    /**
     * View method
     *
     * @param string|null $id Contabancaria id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $contabancaria = $this->Contabancarias->get($id, [
            'contain' => ['Empresas', 'Clientes', 'Fornecedores', 'Bancos', 'Estados', 'Cidades',
                'Boletos', 'Movimentacaobancarias' => ['sort' => ['Movimentacaobancarias.dt_cadastro' => 'DESC']],
                'Movimentacaobancarias.Movimentacaotipos', 'Movimentacaobancarias.Fornecedores', 'Movimentacaobancarias.Clientes', 'Movimentacaobancarias.Formaspagamentos']
        ]);

        if ($contabancaria->empresa_id != $this->Auth->user('empresa_id')) {
            $this->Flash->error(__('Você não tem permissão de visualizar um registro que não pertence à sua Empresa'));
            return $this->redirect($this->referer());
        }


        $this->set('contabancaria', $contabancaria);
        $this->set('_serialize', ['contabancaria']);

        if ($contabancaria->cliente_id || $contabancaria->fornecedor_id) {
            $this->render('viewterceiros');
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $contabancaria = $this->Contabancarias->newEntity();
        if ($this->request->is('post')) {
            $contabancaria = $this->Contabancarias->patchEntity($contabancaria, $this->request->data);
            $contabancaria->dt_cadastro = date('Y-m-d H:i:s');
            $contabancaria->user_id = $this->Auth->user('id');
            $contabancaria->empresa_id = $this->Auth->user('empresa_id');
            $contabancaria->valorcaixa = $this->formatarmoedaTomysql($contabancaria->valorcaixa);

            if ($this->Contabancarias->save($contabancaria)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $contabancaria->valorcaixa = $contabancaria->valorcaixa ? str_replace('.', ',', $contabancaria->valorcaixa) : '';
        $bancos = $this->Contabancarias->Bancos->find('list');
        $estados = $this->Contabancarias->Estados->find('list');
        $this->set(compact('contabancaria', 'bancos', 'estados'));
        $this->set('_serialize', ['contabancaria']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addterceiros() {
        $contabancaria = $this->Contabancarias->newEntity();
        if ($this->request->is('post')) {
            $contabancaria = $this->Contabancarias->patchEntity($contabancaria, $this->request->data);
            $contabancaria->dt_cadastro = date('Y-m-d H:i:s');
            $contabancaria->user_id = $this->Auth->user('id');
            $contabancaria->empresa_id = $this->Auth->user('empresa_id');
            $contabancaria->cliente_id = $contabancaria->cliente_id ? $contabancaria->cliente_id : null;
            $contabancaria->fornecedor_id = $contabancaria->fornecedor_id ? $contabancaria->fornecedor_id : null;

            if ($this->Contabancarias->save($contabancaria)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $clientes = $this->Contabancarias->Clientes->find('list')->where(['Clientes.empresa_id' => $this->Auth->user('empresa_id')]);
        $fornecedores = $this->Contabancarias->Fornecedores->find('list')->where(['Fornecedores.empresa_id' => $this->Auth->user('empresa_id')]);
        $bancos = $this->Contabancarias->Bancos->find('list');
        $this->set(compact('contabancaria', 'clientes', 'fornecedores', 'bancos'));
        $this->set('_serialize', ['contabancaria']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contabancaria id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $contabancaria = $this->Contabancarias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contabancaria = $this->Contabancarias->patchEntity($contabancaria, $this->request->data);

            if ($contabancaria->empresa_id != $this->Auth->user('empresa_id')) {
                $this->Flash->error(__('Você não tem permissão de editar um registro que não pertence à sua Empresa'));
                return $this->redirect($this->referer());
            }
            $contabancaria->valorcaixa = $this->formatarmoedaTomysql($contabancaria->valorcaixa);
//            debug($contabancaria);
//            die;
            if ($this->Contabancarias->save($contabancaria)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $contabancaria->valorcaixa = $contabancaria->valorcaixa ? str_replace('.', ',', $contabancaria->valorcaixa) : '';
        $clientes = $this->Contabancarias->Clientes->find('list');
        $fornecedores = $this->Contabancarias->Fornecedores->find('list');
        $bancos = $this->Contabancarias->Bancos->find('list');
        $estados = $this->Contabancarias->Estados->find('list');
        $this->set(compact('contabancaria', 'clientes', 'fornecedores', 'bancos', 'estados'));
        $this->set('_serialize', ['contabancaria']);
    }

    /**
     * Editterceiros method
     *
     * @param string|null $id Contabancaria id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editterceiros($id = null) {
        $contabancaria = $this->Contabancarias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contabancaria = $this->Contabancarias->patchEntity($contabancaria, $this->request->data);

            if ($contabancaria->empresa_id != $this->Auth->user('empresa_id')) {
                $this->Flash->error(__('Você não tem permissão de editar um registro que não pertence à sua Empresa'));
                return $this->redirect($this->referer());
            }

            $contabancaria->cliente_id = $contabancaria->cliente_id ? $contabancaria->cliente_id : null;
            $contabancaria->fornecedor_id = $contabancaria->fornecedor_id ? $contabancaria->fornecedor_id : null;

            if ($this->Contabancarias->save($contabancaria)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $clientes = $this->Contabancarias->Clientes->find('list')->where(['Clientes.empresa_id' => $this->Auth->user('empresa_id')]);
        $fornecedores = $this->Contabancarias->Fornecedores->find('list')->where(['Fornecedores.empresa_id' => $this->Auth->user('empresa_id')]);
        $bancos = $this->Contabancarias->Bancos->find('list');
        $this->set(compact('contabancaria', 'clientes', 'fornecedores', 'bancos'));
        $this->set('_serialize', ['contabancaria']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contabancaria id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $contabancaria = $this->Contabancarias->get($id);
        if ($contabancaria->empresa_id == $this->Auth->user('empresa_id')) {

            if ($this->Contabancarias->delete($contabancaria)) {
                $this->Flash->success(__('O registro foi removido com sucesso.'));
            } else {
                $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
            }
        } else {
            $this->Flash->error(__('Você não tem permissão de deletar um registro que não pertence à sua Empresa'));
        }

        return $this->redirect($this->request->referer());
    }

}
