<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Funcionarios Controller
 *
 * @property \App\Model\Table\FuncionariosTable $Funcionarios
 */
class FuncionariosController extends AppController {

    public function isAuthorized($user) {
        if (!$this->Auth->user('admin') && $this->Auth->user('empresa_id') != $this->_ID_TRIADE && !$this->request->is('ajax') && !$this->Auth->user('admin_empresa') && !$this->Auth->user('admin_setores')) {
            return false;
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Cargos']
        ];
        $funcionarios = $this->paginate($this->Funcionarios->find()->where(['empresa_id' => $this->Auth->user('empresa_id')]));

        $this->set(compact('funcionarios'));
        $this->set('_serialize', ['funcionarios']);
    }

    /**
     * Listajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function listajax($empresa_id) {
        $retorno = $this->Funcionarios->find('list')->where(['empresa_id' => $empresa_id]);
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * View method
     *
     * @param string|null $id Funcionario id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $funcionario = $this->Funcionarios->get($id, [
            'contain' => ['Users', 'Empresas', 'Cargos', 'Estadocivils', 'Sexos', 'Estados', 'Cidades', 'Documentos.Tipodocumentos', 'Funcionariodependentes.Parentescos', 'Funcionarioescolaridades.Cursos', 'Funcionarioescolaridades.Nivelescolaridades', 'Funcionarioescolaridades.Escolas']
        ]);

        $this->set('funcionario', $funcionario);
        $this->set('_serialize', ['funcionario']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($empresa_id = null) {
        $funcionario = $this->Funcionarios->newEntity();
        if ($this->request->is('post')) {
            $funcionario = $this->Funcionarios->patchEntity($funcionario, $this->request->data);

            if ($this->Auth->user('empresa_id') != $this->_ID_TRIADE) {
                $funcionario->empresa_id = $this->Auth->user('empresa_id');
            }

            $funcionario->dt_cadastro = date('Y-m-d H:i:s');
            $funcionario->user_cadastrou = $this->Auth->user('id');

            $funcionario->cnh_dt_habilitacao = $this->convertDateBRtoEN($funcionario->cnh_dt_habilitacao);
            $funcionario->cnh_dt_vencimento = $this->convertDateBRtoEN($funcionario->cnh_dt_vencimento);
            $funcionario->dt_nascimento = $this->convertDateBRtoEN($funcionario->dt_nascimento);
            $funcionario->eleitor_dt_emissao = $this->convertDateBRtoEN($funcionario->eleitor_dt_emissao);
            $funcionario->rg_dt_expedicao = $this->convertDateBRtoEN($funcionario->rg_dt_expedicao);

            $funcionario->salario = $this->formatarmoedaTomysql($funcionario->salario);
            $funcionario->aux_alimentacao = $this->formatarmoedaTomysql($funcionario->aux_alimentacao);
//            $funcionario->aux_transporte = $this->formatarmoedaTomysql($funcionario->aux_transporte);


            $documentos = array();
            if (!empty($funcionario->files)) {
                $documentos = $this->saveDocuments($funcionario);
            }
            $funcionario->documentos = $documentos;


            if ($this->request->data('cargo_id') && !is_numeric($this->request->data('cargo_id'))) {
                $new = $this->Funcionarios->Cargos->newEntity();
                $new->user_id = $this->Auth->user('id');
                $new->descricao = $this->request->data('cargo_id');
                $new->dt_cadastro = date('Y-m-d H:i:s');
                if ($this->Funcionarios->Cargos->save($new)) {
                    $funcionario->cargo_id = $new->id;
                }
            }



            if ($this->Funcionarios->save($funcionario)) {

                if ($this->Auth->user('user_triade')) {
                    $this->disparaEmailAdmissaoTriade($funcionario);
                    $this->Flash->success(__('Funcionário cadastrado com sucesso.'));
                    return $this->redirect(['controller' => 'Empresas', 'action' => 'edit', $funcionario->empresa_id]);
                } else {
                    $this->Flash->success(__('Os dados do funcionário foram registrados com sucesso. A Tríade entrará em contato em breve para prosseguir com o serviço solicitado.'));
                    return $this->redirect(['controller' => 'Servicos', 'action' => 'index']);
                }
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }


        $funcionario->dt_nascimento = $this->convertDateENtoBR($funcionario->dt_nascimento);
        $funcionario->cnh_dt_habilitacao = $this->convertDateENtoBR($funcionario->cnh_dt_habilitacao);
        $funcionario->cnh_dt_vencimento = $this->convertDateENtoBR($funcionario->cnh_dt_vencimento);
        $funcionario->eleitor_dt_emissao = $this->convertDateENtoBR($funcionario->eleitor_dt_emissao);
        $funcionario->rg_dt_expedicao = $this->convertDateENtoBR($funcionario->rg_dt_expedicao);

        $funcionario->salario = $funcionario->salario ? str_replace('.', ',', $funcionario->salario) : '';
        $funcionario->aux_alimentacao = $funcionario->aux_alimentacao ? str_replace('.', ',', $funcionario->aux_alimentacao) : '';
//        $funcionario->aux_transporte = $funcionario->aux_transporte ? str_replace('.', ',', $funcionario->aux_transporte) : '';

        $empresa_id = $empresa_id ? $empresa_id : $this->Auth->user('empresa_id');
        $empresa = $this->Funcionarios->Empresas->find()->where(['Empresas.id' => $empresa_id])->first();
        $cargos = $this->Funcionarios->Cargos->find('list');
        $estados = $this->Funcionarios->Estados->find('list');
        $estadocivils = $this->Funcionarios->Estadocivils->find('list');
        $comunhaoregimes = $this->Funcionarios->Comunhaoregimes->find('list');
        $sexos = $this->Funcionarios->Sexos->find('list');
        $tipodocumentos = $this->Funcionarios->Documentos->Tipodocumentos->find('list')->where(['grupodocumento_id' => $this->_GrupoDocumentoFuncionarios]);


        $this->set(compact('funcionario', 'empresa', 'cargos', 'estadocivils', 'sexos', 'estados', 'comunhaoregimes', 'cidades', 'tipodocumentos'));
        $this->set('_serialize', ['funcionario']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Funcionario id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $funcionario = $this->Funcionarios->get($id, [
            'contain' => ['Empresas']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $funcionario = $this->Funcionarios->patchEntity($funcionario, $this->request->data);

            if (!$this->Auth->user('admin')) {
                if ($funcionario->empresa_id != $this->Auth->user('empresa_id') || ($funcionario->empresa_id == $this->Auth->user('empresa_id') && !$this->Auth->user('admin_empresa') && !$this->Auth->user('admin_setores'))) {
                    $this->Flash->error(__('Você não tem permissão de editar esse registro'));
                    return $this->redirect($this->referer());
                }
            }

            $funcionario->cnh_dt_habilitacao = $this->convertDateBRtoEN($funcionario->cnh_dt_habilitacao);
            $funcionario->cnh_dt_vencimento = $this->convertDateBRtoEN($funcionario->cnh_dt_vencimento);
            $funcionario->dt_nascimento = $this->convertDateBRtoEN($funcionario->dt_nascimento);
            $funcionario->eleitor_dt_emissao = $this->convertDateBRtoEN($funcionario->eleitor_dt_emissao);
            $funcionario->rg_dt_expedicao = $this->convertDateBRtoEN($funcionario->rg_dt_expedicao);


            $funcionario->salario = $this->formatarmoedaTomysql($funcionario->salario);
            $funcionario->aux_alimentacao = $this->formatarmoedaTomysql($funcionario->aux_alimentacao);
//            $funcionario->aux_transporte = $this->formatarmoedaTomysql($funcionario->aux_transporte);


            $documentos = array();
            if (!empty($funcionario->files)) {
                $documentos = $this->saveDocuments($funcionario);
            }
            $funcionario->documentos = $documentos;


            if ($this->request->data('cargo_id') && !is_numeric($this->request->data('cargo_id'))) {
                $new = $this->Funcionarios->Cargos->newEntity();
                $new->user_id = $this->Auth->user('id');
                $new->descricao = $this->request->data('cargo_id');
                $new->dt_cadastro = date('Y-m-d H:i:s');
                if ($this->Funcionarios->Cargos->save($new)) {
                    $funcionario->cargo_id = $new->id;
                }
            }



            if ($this->Funcionarios->save($funcionario)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));
                if ($this->Auth->user('empresa_id') == $this->_ID_TRIADE) {
                    return $this->redirect(['controller' => 'Empresas', 'action' => 'edit', $funcionario->empresa_id]);
                }
                return $this->redirect(['action' => 'view', $funcionario->id]);
            } else {
                $this->Flash->error(__('Houve um erro ao editar, verifique se os campos estão corretos e tente novamente.'));
            }
        }


        $funcionario->dt_nascimento = $this->convertDateENtoBR($funcionario->dt_nascimento);
        $funcionario->cnh_dt_habilitacao = $this->convertDateENtoBR($funcionario->cnh_dt_habilitacao);
        $funcionario->cnh_dt_vencimento = $this->convertDateENtoBR($funcionario->cnh_dt_vencimento);
        $funcionario->eleitor_dt_emissao = $this->convertDateENtoBR($funcionario->eleitor_dt_emissao);
        $funcionario->rg_dt_expedicao = $this->convertDateENtoBR($funcionario->rg_dt_expedicao);

        $funcionario->salario = $funcionario->salario ? str_replace('.', ',', $funcionario->salario) : '';
        $funcionario->aux_alimentacao = $funcionario->aux_alimentacao ? str_replace('.', ',', $funcionario->aux_alimentacao) : '';
//        $funcionario->aux_transporte = $funcionario->aux_transporte ? str_replace('.', ',', $funcionario->aux_transporte) : '';

        $cargos = $this->Funcionarios->Cargos->find('list');
        $estados = $this->Funcionarios->Estados->find('list');
        $estadocivils = $this->Funcionarios->Estadocivils->find('list');
        $comunhaoregimes = $this->Funcionarios->Comunhaoregimes->find('list');
        $sexos = $this->Funcionarios->Sexos->find('list');
        $tipodocumentos = $this->Funcionarios->Documentos->Tipodocumentos->find('list')->where(['grupodocumento_id' => $this->_GrupoDocumentoFuncionarios]);


        $this->set(compact('funcionario', 'cargos', 'estadocivils', 'sexos', 'estados', 'comunhaoregimes', 'cidades', 'tipodocumentos'));
        $this->set('_serialize', ['funcionario']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Funcionario id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $funcionario = $this->Funcionarios->get($id);
        if ($this->Funcionarios->delete($funcionario)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

    /**
     * SaveDocument method
     *
     * @return Documentos[] $documentos
     */
    public function saveDocuments($funcionario) {
        $documentos = array();
        foreach ($funcionario->files as $key => $file) {
            if ($file['size'] > 0 && !$this->isStringEmpty($funcionario->tipo_documentos[$key])) {
                $newdocumento = $this->Funcionarios->Documentos->newEntity();
                $newdocumento->empresa_id = $funcionario->empresa_id;
                $newdocumento->user_enviou = $this->Auth->user('id');
                $newdocumento->filesize = $file['size'];
                $newdocumento->tipodocumento_id = $funcionario->tipo_documentos[$key];
                $newdocumento->descricao = $this->isStringEmpty($funcionario->descricoes[$key]) ? '' : $funcionario->descricoes[$key];
                $newdocumento->file = $this->saveAttachment($file);
                $newdocumento->dt_enviado = date('Y-m-d H:i:s');
                $documentos[] = $newdocumento;
            }
        }
        return $documentos;
    }

    public function disparaEmailAdmissaoTriade($funcionario) {
        try {
            $email = new Email('default');
            $email->viewVars(['funcionario' => $funcionario, 'cliente' => $this->Auth->user()]);
            $email->subject('[SISTEMA] Solicitação de Admissão de Funcionário')
                    ->template('default', 'solicitacaoadmissao')
                    ->emailFormat('html')
                    ->to($this->_EMAIL_ADMIN)
                    ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

}
