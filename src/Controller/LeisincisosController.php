<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Leisincisos Controller
 *
 * @property \App\Model\Table\LeisincisosTable $Leisincisos
 */
class LeisincisosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Leisnormas', 'Leisartigos', 'Leisparagrafos', 'Users']
        ];
        $leisincisos = $this->paginate($this->Leisincisos);

        $this->set(compact('leisincisos'));
        $this->set('_serialize', ['leisincisos']);
    }

    /**
     * View method
     *
     * @param string|null $id Leisinciso id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $leisinciso = $this->Leisincisos->get($id, [
            'contain' => ['Leisnormas', 'Leisartigos', 'Leisparagrafos', 'Users']
        ]);

        $this->set('leisinciso', $leisinciso);
        $this->set('_serialize', ['leisinciso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $leisinciso = $this->Leisincisos->newEntity();
        if ($this->request->is('post')) {
            $leisinciso = $this->Leisincisos->patchEntity($leisinciso, $this->request->data);
            $leisinciso->dt_cadastro =  date('Y-m-d H:i:s');
            $leisinciso->user_id =  $this->Auth->user('id');
            $leisinciso->empresa_id =  $this->Auth->user('empresa_id');
            if ($this->Leisincisos->save($leisinciso)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $leisnormas = $this->Leisincisos->Leisnormas->find('list', ['limit' => 200]);
        $leisartigos = $this->Leisincisos->Leisartigos->find('list', ['limit' => 200]);
        $leisparagrafos = $this->Leisincisos->Leisparagrafos->find('list', ['limit' => 200]);
        $users = $this->Leisincisos->Users->find('list', ['limit' => 200]);
        $this->set(compact('leisinciso', 'leisnormas', 'leisartigos', 'leisparagrafos', 'users'));
        $this->set('_serialize', ['leisinciso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Leisinciso id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $leisinciso = $this->Leisincisos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $leisinciso = $this->Leisincisos->patchEntity($leisinciso, $this->request->data);
            if ($this->Leisincisos->save($leisinciso)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $leisnormas = $this->Leisincisos->Leisnormas->find('list', ['limit' => 200]);
        $leisartigos = $this->Leisincisos->Leisartigos->find('list', ['limit' => 200]);
        $leisparagrafos = $this->Leisincisos->Leisparagrafos->find('list', ['limit' => 200]);
        $users = $this->Leisincisos->Users->find('list', ['limit' => 200]);
        $this->set(compact('leisinciso', 'leisnormas', 'leisartigos', 'leisparagrafos', 'users'));
        $this->set('_serialize', ['leisinciso']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Leisinciso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $leisinciso = $this->Leisincisos->get($id);
        if ($this->Leisincisos->delete($leisinciso)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
