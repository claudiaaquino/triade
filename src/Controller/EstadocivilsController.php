<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Estadocivils Controller
 *
 * @property \App\Model\Table\EstadocivilsTable $Estadocivils
 */
class EstadocivilsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $estadocivils = $this->paginate($this->Estadocivils);

        $this->set(compact('estadocivils'));
        $this->set('_serialize', ['estadocivils']);
    }

    /**
     * View method
     *
     * @param string|null $id Estadocivil id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $estadocivil = $this->Estadocivils->get($id, [
            'contain' => ['Funcionarios', 'Funcionarios.Empresas', 'Funcionarios.Cargos']
        ]);

        $this->set('estadocivil', $estadocivil);
        $this->set('_serialize', ['estadocivil']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        
        $estadocivil = $this->Estadocivils->newEntity();
        if ($this->request->is('post')) {
            $estadocivil = $this->Estadocivils->patchEntity($estadocivil, $this->request->data);
            if ($this->Estadocivils->save($estadocivil)) {
                $this->Flash->success(__('Estado civil cadastrado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao cadastrar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('estadocivil'));
        $this->set('_serialize', ['estadocivil']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Estadocivil id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        
        $estadocivil = $this->Estadocivils->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $estadocivil = $this->Estadocivils->patchEntity($estadocivil, $this->request->data);
            if ($this->Estadocivils->save($estadocivil)) {
                $this->Flash->success(__('Estado civil atualizado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('estadocivil'));
        $this->set('_serialize', ['estadocivil']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Estadocivil id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        
        $this->request->allowMethod(['post', 'delete']);
        $estadocivil = $this->Estadocivils->get($id);
        if ($this->Estadocivils->delete($estadocivil)) {
            $this->Flash->success(__('Estado civil removido com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }
}
