<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Gruposervicos Controller
 *
 * @property \App\Model\Table\GruposervicosTable $Gruposervicos
 */
class GruposervicosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Areaservicos']
        ];
        
         if ($this->request->is('post') && !empty($this->request->data('termo-pesquisa'))) {
            $query = $this->Gruposervicos->find()->where(["Gruposervicos.nome like '%" . $this->request->data('termo-pesquisa') . "%'"]);
        } else {
            $query = $this->Gruposervicos->find();
        }
        $gruposervicos = $this->paginate($query->orderAsc('Gruposervicos.nome'));

        $this->set(compact('gruposervicos'));
        $this->set('_serialize', ['gruposervicos']);
    }

    /**
     * Ajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function ajax($areaservico_id = null) {
        $retorno = $this->Gruposervicos->find('list')->where(['areaservico_id' => $areaservico_id])->order(['descricao' => 'ASC']);

        $tpview = 'list';
        $this->set(compact('retorno', 'tpview'));
        $this->set('_serialize', ['retorno', 'tpview']);
    }

    /**
     * View method
     *
     * @param string|null $id Gruposervico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $gruposervico = $this->Gruposervicos->get($id, [
            'contain' => ['Areaservicos', 'Tiposervicos']
        ]);

        $this->set('gruposervico', $gruposervico);
        $this->set('_serialize', ['gruposervico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $gruposervico = $this->Gruposervicos->newEntity();
        if ($this->request->is('post')) {
            $gruposervico = $this->Gruposervicos->patchEntity($gruposervico, $this->request->data);
            if ($this->Gruposervicos->save($gruposervico)) {
                $this->Flash->success(__('Grupo de serviço salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Por favor, tente novamente.'));
            }
        }
        $areaservicos = $this->Gruposervicos->Areaservicos->find('list')->where(['empresa_id' => $this->_ID_TRIADE]);
        $this->set(compact('gruposervico', 'areaservicos'));
        $this->set('_serialize', ['gruposervico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Gruposervico id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $gruposervico = $this->Gruposervicos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gruposervico = $this->Gruposervicos->patchEntity($gruposervico, $this->request->data);
            if ($this->Gruposervicos->save($gruposervico)) {
                $this->Flash->success(__('Grupo de Serviço atualizado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        $areaservicos = $this->Gruposervicos->Areaservicos->find('list')->where(['empresa_id' => $this->_ID_TRIADE]);
        $this->set(compact('gruposervico', 'areaservicos'));
        $this->set('_serialize', ['gruposervico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Gruposervico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $gruposervico = $this->Gruposervicos->get($id);
        if ($this->Gruposervicos->delete($gruposervico)) {
            $this->Flash->success(__('Grupo de serviço removido com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

}
