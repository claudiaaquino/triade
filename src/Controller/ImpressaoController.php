<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Impressao Controller
 */
class ImpressaoController extends AppController
{

    /**
     * View method
     *
     * @param string|null $id Impressao id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
    }

}
