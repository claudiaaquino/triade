<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Tiporeceitas Controller
 *
 * @property \App\Model\Table\TiporeceitasTable $Tiporeceitas
 */
class TiporeceitasController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Empresas', 'Users']
        ];

        if ($this->Auth->user('admin') || $this->Auth->user('admin_empresa')) {
            $tiporeceitas = $this->paginate($this->Tiporeceitas->find()->where(['Tiporeceitas.empresa_id' => $this->Auth->user('empresa_id')]));
        }

        $this->set(compact('tiporeceitas'));
        $this->set('_serialize', ['tiporeceitas']);
    }

    /**
     * View method
     *
     * @param string|null $id Tiporeceita id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tiporeceita = $this->Tiporeceitas->get($id, [
            'contain' => ['Movimentacaobancarias'=> ['sort' => ['Movimentacaobancarias.dt_cadastro' => 'DESC']], 
                'Movimentacaobancarias.Contabancarias.Bancos','Movimentacaobancarias.Formaspagamentos','Movimentacaobancarias.Clientes' ]
        ]);

        if ($tiporeceita->empresa_id != $this->Auth->user('empresa_id')) {
            $this->Flash->error(__('Você não tem permissão de visualizar um registro que não pertence à sua Empresa'));
            return $this->redirect(['action' => 'index']);
        }
        $this->set('tiporeceita', $tiporeceita);
        $this->set('_serialize', ['tiporeceita']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $tiporeceita = $this->Tiporeceitas->newEntity();
        if ($this->request->is('post')) {
            $tiporeceita = $this->Tiporeceitas->patchEntity($tiporeceita, $this->request->data);
            $tiporeceita->dt_cadastro = date('Y-m-d H:i:s');
            $tiporeceita->user_id = $this->Auth->user('id');
            $tiporeceita->empresa_id = $this->Auth->user('empresa_id');
            if ($this->Tiporeceitas->save($tiporeceita)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tiporeceita'));
        $this->set('_serialize', ['tiporeceita']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tiporeceita id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tiporeceita = $this->Tiporeceitas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tiporeceita = $this->Tiporeceitas->patchEntity($tiporeceita, $this->request->data);

            if ($tiporeceita->empresa_id != $this->Auth->user('empresa_id')) {
                $this->Flash->error(__('Você não tem permissão de editar um registro que não pertence à sua Empresa'));
                return $this->redirect(['action' => 'index']);
            }

            if ($this->Tiporeceitas->save($tiporeceita)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tiporeceita'));
        $this->set('_serialize', ['tiporeceita']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tiporeceita id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $tiporeceita = $this->Tiporeceitas->get($id);
        if ($tiporeceita->empresa_id == $this->Auth->user('empresa_id')) {
            if ($this->Tiporeceitas->delete($tiporeceita)) {
                $this->Flash->success(__('O registro foi removido com sucesso.'));
            } else {
                $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
            }
        } else {
            $this->Flash->error(__('Você não tem permissão de deletar um registro que não pertence à sua Empresa'));
        }
        return $this->redirect($this->request->referer());
    }

}
