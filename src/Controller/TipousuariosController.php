<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tipousuarios Controller
 *
 * @property \App\Model\Table\TipousuariosTable $Tipousuarios
 */
class TipousuariosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $tipousuarios = $this->paginate($this->Tipousuarios);

        $this->set(compact('tipousuarios'));
        $this->set('_serialize', ['tipousuarios']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipousuario id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tipousuario = $this->Tipousuarios->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('tipousuario', $tipousuario);
        $this->set('_serialize', ['tipousuario']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        
        $tipousuario = $this->Tipousuarios->newEntity();
        if ($this->request->is('post')) {
            $tipousuario = $this->Tipousuarios->patchEntity($tipousuario, $this->request->data);
            if ($this->Tipousuarios->save($tipousuario)) {
                $this->Flash->success(__('Tipo de usuário cadastrado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('tipousuario'));
        $this->set('_serialize', ['tipousuario']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipousuario id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tipousuario = $this->Tipousuarios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipousuario = $this->Tipousuarios->patchEntity($tipousuario, $this->request->data);
            if ($this->Tipousuarios->save($tipousuario)) {
                $this->Flash->success(__('Tipo de usuário atualizado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('tipousuario'));
        $this->set('_serialize', ['tipousuario']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipousuario id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        
        $this->request->allowMethod(['post', 'delete']);
        $tipousuario = $this->Tipousuarios->get($id);
        if ($this->Tipousuarios->delete($tipousuario)) {
            $this->Flash->success(__('Tipo de usuário removido com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }
}
