<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Funcionariodependentes Controller
 *
 * @property \App\Model\Table\FuncionariodependentesTable $Funcionariodependentes
 */
class FuncionariodependentesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Funcionarios', 'Parentescos']
        ];
        $funcionariodependentes = $this->paginate($this->Funcionariodependentes);

        $this->set(compact('funcionariodependentes'));
        $this->set('_serialize', ['funcionariodependentes']);
    }

    /**
     * View method
     *
     * @param string|null $id Funcionariodependente id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $funcionariodependente = $this->Funcionariodependentes->get($id, [
            'contain' => ['Funcionarios', 'Parentescos']
        ]);

        $this->set('funcionariodependente', $funcionariodependente);
        $this->set('_serialize', ['funcionariodependente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $funcionariodependente = $this->Funcionariodependentes->newEntity();
        if ($this->request->is('post')) {
            $funcionariodependente = $this->Funcionariodependentes->patchEntity($funcionariodependente, $this->request->data);
            $funcionariodependente->dt_cadastro =  date('Y-m-d H:i:s');
            $funcionariodependente->user_id =  $this->Auth->user('id');
            if ($this->Funcionariodependentes->save($funcionariodependente)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $funcionarios = $this->Funcionariodependentes->Funcionarios->find('list', ['limit' => 200]);
        $parentescos = $this->Funcionariodependentes->Parentescos->find('list', ['limit' => 200]);
        $this->set(compact('funcionariodependente', 'funcionarios', 'parentescos'));
        $this->set('_serialize', ['funcionariodependente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Funcionariodependente id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $funcionariodependente = $this->Funcionariodependentes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $funcionariodependente = $this->Funcionariodependentes->patchEntity($funcionariodependente, $this->request->data);
            if ($this->Funcionariodependentes->save($funcionariodependente)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $funcionarios = $this->Funcionariodependentes->Funcionarios->find('list', ['limit' => 200]);
        $parentescos = $this->Funcionariodependentes->Parentescos->find('list', ['limit' => 200]);
        $this->set(compact('funcionariodependente', 'funcionarios', 'parentescos'));
        $this->set('_serialize', ['funcionariodependente']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Funcionariodependente id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $funcionariodependente = $this->Funcionariodependentes->get($id);
        if ($this->Funcionariodependentes->delete($funcionariodependente)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
