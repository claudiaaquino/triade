<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Empresasocios Controller
 *
 * @property \App\Model\Table\EmpresasociosTable $Empresasocios
 */
class EmpresasociosController extends AppController {

    public function isAuthorized($user) {
//        if (!$this->Auth->user('admin') && $this->Auth->user('empresa_id') != $this->_ID_TRIADE && !$this->request->is('ajax') && !$this->Auth->user('admin_empresa')) {
//            if (!in_array($this->request->param('action'), array('empresario'))) {
//                return false;
//            }
//        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Users', 'Empresas', 'Cargos', 'Estados', 'Cidades', 'Estadocivils', 'Comunhaoregimes', 'Sexos']
        ];
        $empresasocios = $this->paginate($this->Empresasocios);

        $this->set(compact('empresasocios'));
        $this->set('_serialize', ['empresasocios']);
    }

    /**
     * Socios method
     *
     * @return \Cake\Network\Response|null
     */
    public function socios($idempresa = null) {
        $this->viewBuilder()->layout('frame');
        $empresasocios = $this->Empresasocios->find()->where(['Empresasocios.empresa_id' => $idempresa]);

        if ($empresasocios->count() <= 0) {
            return $this->redirect(['action' => 'addframe', $idempresa]);
        }

        $this->set(compact('empresasocios', 'idempresa'));
        $this->set('_serialize', ['empresasocios']);
    }

    /**
     * Empresario method
     *
     * @return \Cake\Network\Response|null
     */
    public function empresario($empresa_id = null) {

        $empresasocios = $this->Empresasocios->find()->where(['Empresasocios.empresa_id' => $empresa_id]);
        if ($empresasocios->count() <= 0) {
//            return $this->redirect(['action' => 'addempresarioframe', $empresa_id]);
            $this->addempresarioframe($empresa_id);
        } else {
//            return $this->redirect(['action' => 'viewempresarioframe', $empresa_id]);       
            $empresario = $empresasocios->first();
            $this->viewempresarioframe($empresario->id);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Empresasocio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $empresasocio = $this->Empresasocios->get($id, [
            'contain' => ['Users', 'Empresas', 'Cargos', 'Estados', 'Cidades', 'Estadocivils', 'Comunhaoregimes', 'Sexos', 'Documentos.Tipodocumentos']
        ]);

        $this->set('empresasocio', $empresasocio);
        $this->set('_serialize', ['empresasocio']);
    }

    /**
     * Viewframe method
     *
     * @param string|null $id Empresasocio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewframe($socio_id = null) {
        $this->view($socio_id);
        $this->viewBuilder()->layout('frame');
        $this->render('view');
    }

    /**
     * Viewempresarioframe method
     *
     * @param string|null $id Empresasocio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewempresarioframe($empresario_id = null) {
        $this->view($empresario_id);
        $this->viewBuilder()->layout('frame');
        $this->render('viewempresario');
    }

    public function viewempresario($empresario_id = null) {
        $this->view($empresario_id);
        $this->render('viewempresario');
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($empresa_id = null, $frame = false) {

        $empresasocio = $this->Empresasocios->newEntity();
        if ($this->request->is('post')) {
            $empresasocio = $this->Empresasocios->patchEntity($empresasocio, $this->request->data);
            $empresasocio->dt_cadastro = date('Y-m-d');

            $empresasocio->cnh_dt_habilitacao = $this->convertDateBRtoEN($empresasocio->cnh_dt_habilitacao);
            $empresasocio->cnh_dt_vencimento = $this->convertDateBRtoEN($empresasocio->cnh_dt_vencimento);
            $empresasocio->dt_nascimento = $this->convertDateBRtoEN($empresasocio->dt_nascimento);
            $empresasocio->eleitor_dt_emissao = $this->convertDateBRtoEN($empresasocio->eleitor_dt_emissao);
            $empresasocio->rg_dt_expedicao = $this->convertDateBRtoEN($empresasocio->rg_dt_expedicao);


            $documentos = array();
            if (!empty($empresasocio->files)) {
                $documentos = $this->saveDocuments($empresasocio);
            }
            $empresasocio->documentos = $documentos;



            if ($this->Empresasocios->save($empresasocio)) {
                $this->Flash->success(__('O sócio foi cadastrado com sucesso.'));
                $num_socios_cadastrados = $this->Empresasocios->find()->where(['Empresasocios.empresa_id' => $empresasocio->empresa_id])->count();
                $num_socios_possíveis = $this->Empresasocios->Empresas->find()->select('num_socios')->where(['Empresas.id' => $empresasocio->empresa_id])->first();

                if ($num_socios_cadastrados < $num_socios_possíveis->num_socios) {
                    if ($empresasocio->frame) {
                        return $this->redirect(['action' => 'addframe', $empresasocio->empresa_id]);
                    }
                    return $this->redirect(['action' => 'add', $empresasocio->empresa_id]);
                }
                if ($empresasocio->frame) {
                    return $this->redirect(['action' => 'socios', $empresasocio->empresa_id]);
                }
                return $this->redirect(['controller' => 'Empresas', 'action' => 'view', $empresasocio->empresa_id]);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar salvar. Verifique se todos os campos estão preenchidos corretamente.'));
            }
        }


        $empresasocio->dt_nascimento = $this->convertDateENtoBR($empresasocio->dt_nascimento);
        $empresasocio->cnh_dt_habilitacao = $this->convertDateENtoBR($empresasocio->cnh_dt_habilitacao);
        $empresasocio->cnh_dt_vencimento = $this->convertDateENtoBR($empresasocio->cnh_dt_vencimento);
        $empresasocio->eleitor_dt_emissao = $this->convertDateENtoBR($empresasocio->eleitor_dt_emissao);
        $empresasocio->rg_dt_expedicao = $this->convertDateENtoBR($empresasocio->rg_dt_expedicao);

        $empresa = $this->Empresasocios->Empresas->find()->where(['Empresas.id' => $empresa_id])->first();
        $num_socios = $this->Empresasocios->find()->where(['empresa_id' => $empresa_id])->count();
        $cargos = $this->Empresasocios->Cargos->find('list', ['limit' => 200]);
        $estados = $this->Empresasocios->Estados->find('list', ['limit' => 200]);
        $estadocivils = $this->Empresasocios->Estadocivils->find('list', ['limit' => 200]);
        $comunhaoregimes = $this->Empresasocios->Comunhaoregimes->find('list', ['limit' => 200]);
        $sexos = $this->Empresasocios->Sexos->find('list', ['limit' => 200]);
        $tipodocumentos = $this->Empresasocios->Documentos->Tipodocumentos->find('list')->where(['grupodocumento_id' => $this->_GrupoDocumentoSocios]);
        $empresasocio->frame = $frame ? true : ($empresasocio->frame ? true : false);

        $this->set(compact('empresasocio', 'empresa', 'cargos', 'estados', 'cidades', 'estadocivils', 'comunhaoregimes', 'sexos', 'num_socios', 'tipodocumentos'));
        $this->set('_serialize', ['empresasocio']);
        if (($empresa->solicitacao && !$this->Auth->user('admin')) || $empresasocio->frame) {
            $this->viewBuilder()->layout('frame');
        }
    }

    /**
     * Addframe method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addframe($empresa_id = null) {
        $this->add($empresa_id, true);
        $this->viewBuilder()->layout('frame');
        $this->render('add');
    }

    /**
     * Addempresario method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addempresario($empresa_id = null, $frame = false) {

        $empresasocio = $this->Empresasocios->newEntity();
        if ($this->request->is('post')) {
            $empresasocio = $this->Empresasocios->patchEntity($empresasocio, $this->request->data);
            $empresasocio->dt_cadastro = date('Y-m-d');

            $empresasocio->cnh_dt_habilitacao = $this->convertDateBRtoEN($empresasocio->cnh_dt_habilitacao);
            $empresasocio->cnh_dt_vencimento = $this->convertDateBRtoEN($empresasocio->cnh_dt_vencimento);
            $empresasocio->dt_nascimento = $this->convertDateBRtoEN($empresasocio->dt_nascimento);
            $empresasocio->eleitor_dt_emissao = $this->convertDateBRtoEN($empresasocio->eleitor_dt_emissao);
            $empresasocio->rg_dt_expedicao = $this->convertDateBRtoEN($empresasocio->rg_dt_expedicao);
            $empresasocio->responsavelreceita = 1;

            $documentos = array();
            if (!empty($empresasocio->files)) {
                $documentos = $this->saveDocuments($empresasocio);
            }
            $empresasocio->documentos = $documentos;


            if ($this->Empresasocios->save($empresasocio)) {
                $this->Flash->success(__('O empresário foi cadastrado com sucesso.'));

                if ($empresasocio->frame) {
                    return $this->redirect(['action' => 'viewempresarioframe', $empresasocio->id]);
                }
                return $this->redirect(['action' => 'viewempresario', $empresasocio->id]);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar salvar. Verifique se todos os campos estão preenchidos corretamente.'));
            }
        }


        $empresasocio->dt_nascimento = $this->convertDateENtoBR($empresasocio->dt_nascimento);
        $empresasocio->cnh_dt_habilitacao = $this->convertDateENtoBR($empresasocio->cnh_dt_habilitacao);
        $empresasocio->cnh_dt_vencimento = $this->convertDateENtoBR($empresasocio->cnh_dt_vencimento);
        $empresasocio->eleitor_dt_emissao = $this->convertDateENtoBR($empresasocio->eleitor_dt_emissao);
        $empresasocio->rg_dt_expedicao = $this->convertDateENtoBR($empresasocio->rg_dt_expedicao);

        $empresa = $this->Empresasocios->Empresas->find()->where(['Empresas.id' => $empresa_id])->first();
        $cargos = $this->Empresasocios->Cargos->find('list');
        $estados = $this->Empresasocios->Estados->find('list');
        $estadocivils = $this->Empresasocios->Estadocivils->find('list');
        $comunhaoregimes = $this->Empresasocios->Comunhaoregimes->find('list');
        $sexos = $this->Empresasocios->Sexos->find('list');
        $tipodocumentos = $this->Empresasocios->Documentos->Tipodocumentos->find('list')->where(['grupodocumento_id' => $this->_GrupoDocumentoSocios]);

        $empresasocio->frame = $frame ? true : ($empresasocio->frame ? true : false);

        $this->set(compact('empresasocio', 'empresa', 'cargos', 'estados', 'cidades', 'estadocivils', 'comunhaoregimes', 'sexos', 'tipodocumentos'));
        $this->set('_serialize', ['empresasocio']);
    }

    /**
     * Addempresarioframe method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addempresarioframe($empresa_id = null) {
        $this->addempresario($empresa_id, true);
        $this->viewBuilder()->layout('frame');
        $this->render('addempresario');
    }

    /**
     * Editframe method
     *
     * @param string|null $id Empresasocio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editframe($socio_id = null) {
        $this->edit($socio_id, true);
        $this->viewBuilder()->layout('frame');
        $this->render('edit');
    }

    public function editempresarioframe($empresario_id = null) {
        $this->edit($empresario_id, true, true);
        $this->viewBuilder()->layout('frame');
        $this->render('edit');
    }
    public function editempresario($empresario_id = null) {
        $this->edit($empresario_id, true, true);
        $this->render('edit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Empresasocio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null, $frame = false, $empresario = false) {

        $empresasocio = $this->Empresasocios->get($id, [
            'contain' => []
        ]);
        $empresasocio->frame = $frame ? true : ($empresasocio->frame ? true : false);
        $empresa = $this->Empresasocios->Empresas->find()->where(['Empresas.id' => $empresasocio->empresa_id])->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $empresasocio = $this->Empresasocios->patchEntity($empresasocio, $this->request->data);

            $empresasocio->cnh_dt_habilitacao = $this->convertDateBRtoEN($empresasocio->cnh_dt_habilitacao);
            $empresasocio->cnh_dt_vencimento = $this->convertDateBRtoEN($empresasocio->cnh_dt_vencimento);
            $empresasocio->dt_nascimento = $this->convertDateBRtoEN($empresasocio->dt_nascimento);
            $empresasocio->eleitor_dt_emissao = $this->convertDateBRtoEN($empresasocio->eleitor_dt_emissao);
            $empresasocio->rg_dt_expedicao = $this->convertDateBRtoEN($empresasocio->rg_dt_expedicao);


            $documentos = array();
            if (!empty($empresasocio->files)) {
                $documentos = $this->saveDocuments($empresasocio);
            }
            $empresasocio->documentos = $documentos;



            if ($this->Empresasocios->save($empresasocio)) {
                
                if (!$empresa->sociedade && !$empresasocio->frame) {
                    $this->Flash->success(__('Informações do Empresário foram atualizadas'));
                    return $this->redirect(['action' => 'viewempresario', $empresasocio->id]);
                } else if (!$empresa->sociedade && $empresasocio->frame) {
                    $this->Flash->success(__('Informações do Empresário foram atualizadas'));
                    return $this->redirect(['action' => 'viewempresarioframe', $empresasocio->id]);
                } else if ($empresa->sociedade && $empresasocio->frame) {
                    $this->Flash->success(__('Informações do Sócio foram atualizadas'));
                    return $this->redirect(['action' => 'socios', $empresasocio->empresa_id]);
                }

                if ($this->Auth->user('user_triade') || $this->Auth->user('admin')) {
                    $this->Flash->success(__('Informações do Sócio foram atualizadas'));
                    return $this->redirect(['controller' => 'Empresas', 'action' => 'edit', $empresasocio->empresa_id]);
                }
            } else {
                $this->Flash->error(__('Houve um erro ao tentar salvar informações do sócio. Verifique se todos os campos estão preenchidos corretamente.'));
            }
        }
//        $empresa = $this->Empresasocios->Empresas->find()->where(['Empresas.id' => $empresasocio->empresa_id])->first();

        if (($empresa->solicitacao && !$this->Auth->user('admin')) || $empresasocio->frame || !$empresa->solicitacao_finalizada) {
            $this->viewBuilder()->layout('frame');
        }

        $empresasocio->dt_nascimento = $this->convertDateENtoBR($empresasocio->dt_nascimento);
        $empresasocio->cnh_dt_habilitacao = $this->convertDateENtoBR($empresasocio->cnh_dt_habilitacao);
        $empresasocio->cnh_dt_vencimento = $this->convertDateENtoBR($empresasocio->cnh_dt_vencimento);
        $empresasocio->eleitor_dt_emissao = $this->convertDateENtoBR($empresasocio->eleitor_dt_emissao);
        $empresasocio->rg_dt_expedicao = $this->convertDateENtoBR($empresasocio->rg_dt_expedicao);

        $cargos = $this->Empresasocios->Cargos->find('list', ['limit' => 200]);
        $estados = $this->Empresasocios->Estados->find('list', ['limit' => 200]);
        $cidades = $this->Empresasocios->Cidades->find('list', ['limit' => 200]);
        $estadocivils = $this->Empresasocios->Estadocivils->find('list', ['limit' => 200]);
        $comunhaoregimes = $this->Empresasocios->Comunhaoregimes->find('list', ['limit' => 200]);
        $sexos = $this->Empresasocios->Sexos->find('list', ['limit' => 200]);
        $tipodocumentos = $this->Empresasocios->Documentos->Tipodocumentos->find('list')->where(['grupodocumento_id' => $this->_GrupoDocumentoSocios]);
        $this->set(compact('empresasocio', 'users', 'empresa', 'cargos', 'estados', 'cidades', 'estadocivils', 'comunhaoregimes', 'sexos', 'tipodocumentos'));
        $this->set('_serialize', ['empresasocio']);
    }

    /**
     * SaveDocument method
     *
     * @return Documentos[] $documentos
     */
    public function saveDocuments($socio) {
        $documentos = array();
        foreach ($socio->files as $key => $file) {
            if ($file['size'] > 0 && !$this->isStringEmpty($socio->tipo_documentos[$key])) {
                $newdocumento = $this->Empresasocios->Documentos->newEntity();
                $newdocumento->empresa_id = $socio->empresa_id;
                $newdocumento->user_enviou = $this->Auth->user('id');
                $newdocumento->filesize = $file['size'];
                $newdocumento->tipodocumento_id = $socio->tipo_documentos[$key];
                $newdocumento->descricao = $this->isStringEmpty($socio->descricoes[$key]) ? '' : $socio->descricoes[$key];
                $newdocumento->file = $this->saveAttachment($file);
                $newdocumento->dt_enviado = date('Y-m-d H:i:s');
                $documentos[] = $newdocumento;
            }
        }
        return $documentos;
    }

    /**
     * Delete method
     *
     * @param string|null $id Empresasocio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $empresasocio = $this->Empresasocios->get($id);
        if ($this->Empresasocios->delete($empresasocio)) {
            $this->Flash->success(__('O sócio foi removido do cadastro da empresa.'));
        } else {
            $this->Flash->error(__('Houve um erro no servidor, tente novamente depois.'));
        }

        return $this->redirect($this->request->referer());
    }

    /**
     * Delete method
     *
     * @param string|null $id Empresasocio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deleteajax($id = null) {
//        $this->request->allowMethod(['post', 'delete']);
        $empresasocio = $this->Empresasocios->get($id);
        if ($this->Empresasocios->delete($empresasocio)) {
//            return true;
            $retorno = true;
        } else {
//            return false;
            $retorno = false;
        }

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

}
