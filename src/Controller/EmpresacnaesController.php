<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Empresacnaes Controller
 *
 * @property \App\Model\Table\EmpresacnaesTable $Empresacnaes
 */
class EmpresacnaesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Empresas']
        ];
        $empresacnaes = $this->paginate($this->Empresacnaes);

        $this->set(compact('empresacnaes'));
        $this->set('_serialize', ['empresacnaes']);
    }

    /**
     * Loadajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function loadajax() {
//        $retorno = $this->Empresacnaes->find()->where(['Empresacnaes.empresa_id' => $empresa_id])->order(['Cnaes.classe' => 'ASC']);
        $retorno = $this->Empresacnaes->find()->contain(['Cnaeclasses'])
                ->where(['Empresacnaes.empresa_id' => $this->request->query('empresa_id')]);
        $tpview = 'table';
        $this->set(compact('retorno', 'tpview'));
        $this->set('_serialize', ['retorno', 'tpview']);
        $this->render('ajax');
    }

    /**
     * View method
     *
     * @param string|null $id Empresacnae id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $empresacnae = $this->Empresacnaes->get($id, [
            'contain' => ['Empresas']
        ]);

        $this->set('empresacnae', $empresacnae);
        $this->set('_serialize', ['empresacnae']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $empresacnae = $this->Empresacnaes->newEntity();
        if ($this->request->is('post')) {
            $empresacnae = $this->Empresacnaes->patchEntity($empresacnae, $this->request->data);
            if ($this->Empresacnaes->save($empresacnae)) {
                $this->Flash->success(__('The empresacnae has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The empresacnae could not be saved. Please, try again.'));
            }
        }
        $empresas = $this->Empresacnaes->Empresas->find('list', ['limit' => 200]);
        $cnaes = $this->Empresacnaes->Cnaes->find('list', ['limit' => 200]);
        $this->set(compact('empresacnae', 'empresas', 'cnaes'));
        $this->set('_serialize', ['empresacnae']);
    }

    /**
     * Addajax method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addajax() {
        $empresacnae = $this->Empresacnaes->newEntity();
        $empresacnae->empresa_id = $this->request->query('empresa_id');
        $empresacnae->cnaeclasse_id = $this->request->query('cnaeclasse_id');
        $empresacnae->exercida_local = $this->request->query('exercida_local') == 'true' ? 1 : 0;
        $empresacnae->principal = $this->request->query('principal') == 'true' ? 1 : 0;
        if ($this->Empresacnaes->save($empresacnae)) {
//                    $this->Flash->success(__('The empresacnae has been saved.'));
            $retorno = true;
        } else {
//                    $this->Flash->error(__('The empresacnae could not be saved. Please, try again.'));
            $retorno = false;
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Empresacnae id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $empresacnae = $this->Empresacnaes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $empresacnae = $this->Empresacnaes->patchEntity($empresacnae, $this->request->data);
            if ($this->Empresacnaes->save($empresacnae)) {
                $this->Flash->success(__('The empresacnae has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The empresacnae could not be saved. Please, try again.'));
            }
        }
        $empresas = $this->Empresacnaes->Empresas->find('list', ['limit' => 200]);
        $cnaes = $this->Empresacnaes->Cnaeclasses->find('list', ['limit' => 200]);
        $this->set(compact('empresacnae', 'empresas', 'cnaes'));
        $this->set('_serialize', ['empresacnae']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Empresacnae id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $empresacnae = $this->Empresacnaes->get($id);
        if ($this->Empresacnaes->delete($empresacnae)) {
            $this->Flash->success(__('The empresacnae foi deletado.'));
        } else {
            $this->Flash->error(__('The empresacnae could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->request->referer());
    }

    /**
     * Deleteajax method
     *
     * @param string|null $id Empresacnae id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deleteajax() {
//                $this->request->allowMethod(['post', 'delete']);
        $empresacnae = $this->Empresacnaes->get($this->request->query('id'));
        if ($this->Empresacnaes->delete($empresacnae)) {
//                    $this->Flash->success(__('The empresacnae foi deletado.'));
            $retorno = true;
        } else {
            $retorno = false;
//                    $this->Flash->error(__('The empresacnae could not be deleted. Please, try again.'));
        }

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

}
