<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Leisletras Controller
 *
 * @property \App\Model\Table\LeisletrasTable $Leisletras
 */
class LeisletrasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Leisnormas', 'Leisartigos', 'Leisparagrafos', 'Users']
        ];
        $leisletras = $this->paginate($this->Leisletras);

        $this->set(compact('leisletras'));
        $this->set('_serialize', ['leisletras']);
    }

    /**
     * View method
     *
     * @param string|null $id Leisletra id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $leisletra = $this->Leisletras->get($id, [
            'contain' => ['Leisnormas', 'Leisartigos', 'Leisparagrafos', 'Users']
        ]);

        $this->set('leisletra', $leisletra);
        $this->set('_serialize', ['leisletra']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $leisletra = $this->Leisletras->newEntity();
        if ($this->request->is('post')) {
            $leisletra = $this->Leisletras->patchEntity($leisletra, $this->request->data);
            $leisletra->dt_cadastro =  date('Y-m-d H:i:s');
            $leisletra->user_id =  $this->Auth->user('id');
            $leisletra->empresa_id =  $this->Auth->user('empresa_id');
            if ($this->Leisletras->save($leisletra)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $leisnormas = $this->Leisletras->Leisnormas->find('list', ['limit' => 200]);
        $leisartigos = $this->Leisletras->Leisartigos->find('list', ['limit' => 200]);
        $leisparagrafos = $this->Leisletras->Leisparagrafos->find('list', ['limit' => 200]);
        $users = $this->Leisletras->Users->find('list', ['limit' => 200]);
        $this->set(compact('leisletra', 'leisnormas', 'leisartigos', 'leisparagrafos', 'users'));
        $this->set('_serialize', ['leisletra']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Leisletra id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $leisletra = $this->Leisletras->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $leisletra = $this->Leisletras->patchEntity($leisletra, $this->request->data);
            if ($this->Leisletras->save($leisletra)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $leisnormas = $this->Leisletras->Leisnormas->find('list', ['limit' => 200]);
        $leisartigos = $this->Leisletras->Leisartigos->find('list', ['limit' => 200]);
        $leisparagrafos = $this->Leisletras->Leisparagrafos->find('list', ['limit' => 200]);
        $users = $this->Leisletras->Users->find('list', ['limit' => 200]);
        $this->set(compact('leisletra', 'leisnormas', 'leisartigos', 'leisparagrafos', 'users'));
        $this->set('_serialize', ['leisletra']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Leisletra id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $leisletra = $this->Leisletras->get($id);
        if ($this->Leisletras->delete($leisletra)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
