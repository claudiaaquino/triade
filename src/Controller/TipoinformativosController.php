<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tipoinformativos Controller
 *
 * @property \App\Model\Table\TipoinformativosTable $Tipoinformativos
 */
class TipoinformativosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $tipoinformativos = $this->paginate($this->Tipoinformativos);

        $this->set(compact('tipoinformativos'));
        $this->set('_serialize', ['tipoinformativos']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipoinformativo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoinformativo = $this->Tipoinformativos->get($id, [
            'contain' => ['Assuntosinformativos']
        ]);

        $this->set('tipoinformativo', $tipoinformativo);
        $this->set('_serialize', ['tipoinformativo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoinformativo = $this->Tipoinformativos->newEntity();
        if ($this->request->is('post')) {
            $tipoinformativo = $this->Tipoinformativos->patchEntity($tipoinformativo, $this->request->data);
            $tipoinformativo->dt_cadastro =  date('Y-m-d H:i:s');
            $tipoinformativo->user_id =  $this->Auth->user('id');
            $tipoinformativo->empresa_id =  $this->Auth->user('empresa_id');
            if ($this->Tipoinformativos->save($tipoinformativo)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tipoinformativo'));
        $this->set('_serialize', ['tipoinformativo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipoinformativo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoinformativo = $this->Tipoinformativos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoinformativo = $this->Tipoinformativos->patchEntity($tipoinformativo, $this->request->data);
            if ($this->Tipoinformativos->save($tipoinformativo)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tipoinformativo'));
        $this->set('_serialize', ['tipoinformativo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipoinformativo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tipoinformativo = $this->Tipoinformativos->get($id);
        if ($this->Tipoinformativos->delete($tipoinformativo)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
