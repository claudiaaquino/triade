<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Documentos Controller
 *
 * @property \App\Model\Table\DocumentosTable $Documentos
 */
class DocumentosController extends AppController {

    public function beforeFilter(Event $event) {
        if ($this->request->params['action'] == 'addajax') {
            $this->request->session()->id($this->request['pass'][0]);
            $this->request->session()->start();
        }
    }

    public function isAuthorized($user) {
        if (!$this->Auth->user('admin') && !$this->Auth->user('user_triade') && !$this->request->is('ajax') && !$this->Auth->user('admin_empresa')) {
            if (!in_array($this->request->param('action'), ['index', 'view', 'add', 'solicitarreemissao'])) {
                return false;
            }
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($user_id = false) {


        $this->paginate = [
            'contain' => ['Tipodocumentos.Grupodocumentos.Areaservicos' => array('conditions' => ['Areaservicos.id != ' => $this->_ID_Setor_Gerencial]),
                'Empresas', 'Funcionarios']
        ];

        $query = $this->Documentos->find()->distinct();

        //quando deseja abrir automaticamnte todos os documentos de uma emprsa/usuario específico, então não precisa de mais nenhum filtro abaixo.
        // funcionalidade necessaria por causa do email, que abre diretamente p esse link: documentos/index/iduser
        if ($user_id && $this->Auth->user('user_triade')) {
            $user = $this->Documentos->Users->get($user_id, ['contain' => ['Empresas']]);

            //se esse usuario tiver emprsa, mostra todos da empresa, senao, mostra os documentos próprios dele
            if ($user->empresa_id) {
                $query->where(['Documentos.empresa_id' => $user->empresa_id]);
                $nomeremetente = $user->empresa->razao;
            } else {
                $query->where(['Documentos.user_enviou' => $user->id]);
                $nomeremetente = $user->nome;
            }
        } else {
            $nomeremetente = false;

            if ($this->Auth->user('admin') || ($this->Auth->user('admin_empresa') && $this->Auth->user('empresa_id') == $this->_ID_TRIADE)) {
                //admin || (admin_empresa da triade) pode ver todos os documentos de todas as empresas
                //aplicar aqui os filtros habilitados para admins
                if (is_numeric($this->request->data('empresa_id'))) {
                    $query->where(['Documentos.empresa_id' => $this->request->data('empresa_id')]);
                }
            } else if ($this->Auth->user('empresa_id') == $this->_ID_TRIADE && $this->Auth->user('admin_setores')) {
                //se qualquer outro usuário da triade && admin_setor -> vê todos os documentos de todas as empresas relacionados ao setor dele
                $query = $query->where(function ($exp, $q) {
                    return $exp->in('areaservico_id', $this->Auth->user('admin_setores'));
                });
            } else if ($this->Auth->user('admin_empresa') && $this->Auth->user('empresa_id') != $this->_ID_TRIADE) {
                //se usuario qualquer && admin_empresa -> vê todos os documentos referentes à essa empresa 
                $query = $query->where(['Documentos.empresa_id' => $this->Auth->user('empresa_id')]);
            } else if ($this->Auth->user('admin_setores') && $this->Auth->user('empresa_id') != $this->_ID_TRIADE) {
                //se usuario qualquer && admin_setor -> vê todos os documentos referentes àpenas a um setor essa empresa  (e se o usuário tiver mais setores?)
                $query = $query->where(['Documentos.empresa_id' => $this->Auth->user('empresa_id')])
                        ->andWhere(function ($exp, $q) {
                    return $exp->in('Areaservicos.id', $this->Auth->user('admin_setores'));
                });
            }

            //todos os usuários, inclusive o usuario que não tem empresa, vê todos que user_enviou = sessão, e user_destinatario = sessão
            if (!$this->Auth->user('admin') && (!$this->Auth->user('admin_empresa') && !$this->Auth->user('empresa_id') == $this->_ID_TRIADE)) {
                $query = $query->orWhere(['user_destinatario' => $this->Auth->user('id')])->orWhere(['user_enviou' => $this->Auth->user('id')]);
            }

            if (is_numeric($this->request->data('areaservico_id'))) {
                $query->andWhere(['Areaservicos.id' => $this->request->data('areaservico_id')]);
            }
            if (is_numeric($this->request->data('grupodocumento_id'))) {
                $query->andWhere(['Grupodocumentos.id' => $this->request->data('grupodocumento_id')]);
            }
            if (is_numeric($this->request->data('tipodocumento_id'))) {
                $query->andWhere(['Tipodocumentos.id' => $this->request->data('tipodocumento_id')]);
            }
            if (is_numeric($this->request->data('funcionario_id'))) {
                $query->andWhere(['Documentos.funcionario_id' => $this->request->data('funcionario_id')]);
            }

            if ($this->request->data('dt_inicio')) {
                $query->andWhere(['Documentos.dt_enviado >= ' => $this->convertDateBRtoEN($this->request->data('dt_inicio'))]);
            }
            if ($this->request->data('dt_final')) {
                $query->andWhere(['Documentos.dt_enviado <= ' => $this->convertDateBRtoEN($this->request->data('dt_final'))]);
            }
        }

        $documentos = $this->paginate($query->orderDesc('dt_enviado'));

        /* -------------***DADOS USADOS NO FILTRO PARA FILTRAR OS DADOS DA TABELA******* */
        $this->loadModel('Areaservicos');
        $areaservicos = $this->Areaservicos->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')])->orWhere(['todos = 1 '])->orderDesc('descricao');
        $funcionarios = $this->Documentos->Empresas->Funcionarios->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')]);

        if ($this->Auth->user('admin')) {
            $empresas = $this->Documentos->Empresas->find('list');
        }

        $this->set(compact('documentos', 'areaservicos', 'empresas', 'funcionarios', 'nomeremetente'));
        $this->set('_serialize', ['documentos']);
    }

    /**
     * Docsetor method
     *
     * @return \Cake\Network\Response|null
     */
    public function docsetor($setor_id = false, $filtro_extra = null) {
        $setor_id = $setor_id ? $setor_id : (!empty($this->request->data('setor_id')) ? $this->request->data('setor_id') : $this->request->query('setor_id'));
        $filtro_extra = $filtro_extra ? $filtro_extra : (!empty($this->request->query('filtro_extra')) ? $this->request->query('filtro_extra') : ($this->request->data('filtro_extra') ? $this->request->data('filtro_extra') : null));
        $this->paginate = [
            'contain' => ['Tipodocumentos.Grupodocumentos.Areaservicos', 'Empresas', 'Funcionarios']
        ];

        $query = $this->Documentos->find()->distinct();
        $query->where(['Areaservicos.id' => $setor_id]);

        if ($filtro_extra == 'funcionarios') {
            $query->where(['Documentos.funcionario_id is not null']);
            $funcionarios = $this->Documentos->Empresas->Funcionarios->find('list');
            if (!$this->Auth->user('user_triade')) {
                $funcionarios->where(['empresa_id' => $this->Auth->user('empresa_id')]);
            }
        }

        if ($this->Auth->user('admin') || $this->Auth->user('user_triade')) {
            if (is_numeric($this->request->data('empresa_id'))) {
                $query->where(['Documentos.empresa_id' => $this->request->data('empresa_id')]);
            }
        } else {
            $query->where(['Documentos.empresa_id' => $this->Auth->user('empresa_id')]);
        }

        if (is_numeric($this->request->data('tipodocumento_id'))) {
            $query->andWhere(['Tipodocumentos.id' => $this->request->data('tipodocumento_id')]);
        }
        if ($this->request->data('competencia')) {
            $query->andWhere(['Documentos.competencia = ' => $this->request->data('competencia')]);
        }
        if ($this->request->data('exercicio')) {
            $query->andWhere(['Documentos.exercicio = ' => $this->request->data('exercicio')]);
        }
        if (is_numeric($this->request->data('funcionario_id'))) {
            $query->andWhere(['Documentos.funcionario_id' => $this->request->data('funcionario_id')]);
        }

        $documentos = $this->paginate($query->orderDesc('dt_enviado'));


        /* -------------***DADOS USADOS NO FILTRO PARA FILTRAR OS DADOS DA TABELA******* */
        $tipodocumentos = $this->Documentos->Tipodocumentos->find("list")->innerJoinWith('Grupodocumentos')->where(['Grupodocumentos.areaservico_id' => $setor_id]);
        if ($filtro_extra == 'funcionarios') {
            $tipodocumentos->where(['Tipodocumentos.funcionario = 1']);
        }
        $setor = $this->Documentos->Tipodocumentos->Grupodocumentos->Areaservicos->get($setor_id);
        if ($this->Auth->user('admin')) {
            $empresas = $this->Documentos->Empresas->find('list');
        }
        $anos = [];
        $meses = array(1 => 'Janeiro', 2 => 'Fevereiro', 3 => 'Março', 4 => 'Abril', 5 => 'Maio', 6 => 'Junho', 7 => 'Julho', 8 => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro', 12 => 'Dezembro');
        for ($ano = date('Y'); $ano >= date('Y') - 5; $ano--) {
            $anos[$ano] = $ano;
        }

        $this->set(compact('documentos', 'empresas', 'tipodocumentos', 'meses', 'anos', 'setor', 'funcionarios'));
        $this->set('_serialize', ['documentos']);
    }

    /**
     * Loadajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function loadajax() {
        $where = array();
        if ($this->request->query('empresa_id')) {
            $where['Documentos.empresa_id'] = $this->request->query('empresa_id');
        }
        if ($this->request->query('tipodocumento_id')) {
            $where['Documentos.tipodocumento_id'] = $this->request->query('tipodocumento_id');
        }
        if ($this->request->query('grupodocumento_id')) {
            $where['Tipodocumentos.grupodocumento_id'] = $this->request->query('grupodocumento_id');
        }
        if ($this->request->query('areaservico_id')) {
            $where['Grupodocumentos.areaservico_id'] = $this->request->query('areaservico_id');
        }

        $documentos = $this->Documentos->find('all')->contain(['Tipodocumentos.Grupodocumentos.Areaservicos', 'Empresas', 'Funcionarios'])->where($where);

        $tpview = 'table';
        $this->set(compact('documentos', 'tpview'));
        $this->set('_serialize', ['documentos', 'tpview']);
        $this->render('ajax');
    }

    /**
     * View method
     *
     * @param string|null $id Documento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $documento = $this->Documentos->get($id, [
            'contain' => ['Tipodocumentos.Grupodocumentos', 'Empresas', 'Funcionarios']
        ]);

        if (($documento->empresa_id != $this->Auth->user('empresa_id') && !$this->Auth->user('admin')) || ($documento->empresa_id == $this->Auth->user('empresa_id') && (!$this->Auth->user('admin_empresa') && !$documento->user_enviou))) {
            $this->Flash->error(__('Você não tem permissão de visualizar esse registro'));
            return $this->redirect($this->referer());
        }

        $user_enviou = $this->Documentos->Users->find()->select(['id', 'nome'])->where(['id' => $documento->user_enviou])->first();

        $this->set(compact('documento', 'user_enviou'));
        $this->set('_serialize', ['documento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {

        $documento = $this->Documentos->newEntity();
        if ($this->request->is('post')) {
            $documento = $this->Documentos->patchEntity($documento, $this->request->data);

            $documentos = array();
            if (!empty($documento->files)) {
                $documentos = $this->saveDocuments($documento);
            }
            if ($this->Documentos->saveMany($documentos)) {

                if ($this->Auth->user('user_triade') && $documento->notificar_empresa) {
                    //notifica o cliente por email q a triade cadastrou um novo documento p ele.
                    $this->emailNovoDocCliente($documentos);
                } else {
                    //envia email p usuarios da triade habilitados p receber esse tipo de email                                        
                    $this->emailNovoDocTriade($documentos);
                }

                $this->Flash->success(__('Enviado com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O registro de documento não pôde ser salvo. Por favor, tente novamente.'));
            }
        }

        if ($this->Auth->user('admin')) {
            $empresas = $this->Documentos->Empresas->find('list');
        } else {
            $empresas = $this->Documentos->Empresas->find('list')->where(['id' => $this->_ID_TRIADE])->orWhere(['id' => $this->Auth->user('empresa_id')]);
        }
        $funcionarios = $this->Documentos->Empresas->Funcionarios->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')]);
        $areaservicos = $this->Documentos->Empresas->Areaservicos->find('list', ['order' => 'Areaservicos.descricao DESC'])->where(['todos = 1 ']);

        $anos = [];
        $meses = array(1 => 'Janeiro', 2 => 'Fevereiro', 3 => 'Março', 4 => 'Abril', 5 => 'Maio', 6 => 'Junho', 7 => 'Julho', 8 => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro', 12 => 'Dezembro');
        for ($ano = date('Y'); $ano >= date('Y') - 5; $ano--) {
            $anos[$ano] = $ano;
        }

        $this->set(compact('documento', 'areaservicos', 'empresas', 'funcionarios', 'anos', 'meses'));
        $this->set('_serialize', ['documento']);
    }

    /**
     * SaveDocument method
     *
     * @return Documentos[] $documentos
     */
    public function saveDocuments($documento) {
        $documentos = array();
        foreach ($documento->files as $key => $file) {
            if ($file['size'] > 0) {
                $newdocumento = $this->Documentos->newEntity();
                if ($this->isStringEmpty($documento->empresa_ids[$key])) {
                    $newdocumento->empresa_id = $this->Auth->user('empresa_id');
                } else {
                    $newdocumento->empresa_id = $documento->empresa_ids[$key];
                }
                $newdocumento->user_enviou = $this->Auth->user('id');
                $newdocumento->filesize = $file['size'];
                $newdocumento->tipodocumento_id = $documento->tipo_documentos[$key];
                if (!$this->isStringEmpty($documento->funcionario_ids[$key])) {
                    $newdocumento->funcionario_id = $documento->funcionario_ids[$key];
                }
                $newdocumento->descricao = $documento->descricoes[$key];
                $newdocumento->file = $this->saveAttachment($file);
                $newdocumento->dt_enviado = date('Y-m-d H:i:s');
                if (!$this->isStringEmpty($documento->competencias[$key]))
                    $newdocumento->competencia = $documento->competencias[$key];
                if (!$this->isStringEmpty($documento->exercicios[$key]))
                    $newdocumento->exercicio = $documento->exercicios[$key];

                $documentos[] = $newdocumento;
            }
        }
        return $documentos;
    }

    /**
     * Addajax method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addajax() {
        if ($this->request->is('post') || $this->request->is('put') || $this->request->is('get')) {
            $documento = $this->Documentos->patchEntity($this->Documentos->newEntity(), $this->request->data);
            $documento->user_enviou = $this->Auth->user('id');
            $documento->empresa_id = $this->request->data('empresa_id');


            if (!empty($this->request->data['Filename'])) {
                $file = $this->request->data['Filedata']; //put the data into a var for easy use

                $setNewFileName = time() . "_" . rand(000000, 999999);
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                move_uploaded_file($file['tmp_name'], WWW_ROOT . '/docs/' . $setNewFileName . '.' . $ext);


                $documento->file = $setNewFileName . '.' . $ext;
                $documento->dt_enviado = date('Y-m-d H:i:s');
                if ($this->Documentos->save($documento)) {
                    $retorno = $documento->file;
                } else {
                    $retorno = 'Deu erro ao salvar';
                }
            } else {
                $retorno = 'não recebeu o arquivo';
            }
        }

        $this->set('tpview', 'add');
        $this->set('retorno', $retorno);
        $this->viewBuilder()->layout('ajax');
        $this->render('ajax');
    }

    /**
     * Edit method
     *
     * @param string|null $id Documento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $documento = $this->Documentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $documento = $this->Documentos->patchEntity($documento, $this->request->data);
            if ($this->Documentos->save($documento)) {
                $this->Flash->success(__('O registro de documento foi salvo.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O registro de documento não pôde ser salvo. Por favor, tente novamente.'));
            }
        }
        $tipodocumentos = $this->Documentos->Tipodocumentos->find('list', ['limit' => 200]);
        $empresas = $this->Documentos->Empresas->find('list', ['limit' => 200]);
        $funcionarios = $this->Documentos->Funcionarios->find('list', ['limit' => 200]);
        $this->set(compact('documento', 'tipodocumentos', 'empresas', 'funcionarios'));
        $this->set('_serialize', ['documento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Documento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $documento = $this->Documentos->get($id);
        if ($this->Documentos->delete($documento)) {
            $this->Flash->success(__('O registro de documento foi deletado.'));
        } else {
            $this->Flash->error(__('O registro de documento não pôde ser deletado. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

    /*
     * Notifica o cliente que a tríade registrou um novo documento no sistema
     */

    public function emailNovoDocCliente($documentos) {

        foreach ($documentos as $documento) {
            try {
                //envia email de notificação apenas para os administradores da empresa
                $documento = $this->Documentos->get($documento->id, ['contain' => ['Empresas.Users' => array('conditions' => ['Users.admin_empresa = 1']), 'Tipodocumentos']]);

                //enviar email por usuário por documento
                foreach ($documento->empresa->users as $user) {

                    $email = new Email('default');
                    $email->viewVars(['empresa' => $documento->empresa->razao, 'nome' => $user->nome, 'documento' => $documento]);
                    $email->subject('A Tríade anexou um novo documento no sistema -' . $documento->tipodocumento->descricao)
                            ->template('default', 'notificarclientenovodoc')
                            ->emailFormat('html')
                            ->to($user->email)
                            ->send();
                }
            } catch (Exception $ex) {
                $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
            }
        }
    }

    /*
     * Notifica os usuários da triade (que habilitaram esse tipo de notificação no perfil) que um novo documento foi enviado
     */

    public function emailNovoDocTriade($documentos) {


        try {
            $qtdeAnexos = count($documentos);

            if ($this->Auth->user('empresa')) {
                $empresanome = $this->Auth->user('empresa.razao');
                $tituloemail = 'A empresa ' . $empresanome;
            } else {
                $empresanome = false;
                $tituloemail = 'O cliente ' . $this->Auth->user('nome');
            }

            //envia para os usuarios da triade que marcaram essa opção
            $users = $this->Documentos->Users->find()->where(['empresa_id' => $this->_ID_TRIADE, 'notificacao_novodocumento = 1']);

            //enviar email por usuário da triade por documento (apenas os que marcaram essas notificações nas suas configurações)
            foreach ($users as $user) {
                $email = new Email('default');
                $email->viewVars(['empresa' => $empresanome, 'nomeremetente' => $this->Auth->user('nome'), 'qtde' => $qtdeAnexos, 'user_id' => $this->Auth->user('id'), 'nome' => $user->nome]);
                $email->subject($tituloemail . ' anexou novo(s) documento(s) no sistema')
                        ->template('default', 'notificartriadenovodoc')
                        ->emailFormat('html')
                        ->to($user->email)
                        ->send();
            }
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

    function solicitarreemissao($documento_id) {
        $documento = $this->Documentos->get($documento_id);

        if ($documento->empresa_id != $this->Auth->user('empresa_id')) {
            $this->Flash->error(__('Você não tem permissão de executar essa ação.'));
        } else {
            $this->disparaEmailSolicitaReemissao($documento);
        }
        return $this->redirect($this->referer());
    }

    function disparaEmailSolicitaReemissao($documento) {

        try {
            $user = $this->Documentos->Users->get($documento->user_enviou);

            $email = new Email('default');
            $email->viewVars(['empresa' => $empresanome, 'nomeremetente' => $this->Auth->user('nome'), 'qtde' => $qtdeAnexos, 'user_id' => $this->Auth->user('id'), 'nome' => $user->nome]);
            if ($email->subject($tituloemail . ' anexou novo(s) documento(s) no sistema')
                            ->template('default', 'notificartriadenovodoc')
                            ->emailFormat('html')
                            ->to($user->email)
                            ->send()) {
                $this->Flash->success(__('A Contabilidade recebeu sua solicitação para reemissão desse documento, entraremos em contato caso haja dúvidas.'));
            } else {
                $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
            }
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

}
