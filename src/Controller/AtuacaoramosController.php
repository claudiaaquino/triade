<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Atuacaoramos Controller
 *
 * @property \App\Model\Table\AtuacaoramosTable $Atuacaoramos
 */
class AtuacaoramosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $atuacaoramos = $this->paginate($this->Atuacaoramos);

        $this->set(compact('atuacaoramos'));
        $this->set('_serialize', ['atuacaoramos']);
    }

    /**
     * View method
     *
     * @param string|null $id Atuacaoramo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $atuacaoramo = $this->Atuacaoramos->get($id, [
            'contain' => ['Previsaoorcamentos', 'Empresas']
        ]);

        $this->set('atuacaoramo', $atuacaoramo);
        $this->set('_serialize', ['atuacaoramo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $atuacaoramo = $this->Atuacaoramos->newEntity();
        if ($this->request->is('post')) {
            $atuacaoramo = $this->Atuacaoramos->patchEntity($atuacaoramo, $this->request->data);
            $atuacaoramo->dt_cadastro =  date('Y-m-d H:i:s');
            $atuacaoramo->user_id =  $this->Auth->user('id');
            if ($this->Atuacaoramos->save($atuacaoramo)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('atuacaoramo'));
        $this->set('_serialize', ['atuacaoramo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Atuacaoramo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $atuacaoramo = $this->Atuacaoramos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atuacaoramo = $this->Atuacaoramos->patchEntity($atuacaoramo, $this->request->data);
            if ($this->Atuacaoramos->save($atuacaoramo)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('atuacaoramo'));
        $this->set('_serialize', ['atuacaoramo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atuacaoramo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $atuacaoramo = $this->Atuacaoramos->get($id);
        if ($this->Atuacaoramos->delete($atuacaoramo)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
