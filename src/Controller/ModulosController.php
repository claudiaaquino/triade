<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Modulos Controller
 *
 * @property \App\Model\Table\ModulosTable $Modulos
 */
class ModulosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        if ($this->request->is('post') && !empty($this->request->data('termo-pesquisa'))) {
            $query = $this->Modulos->find()->where(["Modulos.nome like '%" . $this->request->data('termo-pesquisa') . "%'"]);
            $query->orWhere(["Modulos.descricao like '%" . $this->request->data('termo-pesquisa') . "%'"]);
        } else {
            $query = $this->Modulos->find();
        }
        $modulos = $this->paginate($query->orderAsc('Modulos.nome'));



        $this->set(compact('modulos'));
        $this->set('_serialize', ['modulos']);
    }

    /**
     * View method
     *
     * @param string|null $id Modulo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $modulo = $this->Modulos->get($id, [
            'contain' => ['Modulosmenus.Menus', 'Usermodulos.Users.Empresas']
        ]);

        $this->set('modulo', $modulo);
        $this->set('_serialize', ['modulo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $modulo = $this->Modulos->newEntity();
        if ($this->request->is('post')) {
            $modulo = $this->Modulos->patchEntity($modulo, $this->request->data);
            $modulo->dt_cadastro = date('Y-m-d H:i:s');

            $modulomenus = array();
            if ($modulo->menu_ids) {
                foreach ($modulo->menu_ids as $menu) {
                    if (is_numeric($menu)) {// se o usuário selecionou um menu já existente
                        $modulomenu = $this->Modulos->Modulosmenus->newEntity();
                        $modulomenu->menu_id = $menu;
                        $modulomenus[] = $modulomenu;
                    } else if (is_string($menu)) {// se o usuário digitou um menu para ser criado
                        $newmenu = $this->Modulos->Modulosmenus->Menus->newEntity();
                        $newmenu->descricao = $menu;
                        $newmenu->dt_cadastro = date('Y-m-d H:i:s');
                        if ($this->Modulos->Modulosmenus->Menus->save($newmenu)) {
                            $modulomenu = $this->Modulos->Modulosmenus->newEntity();
                            $modulomenu->menu_id = $newmenu->id;
                            $modulomenus[] = $modulomenu;
                        }
                    }
                }
            }
            $modulo->modulosmenus = $modulomenus;


            if ($this->Modulos->save($modulo)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $listmenus = $this->Modulos->Modulosmenus->Menus->find('list');
        $this->set(compact('modulo', 'listmenus'));
        $this->set('_serialize', ['modulo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Modulo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $modulo = $this->Modulos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $modulo = $this->Modulos->patchEntity($modulo, $this->request->data);

            $cadastrados = $this->Modulos->Modulosmenus->find()->where(['modulo_id' => $id])->extract('menu_id')->filter()->toArray();

            $modulomenus = array();
            if ($modulo->menu_ids) {
                foreach ($modulo->menu_ids as $menu) {
                    if (!in_array($menu, $cadastrados)) {
                        if (is_numeric($menu)) {// se o usuário selecionou um menu já existente
                            $modulomenu = $this->Modulos->Modulosmenus->newEntity();
                            $modulomenu->menu_id = $menu;
                            $modulomenus[] = $modulomenu;
                        } else if (is_string($menu)) {// se o usuário digitou um menu para ser criado
                            $newmenu = $this->Modulos->Modulosmenus->Menus->newEntity();
                            $newmenu->descricao = $menu;
                            $newmenu->dt_cadastro = date('Y-m-d H:i:s');
                            if ($this->Modulos->Modulosmenus->Menus->save($newmenu)) {
                                $modulomenu = $this->Modulos->Modulosmenus->newEntity();
                                $modulomenu->menu_id = $newmenu->id;
                                $modulomenus[] = $modulomenu;
                            }
                        }
                    }
                }
            }
            $modulo->modulosmenus = $modulomenus;
            $this->Modulos->Modulosmenus->deleteAll(['modulo_id' => $id, 'menu_id NOT IN' => $modulo->menu_ids]);


            if ($this->Modulos->save($modulo)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'view', $id]);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->loadModel('Menus');
        $selectedmenus = array_keys($this->Menus->find('list')->innerJoinWith('Modulosmenus')->where(['Modulosmenus.modulo_id' => $id])->toArray());
        $listmenus = $this->Modulos->Modulosmenus->Menus->find('list');
        $this->set(compact('modulo', 'listmenus', 'selectedmenus'));
        $this->set('_serialize', ['modulo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Modulo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $modulo = $this->Modulos->get($id);
        if ($this->Modulos->delete($modulo)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

}
