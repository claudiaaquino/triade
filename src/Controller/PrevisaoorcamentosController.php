<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Previsaoorcamentos Controller
 *
 * @property \App\Model\Table\PrevisaoorcamentosTable $Previsaoorcamentos
 */
class PrevisaoorcamentosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Tiposervicos', 'Apuracaoformas', 'Atuacaoramos', 'Formaspagamentoservicos', 'Telas', 'Users']
        ];
        $previsaoorcamentos = $this->paginate($this->Previsaoorcamentos);

        $this->set(compact('previsaoorcamentos'));
        $this->set('_serialize', ['previsaoorcamentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Previsaoorcamento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $previsaoorcamento = $this->Previsaoorcamentos->get($id, [
            'contain' => ['Tiposervicos', 'Apuracaoformas', 'Atuacaoramos', 'Formaspagamentoservicos', 'Telas', 'Users']
        ]);

        $this->set('previsaoorcamento', $previsaoorcamento);
        $this->set('_serialize', ['previsaoorcamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $previsaoorcamento = $this->Previsaoorcamentos->newEntity();
        if ($this->request->is('post')) {
            $previsaoorcamento = $this->Previsaoorcamentos->patchEntity($previsaoorcamento, $this->request->data);
            $previsaoorcamento->dt_cadastro = date('Y-m-d H:i:s');
            $previsaoorcamento->user_id = $this->Auth->user('id');
            $previsaoorcamento->empresa_id = $this->Auth->user('empresa_id');

            $previsaoorcamento->valor_servico = $this->formatarmoedaTomysql($previsaoorcamento->valor_servico);
            $previsaoorcamento->valor_adicionalmensal = $this->formatarmoedaTomysql($previsaoorcamento->valor_adicionalmensal);
            $previsaoorcamento->max_faturamento_mensal = $this->formatarmoedaTomysql($previsaoorcamento->max_faturamento_mensal);
            $previsaoorcamento->min_faturamento_mensal = $this->formatarmoedaTomysql($previsaoorcamento->min_faturamento_mensal);


            if ($this->Previsaoorcamentos->save($previsaoorcamento)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $previsaoorcamento->valor_servico = $previsaoorcamento->valor_servico ? str_replace('.', ',', $previsaoorcamento->valor_servico) : '';
        $previsaoorcamento->valor_adicionalmensal = $previsaoorcamento->valor_adicionalmensal ? str_replace('.', ',', $previsaoorcamento->valor_adicionalmensal) : '';
        $previsaoorcamento->min_faturamento_mensal = $previsaoorcamento->min_faturamento_mensal ? str_replace('.', ',', $previsaoorcamento->min_faturamento_mensal) : '';
        $previsaoorcamento->max_faturamento_mensal = $previsaoorcamento->max_faturamento_mensal ? str_replace('.', ',', $previsaoorcamento->max_faturamento_mensal) : '';


        $tiposervicos = $this->Previsaoorcamentos->Tiposervicos->find('list');
        $apuracaoformas = $this->Previsaoorcamentos->Apuracaoformas->find('list');
        $atuacaoramos = $this->Previsaoorcamentos->Atuacaoramos->find('list');
        $formaspagamentoservicos = $this->Previsaoorcamentos->Formaspagamentoservicos->find('list');
        $telas = $this->Previsaoorcamentos->Telas->find('list')->where(['flag_servico = 1 ']);
        $this->set(compact('previsaoorcamento', 'tiposervicos', 'apuracaoformas', 'atuacaoramos', 'formaspagamentoservicos', 'telas'));
        $this->set('_serialize', ['previsaoorcamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Previsaoorcamento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $previsaoorcamento = $this->Previsaoorcamentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $previsaoorcamento = $this->Previsaoorcamentos->patchEntity($previsaoorcamento, $this->request->data);

            $previsaoorcamento->valor_servico = $this->formatarmoedaTomysql($previsaoorcamento->valor_servico);
            $previsaoorcamento->valor_adicionalmensal = $this->formatarmoedaTomysql($previsaoorcamento->valor_adicionalmensal);
            $previsaoorcamento->max_faturamento_mensal = $this->formatarmoedaTomysql($previsaoorcamento->max_faturamento_mensal);
            $previsaoorcamento->min_faturamento_mensal = $this->formatarmoedaTomysql($previsaoorcamento->min_faturamento_mensal);


            if ($this->Previsaoorcamentos->save($previsaoorcamento)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }


        $previsaoorcamento->valor_servico = $previsaoorcamento->valor_servico ? str_replace('.', ',', $previsaoorcamento->valor_servico) : '';
        $previsaoorcamento->valor_adicionalmensal = $previsaoorcamento->valor_adicionalmensal ? str_replace('.', ',', $previsaoorcamento->valor_adicionalmensal) : '';
        $previsaoorcamento->min_faturamento_mensal = $previsaoorcamento->min_faturamento_mensal ? str_replace('.', ',', $previsaoorcamento->min_faturamento_mensal) : '';
        $previsaoorcamento->max_faturamento_mensal = $previsaoorcamento->max_faturamento_mensal ? str_replace('.', ',', $previsaoorcamento->max_faturamento_mensal) : '';


        $tiposervicos = $this->Previsaoorcamentos->Tiposervicos->find('list');
        $apuracaoformas = $this->Previsaoorcamentos->Apuracaoformas->find('list');
        $atuacaoramos = $this->Previsaoorcamentos->Atuacaoramos->find('list');
        $formaspagamentoservicos = $this->Previsaoorcamentos->Formaspagamentoservicos->find('list');
        $telas = $this->Previsaoorcamentos->Telas->find('list')->where(['flag_servico = 1 ']);
        $this->set(compact('previsaoorcamento', 'tiposervicos', 'apuracaoformas', 'atuacaoramos', 'formaspagamentoservicos', 'telas'));
        $this->set('_serialize', ['previsaoorcamento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Previsaoorcamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $previsaoorcamento = $this->Previsaoorcamentos->get($id);
        if ($this->Previsaoorcamentos->delete($previsaoorcamento)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Calculaorcamento method
     *
     * @return \Cake\Network\Response|null
     */
    public function calculaorcamento($tiposervico_id) {

        $retorno = $this->Previsaoorcamentos->find()->where(['tiposervico_id' => $tiposervico_id])->first();

        $retorno->gratis = $this->isServicoGratuito($retorno);
        $retorno->mensalidade_atual = $this->Auth->user('empresa.mensalidade');


        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    public function isServicoGratuito($orcamento) {
        $this->loadModel('Servicos');
        if ($orcamento->gratis_todosusuarios) {
            if ($orcamento->qtdeilimitadagratis) {
                return true;
            } else {
                // fazer pesquisa de gratuidade por periodo, seja mes, ano ...etc
                if ($this->Auth->user('empresa_id')) {
                    $count = $this->Servicos->find()->where(['tiposervico_id' => $orcamento->tiposervico_id, 'empresa_id' => $this->Auth->user('empresa_id')])->count();
                } else {
                    $count = $this->Servicos->find()->where(['tiposervico_id' => $orcamento->tiposervico_id, 'user_id' => $this->Auth->user('id')])->count();
                }
                if ($count < $orcamento->qtdegratis) {
                    return true;
                }
            }
        } else if ($orcamento->gratis_todoscliente && !empty($this->Auth->user('empresa_id'))) {
            if ($orcamento->qtdeilimitadagratis) {
                return true;
            } else {
                // fazer pesquisa de gratuidade por periodo, seja mes, ano ...etc
                $count = $this->Servicos->find()->where(['tiposervico_id' => $orcamento->tiposervico_id, 'empresa_id' => $this->Auth->user('empresa_id')])->count();
            }
            if ($count < $orcamento->qtdegratis) {
                return true;
            }
        }

        return false;
    }

}
