<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Cnaesections Controller
 *
 * @property \App\Model\Table\CnaesectionsTable $Cnaesections
 */
class CnaesectionsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $cnaesections = $this->paginate($this->Cnaesections);

        $this->set(compact('cnaesections'));
        $this->set('_serialize', ['cnaesections']);
    }

    /**
     * listajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function listajax() {
        $retorno = $this->Cnaesections->find('ajaxList');
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * View method
     *
     * @param string|null $id Cnaesection id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $cnaesection = $this->Cnaesections->get($id, [
            'contain' => ['Cnaedivisions']
        ]);

        $this->set('cnaesection', $cnaesection);
        $this->set('_serialize', ['cnaesection']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $cnaesection = $this->Cnaesections->newEntity();
        if ($this->request->is('post')) {
            $cnaesection = $this->Cnaesections->patchEntity($cnaesection, $this->request->data);
            if ($this->Cnaesections->save($cnaesection)) {
                $this->Flash->success(__('Seção cadastrada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao cadastrar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('cnaesection'));
        $this->set('_serialize', ['cnaesection']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cnaesection id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $cnaesection = $this->Cnaesections->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cnaesection = $this->Cnaesections->patchEntity($cnaesection, $this->request->data);
            if ($this->Cnaesections->save($cnaesection)) {
                $this->Flash->success(__('Seção atualizada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('cnaesection'));
        $this->set('_serialize', ['cnaesection']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cnaesection id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $cnaesection = $this->Cnaesections->get($id);
        if ($this->Cnaesections->delete($cnaesection)) {
            $this->Flash->success(__('Seção removida com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

}
