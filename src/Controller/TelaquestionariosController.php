<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Telaquestionarios Controller
 *
 * @property \App\Model\Table\TelaquestionariosTable $Telaquestionarios
 */
class TelaquestionariosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Assuntos', 'Leisnormas', 'Tiposervicos', 'Documentos', 'Telas', 'Tipoactions', 'Estados', 'Cidades', 'Users']
        ];
        $telaquestionarios = $this->paginate($this->Telaquestionarios);

        $this->set(compact('telaquestionarios'));
        $this->set('_serialize', ['telaquestionarios']);
    }

    /**
     * View method
     *
     * @param string|null $id Telaquestionario id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $telaquestionario = $this->Telaquestionarios->get($id, [
            'contain' => ['Assuntos', 'Leisnormas', 'Tiposervicos', 'Documentos', 'Telas', 'Tipoactions', 'Estados', 'Cidades', 'Users', 'Telaquestionarios']
        ]);

        $this->set('telaquestionario', $telaquestionario);
        $this->set('_serialize', ['telaquestionario']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $telaquestionario = $this->Telaquestionarios->newEntity();
        if ($this->request->is('post')) {
            $telaquestionario = $this->Telaquestionarios->patchEntity($telaquestionario, $this->request->data);
            $telaquestionario->dt_cadastro =  date('Y-m-d H:i:s');
            $telaquestionario->user_id =  $this->Auth->user('id');
            $telaquestionario->empresa_id =  $this->Auth->user('empresa_id');
            if ($this->Telaquestionarios->save($telaquestionario)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $assuntos = $this->Telaquestionarios->Assuntos->find('list', ['limit' => 200]);
        $leisnormas = $this->Telaquestionarios->Leisnormas->find('list', ['limit' => 200]);
        $tiposervicos = $this->Telaquestionarios->Tiposervicos->find('list', ['limit' => 200]);
        $documentos = $this->Telaquestionarios->Documentos->find('list', ['limit' => 200]);
        $telas = $this->Telaquestionarios->Telas->find('list', ['limit' => 200]);
        $tipoactions = $this->Telaquestionarios->Tipoactions->find('list', ['limit' => 200]);
        $estados = $this->Telaquestionarios->Estados->find('list', ['limit' => 200]);
        $cidades = $this->Telaquestionarios->Cidades->find('list', ['limit' => 200]);
        $users = $this->Telaquestionarios->Users->find('list', ['limit' => 200]);
        $this->set(compact('telaquestionario', 'assuntos', 'leisnormas', 'tiposervicos', 'documentos', 'telas', 'tipoactions', 'estados', 'cidades', 'users'));
        $this->set('_serialize', ['telaquestionario']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Telaquestionario id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $telaquestionario = $this->Telaquestionarios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $telaquestionario = $this->Telaquestionarios->patchEntity($telaquestionario, $this->request->data);
            if ($this->Telaquestionarios->save($telaquestionario)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $assuntos = $this->Telaquestionarios->Assuntos->find('list', ['limit' => 200]);
        $leisnormas = $this->Telaquestionarios->Leisnormas->find('list', ['limit' => 200]);
        $tiposervicos = $this->Telaquestionarios->Tiposervicos->find('list', ['limit' => 200]);
        $documentos = $this->Telaquestionarios->Documentos->find('list', ['limit' => 200]);
        $telas = $this->Telaquestionarios->Telas->find('list', ['limit' => 200]);
        $tipoactions = $this->Telaquestionarios->Tipoactions->find('list', ['limit' => 200]);
        $estados = $this->Telaquestionarios->Estados->find('list', ['limit' => 200]);
        $cidades = $this->Telaquestionarios->Cidades->find('list', ['limit' => 200]);
        $users = $this->Telaquestionarios->Users->find('list', ['limit' => 200]);
        $this->set(compact('telaquestionario', 'assuntos', 'leisnormas', 'tiposervicos', 'documentos', 'telas', 'tipoactions', 'estados', 'cidades', 'users'));
        $this->set('_serialize', ['telaquestionario']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Telaquestionario id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $telaquestionario = $this->Telaquestionarios->get($id);
        if ($this->Telaquestionarios->delete($telaquestionario)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
