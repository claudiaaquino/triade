<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Fornecedores Controller
 *
 * @property \App\Model\Table\FornecedoresTable $Fornecedores
 */
class FornecedoresController extends AppController {

    public function isAuthorized($user) {
        if (!$this->Auth->user('admin') && $this->Auth->user('empresa_id') != $this->_ID_TRIADE && !$this->request->is('ajax') && !$this->Auth->user('admin_empresa') && !$this->Auth->user('admin_setores')) {
            return false;
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Empresas', 'Estados', 'Cidades']
        ];

        if ($this->request->is('post') && !empty($this->request->data('termo-pesquisa'))) {
            $query = $this->Fornecedores->find()->where(["Fornecedores.nome like '%" . $this->request->data('termo-pesquisa') . "%'"]);
        } else {
            $query = $this->Fornecedores->find();
        }

        $fornecedores = $this->paginate($query->where(['Fornecedores.empresa_id' => $this->Auth->user('empresa_id')])->orderAsc('Fornecedores.nome'));

        $this->set(compact('fornecedores'));
        $this->set('_serialize', ['fornecedores']);
    }

    /**
     * View method
     *
     * @param string|null $id Fornecedore id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $fornecedore = $this->Fornecedores->get($id, [
            'contain' => ['Empresas', 'Estados', 'Cidades', 'Contabancarias.Bancos',
                'Movimentacaobancarias' => ['sort' => ['Movimentacaobancarias.dt_cadastro' => 'DESC']], 'Movimentacaobancarias.Formaspagamentos',
                'Movimentacaobancarias.Contabancarias.Bancos']
        ]);

        if ($fornecedore->empresa_id != $this->Auth->user('empresa_id')) {
            $this->Flash->error(__('Você não tem permissão de visualizar um registro que não pertence à sua Empresa'));
            return $this->redirect($this->referer());
        }
        $this->set('fornecedore', $fornecedore);
        $this->set('_serialize', ['fornecedore']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $fornecedore = $this->Fornecedores->newEntity();
        if ($this->request->is('post')) {
            $fornecedore = $this->Fornecedores->patchEntity($fornecedore, $this->request->data);
            $fornecedore->dt_cadastro = date('Y-m-d H:i:s');
            $fornecedore->user_id = $this->Auth->user('id');
            $fornecedore->empresa_id = $this->Auth->user('empresa_id');
            if ($this->Fornecedores->save($fornecedore)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $estados = $this->Fornecedores->Estados->find('list');
        $this->set(compact('fornecedore', 'estados'));
        $this->set('_serialize', ['fornecedore']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fornecedore id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $fornecedore = $this->Fornecedores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fornecedore = $this->Fornecedores->patchEntity($fornecedore, $this->request->data);


            if ($fornecedore->empresa_id != $this->Auth->user('empresa_id')) {
                $this->Flash->error(__('Você não tem permissão de editar um registro que não pertence à sua Empresa'));
                return $this->redirect($this->referer());
            }

            if ($this->Fornecedores->save($fornecedore)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $estados = $this->Fornecedores->Estados->find('list');
        $this->set(compact('fornecedore', 'estados'));
        $this->set('_serialize', ['fornecedore']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fornecedore id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $fornecedore = $this->Fornecedores->get($id);
        if ($fornecedore->empresa_id == $this->Auth->user('empresa_id')) {
            if ($this->Fornecedores->delete($fornecedore)) {
                $this->Flash->success(__('O registro foi removido com sucesso.'));
            } else {
                $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
            }
        } else {
            $this->Flash->error(__('Você não tem permissão de deletar um registro que não pertence à sua Empresa'));
        }

        return $this->redirect($this->request->referer());
    }

}
