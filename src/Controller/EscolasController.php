<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Escolas Controller
 *
 * @property \App\Model\Table\EscolasTable $Escolas
 */
class EscolasController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $escolas = $this->paginate($this->Escolas);

        $this->set(compact('escolas'));
        $this->set('_serialize', ['escolas']);
    }

    /**
     * View method
     *
     * @param string|null $id Escola id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        
        $escola = $this->Escolas->get($id, [
            'contain' => ['Escolacampus', 'Funcionarioescolaridades', 'Funcionarioescolaridades.Cursos', 'Funcionarioescolaridades.Escolas', 'Funcionarioescolaridades.Funcionarios', 'Funcionarioescolaridades.Nivelescolaridades']
        ]);

        $this->set('escola', $escola);
        $this->set('_serialize', ['escola']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $escola = $this->Escolas->newEntity();
        if ($this->request->is('post')) {
            $escola = $this->Escolas->patchEntity($escola, $this->request->data);
            if ($this->Escolas->save($escola)) {
                $this->Flash->success(__('Instituição cadastrada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao cadastrar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('escola'));
        $this->set('_serialize', ['escola']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Escola id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        
        $escola = $this->Escolas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $escola = $this->Escolas->patchEntity($escola, $this->request->data);
            if ($this->Escolas->save($escola)) {
                $this->Flash->success(__('Instituição atualizada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        
        $this->set(compact('escola'));
        $this->set('_serialize', ['escola']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Escola id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        
        $this->request->allowMethod(['post', 'delete']);
        $escola = $this->Escolas->get($id);
        if ($this->Escolas->delete($escola)) {
            $this->Flash->success(__('Escola deletada com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }
}
