<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $paginate = [
        'limit' => 10
    ];
    public $_ADMIN = 1;
    public $_EMPRESA = 2;
    public $_CLIENTE = 3;
    public $_FUNCIONARIO = 4;
    public $_NEWUSER = 5;
    public $_ID_TRIADE = 1;
    public $_ID_Status_Servico_Executado = 4;
    public $_EMAIL_ADMIN = array('claudia@triadeconsultoria.adv.br'/* ,'andreluiz@triadeconsultoria.adv.br' */);
    public $_GrupoDocumentoFormacaoEmpresa = 6; //tipo de documentos para a constituição da empresa
    public $_GrupoDocumentoSocios = 17; //tipo de documentos dos sócios
    public $_GrupoDocumentoFuncionarios = 21; //tipo de documentos dos Funcionarios
    public $_ID_Setor_Gerencial = 11; //ID do registro de setor gerencial
    public $_ModulosParaTodos = array('tarefas', 'mensagens', 'documentos', 'users', 'app', 'home', 'servicos', 'empresas', 'empresasocios'); //as controller que todos os usuários podem acessar

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */

    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'App',
                'action' => 'redirectfirstView'
            ],
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'username', 'password' => 'password'],
                    'finder' => 'auth'
                ]
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'authorize' => ['Controller'],
            'authError' => 'Você não tem permissão para acessar esse conteúdo.',
            'storage' => 'Session'
        ]);
    }

    public function isAuthorized($user) {
        if ($this->Auth->user('id') && !$this->request->is('ajax') && !$this->Auth->user('admin') && $this->Auth->user('empresa_id') != $this->_ID_TRIADE) {//se estiver logado e a requisição não for ajax, verifica as permissões, para saber a qual controller o usuario tem acesso liberado
            if (!in_array(strtolower($this->request->param('controller')), array_map('strtolower', $this->_ModulosParaTodos)) && (empty($this->Auth->user('permissoes')) ||
                    !in_array(strtolower($this->request->param('controller')), array_map('strtolower', $this->Auth->user('permissoes'))))) {
                return false;
            }
        }
        return true;
    }

    public function convertDateBRtoEN($date) {
        return $date ? implode("-", array_reverse(explode("/", $date))) : null;
    }

    public function convertDateENtoBR($date) {
        return $date ? implode("/", array_reverse(explode("-", $date))) : '';
    }

    public function getCompetencia($date) { //mes/ano
        $competencia = explode("/", $date);
        return $competencia[1] . '/' . $competencia[2];
    }

    public function formatarmoedaTomysql($valor) {
        return str_replace(array('.', ','), array('', '.'), $valor); //retorna o valor formatado para gravar no banco
    }

    public function isStringEmpty($string) {
        return (!$string || $string == null || $string == 'null' || $string == '' || $string == 'undefined' || $string == '0');
    }

    public function saveAttachment($file, $path = false) {
        $setNewFileName = time() . "_" . rand(000000, 999999);
        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
        if ($path) {
            if (!is_dir(WWW_ROOT . '/docs/' . $path)) {
                mkdir(WWW_ROOT . '/docs/' . $path);
            }
            move_uploaded_file($file['tmp_name'], WWW_ROOT . '/docs/' . $path . '/' . $setNewFileName . '.' . $ext);
        } else {
            move_uploaded_file($file['tmp_name'], WWW_ROOT . '/docs/' . $setNewFileName . '.' . $ext);
        }
        return $setNewFileName . '.' . $ext;
    }

    function strip_tags_content($text, $tags = '', $invert = FALSE) {
        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
        $tags = array_unique($tags[1]);

        if (is_array($tags) AND count($tags) > 0) {
            if ($invert == FALSE) {
                return preg_replace('@<(?!(?:' . implode('|', $tags) . ')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
            } else {
                return preg_replace('@<(' . implode('|', $tags) . ')\b.*?>.*?</\1>@si', '', $text);
            }
        } elseif ($invert == FALSE) {
            return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        return $text;
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event) {
        if (!array_key_exists('_serialize', $this->viewVars) &&
                in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

        $layout_options = array('frame', 'pdf', 'acessoindevido', 'legislativo');
        if ($user_id = $this->request->session()->read('Auth.User.id')) {
            if ($this->request->is('ajax') || $this->viewBuilder()->layout() == 'ajax') {
                $this->viewBuilder()->layout('ajax');
            } else if (!in_array($this->viewBuilder()->layout(), $layout_options)) {
//                if ($this->viewBuilder()->layout() != 'frame' && $this->viewBuilder()->layout() != 'pdf') { // se vier de frame, deverá ser setado esse valor na própria action
//                if ($this->viewBuilder()->layout() != 'acessoindevido')
                $this->viewBuilder()->layout('index');

                $menus = TableRegistry::get('Menus');
                $this->set("menus", $menus->find('menusOfUsers', ['user_id' => $user_id]));

                if (!$foto = $this->request->session()->read('Auth.User.foto')) {
                    $foto = 'default.png';
                }

                $this->set("foto", $foto);
                $name = $this->request->session()->read('Auth.User.nome');
                $this->set("loggednome", $name);

                $loggedfirstname = explode(' ', $name);
                $this->set("loggedfirstname", $loggedfirstname[0]);


//colocar os menus na sessão, para não precisar ficar consultando sempre no banco de dados toda vez que for carregar uma tela */
//            if (!$this->request->session()->check('Auth.User.menus')) {
//                $menus = TableRegistry::get('Menus');
//                $this->request->session()->write('Auth.User.menus', $menus->find('menusOfUsers', ['user_id' => $user_id]));
//            }
//            $this->set("menus", $this->request->session()->read('Auth.User.menus'));
//                SOLUÇÃO:
//                ->toArray();
            }

            if ($this->request->session()->read('Auth.User.admin_setores')) {
                $this->set("admin_setores", $this->request->session()->read('Auth.User.admin_setores'));
            }

            $this->set("user_triade", ($this->request->session()->read('Auth.User.empresa_id') == $this->_ID_TRIADE));
            $this->set("admin_empresa", $this->request->session()->read('Auth.User.admin_empresa'));
            $this->set("empresa_id", $this->request->session()->read('Auth.User.empresa_id'));
            $this->set("backlink", $this->referer());
            $this->set("admin", $this->request->session()->read('Auth.User.admin'));
            $this->set("loggeduser", $this->request->session()->read('Auth.User.id'));
            $this->set("loggedIn", true);
        } else {
            $this->set("loggedIn", false);
        }
    }

    public function redirectfirstView() {

        $tipousuario_id = $this->Auth->user('tipousuario_id');
        $this->request->session()->write('Auth.User.admin', false);


        if ($tipousuario_id == $this->_ADMIN) {//ADMIN-triade
            $this->request->session()->write('Auth.User.admin', true);
            return $this->redirect(['controller' => 'Empresas', 'action' => 'index']);
        } else if ($tipousuario_id == $this->_EMPRESA) {//EMPRESAS
            return $this->redirect(['controller' => 'Empresas', 'action' => 'view', $this->Auth->user('empresa_id')]);
        } else if ($tipousuario_id == $this->_CLIENTE) {//CLIENTES
            return $this->redirect(['controller' => 'Empresas', 'action' => 'view']);
        } else if ($tipousuario_id == $this->_FUNCIONARIO) {//FUNCIONÁRIOS
            return $this->redirect(['controller' => 'Documentos', 'action' => 'add']);
        } else if ($tipousuario_id == $this->_NEWUSER) {//novo funcionario
            return $this->redirect(['controller' => 'Users', 'action' => 'view', $this->request->session()->read('Auth.User.id')]);
        }
    }

    public function beforeFilter(Event $event) {
        if (!$this->Auth->user('id')) {
            $this->Auth->config('authError', false);
        }
    }

}
