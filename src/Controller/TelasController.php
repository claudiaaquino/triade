<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Telas Controller
 *
 * @property \App\Model\Table\TelasTable $Telas
 */
class TelasController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        if ($this->request->is('post') && !empty($this->request->data('termo-pesquisa'))) {
            $query = $this->Telas->find()->where(["Telas.descricao like '%" . $this->request->data('termo-pesquisa') . "%'"]);
        } else {
            $query = $this->Telas->find();
        }
        $telas = $this->paginate($query->orderAsc('Telas.descricao'));

        $this->set(compact('telas'));
        $this->set('_serialize', ['telas']);
    }

    /**
     * View method
     *
     * @param string|null $id Tela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tela = $this->Telas->get($id, [
            'contain' => ['Telaquestionarios']
        ]);

        $this->set('tela', $tela);
        $this->set('_serialize', ['tela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $tela = $this->Telas->newEntity();
        if ($this->request->is('post')) {
            $tela = $this->Telas->patchEntity($tela, $this->request->data);
            $tela->dt_cadastro = date('Y-m-d H:i:s');
            $tela->user_id = $this->Auth->user('id');
            $tela->empresa_id = $this->Auth->user('empresa_id');
            if ($this->Telas->save($tela)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tela'));
        $this->set('_serialize', ['tela']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tela = $this->Telas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tela = $this->Telas->patchEntity($tela, $this->request->data);
            if ($this->Telas->save($tela)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tela'));
        $this->set('_serialize', ['tela']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $tela = $this->Telas->get($id);
        if ($this->Telas->delete($tela)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
