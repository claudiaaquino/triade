<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Mensagemdocumentos Controller
 *
 * @property \App\Model\Table\MensagemdocumentosTable $Mensagemdocumentos
 */
class MensagemdocumentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Mensagems', 'UserAnexous']
        ];
        $mensagemdocumentos = $this->paginate($this->Mensagemdocumentos);

        $this->set(compact('mensagemdocumentos'));
        $this->set('_serialize', ['mensagemdocumentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Mensagemdocumento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mensagemdocumento = $this->Mensagemdocumentos->get($id, [
            'contain' => ['Mensagems', 'UserAnexous']
        ]);

        $this->set('mensagemdocumento', $mensagemdocumento);
        $this->set('_serialize', ['mensagemdocumento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mensagemdocumento = $this->Mensagemdocumentos->newEntity();
        if ($this->request->is('post')) {
            $mensagemdocumento = $this->Mensagemdocumentos->patchEntity($mensagemdocumento, $this->request->data);
            if ($this->Mensagemdocumentos->save($mensagemdocumento)) {
                $this->Flash->success(__('The mensagemdocumento has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The mensagemdocumento could not be saved. Please, try again.'));
            }
        }
        $mensagems = $this->Mensagemdocumentos->Mensagems->find('list', ['limit' => 200]);
        $userAnexous = $this->Mensagemdocumentos->UserAnexous->find('list', ['limit' => 200]);
        $this->set(compact('mensagemdocumento', 'mensagems', 'userAnexous'));
        $this->set('_serialize', ['mensagemdocumento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Mensagemdocumento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mensagemdocumento = $this->Mensagemdocumentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mensagemdocumento = $this->Mensagemdocumentos->patchEntity($mensagemdocumento, $this->request->data);
            if ($this->Mensagemdocumentos->save($mensagemdocumento)) {
                $this->Flash->success(__('The mensagemdocumento has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The mensagemdocumento could not be saved. Please, try again.'));
            }
        }
        $mensagems = $this->Mensagemdocumentos->Mensagems->find('list', ['limit' => 200]);
        $userAnexous = $this->Mensagemdocumentos->UserAnexous->find('list', ['limit' => 200]);
        $this->set(compact('mensagemdocumento', 'mensagems', 'userAnexous'));
        $this->set('_serialize', ['mensagemdocumento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Mensagemdocumento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mensagemdocumento = $this->Mensagemdocumentos->get($id);
        if ($this->Mensagemdocumentos->delete($mensagemdocumento)) {
            $this->Flash->success(__('The mensagemdocumento has been deleted.'));
        } else {
            $this->Flash->error(__('The mensagemdocumento could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->request->referer());
    }
}
