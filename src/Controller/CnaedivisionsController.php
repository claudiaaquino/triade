<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Cnaedivisions Controller
 *
 * @property \App\Model\Table\CnaedivisionsTable $Cnaedivisions
 */
class CnaedivisionsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Cnaesections']
        ];
        $cnaedivisions = $this->paginate($this->Cnaedivisions);

        $this->set(compact('cnaedivisions'));
        $this->set('_serialize', ['cnaedivisions']);
    }

    /**
     * Listajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function listajax($section_id) {
        $retorno = $this->Cnaedivisions->find('ajaxList')->where(['Cnaedivisions.cnaesection_id' => $section_id]);
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * View method
     *
     * @param string|null $id Cnaedivision id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $cnaedivision = $this->Cnaedivisions->get($id, [
            'contain' => ['Cnaesections', 'Cnaegroups']
        ]);

        $this->set('cnaedivision', $cnaedivision);
        $this->set('_serialize', ['cnaedivision']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $cnaedivision = $this->Cnaedivisions->newEntity();
        if ($this->request->is('post')) {
            $cnaedivision = $this->Cnaedivisions->patchEntity($cnaedivision, $this->request->data);
            if ($this->Cnaedivisions->save($cnaedivision)) {
                $this->Flash->success(__('Divisão cadastrada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Por favor, tente novamente.'));
            }
        }
        $cnaesections = $this->Cnaedivisions->Cnaesections->find('list', ['limit' => 200]);
        $this->set(compact('cnaedivision', 'cnaesections'));
        $this->set('_serialize', ['cnaedivision']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cnaedivision id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $cnaedivision = $this->Cnaedivisions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cnaedivision = $this->Cnaedivisions->patchEntity($cnaedivision, $this->request->data);
            if ($this->Cnaedivisions->save($cnaedivision)) {
                $this->Flash->success(__('Divisão atualizada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        
        $cnaesections = $this->Cnaedivisions->Cnaesections->find('list', ['limit' => 200]);
        $this->set(compact('cnaedivision', 'cnaesections'));
        $this->set('_serialize', ['cnaedivision']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cnaedivision id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $cnaedivision = $this->Cnaedivisions->get($id);
        if ($this->Cnaedivisions->delete($cnaedivision)) {
            $this->Flash->success(__('Divisão removida com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

}
