<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Tarefas Controller
 *
 * @property \App\Model\Table\TarefasTable $Tarefas
 */
class TarefasController extends AppController {

    public function beforeFilter(Event $event) {
        if (!$this->Auth->user('id')) {
            $this->Auth->allow(['gerartarefas', 'gerardeveresclientes', 'disparaEmailSemanalResponsavel', 'disparaEmailDiarioResponsavel', 'disparaEmailPrazoFinalResponsavel', 'disparaEmailLembretesProgramadosHoje', 'disparaEmailAlertaAtrasoResponsavel', 'disparaEmailAlertaAtrasoSupervisores']);
        }
    }

    /**
     * Index method
     * @return \Cake\Network\Response|null
     */
    public function index() {

        $this->paginate = [
            'contain' => ['Users', 'Tarefatipos', 'Tarefaprioridades', 'Areaservicos', 'Tiposervicos']
        ];

//*SE FOR ADMIN TRIADE, EMPRESA OU SETOR, ELE PODE USAR OS FILTROS PARA PODER VISUALIZAR TAREFAS DA EMPRESA, ATRIBUIDAS À OUTROS USUÁRIOS**/

        $tarefas = $this->paginate($this->Tarefas
                        ->find()
                        ->innerJoinWith('Tarefausuarios')
                        ->orderAsc('Tarefas.dt_inicio')
                        ->where(['Tarefausuarios.user_id' => $this->Auth->user('id')])
        );

        $this->set(compact('tarefas'));
        $this->set('_serialize', ['tarefas']);
    }

    /**
     * Upcomingtasks method
     *
     * @return \Cake\Network\Response|null
     */
    public function upcomingtasksajax() {
        $tarefas = $this->Tarefas->find()
                ->innerJoinWith('Tarefausuarios')
                ->contain(['Tarefaprioridades', 'Tarefatipos'])
                ->where(['Tarefausuarios.user_id' => $this->Auth->user('id')])
                ->andWhere(['Tarefausuarios.dt_concluida is null'])
                ->andWhere(['Tarefas.dt_concluida is null'])
                ->orderAsc('Tarefas.dt_inicio')
                ->limit(4);

        $count_upcoming = $tarefas->count();
        $this->set(compact('tarefas', 'count_upcoming'));
        $this->set('_serialize', ['tarefas']);
    }

    /**
     * View method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tarefa = $this->Tarefas->get($id, [
            'contain' => ['Empresas', 'Areaservicos', 'Users', 'Tarefatipos', 'Tiposervicos', 'Tarefaprioridades', 'Tarefadocumentos.Documentos.Tipodocumentos', 'Tarefausuarios.Users']
        ]);

        if (!$this->Auth->user('admin') && $this->Auth->user('empresa_id') != $this->_ID_TRIADE &&
                (($tarefa->empresa_id && $tarefa->empresa_id != $this->Auth->user('empresa_id')) || (!$tarefa->empresa_id && $tarefa->user_id && $tarefa->user_id != $this->Auth->user('id'))
                )) {
            $this->Flash->error(__('Você não tem permissão de visualizar esse registro'));
            return $this->redirect($this->referer());
        }

        if ($tarefa->dt_vencimento) {
            $tarefa->competencia = $this->getCompetencia($tarefa->dt_vencimento);
        }

        if ($tarefa->tiposervico_id) {
            $documentos = $this->listdocsinstrucao($tarefa->tiposervico_id);
            $diretorio = 'docs\\instrucao_servicos\\' . $tarefa->tiposervico_id . '\\';
        }

        $this->set('tarefa', $tarefa);
        $this->set(compact('tarefa', 'diretorio', 'documentos'));
        $this->set('_serialize', ['tarefa']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {

        $tarefa = $this->Tarefas->newEntity();
        if ($this->request->is('post')) {
            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->data);
            $tarefa->dt_lembrete = $this->convertDateBRtoEN($tarefa->dt_lembrete);
            $tarefa->hr_tarefa = $tarefa->hr_tarefa ? $tarefa->hr_tarefa : '';
            $tarefa->dt_inicio = $this->convertDateBRtoEN($tarefa->dt_inicio) . ' ' . $tarefa->hr_tarefa;
            $tarefa->dt_final = $this->convertDateBRtoEN($tarefa->dt_final);
            $tarefa->empresa_cadastrou = $this->Auth->user('empresa_id');
            $tarefa->empresa_id = $tarefa->empresa_id ? $tarefa->empresa_id : $this->Auth->user('empresa_id');
            /* --------------------------------------------------------------------------------------------- */
            /* --------------------------------------------------------------------------------------------- */
            $users = array();
            if ($tarefa->user_ids) {
//fazer aqui para capturar emails digitados também.
            } else if ($tarefa->admin_empresa) {
                $tarefa->user_ids = $this->Tarefas->Users->find()->select(['id'])->where(['empresa_id' => $tarefa->userempresa_id, 'admin_empresa = 1 ']);
            } else if ($tarefa->admin_setor) {
                $tarefa->user_ids = $this->Tarefas->Users->innerJoinWith('Usersareaservicos')->find()->select(['id'])->where(['Users.empresa_id' => $tarefa->userempresa_id, 'Usersareaservicos.areaservico_id' => $tarefa->areaservico_id, 'Usersareaservicos.admin_setor = 1 ']);
            } else if ($tarefa->userempresa_id && (!$tarefa->areaservico_id || $tarefa->areaservico_id == 'null')) {
                $tarefa->user_ids = $this->Tarefas->Users->find()->select(['id'])->where(['empresa_id' => $tarefa->userempresa_id]);
            } else if ($tarefa->userempresa_id && ($tarefa->areaservico_id && $tarefa->areaservico_id != 'null')) {
                $tarefa->user_ids = $this->Tarefas->Users->find()->innerJoinWith('Usersareaservicos')->select(['Users.id'])
                        ->where(['Usersareaservicos.empresa_id' => $tarefa->userempresa_id, 'Usersareaservicos.areaservico_id' => $tarefa->areaservico_id]);
            }

            if ($tarefa->user_ids) {
                foreach ($tarefa->user_ids as $user_id) {
                    $user_id = is_numeric($user_id) ? $user_id : $user_id->id;
                    $user = $this->Tarefas->Tarefausuarios->newEntity();
                    $user->user_id = $user_id;
                    $users[] = $user;
                }
            } else {
                $user = $this->Tarefas->Tarefausuarios->newEntity();
                $user->user_id = $this->Auth->user('id');
                $users[] = $user;
            }
            $tarefa->tarefausuarios = $users;


            $tarefadocumentos = array();
            if (!empty($tarefa->file)) {
                $tarefadocumentos = $this->saveDocuments($tarefa);
            }
            $tarefa->tarefadocumentos = $tarefadocumentos;


            /* --------------------------------------------------------------------------------------------- */
            /* --------------------------------------------------------------------------------------------- */

            $tarefa->dt_cadastro = date('Y-m-d h:i:s');
            $tarefa->user_id = $this->Auth->user('id');
            if ($this->Tarefas->save($tarefa)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'agenda']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }


        $tarefa->dt_lembrete = $this->convertDateENtoBR($tarefa->dt_lembrete);
        $dt_tarefa = explode(' ', $tarefa->dt_inicio);
        $tarefa->dt_inicio = $this->convertDateENtoBR($dt_tarefa[0]);
        $tarefa->hr_tarefa = $dt_tarefa[1];
        $tarefa->dt_final = $this->convertDateENtoBR($tarefa->dt_final);


        $users = $this->Tarefas->Users->find('list');
        $tarefatipos = $this->Tarefas->Tarefatipos->find('list');
        $tarefaprioridades = $this->Tarefas->Tarefaprioridades->find('list');
        $areaservicos = $this->Tarefas->Areaservicos->find('list');
        $this->set(compact('tarefa', 'users', 'tarefatipos', 'tarefaprioridades', 'areaservicos', 'tiposervicos'));
        $this->set('_serialize', ['tarefa']);
    }

    /**
     * SaveDocument method
     *
     * @return Mensagensdocumentos[] $mensagemdocumentos
     */
    public function saveDocuments($tarefa) {
        $tarefadocumentos = array();
        $this->loadModel('Documentos');
        foreach ($tarefa->file as $key => $file) {
            if ($file['size'] > 0) {
                $documento = $this->Documentos->newEntity();
                $documento->empresa_id = $this->Auth->user('empresa_id');
                $documento->user_enviou = $this->Auth->user('id');
                $documento->filesize = $file['size'];
                $documento->tipodocumento_id = $tarefa->tipo_documentos[$key];
                if ($tarefa->funcionario[$key] != 'null' && $tarefa->funcionario[$key] != '0') {
                    $documento->funcionario_id = $tarefa->funcionario[$key];
                }
                $documento->descricao = $tarefa->descricoes[$key];
                $documento->file = $this->saveAttachment($file);
                $documento->dt_enviado = date('Y-m-d H:i:s');
                if ($this->Documentos->save($documento)) {
                    $tarefadocumento = $this->Tarefas->Tarefadocumentos->newEntity();
                    $tarefadocumento->documento_id = $documento->id;
                    $tarefadocumentos[] = $tarefadocumento;
                }
            }
        }
        return $tarefadocumentos;
    }

    /**
     * Edit method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tarefa = $this->Tarefas->get($id, [
            'contain' => ['Tarefadocumentos.Documentos.Tipodocumentos', 'Tarefausuarios.Users', 'Tiposervicos']
        ]);
        if ($tarefa->user_id != $this->Auth->user('id') && !$tarefa->permite_editar) {
            $this->Flash->error(__('Você não tem permissão para deletar essa tarefa.'));
            return $this->redirect($this->referer());
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->data);
            $tarefa->dt_lembrete = $this->convertDateBRtoEN($tarefa->dt_lembrete);
            $tarefa->hr_tarefa = $tarefa->hr_tarefa ? $tarefa->hr_tarefa : '';
            $tarefa->dt_inicio = $this->convertDateBRtoEN($tarefa->dt_inicio) . ' ' . $tarefa->hr_tarefa;
            $tarefa->dt_final = $this->convertDateBRtoEN($tarefa->dt_final);
            $tarefa->dt_concluida = $this->convertDateBRtoEN($tarefa->dt_concluida);

            if ($this->Tarefas->save($tarefa)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect($this->referer());
//                return $this->redirect(['action' => 'agenda']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $tarefa->dt_lembrete = $this->convertDateENtoBR($tarefa->dt_lembrete);
        $dt_tarefa = explode(' ', $tarefa->dt_inicio);
        $tarefa->dt_inicio = $this->convertDateENtoBR($dt_tarefa[0]);
        $tarefa->hr_tarefa = $dt_tarefa[1];
        $tarefa->dt_final = $this->convertDateENtoBR($tarefa->dt_final);
        $tarefa->dt_concluida = $this->convertDateENtoBR($tarefa->dt_concluida);


        $empresas = $this->Tarefas->Empresas->find('list');
        $tarefatipos = $this->Tarefas->Tarefatipos->find('list');
        $tarefaprioridades = $this->Tarefas->Tarefaprioridades->find('list');
        $areaservicos = '';
        if ($tarefa->empresa_id) {
            $areaservicos = $this->Tarefas->Areaservicos->find('list')->where(['empresa_id' => $tarefa->empresa_id])->orWhere(['todos = 1']);
        }
        $gruposervicos = '';
        if ($tarefa->areaservico_id) {
            $gruposervicos = $this->Tarefas->Areaservicos->Gruposervicos->find('list')->where(['areaservico_id' => $tarefa->areaservico_id]);
        }
        $tiposervicos = '';
        if ($tarefa->tiposervico) {
            $tiposervicos = $this->Tarefas->Tiposervicos->find('list')->where(['gruposervico_id' => $tarefa->tiposervico->gruposervico_id]);
        }

        $this->set(compact('tarefa', 'empresas', 'tarefatipos', 'tarefaprioridades', 'areaservicos', 'tiposervicos', 'gruposervicos'));
        $this->set('_serialize', ['tarefa']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {

        $this->request->allowMethod(['post', 'delete']);
        $tarefa = $this->Tarefas->get($id);
        /* acrescentar aqui para verificar se ele está dentre os usuários atribuidos par poder deletar a tarefa */
        if ($tarefa->user_id == $this->Auth->user('id') || $tarefa->permite_deletar) {
            if ($this->Tarefas->delete($tarefa)) {
                $this->Flash->success(__('O registro foi removido com sucesso.'));
            } else {
                $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
            }
        } else {
            $this->Flash->error(__('Você não tem permissão de deletar esse registro.'));
        }

        return $this->redirect($this->request->referer());
    }

    /**
     *  Função para carregar a view agenda e seus dados.
     */
    public function agenda() {

        $tarefa = $this->Tarefas->newEntity();
        $tarefatipos = $this->Tarefas->Tarefatipos->find('list')
                ->where(['Tarefatipos.empresa_id' => $this->Auth->user('empresa_id'), 'Tarefatipos.todos_empresa' => '1'])
                ->orWhere(['Tarefatipos.todos_sistema' => '1'])
                ->orWhere(['Tarefatipos.user_id' => $this->Auth->user('id')]);
        $tarefaprioridades = $this->Tarefas->Tarefaprioridades->find('list');

        $areaservicos = $this->Tarefas->Areaservicos->find('list')->where(['todos' => 1])->orWhere(['empresa_id' => $this->Auth->user('empresa_id')]);
        $empresas = $this->Tarefas->Empresas->find('list');
        $users = $this->Tarefas->Users->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')]);
        $meses = array(1 => 'Janeiro', 2 => 'Fevereiro', 3 => 'Março', 4 => 'Abril', 5 => 'Maio', 6 => 'Junho', 7 => 'Julho', 8 => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro', 12 => 'Dezembro');
        $ano = date('Y') - 2;
        $anos = array($ano, ++$ano, ++$ano, ++$ano, ++$ano);
        $this->set(compact('tarefa', 'tarefatipos', 'tarefaprioridades', 'areaservicos', 'empresas', 'users', 'meses', 'anos'));
        $this->set('_serialize', ['tarefa']);
    }

    /**
     * Função para carregar as tarefas via ajax.
     */
    public function load() {

        $users_id = !empty($this->request->data("filtro_user_ids")) ? array_values($this->request->data("filtro_user_ids")) : array();
        $empresas_id = !empty($this->request->data("filtro_empresa_id")) ? array_values($this->request->data("filtro_empresa_id")) : array();
        $areaservico_id = $this->request->data("filtro_areaservico_id");
        $gruposervico_id = $this->request->data("filtro_gruposervico_id");
        $tiposervico_id = $this->request->data("filtro_tiposervico_id");
        $tarefatipo_id = $this->request->data("filtro_tarefatipo_id");
        $tarefaprioridade_id = $this->request->data("filtro_tarefaprioridade_id");
        $meses = !empty($this->request->data("mes")) ? array_values($this->request->data("mes")) : array();
        $anos = !empty($this->request->data("ano")) ? array_values($this->request->data("ano")) : array();
        $status = $this->request->data("filtro_status");

//esse filtro: filtro_minhas_tarefas, só aparece para admin, pois do contrário, o usuário apenas verá as suas próprias mesmo
        if (!$this->Auth->user('user_triade') || $this->request->data("filtro_minhas_tarefas")) {
            $tarefas = $this->Tarefas->find()->innerJoinWith('Tarefausuarios')->leftJoinWith('Tiposervicos')->leftJoinWith('Tipodeveres')->contain(['Tarefatipos', 'Tarefaprioridades', 'Empresas', 'Tiposervicos', 'Servicos'])->where(['Tarefausuarios.user_id' => $this->Auth->user('id')]);
        } else if (!empty($users_id)) {
            $tarefas = $this->Tarefas->find()->innerJoinWith('Tarefausuarios')->leftJoinWith('Tiposervicos')->contain(['Tarefatipos', 'Tarefaprioridades', 'Empresas', 'Tiposervicos', 'Servicos'])->where(['Tarefausuarios.user_id IN ' => $users_id]);
        } else if (!empty($empresas_id) && !$this->request->data("filtro_minhas_tarefas")) {
            $tarefas = $this->Tarefas->find()->innerJoinWith('Tarefausuarios')->leftJoinWith('Tiposervicos')->contain(['Tarefatipos', 'Tarefaprioridades', 'Empresas', 'Tiposervicos', 'Servicos']);
        } else {
//             if ($this->request->data("filtro_minhas_tarefas") || (!$this->Auth->user('admin') && $this->Auth->user('empresa_id') != $this->_ID_TRIADE)) {
            $tarefas = $this->Tarefas->find()->innerJoinWith('Tarefausuarios')->leftJoinWith('Tiposervicos')->leftJoinWith('Tipodeveres')->contain(['Tarefatipos', 'Tarefaprioridades', 'Empresas', 'Tiposervicos', 'Servicos']);
        }


        if (!empty($empresas_id)) {
            $tarefas->where(['Tarefas.empresa_id IN ' => $empresas_id]);
        }

        if (!empty($areaservico_id) && $areaservico_id != '0') {
            $tarefas->where(['Tarefas.areaservico_id' => $areaservico_id]);
        }

        if (!empty($gruposervico_id) && $gruposervico_id != '0') {
            $tarefas->where(['Tiposervicos.gruposervico_id' => $gruposervico_id]);
        }

        if (!empty($tiposervico_id) && $tiposervico_id != '0') {
            $tarefas->where(['Tarefas.tiposervico_id' => $tiposervico_id]);
        }

        if (!empty($tarefatipo_id) && $tarefatipo_id != '0') {
            $tarefas->where(['Tarefas.tarefatipo_id' => $tarefatipo_id]);
        }

        if (!empty($tarefaprioridade_id) && $tarefaprioridade_id != '0') {
            $tarefas->where(['Tarefas.tarefaprioridade_id' => $tarefaprioridade_id]);
        }


        if (!empty($meses)) {
            $tarefas->where(['MONTH(Tarefas.dt_inicio) IN' => $meses])->orWhere(['MONTH(Tarefas.dt_final) IN' => $meses]);
        }

        if (!empty($anos)) {
            $tarefas->where(['YEAR(Tarefas.dt_inicio) IN' => $anos])->orWhere(['YEAR(Tarefas.dt_final) IN' => $anos]);
        }


        if ($status == '0') {//Atrasada
//                $tarefas->where(['Tarefas.dt_final < SYSDATE()', '(Tarefas.dt_concluida IS NULL   OR Tarefausuarios.dt_concluida IS NULL)']);
            $tarefas->where(['Tarefas.dt_final < SYSDATE()', 'Tarefas.dt_concluida IS NULL']);
        } else if ($status == '1') {//No prazo
//                $tarefas->where(['Tarefas.dt_final >= SYSDATE()', '(Tarefas.dt_concluida IS NULL   OR Tarefausuarios.dt_concluida IS NULL)']);
            $tarefas->where(['Tarefas.dt_final >= SYSDATE()', 'Tarefas.dt_concluida IS NULL']);
        } else if ($status == '2') {//Concluída
            $tarefas->where(['Tarefas.dt_concluida IS NOT NULL  OR Tarefausuarios.dt_concluida IS NOT NULL']);
        }

//        print_r($tarefas->sql()); 

        $this->set(compact('tarefas'));
        $this->set('_serialize', ['tarefas']);
    }

    /**
     * Função para remover a tarefa via ajax. Os parametros são passados por json.
     */
    public function deleteajax() {
        $id = $this->request->data["id"];
        $this->request->allowMethod(['post', 'delete']);
        $tarefa = $this->Tarefas->get($id);

        if ($this->Tarefas->delete($tarefa)) {
            $success = true;
        } else {
            $success = false;
        }
    }

    /**
     * Função para concluir a tarefa via ajax. 
     */
    public function concluir($id) {

        $tarefa = $this->Tarefas->get($id, ['contain' => ['Tiposervicos', 'Tarefadocumentos.Documentos', 'Servicos']]);
        $tarefa->dt_concluida = date('Y-m-d h:i:s');
        $tarefa->anotacao_interna = $this->request->data("anotacao_interna");
        $tarefa->user_id = $this->Auth->user('id');
        $tarefa->has_movimento = $this->request->data("has_movimento") ? $this->request->data("has_movimento") : '0';

        if ($tarefa->tiposervico) {
            $tarefa->anotacao_cliente = $this->request->data("anotacao_cliente");
            if ($this->request->data("dt_vencimento")) {
                $tarefa->dt_vencimento = $this->request->data("dt_vencimento");
            }
        }

        $tarefausuario = $this->Tarefas->Tarefausuarios->find()->where(['tarefa_id' => $id, 'user_id' => $this->Auth->user('id')])->first();
        $tarefausuario->dt_concluida = date('Y-m-d h:i:s');

        /** Se essa tarefa foi gerada a partir de uma solicitação de serviço, então atualiza o status do serviço como concluido tbm */
        if ($tarefa->servico_id) {
            $servico = $this->Tarefas->Servicos->find()->where(['id' => $tarefa->servico_id])->first();
            $servico->status_servico = $this->_ID_Status_Servico_Executado;
            $servico->user_executou = $this->Auth->user('id');
            $servico->dt_finalizado = date('Y-m-d');
        }


        if ($this->Tarefas->save($tarefa) && $this->Tarefas->Tarefausuarios->save($tarefausuario)) {

            /** atualiza o serviço referente à essa tarefa */
            if ($tarefa->servico_id) {
                $this->Tarefas->Servicos->save($servico);
            }

            /** Se for alguma tarefa referente à um servico */
            if ($tarefa->tiposervico) {
                $this->loadModel('Users');
                $user = $this->Users->find()->where(['empresa_id' => $tarefa->empresa_id, 'admin_empresa = 1'])->first();
                /** Notifica o cliente que essa tarefa foi concluida */
                if ($tarefa->tiposervico->notificacao_concluido_cliente && !$tarefa->tiposervico->notificacao_dt_vencimento) {
                    $this->disparaEmailClienteTarefaConcluida($tarefa, $user);
                }
                /** Notifica o cliente que essa tarefa foi concluida e que ele tem um documento a ser pago na tal data de vencimento */
                /** Só vai notificar o cliente quando tiver movimentação no mês */
                if ($tarefa->tiposervico->notificacao_concluido_cliente && $tarefa->tiposervico->notificacao_dt_vencimento && $tarefa->dt_vencimento && $tarefa->has_movimento) {
                    $this->disparaEmailClienteNovoDocVencimento($tarefa, $user);
                    /** gera um registro em cronlembretes para enviar email pro cliente quando chegar a data de vencimento */
                    $this->geraLembreteVencimento($tarefa, $user);
                }
            }

            $retorno = $tarefa->dt_concluida;
        } else {
            $retorno = null;
        }

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Função para reabrir a tarefa via ajax. 
     */
    public function reabrir($id) {
        $tarefa = $this->Tarefas->get($id, ['contain' => ['Empresas', 'Tiposervicos', 'Tarefausuarios' => array('conditions' => ['Tarefausuarios.dt_concluida is not null'])]]);

        if (!$this->Auth->user('admin') && $tarefa->user_id != $this->Auth->user('id')) {
            $this->Flash->error(__('Você não tem permissão para reabrir essa tarefa.'));
            return $this->redirect($this->request->referer());
        }

        $tarefa->dt_reaberta = date('Y-m-d h:i:s');
        $tarefa->dt_concluida = null;
        $tarefa->justificativa_refazer = $this->request->data("justificativa_refazer");
        $tarefa->notificacao_refazer = $this->request->data("notificacao_refazer");

        if ($this->Tarefas->save($tarefa)) {
            if ($tarefa->notificacao_refazer) {
                $this->disparaEmailTarefaReabertaResponsavel($tarefa);
            }
            $retorno = $tarefa->dt_reaberta;
        } else {
            $retorno = null;
        }

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Lista os documentos cadastrados com instruções para a execução desse serviço
     */
    public function listdocsinstrucao($tiposervico_id) {
        $documentos = array();
        $diretorio = WWW_ROOT . 'docs\\instrucao_servicos\\' . $tiposervico_id;
        if (is_dir($diretorio)) {
            $documentos = array_diff(scandir($diretorio), array('..', '.'));
        }

        return $documentos;
    }

    /**
     *  Função utilizada no cronjob
     * */
    public function gerartarefas() {
        $this->loadModel('Tiposervicos');
        $servicos = $this->Tiposervicos->find()->contain(['Userservicos', 'Empresaservicos.Empresas', 'Gruposervicos'])->where(['tarefa_automatica = 1']);
        $tarefas_ids = array();
        foreach ($servicos as $servico) {
            $meses = array();
            if (!empty($servico->meses)) {
                @$result = array_filter(json_decode($servico->meses));
                if (!empty($result)) {
                    $meses = $result;
                }
            }


            if ((($servico->anual || $servico->mensal) && in_array(date('m'), $meses)) || ($servico->mensal && empty($meses))) {

                $dia_fim_execucao = Time::now();
                $dia_fim_execucao->day($servico->dia_fim_execucao);
                $dia_fim_execucao->hour(0)->minute(0)->second(0);
                if ($servico->dia_fim_execucao_uteis) {
                    if ($dia_fim_execucao->isSaturday()) {
                        $dia_fim_execucao->addDays(2);
                    } else if ($dia_fim_execucao->isSunday()) {
                        $dia_fim_execucao->addDay();
                    }
                }


//se nao for informado a data de inicio, ela será a mesma do dia_fim_execucao
                if (!$servico->dia_inicio_execucao)
                    $servico->dia_inicio_execucao = $servico->dia_fim_execucao;

                $dia_inicio_execucao = Time::now();
                $dia_inicio_execucao->day($servico->dia_inicio_execucao);
                $dia_inicio_execucao->hour(0)->minute(0)->second(0);
                if ($servico->dia_inicio_execucao_uteis) {
                    if ($dia_inicio_execucao->isSaturday()) {
                        $dia_inicio_execucao->addDays(2);
                    } else if ($dia_inicio_execucao->isSunday()) {
                        $dia_inicio_execucao->addDay();
                    }
                }


//se nao for informado a data de inicio, ela será a mesma do dia_fim_execucao
                if (!$servico->dia_lembrete_execucao)
                    $servico->dia_lembrete_execucao = $servico->dia_fim_execucao;

                $dia_lembrete_execucao = Time::now();
                $dia_lembrete_execucao->day($servico->dia_lembrete_execucao);
//            $dia_lembrete_execucao->subDay(); // o sistema vai alertar um dia antes do prazo limite para conlusão da tarefa
                $dia_lembrete_execucao->hour(0)->minute(0)->second(0);
                if ($servico->dia_lembrete_execucao_uteis) {
                    if ($dia_lembrete_execucao->isSaturday()) {
                        $dia_lembrete_execucao->subDay();
                    } else if ($dia_lembrete_execucao->isSunday()) {
                        $dia_lembrete_execucao->subDays(2);
                    }
                }


//verifica se essa tarefa já foi cadastrada para esse cliente
                if ($servico->empresaservicos) {
                    foreach ($servico->empresaservicos as $empresa) {
                        if ($this->Tarefas->find()->where(['automatica = 1', 'empresa_id' => $empresa->empresa_id, 'tiposervico_id' => $servico->id, 'dt_inicio' => $dia_inicio_execucao->toDateTimeString(), 'dt_final' => $dia_fim_execucao->toDateTimeString()])->count() <= 0) {
                            $tarefa = $this->Tarefas->newEntity();
                            $tarefa->tiposervico_id = $servico->id;
                            $tarefa->empresa_id = $empresa->empresa_id;
                            $tarefa->areaservico_id = $servico->gruposervico->areaservico_id;
                            $tarefa->automatica = 1;
                            $tarefa->permite_deletar = 0; // não permite deletar
                            $tarefa->empresa_cadastrou = $this->_ID_TRIADE;
                            $tarefa->cor_tarefa = $servico->cor_tarefa;
                            $tarefa->titulo = 'Serviço: ' . $servico->nome . ' da Empresa: ' . $empresa->empresa->razao;
                            $tarefa->descricao = 'Tarefa gerada automaticamente pelo sistema: ' . $servico->descricao;
                            $tarefa->dt_final = $dia_fim_execucao->toDateTimeString();
                            $tarefa->dt_inicio = $dia_inicio_execucao->toDateTimeString();
                            $tarefa->dt_lembrete = $dia_lembrete_execucao->toDateTimeString();
                            $tarefa->dt_cadastro = date('Y-m-d H:i:s');

//tipo de tarefa - cmpo obrigatorio
                            $tipotarefa = $this->Tarefas->Tarefatipos->find()->where(['descricao' => 'Serviço de Rotina da Tríade'])->first();
                            if (empty($tipotarefa)) {
                                $tipotarefa = $this->Tarefas->Tarefatipos->newEntity();
                                $tipotarefa->descricao = 'Serviço de Rotina da Tríade';
                                $tipotarefa->empresa_id = $this->_ID_TRIADE;
                                $tipotarefa->user_id = 1;
                                $tipotarefa->todos_empresa = 1;
                                $tipotarefa->dt_cadastro = date('Y-m-d H:i:s');
                                $this->Tarefas->Tarefatipos->save($tipotarefa);
                            }
                            $tarefa->tarefatipo_id = $tipotarefa->id;


//prioridade  
                            $prioridade = $this->Tarefas->Tarefaprioridades->find()->orderAsc('ordem')->first();
                            if (empty($prioridade)) {
                                $prioridade = $this->Tarefas->Tarefaprioridades->newEntity();
                                $prioridade->descricao = 'Alta';
                                $prioridade->ordem = 1;
                                $prioridade->dt_cadastro = date('Y-m-d H:i:s');
                                $this->Tarefas->Tarefaprioridades->save($prioridade);
                            }
                            $tarefa->tarefaprioridade_id = $prioridade->id;



                            $users = array();
                            if ($servico->has('userservicos')) {
                                foreach ($servico->userservicos as $userservico) {
                                    $user = $this->Tarefas->Tarefausuarios->newEntity();
                                    $user->user_id = $userservico->user_id;
                                    $users[] = $user;
                                }
                            }
                            $tarefa->tarefausuarios = $users;

                            $this->Tarefas->save($tarefa);
                            $tarefas_ids[] = $tarefa->id;
                        }
                    }
                }
            }
        }

        if (!empty($tarefas_ids)) {
            $tarefas = $this->Tarefas->find()->contain(['Tarefausuarios.Users', 'Empresas', 'Tarefaprioridades', 'Tiposervicos', 'Areaservicos'])->where(['Tarefas.id IN' => $tarefas_ids]);

            $this->disparaEmailTarefasAdmin($tarefas);
            $this->disparaEmailResponsavelTarefa($tarefas_ids);
        }



//        $this->viewBuilder()->layout('frame');
        $this->set(compact('tarefas'));
        $this->set('_serialize', ['tarefas']);
    }

    public function gerardeveresclientes() {
        $this->loadModel('Tipodeveres');
        $servicos = $this->Tipodeveres->find()->contain(['Empresadeveres', 'Gruposervicos'])->where(['tarefa_automatica = 1']);
        $tarefas_ids = array();
        foreach ($servicos as $servico) {

            $dia_fim_execucao = Time::now();
            $dia_fim_execucao->day($servico->dia_fim_execucao);
            $dia_fim_execucao->hour(0)->minute(0)->second(0);
            if ($servico->dia_fim_execucao_uteis) {
                if ($dia_fim_execucao->isSaturday()) {
                    $dia_fim_execucao->addDays(2);
                } else if ($dia_fim_execucao->isSunday()) {
                    $dia_fim_execucao->addDay();
                }
            }


//se nao for informado a data de inicio, ela será a mesma do dia_fim_execucao
            if (!$servico->dia_inicio_execucao)
                $servico->dia_inicio_execucao = $servico->dia_fim_execucao;

            $dia_inicio_execucao = Time::now();
            $dia_inicio_execucao->day($servico->dia_inicio_execucao);
            $dia_inicio_execucao->hour(0)->minute(0)->second(0);
            if ($servico->dia_inicio_execucao_uteis) {
                if ($dia_inicio_execucao->isSaturday()) {
                    $dia_inicio_execucao->addDays(2);
                } else if ($dia_inicio_execucao->isSunday()) {
                    $dia_inicio_execucao->addDay();
                }
            }


//se nao for informado a data de inicio, ela será a mesma do dia_fim_execucao
            if (!$servico->dia_lembrete_execucao)
                $servico->dia_lembrete_execucao = $servico->dia_fim_execucao;

            $dia_lembrete_execucao = Time::now();
            $dia_lembrete_execucao->day($servico->dia_lembrete_execucao);
//            $dia_lembrete_execucao->subDay(); // o sistema vai alertar um dia antes do prazo limite para conlusão da tarefa
            $dia_lembrete_execucao->hour(0)->minute(0)->second(0);
            if ($servico->dia_lembrete_execucao_uteis) {
                if ($dia_lembrete_execucao->isSaturday()) {
                    $dia_lembrete_execucao->subDay();
                } else if ($dia_lembrete_execucao->isSunday()) {
                    $dia_lembrete_execucao->subDays(2);
                }
            }


//verifica se essa tarefa já foi cadastrada para esse cliente
            if ($servico->empresadeveres) {
                foreach ($servico->empresadeveres as $empresa) {
                    if ($this->Tarefas->find()->where(['automatica = 1', 'empresa_id' => $empresa->empresa_id, 'tipodever_id' => $servico->id, 'dt_inicio' => $dia_inicio_execucao->toDateTimeString(), 'dt_final' => $dia_fim_execucao->toDateTimeString()])->count() <= 0) {
                        $tarefa = $this->Tarefas->newEntity();
                        $tarefa->tipodever_id = $servico->id;
                        $tarefa->empresa_id = $empresa->empresa_id;
                        $tarefa->areaservico_id = $servico->gruposervico->areaservico_id;
                        $tarefa->automatica = 1;
                        $tarefa->permite_deletar = 0; // não permite deletar
                        $tarefa->empresa_cadastrou = $this->_ID_TRIADE;
                        $tarefa->cor_tarefa = $servico->cor_tarefa;
                        $tarefa->titulo = 'Responsabilidades da Empresa: ' . $servico->nome;
                        $tarefa->descricao = 'Tarefa gerada automaticamente pelo sistema: ' . $servico->descricao;
                        $tarefa->dt_final = $dia_fim_execucao->toDateTimeString();
                        $tarefa->dt_inicio = $dia_inicio_execucao->toDateTimeString();
                        $tarefa->dt_lembrete = $dia_lembrete_execucao->toDateTimeString();

//tipo de tarefa - cmpo obrigatorio
                        $tipotarefa = $this->Tarefas->Tarefatipos->find()->where(['descricao' => 'Responsabilidade do Cliente'])->first();
                        if (empty($tipotarefa)) {
                            $tipotarefa = $this->Tarefas->Tarefatipos->newEntity();
                            $tipotarefa->descricao = 'Responsabilidade do Cliente';
                            $tipotarefa->empresa_id = $this->_ID_TRIADE;
                            $tipotarefa->user_id = 1;
                            $tipotarefa->todos_empresa = 1;
                            $tipotarefa->dt_cadastro = date('Y-m-d H:i:s');
                            $this->Tarefas->Tarefatipos->save($tipotarefa);
                        }
                        $tarefa->tarefatipo_id = $tipotarefa->id;


//prioridade  
                        $prioridade = $this->Tarefas->Tarefaprioridades->find()->orderAsc('ordem')->first();
                        if (empty($prioridade)) {
                            $prioridade = $this->Tarefas->Tarefaprioridades->newEntity();
                            $prioridade->descricao = 'Alta';
                            $prioridade->ordem = 1;
                            $prioridade->dt_cadastro = date('Y-m-d H:i:s');
                            $this->Tarefas->Tarefaprioridades->save($prioridade);
                        }
                        $tarefa->tarefaprioridade_id = $prioridade->id;



                        $user = $this->Tarefas->Users->find()->where(['empresa_id' => $empresa->empresa_id, 'admin_empresa = 1'])->first();
                        if (!empty($user)) {
                            $usertarefa = $this->Tarefas->Tarefausuarios->newEntity();
                            $usertarefa->user_id = $user->id;
                            $tarefa->tarefausuarios = array($usertarefa);
                        }

                        $this->Tarefas->save($tarefa);
                        $tarefas_ids[] = $tarefa->id;
                    }
                }
            }
        }



        if (!empty($tarefas_ids)) {
            $tarefas = $this->Tarefas->find()->contain(['Tarefausuarios.Users', 'Empresas', 'Tarefaprioridades', 'Tipodeveres', 'Areaservicos'])->where(['Tarefas.id IN' => $tarefas_ids]);
        }
        $this->viewBuilder()->layout('frame');
        $this->set(compact('tarefas'));
        $this->set('_serialize', ['tarefas']);
    }

    public function disparaEmailTarefasAdmin($tarefas) {
        try {
            $email = new Email('default');
            $email->viewVars(['tarefas' => $tarefas, 'root' => WWW_ROOT]);
            $email->subject('Rotina de Tarefas Automáticas')
                    ->template('default', 'tarefasautomaticas')
                    ->emailFormat('html')
                    ->to($this->_EMAIL_ADMIN)
                    ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

    public function disparaEmailResponsavelTarefa($tarefas_ids) {
        try {
            $this->loadModel('Users');
            $users = $this->Users->find()->distinct()->innerJoinWith('Tarefausuarios.Tarefas')->contain(['Tarefausuarios.Tarefas.Empresas', 'Tarefausuarios.Tarefas.Tiposervicos', 'Tarefausuarios.Tarefas.Areaservicos'])->where(['Tarefas.id IN' => $tarefas_ids]);
            if ($users) {
                foreach ($users as $user) {
                    $email = new Email('default');
                    $email->viewVars(['tarefas' => $user->tarefausuarios, 'root' => WWW_ROOT, 'nomeresponsavel' => $user->nome]);
                    $email->subject('Nova Tarefa Atribuída')
                            ->template('default', 'novatarefaatribuida')
                            ->emailFormat('html')
                            ->to($user->email)
                            ->send();
                }
            }
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

    public function disparaEmailSemanalResponsavel() { // o job irá disparar esse email todo domingo
        try {
            $this->loadModel('Users');
            $users = $this->Users->find()->distinct()->contain(['Tarefausuarios.Tarefas' => array('conditions' => ['DATEDIFF(Tarefas.dt_final,CURDATE()) <= 7']), 'Tarefausuarios.Tarefas.Empresas', 'Tarefausuarios.Tarefas.Tiposervicos', 'Tarefausuarios.Tarefas.Areaservicos'])->where(['Users.empresa_id' => $this->_ID_TRIADE]);
            if ($users->count() > 0) {
                foreach ($users as $user) {
                    if ($user->notificacao_semanal_tarefa && $user->tarefausuarios) {
                        echo $user->nome;
                        $email = new Email('default');
                        $email->viewVars(['tarefas' => $user->tarefausuarios, 'root' => WWW_ROOT, 'nomeresponsavel' => $user->nome]);
                        $email->subject('Tarefas Programadas pra Semana')
                                ->template('default', 'lembretesemanaltarefas')
                                ->emailFormat('html')
                                ->to($user->email)
                                ->send();
                    }
                }
            }
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
        die;
    }

    public function disparaEmailDiarioResponsavel() { // o job irá disparar esse email todo dia
        try {
            $this->loadModel('Users');
            $users = $this->Users->find()->distinct()->contain(['Tarefausuarios.Tarefas' => array('conditions' => ['CURDATE() <=  Tarefas.dt_final', 'CURDATE() >=  Tarefas.dt_inicio', 'Tarefas.dt_concluida is null']), 'Tarefausuarios.Tarefas.Empresas', 'Tarefausuarios.Tarefas.Tiposervicos', 'Tarefausuarios.Tarefas.Areaservicos'])->where(['Users.empresa_id' => $this->_ID_TRIADE]);
            if ($users->count() > 0) {
                foreach ($users as $user) {
                    if ($user->notificacao_diaria_tarefa && $user->tarefausuarios) {
                        echo $user->nome;
                        $email = new Email('default');
                        $email->viewVars(['tarefas' => $user->tarefausuarios, 'root' => WWW_ROOT, 'nomeresponsavel' => $user->nome]);
                        $email->subject('Tarefas planejadas para hoje*')
                                ->template('default', 'lembretetarefasdiarias')
                                ->emailFormat('html')
                                ->to($user->email)
                                ->send();
                    }
                }
            }
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
        die;
    }

    public function disparaEmailPrazoFinalResponsavel() { // o job irá disparar esse email sempre que tiver tarefa pro dia final
        try {
            $this->loadModel('Users');
            $users = $this->Users->find()->distinct()->contain(['Tarefausuarios.Tarefas' => array('conditions' => ['DATEDIFF(Tarefas.dt_final,CURDATE()) = 0', 'Tarefas.dt_concluida is null']), 'Tarefausuarios.Tarefas.Empresas', 'Tarefausuarios.Tarefas.Tiposervicos', 'Tarefausuarios.Tarefas.Areaservicos'])->where(['Users.empresa_id' => $this->_ID_TRIADE]);
            if ($users->count() > 0) {
                foreach ($users as $user) {
                    if ($user->notificacao_prazo_tarefa && $user->tarefausuarios) {
                        echo $user->nome;
                        $email = new Email('default');
                        $email->viewVars(['tarefas' => $user->tarefausuarios, 'root' => WWW_ROOT, 'nomeresponsavel' => $user->nome]);
                        $email->subject('Tarefas no Prazo final para entregar')
                                ->template('default', 'lembreteprazofinal')
                                ->emailFormat('html')
                                ->to($user->email)
                                ->send();
                    }
                }
            }
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
        die;
    }

    public function disparaEmailAlertaAtrasoResponsavel() { // o job irá disparar esse email quando os usuarios estiverem com tarefas atrasadas no sistema
        try {
            $this->loadModel('Users');
            $users = $this->Users->find()->distinct()->contain(['Tarefausuarios.Tarefas' => array('conditions' => ['DATEDIFF(CURDATE(),Tarefas.dt_final) >= 1', 'Tarefas.dt_concluida is null']), 'Tarefausuarios.Tarefas.Empresas', 'Tarefausuarios.Tarefas.Tiposervicos', 'Tarefausuarios.Tarefas.Areaservicos'])->where(['Users.empresa_id' => $this->_ID_TRIADE]);
            if ($users->count() > 0) {
                foreach ($users as $user) {
                    if ($user->notificacao_atraso_tarefa && $user->tarefausuarios) {
                        echo $user->nome;
                        $email = new Email('default');
                        $email->viewVars(['tarefas' => $user->tarefausuarios, 'root' => WWW_ROOT, 'nomeresponsavel' => $user->nome]);
                        $email->subject('Minhas Tarefas Atrasadas')
                                ->template('default', 'lembretetarefasatrasadas')
                                ->emailFormat('html')
                                ->to($user->email)
                                ->send();
                    }
                }
            }
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
        die;
    }

    public function disparaEmailAlertaAtrasoSupervisores() { // o job irá disparar esse email quando os usuarios estiverem com tarefas atrasadas no sistema
        try {
            $this->loadModel('Users');
            $users = $this->Users->find()->distinct()->contain(['Tarefausuarios.Tarefas' => array('conditions' => ['DATEDIFF(CURDATE(),Tarefas.dt_final) >= 1', 'Tarefas.dt_concluida is null']), 'Tarefausuarios.Tarefas.Empresas', 'Tarefausuarios.Tarefas.Tiposervicos', 'Tarefausuarios.Tarefas.Areaservicos'])->where(['Users.empresa_id' => $this->_ID_TRIADE, 'Users.tipousuario_id != ' . $this->_ADMIN]);
            if ($users->count() > 0) {
                $email = new Email('default');
                $email->viewVars(['users' => $users]);
                $email->subject('Tarefas Atrasadas dos Funcionários')
                        ->template('default', 'notificacaotarefasatrasadasfuncionarios')
                        ->emailFormat('html')
                        ->to($this->_EMAIL_ADMIN)
                        ->send();
            }
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
        die;
    }

    /*
     * Notifica o cliente por email quando essa tarefa for concluida
     */

    public function disparaEmailClienteTarefaConcluida($tarefa, $user) {
        try {
            $competencia = date('m/Y');
            $email = new Email('default');
            $email->viewVars(['tarefa' => $tarefa, 'root' => WWW_ROOT, 'nome' => $user->nome, 'competencia' => $competencia]);
            $email->subject('A Tríade concluiu um serviço. Competencia: ' . $competencia)
                    ->template('default', 'notificarclientetarefaconcluida')
                    ->emailFormat('html')
                    ->to($user->email)
                    ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

    /*
     * Notifica o responsavel pela tarefa por email quando essa tarefa for reaberta
     */

    public function disparaEmailTarefaReabertaResponsavel($tarefa) { // o job irá disparar esse email quando os usuarios estiverem com tarefas atrasadas no sistema
        try {
            $this->loadModel('Users');
            $user = $this->Users->find()->where(['id' => $tarefa->tarefausuarios[0]->user_id])->first();
            $dt_reaberta = explode(' ', $tarefa->dt_reaberta);
            $tarefa->dt_reaberta = $this->convertDateENtoBR($dt_reaberta[0]);

            $email = new Email('default');
            $email->viewVars(['tarefa' => $tarefa, 'root' => WWW_ROOT, 'nomeresponsavel' => $user->nome]);
            $email->subject('Tarefa Reaberta')
                    ->template('default', 'notificacaoreaberturatarefa')
                    ->emailFormat('html')
                    ->to($user->email)
                    ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

    /*
     * Notifica o cliente por email quando essa tarefa for concluida e avisa sobre a data de vencimento do documento
     */

    public function disparaEmailClienteNovoDocVencimento($tarefa, $user) {
        try {
            $competencia = date('m/Y');
            $tarefa->dt_vencimento = $this->convertDateENtoBR($tarefa->dt_vencimento);
            $email = new Email('default');
            $email->viewVars(['tarefa' => $tarefa, 'root' => WWW_ROOT, 'nome' => $user->nome, 'competencia' => $competencia]);
            $email->subject('A Tríade concluiu um serviço. Competencia: ' . $competencia)
                    ->template('default', 'notificarclientedtvencimento')
                    ->emailFormat('html')
                    ->to($user->email)
                    ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

    public function geraLembreteVencimento($tarefa, $user) {
        $this->loadModel('Cronlembretes');
        $lembrete = $this->Cronlembretes->newEntity();
        $lembrete->dt_lembrete = $this->convertDateBRtoEN($tarefa->dt_vencimento);
        $lembrete->dt_vencimento = $lembrete->dt_lembrete;
        $lembrete->user_id = $user->id;
        $lembrete->empresa_id = $user->empresa_id;
        $lembrete->titulo = 'Documento a vencer em ' . $tarefa->dt_vencimento;
        $lembrete->descricao = $tarefa->tiposervico->lembrete_vencimento . '<br><br>Anotações: ' . $tarefa->anotacao_cliente . '<br>';

        $docs = array();
        if ($tarefa->tarefadocumentos) {
            foreach ($tarefa->tarefadocumentos as $documento) {
                $docs[] = $documento->documento_id;
            }
        }
        $lembrete->documentos = json_encode($docs);
        $this->Cronlembretes->save($lembrete);
    }

    public function disparaEmailLembretesProgramadosHoje() {
        try {
            $this->loadModel('Cronlembretes');
            $lembretes = $this->Cronlembretes->find()->contain(['Users'])->where(['DATEDIFF(Cronlembretes.dt_lembrete,CURDATE()) = 0']);
            foreach ($lembretes as $lembrete) {
                $email = new Email('default');
                $email->viewVars(['lembrete' => $lembrete, 'root' => WWW_ROOT, 'nome' => $lembrete->user->nome]);
                $email->subject('Lembrete: ' . $lembrete->titulo)
                        ->template('default', 'lembreteprogramado')
                        ->emailFormat('html')
                        ->to($lembrete->user->email)
                        ->send();
            }
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
        die;
    }

}
