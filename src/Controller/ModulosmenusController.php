<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Modulosmenus Controller
 *
 * @property \App\Model\Table\ModulosmenusTable $Modulosmenus
 */
class ModulosmenusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Modulos', 'Menus']
        ];
        $modulosmenus = $this->paginate($this->Modulosmenus);

        $this->set(compact('modulosmenus'));
        $this->set('_serialize', ['modulosmenus']);
    }

    /**
     * View method
     *
     * @param string|null $id Modulosmenu id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $modulosmenu = $this->Modulosmenus->get($id, [
            'contain' => ['Modulos', 'Menus']
        ]);

        $this->set('modulosmenu', $modulosmenu);
        $this->set('_serialize', ['modulosmenu']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $modulosmenu = $this->Modulosmenus->newEntity();
        if ($this->request->is('post')) {
            $modulosmenu = $this->Modulosmenus->patchEntity($modulosmenu, $this->request->data);
            $modulosmenu->dt_cadastro =  date('Y-m-d H:i:s');
            $modulosmenu->user_id =  $this->Auth->user('id');
            $modulosmenu->empresa_id =  $this->Auth->user('empresa_id');
            if ($this->Modulosmenus->save($modulosmenu)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $modulos = $this->Modulosmenus->Modulos->find('list', ['limit' => 200]);
        $menus = $this->Modulosmenus->Menus->find('list', ['limit' => 200]);
        $this->set(compact('modulosmenu', 'modulos', 'menus'));
        $this->set('_serialize', ['modulosmenu']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Modulosmenu id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $modulosmenu = $this->Modulosmenus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $modulosmenu = $this->Modulosmenus->patchEntity($modulosmenu, $this->request->data);
            if ($this->Modulosmenus->save($modulosmenu)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $modulos = $this->Modulosmenus->Modulos->find('list', ['limit' => 200]);
        $menus = $this->Modulosmenus->Menus->find('list', ['limit' => 200]);
        $this->set(compact('modulosmenu', 'modulos', 'menus'));
        $this->set('_serialize', ['modulosmenu']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Modulosmenu id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $modulosmenu = $this->Modulosmenus->get($id);
        if ($this->Modulosmenus->delete($modulosmenu)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
