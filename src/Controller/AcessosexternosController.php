<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Acessosexternos Controller
 *
 * @property \App\Model\Table\AcessosexternosTable $Acessosexternos
 */
class AcessosexternosController extends AppController {

    public function isAuthorized($user) {
        if (!$this->Auth->user('user_triade') && !$this->request->is('ajax')) {
            if (!in_array($this->request->param('action'), ['view', 'index'])) {
                return false;
            }
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Empresas', 'Sistemasexternos']
        ];

        $query = $this->Acessosexternos->find()->innerJoinWith('Empresas')->innerJoinWith('Sistemasexternos');

        if ($this->request->is('post') && !empty($this->request->data('termo-pesquisa'))) {
            $query->orWhere(["Empresas.razao like '%" . $this->request->data('termo-pesquisa') . "%'"]);
            $query->orWhere(["Sistemasexternos.descricao like '%" . $this->request->data('termo-pesquisa') . "%'"]);
        }


        if ($this->Auth->user('admin') || $this->Auth->user('user_triade')) {
            $acessosexternos = $this->paginate($query->orderAsc('Empresas.razao'));
        } else if ($this->Auth->user('admin_empresa')) {
            $acessosexternos = $this->paginate($query->where(['Acessosexternos.empresa_id' => $this->Auth->user('empresa_id'), 'Acessosexternos.exibecliente = 1 '])->orderAsc('Empresas.razao'));
        }

        $this->set(compact('acessosexternos'));
        $this->set('_serialize', ['acessosexternos']);
    }

    /**
     * View method
     *
     * @param string|null $id Acessosexterno id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $acessosexterno = $this->Acessosexternos->get($id, [
            'contain' => ['Empresas', 'Sistemasexternos']
        ]);


        if (($acessosexterno->empresa_id != $this->Auth->user('empresa_id') && !$this->Auth->user('admin')) || ($acessosexterno->empresa_id == $this->Auth->user('empresa_id') && (!$this->Auth->user('admin_empresa') || !$acessosexterno->exibecliente))) {
            $this->Flash->error(__('Você não tem permissão de visualizar esse registro'));
            return $this->redirect($this->referer());
        }

        $this->set('acessosexterno', $acessosexterno);
        $this->set('_serialize', ['acessosexterno']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($set_empresa_id = null) {
        $acessosexterno = $this->Acessosexternos->newEntity();
        if ($this->request->is('post')) {
            $acessosexterno = $this->Acessosexternos->patchEntity($acessosexterno, $this->request->data);
            $acessosexterno->dt_cadastro = date('Y-m-d H:i:s');
            $acessosexterno->user_id = $this->Auth->user('id');
            $acessosexterno->empresa_id = $acessosexterno->empresa_id ? $acessosexterno->empresa_id : $this->request->data('set_empresa_id');

            if ($this->Acessosexternos->save($acessosexterno)) {
                $this->Flash->success(__('Senha de acesso salva com sucesso.'));
                if ($acessosexterno->empresa_id) {
                    return $this->redirect(['controller' => 'Empresas', 'action' => 'edit', $acessosexterno->empresa_id]);
                }
//                return $this->redirect(['action' => 'add']);
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $empresas = $this->Acessosexternos->Empresas->find('list', ['limit' => 200]);
        if ($set_empresa_id) {
            $empresas->where(['id' => $set_empresa_id])->first();
        }
        $sistemasexternos = $this->Acessosexternos->Sistemasexternos->find('list', ['limit' => 200]);
        $this->set(compact('acessosexterno', 'empresas', 'sistemasexternos', 'set_empresa_id'));
        $this->set('_serialize', ['acessosexterno']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Acessosexterno id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {


        $acessosexterno = $this->Acessosexternos->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $acessosexterno = $this->Acessosexternos->patchEntity($acessosexterno, $this->request->data);
            if ($this->Acessosexternos->save($acessosexterno)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $empresas = $this->Acessosexternos->Empresas->find('list', ['limit' => 200]);
        $sistemasexternos = $this->Acessosexternos->Sistemasexternos->find('list', ['limit' => 200]);
        $this->set(compact('acessosexterno', 'empresas', 'sistemasexternos'));
        $this->set('_serialize', ['acessosexterno']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Acessosexterno id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {

        $this->request->allowMethod(['post', 'delete']);
        $acessosexterno = $this->Acessosexternos->get($id);
        if ($this->Acessosexternos->delete($acessosexterno)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

}
