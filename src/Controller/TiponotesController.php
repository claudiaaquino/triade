<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Tiponotes Controller
 *
 * @property \App\Model\Table\TiponotesTable $Tiponotes
 */
class TiponotesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        if ($this->request->is('post') && !empty($this->request->data('termo-pesquisa'))) {
            $query = $this->Tiponotes->find()->where(["Tiponotes.descricao like '%" . $this->request->data('termo-pesquisa') . "%'"]);
        } else {
            $query = $this->Tiponotes->find();
        }
        $tiponotes = $this->paginate($query->orderAsc('Tiponotes.descricao'));

        $this->set(compact('tiponotes'));
        $this->set('_serialize', ['tiponotes']);
    }

    /**
     * View method
     *
     * @param string|null $id Tiponote id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tiponote = $this->Tiponotes->get($id, [
            'contain' => ['Anotacaoassuntos.Assuntos']
        ]);

        $this->set('tiponote', $tiponote);
        $this->set('_serialize', ['tiponote']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $tiponote = $this->Tiponotes->newEntity();
        if ($this->request->is('post')) {
            $tiponote = $this->Tiponotes->patchEntity($tiponote, $this->request->data);
            $tiponote->dt_cadastro = date('Y-m-d H:i:s');
            $tiponote->user_id = $this->Auth->user('id');
            $tiponote->empresa_id = $this->Auth->user('empresa_id');
            if ($this->Tiponotes->save($tiponote)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tiponote'));
        $this->set('_serialize', ['tiponote']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tiponote id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tiponote = $this->Tiponotes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tiponote = $this->Tiponotes->patchEntity($tiponote, $this->request->data);
            if ($this->Tiponotes->save($tiponote)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tiponote'));
        $this->set('_serialize', ['tiponote']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tiponote id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $tiponote = $this->Tiponotes->get($id);
        if ($this->Tiponotes->delete($tiponote)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
