<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Leisparagrafos Controller
 *
 * @property \App\Model\Table\LeisparagrafosTable $Leisparagrafos
 */
class LeisparagrafosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Leisnormas', 'Leisartigos', 'Users']
        ];
        $leisparagrafos = $this->paginate($this->Leisparagrafos);

        $this->set(compact('leisparagrafos'));
        $this->set('_serialize', ['leisparagrafos']);
    }

    /**
     * View method
     *
     * @param string|null $id Leisparagrafo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $leisparagrafo = $this->Leisparagrafos->get($id, [
            'contain' => ['Leisnormas', 'Leisartigos', 'Users']
        ]);

        $this->set('leisparagrafo', $leisparagrafo);
        $this->set('_serialize', ['leisparagrafo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $leisparagrafo = $this->Leisparagrafos->newEntity();
        if ($this->request->is('post')) {
            $leisparagrafo = $this->Leisparagrafos->patchEntity($leisparagrafo, $this->request->data);
            $leisparagrafo->dt_cadastro =  date('Y-m-d H:i:s');
            $leisparagrafo->user_id =  $this->Auth->user('id');
            $leisparagrafo->empresa_id =  $this->Auth->user('empresa_id');
            if ($this->Leisparagrafos->save($leisparagrafo)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $leisnormas = $this->Leisparagrafos->Leisnormas->find('list', ['limit' => 200]);
        $leisartigos = $this->Leisparagrafos->Leisartigos->find('list', ['limit' => 200]);
        $users = $this->Leisparagrafos->Users->find('list', ['limit' => 200]);
        $this->set(compact('leisparagrafo', 'leisnormas', 'leisartigos', 'users'));
        $this->set('_serialize', ['leisparagrafo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Leisparagrafo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $leisparagrafo = $this->Leisparagrafos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $leisparagrafo = $this->Leisparagrafos->patchEntity($leisparagrafo, $this->request->data);
            if ($this->Leisparagrafos->save($leisparagrafo)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $leisnormas = $this->Leisparagrafos->Leisnormas->find('list', ['limit' => 200]);
        $leisartigos = $this->Leisparagrafos->Leisartigos->find('list', ['limit' => 200]);
        $users = $this->Leisparagrafos->Users->find('list', ['limit' => 200]);
        $this->set(compact('leisparagrafo', 'leisnormas', 'leisartigos', 'users'));
        $this->set('_serialize', ['leisparagrafo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Leisparagrafo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $leisparagrafo = $this->Leisparagrafos->get($id);
        if ($this->Leisparagrafos->delete($leisparagrafo)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
