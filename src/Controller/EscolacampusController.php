<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Escolacampus Controller
 *
 * @property \App\Model\Table\EscolacampusTable $Escolacampus
 */
class EscolacampusController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        
        $this->paginate = [
            'contain' => ['Escolas']
        ];
        
        $escolacampus = $this->paginate($this->Escolacampus);

        $this->set(compact('escolacampus'));
        $this->set('_serialize', ['escolacampus']);
    }

    /**
     * View method
     *
     * @param string|null $id Escolacampus id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        
        $escolacampus = $this->Escolacampus->get($id, [
            'contain' => ['Escolas', 'Funcionarioescolaridades']
        ]);

        $this->set('escolacampus', $escolacampus);
        $this->set('_serialize', ['escolacampus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        
        $escolacampus = $this->Escolacampus->newEntity();
        if ($this->request->is('post')) {
            $escolacampus = $this->Escolacampus->patchEntity($escolacampus, $this->request->data);
            if ($this->Escolacampus->save($escolacampus)) {
                $this->Flash->success(__('Campus cadastrado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao cadastrar. Por favor, tente novamente.'));
            }
        }
        
        $escolas = $this->Escolacampus->Escolas->find('list', ['limit' => 200]);
        $this->set(compact('escolacampus', 'escolas'));
        $this->set('_serialize', ['escolacampus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Escolacampus id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        
        $escolacampus = $this->Escolacampus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $escolacampus = $this->Escolacampus->patchEntity($escolacampus, $this->request->data);
            if ($this->Escolacampus->save($escolacampus)) {
                $this->Flash->success(__('Campus cadastrado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        
        $escolas = $this->Escolacampus->Escolas->find('list', ['limit' => 200]);
        $this->set(compact('escolacampus', 'escolas'));
        $this->set('_serialize', ['escolacampus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Escolacampus id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        
        $this->request->allowMethod(['post', 'delete']);
        $escolacampus = $this->Escolacampus->get($id);
        if ($this->Escolacampus->delete($escolacampus)) {
            $this->Flash->success(__('Campus removido com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }
}
