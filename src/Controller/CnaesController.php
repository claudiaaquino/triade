<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Cnaes Controller
 *
 * @property \App\Model\Table\CnaesTable $Cnaes
 */
class CnaesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $cnaes = $this->paginate($this->Cnaes);

        $this->set(compact('cnaes'));
        $this->set('_serialize', ['cnaes']);
    }

    /**
     * Loadajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function ajax() {
        $retorno = $this->Cnaes->find('list');
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * View method
     *
     * @param string|null $id Cnae id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $cnae = $this->Cnaes->get($id, [
            'contain' => ['Empresacnaes.Empresas']
        ]);

        $this->set('cnae', $cnae);
        $this->set('_serialize', ['cnae']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $cnae = $this->Cnaes->newEntity();
        if ($this->request->is('post')) {
            $cnae = $this->Cnaes->patchEntity($cnae, $this->request->data);
            if ($this->Cnaes->save($cnae)) {
                $this->Flash->success(__('CNAE cadastrado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao cadastrar. Por favor, tente novamente.'));
            }
        }
        
        $this->set(compact('cnae'));
        $this->set('_serialize', ['cnae']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cnae id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        
        $cnae = $this->Cnaes->get($id, [
            'contain' => []
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cnae = $this->Cnaes->patchEntity($cnae, $this->request->data);
            if ($this->Cnaes->save($cnae)) {
                $this->Flash->success(__('CNAE atualizado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('cnae'));
        $this->set('_serialize', ['cnae']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cnae id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $cnae = $this->Cnaes->get($id);
        if ($this->Cnaes->delete($cnae)) {
            $this->Flash->success(__('CNAE removido com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

}
