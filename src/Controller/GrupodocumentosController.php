<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Grupodocumentos Controller
 *
 * @property \App\Model\Table\GrupodocumentosTable $Grupodocumentos
 */
class GrupodocumentosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Areaservicos']
        ];

        if ($this->request->is('post') && !empty($this->request->data('termo-pesquisa'))) {
            $query = $this->Grupodocumentos->find()->where(["Grupodocumentos.descricao like '%" . $this->request->data('termo-pesquisa') . "%'"]);
        } else {
            $query = $this->Grupodocumentos->find();
        }
        $grupodocumentos = $this->paginate($query->orderAsc('Grupodocumentos.descricao'));


        $this->set(compact('grupodocumentos'));
        $this->set('_serialize', ['grupodocumentos']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function ajax($areaservico_id = null) {// retorna os grupos, de acordo com o setor passado por parametro, e codifica para o tipo json 
        $this->viewBuilder()->layout('ajax');
        $retorno = $this->Grupodocumentos->find('list')->where(['areaservico_id' => $areaservico_id])->order(['descricao' => 'ASC']);

        $tpview = 'list';
        $this->set(compact('retorno', 'tpview'));
        $this->set('_serialize', ['retorno', 'tpview']);
        $this->render('ajax');
    }

    /**
     * View method
     *
     * @param string|null $id Grupodocumento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $grupodocumento = $this->Grupodocumentos->get($id, [
            'contain' => ['Areaservicos', 'Tipodocumentos']
        ]);

        $this->set('grupodocumento', $grupodocumento);
        $this->set('_serialize', ['grupodocumento']);
    }

    /**
     * Loadajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function loadajax($mod) {
        $retorno = $this->Grupodocumentos->find()
                ->where(['areaservico_id' => $this->request->query('areaservico_id')]);

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $grupodocumento = $this->Grupodocumentos->newEntity();
        if ($this->request->is('post')) {
            $grupodocumento = $this->Grupodocumentos->patchEntity($grupodocumento, $this->request->data);
            $grupodocumento->dt_cadastro = date('Y-m-d H:i:s');
            $grupodocumento->user_id = $this->Auth->user('id');
            if ($this->Grupodocumentos->save($grupodocumento)) {
                $this->Flash->success(__('Grupo de documento salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Por favor, tente novamente.'));
            }
        }
        $areaservicos = $this->Grupodocumentos->Areaservicos->find('list', ['limit' => 200]);
        $this->set(compact('grupodocumento', 'areaservicos'));
        $this->set('_serialize', ['grupodocumento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grupodocumento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $grupodocumento = $this->Grupodocumentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $grupodocumento = $this->Grupodocumentos->patchEntity($grupodocumento, $this->request->data);
            if ($this->Grupodocumentos->save($grupodocumento)) {
                $this->Flash->success(__('Grupo de documento atualizado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        $areaservicos = $this->Grupodocumentos->Areaservicos->find('list', ['limit' => 200]);
        $this->set(compact('grupodocumento', 'areaservicos'));
        $this->set('_serialize', ['grupodocumento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Grupodocumento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $grupodocumento = $this->Grupodocumentos->get($id);
        if ($this->Grupodocumentos->delete($grupodocumento)) {
            $this->Flash->success(__('Grupo de documento removido com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

}
