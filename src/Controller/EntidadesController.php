<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Entidades Controller
 *
 * @property \App\Model\Table\EntidadesTable $Entidades
 */
class EntidadesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => []
        ];
        $entidades = $this->paginate($this->Entidades);

        $this->set(compact('entidades'));
        $this->set('_serialize', ['entidades']);
    }

    /**
     * View method
     *
     * @param string|null $id Entidade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $entidade = $this->Entidades->get($id, [
            'contain' => ['Leisnormas.Orgaos']
        ]);

        $this->set('entidade', $entidade);
        $this->set('_serialize', ['entidade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $entidade = $this->Entidades->newEntity();
        if ($this->request->is('post')) {
            $entidade = $this->Entidades->patchEntity($entidade, $this->request->data);
            $entidade->dt_cadastro = date('Y-m-d H:i:s');
            $entidade->user_id = $this->Auth->user('id');
            $entidade->empresa_id = $this->Auth->user('empresa_id');
            if ($this->Entidades->save($entidade)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        
        $this->set(compact('entidade'));
        $this->set('_serialize', ['entidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Entidade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $entidade = $this->Entidades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $entidade = $this->Entidades->patchEntity($entidade, $this->request->data);
            if ($this->Entidades->save($entidade)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        
        $this->set(compact('entidade'));
        $this->set('_serialize', ['entidade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Entidade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $entidade = $this->Entidades->get($id);
        if ($this->Entidades->delete($entidade)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
