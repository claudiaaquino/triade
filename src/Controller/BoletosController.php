<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Boletos Controller
 *
 * @property \App\Model\Table\BoletosTable $Boletos
 */
class BoletosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Contratos', 'Contabancarias']
        ];
        $boletos = $this->paginate($this->Boletos);

        $this->set(compact('boletos'));
        $this->set('_serialize', ['boletos']);
    }

    /**
     * View method
     *
     * @param string|null $id Boleto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $boleto = $this->Boletos->get($id, [
            'contain' => ['Contratos', 'Contabancarias']
        ]);

        $this->set('boleto', $boleto);
        $this->set('_serialize', ['boleto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $boleto = $this->Boletos->newEntity();
        if ($this->request->is('post')) {
            $boleto = $this->Boletos->patchEntity($boleto, $this->request->data);
            $boleto->dt_cadastro =  date('Y-m-d H:i:s');
            $boleto->user_id =  $this->Auth->user('id');
            if ($this->Boletos->save($boleto)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $contratos = $this->Boletos->Contratos->find('list', ['limit' => 200]);
        $contabancarias = $this->Boletos->Contabancarias->find('list', ['limit' => 200]);
        $this->set(compact('boleto', 'contratos', 'contabancarias'));
        $this->set('_serialize', ['boleto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Boleto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $boleto = $this->Boletos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $boleto = $this->Boletos->patchEntity($boleto, $this->request->data);
            if ($this->Boletos->save($boleto)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $contratos = $this->Boletos->Contratos->find('list', ['limit' => 200]);
        $contabancarias = $this->Boletos->Contabancarias->find('list', ['limit' => 200]);
        $this->set(compact('boleto', 'contratos', 'contabancarias'));
        $this->set('_serialize', ['boleto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Boleto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $boleto = $this->Boletos->get($id);
        if ($this->Boletos->delete($boleto)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
