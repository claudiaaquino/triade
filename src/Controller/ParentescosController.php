<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Parentescos Controller
 *
 * @property \App\Model\Table\ParentescosTable $Parentescos
 */
class ParentescosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $parentescos = $this->paginate($this->Parentescos);

        $this->set(compact('parentescos'));
        $this->set('_serialize', ['parentescos']);
    }

    /**
     * View method
     *
     * @param string|null $id Parentesco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $parentesco = $this->Parentescos->get($id, [
            'contain' => ['Funcionariodependentes']
        ]);

        $this->set('parentesco', $parentesco);
        $this->set('_serialize', ['parentesco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $parentesco = $this->Parentescos->newEntity();
        if ($this->request->is('post')) {
            $parentesco = $this->Parentescos->patchEntity($parentesco, $this->request->data);
            $parentesco->dt_cadastro =  date('Y-m-d H:i:s');
            $parentesco->user_id =  $this->Auth->user('id');
            if ($this->Parentescos->save($parentesco)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('parentesco'));
        $this->set('_serialize', ['parentesco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Parentesco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $parentesco = $this->Parentescos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $parentesco = $this->Parentescos->patchEntity($parentesco, $this->request->data);
            if ($this->Parentescos->save($parentesco)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('parentesco'));
        $this->set('_serialize', ['parentesco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Parentesco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $parentesco = $this->Parentescos->get($id);
        if ($this->Parentescos->delete($parentesco)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
