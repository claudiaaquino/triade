<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Cnaegroups Controller
 *
 * @property \App\Model\Table\CnaegroupsTable $Cnaegroups
 */
class CnaegroupsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Cnaedivisions']
        ];
        $cnaegroups = $this->paginate($this->Cnaegroups);

        $this->set(compact('cnaegroups'));
        $this->set('_serialize', ['cnaegroups']);
    }

    /**
     * Listajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function listajax($division_id) {
        $retorno = $this->Cnaegroups->find('ajaxList')->where(['Cnaegroups.cnaedivision_id' => $division_id]);
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * View method
     *
     * @param string|null $id Cnaegroup id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $cnaegroup = $this->Cnaegroups->get($id, [
            'contain' => ['Cnaedivisions', 'Cnaeclasses']
        ]);

        $this->set('cnaegroup', $cnaegroup);
        $this->set('_serialize', ['cnaegroup']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $cnaegroup = $this->Cnaegroups->newEntity();
        if ($this->request->is('post')) {
            $cnaegroup = $this->Cnaegroups->patchEntity($cnaegroup, $this->request->data);
            if ($this->Cnaegroups->save($cnaegroup)) {
                $this->Flash->success(__('Grupo cadastrado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao cadastrar. Por favor, tente novamente.'));
            }
        }
        
        $cnaedivisions = $this->Cnaegroups->Cnaedivisions->find('list', ['limit' => 200]);
        $this->set(compact('cnaegroup', 'cnaedivisions'));
        $this->set('_serialize', ['cnaegroup']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cnaegroup id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $cnaegroup = $this->Cnaegroups->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cnaegroup = $this->Cnaegroups->patchEntity($cnaegroup, $this->request->data);
            if ($this->Cnaegroups->save($cnaegroup)) {
                $this->Flash->success(__('Grupo atualizado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        $cnaedivisions = $this->Cnaegroups->Cnaedivisions->find('list', ['limit' => 200]);
        $this->set(compact('cnaegroup', 'cnaedivisions'));
        $this->set('_serialize', ['cnaegroup']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cnaegroup id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $cnaegroup = $this->Cnaegroups->get($id);
        if ($this->Cnaegroups->delete($cnaegroup)) {
            $this->Flash->success(__('Grupo removido com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

}
