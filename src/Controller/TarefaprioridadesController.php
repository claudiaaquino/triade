<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tarefaprioridades Controller
 *
 * @property \App\Model\Table\TarefaprioridadesTable $Tarefaprioridades
 */
class TarefaprioridadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $tarefaprioridades = $this->paginate($this->Tarefaprioridades);

        $this->set(compact('tarefaprioridades'));
        $this->set('_serialize', ['tarefaprioridades']);
    }

    /**
     * View method
     *
     * @param string|null $id Tarefaprioridade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tarefaprioridade = $this->Tarefaprioridades->get($id, [
            'contain' => ['Tarefas']
        ]);

        $this->set('tarefaprioridade', $tarefaprioridade);
        $this->set('_serialize', ['tarefaprioridade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tarefaprioridade = $this->Tarefaprioridades->newEntity();
        if ($this->request->is('post')) {
            $tarefaprioridade = $this->Tarefaprioridades->patchEntity($tarefaprioridade, $this->request->data);
            $tarefaprioridade->dt_cadastro =  date('Y-m-d H:i:s');
            $tarefaprioridade->user_id =  $this->Auth->user('id');
            if ($this->Tarefaprioridades->save($tarefaprioridade)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tarefaprioridade'));
        $this->set('_serialize', ['tarefaprioridade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tarefaprioridade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tarefaprioridade = $this->Tarefaprioridades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tarefaprioridade = $this->Tarefaprioridades->patchEntity($tarefaprioridade, $this->request->data);
            if ($this->Tarefaprioridades->save($tarefaprioridade)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tarefaprioridade'));
        $this->set('_serialize', ['tarefaprioridade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tarefaprioridade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tarefaprioridade = $this->Tarefaprioridades->get($id);
        if ($this->Tarefaprioridades->delete($tarefaprioridade)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
