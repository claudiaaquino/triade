<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Leisartigos Controller
 *
 * @property \App\Model\Table\LeisartigosTable $Leisartigos
 */
class LeisartigosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Leisnormas', 'Leispartes', 'Leislivros', 'Leistitulos', 'Leiscapitulos', 'Leissections', 'Leissubsections', 'Users']
        ];
        $leisartigos = $this->paginate($this->Leisartigos);

        $this->set(compact('leisartigos'));
        $this->set('_serialize', ['leisartigos']);
    }

    /**
     * View method
     *
     * @param string|null $id Leisartigo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $leisartigo = $this->Leisartigos->get($id, [
            'contain' => ['Leisnormas', 'Leispartes', 'Leislivros', 'Leistitulos', 'Leiscapitulos', 'Leissections', 'Leissubsections', 'Users', 'Leisincisos', 'Leisletras', 'Leisparagrafos']
        ]);

        $this->set('leisartigo', $leisartigo);
        $this->set('_serialize', ['leisartigo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $leisartigo = $this->Leisartigos->newEntity();
        if ($this->request->is('post')) {
            $leisartigo = $this->Leisartigos->patchEntity($leisartigo, $this->request->data);
            $leisartigo->dt_cadastro = date('Y-m-d H:i:s');
            $leisartigo->user_id = $this->Auth->user('id');
            $leisartigo->empresa_id = $this->Auth->user('empresa_id');


            if ($this->request->data('leisnorma_id') && !is_numeric($this->request->data('leisnorma_id'))) {
                $new = $this->Leisartigos->Leisnormas->newEntity();
                $new->numero = $this->request->data('leisnorma_id');
                $new->descricao = $this->request->data('leisnorma_id');
                $new->user_id = $this->Auth->user('id');
                $new->dt_cadastro = date('Y-m-d H:i:s');
                if ($this->Leisartigos->Leisnormas->save($new)) {
                    $leisartigo->leisnorma_id = $new->id;
                }
            }

            $paragrafos = array();
            if ($this->request->data('leisparagrafos_id') && is_array($this->request->data('leisparagrafos_id'))) {
                foreach ($this->request->data('leisparagrafos_id') as $paragrafo) {
                    if (is_numeric($paragrafo)) {
                        $paragrafoUpdate = $this->Leisartigos->Leisparagrafos->get($paragrafo);
                        $paragrafoUpdate->leisnorma_id = $leisartigo->leisnorma_id;
                        $paragrafos[] = $paragrafoUpdate;
                    } else {
                        $new = $this->Leisartigos->Leisparagrafos->newEntity();
                        $new->nome = $paragrafo;
                        $new->descricao = $paragrafo;
                        $new->leisnorma_id = $leisartigo->leisnorma_id;
                        $new->user_id = $this->Auth->user('id');
                        $new->dt_cadastro = date('Y-m-d H:i:s');
                        if ($this->Leisartigos->Leisparagrafos->save($new)) {
                            $paragrafos[] = $new;
                        }
                    }
                }
            }
            $leisartigo->leisparagrafos = $paragrafos;


            $incisos = array();
            if ($this->request->data('leisincisos_id') && is_array($this->request->data('leisincisos_id'))) {
                foreach ($this->request->data('leisincisos_id') as $inciso) {
                    if (is_numeric($inciso)) {
                        $incisoUpdate = $this->Leisartigos->Leisincisos->get($inciso);
                        $incisoUpdate->leisnorma_id = $leisartigo->leisnorma_id;
                        $incisos[] = $incisoUpdate;
                    } else {
                        $new = $this->Leisartigos->Leisincisos->newEntity();
                        $new->nome = $inciso;
                        $new->descricao = $inciso;
                        $new->leisnorma_id = $leisartigo->leisnorma_id;
                        $new->user_id = $this->Auth->user('id');
                        $new->dt_cadastro = date('Y-m-d H:i:s');
                        if ($this->Leisartigos->Leisincisos->save($new)) {
                            $incisos[] = $new;
                        }
                    }
                }
            }
            $leisartigo->leisincisos = $incisos;



            $letras = array();
            if ($this->request->data('leisletras_id') && is_array($this->request->data('leisletras_id'))) {
                foreach ($this->request->data('leisletras_id') as $letra) {
                    if (is_numeric($letra)) {
                        $letraUpdate = $this->Leisartigos->Leisletras->get($letra);
                        $letraUpdate->leisnorma_id = $leisartigo->leisnorma_id;
                        $letras[] = $letraUpdate;
                    } else {
                        $new = $this->Leisartigos->Leisletras->newEntity();
                        $new->nome = $letra;
                        $new->descricao = $letra;
                        $new->leisnorma_id = $leisartigo->leisnorma_id;
                        $new->user_id = $this->Auth->user('id');
                        $new->dt_cadastro = date('Y-m-d H:i:s');
                        if ($this->Leisartigos->Leisletras->save($new)) {
                            $letras[] = $new;
                        }
                    }
                }
            }
            $leisartigo->leisletras = $letras;



            if ($this->Leisartigos->save($leisartigo)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $leisnormas = $this->Leisartigos->Leisnormas->find('list');
        $leisparagrafos = $this->Leisartigos->Leisparagrafos->find('list');
        $leisincisos = $this->Leisartigos->Leisincisos->find('list');
        $leisletras = $this->Leisartigos->Leisletras->find('list');
        $this->set(compact('leisartigo', 'leisnormas', 'leisparagrafos', 'leisincisos', 'leisletras'));
        $this->set('_serialize', ['leisartigo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Leisartigo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $leisartigo = $this->Leisartigos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $leisartigo = $this->Leisartigos->patchEntity($leisartigo, $this->request->data);



            if ($this->request->data('leisnorma_id') && !is_numeric($this->request->data('leisnorma_id'))) {
                $new = $this->Leisartigos->Leisnormas->newEntity();
                $new->numero = $this->request->data('leisnorma_id');
                $new->descricao = $this->request->data('leisnorma_id');
                $new->user_id = $this->Auth->user('id');
                $new->dt_cadastro = date('Y-m-d H:i:s');
                if ($this->Leisartigos->Leisnormas->save($new)) {
                    $leisartigo->leisnorma_id = $new->id;
                }
            }

            $paragrafoscadastrados = $this->Leisartigos->Leisparagrafos->find()->where(['leisartigo_id' => $id])->extract('id')->filter()->toArray();
            $paragrafos = array();
            if ($this->request->data('leisparagrafos_id') && is_array($this->request->data('leisparagrafos_id'))) {
                foreach ($this->request->data('leisparagrafos_id') as $paragrafo) {
                    if (!in_array($paragrafo, $paragrafoscadastrados)) {
                        if (is_numeric($paragrafo)) {
                            $paragrafoUpdate = $this->Leisartigos->Leisparagrafos->get($paragrafo);
                            $paragrafoUpdate->leisnorma_id = $leisartigo->leisnorma_id;
                            $paragrafos[] = $paragrafoUpdate;
                        } else {
                            $new = $this->Leisartigos->Leisparagrafos->newEntity();
                            $new->nome = $paragrafo;
                            $new->descricao = $paragrafo;
                            $new->leisnorma_id = $leisartigo->leisnorma_id;
                            $new->user_id = $this->Auth->user('id');
                            $new->dt_cadastro = date('Y-m-d H:i:s');
                            if ($this->Leisartigos->Leisparagrafos->save($new)) {
                                $paragrafos[] = $new;
                            }
                        }
                    }
                }
            }
            $leisartigo->leisparagrafos = $paragrafos;
            $this->Leisartigos->Leisparagrafos->updateAll(['leisartigo_id = null'], ['leisartigo_id' => $id, 'id NOT IN' => $this->request->data('leisparagrafos_id')]);


            $incisoscadastrados = $this->Leisartigos->Leisincisos->find()->where(['leisartigo_id' => $id])->extract('id')->filter()->toArray();
            $incisos = array();
            if ($this->request->data('leisincisos_id') && is_array($this->request->data('leisincisos_id'))) {
                foreach ($this->request->data('leisincisos_id') as $inciso) {
                    if (!in_array($inciso, $incisoscadastrados)) {
                        if (is_numeric($inciso)) {
                            $incisoUpdate = $this->Leisartigos->Leisincisos->get($inciso);
                            $incisoUpdate->leisnorma_id = $leisartigo->leisnorma_id;
                            $incisos[] = $incisoUpdate;
                        } else {
                            $new = $this->Leisartigos->Leisincisos->newEntity();
                            $new->nome = $inciso;
                            $new->descricao = $inciso;
                            $new->leisnorma_id = $leisartigo->leisnorma_id;
                            $new->user_id = $this->Auth->user('id');
                            $new->dt_cadastro = date('Y-m-d H:i:s');
                            if ($this->Leisartigos->Leisincisos->save($new)) {
                                $incisos[] = $new;
                            }
                        }
                    }
                }
            }
            $leisartigo->leisincisos = $incisos;
            $this->Leisartigos->Leisincisos->updateAll(['leisartigo_id = null'], ['leisartigo_id' => $id, 'id NOT IN' => $this->request->data('leisincisos_id')]);


            $letrascadastrados = $this->Leisartigos->Leisletras->find()->where(['leisartigo_id' => $id])->extract('id')->filter()->toArray();
            $letras = array();
            if ($this->request->data('leisletras_id') && is_array($this->request->data('leisletras_id'))) {
                foreach ($this->request->data('leisletras_id') as $letra) {
                    if (!in_array($letra, $letrascadastrados)) {
                        if (is_numeric($letra)) {
                            $letraUpdate = $this->Leisartigos->Leisletras->get($letra);
                            $letraUpdate->leisnorma_id = $leisartigo->leisnorma_id;
                            $letras[] = $letraUpdate;
                        } else {
                            $new = $this->Leisartigos->Leisletras->newEntity();
                            $new->nome = $letra;
                            $new->descricao = $letra;
                            $new->leisnorma_id = $leisartigo->leisnorma_id;
                            $new->user_id = $this->Auth->user('id');
                            $new->dt_cadastro = date('Y-m-d H:i:s');
                            if ($this->Leisartigos->Leisletras->save($new)) {
                                $letras[] = $new;
                            }
                        }
                    }
                }
            }
            $leisartigo->leisletras = $letras;
            $this->Leisartigos->Leisletras->updateAll(['leisartigo_id = null'], ['leisartigo_id' => $id, 'id NOT IN' => $this->request->data('leisletras_id')]);


            if ($this->Leisartigos->save($leisartigo)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $leisnormas = $this->Leisartigos->Leisnormas->find('list');
        $leisparagrafos = $this->Leisartigos->Leisparagrafos->find('list');
        $leisincisos = $this->Leisartigos->Leisincisos->find('list');
        $leisletras = $this->Leisartigos->Leisletras->find('list');

        $selectedparagrafos = array_keys($this->Leisartigos->Leisparagrafos->find('list')->where(['leisartigo_id' => $id])->toArray());
        $selectedincisos = array_keys($this->Leisartigos->Leisincisos->find('list')->where(['leisartigo_id' => $id])->toArray());
        $selectedletras = array_keys($this->Leisartigos->Leisletras->find('list')->where(['leisartigo_id' => $id])->toArray());

        $this->set(compact('leisartigo', 'leisnormas', 'leisparagrafos', 'leisincisos', 'leisletras', 'selectedparagrafos', 'selectedincisos', 'selectedletras'));
        $this->set('_serialize', ['leisartigo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Leisartigo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $leisartigo = $this->Leisartigos->get($id);
        if ($this->Leisartigos->delete($leisartigo)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
