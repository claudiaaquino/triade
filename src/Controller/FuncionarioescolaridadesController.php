<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Funcionarioescolaridades Controller
 *
 * @property \App\Model\Table\FuncionarioescolaridadesTable $Funcionarioescolaridades
 */
class FuncionarioescolaridadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Funcionarios', 'Nivelescolaridades', 'Cursos', 'Escolas', 'Escolacampus', 'Turnos']
        ];
        $funcionarioescolaridades = $this->paginate($this->Funcionarioescolaridades);

        $this->set(compact('funcionarioescolaridades'));
        $this->set('_serialize', ['funcionarioescolaridades']);
    }

    /**
     * View method
     *
     * @param string|null $id Funcionarioescolaridade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $funcionarioescolaridade = $this->Funcionarioescolaridades->get($id, [
            'contain' => ['Funcionarios', 'Nivelescolaridades', 'Cursos', 'Escolas', 'Escolacampus', 'Turnos']
        ]);

        $this->set('funcionarioescolaridade', $funcionarioescolaridade);
        $this->set('_serialize', ['funcionarioescolaridade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $funcionarioescolaridade = $this->Funcionarioescolaridades->newEntity();
        if ($this->request->is('post')) {
            $funcionarioescolaridade = $this->Funcionarioescolaridades->patchEntity($funcionarioescolaridade, $this->request->data);
            $funcionarioescolaridade->dt_cadastro =  date('Y-m-d H:i:s');
            $funcionarioescolaridade->user_id =  $this->Auth->user('id');
            if ($this->Funcionarioescolaridades->save($funcionarioescolaridade)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $funcionarios = $this->Funcionarioescolaridades->Funcionarios->find('list', ['limit' => 200]);
        $nivelescolaridades = $this->Funcionarioescolaridades->Nivelescolaridades->find('list', ['limit' => 200]);
        $cursos = $this->Funcionarioescolaridades->Cursos->find('list', ['limit' => 200]);
        $escolas = $this->Funcionarioescolaridades->Escolas->find('list', ['limit' => 200]);
        $escolacampus = $this->Funcionarioescolaridades->Escolacampus->find('list', ['limit' => 200]);
        $turnos = $this->Funcionarioescolaridades->Turnos->find('list', ['limit' => 200]);
        $this->set(compact('funcionarioescolaridade', 'funcionarios', 'nivelescolaridades', 'cursos', 'escolas', 'escolacampus', 'turnos'));
        $this->set('_serialize', ['funcionarioescolaridade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Funcionarioescolaridade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $funcionarioescolaridade = $this->Funcionarioescolaridades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $funcionarioescolaridade = $this->Funcionarioescolaridades->patchEntity($funcionarioescolaridade, $this->request->data);
            if ($this->Funcionarioescolaridades->save($funcionarioescolaridade)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $funcionarios = $this->Funcionarioescolaridades->Funcionarios->find('list', ['limit' => 200]);
        $nivelescolaridades = $this->Funcionarioescolaridades->Nivelescolaridades->find('list', ['limit' => 200]);
        $cursos = $this->Funcionarioescolaridades->Cursos->find('list', ['limit' => 200]);
        $escolas = $this->Funcionarioescolaridades->Escolas->find('list', ['limit' => 200]);
        $escolacampus = $this->Funcionarioescolaridades->Escolacampus->find('list', ['limit' => 200]);
        $turnos = $this->Funcionarioescolaridades->Turnos->find('list', ['limit' => 200]);
        $this->set(compact('funcionarioescolaridade', 'funcionarios', 'nivelescolaridades', 'cursos', 'escolas', 'escolacampus', 'turnos'));
        $this->set('_serialize', ['funcionarioescolaridade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Funcionarioescolaridade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $funcionarioescolaridade = $this->Funcionarioescolaridades->get($id);
        if ($this->Funcionarioescolaridades->delete($funcionarioescolaridade)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
