<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Atividadesauxiliares Controller
 *
 * @property \App\Model\Table\AtividadesauxiliaresTable $Atividadesauxiliares
 */
class AtividadesauxiliaresController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        
        if ($this->request->is('post') && !empty($this->request->data('termo-pesquisa'))) {
            $query = $this->Atividadesauxiliares->find()->where(["Atividadesauxiliares.descricao like '%" . $this->request->data('termo-pesquisa') . "%'"]);
        } else {
            $query = $this->Atividadesauxiliares->find();
        }

        $atividadesauxiliares = $this->paginate($query->orderAsc('Atividadesauxiliares.descricao'));

        $this->set(compact('atividadesauxiliares'));
        $this->set('_serialize', ['atividadesauxiliares']);
    }

    /**
     * View method
     *
     * @param string|null $id Atividadesauxiliar id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $atividadesauxiliar = $this->Atividadesauxiliares->get($id, [
            'contain' => []
        ]);

        $this->set('atividadesauxiliar', $atividadesauxiliar);
        $this->set('_serialize', ['atividadesauxiliar']);
    }

    /**
     * ajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function ajax() {
        $retorno = $this->Atividadesauxiliares->find('list');
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $atividadesauxiliar = $this->Atividadesauxiliares->newEntity();
        if ($this->request->is('post')) {
            $atividadesauxiliar = $this->Atividadesauxiliares->patchEntity($atividadesauxiliar, $this->request->data);
            if ($this->Atividadesauxiliares->save($atividadesauxiliar)) {
                $this->Flash->success(__('Atividade auxiliar cadastrada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao cadastrar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('atividadesauxiliar'));
        $this->set('_serialize', ['atividadesauxiliar']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Atividadesauxiliar id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $atividadesauxiliar = $this->Atividadesauxiliares->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $atividadesauxiliar = $this->Atividadesauxiliares->patchEntity($atividadesauxiliar, $this->request->data);
            if ($this->Atividadesauxiliares->save($atividadesauxiliar)) {
                $this->Flash->success(__('Atividade auxiliar atualizada com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('atividadesauxiliar'));
        $this->set('_serialize', ['atividadesauxiliar']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atividadesauxiliar id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $atividadesauxiliar = $this->Atividadesauxiliares->get($id);
        if ($this->Atividadesauxiliares->delete($atividadesauxiliar)) {
            $this->Flash->success(__('Ativade auxiliar removida com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

}
