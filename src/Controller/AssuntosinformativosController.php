<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Assuntosinformativos Controller
 *
 * @property \App\Model\Table\AssuntosinformativosTable $Assuntosinformativos
 */
class AssuntosinformativosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Assuntos', 'Tipoinformativos']
        ];
        $assuntosinformativos = $this->paginate($this->Assuntosinformativos);

        $this->set(compact('assuntosinformativos'));
        $this->set('_serialize', ['assuntosinformativos']);
    }

    /**
     * View method
     *
     * @param string|null $id Assuntosinformativo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $assuntosinformativo = $this->Assuntosinformativos->get($id, [
            'contain' => ['Assuntos', 'Tipoinformativos']
        ]);

        $this->set('assuntosinformativo', $assuntosinformativo);
        $this->set('_serialize', ['assuntosinformativo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $assuntosinformativo = $this->Assuntosinformativos->newEntity();
        if ($this->request->is('post')) {
            $assuntosinformativo = $this->Assuntosinformativos->patchEntity($assuntosinformativo, $this->request->data);
            $assuntosinformativo->dt_cadastro =  date('Y-m-d H:i:s');
            $assuntosinformativo->user_id =  $this->Auth->user('id');
            $assuntosinformativo->empresa_id =  $this->Auth->user('empresa_id');
            if ($this->Assuntosinformativos->save($assuntosinformativo)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $assuntos = $this->Assuntosinformativos->Assuntos->find('list', ['limit' => 200]);
        $tipoinformativos = $this->Assuntosinformativos->Tipoinformativos->find('list', ['limit' => 200]);
        $this->set(compact('assuntosinformativo', 'assuntos', 'tipoinformativos'));
        $this->set('_serialize', ['assuntosinformativo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Assuntosinformativo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $assuntosinformativo = $this->Assuntosinformativos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $assuntosinformativo = $this->Assuntosinformativos->patchEntity($assuntosinformativo, $this->request->data);
            if ($this->Assuntosinformativos->save($assuntosinformativo)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $assuntos = $this->Assuntosinformativos->Assuntos->find('list', ['limit' => 200]);
        $tipoinformativos = $this->Assuntosinformativos->Tipoinformativos->find('list', ['limit' => 200]);
        $this->set(compact('assuntosinformativo', 'assuntos', 'tipoinformativos'));
        $this->set('_serialize', ['assuntosinformativo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Assuntosinformativo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $assuntosinformativo = $this->Assuntosinformativos->get($id);
        if ($this->Assuntosinformativos->delete($assuntosinformativo)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
