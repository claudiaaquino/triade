<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Empresaatividades Controller
 *
 * @property \App\Model\Table\EmpresaatividadesTable $Empresaatividades
 */
class EmpresaatividadesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Empresas', 'Atividadesauxiliares']
        ];
        $empresaatividades = $this->paginate($this->Empresaatividades);

        $this->set(compact('empresaatividades'));
        $this->set('_serialize', ['empresaatividades']);
    }

    /**
     * View method
     *
     * @param string|null $id Empresaatividade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $empresaatividade = $this->Empresaatividades->get($id, [
            'contain' => ['Empresas', 'Atividadesauxiliares']
        ]);

        $this->set('empresaatividade', $empresaatividade);
        $this->set('_serialize', ['empresaatividade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $empresaatividade = $this->Empresaatividades->newEntity();
        if ($this->request->is('post')) {
            $empresaatividade = $this->Empresaatividades->patchEntity($empresaatividade, $this->request->data);
            if ($this->Empresaatividades->save($empresaatividade)) {
                $this->Flash->success(__('The empresaatividade has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The empresaatividade could not be saved. Please, try again.'));
            }
        }
        $empresas = $this->Empresaatividades->Empresas->find('list', ['limit' => 200]);
        $atividadesauxiliares = $this->Empresaatividades->Atividadesauxiliares->find('list', ['limit' => 200]);
        $this->set(compact('empresaatividade', 'empresas', 'atividadesauxiliares'));
        $this->set('_serialize', ['empresaatividade']);
    }

    /**
     * Addajax method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addajax() {
        $empresaatividade = $this->Empresaatividades->newEntity();
        $empresaatividade->empresa_id = $this->request->query('empresa_id');
        $empresaatividade->atividadesauxiliar_id = $this->request->query('atividadesauxiliar_id');
        $empresaatividade->dt_cadastro = date('Y-m-d');
        if ($this->Empresaatividades->save($empresaatividade)) {
//                    $this->Flash->success(__('The empresacnae has been saved.'));
            $retorno = true;
        } else {
//                    $this->Flash->error(__('The empresacnae could not be saved. Please, try again.'));
            $retorno = false;
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Loadajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function loadajax() {
        $retorno = $this->Empresaatividades->find()->contain(['Atividadesauxiliares'])
                ->where(['Empresaatividades.empresa_id' => $this->request->query('empresa_id')]);

        $tpview = 'table';
        $this->set(compact('retorno', 'tpview'));
        $this->set('_serialize', ['retorno', 'tpview']);
        $this->render('ajax');
    }

    /**
     * Edit method
     *
     * @param string|null $id Empresaatividade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $empresaatividade = $this->Empresaatividades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $empresaatividade = $this->Empresaatividades->patchEntity($empresaatividade, $this->request->data);
            if ($this->Empresaatividades->save($empresaatividade)) {
                $this->Flash->success(__('The empresaatividade has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The empresaatividade could not be saved. Please, try again.'));
            }
        }
        $empresas = $this->Empresaatividades->Empresas->find('list', ['limit' => 200]);
        $atividadesauxiliares = $this->Empresaatividades->Atividadesauxiliares->find('list', ['limit' => 200]);
        $this->set(compact('empresaatividade', 'empresas', 'atividadesauxiliares'));
        $this->set('_serialize', ['empresaatividade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Empresaatividade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $empresaatividade = $this->Empresaatividades->get($id);
        if ($this->Empresaatividades->delete($empresaatividade)) {
            $this->Flash->success(__('The empresaatividade has been deleted.'));
        } else {
            $this->Flash->error(__('The empresaatividade could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->request->referer());
    }

    /**
     * Deleteajax method
     *
     * @param string|null $id Empresaatividade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deleteajax() {
        $empresaatividade = $this->Empresaatividades->get($this->request->query('id'));
        if ($this->Empresaatividades->delete($empresaatividade)) {
            $retorno = true;
        } else {
            $retorno = false;
        }

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

}
