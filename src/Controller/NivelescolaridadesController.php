<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Nivelescolaridades Controller
 *
 * @property \App\Model\Table\NivelescolaridadesTable $Nivelescolaridades
 */
class NivelescolaridadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $nivelescolaridades = $this->paginate($this->Nivelescolaridades);

        $this->set(compact('nivelescolaridades'));
        $this->set('_serialize', ['nivelescolaridades']);
    }

    /**
     * View method
     *
     * @param string|null $id Nivelescolaridade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $nivelescolaridade = $this->Nivelescolaridades->get($id, [
            'contain' => ['Cursos', 'Funcionarioescolaridades']
        ]);

        $this->set('nivelescolaridade', $nivelescolaridade);
        $this->set('_serialize', ['nivelescolaridade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $nivelescolaridade = $this->Nivelescolaridades->newEntity();
        if ($this->request->is('post')) {
            $nivelescolaridade = $this->Nivelescolaridades->patchEntity($nivelescolaridade, $this->request->data);
            $nivelescolaridade->dt_cadastro =  date('Y-m-d H:i:s');
            $nivelescolaridade->user_id =  $this->Auth->user('id');
            if ($this->Nivelescolaridades->save($nivelescolaridade)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('nivelescolaridade'));
        $this->set('_serialize', ['nivelescolaridade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Nivelescolaridade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $nivelescolaridade = $this->Nivelescolaridades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $nivelescolaridade = $this->Nivelescolaridades->patchEntity($nivelescolaridade, $this->request->data);
            if ($this->Nivelescolaridades->save($nivelescolaridade)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('nivelescolaridade'));
        $this->set('_serialize', ['nivelescolaridade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Nivelescolaridade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $nivelescolaridade = $this->Nivelescolaridades->get($id);
        if ($this->Nivelescolaridades->delete($nivelescolaridade)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
