<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Tipodespesas Controller
 *
 * @property \App\Model\Table\TipodespesasTable $Tipodespesas
 */
class TipodespesasController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Empresas', 'Users']
        ];

        if ($this->Auth->user('admin') || $this->Auth->user('admin_empresa')) {
            $tipodespesas = $this->paginate($this->Tipodespesas->find()->where(['Tipodespesas.empresa_id' => $this->Auth->user('empresa_id')]));
        }

        $this->set(compact('tipodespesas'));
        $this->set('_serialize', ['tipodespesas']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipodespesa id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tipodespesa = $this->Tipodespesas->get($id, [
            'contain' => ['Movimentacaobancarias'=> ['sort' => ['Movimentacaobancarias.dt_cadastro' => 'DESC']], 
                'Movimentacaobancarias.Contabancarias.Bancos','Movimentacaobancarias.Formaspagamentos',
                'Movimentacaobancarias.Fornecedores' ]
        ]);

        if ($tipodespesa->empresa_id != $this->Auth->user('empresa_id')) {
            $this->Flash->error(__('Você não tem permissão de visualizar um registro que não pertence à sua Empresa'));
            return $this->redirect(['action' => 'index']);
        }
        $this->set('tipodespesa', $tipodespesa);
        $this->set('_serialize', ['tipodespesa']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $tipodespesa = $this->Tipodespesas->newEntity();
        if ($this->request->is('post')) {
            $tipodespesa = $this->Tipodespesas->patchEntity($tipodespesa, $this->request->data);
            $tipodespesa->dt_cadastro = date('Y-m-d H:i:s');
            $tipodespesa->user_id = $this->Auth->user('id');
            $tipodespesa->empresa_id = $this->Auth->user('empresa_id');
            if ($this->Tipodespesas->save($tipodespesa)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $this->set(compact('tipodespesa'));
        $this->set('_serialize', ['tipodespesa']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipodespesa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tipodespesa = $this->Tipodespesas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipodespesa = $this->Tipodespesas->patchEntity($tipodespesa, $this->request->data);

            if ($tipodespesa->empresa_id != $this->Auth->user('empresa_id')) {
                $this->Flash->error(__('Você não tem permissão de editar um registro que não pertence à sua Empresa'));
                return $this->redirect(['action' => 'index']);
            }

            if ($this->Tipodespesas->save($tipodespesa)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $this->set(compact('tipodespesa'));
        $this->set('_serialize', ['tipodespesa']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipodespesa id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $tipodespesa = $this->Tipodespesas->get($id);
        if ($tipodespesa->empresa_id == $this->Auth->user('empresa_id')) {
            if ($this->Tipodespesas->delete($tipodespesa)) {
                $this->Flash->success(__('O registro foi removido com sucesso.'));
            } else {
                $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
            }
        } else {
            $this->Flash->error(__('Você não tem permissão de deletar um registro que não pertence à sua Empresa'));
        }

        return $this->redirect($this->request->referer());
    }

}
