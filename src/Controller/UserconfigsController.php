<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Userconfigs Controller
 *
 * @property \App\Model\Table\UserconfigsTable $Userconfigs
 */
class UserconfigsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $userconfigs = $this->paginate($this->Userconfigs);

        $this->set(compact('userconfigs'));
        $this->set('_serialize', ['userconfigs']);
    }

    /**
     * View method
     *
     * @param string|null $id Userconfig id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userconfig = $this->Userconfigs->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('userconfig', $userconfig);
        $this->set('_serialize', ['userconfig']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userconfig = $this->Userconfigs->newEntity();
        if ($this->request->is('post')) {
            $userconfig = $this->Userconfigs->patchEntity($userconfig, $this->request->data);
            $userconfig->dt_cadastro =  date('Y-m-d H:i:s');
            $userconfig->user_id =  $this->Auth->user('id');
            $userconfig->empresa_id =  $this->Auth->user('empresa_id');
            if ($this->Userconfigs->save($userconfig)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $users = $this->Userconfigs->Users->find('list', ['limit' => 200]);
        $this->set(compact('userconfig', 'users'));
        $this->set('_serialize', ['userconfig']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Userconfig id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userconfig = $this->Userconfigs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userconfig = $this->Userconfigs->patchEntity($userconfig, $this->request->data);
            if ($this->Userconfigs->save($userconfig)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $users = $this->Userconfigs->Users->find('list', ['limit' => 200]);
        $this->set(compact('userconfig', 'users'));
        $this->set('_serialize', ['userconfig']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Userconfig id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userconfig = $this->Userconfigs->get($id);
        if ($this->Userconfigs->delete($userconfig)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
