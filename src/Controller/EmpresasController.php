<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Empresas Controller
 *
 * @property \App\Model\Table\EmpresasTable $Empresas
 */
class EmpresasController extends AppController {

    public $_TOTALSTEPS = 5; //sendo o index 1
    public $_TOTALSTEPS_ADD = 5; //sendo o index 1

    public function isAuthorized($user) {
        if (parent::isAuthorized($user)) {
            if (!$this->Auth->user('admin') && !$this->Auth->user('user_triade') && !$this->request->is('ajax')) {
                if (!in_array($this->request->param('action'), array('view', 'solicitarabertura', 'pedidosabertura', 'add'))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        if ($this->request->is('post') && !empty($this->request->data('termo-pesquisa'))) {
            $query = $this->Empresas->find()->where(["Empresas.razao like '%" . $this->request->data('termo-pesquisa') . "%'"]);
        } else {
            $query = $this->Empresas->find();
        }

        $empresas = $this->paginate($query->where(['solicitacao' => '0'])->orderAsc('Empresas.razao'));
        $this->set(compact('empresas'));
        $this->set('_serialize', ['empresas']);
    }

    /**
     * View method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        // se o link não tiver informando o id, é pq tem que mostrar a pagina do proprio usuario
        //verificação de permissão para visualizar dados de empresas
        if ($id == null || (!$this->Auth->user('admin') && $id != $this->Auth->user('empresa_id') )) {
            if ($this->Auth->user('empresa_id')) {
                $id = $this->Auth->user('empresa_id');
            } else if ($this->Empresas->find()->where(['id' => $id, 'user_id' => $this->Auth->user('id')])->count() > 0) {
                //no caso de empresa que está em processo de solicitação, a empresa ainda nao estará na sessão dele
                //então coloca na sessão
                $this->request->session()->write('Auth.User.empresa_id', $id);
            } else {
                $this->Flash->error(__('Você não tem permissão de visualizar registros que não pertencem à sua Empresa'));
                return $this->redirect($this->referer());
            }
        }

        $empresa = $this->Empresas->get($id, [
            'contain' => ['Estados', 'Cidades', 'Portes', 'Apuracaoformas', 'Atuacaoramos', 'Empresasocios.Documentos.Tipodocumentos',
                'Documentos.Tipodocumentos.Grupodocumentos' => array('conditions' => ['Grupodocumentos.id = ' => $this->_GrupoDocumentoFormacaoEmpresa]),
                'Documentos.Funcionarios', 'Users', 'Funcionarios.Cargos', 'Empresacnaes.Cnaeclasses', 'Acessosexternos.Sistemasexternos']
        ]);

        if ($empresa->status == '0') {
            $this->Flash->error(__('Aviso: Essa empresa está inativa no sistema.'));
        } else if ($empresa->status == '2') {
            $this->Flash->error(__('Aviso: Foi dado a baixa nessa empresa. Ela está baixada.'));
        }

        $this->set('empresa', $empresa);
        $this->set('_serialize', ['empresa']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null) {
        $id = ($id == null) ? ($this->request->data('id') ? $this->request->data('id') : null) : $id;

        //verificação de permissão para add/edit dados de empresas
        if (!$this->Auth->user('user_triade') && ($id && $id != $this->Auth->user('empresa_id'))) {
            if ($this->Empresas->find()->where(['id' => $id, 'user_id' => $this->Auth->user('id')])->count() <= 0) {
                //no caso de empresa que nem estar em processo de solicitação
                $this->Flash->error(__('Você não tem permissão de cadastrar uma nova empresa'));
                return $this->redirect($this->referer());
            }
        }


        $empresa = $id ? $this->Empresas->get($id) : $this->Empresas->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $empresa = $this->Empresas->patchEntity($empresa, $this->request->data);
            if (!$empresa->id) {
                $empresa->dt_cadastro = date('Y-m-d h:i:s');
                $empresa->user_id = $this->Auth->user('id');
            }
            if ($empresa->dirty('dt_nire')) {
                $empresa->dt_nire = $this->convertDateBRtoEN($empresa->dt_nire);
            }

            $empresa->capitalsocial = $this->formatarmoedaTomysql($empresa->capitalsocial);
            $empresa->faturamento_mensal = $this->formatarmoedaTomysql($empresa->faturamento_mensal);

            ++$empresa->num_step; // passa para o proximo passo, 
            if ($empresa->num_step > $this->_TOTALSTEPS) {
                $empresa->solicitacao_finalizada = 1;
            }

            if ($this->Empresas->save($empresa)) {
                if ($empresa->num_step > $this->_TOTALSTEPS) {
                    if ($this->Auth->user('empresa_id') == $this->_ID_TRIADE) {
                        $this->Flash->success(__('Empresa cadastrada com sucesso.'));
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->success(__('Empresa cadastrada com sucesso. A Triade Consultoria entrará em contato em breve.'));
                        $this->emailNewEmpresaToTriade($empresa);
                        $this->emailNewEmpresaToCliente($empresa);
                        return $this->redirect(['action' => 'view', $empresa->id]);
                    }
                } else {
                    return $this->redirect(['action' => 'add', $empresa->id]);
                }
            } else {
                $this->Flash->error(__('O registro de empresa não pôde ser salvo. Por favor, verifique as informações do formulário e tente novamente.'));
            }
        }

        $empresa->capitalsocial = $empresa->capitalsocial ? str_replace('.', ',', $empresa->capitalsocial) : '';
        $empresa->faturamento_mensal = $empresa->faturamento_mensal ? str_replace('.', ',', $empresa->faturamento_mensal) : '';
        $empresa->dt_nire = $this->convertDateENtoBR($empresa->dt_nire);
        $apuracaoformas = $this->Empresas->Apuracaoformas->find('list');
        $atuacaoramos = $this->Empresas->Atuacaoramos->find('list');
        $portes = $this->Empresas->Portes->find('list');
        $estados = $this->Empresas->Estados->find('list', ['limit' => 200])->orderAsc('estado_sigla');
        $this->set(compact('empresa', 'estados', 'apuracaoformas', 'atuacaoramos', 'portes'));
        $this->set('_serialize', ['empresa']);
    }

    /**
     * Solicitar Abertura de empresa method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function solicitarabertura($id = null) {

        $empresa = $id ? $this->Empresas->get($id) : $this->Empresas->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $empresa = $this->Empresas->patchEntity($empresa, $this->request->data);

            /* $emp_ramos = array();            
              if($ramos = $this->request->data('atuacaoramo_id')){
              foreach ($ramos as $ramo) {
              $atuacaoramo = $this->Empresas->Empresaramos->newEntity();
              $atuacaoramo->atuacaoramo_id = $ramo;
              $emp_ramos[] = $atuacaoramo;
              }
              debug($ramos);
              }
              $empresa->atuacaoramos = $emp_ramos; */

            if (!$empresa->id) {
                $empresa->solicitacao = 1;
                $empresa->dt_solicitacao = date('Y-m-d');
                $empresa->user_id = $this->Auth->user('id');
            }

            $empresa->capitalsocial = $this->formatarmoedaTomysql($empresa->capitalsocial);
            $empresa->faturamento_mensal = $this->formatarmoedaTomysql($empresa->faturamento_mensal);

            ++$empresa->num_step; // passa para o proximo passo,             
            if ($empresa->num_step > $this->_TOTALSTEPS) {
                $empresa->solicitacao_finalizada = 1;
            }

            if ($this->Empresas->save($empresa)) {
                if ($empresa->num_step > $this->_TOTALSTEPS) {
                    $this->Flash->success(__('Sua solicitação de abertura de empresa foi registrada, a Tríade entrará em contato em breve.'));
                    $this->emailAberturaEmpresaToTriade($empresa);
                    $this->emailAnaliseAberturaToCliente($empresa);
                    return $this->redirect(['action' => 'pedidosabertura']);
                } else {
                    return $this->redirect(['action' => 'solicitarabertura', $empresa->id]);
//                    if ($empresa->num_socios > 0) {
//                        return $this->redirect(['controller' => 'Empresasocios', 'action' => 'add', $empresa->id]);
//                    }
                }
            } else {
                $this->Flash->error(__('Para prosseguir a solicitação, verifique se as informações do formulário estão corretamente preenchidas.'));
            }
        }
        $empresa->capitalsocial = $empresa->capitalsocial ? str_replace('.', ',', $empresa->capitalsocial) : '';
        $empresa->faturamento_mensal = $empresa->faturamento_mensal ? str_replace('.', ',', $empresa->faturamento_mensal) : '';
        $apuracaoformas = $this->Empresas->Apuracaoformas->find('list');
//        $atuacaoramos = $this->Empresas->Empresaramos->Atuacaoramos->find('list');
        $atuacaoramos = $this->Empresas->Atuacaoramos->find('list');
        $portes = $this->Empresas->Portes->find('list');
        $estados = $this->Empresas->Estados->find('list')->orderAsc('estado_sigla');
        $this->set(compact('empresa', 'estados', 'portes', 'apuracaoformas', 'atuacaoramos'));
        $this->set('_serialize', ['empresa']);
    }

    /**
     * Pedidos de abertura de Empresa method
     *
     * @return \Cake\Network\Response|null
     */
    public function pedidosabertura() {
        $where = array('solicitacao' => '1');
        if ($this->Auth->user('tipousuario_id') != $this->_ADMIN) {
            $where['user_id'] = $this->Auth->user('id');
        }
        $empresas = $this->paginate($this->Empresas->find()->where($where)->orderAsc('dt_solicitacao'));

        $this->set(compact('empresas'));
        $this->set('_serialize', ['empresas']);
    }

    /**
     * Pedidos de abertura de Empresa method
     *
     * @return \Cake\Network\Response|null
     */
    public function sociosajax($empresa_id) {
        $socios = $this->Empresas->Empresasocios->find()->contain(['Cidades', 'Estados', 'Cargos', 'Estadocivils', 'Comunhaoregimes', 'Sexos'])
                ->where(['Empresasocios.empresa_id' => $empresa_id]);

        $cargos = $this->Empresas->Empresasocios->Cargos->find('list');
        $estados = $this->Empresas->Empresasocios->Estados->find('list');
        $cidades = $this->Empresas->Empresasocios->Cidades->find('list');
        $estadocivils = $this->Empresas->Empresasocios->Estadocivils->find('list');
        $comunhaoregimes = $this->Empresas->Empresasocios->Comunhaoregimes->find('list');
        $sexos = $this->Empresas->Empresasocios->Sexos->find('list');

        $this->set(compact('socios', 'users', 'empresas', 'cargos', 'estados', 'cidades', 'estadocivils', 'comunhaoregimes', 'sexos'));
        $this->set('_serialize', ['socios', 'users', 'empresas', 'cargos', 'estados', 'cidades', 'estadocivils', 'comunhaoregimes', 'sexos']);
    }

    /**
     * Efetuar Abertura da Empresa method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function efetuarabertura($id = null) {
        $empresa = $this->Empresas->get($id);
        $empresa->solicitacao = 0;

        if ($this->Empresas->save($empresa)) {
            $this->Flash->success(__('A empresa foi confirmada no sistema.'));

            return $this->redirect(['action' => 'pedidosabertura']);
        } else {
            $this->Flash->error(__('O registro de empresa não pôde ser cadastrado no sistema. Por favor, verifique as informações do formulário e tente novamente.'));
        }
    }

    /**
     * Setoresajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function setoresajax($empresa_id) {
        $retorno = $this->Empresas->Areaservicos->find('list')->where(['Areaservicos.empresa_id' => $empresa_id])->orWhere(['Areaservicos.todos' => 1]);
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Usuariosajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function usuariosajax() {
        /** Busca todos os usuarios da empresa selecionada (independente de setor), e se o usuario logado for da triade, exibe os usuarios da triade tbm */
        if (!$this->isStringEmpty($this->request->query('empresa_id')) && $this->isStringEmpty($this->request->query('areaservico_id'))) {
            $retorno = $this->Empresas->Users->find('list')->where(['Users.empresa_id' => $this->request->query('empresa_id')]);
            if ($this->Auth->user('user_triade')) {
                $retorno->orWhere(['Users.empresa_id' => $this->_ID_TRIADE]);
            }

            /** Busca o usuario logado */
        } else if ($this->isStringEmpty($this->request->query('empresa_id')) && $this->isStringEmpty($this->request->query('areaservico_id'))) {

            $retorno = $this->Empresas->Users->find('list')->where(['Users.id' => $this->Auth->user('id')]);
            if ($this->Auth->user('user_triade')) {
                $retorno->orWhere(['Users.empresa_id' => $this->_ID_TRIADE]);
            }


            /** Busca todos os usuarios da empresa selecionada do setor selecionado, e se o usuario logado for da triade, exibe os usuarios da triade tbm */
        } else if (!$this->isStringEmpty($this->request->query('empresa_id')) && !$this->isStringEmpty($this->request->query('areaservico_id'))) {

            $retorno = $this->Empresas->Users->find('list')->innerJoinWith('Usersareaservicos')->contain(['Usersareaservicos'])
                    ->select(['Users.id', 'Users.nome'])
                    ->where(['Usersareaservicos.empresa_id' => $this->request->query('empresa_id'), 'Usersareaservicos.areaservico_id' => $this->request->query('areaservico_id')]);

            /** Busca todos os usuarios de um setor específico da triade */
        } else if ($this->isStringEmpty($this->request->query('empresa_id')) && !$this->isStringEmpty($this->request->query('areaservico_id'))) {

            $retorno = $this->Empresas->Users->find('list')->innerJoinWith('Usersareaservicos')->contain(['Usersareaservicos'])
                    ->select(['Users.id', 'Users.nome'])
                    ->where(['Usersareaservicos.empresa_id' => $this->_ID_TRIADE, 'Usersareaservicos.areaservico_id' => $this->request->query('areaservico_id')]);
        }




        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        if ($this->Auth->user('admin') || $this->Auth->user('admin_empresa')) {
            if ($id == null || (!$this->Auth->user('admin') && $id != $this->Auth->user('empresa_id') )) {
                $id = $this->Auth->user('empresa_id');
            }
        } else {
            $this->Flash->error(__('Você não tem permissão de editar registros que não pertencem à sua Empresa'));
            return $this->redirect($this->referer());
        }


        $empresa = $this->Empresas->get($id, [
            'contain' => ['Documentos.Tipodocumentos', 'Funcionarios.Cargos', 'Empresasocios.Documentos.Tipodocumentos']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $empresa = $this->Empresas->patchEntity($empresa, $this->request->data);
            if ($empresa->dirty('dt_nire')) {
                $empresa->dt_nire = $this->convertDateBRtoEN($empresa->dt_nire);
            }

            $empresa->capitalsocial = $this->formatarmoedaTomysql($empresa->capitalsocial);
            $empresa->faturamento_mensal = $this->formatarmoedaTomysql($empresa->faturamento_mensal);

            if ($this->Empresas->save($empresa)) {
                $this->Flash->success(__('O registro de empresa foi atualizado.'));

                return $this->redirect(['action' => 'edit', $id]);
            } else {
                $this->Flash->error(__('O registro de empresa não pôde ser atualizado. Por favor, verifique as informações do formulário e tente novamente.'));
            }
        }


        if ($empresa->status == '0') {
            $this->Flash->error(__('Aviso: Essa empresa está inativa no sistema.'));
        } else if ($empresa->status == '2') {
            $this->Flash->error(__('Aviso: Foi dado a baixa nessa empresa. Ela está baixada.'));
        }



        $empresa->dt_nire = $this->convertDateENtoBR($empresa->dt_nire);
        $empresa->capitalsocial = $empresa->capitalsocial ? str_replace('.', ',', $empresa->capitalsocial) : '';
        $empresa->faturamento_mensal = $empresa->faturamento_mensal ? str_replace('.', ',', $empresa->faturamento_mensal) : '';
        $this->loadModel('Tipodocumentos');
//        $atuacaoramos = $this->Empresas->Empresaramos->Atuacaoramos->find('list');
        $grupodocumento_id = $this->_GrupoDocumentoFormacaoEmpresa;
        $apuracaoformas = $this->Empresas->Apuracaoformas->find('list');
        $tipodocumentos = $this->Tipodocumentos->find('list')->where(['grupodocumento_id' => $grupodocumento_id]);
        $atuacaoramos = $this->Empresas->Atuacaoramos->find('list');
        $portes = $this->Empresas->Portes->find('list');
        $estados = $this->Empresas->Estados->find('list')->orderAsc('estado_sigla');
        $acessosexternos = $this->Empresas->Acessosexternos->find()->contain(['Sistemasexternos'])->where(['Acessosexternos.empresa_id' => $id]);
        $servicos = $this->Empresas->Empresaservicos->Tiposervicos->find('list')->where(['Tiposervicos.interno = 1 '])->orderAsc('Tiposervicos.nome');
        $deveres = $this->Empresas->Empresadeveres->Tipodeveres->find('list')->orderAsc('Tipodeveres.nome');
        $this->set(compact('empresa', 'estados', 'acessosexternos', 'portes', 'apuracaoformas', 'atuacaoramos', 'tipodocumentos', 'grupodocumento_id', 'servicos', 'deveres'));
        $this->set('_serialize', ['empresa']);
    }

    /**
     * Addlegisanotacao method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function addlegisanotacao($id) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $anotacao = $this->Empresas->Legislativoempresas->newEntity();
            $anotacao->empresa_id = $id;
            $anotacao->titulo = $this->request->data('titulo');
            $anotacao->anotacao = $this->request->data('anotacao');
            $anotacao->dt_cadastro = date('Y-m-d H:i:s');
            /* verifica se já existia esse tipo ou é um novo cadastro para ser vinculado */
            if (is_numeric($this->request->data('tiponote_id'))) {
                $anotacao->tiponote_id = $this->request->data('tiponote_id');
            } else {
                $new = $this->Empresas->Legislativoempresas->Tiponotes->newEntity();
                $new->descricao = $this->request->data('tiponote_id');
                if ($this->Empresas->Legislativoempresas->Tiponotes->save($new)) {
                    $anotacao->tiponote_id = $new->id;
                }
            }

            if ($this->Empresas->Legislativoempresas->save($anotacao)) {
                $retorno = $anotacao;
            } else {
                $retorno = null;
            }
        } else {
            $retorno = null;
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Editlegisanotacao method
     *
     * @param string|null $id Legislativoempresa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editlegisanotacao($id) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $anotacao = $this->Empresas->Legislativoempresas->get($id);
            $anotacao->titulo = $this->request->data('titulo');
            $anotacao->anotacao = $this->request->data('anotacao');
            /* verifica se já existia esse tipo ou é um novo cadastro para ser vinculado */
            if (is_numeric($this->request->data('tiponote_id'))) {
                $anotacao->tiponote_id = $this->request->data('tiponote_id');
            } else {
                $new = $this->Empresas->Legislativoempresas->Tiponotes->newEntity();
                $new->descricao = $this->request->data('tiponote_id');
                if ($this->Empresas->Legislativoempresas->Tiponotes->save($new)) {
                    $anotacao->tiponote_id = $new->id;
                }
            }

            if ($this->Empresas->Legislativoempresas->save($anotacao)) {
                $retorno = $anotacao;
            } else {
                $retorno = null;
            }
        } else {
            $retorno = null;
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Getalllegisanotacoes method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function getalllegisanotacoes($id) {
        $retorno = $this->Empresas->Legislativoempresas->find()->contain(['Tiponotes'])->where(['Legislativoempresas.empresa_id' => $id]);
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Detailslegisview method
     *
     * @param string|null $id Legislativoempresa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function detailslegisview($id) {
        $retorno = $this->Empresas->Legislativoempresas->get($id);
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Deletelegisanotacao method
     *
     * @param string|null $id Legislativoempresa id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deletelegisanotacao($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $anotacao = $this->Empresas->Legislativoempresas->get($id);
        if ($this->Empresas->Legislativoempresas->delete($anotacao)) {
            $retorno = true;
        } else {
            $retorno = false;
        }

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Desativar method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function desativar($id = null) {
        if ($this->Auth->user('user_triade')) {
            $empresa = $this->Empresas->get($id);
            $empresa->status = 0;
            if ($this->Empresas->save($empresa)) {
                $this->Flash->success(__('A empresa agora está inativa no sistema'));
            } else {
                $this->Flash->error(__('Não foi possível desativar essa empresa. Por favor, tente novamente.'));
            }
        } else {
            $this->Flash->error(__('Acesso Restrito somente à Administradores'));
        }
        return $this->redirect($this->request->referer());
    }

    /**
     * Reativar method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function reativar($id = null) {
        if ($this->Auth->user('user_triade')) {
            $empresa = $this->Empresas->get($id);
            $empresa->status = 1;
            if ($this->Empresas->save($empresa)) {
                $this->Flash->success(__('A empresa está novamente ativa no sistema'));
            } else {
                $this->Flash->error(__('Não foi possível reativar essa empresa. Por favor, tente novamente.'));
            }
        } else {
            $this->Flash->error(__('Acesso Restrito somente à Administradores'));
        }
        return $this->redirect($this->request->referer());
    }

    /**
     * Darbaixa method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function darbaixa($id = null) {
        if ($this->Auth->user('user_triade')) {
            $empresa = $this->Empresas->get($id);
            $empresa->status = 2;
            if ($this->Empresas->save($empresa)) {
                $this->Flash->success(__('Foi dado a baixa dessa empresa no sistema'));
            } else {
                $this->Flash->error(__('Não foi possível dar baixa nessa empresa. Por favor, tente novamente.'));
            }
        } else {
            $this->Flash->error(__('Acesso Restrito somente à Administradores'));
        }
        return $this->redirect($this->request->referer());
    }

    /**
     * Delete method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if ($this->Auth->user('admin')) {
            $empresa = $this->Empresas->get($id);
            if ($this->Empresas->delete($empresa)) {
                $this->Flash->success(__('O registro de empresa foi deletado.'));
            } else {
                $this->Flash->error(__('O registro de empresa não pôde ser deletado. Por favor, tente novamente.'));
            }
        } else {
            $this->Flash->error(__('Acesso Restrito somente à Administradores'));
        }
        return $this->redirect($this->request->referer());
    }

    /**
     * Addservico method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function addservico($id) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->Empresas->Empresaservicos->find()->where(['empresa_id' => $id, 'tiposervico_id' => $this->request->data('tiposervico_id')])->count() <= 0) {
                $servicoempresa = $this->Empresas->Empresaservicos->newEntity();
                $servicoempresa->empresa_id = $id;
                $servicoempresa->tiposervico_id = $this->request->data('tiposervico_id');
                $servicoempresa->user_id = $this->Auth->user('id');
                $servicoempresa->dt_cadastro = date('Y-m-d H:i:s');

                if ($this->Empresas->Empresaservicos->save($servicoempresa)) {
                    $retorno = $servicoempresa;
                } else {
                    $retorno = 'nao salvou';
                }
            } else {
                $retorno = 'ja tava cadastrado';
            }
        } else {
            $retorno = 'parametro avacalhado';
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Getdeveres method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function getdeveres($id) {
        $retorno = $this->Empresas->Empresadeveres->find()->where(['empresa_id' => $id]);
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Getservicos method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function getservicos($id) {
        $retorno = $this->Empresas->Empresaservicos->find()->where(['empresa_id' => $id]);
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Removeservico method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function removeservico($id) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->Empresas->Empresaservicos->deleteAll(['empresa_id' => $id, 'tiposervico_id' => $this->request->data('tiposervico_id')])) {
                $retorno = true;
            } else {
                $retorno = null;
            }
        } else {
            $retorno = null;
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Adddever method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function adddever($id) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->Empresas->Empresadeveres->find()->where(['empresa_id' => $id, 'tipodever_id' => $this->request->data('tipodever_id')])->count() <= 0) {
                $deverempresa = $this->Empresas->Empresadeveres->newEntity();
                $deverempresa->empresa_id = $id;
                $deverempresa->tipodever_id = $this->request->data('tipodever_id');
                $deverempresa->user_id = $this->Auth->user('id');
                $deverempresa->dt_cadastro = date('Y-m-d H:i:s');

                if ($this->Empresas->Empresadeveres->save($deverempresa)) {
                    $retorno = $deverempresa;
                } else {
                    $retorno = 'nao salvou';
                }
            } else {
                $retorno = 'ja tava cadastrado';
            }
        } else {
            $retorno = 'parametro avacalhado';
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Removedever method
     *
     * @param string|null $id Empresa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function removedever($id) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->Empresas->Empresadeveres->deleteAll(['empresa_id' => $id, 'tipodever_id' => $this->request->data('tipodever_id')])) {
                $retorno = true;
            } else {
                $retorno = null;
            }
        } else {
            $retorno = null;
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /* Email disparado para o cliente para confirmar o recebimento da solicitação dele */

    public function emailNewEmpresaToCliente($empresa) {
        try {
            $email = new Email('default');
            $email->viewVars(['nomecliente' => $this->Auth->user('nome'), 'nomeempresa' => $empresa->razao, 'cnpj' => $empresa->cnpj]);
            $email->subject('Processando Pedido de Cadastramento')
                    ->template('default', 'analisecadastroempresa')
                    ->emailFormat('html')
                    ->to($this->Auth->user('email'))
                    ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

    /* Email disparado para a triade quando alguém cadastra uma nova empresa p triade */

    public function emailNewEmpresaToTriade($empresa) {
        try {
            $email = new Email('default');
            $email->viewVars(['nomecliente' => $this->Auth->user('nome'), 'nomeempresa' => $empresa->razao, 'cnpj' => $empresa->cnpj, 'idcliente' => $this->Auth->user('id'), 'idempresa' => $empresa->id]);
            $email->subject('Novo Pedido de Cadastramento de Empresa')
                    ->template('default', 'novocadastroempresa')
                    ->emailFormat('html')
                    ->to($this->_EMAIL_ADMIN)
                    ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

    /* Email disparado para o cliente para confirmar solicitação de empresa */

    public function emailAnaliseAberturaToCliente($empresa) {
        try {
            $email = new Email('default');
            $email->viewVars(['nomecliente' => $this->Auth->user('nome'), 'nomeempresa' => $empresa->nome1]);
            $email->subject('Pedido de Abertura de Empresa')
                    ->template('default', 'analiseaberturaempresa')
                    ->emailFormat('html')
                    ->to($this->Auth->user('email'))
                    ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

    /* Email disparado para a triade quando alguém cadastra uma nova empresa p triade */

    public function emailAberturaEmpresaToTriade($empresa) {
        try {
            $email = new Email('default');
            $email->viewVars(['nomecliente' => $this->Auth->user('nome'), 'nomeempresa' => $empresa->nome1, 'idcliente' => $this->Auth->user('id'), 'idempresa' => $empresa->id]);
            $email->subject('Solicitação de Abertura de Empresa')
                    ->template('default', 'novaaberturaempresa')
                    ->emailFormat('html')
                    ->to($this->_EMAIL_ADMIN)
                    ->send();
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email para o seu email cadastrado. Confira se está correto, por favor.'));
        }
    }

}
