<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Tipodeveres Controller
 *
 * @property \App\Model\Table\TipodeveresTable $Tipodeveres
 */
class TipodeveresController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Gruposervicos.Areaservicos']
        ];
        $tipodeveres = $this->paginate($this->Tipodeveres);

        $this->set(compact('tipodeveres'));
        $this->set('_serialize', ['tipodeveres']);
    }

    /**
     * ajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function ajax($gruposervico_id = null) {
        $retorno = $this->Tipodeveres->find('list')->where(['gruposervico_id' => $gruposervico_id])->order(['descricao' => 'ASC']);
        $tpview = 'list';
        $this->set(compact('retorno', 'tpview'));
        $this->set('_serialize', ['retorno', 'tpview']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipodevere id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $tipodevere = $this->Tipodeveres->get($id, [
            'contain' => ['Gruposervicos.Areaservicos']
        ]);

        $this->set('tipodevere', $tipodevere);
        $this->set('_serialize', ['tipodevere']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $tipodevere = $this->Tipodeveres->newEntity();
        if ($this->request->is('post')) {
            $tipodevere = $this->Tipodeveres->patchEntity($tipodevere, $this->request->data);
            $tipodevere->dt_cadastro = date('Y-m-d H:i:s');
            $tipodevere->user_id = $this->Auth->user('id');

            $empresas = array();
            if ($tipodevere->empresa_ids) {
                foreach ($tipodevere->empresa_ids as $empresa) {
                    $deverempresa = $this->Tipodeveres->Empresadeveres->newEntity();
                    $deverempresa->empresa_id = $empresa;
                    $deverempresa->user_id = $this->Auth->user('id');
                    $deverempresa->dt_cadastro = date('Y-m-d H:i:s');
                    $empresas[] = $deverempresa;
                }
            }

            $tipodevere->empresadeveres = $empresas;
            

            if ($this->Tipodeveres->save($tipodevere)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $empresas = $this->Tipodeveres->Empresadeveres->Empresas->find('list');
        $areaservicos = $this->Tipodeveres->Gruposervicos->Areaservicos->find('list')->where(['empresa_id' => $this->_ID_TRIADE]);
        $gruposervicos = $this->Tipodeveres->Gruposervicos->find('list');
        $this->set(compact('tipodevere', 'gruposervicos', 'areaservicos', 'empresas'));
        $this->set('_serialize', ['tipodevere']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipodevere id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $tipodevere = $this->Tipodeveres->get($id, [
            'contain' => ['Gruposervicos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipodevere = $this->Tipodeveres->patchEntity($tipodevere, $this->request->data);

            $cadastrados = $this->Tipodeveres->Empresadeveres->find()->where(['tipodever_id' => $id])->extract('empresa_id')->filter()->toArray();
            $empresas = array();
            if ($tipodevere->empresa_ids) {
                foreach ($tipodevere->empresa_ids as $empresa) {
                    if (!in_array($empresa, $cadastrados)) {
                        $deverempresa = $this->Tipodeveres->Empresadeveres->newEntity();
                        $deverempresa->empresa_id = $empresa;
                        $deverempresa->user_id = $this->Auth->user('id');
                        $deverempresa->dt_cadastro = date('Y-m-d H:i:s');
                        $empresas[] = $deverempresa;
                    }
                }
                $this->Tipodeveres->Empresadeveres->deleteAll(['tipodever_id' => $id, 'empresa_id NOT IN' => $tipodevere->empresa_ids]);
            } else {
                $this->Tipodeveres->Empresadeveres->deleteAll(['tipodever_id' => $id]);
            }
            $tipodevere->empresadeveres = $empresas;

            if ($this->Tipodeveres->save($tipodevere)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->loadModel('Empresas');
        $selectedempresas = array_keys($this->Empresas->find('list')->innerJoinWith('Empresadeveres')->where(['Empresadeveres.tipodever_id' => $id])->toArray());

        $empresas = $this->Empresas->find('list');
        $tipodevere->areaservico_id = $tipodevere->gruposervico->areaservico_id;
        $areaservicos = $this->Tipodeveres->Gruposervicos->Areaservicos->find('list')->where(['empresa_id' => $this->_ID_TRIADE]);
        $gruposervicos = $this->Tipodeveres->Gruposervicos->find('list');
        $this->set(compact('tipodevere', 'gruposervicos', 'areaservicos', 'empresas', 'selectedempresas'));
        $this->set('_serialize', ['tipodevere']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipodevere id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $tipodevere = $this->Tipodeveres->get($id);
        if ($this->Tipodeveres->delete($tipodevere)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

}
