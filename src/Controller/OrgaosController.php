<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Orgaos Controller
 *
 * @property \App\Model\Table\OrgaosTable $Orgaos
 */
class OrgaosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => []
        ];
        $orgaos = $this->paginate($this->Orgaos);

        $this->set(compact('orgaos'));
        $this->set('_serialize', ['orgaos']);
    }

    /**
     * View method
     *
     * @param string|null $id Orgao id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $orgao = $this->Orgaos->get($id, [
            'contain' => ['Leisnormas.Entidades']
        ]);

        $this->set('orgao', $orgao);
        $this->set('_serialize', ['orgao']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $orgao = $this->Orgaos->newEntity();
        if ($this->request->is('post')) {
            $orgao = $this->Orgaos->patchEntity($orgao, $this->request->data);
            $orgao->dt_cadastro =  date('Y-m-d H:i:s');
            $orgao->user_id =  $this->Auth->user('id');
            $orgao->empresa_id =  $this->Auth->user('empresa_id');
            if ($this->Orgaos->save($orgao)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        
        $this->set(compact('orgao'));
        $this->set('_serialize', ['orgao']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Orgao id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $orgao = $this->Orgaos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $orgao = $this->Orgaos->patchEntity($orgao, $this->request->data);
            if ($this->Orgaos->save($orgao)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        
        $this->set(compact('orgao'));
        $this->set('_serialize', ['orgao']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Orgao id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $orgao = $this->Orgaos->get($id);
        if ($this->Orgaos->delete($orgao)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
