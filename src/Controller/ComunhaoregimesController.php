<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Comunhaoregimes Controller
 *
 * @property \App\Model\Table\ComunhaoregimesTable $Comunhaoregimes
 */
class ComunhaoregimesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $comunhaoregimes = $this->paginate($this->Comunhaoregimes);

        $this->set(compact('comunhaoregimes'));
        $this->set('_serialize', ['comunhaoregimes']);
    }

    /**
     * View method
     *
     * @param string|null $id Comunhaoregime id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $comunhaoregime = $this->Comunhaoregimes->get($id, [
            'contain' => ['Empresasocios']
        ]);

        $this->set('comunhaoregime', $comunhaoregime);
        $this->set('_serialize', ['comunhaoregime']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $comunhaoregime = $this->Comunhaoregimes->newEntity();
        if ($this->request->is('post')) {
            $comunhaoregime = $this->Comunhaoregimes->patchEntity($comunhaoregime, $this->request->data);
            if ($this->Comunhaoregimes->save($comunhaoregime)) {
                $this->Flash->success(__('Regime de comunhão cadastrado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao cadastrar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('comunhaoregime'));
        $this->set('_serialize', ['comunhaoregime']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Comunhaoregime id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $comunhaoregime = $this->Comunhaoregimes->get($id, [
            'contain' => []
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $comunhaoregime = $this->Comunhaoregimes->patchEntity($comunhaoregime, $this->request->data);
            if ($this->Comunhaoregimes->save($comunhaoregime)) {
                $this->Flash->success(__('Regime de comunhão atualizado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('comunhaoregime'));
        $this->set('_serialize', ['comunhaoregime']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Comunhaoregime id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        
        $this->request->allowMethod(['post', 'delete']);
        $comunhaoregime = $this->Comunhaoregimes->get($id);
        
        if ($this->Comunhaoregimes->delete($comunhaoregime)) {
            $this->Flash->success(__('Regime de comunhão removido com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }
}
