<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Leisnormas Controller
 *
 * @property \App\Model\Table\LeisnormasTable $Leisnormas
 */
class LeisnormasController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Entidades', 'Orgaos', 'Users']
        ];
        $leisnormas = $this->paginate($this->Leisnormas);

        $this->set(compact('leisnormas'));
        $this->set('_serialize', ['leisnormas']);
    }

    /**
     * Listajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function listajax() {
        $retorno = $this->Leisnormas->find('ajaxList');
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * View method
     *
     * @param string|null $id Leisnorma id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $leisnorma = $this->Leisnormas->get($id, [
            'contain' => ['Entidades', 'Orgaos', 'Users', 'Leisartigos', 'Leisassuntos.Assuntos', 'Leiscapitulos', 'Leisdocumentos', 'Leisincisos.Leisartigos', 'Leisletras.Leisartigos', 'Leislivros', 'Leisparagrafos.Leisartigos', 'Leispartes', 'Leissections', 'Leissubsections', 'Leistitulos', 'Telaquestionarios']
        ]);

        $this->set('leisnorma', $leisnorma);
        $this->set('_serialize', ['leisnorma']);
    }

    /**
     * Detailsview method
     *
     * @param string|null $id Leisassunto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function detailsview($id = null) {
        $lei = $this->Leisnormas->Leisassuntos->get($id);
        $this->set('lei', $lei);
        $this->set('_serialize', ['lei']);
        $this->render('detailsajax');
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $leisnorma = $this->Leisnormas->newEntity();
        if ($this->request->is('post')) {
            $leisnorma = $this->Leisnormas->patchEntity($leisnorma, $this->request->data);
            $leisnorma->dt_cadastro = date('Y-m-d H:i:s');
            $leisnorma->user_id = $this->Auth->user('id');
            $leisnorma->empresa_id = $this->Auth->user('empresa_id');

            $artigos = array();
            if ($this->request->data('leisartigos_id') && is_array($this->request->data('leisartigos_id'))) {
                foreach ($this->request->data('leisartigos_id') as $artigo) {
                    if (is_numeric($artigo)) {
                        $artigos[] = $this->Leisnormas->Leisartigos->get($artigo);
                    } else {
                        $new = $this->Leisnormas->Leisartigos->newEntity();
                        $new->nome = $artigo;
                        $new->descricao = $artigo;
                        $new->user_id = $this->Auth->user('id');
                        $new->dt_cadastro = date('Y-m-d H:i:s');
//                        if ($this->Leisnormas->Leisartigos->save($new)) {
                        $artigos[] = $new;
//                        }
                    }
                }
            }
            $leisnorma->leisartigos = $artigos;

            $assuntos = array();
            if ($this->request->data('assuntos_id') && is_array($this->request->data('assuntos_id'))) {
                foreach ($this->request->data('assuntos_id') as $assunto) {
                    if (is_numeric($assunto)) {
                        $leisassunto = $this->Leisnormas->Leisassuntos->newEntity();
                        $leisassunto->assunto_id = $assunto;
                        $assuntos[] = $leisassunto;
                    } else {
                        $new = $this->Leisnormas->Leisassuntos->Assuntos->newEntity();
                        $new->descricao = $assunto;
                        $new->user_id = $this->Auth->user('id');
                        $new->dt_cadastro = date('Y-m-d H:i:s');
                        if ($this->Leisnormas->Leisassuntos->Assuntos->save($new)) {
                            $leisassunto = $this->Leisnormas->Leisassuntos->newEntity();
                            $leisassunto->assunto_id = $new->id;
                            $assuntos[] = $leisassunto;
                        }
                    }
                }
            }
            $leisnorma->leisassuntos = $assuntos;


            if ($this->Leisnormas->save($leisnorma)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $entidades = $this->Leisnormas->Entidades->find('list');
        $orgaos = $this->Leisnormas->Orgaos->find('list');
        $leisartigos = $this->Leisnormas->Leisartigos->find('list');
        $assuntos = $this->Leisnormas->Leisassuntos->Assuntos->find('list');
        $this->set(compact('leisnorma', 'entidades', 'orgaos', 'leisartigos', 'assuntos'));
        $this->set('_serialize', ['leisnorma']);
    }

    /**
     * Addlei method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addlei() {
        set_time_limit(300);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $leisnorma = $this->Leisnormas->newEntity();
            $leisnorma->url = $this->request->data('url_lei');
            $leisnorma->dt_cadastro = date('Y-m-d H:i:s');
            $leisnorma->user_id = $this->Auth->user('id');


            $content = file_get_contents($this->request->data('url_lei'));
            if (!$content) {
                return false;
            }
            $content = utf8_encode($this->strip_tags_content($content, '<center>', true)); //tira o cabeçalho do site externo, que no caso usa a tag center
            $leisnorma->texto_html = addslashes($content);
            $leisnorma->texto_completo = addslashes(trim(strip_tags($content, '<blockquote><p><span><a><table><tr><tbody><thead><tfoot><td><br></br><br/>')));

            $doc = new \DOMDocument();
            $content = preg_replace("/&#?[a-z0-9]+;/i", " ", $content); //tirando os caracteres especiais
            @$doc->loadHTML($content);
            $titulo = $doc->getElementsByTagName('p');

            @$detalhes = $leisnorma->descricao = utf8_decode(trim(strip_tags($titulo->item(0)->nodeValue)));

            $leisnorma->tipolei_descricao = strtok($detalhes, ' ');
            $segundaparte_descricao = strtok(' ');
            if (strstr($segundaparte_descricao, 'º') == false) {
                $leisnorma->tipolei_descricao .= ' ' . $segundaparte_descricao;
                strtok(' ');
            }

            $leisnorma->numero = strtok(',');

            //verifica se essa lei ja foi cadastrada do banco de dados
            $leiexistente = $this->Leisnormas->find()->where(['tipolei_descricao' => $leisnorma->tipolei_descricao])->andWhere(['numero' => $leisnorma->numero]);
            if ($leiexistente->count() > 0) {
                return $leiexistente->first();
            }

            $dia = trim(strtok('DE '));
            if (!is_int($dia) && !is_numeric($dia)) {
                $dia = strtok('DE ');
            }

            $mes = strtok(' ');

            if (strlen($mes) <= 3 && !$this->isMes($mes)) {
                $mes = strtok(' ');
                if (!$this->isMes($mes)) {
                    $mes = strtok(' ');
                }
            }

            $mes = $this->messtringTonumero(trim(strtoupper($mes)));
            $ano = strtok('DE ');
            $ano = str_replace(array('.', ','), '', $ano);

            $leisnorma->ano = $ano;
            $date = $ano . '-' . $mes . '-' . $dia;

            if (@checkdate($mes, $dia, $ano)) {
                $leisnorma->dt_publicacao_string = $date;
                $leisnorma->dt_publicacao = $date;
            } else {
                $date = trim(substr(trim(strip_tags($leisnorma->texto_completo)), '-10'));
                $data = explode('.', $date);
                $date = implode('-', array_reverse($data));
                if (@checkdate($data[1], $data[0], $data[2])) {
                    $leisnorma->ano = $data[2];
                    $leisnorma->dt_publicacao_string = $date;
                    $leisnorma->dt_publicacao = $date;
                }
            }


            $ementa = $doc->getElementsByTagName('td');
            @$leisnorma->ementa = utf8_decode(trim(strip_tags($ementa->item(1)->nodeValue)));
            if ($this->Leisnormas->save($leisnorma)) {
                return $leisnorma;
            } else {
                return false;
            }
        }
    }

    /**
     * Addleiassunto method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addleiassunto($id_assunto) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($leisnorma = $this->addlei()) {
                $lei = $this->Leisnormas->Leisassuntos->newEntity();
                $lei->assunto_id = $id_assunto;
                $lei->leisnorma_id = $leisnorma->id;
                $lei->texto_modificado = $leisnorma->texto_html;

                //verifica se essa lei ja foi vinculada para esse assunto
                $vinculoexistente = $this->Leisnormas->Leisassuntos->find()->where(['assunto_id' => $lei->assunto_id])->andWhere(['leisnorma_id' => $lei->leisnorma_id]);
                if ($vinculoexistente->count() > 0) {
                    $retorno = true;
                } else {
                    if ($this->Leisnormas->Leisassuntos->save($lei)) {
                        $leiassunto = $lei;
//                        $leisnorma->texto_completo = $leisnorma->texto_html = '';
                        $retorno = $leisnorma;
                    } else {
                        $retorno = null;
                    }
                }
            } else {
                $retorno = null;
            }
        } else {
            $retorno = null;
        }
        $this->set(compact('retorno', 'leiassunto'));
        $this->set('_serialize', ['retorno', 'leiassunto']);
    }

    /**
     * Addleiassunto method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function editleiassunto($id_leiassunto) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $leiassunto = $this->Leisnormas->Leisassuntos->get($id_leiassunto);
            $leiassunto->texto_modificado = addslashes($this->request->data('texto_modificado'));


            if ($this->Leisnormas->Leisassuntos->save($leiassunto)) {
                $retorno = true;
            } else {
                $retorno = null;
            }
        } else {
            $retorno = null;
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    function isMes($mes) {
        $meses = array('janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro');
        if (in_array(strtolower(trim($mes)), $meses)) {
            return true;
        }
        return false;
    }

    function messtringTonumero($mes) {
        switch ($mes) {
            case 'JANEIRO':
                $mes = '01';
                break;
            case 'FEVEREIRO':
                $mes = '02';
                break;
            case 'MARÇO':
                $mes = '03';
                break;
            case 'ABRIL':
                $mes = '04';
                break;
            case 'MAIO':
                $mes = '05';
                break;
            case 'JUNHO':
                $mes = '06';
                break;
            case 'JULHO':
                $mes = '07';
                break;
            case 'AGOSTO':
                $mes = '08';
                break;
            case 'SETEMBRO':
                $mes = '09';
                break;
            case 'OUTUBRO':
                $mes = '10';
                break;
            case 'NOVEMBRO':
                $mes = '11';
                break;
            case 'DEZEMBRO':
                $mes = '12';
                break;
            default:
                $mes = false;
                break;
        }
        return $mes;
    }

    /**
     * Edit method
     *
     * @param string|null $id Leisnorma id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $leisnorma = $this->Leisnormas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $leisnorma = $this->Leisnormas->patchEntity($leisnorma, $this->request->data);

            $artigoscadastrados = $this->Leisnormas->Leisartigos->find()->where(['leisnorma_id' => $id])->extract('id')->filter()->toArray();
            $artigos = array();
            if ($this->request->data('leisartigos_id') && is_array($this->request->data('leisartigos_id'))) {
                foreach ($this->request->data('leisartigos_id') as $artigo) {
                    if (!in_array($artigo, $artigoscadastrados)) {
                        if (is_numeric($artigo)) {
                            $artigos[] = $this->Leisnormas->Leisartigos->get($artigo);
                        } else {
                            $new = $this->Leisnormas->Leisartigos->newEntity();
                            $new->nome = $artigo;
                            $new->descricao = $artigo;
                            $new->user_id = $this->Auth->user('id');
                            $new->dt_cadastro = date('Y-m-d H:i:s');
                            $artigos[] = $new;
                        }
                    }
                }
            }
            $leisnorma->leisartigos = $artigos;
            $this->Leisnormas->Leisartigos->updateAll(['leisnorma_id = null'], ['leisnorma_id' => $id, 'id NOT IN' => $this->request->data('leisartigos_id')]);


            $assuntoscadastrados = $this->Leisnormas->Leisassuntos->find()->where(['leisnorma_id' => $id])->extract('assunto_id')->filter()->toArray();
            $assuntos = array();
            if ($this->request->data('assuntos_id') && is_array($this->request->data('assuntos_id'))) {
                foreach ($this->request->data('assuntos_id') as $assunto) {
                    if (!in_array($assunto, $assuntoscadastrados)) {
                        if (is_numeric($assunto)) {
                            $leisassunto = $this->Leisnormas->Leisassuntos->newEntity();
                            $leisassunto->assunto_id = $assunto;
                            $assuntos[] = $leisassunto;
                        } else {
                            $new = $this->Leisnormas->Leisassuntos->Assuntos->newEntity();
                            $new->descricao = $assunto;
                            $new->user_id = $this->Auth->user('id');
                            $new->dt_cadastro = date('Y-m-d H:i:s');
                            if ($this->Leisnormas->Leisassuntos->Assuntos->save($new)) {
                                $leisassunto = $this->Leisnormas->Leisassuntos->newEntity();
                                $leisassunto->assunto_id = $new->id;
                                $assuntos[] = $leisassunto;
                            }
                        }
                    }
                }
            }
            $leisnorma->leisassuntos = $assuntos;
            $this->Leisnormas->Leisassuntos->deleteAll(['leisnorma_id' => $id, 'assunto_id NOT IN' => $this->request->data('assuntos_id')]);



            if ($this->Leisnormas->save($leisnorma)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $this->loadModel('Assuntos');
        $selectedassuntos = array_keys($this->Assuntos->find('list')->innerJoinWith('Leisassuntos')->where(['Leisassuntos.leisnorma_id' => $id])->toArray());
        $selectedartigos = array_keys($this->Leisnormas->Leisartigos->find('list')->where(['leisnorma_id' => $id])->toArray());

        $entidades = $this->Leisnormas->Entidades->find('list');
        $orgaos = $this->Leisnormas->Orgaos->find('list');
        $leisartigos = $this->Leisnormas->Leisartigos->find('list');
        $assuntos = $this->Leisnormas->Leisassuntos->Assuntos->find('list');
        $this->set(compact('leisnorma', 'entidades', 'orgaos', 'leisartigos', 'assuntos', 'selectedassuntos', 'selectedartigos'));
        $this->set('_serialize', ['leisnorma']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Leisnorma id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $leisnorma = $this->Leisnormas->get($id);
        if ($this->Leisnormas->delete($leisnorma)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
