<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Formaspagamentos Controller
 *
 * @property \App\Model\Table\FormaspagamentosTable $Formaspagamentos
 */
class FormaspagamentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $formaspagamentos = $this->paginate($this->Formaspagamentos);

        $this->set(compact('formaspagamentos'));
        $this->set('_serialize', ['formaspagamentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Formaspagamento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $formaspagamento = $this->Formaspagamentos->get($id, [
            'contain' => ['Users', 'Livrocaixas', 'Movimentacaobancarias']
        ]);

        $this->set('formaspagamento', $formaspagamento);
        $this->set('_serialize', ['formaspagamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $formaspagamento = $this->Formaspagamentos->newEntity();
        if ($this->request->is('post')) {
            $formaspagamento = $this->Formaspagamentos->patchEntity($formaspagamento, $this->request->data);
            $formaspagamento->dt_cadastro =  date('Y-m-d H:i:s');
            $formaspagamento->user_id =  $this->Auth->user('id');
            if ($this->Formaspagamentos->save($formaspagamento)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $users = $this->Formaspagamentos->Users->find('list', ['limit' => 200]);
        $this->set(compact('formaspagamento', 'users'));
        $this->set('_serialize', ['formaspagamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Formaspagamento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $formaspagamento = $this->Formaspagamentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $formaspagamento = $this->Formaspagamentos->patchEntity($formaspagamento, $this->request->data);
            if ($this->Formaspagamentos->save($formaspagamento)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $users = $this->Formaspagamentos->Users->find('list', ['limit' => 200]);
        $this->set(compact('formaspagamento', 'users'));
        $this->set('_serialize', ['formaspagamento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Formaspagamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $formaspagamento = $this->Formaspagamentos->get($id);
        if ($this->Formaspagamentos->delete($formaspagamento)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
