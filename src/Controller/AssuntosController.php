<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Assuntos Controller
 *
 * @property \App\Model\Table\AssuntosTable $Assuntos
 */
class AssuntosController extends AppController {

    public function beforeFilter(Event $event) {
        if ($this->request->params['action'] == 'adddocumento') {//seta a sessão
            $this->request->session()->id($this->request['pass'][0]);
            $this->request->session()->start();
        }
    }

    public function beforeRender(\Cake\Event\Event $event) {
        $this->viewBuilder()->layout('legislativo');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Assuntosfilhos']
        ];
        $assuntos = $this->paginate($this->Assuntos->find()->where(['assunto_id is null'])->orderAsc('Assuntos.descricao'));
        $todosassuntos = $this->Assuntos->find('list')->orderAsc('Assuntos.descricao');
        $tipoanotacoes = $this->Assuntos->Anotacaoassuntos->Tiponotes->find('list');

        $this->viewBuilder()->layout('legislativo');
        $this->set(compact('assuntos', 'todosassuntos', 'tipoanotacoes'));
        $this->set('_serialize', ['assuntos']);
    }

    /**
     * subassuntosajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function subassuntosajax($id) {
        $assunto = $this->Assuntos->get($id, ['contain' => ['Assuntosfilhos']]);

        $this->set(compact('assunto'));
        $this->set('_serialize', ['assunto']);
    }

    /**
     * View method
     *
     * @param string|null $id Assunto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $assunto = $this->Assuntos->get($id, [
            'contain' => ['Users', 'Assuntosfilhos', 'Assuntospai', 'Anotacaoassuntos', 'Assuntosdocumentos', 'Leisassuntos.Leisnormas', 'Telaquestionarios']
        ]);

        $this->set('assunto', $assunto);
        $this->set('_serialize', ['assunto']);
    }

    /**
     * Search method
     *
     * @param string|null $palavra string com palavras chaves/tags.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function search() {
        $palavra = $this->request->data('txt-search');
        $palavras = explode(' ', str_replace(array(',', '.', ';', '!'), '', $palavra));
        $OR_tags = array('Tags.descricao' => "$palavra", 'Assuntos.descricao' => "$palavra");
        foreach ($palavras as $tag) {
            $OR_tags['Tags.descricao LIKE'] = "%$tag%";
            $OR_tags['Assuntos.descricao LIKE'] = "%$tag%";
            $OR_tags['Anotacaoassuntos.anotacao LIKE'] = "%$tag%";
        }

        $assuntos = $this->Assuntos->find()->distinct()->contain(['Assuntostags.Tags'])->leftJoinWith('Assuntostags.Tags')->leftJoinWith('Anotacaoassuntos')->where(['OR' => $OR_tags])->orderAsc('Assuntos.descricao');

        $this->set('assuntos', $assuntos);
        $this->set('_serialize', ['assuntos']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {

        $assunto = $this->Assuntos->newEntity();
        if ($this->request->is('post')) {
            $assunto = $this->Assuntos->patchEntity($assunto, $this->request->data);
            $assunto->dt_cadastro = date('Y-m-d H:i:s');
            $assunto->nome = $assunto->descricao;
            $assunto->user_id = $this->Auth->user('id');

            if ($this->request->data('assunto_id') && !is_numeric($this->request->data('assunto_id'))) {
                $new = $this->Assuntos->newEntity();
                $new->descricao = $this->request->data('assunto_id');
                $new->user_id = $this->Auth->user('id');
                $new->dt_cadastro = date('Y-m-d H:i:s');
                if ($this->Assuntos->save($new)) {
                    $assunto->assunto_id = $new->id;
                }
            }
            /*
             * isso será feito através de ajax depois
              $assuntosfilhos = array();
              if ($this->request->data('assuntos_id') && is_array($this->request->data('assuntos_id'))) {
              foreach ($this->request->data('assuntos_id') as $assuntofilho) {
              if (is_numeric($assuntofilho)) {
              $assuntosfilhos[] = $this->Assuntos->get($assuntofilho);
              } else {
              $new = $this->Assuntos->newEntity();
              $new->descricao = $assuntofilho;
              $new->user_id = $this->Auth->user('id');
              $new->dt_cadastro = date('Y-m-d H:i:s');
              if ($this->Assuntos->save($new)) {
              $assuntosfilhos[] = $new;
              }
              }
              }
              }
              $assunto->assuntosfilhos = $assuntosfilhos;
             */


            if ($this->Assuntos->save($assunto, ['associated' => 'Assuntosfilhos'])) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $assuntos = $this->Assuntos->find('list');
        $this->set(compact('assunto', 'assuntos'));
        $this->set('_serialize', ['assunto']);
    }

    /**
     * Getallinfos method
     *
     * @param string|null $id Assunto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function getallinfos($id) {
        $retorno = $this->Assuntos->get($id, ['contain' => ['Anotacaoassuntos.Tiponotes', 'Assuntosinformativos.Tipoinformativos', 'Assuntosfilhos', 'Assuntosdocumentos.Documentos', 'Leisassuntos.Leisnormas', 'Telaquestionarios', 'Assuntostags']]);
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Addsubassunto method
     *
     * @param string|null $id Assunto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function addsubassunto($id) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $assunto_id = $this->request->data('subassunto_id');
            $new_assunto = $this->request->data('subassunto_descricao');

            if (is_numeric($assunto_id)) {
                $new = $this->Assuntos->get($assunto_id);
                $new->assunto_id = $id;
            } else {
                $new = $this->Assuntos->newEntity();
                $new->assunto_id = $id;
                $new->nome = $new_assunto;
                $new->descricao = $new_assunto;
                $new->user_id = $this->Auth->user('id');
                $new->dt_cadastro = date('Y-m-d H:i:s');
            }

            if ($this->Assuntos->save($new)) {
                $retorno = $new;
            } else {
                $retorno = false;
            }
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Deletesubassunto method
     *
     * @param string|null $id Assunto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deletesubassunto($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if ($this->Assuntos->delete($this->Assuntos->get($id))) {
            $retorno = true;
        } else {
            $retorno = false;
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Getanotacoes method
     *
     * @param string|null $id Assunto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function getanotacoes($id) {
        $retorno = $this->Assuntos->Anotacaoassuntos->find()->contain(['Tiponotes'])->where(['Anotacaoassuntos.assunto_id' => $id]);
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Addanotacao method
     *
     * @param string|null $id Assunto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function addanotacao($id) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $anotacao = $this->Assuntos->Anotacaoassuntos->newEntity();
            $anotacao->assunto_id = $id;
            $anotacao->titulo = $this->request->data('titulo');
            $anotacao->anotacao = $this->request->data('anotacao');
            $anotacao->dt_cadastro = date('Y-m-d H:i:s');
            /* verifica se já existia esse tipo ou é um novo cadastro para ser vinculado */
            if (is_numeric($this->request->data('tiponote_id'))) {
                $anotacao->tiponote_id = $this->request->data('tiponote_id');
            } else {
                $new = $this->Assuntos->Anotacaoassuntos->Tiponotes->newEntity();
                $new->descricao = $this->request->data('tiponote_id');
                if ($this->Assuntos->Anotacaoassuntos->Tiponotes->save($new)) {
                    $anotacao->tiponote_id = $new->id;
                }
            }

            if ($this->Assuntos->Anotacaoassuntos->save($anotacao)) {
                $retorno = $anotacao;
            } else {
                $retorno = null;
            }
        } else {
            $retorno = null;
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Getanotacao method
     *
     * @param string|null $id Anotacaoassunto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function getanotacao($id) {
        $anotacao = $this->Assuntos->Anotacaoassuntos->get($id);

        $this->set(compact('anotacao'));
        $this->set('_serialize', ['anotacao']);
    }

    /**
     * Editanotacao method
     *
     * @param string|null $id Anotacaoassunto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editanotacao($id) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $anotacao = $this->Assuntos->Anotacaoassuntos->get($id);
            $anotacao->titulo = $this->request->data('titulo');
            $anotacao->anotacao = $this->request->data('anotacao');
            /* verifica se já existia esse tipo ou é um novo cadastro para ser vinculado */
            if (is_numeric($this->request->data('tiponote_id'))) {
                $anotacao->tiponote_id = $this->request->data('tiponote_id');
            } else {
                $new = $this->Assuntos->Anotacaoassuntos->Tiponotes->newEntity();
                $new->descricao = $this->request->data('tiponote_id');
                if ($this->Assuntos->Anotacaoassuntos->Tiponotes->save($new)) {
                    $anotacao->tiponote_id = $new->id;
                }
            }

            if ($this->Assuntos->Anotacaoassuntos->save($anotacao)) {
                $retorno = $anotacao;
            } else {
                $retorno = null;
            }
        } else {
            $retorno = null;
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Deleteanotacao method
     *
     * @param string|null $id Anotacaoassunto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deleteanotacao($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $anotacao = $this->Assuntos->Anotacaoassuntos->get($id);
        if ($this->Assuntos->Anotacaoassuntos->delete($anotacao)) {
            $retorno = true;
        } else {
            $retorno = false;
        }

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Tiponotesajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function tiponotesajax() {
        $retorno = $this->Assuntos->Anotacaoassuntos->Tiponotes->find('list')->orderAsc('Tiponotes.descricao');
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Addquestionario method
     *
     * @param string|null $id Assunto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function addquestionario($id) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $questionario = $this->Assuntos->Telaquestionarios->newEntity();
            $questionario->assunto_id = $id;
            $questionario->pergunta = $this->request->data('pergunta');
            $questionario->resposta = $this->request->data('resposta');
            $questionario->user_id = $this->Auth->user('id');
            $questionario->dt_cadastro = date('Y-m-d H:i:s');


            if ($this->Assuntos->Telaquestionarios->save($questionario)) {
                $retorno = $questionario;
            } else {
                $retorno = null;
            }
        } else {
            $retorno = null;
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Deletequestionario method
     *
     * @param string|null $id Telaquestionarios id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deletequestionario($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $questionario = $this->Assuntos->Telaquestionarios->get($id);
        if ($this->Assuntos->Telaquestionarios->delete($questionario)) {
            $retorno = true;
        } else {
            $retorno = false;
        }

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Attachlei method
     *
     * @param string|null $id Assunto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function attachlei($id) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lei = $this->Assuntos->Leisassuntos->newEntity();
            $lei->assunto_id = $id;
            $lei->leisnorma_id = $this->request->data('leisnorma_id');


            if ($this->Assuntos->Leisassuntos->save($lei)) {
                $leiassunto = $lei->id;
                $retorno = $this->Assuntos->Leisassuntos->Leisnormas->get($lei->leisnorma_id);
            } else {
                $retorno = null;
            }
        } else {
            $retorno = null;
        }
        $this->set(compact('retorno', 'leiassunto'));
        $this->set('_serialize', ['retorno', 'leiassunto']);
    }

    /**
     * Detachlei method
     *
     * @param string|null $id Leisassuntos id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function detachlei($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $lei = $this->Assuntos->Leisassuntos->get($id);
        if ($this->Assuntos->Leisassuntos->delete($lei)) {
            $retorno = true;
        } else {
            $retorno = false;
        }

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Addtag method
     *
     * @param string|null $id Assunto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function addtag($assunto_id) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tag = $this->request->data('tag_id');
            if (is_numeric($tag)) {// se o usuário selecionou uma tag que já existente
                $newtag_id = $tag;
            } else if (is_string($tag)) {// se o usuário digitou uma nova tag
                $newtag = $this->Assuntos->Assuntostags->Tags->newEntity();
                $newtag->descricao = $tag;
                $newtag->dt_cadastro = date('Y-m-d H:i:s');
                if ($this->Assuntos->Assuntostags->Tags->save($newtag)) {
                    $newtag_id = $newtag->id;
                }
            }

            $newtagassunto = $this->Assuntos->Assuntostags->newEntity();
            $newtagassunto->assunto_id = $assunto_id;
            $newtagassunto->tag_id = $newtag_id;
            $newtagassunto->dt_cadastro = date('Y-m-d H:i:s');


            if ($this->Assuntos->Assuntostags->save($newtagassunto)) {
                $retorno = true;
            } else {
                $retorno = null;
            }
        } else {
            $retorno = null;
        }
        $this->set(compact('retorno', 'newtag_id'));
        $this->set('_serialize', ['retorno', 'newtag_id']);
    }

    /**
     * Deletetag method
     *
     * @param string|null $id Assuntos id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deletetag($assunto_id = null) {
        $this->request->allowMethod(['post', 'patch', 'delete']);
        $tag = $this->Assuntos->Assuntostags->find()->where(['assunto_id' => $assunto_id, 'tag_id' => $this->request->data('tag_id')])->first();
        if ($this->Assuntos->Assuntostags->delete($tag)) {
            $retorno = true;
        } else {
            $retorno = false;
        }

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Listtags method
     *
     * @return \Cake\Network\Response|null
     */
    public function listtags() {
        $retorno = $this->Assuntos->Assuntostags->Tags->find('list')->orderAsc('descricao');
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Adddocumento method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function adddocumento() {
        if ($this->request->is('post') || $this->request->is('put') || $this->request->is('get')) {

            if (!empty($this->request->data['Filename'])) {
                $file = $this->request->data['Filedata']; //put the data into a var for easy use

                if ($this->isStringEmpty($this->request->data('documento-nome'))) {
                    $filename = $file['name'];
                } else {
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $filename = $this->request->data('documento-nome') . '.' . $ext;
                }

                if (!is_dir(WWW_ROOT . '/docs/assuntos/' . $this->request->data('assunto_id'))) {
                    debug(mkdir(WWW_ROOT . '/docs/assuntos/' . $this->request->data('assunto_id')));
                }
                $retorno = move_uploaded_file($file['tmp_name'], WWW_ROOT . '/docs/assuntos/' . $this->request->data('assunto_id') . '/' . $filename);
            }
        }

        $this->set('tpview', 'add');
        $this->set('retorno', $retorno);
        $this->viewBuilder()->layout('ajax');
        $this->render('ajax');
    }

    public function listdocumentos($assunto_id) {
        $documentos = array();
        $diretorio = WWW_ROOT . 'docs\\assuntos\\' . $assunto_id;
        if (is_dir($diretorio)) {
            $documentos = array_diff(scandir($diretorio), array('..', '.'));
        }
        $diretorio = 'docs\\assuntos\\' . $assunto_id;
        $this->set(compact('documentos', 'diretorio'));
        $this->set('_serialize', ['documentos', 'diretorio']);
    }

    public function deletedocumento($assunto_id) {

        $file = WWW_ROOT . 'docs\\assuntos\\' . $assunto_id . '\\' . $this->request->data('documento-nome');
        if (file_exists($file)) {
            $retorno = unlink($file);
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Assunto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $assunto = $this->Assuntos->patchEntity($this->Assuntos->get($id), $this->request->data);
            if ($this->Assuntos->save($assunto)) {
                $retorno = true;
            } else {
                $retorno = false;
            }
        }
        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Assunto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
//        $this->request->allowMethod(['post', 'delete']);
        if ($this->Auth->user('admin')) {
            $assunto = $this->Assuntos->get($id);
            if ($this->Assuntos->delete($assunto)) {
                $this->Flash->success(__('O registro foi removido com sucesso.'));
            } else {
                $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }

}
