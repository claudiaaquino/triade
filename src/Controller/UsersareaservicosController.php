<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Usersareaservicos Controller
 *
 * @property \App\Model\Table\UsersareaservicosTable $Usersareaservicos
 */
class UsersareaservicosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Empresas', 'Areaservicos', 'Users']
        ];
        $usersareaservicos = $this->paginate($this->Usersareaservicos->find()->where(['Usersareaservicos.empresa_id' => $this->Auth->user('empresa_id')]));

        $this->set(compact('usersareaservicos'));
        $this->set('_serialize', ['usersareaservicos']);
    }

    /**
     * View method
     *
     * @param string|null $id Usersareaservico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $usersareaservico = $this->Usersareaservicos->get($id, [
            'contain' => ['Empresas', 'Areaservicos', 'Users']
        ]);


        if ($usersareaservico->empresa_id != $this->Auth->user('empresa_id') || ($usersareaservico->empresa_id == $this->Auth->user('empresa_id') && !$this->Auth->user('admin_empresa')) || !$this->Auth->user('admin')) {
            $this->Flash->error(__('Você não tem permissão de visualizar um registro que não pertence à sua Empresa'));
            return $this->redirect(['action' => 'index']);
        }


        $this->set('usersareaservico', $usersareaservico);
        $this->set('_serialize', ['usersareaservico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $usersareaservico = $this->Usersareaservicos->newEntity();
        if ($this->request->is('post')) {
            $usersareaservico = $this->Usersareaservicos->patchEntity($usersareaservico, $this->request->data);
            $usersareaservico->dt_cadastro = date('Y-m-d H:i:s');
            $usersareaservico->empresa_id = $this->Auth->user('empresa_id');
            if ($this->Usersareaservicos->save($usersareaservico)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $areaservicos = $this->Usersareaservicos->Areaservicos->find('list')->where(['Areaservicos.empresa_id' => $this->Auth->user('empresa_id')])->orWhere(['todos = 1 ']);
        $users = $this->Usersareaservicos->Users->find('list', ['limit' => 200])->where(['Users.empresa_id' => $this->Auth->user('empresa_id')]);
        $this->set(compact('usersareaservico', 'empresas', 'areaservicos', 'users'));
        $this->set('_serialize', ['usersareaservico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Usersareaservico id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $usersareaservico = $this->Usersareaservicos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersareaservico = $this->Usersareaservicos->patchEntity($usersareaservico, $this->request->data);


            if ($usersareaservico->empresa_id != $this->Auth->user('empresa_id') || ($usersareaservico->empresa_id == $this->Auth->user('empresa_id') && !$this->Auth->user('admin_empresa')) || !$this->Auth->user('admin')) {
                $this->Flash->error(__('Você não tem permissão de editar um registro que não pertence à sua Empresa'));
                return $this->redirect(['action' => 'index']);
            }

            if ($this->Usersareaservicos->save($usersareaservico)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }

        $areaservicos = $this->Usersareaservicos->Areaservicos->find('list')->where(['Areaservicos.empresa_id' => $this->Auth->user('empresa_id')]);
        $users = $this->Usersareaservicos->Users->find('list')->where(['Users.empresa_id' => $this->Auth->user('empresa_id')]);
        $this->set(compact('usersareaservico', 'empresas', 'areaservicos', 'users'));
        $this->set('_serialize', ['usersareaservico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Usersareaservico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $usersareaservico = $this->Usersareaservicos->get($id);
        if ($usersareaservico->empresa_id == $this->Auth->user('empresa_id') && $this->Auth->user('empresa_id')) {
            if ($this->Usersareaservicos->delete($usersareaservico)) {
                $this->Flash->success(__('O registro foi removido com sucesso.'));
            } else {
                $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
            }
        } else {
            $this->Flash->error(__('Você não tem permissão de deletar um registro que não pertence à sua Empresa'));
        }

        return $this->redirect($this->request->referer());
    }

    /**
     * Lista os usuários do setor de serviço selecionado, e retorna um objeto do tipo json com os usuários.
     * 
     */
    public function loadUsers() {
        $empresa = $this->request->data["idEmpresa"];
        $areaServico = $this->request->data["idAreaServico"];

        $this->viewBuilder()->layout('ajax');
        $this->loadModel("Users");
        
        $retorno = $this->Users
                ->find('list')
                ->innerJoinWith('Usersareaservicos')
                ->contain(['Usersareaservicos'])
                ->select(['Users.id','Users.nome'])
                ->where(['Usersareaservicos.empresa_id' => $empresa, 'Usersareaservicos.areaservico_id' => $areaServico]);

        $tpview = 'list';
        $this->set(compact('retorno', 'tpview'));
        $this->set('_serialize', ['retorno', 'tpview']);
        $this->render('ajax');
    }

}
