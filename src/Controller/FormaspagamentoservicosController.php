<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Formaspagamentoservicos Controller
 *
 * @property \App\Model\Table\FormaspagamentoservicosTable $Formaspagamentoservicos
 */
class FormaspagamentoservicosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $formaspagamentoservicos = $this->paginate($this->Formaspagamentoservicos);

        $this->set(compact('formaspagamentoservicos'));
        $this->set('_serialize', ['formaspagamentoservicos']);
    }

    /**
     * View method
     *
     * @param string|null $id Formaspagamentoservico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $formaspagamentoservico = $this->Formaspagamentoservicos->get($id, [
            'contain' => ['Previsaoorcamentos']
        ]);

        $this->set('formaspagamentoservico', $formaspagamentoservico);
        $this->set('_serialize', ['formaspagamentoservico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $formaspagamentoservico = $this->Formaspagamentoservicos->newEntity();
        if ($this->request->is('post')) {
            $formaspagamentoservico = $this->Formaspagamentoservicos->patchEntity($formaspagamentoservico, $this->request->data);
            $formaspagamentoservico->dt_cadastro =  date('Y-m-d H:i:s');
            $formaspagamentoservico->user_id =  $this->Auth->user('id');
            $formaspagamentoservico->empresa_id =  $this->Auth->user('empresa_id');
            if ($this->Formaspagamentoservicos->save($formaspagamentoservico)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('formaspagamentoservico'));
        $this->set('_serialize', ['formaspagamentoservico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Formaspagamentoservico id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $formaspagamentoservico = $this->Formaspagamentoservicos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $formaspagamentoservico = $this->Formaspagamentoservicos->patchEntity($formaspagamentoservico, $this->request->data);
            if ($this->Formaspagamentoservicos->save($formaspagamentoservico)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('formaspagamentoservico'));
        $this->set('_serialize', ['formaspagamentoservico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Formaspagamentoservico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $formaspagamentoservico = $this->Formaspagamentoservicos->get($id);
        if ($this->Formaspagamentoservicos->delete($formaspagamentoservico)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
