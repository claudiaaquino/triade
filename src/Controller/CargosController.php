<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Cargos Controller
 *
 * @property \App\Model\Table\CargosTable $Cargos
 */
class CargosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {

        if ($this->request->is('post') && !empty($this->request->data('termo-pesquisa'))) {
            $query = $this->Cargos->find()->where(["descricao like '%" . $this->request->data('termo-pesquisa') . "%'"]);
        } else {
            $query = $this->Cargos->find();
        }

        $cargos = $this->paginate($query->order(['descricao']));

        $this->set(compact('cargos'));
        $this->set('_serialize', ['cargos']);
    }

    /**
     * View method
     *
     * @param string|null $id Cargo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $cargo = $this->Cargos->get($id, [
            'contain' => ['Funcionarios.Empresas']
        ]);

        $this->set('cargo', $cargo);
        $this->set('_serialize', ['cargo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $cargo = $this->Cargos->newEntity();
        if ($this->request->is('post')) {
            $cargo = $this->Cargos->patchEntity($cargo, $this->request->data);
            if ($this->Cargos->save($cargo)) {
                $this->Flash->success(__('O cargo foi salvo.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O cargo não pôde ser salvo. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('cargo'));
        $this->set('_serialize', ['cargo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cargo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $cargo = $this->Cargos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cargo = $this->Cargos->patchEntity($cargo, $this->request->data);
            if ($this->Cargos->save($cargo)) {
                $this->Flash->success(__('O cargo foi salvo.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O cargo não pôde ser salvo. Tente novamente.'));
            }
        }
        $this->set(compact('cargo'));
        $this->set('_serialize', ['cargo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cargo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $cargo = $this->Cargos->get($id);
        if ($this->Cargos->delete($cargo)) {
            $this->Flash->success(__('O cargo foi deletado.'));
        } else {
            $this->Flash->error(__('O cargo não pôde ser deletado. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }

    /**
     * Desativar method
     *
     * @param string|null $id Cargo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function desativar($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if ($this->Cargos->updateAll(['status' => 0], ['id' => $id])) {
            $this->Flash->success(__('O cargo foi deletado.'));
        } else {
            $this->Flash->error(__('O cargo já estava desativado.'));
        }

        return $this->redirect($this->request->referer());
    }

}
