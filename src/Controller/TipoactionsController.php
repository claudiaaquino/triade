<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tipoactions Controller
 *
 * @property \App\Model\Table\TipoactionsTable $Tipoactions
 */
class TipoactionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $tipoactions = $this->paginate($this->Tipoactions);

        $this->set(compact('tipoactions'));
        $this->set('_serialize', ['tipoactions']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipoaction id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoaction = $this->Tipoactions->get($id, [
            'contain' => ['Telaquestionarios']
        ]);

        $this->set('tipoaction', $tipoaction);
        $this->set('_serialize', ['tipoaction']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoaction = $this->Tipoactions->newEntity();
        if ($this->request->is('post')) {
            $tipoaction = $this->Tipoactions->patchEntity($tipoaction, $this->request->data);
            $tipoaction->dt_cadastro =  date('Y-m-d H:i:s');
            $tipoaction->user_id =  $this->Auth->user('id');
            if ($this->Tipoactions->save($tipoaction)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tipoaction'));
        $this->set('_serialize', ['tipoaction']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipoaction id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoaction = $this->Tipoactions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoaction = $this->Tipoactions->patchEntity($tipoaction, $this->request->data);
            if ($this->Tipoactions->save($tipoaction)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('tipoaction'));
        $this->set('_serialize', ['tipoaction']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipoaction id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tipoaction = $this->Tipoactions->get($id);
        if ($this->Tipoactions->delete($tipoaction)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
