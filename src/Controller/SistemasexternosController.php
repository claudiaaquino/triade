<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Sistemasexternos Controller
 *
 * @property \App\Model\Table\SistemasexternosTable $Sistemasexternos
 */
class SistemasexternosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {

        if ($this->request->is('post') && !empty($this->request->data('termo-pesquisa'))) {
            $query = $this->Sistemasexternos->find()->where(["Sistemasexternos.descricao like '%" . $this->request->data('termo-pesquisa') . "%'"]);
        } else {
            $query = $this->Sistemasexternos->find();
        }
        $sistemasexternos = $this->paginate($query->orderAsc('Sistemasexternos.descricao'));

        $this->set(compact('sistemasexternos'));
        $this->set('_serialize', ['sistemasexternos']);
    }

    /**
     * View method
     *
     * @param string|null $id Sistemasexterno id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $sistemasexterno = $this->Sistemasexternos->get($id, [
            'contain' => ['Acessosexternos.Empresas']
        ]);

        $this->set('sistemasexterno', $sistemasexterno);
        $this->set('_serialize', ['sistemasexterno']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $sistemasexterno = $this->Sistemasexternos->newEntity();
        if ($this->request->is('post')) {
            $sistemasexterno = $this->Sistemasexternos->patchEntity($sistemasexterno, $this->request->data);
            $sistemasexterno->dt_cadastro = date('Y-m-d H:i:s');
            $sistemasexterno->user_id = $this->Auth->user('id');
            if ($this->Sistemasexternos->save($sistemasexterno)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('sistemasexterno'));
        $this->set('_serialize', ['sistemasexterno']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sistemasexterno id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $sistemasexterno = $this->Sistemasexternos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sistemasexterno = $this->Sistemasexternos->patchEntity($sistemasexterno, $this->request->data);
            if ($this->Sistemasexternos->save($sistemasexterno)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('sistemasexterno'));
        $this->set('_serialize', ['sistemasexterno']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sistemasexterno id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $sistemasexterno = $this->Sistemasexternos->get($id);
        if ($this->Sistemasexternos->delete($sistemasexterno)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }

}
