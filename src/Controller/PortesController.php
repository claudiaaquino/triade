<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Portes Controller
 *
 * @property \App\Model\Table\PortesTable $Portes
 */
class PortesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $portes = $this->paginate($this->Portes);

        $this->set(compact('portes'));
        $this->set('_serialize', ['portes']);
    }

    /**
     * View method
     *
     * @param string|null $id Porte id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $porte = $this->Portes->get($id, [
            'contain' => ['Empresas', 'Empresas.Cidades', 'Empresas.Estados']
        ]);

        $this->set('porte', $porte);
        $this->set('_serialize', ['porte']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $porte = $this->Portes->newEntity();
        if ($this->request->is('post')) {
            $porte = $this->Portes->patchEntity($porte, $this->request->data);
            if ($this->Portes->save($porte)) {
                $this->Flash->success(__('Porte cadastrado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao cadastrar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('porte'));
        $this->set('_serialize', ['porte']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Porte id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $porte = $this->Portes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $porte = $this->Portes->patchEntity($porte, $this->request->data);
            if ($this->Portes->save($porte)) {
                $this->Flash->success(__('Porte atualizado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao atualizar. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('porte'));
        $this->set('_serialize', ['porte']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Porte id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $porte = $this->Portes->get($id);
        if ($this->Portes->delete($porte)) {
            $this->Flash->success(__('Porte removido com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao remover porte. Por favor, tente novamente.'));
        }

        return $this->redirect($this->request->referer());
    }
}
