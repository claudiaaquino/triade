<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Apuracaoformas Controller
 *
 * @property \App\Model\Table\ApuracaoformasTable $Apuracaoformas
 */
class ApuracaoformasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $apuracaoformas = $this->paginate($this->Apuracaoformas);

        $this->set(compact('apuracaoformas'));
        $this->set('_serialize', ['apuracaoformas']);
    }

    /**
     * View method
     *
     * @param string|null $id Apuracaoforma id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $apuracaoforma = $this->Apuracaoformas->get($id, [
            'contain' => ['Previsaoorcamentos', 'Empresas']
        ]);

        $this->set('apuracaoforma', $apuracaoforma);
        $this->set('_serialize', ['apuracaoforma']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $apuracaoforma = $this->Apuracaoformas->newEntity();
        if ($this->request->is('post')) {
            $apuracaoforma = $this->Apuracaoformas->patchEntity($apuracaoforma, $this->request->data);
            $apuracaoforma->dt_cadastro =  date('Y-m-d H:i:s');
            $apuracaoforma->user_id =  $this->Auth->user('id');
            if ($this->Apuracaoformas->save($apuracaoforma)) {
                $this->Flash->success(__('Registro salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao adicionar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('apuracaoforma'));
        $this->set('_serialize', ['apuracaoforma']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Apuracaoforma id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $apuracaoforma = $this->Apuracaoformas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $apuracaoforma = $this->Apuracaoformas->patchEntity($apuracaoforma, $this->request->data);
            if ($this->Apuracaoformas->save($apuracaoforma)) {
                $this->Flash->success(__('O registro foi atualizado com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
            }
        }
        $this->set(compact('apuracaoforma'));
        $this->set('_serialize', ['apuracaoforma']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Apuracaoforma id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $apuracaoforma = $this->Apuracaoformas->get($id);
        if ($this->Apuracaoformas->delete($apuracaoforma)) {
            $this->Flash->success(__('O registro foi removido com sucesso.'));
        } else {
            $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
        }

        return $this->redirect($this->request->referer());
    }
}
