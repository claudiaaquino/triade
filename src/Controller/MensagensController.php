<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Mensagens Controller
 *
 * @property \App\Model\Table\MensagensTable $Mensagens
 */
class MensagensController extends AppController {

    /**
     * Index method
     * @param string|null $id Mensagen id.
     * @return \Cake\Network\Response|null
     */
    public function index($id = null) {
        $this->paginate = [
            'limit' => 5,
            'contain' => ['Users.Empresas', 'Mensagensdestinatarios.Users.Empresas', 'Users.Usersareaservicos', 'Respostas', 'Mensagensdocumentos.Documentos.Tipodocumentos']
        ];

        //obtem todas as mensagens e organiza para paginação
        $query = $this->Mensagens->find()->matching('Mensagensdestinatarios');

        if ($this->request->is('post')) {
            if ($this->request->data('has_anexo')) {
                $query->innerJoinWith('Mensagensdocumentos');
            }

            if (is_array($this->request->data('remetentes_search'))) {
                $query->where(['Mensagens.user_id in ' => $this->request->data('remetentes_search')]);
            }

            if (is_array($this->request->data('empresa_search'))) {
                $query->innerJoinWith('Users')->where(['Users.empresa_id in ' => $this->request->data('empresa_search')]);
            }


            if ($this->request->data('dt_inicio')) {
                $query->andWhere(['Mensagens.dt_envio >= ' => $this->convertDateBRtoEN($this->request->data('dt_inicio'))]);
            }

            if ($this->request->data('dt_final')) {
                $query->andWhere(['Mensagens.dt_envio <= ' => $this->convertDateBRtoEN($this->request->data('dt_final'))]);
            }

            if ($this->request->data('assunto_search')) {
                $query->andWhere(['Mensagens.assunto like ' => '%' . $this->request->data('assunto_search') . '%']);
            }
        }


        $mensagens = $this->paginate($query->where(['Mensagensdestinatarios.user_id' => $this->Auth->user('id')])->orderDesc('Mensagens.dt_envio'));
        //verificar total de mensagens
        $count = $query->count();
        $count_unread = $query->andWhere(['Mensagensdestinatarios.dt_leitura is null'])->count();



        /* -------------***DADOS USADOS NO FILTRO PARA FILTRAR OS DADOS DA TABELA******* */

        //campos para compor nova mensagem
        $newmensagem = $this->Mensagens->newEntity();

        if ($this->Auth->user('admin')) {
            $users = $this->Mensagens->Users->find('list');
            $empresas = $this->Mensagens->Empresas->find('list');
        } else {
            $users = $this->Mensagens->Users->find('list')->innerJoinWith('Mensagens.Mensagensdestinatarios')->where(['Mensagensdestinatarios.user_id' => $this->Auth->user('id')]);
            $empresas = $this->Mensagens->Empresas->find('list')->where(['id' => $this->_ID_TRIADE])->orWhere(['id' => $this->Auth->user('empresa_id')]);
        }
        $funcionarios = $this->Mensagens->Empresas->Funcionarios->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')]);
        $areaservicos = $this->Mensagens->Areaservicos->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')])->orWhere(['todos = 1 '])->orderDesc('descricao');

        $this->set(compact('mensagens', 'newmensagem', 'empresas', 'areaservicos', 'users', 'count', 'count_unread', 'id', 'funcionarios'));
        $this->set('_serialize', ['mensagens']);
    }

    /**
     * Enviadas method
     * @param string|null $id Mensagen id.
     * @return \Cake\Network\Response|null
     */
    public function enviadas($id = null) {
        $this->paginate = [
            'limit' => 5,
            'contain' => ['Mensagensdestinatarios.Users.Empresas', 'Users.Usersareaservicos', 'Respostas', 'Mensagensdocumentos.Documentos.Tipodocumentos']
        ];

        //obtem todas as mensagens e organiza para paginação
        $query = $this->Mensagens->find();

        if ($this->request->is('post')) {
            if ($this->request->data('has_anexo')) {
                $query->innerJoinWith('Mensagensdocumentos');
            }

            if (is_array($this->request->data('user_ids'))) {
                $query->innerJoinWith('Mensagensdestinatarios')->where(['Mensagensdestinatarios.user_id in ' => $this->request->data('user_ids')]);
            }

            if (is_numeric($this->request->data('empresa_id'))) {
                $query->andWhere(['Mensagens.empresa_id' => $this->request->data('empresa_id')]);
            }

            if (is_numeric($this->request->data('areaservico_id'))) {
                $query->andWhere(['Mensagens.areaservico_id' => $this->request->data('areaservico_id')]);
            }


            if ($this->request->data('dt_inicio')) {
                $query->andWhere(['Mensagens.dt_envio >= ' => $this->convertDateBRtoEN($this->request->data('dt_inicio'))]);
            }

            if ($this->request->data('dt_final')) {
                $query->andWhere(['Mensagens.dt_envio <= ' => $this->convertDateBRtoEN($this->request->data('dt_final'))]);
            }

            if ($this->request->data('assunto')) {
                $query->andWhere(['Mensagens.assunto like ' => '%' . $this->request->data('assunto') . '%']);
            }
        }


        $mensagens = $this->paginate($query->where(['Mensagens.user_id' => $this->Auth->user('id')])->orderDesc('Mensagens.dt_envio'));
        //verificar total de mensagens
        $count = $query->count();


        /* -------------***DADOS USADOS NO FILTRO PARA FILTRAR OS DADOS DA TABELA******* */

        if ($this->Auth->user('admin')) {
            $empresas = $this->Mensagens->Empresas->find('list');
            $areaservicos = $this->Mensagens->Areaservicos->find('list')->where(['empresa_id' => $this->Auth->user('empresa_id')])->orWhere(['todos = 1 '])->orderDesc('descricao');
        }

        $users = $this->Mensagens->Users->find('list')->innerJoinWith('Mensagensdestinatarios.Mensagens')->where(['Mensagens.user_id' => $this->Auth->user('id')]);
        $this->set(compact('mensagens', 'areaservicos', 'empresas', 'count', 'id', 'users'));
        $this->set('_serialize', ['mensagens']);
    }

    /**
     * Unreadajax method
     *
     * @return \Cake\Network\Response|null
     */
    public function unreadajax() {
        $mensagens = $this->Mensagens->find()->matching('Mensagensdestinatarios')->contain(['Users.Empresas'])->where(['Mensagensdestinatarios.user_id' => $this->Auth->user('id')])->andWhere(['Mensagensdestinatarios.dt_leitura is null']);
        $count_unread = $mensagens->count();
        $this->set(compact('mensagens', 'count_unread'));
        $this->set('_serialize', ['mensagens']);
    }

    /**
     * View method
     *
     * @param string|null $id Mensagen id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /* public function view($id = null) {
      $mensagen = $this->Mensagens->get($id, [
      'contain' => ['Empresas', 'Areaservicos', 'Users', 'Mensagensdestinatarios']
      ]);

      $this->set('mensagen', $mensagen);
      $this->set('_serialize', ['mensagen']);
      } */

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $mensagen = $this->Mensagens->newEntity();

        if ($this->request->is('post')) {
            $mensagen = $this->Mensagens->patchEntity($mensagen, $this->request->data);
            $mensagen->dt_envio = date('Y-m-d H:i:s');
            $mensagen->user_id = $this->Auth->user('id');
            $mensagen->assunto = strip_tags($mensagen->assunto);
//            $mensagen->texto = strip_tags($mensagen->texto, "<div><img><br><blockquote><font><b><i><strike><u><ul><li><ol><a>");

            if ($mensagen->user_ids) {
                //fazer aqui para capturar emails digitados também.
            } else if ($mensagen->sendoptions == 'todos_triade') {
                $mensagen->user_ids = $this->Mensagens->Users->find()->select(['id'])->where(['empresa_id' => $this->_ID_TRIADE]);
            } else if ($mensagen->sendoptions == 'todos_empresa') {
                $mensagen->todos_empresa = 1;
                $mensagen->user_ids = $this->Mensagens->Users->find()->select(['id'])->where(['empresa_id' => $mensagen->empresa_id]);
            } else if ($mensagen->sendoptions == 'somente_admin') {
                $mensagen->user_ids = $this->Mensagens->Users->find()->select(['id'])->where(['empresa_id' => $mensagen->empresa_id, 'admin_empresa' => 1]);
            } else if ($mensagen->sendoptions == 'todos_sistema') {
                $mensagen->todos_sistema = 1;
                $mensagen->user_ids = $this->Mensagens->Users->find()->select(['id'])->where(['status = 1 ']);
            } else if ($mensagen->empresa_id && ($this->isStringEmpty($mensagen->areaservico_id))) {
                $mensagen->user_ids = $this->Mensagens->Users->find()->select(['id'])->where(['empresa_id' => $mensagen->empresa_id]);
            } else if ($mensagen->empresa_id && (!$this->isStringEmpty($mensagen->areaservico_id))) {
                $mensagen->user_ids = $this->Mensagens->Users->find()->innerJoinWith('Usersareaservicos')->select(['Users.id'])
                        ->where(['Usersareaservicos.empresa_id' => $mensagen->empresa_id, 'Usersareaservicos.areaservico_id' => $mensagen->areaservico_id]);
            }

            $mensagemdocumentos = array();
            if (!empty($mensagen->file)) {
                $mensagemdocumentos = $this->saveDocuments($mensagen);
            }

            if ($mensagen->encaminhada != 'false' && $mensagen->encaminhada > 0) {
                $fwd_msg = $this->Mensagens->find()->leftJoinWith('Mensagensdocumentos')->contain(['Mensagensdocumentos', 'Users.Empresas'])
                                ->where(['Mensagens.id' => $mensagen->encaminhada])->first();


                if ($fwd_msg->mensagensdocumentos) {
                    foreach ($fwd_msg->mensagensdocumentos as $documento) {
                        $mensagemdocumento = $this->Mensagens->Mensagensdocumentos->newEntity();
                        $mensagemdocumento->documento_id = $documento->documento_id;
                        $mensagemdocumentos[] = $mensagemdocumento;
                    }
                }
                $empresa_sessao = $this->Auth->user('empresa');
                $empresanome_remetente = $fwd_msg->user->empresa ? ' (' . $fwd_msg->user->empresa->razao . ')' : '';
                $empresanome_destinatario = !empty($empresa_sessao) ? ' (' . $empresa_sessao['razao'] . ')' : '';

                $mensagen->texto .= '<hr><strong>Mensagem Original </strong><br>enviada em ' . $fwd_msg->dt_envio . '<br> De ' . $fwd_msg->user->nome . $empresanome_remetente
                        . ' para ' . $this->Auth->user('nome') . $empresanome_destinatario . '<br><br>' . $fwd_msg->texto;
            }
            $mensagen->mensagensdocumentos = $mensagemdocumentos;

            $usersdestinatarios = array();
            $users_ids = array();
            if ($mensagen->user_ids) {
                foreach ($mensagen->user_ids as $user_id) {
                    $user_id = is_numeric($user_id) ? $user_id : $user_id->id;
                    $user = $this->Mensagens->Mensagensdestinatarios->newEntity();
                    $user->user_id = $user_id;
                    $usersdestinatarios[] = $user;
                    $users_ids[] = $user_id; // usado para disparar os emails depois
                }
            }
            $mensagen->mensagensdestinatarios = $usersdestinatarios;

            $mensagen->texto = trim($mensagen->texto);
            if ($this->Mensagens->save($mensagen)) {
                $this->Flash->success(__('Enviada com sucesso.'));
                $this->disparaEmailMensagemEnviada($mensagen, $users_ids);
            } else {
                $this->Flash->error(__('Houve um erro ao enviar a mensagem, verifique os campos e tente novamente.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * SaveDocument method
     *
     * @return Mensagensdocumentos[] $mensagemdocumentos
     */
    public function saveDocuments($mensagen) {
        $mensagemdocumentos = array();
        $this->loadModel('Documentos');
        foreach ($mensagen->file as $key => $file) {
            if ($file['size'] > 0) {
                $documento = $this->Documentos->newEntity();
                $documento->empresa_id = $this->Auth->user('empresa_id');
                $documento->user_enviou = $this->Auth->user('id');
                $documento->filesize = $file['size'];
                $documento->tipodocumento_id = $mensagen->tipo_documentos[$key];
                if (!$this->isStringEmpty($mensagen->funcionario[$key])) {
                    $documento->funcionario_id = $mensagen->funcionario[$key];
                }
                $documento->descricao = $mensagen->descricoes[$key];
                $documento->file = $this->saveAttachment($file);
                $documento->dt_enviado = date('Y-m-d H:i:s');
                if ($this->Documentos->save($documento)) {
                    $mensagemdocumento = $this->Mensagens->Mensagensdocumentos->newEntity();
                    $mensagemdocumento->documento_id = $documento->id;
                    $mensagemdocumentos[] = $mensagemdocumento;
                }
            }
        }
        return $mensagemdocumentos;
    }

    /**
     * Edit method
     *
     * @param string|null $id Mensagen id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    /* public function edit($id = null) {
      $mensagen = $this->Mensagens->get($id, [
      'contain' => []
      ]);
      if ($this->request->is(['patch', 'post', 'put'])) {
      $mensagen = $this->Mensagens->patchEntity($mensagen, $this->request->data);
      if ($this->Mensagens->save($mensagen)) {
      $this->Flash->success(__('O registro foi atualizado com sucesso'));

      return $this->redirect(['action' => 'index']);
      } else {
      $this->Flash->error(__('Houve um erro ao tentar atualizar esse registro, verifique os campos e tente novamente.'));
      }
      }
      $empresas = $this->Mensagens->Empresas->find('list', ['limit' => 200]);
      $areaservicos = $this->Mensagens->Areaservicos->find('list', ['limit' => 200]);
      $users = $this->Mensagens->Users->find('list', ['limit' => 200]);
      $this->set(compact('mensagen', 'empresas', 'areaservicos', 'users'));
      $this->set('_serialize', ['mensagen']);
      } */

    /**
     * Delete method
     *
     * @param string|null $id Mensagen id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /* public function delete($id = null) {
      $this->request->allowMethod(['post', 'delete']);
      $mensagen = $this->Mensagens->get($id);
      if ($this->Mensagens->delete($mensagen)) {
      $this->Flash->success(__('O registro foi removido com sucesso.'));
      } else {
      $this->Flash->error(__('Houve um erro ao tentar deletar esse registro, tente novamente mais tarde.'));
      }

      return $this->redirect($this->request->referer());
      } */

    /**
     * RegistraLeitura method
     *
     * @param string $id Mensagen id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function registraLeitura($id) {
        $mensagensdetinatarios = $this->Mensagens->Mensagensdestinatarios->find()->where(['mensagen_id' => $id, 'user_id' => $this->Auth->user('id')])->first();
        if (!$mensagensdetinatarios->dt_leitura) {
            $mensagensdetinatarios->dt_leitura = date('Y-m-d H:i:s');
            if ($this->Mensagens->Mensagensdestinatarios->save($mensagensdetinatarios)) {
                $retorno = $mensagensdetinatarios->dt_leitura;
            } else {
                $retorno = false;
            }
        } else {
            $retorno = $mensagensdetinatarios->dt_leitura;
        }

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
    }

    function disparaEmailMensagemEnviada($mensagem, $users_ids) {
        try {
            $userremetente = $this->Mensagens->Mensagensdestinatarios->Users->get($mensagem->user_id);
            foreach ($users_ids as $user_id) {
                $user = $this->Mensagens->Mensagensdestinatarios->Users->get($user_id);
                if ($user->notificacao_mensagem) {
                    $email = new Email('default');
                    $email->viewVars(['nomecliente' => $user->nome, 'nomeremetente' => $userremetente->nome, 'idmensagem' => $mensagem->id]);
                    $email->subject('Você recebeu nova mensagem do Sistema Tríade')
                            ->template('default', 'novamensagem')
                            ->emailFormat('html')
                            ->to($user->email)
                            ->send();
                }
            }
        } catch (Exception $ex) {
            $this->Flash->error(__('Não foi possível enviar email de confirmação para o email informado. Confira se está correto, por favor.'));
        }
    }

}
